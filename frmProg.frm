VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmProg 
   Caption         =   "Downloading Please Wait"
   ClientHeight    =   1515
   ClientLeft      =   5310
   ClientTop       =   4155
   ClientWidth     =   4785
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   1515
   ScaleWidth      =   4785
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   300
      Left            =   3720
      Top             =   3000
   End
   Begin MSComctlLib.ProgressBar prgDnld 
      Height          =   300
      Left            =   120
      TabIndex        =   0
      Top             =   1080
      Width           =   4545
      _ExtentX        =   8017
      _ExtentY        =   529
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.Shape Shape1 
      BorderStyle     =   0  'Transparent
      FillColor       =   &H00C0C0C0&
      FillStyle       =   0  'Solid
      Height          =   120
      Left            =   240
      Top             =   3075
      Width           =   3975
   End
   Begin VB.Label Label5 
      Caption         =   "1"
      Height          =   285
      Left            =   1320
      TabIndex        =   4
      Top             =   600
      Width           =   825
   End
   Begin VB.Label Label4 
      Caption         =   "Record No.:"
      Height          =   285
      Left            =   240
      TabIndex        =   3
      Top             =   600
      Width           =   945
   End
   Begin VB.Label Label2 
      Height          =   285
      Left            =   1800
      TabIndex        =   2
      Top             =   240
      Width           =   1350
   End
   Begin VB.Label Label1 
      Caption         =   "Creating Book No.:"
      Height          =   285
      Left            =   240
      TabIndex        =   1
      Top             =   240
      Width           =   1425
   End
   Begin VB.Shape Shape2 
      BorderStyle     =   0  'Transparent
      FillColor       =   &H00404080&
      FillStyle       =   0  'Solid
      Height          =   285
      Left            =   240
      Top             =   3000
      Width           =   3975
   End
End
Attribute VB_Name = "frmProg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Load()

If WindowState = vbNormal Then
    Me.Move (1000)
    Me.Top = 500
    prgDnld.Value = 1
End If
Shape1.Width = 1
frmProg.Width = 4905
frmProg.Height = 2025
'    Timer1.Enabled = True
'    For i = 1 To 50 Step 1
'        prgDnld.Value = i
'    Next i
End Sub

Private Sub Timer1_Timer()

'    For i = 1 To 100 Step 1
'        prgDnld.Value = i
'    Next i
'    Unload Me
End Sub

Sub ProgressBar(cnt As Double, bkno As String)

Label2.Caption = bkno
Label5.Caption = Label5.Caption + 1

If Shape1.Width + cnt > Shape2.Width Or Shape1.Width + cnt = Shape2.Width Then
    Label2.Caption = ""
    Timer1.Enabled = False
    Label5.Caption = "1"
    Shape1.Width = 0
Else
    Shape1.Width = Shape1.Width + cnt
End If

If prgDnld.Value + cnt > 100 Or prgDnld.Value + cnt = 100 Then
    Label2.Caption = ""
    Timer1.Enabled = False
    Label5.Caption = "1"
    prgDnld.Value = 0
Else
    prgDnld.Value = prgDnld.Value + cnt
End If

End Sub
