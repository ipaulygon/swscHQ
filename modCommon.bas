Attribute VB_Name = "modCommon"
Option Explicit

'database connection
Global g_Conn As ADODB.Connection
Global g_Conn_backup As ADODB.Connection 'Added by J.Espiritu for Database Replication to MCFSDBBACKUP

'recordset
Global g_rs_RFF As ADODB.Recordset
Global g_rs_RHH As ADODB.Recordset
Global g_rs_RRATE As ADODB.Recordset
Global g_rs_RREADER As ADODB.Recordset
Global g_rs_SPARAM As ADODB.Recordset
Global g_rs_SUSER As ADODB.Recordset
Global g_rs_GRIDMRU As ADODB.Recordset
Global g_rs_TBOOK As ADODB.Recordset
Global g_rs_BUSCTR As ADODB.Recordset
Global g_rs_TFCONN As ADODB.Recordset
Global g_rs_TSCHED As ADODB.Recordset
Global g_rs_TUPBOOK As ADODB.Recordset
Global g_rs_TDOWNLOAD As ADODB.Recordset
Global g_rs_TUPLOAD As ADODB.Recordset
Global g_rs_TBKINFO As ADODB.Recordset
Global g_rs_BOOKSTAT As ADODB.Recordset
Global g_rs_BOOKINQ As ADODB.Recordset
Global g_rs_RBUSCTR As ADODB.Recordset
Global g_rs_TDLERROR As ADODB.Recordset
Global g_rs_TUPLOADHIS As ADODB.Recordset
Global g_rs_TBKINFOHIS As ADODB.Recordset
Global g_rs_TVEREXTRACT As ADODB.Recordset
Global g_rs_TDCBOOK As ADODB.Recordset
Global g_rs_TCUSTMASTER As ADODB.Recordset
Global g_rs_TBASICHRGDTL As ADODB.Recordset
Global g_rs_TDCDOWNLOAD As ADODB.Recordset
Global g_rs_TRCBOOK As ADODB.Recordset
Global g_rs_TRCDOWNLOAD As ADODB.Recordset

'rover file
Global g_str_Table As String
Global g_str_AddEdit As String
Global g_str_rep As String
Global g_str_DLUL As String

Public Appn As CRAXDRT.Application      'Just needed to hold everything together
Public cReport As CRAXDRT.Report        'Main object you will work with

' INI file section variables
Global g_Company As String              ' Company Name
Global g_Loader_Path As String          ' rover loader program location
Global g_Current_Filepath  As String    ' location for the intermediatre downloaded/uploaded text files
Global g_BillMonth As String
Global g_Database As String
Global g_DataSource As String
Global g_User As String                 'logged in user
Global g_Security As String

'Validation variable
Global g_ActionItem As String
Global g_ReOpeningFee As String

'meter details
Global gInstallNo As String
Global gMeterNo As String
Global gRdgDate As String

'User Identifier 'added 7-17-2013 jums
Global g_rs_headername As String
Global g_rs_userlevel As String
Global g_AppName As String

' Passed variables to MRU Management forms
Public sBusCenter As String
Public sMRU As String
Public intDayNo As Integer
Public intBillCycle As Integer

' Report Parameters Array
Public Const RepParmArraySize = 6
Public arrReportParm(RepParmArraySize) As String

'new code added Dir 08-08-2013 jums
Global g_dir As ADODB.Recordset

'length of bookno
Public Const lBookNo = 8

 'API DECLARATIONS
Declare Function GetPrivateProfileString Lib "kernel32" Alias _
                 "GetPrivateProfileStringA" (ByVal lpApplicationName _
                 As String, ByVal lpKeyName As Any, ByVal lpDefault _
                 As String, ByVal lpReturnedString As String, ByVal _
                 nSize As Long, ByVal lpFileName As String) As Long
Declare Function WritePrivateProfileString Lib "kernel32" Alias _
                 "WritePrivateProfileStringA" (ByVal lpApplicationName _
                 As String, ByVal lpKeyName As Any, ByVal lpString As Any, _
                 ByVal lpFileName As String) As Long
                 
'-------------------------------
' Customize Msgbox Button Text
'-------------------------------
Private Declare Function GetCurrentThreadId Lib "kernel32" () As Long
Private Declare Function SetWindowsHookEx Lib "user32" Alias "SetWindowsHookExA" (ByVal idHook As Long, ByVal lpfn As Long, ByVal hmod As Long, ByVal dwThreadId As Long) As Long
Private Declare Function UnhookWindowsHookEx Lib "user32" (ByVal hHook As Long) As Long
Private Declare Function FindWindowEx Lib "user32.dll" Alias "FindWindowExA" (ByVal hwndParent As Long, ByVal hwndChildAfter As Long, ByVal lpszClass As String, ByVal lpszWindow As String) As Long
Private Declare Function SetWindowText Lib "user32.dll" Alias "SetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String) As Long
Private Declare Function GetClassName Lib "user32.dll" Alias "GetClassNameA" (ByVal hwnd As Long, ByVal lpClassName As String, ByVal nMaxCount As Long) As Long
Private Declare Function SendMessage Lib "user32.dll" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
    Private Const HCBT_ACTIVATE = 5
    Private Const WH_CBT = 5
    Private hHook As Long, ButtonText(3) As String, PasswordChar As Byte

Private Function ChangeButtonsText(ByVal lMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
    Dim t As Integer, cName As String, Length As Long, Btn(0 To 3) As Long


    If lMsg = HCBT_ACTIVATE Then
        Btn(0) = FindWindowEx(wParam, 0, vbNullString, vbNullString)


        For t = 1 To 3
            Btn(t) = FindWindowEx(wParam, Btn(t - 1), vbNullString, vbNullString)
            If Btn(t) = 0 Then Exit For
        Next t


        For t = 0 To 3


            If (Btn(t) <> 0) And (Btn(t) <> wParam) Then
                cName = Space(255)
                Length = GetClassName(Btn(t), cName, 255)
                cName = Left(cName, Length)


                If UCase(cName) = "BUTTON" Then
                    If ButtonText(t) <> vbNullString Then SetWindowText Btn(t), ButtonText(t)
                End If
            End If
        Next t
        UnhookWindowsHookEx hHook
    End If
    ChangeButtonsText = False
End Function

Public Function CustomMsgBox(ByVal zMessage As String, Optional ByVal zButtons As VbMsgBoxStyle = vbOKOnly, Optional ByVal zTitle As String = vbNullString, Optional ByVal Button1Text As String = vbNullString, Optional ByVal Button2Text As String = vbNullString, Optional ByVal Button3Text As String = vbNullString) As VbMsgBoxResult
    Dim Thread As Long
    ButtonText(0) = Button1Text
    ButtonText(1) = Button2Text
    ButtonText(2) = Button3Text
    Thread = GetCurrentThreadId()
    hHook = SetWindowsHookEx(WH_CBT, AddressOf ChangeButtonsText, ByVal 0&, Thread)

    If zTitle = vbNullString Then
        CustomMsgBox = MsgBox(zMessage, zButtons)
    Else
        CustomMsgBox = MsgBox(zMessage, zButtons, zTitle)
    End If
End Function
                 
Public Function sGetINI(sINIFile As String, sSection As String, sKey _
                As String, sDefault As String) As String
    Dim sTemp As String * 256
    Dim nLength As Integer
    sTemp = Space$(256)
    nLength = GetPrivateProfileString(sSection, sKey, sDefault, sTemp, _
              255, sINIFile)
    sGetINI = Left$(sTemp, nLength)
End Function
Public Sub writeINI(sINIFile As String, sSection As String, sKey _
           As String, sValue As String)
    Dim n As Integer
    Dim sTemp As String
    sTemp = sValue
    'Replace any CR/LF characters with spaces
    For n = 1 To Len(sValue)
        If Mid$(sValue, n, 1) = vbCr Or Mid$(sValue, n, 1) = vbLf _
        Then Mid$(sValue, n) = " "
    Next n
    n = WritePrivateProfileString(sSection, sKey, sTemp, sINIFile)
End Sub
Public Sub Main()
    
' Initialize variablesfrom INI file
ReadInitFiles

g_AppName = "SubicWater Commercial Functions System Headquarters v" & App.Major & "." & App.Minor & "." & App.Revision

'create database connection
Set g_Conn = CreateObject("ADODB.Connection")
Set g_Conn_backup = CreateObject("ADODB.Connection")
'g_Conn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\mrmshq.mdb"
g_Conn.ConnectionString = "Provider=SQLOLEDB.1; Data Source=" & g_DataSource & ";database=" & g_Database & "; Initial Catalog=;User Id=sa;Password=sqladmin;"
    
'Hardcoded TCP OLE DB initstring
'Provider=SQLOLEDB.1;User Id=sa;Password=sqladmin;Persist Security Info=False;User ID=clientID;Data Source=tcp:TcpIpAddress,port

'[oledb]
'Hardcoded Named Pipes OLE DB initstring
'Provider=SQLOLEDB.1;User Id=sa;Password=sqladmin;Persist Security Info=False;User ID=clientID;Data Source=np:\\ServerName\pipe\MSSQL$InstanceName\sql\query

    
    g_Conn.Open
    g_Conn.CommandTimeout = 1000
    'g_Conn.ConnectionTimeout = 15000

    frmLogin.Show
    
End Sub

Public Sub ReadInitFiles()
    Dim sINIFile As String
    
    sINIFile = App.Path & "\cfshq.ini"
   
    g_Loader_Path = sGetINI(sINIFile, "Settings", "LoaderPath", App.Path)
    g_Current_Filepath = sGetINI(sINIFile, "Settings", "CurrentFilePath", App.Path & "\CURRENT")
    g_BillMonth = sGetINI(sINIFile, "Settings", "CurrentBillPeriod", Format(Date, "YYYYMM"))
    g_Database = sGetINI(sINIFile, "Settings", "Database", "SWCFSDB")
    g_DataSource = sGetINI(sINIFile, "Settings", "DataSource", "SZ-MWSI-SUBIC")
    'Company Name, prompt if not found
    g_Company = sGetINI(sINIFile, "Settings", "CompanyName", "?")
    If g_Company = "?" Then
      g_Company = InputBox$("Enter the Company Name:", "First Run of Program")
      ' Generate the INI file
      writeINI sINIFile, "Settings", "CompanyName", g_Company
      writeINI sINIFile, "Settings", "LoaderPath", g_Loader_Path
      writeINI sINIFile, "Settings", "CurrentFilePath", g_Current_Filepath
    End If
    
End Sub


Function OpenRecordset(ByRef RecSet As ADODB.Recordset, ByVal strSQL As String, str_Form As String, str_Module As String) As Boolean
    'Returns 'FALSE' for No Records
  
    
    OpenRecordset = False
    Set RecSet = Nothing
    On Error GoTo ErrorOpen
    
    'create recordset
    Set RecSet = CreateObject("ADODB.Recordset")
  
    RecSet.Open strSQL, g_Conn, adOpenDynamic, adLockOptimistic
 
    
    If RecSet.EOF Then
        Set RecSet = Nothing
    Else
        OpenRecordset = True
    End If
        
    Exit Function
    
ErrorOpen:
    Show_Error str_Form, str_Module, Err.Number, Err.Description
    Set RecSet = Nothing

End Function

Function DBExecute(ByVal strSQL As String, str_Form As String, str_Module As String) As Boolean
    'Returns 'TRUE' if execution is successful
    
    DBExecute = False
    On Error GoTo ErrorExecute
    g_Conn.Execute strSQL
    DBExecute = True
    Exit Function
    
ErrorExecute:
    Show_Error str_Form, str_Module, Err.Number, Err.Description
    
End Function

Public Sub LoadCombos(ByRef cmb As ComboBox, strSQL As String, bolCode As Boolean, bolSpace As Boolean, bolBlank As Boolean, strFormName As String, strModName As String)
    cmb.Clear
    If Not OpenRecordset(g_rs_TUPBOOK, strSQL, strFormName, strModName) Then Exit Sub
    g_rs_TUPBOOK.MoveFirst
    
    If bolSpace Then cmb.AddItem ""
    While Not g_rs_TUPBOOK.EOF
        If bolCode Then
            cmb.AddItem CheckNull(g_rs_TUPBOOK.Fields(1)) & Space(50) & CheckNull(g_rs_TUPBOOK.Fields(0))
        Else
            cmb.AddItem CheckNull(g_rs_TUPBOOK.Fields(0))
        End If
        g_rs_TUPBOOK.MoveNext
    Wend
    
    If bolBlank Then
        cmb.ListIndex = -1
    Else
        cmb.ListIndex = 0
    End If
    
End Sub

Public Sub MatchCombo(ByRef cmb As ComboBox, ByVal strMatch As String, bolUseText As Boolean)
    Dim l_int_Index As Integer
    Dim l_str_cmbText As String
    
    If cmb.ListCount < 1 Then Exit Sub
    
    For l_int_Index = 0 To cmb.ListCount - 1
        cmb.ListIndex = l_int_Index
        If Len(cmb.Text) > 50 Then
            l_str_cmbText = Trim(Left(cmb.Text, 50))
        Else
            l_str_cmbText = Trim(cmb.Text)
        End If
        If l_str_cmbText = strMatch Then Exit Sub
    Next

    cmb.ListIndex = 0
    If bolUseText Then cmb.Text = strMatch

End Sub

Public Sub Show_Error(str_Form As String, str_Module As String, int_ErrNo As Integer, str_ErrDesc As String)
    Dim str_Message      As String

    str_Message = str_Form & ": " & str_Module & vbCrLf
    str_Message = str_Message & "Error No: " & int_ErrNo & vbCrLf
    str_Message = str_Message & "Description: " & str_ErrDesc
    
    MsgBox str_Message, vbCritical, "Error Found"

    Err.Clear
    
End Sub

Function CheckNull(ByVal str As Variant, Optional bolInteger As Boolean) As String
    If IsNull(str) Or Trim(str) = "" Then
        If bolInteger Then
            CheckNull = "0"
        Else
            CheckNull = ""
        End If
    Else
        CheckNull = Trim(str)
    End If
    
End Function

Function NumericEntry(ByRef txt As TextBox, ByRef lbl As Label) As Boolean
    NumericEntry = True
    If Len(Trim(txt.Text)) < 1 Then Exit Function
        
    If Not IsNumeric(Trim(txt.Text)) Then
        NumericEntry = False
        MsgBox Left(lbl.Caption, Len(lbl.Caption) - 3) & " entry is not numeric.  Please re-enter a numeric value.", vbExclamation, "Invalid Entry"
        txt.SetFocus
    End If

End Function

Function ListViewDelete(ByRef lsv As ListView, intLvColumn1 As Integer, Optional intLvColumn2 As Integer) As Boolean
    Dim l_str_Msg As String
    
    ListViewDelete = False
    
    If intLvColumn1 = intLvColumn2 Then intLvColumn2 = 0
    If intLvColumn1 < 1 Or intLvColumn1 > lsv.ColumnHeaders.Count Then intLvColumn1 = 1
    If intLvColumn2 > lsv.ColumnHeaders.Count Then intLvColumn2 = 0
    
    If intLvColumn1 > 1 Then
        l_str_Msg = "Delete " & lsv.ColumnHeaders(intLvColumn1).Text & " " & lsv.SelectedItem.SubItems(intLvColumn1 - 1)
    Else
        l_str_Msg = "Delete " & lsv.ColumnHeaders(intLvColumn1).Text & " " & lsv.SelectedItem.Text
    End If
    
    If intLvColumn2 > 0 Then
        If intLvColumn2 > 1 Then
            l_str_Msg = l_str_Msg & " - " & lsv.ColumnHeaders(intLvColumn2).Text & " " & lsv.SelectedItem.SubItems(intLvColumn2 - 1)
        Else
            l_str_Msg = l_str_Msg & " - " & lsv.ColumnHeaders(intLvColumn2).Text & " " & lsv.SelectedItem.Text
            End If
    End If
    
    If MsgBox(l_str_Msg & "?", vbYesNo + vbQuestion, "Delete Record") = vbYes Then ListViewDelete = True
    
End Function

Public Sub ListviewSort(ByRef lsv As ListView, ByVal Index As Long)
    lsv.SortKey = Index - 1
    
    If lsv.SortOrder = lvwAscending Then
        lsv.SortOrder = lvwDescending
    Else
        lsv.SortOrder = lvwAscending
    End If

End Sub

Public Sub TextHighlight(ByRef txt As TextBox)
    txt.SelStart = 0
    txt.SelLength = Len(txt.Text)

End Sub

'added for password encryption 07-17-2013 jums
Public Function PWDEncrypt(ByVal PWD As String, ByVal iMode As Integer) As String
' Function to Encrypt/Decrypt a password string (not case sensitive)
'   iMode : 0 - Encrypt
'                1 - Decrypt
   
Dim lc_password As String
Dim lc_table_x As String, retval As String
Dim pwdlen As Integer
Dim i%, j%, k%

    ' Set key table
     lc_table_x = "0OIUY9TRE13ASDFGH7KLMN5VCXZW2Q4B6J8P "
    
    ' Get password length
    lc_password = UCase(PWD)
    pwdlen = Len(Trim(lc_password))
    If pwdlen > 0 Then
        retval = ""
        If iMode = 0 Then ' Encrypt
             For i% = 1 To pwdlen
                For j% = 1 To 37
                    If Mid(lc_password, i%, 1) = Mid(lc_table_x, j%, 1) Then
                        k% = j% + i%
                        If k% > 37 Then
                            k% = k% - 37
                            If k% = 0 Then k% = 37
                        End If
                        retval = retval & Mid(lc_table_x, k%, 1)
                        Exit For
                    End If
                Next
             Next
        Else    ' Decrypt
            For i% = 1 To pwdlen
                For j% = 1 To 37
                    If Mid(lc_password, i%, 1) = Mid(lc_table_x, j%, 1) Then
                        k% = j% - i%
                        If k% < 1 Then k% = 37 + k%
                        retval = retval & Mid(lc_table_x, k%, 1)
                        Exit For
                    End If
                Next
             Next
        End If
    Else
        retval = " "
    End If
    
    PWDEncrypt = retval
    
End Function

