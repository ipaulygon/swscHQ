VERSION 5.00
Begin VB.Form frmConfig 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Statement of Account"
   ClientHeight    =   9300
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4875
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9300
   ScaleWidth      =   4875
   Begin VB.Frame Frame1 
      Caption         =   " SOA Message"
      Height          =   1800
      Left            =   120
      TabIndex        =   39
      Top             =   6900
      Width           =   4620
      Begin VB.TextBox txtSOAMsg 
         Height          =   285
         Index           =   3
         Left            =   1700
         MaxLength       =   25
         TabIndex        =   18
         Top             =   1300
         Width           =   2700
      End
      Begin VB.TextBox txtSOAMsg 
         Height          =   285
         Index           =   2
         Left            =   1700
         MaxLength       =   25
         TabIndex        =   17
         Top             =   960
         Width           =   2700
      End
      Begin VB.TextBox txtSOAMsg 
         Height          =   285
         Index           =   1
         Left            =   1700
         MaxLength       =   25
         TabIndex        =   16
         Top             =   620
         Width           =   2700
      End
      Begin VB.TextBox txtSOAMsg 
         Height          =   285
         Index           =   0
         Left            =   1700
         MaxLength       =   25
         TabIndex        =   15
         Top             =   280
         Width           =   2700
      End
      Begin VB.Label lblSOAMsg 
         Alignment       =   1  'Right Justify
         Caption         =   "Message 4 : "
         Height          =   285
         Index           =   3
         Left            =   120
         TabIndex        =   43
         Top             =   1310
         Width           =   1500
      End
      Begin VB.Label lblSOAMsg 
         Alignment       =   1  'Right Justify
         Caption         =   "Message 3 : "
         Height          =   285
         Index           =   2
         Left            =   120
         TabIndex        =   42
         Top             =   970
         Width           =   1500
      End
      Begin VB.Label lblSOAMsg 
         Alignment       =   1  'Right Justify
         Caption         =   "Message 2 : "
         Height          =   285
         Index           =   1
         Left            =   120
         TabIndex        =   41
         Top             =   630
         Width           =   1500
      End
      Begin VB.Label lblSOAMsg 
         Alignment       =   1  'Right Justify
         Caption         =   "Message 1 : "
         Height          =   285
         Index           =   0
         Left            =   120
         TabIndex        =   40
         Top             =   290
         Width           =   1500
      End
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save"
      Default         =   -1  'True
      Height          =   390
      Left            =   1500
      TabIndex        =   19
      Top             =   8805
      Width           =   915
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   390
      Left            =   2535
      TabIndex        =   20
      Top             =   8805
      Width           =   915
   End
   Begin VB.Frame Frame2 
      Height          =   2180
      Left            =   120
      TabIndex        =   33
      Top             =   60
      Width           =   4620
      Begin VB.TextBox txtHiPercent 
         Height          =   285
         Left            =   1700
         MaxLength       =   7
         TabIndex        =   2
         Top             =   950
         Width           =   2700
      End
      Begin VB.TextBox txtUtilName 
         Height          =   285
         Left            =   1700
         MaxLength       =   50
         TabIndex        =   0
         Top             =   240
         Width           =   2700
      End
      Begin VB.ComboBox cmbMosAve 
         Height          =   315
         Left            =   1700
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1650
         Width           =   2700
      End
      Begin VB.TextBox txtLoPercent 
         Height          =   285
         Left            =   1700
         MaxLength       =   7
         TabIndex        =   3
         Top             =   1290
         Width           =   2700
      End
      Begin VB.ComboBox cmbLtoR 
         Height          =   315
         Left            =   1700
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   580
         Width           =   2700
      End
      Begin VB.Label lblUtilName 
         Alignment       =   1  'Right Justify
         Caption         =   "Utility Name : "
         Height          =   195
         Left            =   120
         TabIndex        =   38
         Top             =   250
         Width           =   1500
      End
      Begin VB.Label lblLtoR 
         Alignment       =   1  'Right Justify
         Caption         =   "Left_Right : "
         Height          =   195
         Left            =   120
         TabIndex        =   37
         Top             =   590
         Width           =   1500
      End
      Begin VB.Label lblHiPercent 
         Alignment       =   1  'Right Justify
         Caption         =   "High Percent : "
         Height          =   195
         Left            =   120
         TabIndex        =   36
         Top             =   960
         Width           =   1500
      End
      Begin VB.Label lblMosAve 
         Alignment       =   1  'Right Justify
         Caption         =   "Months to Average : "
         Height          =   195
         Left            =   120
         TabIndex        =   35
         Top             =   1660
         Width           =   1500
      End
      Begin VB.Label lblLoPercent 
         Alignment       =   1  'Right Justify
         Caption         =   "Low Percent : "
         Height          =   195
         Left            =   120
         TabIndex        =   34
         Top             =   1300
         Width           =   1500
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   " Other Rate Description "
      Height          =   1800
      Left            =   120
      TabIndex        =   28
      Top             =   2360
      Width           =   4620
      Begin VB.TextBox txtORDesc 
         Height          =   285
         Index           =   3
         Left            =   1700
         MaxLength       =   25
         TabIndex        =   8
         Top             =   1300
         Width           =   2700
      End
      Begin VB.TextBox txtORDesc 
         Height          =   285
         Index           =   2
         Left            =   1700
         MaxLength       =   25
         TabIndex        =   7
         Top             =   960
         Width           =   2700
      End
      Begin VB.TextBox txtORDesc 
         Height          =   285
         Index           =   1
         Left            =   1700
         MaxLength       =   25
         TabIndex        =   6
         Top             =   620
         Width           =   2700
      End
      Begin VB.TextBox txtORDesc 
         Height          =   285
         Index           =   0
         Left            =   1700
         MaxLength       =   25
         TabIndex        =   5
         Top             =   280
         Width           =   2700
      End
      Begin VB.Label lblORDesc 
         Alignment       =   1  'Right Justify
         Caption         =   "Description 4 : "
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   32
         Top             =   1310
         Width           =   1500
      End
      Begin VB.Label lblORDesc 
         Alignment       =   1  'Right Justify
         Caption         =   "Description 3 : "
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   31
         Top             =   970
         Width           =   1500
      End
      Begin VB.Label lblORDesc 
         Alignment       =   1  'Right Justify
         Caption         =   "Description 2 : "
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   30
         Top             =   630
         Width           =   1500
      End
      Begin VB.Label lblORDesc 
         Alignment       =   1  'Right Justify
         Caption         =   "Description 1 : "
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   29
         Top             =   300
         Width           =   1500
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   " SOA Heading "
      Height          =   2480
      Left            =   120
      TabIndex        =   21
      Top             =   4280
      Width           =   4620
      Begin VB.TextBox txtSOAHead 
         Height          =   285
         Index           =   5
         Left            =   1700
         MaxLength       =   25
         TabIndex        =   14
         Top             =   1980
         Width           =   2700
      End
      Begin VB.TextBox txtSOAHead 
         Height          =   285
         Index           =   4
         Left            =   1700
         MaxLength       =   25
         TabIndex        =   13
         Top             =   1640
         Width           =   2700
      End
      Begin VB.TextBox txtSOAHead 
         Height          =   285
         Index           =   3
         Left            =   1700
         MaxLength       =   25
         TabIndex        =   12
         Top             =   1300
         Width           =   2700
      End
      Begin VB.TextBox txtSOAHead 
         Height          =   285
         Index           =   2
         Left            =   1700
         MaxLength       =   25
         TabIndex        =   11
         Top             =   960
         Width           =   2700
      End
      Begin VB.TextBox txtSOAHead 
         Height          =   285
         Index           =   1
         Left            =   1700
         MaxLength       =   25
         TabIndex        =   10
         Top             =   620
         Width           =   2700
      End
      Begin VB.TextBox txtSOAHead 
         Height          =   285
         Index           =   0
         Left            =   1700
         MaxLength       =   25
         TabIndex        =   9
         Top             =   280
         Width           =   2700
      End
      Begin VB.Label lblSOAHead 
         Alignment       =   1  'Right Justify
         Caption         =   "Heading 6 : "
         Height          =   195
         Index           =   5
         Left            =   120
         TabIndex        =   27
         Top             =   1990
         Width           =   1500
      End
      Begin VB.Label lblSOAHead 
         Alignment       =   1  'Right Justify
         Caption         =   "Heading 5 : "
         Height          =   195
         Index           =   4
         Left            =   120
         TabIndex        =   26
         Top             =   1650
         Width           =   1500
      End
      Begin VB.Label lblSOAHead 
         Alignment       =   1  'Right Justify
         Caption         =   "Heading 4 : "
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   25
         Top             =   1310
         Width           =   1500
      End
      Begin VB.Label lblSOAHead 
         Alignment       =   1  'Right Justify
         Caption         =   "Heading 3 : "
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   24
         Top             =   970
         Width           =   1500
      End
      Begin VB.Label lblSOAHead 
         Alignment       =   1  'Right Justify
         Caption         =   "Heading 2 : "
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   23
         Top             =   630
         Width           =   1500
      End
      Begin VB.Label lblSOAHead 
         Alignment       =   1  'Right Justify
         Caption         =   "Heading 1 : "
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   22
         Top             =   290
         Width           =   1500
      End
   End
End
Attribute VB_Name = "frmConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim m_bol_SParam As Boolean

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdSave_Click()
    Dim l_str_Sql As String
    Dim l_int_Index As Integer
    
    If Not NumericEntry(txtHiPercent, lblHiPercent) Then Exit Sub
    If Not NumericEntry(txtLoPercent, lblLoPercent) Then Exit Sub
    
    If m_bol_SParam = False Then
        l_str_Sql = "INSERT INTO S_PARAM(COMPANY_NAME, LTOR, HIPERCENT, LOPERCENT, MOSAVE, " & _
            "ORDESC1, ORDESC2, ORDESC3, ORDESC4, " & _
            "SOAHEAD1, SOAHEAD2, SOAHEAD3, SOAHEAD4, SOAHEAD5, SOAHEAD6, " & _
            "SOAMSG1, SOAMSG2, SOAMSG3, SOAMSG4) VALUES('" & _
            Trim(txtUtilName) & "', " & GetcmbLtoR & ", " & CheckNull(txtHiPercent, True) & ", " & _
            CheckNull(txtLoPercent, True) & ", " & CheckNull(cmbMosAve.Text, True) & ", '"
        
        For l_int_Index = 0 To 3
            l_str_Sql = l_str_Sql & Trim(txtORDesc(l_int_Index)) & "', '"
        Next
        
        For l_int_Index = 0 To 5
            l_str_Sql = l_str_Sql & Trim(txtSOAHead(l_int_Index)) & "', '"
        Next
        
        For l_int_Index = 0 To 3
            l_str_Sql = l_str_Sql & Trim(txtSOAMsg(l_int_Index)) & "', '"
        Next
            
        l_str_Sql = Trim(Left(l_str_Sql, Len(l_str_Sql) - 3)) & ")"
    Else
        l_str_Sql = "UPDATE S_PARAM SET COMPANY_NAME = '" & txtUtilName & _
            "', LTOR = " & GetcmbLtoR & _
            ", HIPERCENT = " & CheckNull(txtHiPercent, True) & _
            ", LOPERCENT = " & CheckNull(txtLoPercent, True) & _
            ", MOSAVE = " & CheckNull(cmbMosAve.Text, True)

        For l_int_Index = 0 To 3
            l_str_Sql = l_str_Sql & ", ORDESC" & l_int_Index + 1 & " = '" & Trim(txtORDesc(l_int_Index)) & "'"
        Next
        
        For l_int_Index = 0 To 5
            l_str_Sql = l_str_Sql & ", SOAHEAD" & l_int_Index + 1 & " = '" & Trim(txtSOAHead(l_int_Index)) & "'"
        Next
        
        For l_int_Index = 0 To 3
            l_str_Sql = l_str_Sql & ", SOAMSG" & l_int_Index + 1 & " = '" & Trim(txtSOAMsg(l_int_Index)) & "'"
        Next
    End If
    
    If DBExecute(l_str_Sql, Me.Name, "cmdSave_Click") Then Unload Me

End Sub

Function GetcmbLtoR() As Integer
    If cmbLtoR.ListIndex = 0 Then
        GetcmbLtoR = True
    Else
        GetcmbLtoR = False
    End If
    
End Function

Private Sub Form_Load()
    Dim l_int_Index As Integer

If WindowState = vbNormal Then
    Me.Move (0)
    Me.Top = 0
End If
    Combos_Load
    m_bol_SParam = OpenRecordset(g_rs_SPARAM, "SELECT * FROM S_PARAM", Me.Name, "Form_Load")
    If m_bol_SParam = False Then Exit Sub
    g_rs_SPARAM.MoveFirst
    
    txtUtilName = CheckNull(g_rs_SPARAM.Fields("COMPANY_NAME"))
    txtHiPercent = CheckNull(g_rs_SPARAM.Fields("HIPERCENT"))
    txtLoPercent = CheckNull(g_rs_SPARAM.Fields("LOPERCENT"))
    
    For l_int_Index = 0 To 3
        txtORDesc(l_int_Index) = CheckNull(g_rs_SPARAM.Fields("ORDESC" & l_int_Index + 1))
    Next
    
    For l_int_Index = 0 To 5
        txtSOAHead(l_int_Index) = CheckNull(g_rs_SPARAM.Fields("SOAHEAD" & l_int_Index + 1))
    Next
    
    For l_int_Index = 0 To 3
        txtSOAMsg(l_int_Index) = CheckNull(g_rs_SPARAM.Fields("SOAMSG" & l_int_Index + 1))
    Next

    MatchCombos
    g_rs_SPARAM.Close
    Set g_rs_SPARAM = Nothing

End Sub

Private Sub Combos_Load()
    Dim l_int_Ctr As Integer
    
    cmbMosAve.Clear
    For l_int_Ctr = 1 To 12
        cmbMosAve.AddItem l_int_Ctr
    Next
    cmbMosAve.ListIndex = 0
    
    cmbLtoR.Clear
    cmbLtoR.AddItem "Left to Right"
    cmbLtoR.AddItem "Right to Left"
    cmbLtoR.ListIndex = 0
    
End Sub

Private Sub MatchCombos()
    Dim l_int_Ctr As Integer
    
    For l_int_Ctr = 0 To 11
        cmbMosAve.ListIndex = l_int_Ctr
        If cmbMosAve.Text = CheckNull(g_rs_SPARAM.Fields("MOSAVE")) Then GoTo LtoR
    Next
    cmbMosAve.ListIndex = 0
    
LtoR:
    If CheckNull(g_rs_SPARAM.Fields("LTOR")) = True Then
        cmbLtoR.ListIndex = 0
    Else
        cmbLtoR.ListIndex = 1
    End If
    
End Sub

Private Sub txtHiPercent_GotFocus()
    TextHighlight txtHiPercent
End Sub

Private Sub txtLoPercent_GotFocus()
    TextHighlight txtLoPercent
End Sub

Private Sub txtORDesc_GotFocus(Index As Integer)
    TextHighlight txtORDesc(Index)
End Sub

Private Sub txtSOAHead_GotFocus(Index As Integer)
    TextHighlight txtSOAHead(Index)
End Sub

Private Sub txtSOAMsg_GotFocus(Index As Integer)
    TextHighlight txtSOAMsg(Index)
End Sub

Private Sub txtCOMPANY_NAME_GotFocus()
    TextHighlight txtUtilName
End Sub


