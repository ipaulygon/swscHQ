Attribute VB_Name = "udfstring"
    Const MAXARRSIZE = 16777216  ' 2**24

    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'When performance is an issue: use Split Function rather than this function.
    'Getallwords2() User-Defined Function Inserts the words from a string into the array.
    'Getallwords2(cString[, cStringSplitting])
    'Parameters
    'cString  String - Specifies the string whose words will be inserted into the array.
    'cStringSplitting String - Optional. Specifies the string used to separate words in cString.
    'The default delimiter is space.
    'Note that Getallwords2() uses  cStringSplitting as a single delimiter.
    'Returns a zero-based, two-dimensional array.
    'The array created will have the following structure:
    'Column 0 � Sequential number specifying word order in the string � type Long (Variant)
    'Column 1 � The complete word found � type String (Variant)
    'Column 2 � The character position in lcString where the word starts � type Long (Variant)
    'Column 3 � The length of the word � type Long (Variant)
    'Remarks Getallwords2() by default assumes that words are delimited by space. If you specify another string as delimiter, this function ignores spaces and uses only the specified string.
    'Example
    ' Dim cString As String, nIndex As Integer, ArrWords As Variant
    ' cString = "We hold these truths to be self-evident, that all men are created equal, that they are endowed by their Creator with certain unalienable Rights, that among these are Life, Liberty and the pursuit of Happiness."
    ' nIndex = 29
    ' ArrWords = Getallwords2(cString, " ")
    ' Print ArrWords(nIndex, 1)               ' Displays "Liberty"
    ' Print ArrWords(UBound(ArrWords, 1), 0)  ' Displays 35
    'See Also Getwordnum(), Getnumword(), Getwordcount(),  Getallwords()  User-Defined Functions
     Public Function Getallwords2(ByVal cString As String, Optional ByVal cStringSplitting As String = " ") As Variant
        Dim k As Integer, i As Long, wordcount As Integer, BegOfWord As Integer, flag As Boolean
        BegOfWord = 1
        Dim arrWords3(MAXARRSIZE, 1) As Long
        Dim Allwords() As Variant

        If IsNull(cString) Then
            cString = ""
        End If
        If IsNull(cStringSplitting) Or Len(cStringSplitting) = 0 Then
            cStringSplitting = " " ' if no break string is specified, the function uses space to delimit words.
        End If
        Do While True
            If k - BegOfWord > 0 Then
                arrWords3(wordcount, 0) = BegOfWord      ' Startofword
                arrWords3(wordcount, 1) = k - BegOfWord  ' Lengthofword
                wordcount = wordcount + 1
                BegOfWord = k
            End If
            If flag Then
                Exit Do
            End If
            If Len(cStringSplitting) > 0 Then
                While Len(cString) >= BegOfWord - 1 + Len(cStringSplitting) And InStr(1, cStringSplitting, Mid$(cString, BegOfWord, Len(cStringSplitting)), vbBinaryCompare) > 0 ' skip break strings, if any
                    BegOfWord = BegOfWord + Len(cStringSplitting)
                Wend
                k = InStr(BegOfWord, cString, cStringSplitting, vbBinaryCompare)
            End If
            If k = 0 Then
                k = Len(cString) + 1
                flag = True
            End If
        Loop
        
        If wordcount = 0 Then
            ReDim Allwords(0)
            Allwords(0) = Null
        Else
            ReDim Allwords(wordcount - 1, 3)
            For i = 0 To wordcount - 1
                Allwords(i, 0) = i + 1  ' Wordnum
                Allwords(i, 1) = Mid$(cString, arrWords3(i, 0), arrWords3(i, 1)) ' Word
                Allwords(i, 2) = arrWords3(i, 0)  ' Startofword
                Allwords(i, 3) = arrWords3(i, 1)  ' Lengthofword
            Next i
        End If
        Getallwords2 = Allwords
    End Function
    
    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'Getallwords() User-Defined Function Inserts the words from a string into the array.
    'Getallwords(cString[, cDelimiters])
    'Parameters
    'cString  String - Specifies the string whose words will be inserted into the array.
    'cDelimiters String - Optional. Specifies one or more optional characters used to separate words in cString.
    'The default delimiters are space, tab, carriage return, and line feed. Note that Getallwords( ) uses each of the characters in cDelimiters as individual delimiters, not the entire string as a single delimiter.
    'Returns a zero-based, two-dimensional array.
    'The array created will have the following structure:
    'Column 0 � Sequential number specifying word order in the string � type Long (Variant)
    'Column 1 � The complete word found � type String (Variant)
    'Column 2 � The character position in lcString where the word starts � type Long (Variant)
    'Column 3 � The length of the word � type Long (Variant)
    'Remarks Getallwords() by default assumes that words are delimited by spaces or tabs. If you specify another character as delimiter, this function ignores spaces and tabs and uses only the specified character.
    'Example
    ' Dim cString As String, i As Integer, ArrWords As Variant
    ' cString = "The default delimiters are space, tab, carriage return, and line feed. If you specify another character as delimiter, this function ignores spaces and tabs and uses only the specified character."
    ' ArrWords = Getallwords(cString)
    ' For i = 0 To UBound(ArrWords, 1)
    '    Print ArrWords(i, 0), ArrWords(i, 1), ArrWords(i, 2), ArrWords(i, 3)
    ' Next i
    ' ArrWords = Getallwords(cString, " ,.")
    ' For i = 0 To UBound(ArrWords, 1)
    '    Print ArrWords(i, 0), ArrWords(i, 1), ArrWords(i, 2), ArrWords(i, 3)
    ' Next i
    'See Also Getwordnum(), Getnumword(), Getwordcount(),  Getallwords2() User-Defined Functions
     Public Function Getallwords(ByVal cString As String, Optional ByVal cDelimiters As String = " " & vbTab & vbCrLf) As Variant
        Dim k As Long, i As Long, wordcount As Long, BegOfWord As Long, flag As Boolean
        k = 1
        BegOfWord = 1
        Dim arrWords3(MAXARRSIZE, 1) As Long
        Dim Allwords() As Variant
        If IsNull(cString) Then
            cString = ""
        End If
        If IsNull(cDelimiters) Then
            cDelimiters = " " & vbTab & vbCrLf ' if no break string is specified, the function uses spaces, tabs, carriage return and line feed to delimit words.
        End If
        Do While True
            If k - BegOfWord > 0 Then
                arrWords3(wordcount, 0) = BegOfWord      ' Startofword
                arrWords3(wordcount, 1) = k - BegOfWord  ' Lengthofword
                wordcount = wordcount + 1
                BegOfWord = k
            End If
            If flag Then
                Exit Do
            End If
            While Len(cString) >= k And InStr(1, cDelimiters, Mid$(cString, k, 1), vbBinaryCompare) > 0  ' skip  break characters, if any
                k = k + 1
                BegOfWord = BegOfWord + 1
            Wend
            While Len(cString) >= k And InStr(1, cDelimiters, Mid$(cString, k, 1), vbBinaryCompare) = 0  ' skip  the character in the word
                k = k + 1
            Wend
            If k > Len(cString) Then
                flag = True
            End If
        Loop
        
        If wordcount = 0 Then
            ReDim Allwords(0)
            Allwords(0) = Null
        Else
            ReDim Allwords(wordcount - 1, 3)
            For i = 0 To wordcount - 1
                Allwords(i, 0) = i + 1 ' Wordnum
                Allwords(i, 1) = Mid$(cString, arrWords3(i, 0), arrWords3(i, 1)) ' Word
                Allwords(i, 2) = arrWords3(i, 0)  ' Startofword
                Allwords(i, 3) = arrWords3(i, 1)  ' Lengthofword
            Next i
        End If
        Getallwords = Allwords
    End Function
    
    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'Getwordcount() User-Defined Function Counts the words in a string.
    'Getwordcount(cString[, cDelimiters])
    'Parameters
    'cString  String - Specifies the string whose words will be counted.
    'cDelimiters String - Optional. Specifies one or more optional characters used to separate words in cString.
    'The default delimiters are space, tab, carriage return, and line feed. Note that Getwordcount( ) uses each of the characters in cDelimiters as individual delimiters, not the entire string as a single delimiter.
    'Return Value Long
    'Remarks Getwordcount() by default assumes that words are delimited by spaces or tabs. If you specify another character as delimiter, this function ignores spaces and tabs and uses only the specified character.
    'If you use "AAA aaa, BBB bbb, CCC ccc." as the target string for Getwordcount(), you can get all the following results.
    ' Dim cString As  String
    ' cString = "AAA aaa, BBB bbb, CCC ccc."
    ' Print Getwordcount(cString)                   '  6 - character groups, delimited by ' '
    ' Print Getwordcount(cString, ",")              '  3 - character groups, delimited by ","
    ' Print Getwordcount(cString, ".")              '  1 - character group, delimited by '.'
    'See Also Getwordnum(), Getallwords() User-Defined Functions
    'UDF the name and functionality of which correspond  to the  Visual FoxPro function
    Public Function Getwordcount(ByVal cString As String, Optional ByVal cDelimiters As String = " " & vbTab & vbCrLf) As Long
        Dim k As Long, wordcount As Long
        If IsNull(cDelimiters) Then
            cDelimiters = " " & vbTab & vbCrLf ' if no break string is specified, the function uses spaces, tabs, carriage return and line feed to delimit words.
        End If
        Do
            k = k + 1
        Loop While Len(cString) >= k And InStr(1, cDelimiters, Mid$(cString, k, 1), vbBinaryCompare) > 0  ' skip opening break characters, if any
        If k <= Len(cString) Then
            wordcount = 1 '  count the one we are in now count transitions from "not in word" to "in word"
            While k <= Len(cString)  '  if the current character is a break char, but the next one is not, we have entered a new word
                If k + 1 <= Len(cString) And InStr(1, cDelimiters, Mid$(cString, k, 1), vbBinaryCompare) > 0 And InStr(1, cDelimiters, Mid$(cString, k + 1, 1), vbBinaryCompare) = 0 Then
                    wordcount = wordcount + 1
                    k = k + 1 ' Skip over the first character in the word. We know it cannot be a break character.
                End If
                k = k + 1
            Wend
        End If
        Getwordcount = wordcount
    End Function

    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'Getwordnum() User-Defined Function
    'Returns a specified word from a string.
    'Getwordnum(cString, nIndex[, cDelimiters])
    'Parameters cString  String - Specifies the string to be evaluated
    'nIndex Long - Specifies the index position of the word to be returned. For example, if nIndex is 3, Getwordnum( ) returns the third word (if cString contains three or more words).
    'cDelimiters String - Optional. Specifies one or more optional characters used to separate words in cString.
    'The default delimiters are space, tab, carriage return, and line feed. Note that GetWordNum( ) uses each of the characters in cDelimiters as individual delimiters, not the entire string as a single delimiter.
    'Return Value String
    'Remarks Returns the word at the position specified by nIndex in the target string, cString. If cString contains fewer than nIndex words, Getwordnum( ) returns an empty string.
    'Example
    ' Print Getwordnum("To be, or not to be: that is the question:", 10, " ,.:")  ' Displays "question"
    'See Also Getnumword(), Getwordcount(), Getallwords() User-Defined Functions
    'UDF the name and functionality of which correspond  to the Visual FoxPro function
    Public Function Getwordnum(ByVal cString As String, ByVal nIndex As Long, Optional ByVal cDelimiters As String = " " & vbTab & vbCrLf) As String
        Dim k As Long, l As Long, lmin As Long, i As Long, j As Long, nEndString As Long, cWord As String
        If IsNull(cDelimiters) Then
            cDelimiters = " " & vbTab & vbCrLf ' if no break string is specified, the function uses spaces, tabs, carriage return and line feed to delimit words.
        End If
        k = 1
        nEndString = Len(cString) + 1
        For i = 1 To nIndex
            While nEndString > k And InStr(1, cDelimiters, Mid$(cString, k, 1), vbBinaryCompare) > 0  ' skip opening break characters, if any
                k = k + 1
            Wend
            If k >= nEndString Then
                Exit For
            End If
            lmin = nEndString  '  find next break character it marks the end of this word
            For j = 1 To Len(cDelimiters)
                l = InStr(k, cString, Mid$(cDelimiters, j, 1), vbBinaryCompare)
                If l > 0 And lmin > l Then
                    lmin = l
                End If
            Next
            If i = nIndex Then '  this is the actual word we are looking for
                cWord = Mid$(cString, k, lmin - k)
                Exit For
            End If
            k = lmin + 1
            If k >= nEndString Then
                Exit For
            End If
        Next i
        Getwordnum = cWord
    End Function
    
'Getnumword(cString, cWord[, cDelimiters] [, nOccurrence] [, nFlags])
'Return Value  Long
'Returns the index position of the word specified by cWord in the target string, cString.
'If cString don't contain the word cWord, Getnumword( ) returns 0.
'cString String Specifies the string to be evaluated
'cWord String  Specifies the word to search for in cString.
'cDelimiters String  Optional. Specifies one or more optional characters used to separate words in cString.
'The default delimiters are space, tab, carriage return, and line feed.
'Note that Getnumword( ) uses each of the characters in cDelimiters as individual delimiters, not the entire string as a single delimiter.
'nOccurrence Long Specifies which occurrence, first, second, third, and so on, of cWord to search for in cString.
'By default, Getnumword( ) searches for the first occurrence of cWord (nOccurrence = 1).
'nFlags Boolean Specifies Case-sensitive search criteria to apply to this function. Valid values  are False (the Default) And True.
'False Case-sensitive search
'True Case insensitive search
'Examples
' Print Getnumword("O Canada! Our home and native land!", "Canada!")  '  Displays 2
' Dim lcAdmissibleCodes As String, lcCodeforChecking  As String
' lcAdmissibleCodes = "W,WN,IR,IU"
' lcCodeforChecking = "I"
' Print InStr(1, lcAdmissibleCodes, lcCodeforChecking, vbBinaryCompare)     ' Displays 6
' Print Getnumword(lcAdmissibleCodes, lcCodeforChecking, "," + Space(1)) ' Displays  0
' lcCodeforChecking = "IR,"
' Print InStr(1, lcAdmissibleCodes, lcCodeforChecking, vbBinaryCompare)     ' Displays 6
' Print Getnumword(lcAdmissibleCodes, lcCodeforChecking, "," + Space(1)) ' Displays  0
' lcAdmissibleCodes = "W,WN,IR,IU,WN,DS"
' lcCodeforChecking = "WN"
' Print Getnumword(lcAdmissibleCodes, lcCodeforChecking, "," + Space(1), 2) ' Displays  5
'See Also Getwordnum(), Getwordcount(), Getallwords() User-Defined Functions
Public Function Getnumword(ByVal cString As String, ByVal cWord As String, Optional ByVal cDelimiters As String = " " & vbTab & vbCrLf, Optional ByVal nOccurrence As Long = 1, Optional ByVal nFlags As Boolean = False) As Long
    Dim EndOfWord As Long, BegOfWord  As Long, wordcount As Long, k As Long, lnOccurrence As Long, lenofword As Long, flagexit As Boolean
    k = 1
    lenofword = Len(cWord)
    If IsNull(cDelimiters) Then
        cDelimiters = " " & vbTab & vbCrLf ' if no break string is specified, the function uses spaces, tabs, carriage return and line feed to delimit words.
    End If
    '  nOccurrence Specifies which occurrence, first, second, third, and so on, of cWord to search for in cString.
    '  By default, Getnumword( ) searches for the first occurrence of cWord (nOccurrence = 1).
    If nOccurrence < 1 Then
        nOccurrence = 1
    End If
    
    Do While True
        If EndOfWord > BegOfWord Then '  BegOfWord begin of previous word
            If (lenofword = (EndOfWord - BegOfWord)) Then '  length of previous word
                '   nFlags  Specifies Case-sensitive search criteria To Apply To This Function. Valid Values are False (the Default) And True.
                '   False Case-sensitive search
                '   True  Case insensitive search
                If InStr(1, Mid$(cString, BegOfWord, lenofword), cWord, IIf(nFlags, vbTextCompare, vbBinaryCompare)) > 0 Then
                    lnOccurrence = lnOccurrence + 1
                    If lnOccurrence = nOccurrence Then
                        flagexit = True
                    End If
                End If
            End If
            wordcount = wordcount + 1
        End If
        If flagexit Then
            Exit Do
        End If
        
      Do While InStr(1, cDelimiters, Mid$(cString, k, 1), vbBinaryCompare) > 0 And Len(cString) >= k    ' skip  break characters, if any
            k = k + 1
      Loop
      BegOfWord = k
      
      If Len(cString) >= k Then
            k = k + 1  '  Skip over the first character in the word. We know it can not be a break character.
      End If
      Do While InStr(1, cDelimiters, Mid$(cString, k, 1), vbBinaryCompare) = 0 And Len(cString) >= k    ' skip  the character in the word
            k = k + 1
      Loop
      EndOfWord = k
      If k > Len(cString) Then
            flagexit = True
      Else
            k = k + 1 '  Skip over the first break character. We know it can not be a word character.
      End If
    Loop
    Getnumword = IIf(lnOccurrence = nOccurrence, wordcount, 0)
End Function
    
    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'Returns the next highest integer that is greater than or equal to the specified numeric expression.
    Public Function Ceiling(ByVal nExpression As Double) As Long
        Ceiling = Int(nExpression) + IIf(CDbl(Int(nExpression)) <> nExpression, 1, 0)
    End Function
    
    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'Returns a character string that contains a specified character expression repeated a specified number of times.
    Public Function Replicate(ByVal cExpression As String, ByVal nTimes As Long) As String
        Dim i As Long, cResExpression As String
            For i = 1 To nTimes
              cResExpression = cResExpression & cExpression
            Next i
        Replicate = cResExpression
    End Function
    
    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'At() User-Defined Function
    'Returns the beginning numeric position of the first occurrence of a character expression within another character expression,
    'counting from the leftmost character.
    '(including  overlaps)
    'At(cSearchExpression, cExpressionSearched [, nOccurrence]) Return Values Long
    'Parameters
    'cSearchExpression String Specifies the character expression that At( ) searches for in cExpressionSearched.
    'cExpressionSearched String Specifies the character expression cSearchExpression searches for.
    'nOccurrence Long Specifies which occurrence (first, second, third, and so on) of cSearchExpression is searched for in cExpressionSearched.
    'By default, At() searches for the first occurrence of cSearchExpression (nOccurrence = 1).
    'Including nOccurrence lets you search for additional occurrences of cSearchExpression in cExpressionSearched.
    'At( ) returns 0 if nOccurrence is greater than the number of times cSearchExpression occurs in cExpressionSearched.
    'Remarks
    'At() searches the second character expression for the first occurrence of the first character expression.
    'It then returns an integer indicating the position of the first character in the character expression found.
    'If the character expression is not found, At() returns 0. The search performed by At() is case-sensitive.
    'Example
    ' Dim gcString As  String, gcFindString As  String
    ' gcString = "Now is the time for all good men"
    ' gcFindString = "is the"
    ' Print At(gcFindString, gcString)   ' Displays 5
    ' gcFindString = "IS"
    ' Print At(gcFindString, gcString)   ' Displays 0, case-sensitive
    ' gcString = "goood men"
    ' gcFindString = "oo"
    ' Print At(gcFindString, gcString, 1)   ' Displays 2
    ' Print At(gcFindString, gcString, 2)   ' Displays 3, including  overlaps
    'See Also Rat(), Atc(), At2()  User-Defined Function
    'UDF the name and functionality of which correspond  to the  Visual FoxPro function
    Public Function At(ByVal cSearchExpression As String, ByVal cExpressionSearched As String, Optional ByVal nOccurrence As Long = 1) As Long
        Dim i As Long, StartingPosition As Long
        If nOccurrence > 0 Then
            If Len(cExpressionSearched) > 0 And Len(cSearchExpression) > 0 Then
                Do
                    i = i + 1
                    StartingPosition = InStr(StartingPosition + 1, cExpressionSearched, cSearchExpression, vbBinaryCompare)
                Loop While StartingPosition <> 0 And nOccurrence > i
            End If
        End If
        At = StartingPosition
    End Function
    
    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'Rat( ) User-Defined Function
    'Returns the numeric position of the last (rightmost) occurrence of a character string within another character string.
    '(including  overlaps)
    'Rat(cSearchExpression, cExpressionSearched [, nOccurrence])
    'Return Values Long
    'Parameters
    'cSearchExpression String Specifies the character expression that Rat( ) looks for in cExpressionSearched.
    'cExpressionSearched String Specifies the character expression that Rat() searches.
    'nOccurrence Long Specifies which occurrence, starting from the right and moving left,
    'of cSearchExpression Rat() searches for in cExpressionSearched. By default,
    'Rat() searches for the last occurrence of cSearchExpression (nOccurrence = 1).
    'If nOccurrence is 2, Rat() searches for the next to last occurrence, and so on.
    'Remarks
    'Rat(), the reverse of the At() function, searches the character expression in cExpressionSearched
    'starting from the right and moving left, looking for the last occurrence of the string specified in cSearchExpression.
    'Rat() returns an integer indicating the position of the first character in cSearchExpression in cExpressionSearched.
    'Rat() returns 0 if cSearchExpression is not found in cExpressionSearched, or if nOccurrence is greater
    'than the number of times cSearchExpression occurs in cExpressionSearched.
    'The search performed by Rat() is case-sensitive.
    'Example
    ' Dim gcString As  String, gcFindString As  String
    ' gcString = "abracadabra"
    ' gcFindString = "a"
    ' Print Rat(gcFindString , gcString)       ' Displays 11
    ' Print Rat(gcFindString , gcString , 3)   ' Displays 6
    ' gcString = "goood men"
    ' gcFindString = "oo"
    ' Print Rat(gcFindString, gcString, 1)   ' Displays 3
    ' Print Rat(gcFindString, gcString, 2)   ' Displays 2, including  overlaps
    'See Also At()  User-Defined Function
    'UDF the name and functionality of which correspond  to the  Visual FoxPro function
    Public Function Rat(ByVal cSearchExpression As String, ByVal cExpressionSearched As String, Optional ByVal nOccurrence As Long = 1) As Long
        Dim i As Long, StartingPosition As Long, nShift As Long
        If nOccurrence > 0 Then
            If Len(cExpressionSearched) > 0 And Len(cSearchExpression) > 0 Then
                nShift = Len(cSearchExpression) - 2
                StartingPosition = Len(cExpressionSearched) - nShift
                Do
                    i = i + 1
                    If StartingPosition + nShift <= 0 Then
                       StartingPosition = 0
                       Exit Do
                    End If
                    StartingPosition = InStrRev(cExpressionSearched, cSearchExpression, StartingPosition + nShift, vbBinaryCompare)
               Loop While StartingPosition > 0 And nOccurrence > i
            End If
        End If
        Rat = StartingPosition
    End Function

    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'At2() User-Defined Function
    'Returns the beginning numeric position of the first occurrence of a character expression within another character expression, counting from the leftmost character.
    '(excluding  overlaps)
    'At2(cSearchExpression, cExpressionSearched [, nOccurrence]) Return Values Long
    'Parameters
    'cSearchExpression String Specifies the character expression that At2( ) searches for in cExpressionSearched.
    'cExpressionSearched String Specifies the character expression cSearchExpression searches for.
    'nOccurrence Long Specifies which occurrence (first, second, third, and so on) of cSearchExpression is searched for in cExpressionSearched.
    'By default, At2() searches for the first occurrence of cSearchExpression (nOccurrence = 1).
    'Including nOccurrence lets you search for additional occurrences of cSearchExpression in cExpressionSearched.
    'At2( ) returns 0 if nOccurrence is greater than the number of times cSearchExpression occurs in cExpressionSearched.
    'Remarks
    'At2() searches the second character expression for the first occurrence of the first character expression.
    'It then returns an integer indicating the position of the first character in the character expression found.
    'If the character expression is not found, At2() returns 0. The search performed by At2() is case-sensitive.
    'Example
    ' Dim gcString As  String, gcFindString As  String
    ' gcString = "Now is the time for all good men"
    ' gcFindString = "is the"
    ' Print At2(gcFindString, gcString)   ' Displays 5
    ' gcFindString = "IS"
    ' Print At2(gcFindString, gcString)   ' Displays 0, case-sensitive
    ' gcString = "goood men"
    ' gcFindString = "oo"
    ' Print At2(gcFindString, gcString, 1)   ' Displays 2
    ' Print At2(gcFindString, gcString, 2)   ' Displays 0, excluding  overlaps
    'See Also At(), Atc(), Atc2()  User-Defined Function
    Public Function At2(ByVal cSearchExpression As String, ByVal cExpressionSearched As String, Optional ByVal nOccurrence As Long = 1) As Long
        Dim i As Long, StartingPosition As Long
        If nOccurrence > 0 Then
            If Len(cExpressionSearched) > 0 And Len(cSearchExpression) > 0 Then
                StartingPosition = 1 - Len(cSearchExpression)
                Do
                    i = i + 1
                    StartingPosition = InStr(StartingPosition + Len(cSearchExpression), cExpressionSearched, cSearchExpression, vbBinaryCompare)
                Loop While StartingPosition <> 0 And nOccurrence > i
            End If
        End If
        At2 = StartingPosition
    End Function

    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'Ratc( ) User-Defined Function
    'Returns the numeric position of the last (rightmost) occurrence of a character string within another character string.
    'The search performed by Ratc() is case-insensitive (including  overlaps).
    'Ratc(cSearchExpression, cExpressionSearched [, nOccurrence])
    'Return Values Long
    'Parameters
    'cSearchExpression String Specifies the character expression that Ratc() looks for in cExpressionSearched.
    'cExpressionSearched String Specifies the character expression that Ratc() searches.
    'nOccurrence Long Specifies which occurrence, starting from the right and moving left,
    'of cSearchExpression Ratc() searches for in cExpressionSearched.
    'By default, Ratc() searches for the last occurrence of cSearchExpression (nOccurrence = 1).
    'If nOccurrence is 2, Ratc() searches for the next to last occurrence, and so on.
    'Remarks
    'Ratc(), the reverse of the Atc() function, searches the character expression in cExpressionSearched
    'starting from the right and moving left, looking for the last occurrence of the string specified in cSearchExpression.
    'Ratc() returns an integer indicating the position of the first character in cSearchExpression in cExpressionSearched.
    'Ratc() returns 0 if cSearchExpression is not found in cExpressionSearched, or if nOccurrence is greater
    'than the number of times cSearchExpression occurs in cExpressionSearched.
    'Example
    ' Dim gcString As  String, gcFindString As  String
    ' gcString = "abracadabra"
    ' gcFindString = "A"
    ' Print Ratc(gcFindString , gcString)     ' Displays 11
    ' Print Ratc(gcFindString , gcString , 3) ' Displays 6
    'See Also Atc()  User-Defined Function
    Public Function Ratc(ByVal cSearchExpression As String, ByVal cExpressionSearched As String, Optional ByVal nOccurrence As Long = 1) As Long
        Ratc = Rat(LCase$(cSearchExpression), LCase$(cExpressionSearched), nOccurrence)
    End Function
    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'Atc2() User-Defined Function
    'Returns the beginning numeric position of the first occurrence of a character expression within another character expression, counting from the leftmost character.
    'The search performed by Atc2() is case-insensitive (excluding  overlaps).
    'Atc2(cSearchExpression, cExpressionSearched [, nOccurrence]) Return Values Long
    'Parameters
    'cSearchExpression String Specifies the character expression that Atc2( ) searches for in cExpressionSearched.
    'cExpressionSearched String Specifies the character expression cSearchExpression searches for.
    'nOccurrence Long Specifies which occurrence (first, second, third, and so on) of cSearchExpression
    'is searched for in cExpressionSearched. By default, Atc2() searches for the first occurrence of cSearchExpression (nOccurrence = 1).
    'Including nOccurrence lets you search for additional occurrences of cSearchExpression in cExpressionSearched.
    'Atc2() returns 0 if nOccurrence is greater than the number of times cSearchExpression occurs in cExpressionSearched.
    'Remarks
    'Atc2() searches the second character expression for the first occurrence of the first character expression.
    'It then returns an integer indicating the position of the first character in the character expression found.
    'If the character expression is not found, Atc2() returns 0.
    'Example
    ' Dim gcString As  String, gcFindString As  String
    ' gcString = "Now is the time for all good men"
    ' gcFindString = "is the"
    ' Print Atc2(gcFindString, gcString)   ' Displays 5
    ' gcFindString = "IS"
    ' Print Atc2(gcFindString, gcString)   ' Displays 5, case-insensitive
    ' gcString = "goood men"
    ' gcFindString = "oo"
    ' Print Atc2(gcFindString, gcString, 1)   ' Displays 2
    ' Print Atc2(gcFindString, gcString, 2)   ' Displays 0, excluding  overlaps
    'See Also At(), At2(), Atc2()  User-Defined Function
    Public Function Atc2(ByVal cSearchExpression As String, ByVal cExpressionSearched As String, Optional ByVal nOccurrence As Long = 1) As Long
        Atc2 = At2(LCase$(cSearchExpression), LCase$(cExpressionSearched), nOccurrence)
    End Function
    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'Atc() User-Defined Function
    'Returns the beginning numeric position of the first occurrence of a character expression within another character expression, counting from the leftmost character.
    'The search performed by Atc() is case-insensitive (including  overlaps).
    'Atc(cSearchExpression, cExpressionSearched [, nOccurrence]) Return Values Long
    'Parameters
    'cSearchExpression As  String Specifies the character expression that Atc( ) searches for in cExpressionSearched.
    'cExpressionSearched As  String Specifies the character expression cSearchExpression searches for.
    'nOccurrence Long Specifies which occurrence (first, second, third, and so on) of cSearchExpression is searched for in cExpressionSearched.
    'By default, Atc() searches for the first occurrence of cSearchExpression (nOccurrence = 1).
    'Including nOccurrence lets you search for additional occurrences of cSearchExpression in cExpressionSearched.
    'Atc( ) returns 0 if nOccurrence is greater than the number of times cSearchExpression occurs in cExpressionSearched.
    'Remarks
    'Atc() searches the second character expression for the first occurrence of the first character expression,
    'without concern for the case (upper or lower) of the characters in either expression. Use At( ) to perform a case-sensitive search.
    'It then returns an integer indicating the position of the first character in the character expression found.
    'If the character expression is not found, Atc() returns 0.
    'Example
    ' Dim gcString As  String, gcFindString As  String
    ' gcString = "Now is the time for all good men"
    ' gcFindString = "is the"
    ' Print Atc(gcFindString, gcString)   ' Displays 5
    ' gcFindString = "IS"
    ' Print Atc(gcFindString, gcString)   ' Displays 5, case-insensitive
    'See Also At()  User-Defined Function
    'UDF the name and functionality of which correspond  to the  Visual FoxPro function
     Public Function Atc(ByVal cSearchExpression As String, ByVal cExpressionSearched As String, Optional ByVal nOccurrence As Long = 1) As Long
        Atc = At(LCase$(cSearchExpression), LCase$(cExpressionSearched), nOccurrence)
    End Function
    
    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'Chrtran() User-Defined Function
    'Replaces each character in a character expression that matches a character in a second character expression with the corresponding character in a third character expression.
    'Chrtran(cExpressionSearched,   cSearchExpression,  cReplacementExpression)
    'Return Values String
    'Parameters
    'cSearchedExpression   Specifies the expression in which Chrtran( ) replaces characters.
    'cSearchExpression  Specifies the expression containing the characters Chrtran( ) looks for in cSearchedExpression.
    'cReplacementExpression Specifies the expression containing the replacement characters.
    'If a character incSearchExpression is found in cSearchedExpression,
    'the character in cSearchedExpression is replaced by a character from cReplacementExpression
    'that is in the same position in cReplacementExpression as the respective character in cSearchExpression.
    'If cReplacementExpression has fewer characters than cSearchExpression, the additional characters in cSearchExpression
    'are deleted from cSearchedExpression.
    'If cReplacementExpression has more characters than cSearchExpression, the additional characters in cReplacementExpression are ignored.
    'Remarks
    'Chrtran() translates the character expression cSearchedExpression using the translation expressions cSearchExpression and cReplacementExpression
    'and returns the resulting character string.
    'Example
    ' Print Chrtran("ABCDEF", "ACE", "XYZ")       ' Displays XBYDZF
    ' Print Chrtran("ABCDEF", "ACE", "XYZQRST")   ' Displays XBYDZF
    'See Also Strfilter()
    'UDF the name and functionality of which correspond  to the  Visual FoxPro function
    Public Function Chrtran(ByVal cExpressionSearched As String, ByVal cSearchExpression As String, ByVal cReplacementExpression As String) As String
        Dim i As Long, j As Long, flag As Boolean, cExpressionTranslated As String
        If Len(cReplacementExpression) > Len(cSearchExpression) Then
            cReplacementExpression = Mid$(cReplacementExpression, 1, Len(cSearchExpression))
        End If
        For i = 1 To Len(cReplacementExpression)
            If InStr(i, cSearchExpression, Mid$(cReplacementExpression, i, 1), vbBinaryCompare) > 0 Then
                flag = True
                Exit For
            End If
        Next i
        If Not flag Then  '  this algorithm does not work always as Chrtran("eaba","ba","a") '  ' Displays  e  Error !!!  ea  Correctly
            For i = 1 To Len(cSearchExpression)
                If Len(cReplacementExpression) >= i Then
                    cExpressionSearched = Replace$(cExpressionSearched, Mid$(cSearchExpression, i, 1), Mid$(cReplacementExpression, i, 1), 1, -1, vbBinaryCompare)
                Else
                    cExpressionSearched = Replace$(cExpressionSearched, Mid$(cSearchExpression, i, 1), "", 1, -1, vbBinaryCompare)
                End If
            Next i
            cExpressionTranslated = cExpressionSearched
        Else
            For i = 1 To Len(cExpressionSearched)
                j = InStr(1, cSearchExpression, Mid$(cExpressionSearched, i, 1), vbBinaryCompare)
                If j > 0 Then
                    If Len(cReplacementExpression) >= j Then
                        cExpressionTranslated = cExpressionTranslated & Mid$(cReplacementExpression, j, 1)
                    End If
                Else
                    cExpressionTranslated = cExpressionTranslated & Mid$(cExpressionSearched, i, 1)
                End If
            Next i
        End If
        Chrtran = cExpressionTranslated
    End Function
    
    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'Strfilter() User-Defined Function
    'Removes all characters from a string except those specified.
    'Strfilter(cExpressionSearched, cSearchExpression)
    'Return Values String
    'Parameters
    'cExpressionSearched  Specifies the character string to search.
    'cSearchExpression Specifies the characters to search for and retain in cExpressionSearched.
    'Remarks: Strfilter() removes all the characters from cExpressionSearched that are not in cSearchExpression, then returns the characters that remain.
    'Example
    ' Print Strfilter("asdfghh5hh1jk6f3b7mn8m3m0m6","0123456789")    ' Displays 516378306
    ' Print Strfilter("ABCDABCDABCD", "AB")    ' Displays ABABAB
    'See Also Chrtran()
    Public Function Strfilter(ByVal cExpressionSearched As String, ByVal cSearchExpression As String) As String
        Dim StrFiltred As String, i As Integer
        For i = 1 To Len(cExpressionSearched)
            If InStr(1, cSearchExpression, Mid$(cExpressionSearched, i, 1), vbBinaryCompare) > 0 Then
                StrFiltred = StrFiltred & Mid$(cExpressionSearched, i, 1)
            End If
        Next i
        Strfilter = StrFiltred
    End Function
    
    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'Strtran() User-Defined Function
    'Searches a character expression for occurrences of a second character expression,
    'and then replaces each occurrence with a third character expression.
    'Strtran  (cSearched, cExpressionSought , [cReplacement][, nStartOccurrence] [, nNumberOfOccurrences] [, nFlags])
    'Return Values String
    'Parameters
    'cSearched         Specifies the character expression that is searched.
    'cExpressionSought Specifies the character expression that is searched for in cSearched.
    'cReplacement      Specifies the character expression that replaces every occurrence of cExpressionSought in cSearched.
    'If you omit cReplacement, every occurrence of cExpressionSought is replaced with the empty string.
    'nStartOccurrence  Specifies which occurrence of cExpressionSought is the first to be replaced.
    'For example, if nStartOccurrence is 4, replacement begins with the fourth occurrence of cExpressionSought in cSearched and the first three occurrences of cExpressionSought remain unchanged.
    'The occurrence where replacement begins defaults to the first occurrence of cExpressionSought if you omit nStartOccurrence.
    'nNumberOfOccurrences  Specifies the number of occurrences of cExpressionSought to replace.
    'If you omit nNumberOfOccurrences, all occurrences of cExpressionSought, starting with the occurrence specified with nStartOccurrence, are replaced.
    'nFlags  Specifies the case-sensitivity of a search according to the following values:
    'nFlags     Description
    '0 (default) Search is case-sensitive, replace is with exact cReplacement string.
    '1           Search is case-insensitive, replace is with exact cReplacement string.
    '2           Search is case-sensitive; replace is with the case of cReplacement changed to match the case of the string found.
    '            The case of cReplacement will only be changed if the string found is all uppercase, lowercase, or proper case.
    '3           Search is case-insensitive; replace is with the case of cReplacement changed to match the case of the string found.
    '            The case of cReplacement will only be changed if the string found is all uppercase, lowercase, or proper case.
    'Remarks
    'You can specify where the replacement begins and how many replacements are made.
    'Strtran( ) returns the resulting character string.
    'Specify �1 for optional parameters you want to skip over if you just need to specify the nFlags setting.
    'Example
    ' Print Strtran("ABCDEF", "ABC", "XYZ", -1,-1,0)                    ' Displays XYZDEF
    ' Print Strtran("ABCDEF", "ABC", "", -1,-1, 0)                      ' Displays DEF
    ' Print Strtran("ABCDEFABCGHJabcQWE", "ABC", "",2,-1,0)             ' Displays ABCDEFGHJabcQWE
    ' Print Strtran("ABCDEFABCGHJabcQWE", "ABC", "",2,-1,1)             ' Displays ABCDEFGHJQWE
    ' Print Strtran("ABCDEFABCGHJabcQWE", "ABC", "XYZ",  2, 1, 1)       ' Displays ABCDEFXYZGHJabcQWE
    ' Print Strtran("ABCDEFABCGHJabcQWE", "ABC", "XYZ",  2, 3, 1)       ' Displays ABCDEFXYZGHJXYZQWE
    ' Print Strtran("ABCDEFABCGHJabcQWE", "ABC", "XYZ",  2, 1, 2)       ' Displays ABCDEFXYZGHJabcQWE
    ' Print Strtran("ABCDEFABCGHJabcQWE", "ABC", "XYZ",  2, 3, 2)       ' Displays ABCDEFXYZGHJabcQWE
    ' Print Strtran("ABCDEFABCGHJabcQWE", "ABC", "xyZ",  2, 1, 2)       ' Displays ABCDEFXYZGHJabcQWE
    ' Print Strtran("ABCDEFABCGHJabcQWE", "ABC", "xYz",  2, 3, 2)       ' Displays ABCDEFXYZGHJabcQWE
    ' Print Strtran("ABCDEFAbcCGHJAbcQWE", "Aab", "xyZ",  2, 1, 2)      ' Displays ABCDEFAbcCGHJAbcQWE
    ' Print Strtran("abcDEFabcGHJabcQWE", "abc", "xYz",  2, 3, 2)       ' Displays abcDEFxyzGHJxyzQWE
    ' Print Strtran("ABCDEFAbcCGHJAbcQWE", "Aab", "xyZ",  2, 1, 3)      ' Displays ABCDEFAbcCGHJAbcQWE
    ' Print Strtran("ABCDEFAbcGHJabcQWE", "abc", "xYz",  1, 3, 3)       ' Displays XYZDEFXyzGHJxyzQWE
    ' Attention !
    ' for the characters  "�挜���" chr(198), chr(230), chr(140), chr(156),  chr(222), chr(254), chr(223)
    ' this function does not work correctly for nFlags 1,3
    ' for the characters  "��" chr(131), chr(223) this function does not work correctly for nFlags 2,3
    'See Also replace(), Chrtran()
    'UDF the name and functionality of which correspond  to the  Visual FoxPro function
    Public Function Strtran(ByVal cSearched As String, ByVal cExpressionSought As String, Optional ByVal cReplacement As String = "", _
    Optional ByVal nStartOccurrence As Long = -1, Optional ByVal nNumberOfOccurrences As Long = -1, Optional ByVal nFlags As Long = 0) As String
        Dim StartPart As String, FinishPart As String, nAtStartOccurrence As Long, nAtFinishOccurrence As Long
        If nStartOccurrence = -1 Then
            nStartOccurrence = 1
        End If
        If nFlags = 0 Or nFlags = 2 Then
            nAtStartOccurrence = At2(cExpressionSought, cSearched, nStartOccurrence)
            If nNumberOfOccurrences = -1 Then
                nAtFinishOccurrence = 0
            Else
                nAtFinishOccurrence = At2(cExpressionSought, cSearched, nStartOccurrence + nNumberOfOccurrences)
            End If
        ElseIf nFlags = 1 Or nFlags = 3 Then
            nAtStartOccurrence = At2(LCase$(cExpressionSought), LCase$(cSearched), nStartOccurrence)
            If nNumberOfOccurrences = -1 Then
                nAtFinishOccurrence = 0
            Else
                nAtFinishOccurrence = At2(LCase$(cExpressionSought), LCase$(cSearched), nStartOccurrence + nNumberOfOccurrences)
            End If
        Else
            cSearched = "Error, sixth parameter must be 0, 1, 2, 3 !"
        End If
        If nAtStartOccurrence > 0 Then
            StartPart = Mid$(cSearched, 1, nAtStartOccurrence - 1)
            If nAtFinishOccurrence > 0 Then
                FinishPart = Mid$(cSearched, nAtFinishOccurrence)
                cSearched = Mid$(cSearched, nAtStartOccurrence, nAtFinishOccurrence - nAtStartOccurrence)
            Else
                cSearched = Mid$(cSearched, nAtStartOccurrence)
            End If
            If nFlags = 0 Or (nFlags = 2 And Len(cReplacement) = 0) Then
                cSearched = Replace$(cSearched, cExpressionSought, cReplacement, 1, -1, vbBinaryCompare)
            ElseIf nFlags = 1 Or (nFlags = 3 And Len(cReplacement) = 0) Then
                cSearched = Replace$(cSearched, cExpressionSought, cReplacement, 1, -1, vbTextCompare)
            Else
                Dim cNewSearched As String, cNewExpressionSought As String, cNewReplacement As String, nAtPreviousOccurrence As Long, i As Long, lEmpty As Boolean
                nAtStartOccurrence = -Len(cExpressionSought) + 1
                For i = 1 To 2147483647
                    If nFlags = 3 Then
                        nAtStartOccurrence = InStr(nAtStartOccurrence + Len(cExpressionSought), cSearched, cExpressionSought, vbTextCompare)
                    Else
                        nAtStartOccurrence = InStr(nAtStartOccurrence + Len(cExpressionSought), cSearched, cExpressionSought, vbBinaryCompare)
                    End If
                    If nAtStartOccurrence = 0 Then
                        nAtStartOccurrence = nAtPreviousOccurrence
                        Exit For
                    End If
                    If i > 1 Then
                        cNewSearched = cNewSearched & Mid$(cSearched, nAtPreviousOccurrence + Len(cExpressionSought), nAtStartOccurrence - (nAtPreviousOccurrence + Len(cExpressionSought)))
                    Else
                        cNewSearched = cNewSearched & Mid$(cSearched, 1, nAtStartOccurrence - 1)
                    End If
                    cNewExpressionSought = Mid$(cSearched, nAtStartOccurrence, Len(cExpressionSought))
                    If LCase$(cNewExpressionSought) = UCase$(cNewExpressionSought) Then  '  no letters in string
                        cNewReplacement = cReplacement
                    ElseIf cNewExpressionSought = UCase$(cNewExpressionSought) Then
                        cNewReplacement = UCase$(cReplacement)
                    ElseIf cNewExpressionSought = LCase$(cNewExpressionSought) Then
                        cNewReplacement = LCase$(cReplacement)
                    Else
                        lEmpty = True
                    End If
                    If lEmpty Then
                        If UCase$(Left$(cNewExpressionSought, 1)) <> LCase$(Left$(cNewExpressionSought, 1)) And _
                                UCase$(Left$(cNewExpressionSought, 1)) = Left$(cNewExpressionSought, 1) And _
                                LCase$(Mid$(cNewExpressionSought, 2)) = Mid$(cNewExpressionSought, 2) Then
                                cNewReplacement = UCase$(Mid$(cReplacement, 1, 1)) + LCase$(Mid$(cReplacement, 2))
                                lEmpty = False
                        Else
                            Dim j As Long
                            For j = 1 To Len(cExpressionSought)
                                If UCase$(Mid$(cNewExpressionSought, j, 1)) <> LCase$(Mid$(cNewExpressionSought, j, 1)) Then   '  this is letter
                                    If LCase$(Mid$(cNewExpressionSought, j, 1)) = Mid$(cNewExpressionSought, j, 1) Then
                                        cNewReplacement = LCase$(cReplacement)
                                    Else
                                        cNewReplacement = cReplacement
                                    End If
                                    lEmpty = False
                                    Exit For
                                End If
                            Next j
                        End If
                    End If
                    If lEmpty Then
                        cNewReplacement = cReplacement
                        lEmpty = False
                    End If
                    cNewSearched = cNewSearched & cNewReplacement
                    nAtPreviousOccurrence = nAtStartOccurrence
                Next i
                cSearched = cNewSearched & Mid$(cSearched, nAtStartOccurrence + Len(cExpressionSought))
            End If
        End If
        Strtran = StartPart & cSearched & FinishPart
    End Function
    
    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'Occurs() User-Defined Function
    'Returns the number of times a character expression occurs within another character expression (including  overlaps).
    'Occurs(cSearchExpression, cExpressionSearched)
    'Return Values Long
    'Parameters
    'cSearchExpression String Specifies a character expression that Occurs() searches for within cExpressionSearched.
    'cExpressionSearched String Specifies the character expression Occurs() searches for cSearchExpression.
    'Remarks
    'Occurs() returns 0 (zero) if cSearchExpression is not found within cExpressionSearched.
    'Example
    ' Dim gcString As String
    ' gcString = "abracadabra"
    ' Print Occurs("a", gcString )   ' Displays 5
    ' Print Occurs("b", gcString )   ' Displays 2
    ' Print Occurs("c", gcString )   ' Displays 1
    ' Print Occurs("e", gcString )   ' Displays 0
    'Including  overlaps !!!
    ' Print Occurs("ABCA", "ABCABCABCA")  ' Displays 3
    '1 occurrence of substring "ABCA  .. BCABCA"
    '2 occurrence of substring "ABC...ABCA...BCA"
    '3 occurrence of substring "ABCABC...ABCA"
    'See Also At(), Rat(), Occurs2(), At2(), Atc(), Atc2()
    Public Function Occurs(ByVal cSearchExpression As String, ByVal cExpressionSearched As String) As Long
        Dim i As Long, StartingPosition As Long
        If Len(cExpressionSearched) > 0 And Len(cSearchExpression) > 0 Then
            Do
                i = i + 1
                StartingPosition = InStr(StartingPosition + 1, cExpressionSearched, cSearchExpression, vbBinaryCompare)
            Loop While StartingPosition <> 0
            i = i - 1
        End If
        Occurs = i
    End Function

    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'Occurs2() User-Defined Function
    'Returns the number of times a character expression occurs  within another character expression (excluding  overlaps).
    'Occurs2(cSearchExpression, cExpressionSearched)
    'Return Values Long
    'Parameters
    'cSearchExpression String Specifies a character expression that Occurs2() searches for within cExpressionSearched.
    'cExpressionSearched String Specifies the character expression Occurs2() searches for cSearchExpression.
    'Remarks
    'Occurs2() returns 0 (zero) if cSearchExpression is not found within cExpressionSearched.
    'Example
    ' Dim gcString As String
    ' gcString = "abracadabra"
    ' Print Occurs2("a", gcString)   ' Displays 5
    'Attention !!!
    'This function counts the 'occurs' excluding  overlaps !
    ' Print Occurs2("ABCA", "ABCABCABCA")  ' Displays 2
    '1 occurrence of substring "ABCA  .. BCABCA"
    '2 occurrence of substring "ABCABC... ABCA"
    'See Also Occurs()
    Public Function Occurs2(ByVal cSearchExpression As String, ByVal cExpressionSearched As String) As Long
        Dim i As Long
        If Len(cSearchExpression) > 0 Then
             i = CLng((Len(cExpressionSearched) - Len(Replace$(cExpressionSearched, cSearchExpression, "", 1, -1, vbBinaryCompare))) / Len(cSearchExpression))
        End If
        Occurs2 = i
    End Function
    
    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'When performance is an issue: use StrConv( String, 3)  rather than this function.
    'Proper( ) User-Defined Function
    'Returns from a character expression a string capitalized as appropriate for proper names.
    'Proper(cExpression)
    'Return Values String
    'Parameters
    'cExpression String Specifies the character expression from which Proper( ) returns a capitalized character string.
    'Example
    ' Print Proper("Visual Basic.NET")   ' Displays "Visual Basic.net"
    ' Print Proper("VISUAL BASIC.NET")   ' Displays "Visual Basic.net"
    'Remarks
    'UDF the name and functionality of which correspond  to the  Visual FoxPro function
    Public Function Proper(ByVal expression As String) As String
        Dim properexpression As String, symbol As String, i As Long, flag As Boolean
        flag = True
        For i = 1 To Len(expression)
            symbol = LCase$(Mid$(expression, i, 1))
            If symbol = Chr(32) Or symbol = Chr(9) Or symbol = Chr(10) Or symbol = Chr(11) Or symbol = Chr(12) Or symbol = Chr(13) Or symbol = Chr(160) Or symbol = Chr(0) Then
                flag = True
            Else
                If flag Then
                    symbol = UCase$(symbol)
                    flag = False
                End If
            End If
            properexpression = properexpression & symbol
        Next i
        Proper = properexpression
    End Function
    
    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'Padl(), Padr(), Padc() User-Defined Functions
    'Returns a string from an expression, padded with spaces or characters to a specified length on the left or right sides, or both.
    'Padl(eExpression, nResultSize [, cPadCharacter]) -Or-
    'Padr(eExpression, nResultSize [, cPadCharacter]) -Or-
    'Padc(eExpression, nResultSize [, cPadCharacter])
    'Return Values String
    'Parameters
    'eExpression String Specifies the expression to be padded.
    'nResultSize  Long Specifies the total number of characters in the expression after it is padded.
    'cPadCharacter String Specifies the value to use for padding. This value is repeated as necessary to pad the expression to the specified number of characters.
    'If you omit cPadCharacter, spaces (ASCII character 32) are used for padding.
    'Remarks
    'Padl() inserts padding on the left, Padr() inserts padding on the right, and Padc() inserts padding on both sides.
    'Example
    ' Dim lcString  As String
    ' lcString = "TITLE"
    ' Print Padl(lcString, 40)
    ' Print Padl(lcString, 40, "=!=")
    ' Print Padr(lcString, 40, "=+=")
    ' Print Padc(lcString, 40, "=~")
    'UDF the name and functionality of which correspond  to the  Visual FoxPro function
    Public Function Padc(ByVal cString As String, ByVal nLen As Long, Optional ByVal cPadCharacter As String = " ") As String
        If IsNull(cPadCharacter) Or Len(cPadCharacter) = 0 Then
            cPadCharacter = " "
        End If
        If Len(cString) >= nLen Then
            cString = Mid$(cString, 1, nLen)
        Else
            Dim nLeftLen As Long, nRightLen As Long
            nLeftLen = Int((nLen - Len(cString)) / 2)    '  Quantity of characters, added at the left
            nRightLen = nLen - Len(cString) - nLeftLen   '  Quantity of characters, added on the right
            cString = Mid$(Replicate(cPadCharacter, Ceiling(nLeftLen / Len(cPadCharacter)) + 2), 1, nLeftLen) & cString & Mid$(Replicate(cPadCharacter, Ceiling(nRightLen / Len(cPadCharacter)) + 2), 1, nRightLen)
        End If
        Padc = cString
    End Function

    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'Padl(), Padr(), Padc() User-Defined Functions
    'Returns a string from an expression, padded with spaces or characters to a specified length on the left or right sides, or both.
    'UDF the name and functionality of which correspond  to the  Visual FoxPro function
    Public Function Padl(ByVal cString As String, ByVal nLen As Long, Optional ByVal cPadCharacter As String = " ") As String
        If IsNull(cPadCharacter) Or Len(cPadCharacter) = 0 Then
            cPadCharacter = " "
        End If
        If Len(cString) >= nLen Then
            cString = Mid$(cString, 1, nLen)
        Else
            Dim nLeftLen As Long
            nLeftLen = nLen - Len(cString)       '  Quantity of characters, added at the left
            cString = Mid$(Replicate(cPadCharacter, Ceiling(nLeftLen / Len(cPadCharacter)) + 2), 1, nLeftLen) & cString
        End If
        Padl = cString
    End Function
    
    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com
    'Padl(), Padr(), Padc() User-Defined Functions
    'Returns a string from an expression, padded with spaces or characters to a specified length on the left or right sides, or both.
    'UDF the name and functionality of which correspond  to the  Visual FoxPro function
    Public Function Padr(ByVal cString As String, ByVal nLen As Long, Optional ByVal cPadCharacter As String = " ") As String
        If IsNull(cPadCharacter) Or Len(cPadCharacter) = 0 Then
            cPadCharacter = " "
        End If
        If Len(cString) >= nLen Then
            cString = Mid$(cString, 1, nLen)
        Else
            Dim nRightLen As Long
            nRightLen = nLen - Len(cString)  '  Quantity of characters, added on the right
            cString = cString & Mid$(Replicate(cPadCharacter, Ceiling(nRightLen / Len(cPadCharacter)) + 2), 1, nRightLen)
        End If
        Padr = cString
    End Function
 
    'Arabtoroman() Returns the character Roman numeral equivalent of a specified numeric expression (from 1 to 3999)
    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com ,  25 April 2005 or XXV April MMV :-)
    'Arabtoroman(tNum) Return Values String Parameters tNum  Integer
    'Example
    ' Print Arabtoroman(3888)     '  Displays MMMDCCCLXXXVIII
    ' Print Arabtoroman(1888)     '  Displays MDCCCLXXXVIII
    ' Print Arabtoroman(1)        '  Displays I
    ' Print Arabtoroman(234)      '  Displays CCXXXIV
    'See also ROMANTOARAB()
    Public Function Arabtoroman(ByVal tNum As Integer) As String
        Const ROMANNUMERALS = "IVXLCDM"
        Dim lResult As String, tcNum As String, i As Integer
        tcNum = Trim(str(tNum))
        If tNum >= 1 And tNum <= 3999 Then
            For i = Len(tcNum) To 1 Step -1
                tNum = Val(Mid(tcNum, Len(tcNum) - i + 1, 1))
                Select Case tNum
                    Case 0
                    Case 1 To 3
                        lResult = lResult & String$(tNum, Mid$(ROMANNUMERALS, (i - 1) * 2 + 1, 1))
                    Case 4, 9
                        lResult = lResult & Mid$(ROMANNUMERALS, (i - 1) * 2 + 1, 1) & Mid$(ROMANNUMERALS, i * 2 + IIf(tNum = 9, 1, 0), 1)
                    Case Else
                        lResult = lResult & Mid$(ROMANNUMERALS, i * 2, 1) & String$(tNum - 5, Mid$(ROMANNUMERALS, (i - 1) * 2 + 1, 1))
                End Select
            Next i
        Else
            lResult = "Out of range, must be between 1 and 3999"
        End If
        Arabtoroman = lResult
    End Function
    
    'ROMANTOARAB() Returns the number equivalent of a specified character Roman numeral expression (from I to MMMCMXCIX)
    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com ,  25 April 2005 or XXV April MMV :-)
    'ROMANTOARAB(tcRomanNumber) Return Values smallint
    'Parameters tcRomanNumber  String Roman number
    'Example
    ' Print Romantoarab("MMMDCCCLXXXVIII")   '  Displays 3888
    ' Print Romantoarab("MDCCCLXXXVIII")     '  Displays 1888
    ' Print Romantoarab("I")                 '  Displays 1
    ' Print Romantoarab("CCXXXIV")           '  Displays 234
    'See also ARABTOROMAN()
    Public Function Romantoarab(ByVal tcRomanNumber As String) As Integer
        Const ROMANNUMERALS = "IVXLCDM"
        Dim lcRomanNumber As String
        Dim lnResult As Integer, i As Integer, lnDelim As Integer, lnK As Integer, lnJ As Integer
        Dim llOtherwise As Boolean
        
        tcRomanNumber = UCase$(Trim(tcRomanNumber))
        For i = 0 To Len(tcRomanNumber) - 1
            If InStr(ROMANNUMERALS, Mid$(tcRomanNumber, i + 1, 1)) > 0 Then
                lcRomanNumber = lcRomanNumber & Mid$(tcRomanNumber, i + 1, 1)
            Else
                lnResult = -1
                Exit For
            End If
        Next i
        If lnResult > -1 Then
            lnK = Len(lcRomanNumber)
            For i = 1 To 4
                If lnK = 0 Then
                    Exit For
                End If
                lnDelim = 10 ^ (i - 1)
                If lnK >= 2 And i < 4 Then
                    If Mid$(lcRomanNumber, lnK - 1, 2) = (Mid$(ROMANNUMERALS, (i - 1) * 2 + 1, 1) & Mid$(ROMANNUMERALS, i * 2, 1)) Then  '  CD or XL or IV
                       lnResult = lnResult + 4 * lnDelim
                       lnK = lnK - 2
                    ElseIf Mid$(lcRomanNumber, IIf(lnK >= 2, lnK - 1, 1), 2) = (Mid$(ROMANNUMERALS, (i - 1) * 2 + 1, 1) & Mid$(ROMANNUMERALS, i * 2 + 1, 1)) Then  '  CM or XC or IX
                            lnResult = lnResult + 9 * lnDelim
                            lnK = lnK - 2
                    Else
                        llOtherwise = True
                    End If
                 Else
                    llOtherwise = True
                 End If
                 
                If llOtherwise Then
                    For lnJ = 1 To 3    '  MMM or CCC or XXX or III
                        If lnK >= 1 Then
                           If Mid$(lcRomanNumber, lnK, 1) = Mid$(ROMANNUMERALS, (i - 1) * 2 + 1, 1) Then
                             lnResult = lnResult + lnDelim
                             lnK = lnK - 1
                           End If
                        End If
                    Next lnJ
                    If lnK = 0 Then
                        Exit For
                    End If
                    If i < 4 Then
                      If Mid$(lcRomanNumber, lnK, 1) = Mid$(ROMANNUMERALS, i * 2, 1) Then '  D or L or V
                        lnResult = lnResult + 5 * lnDelim
                        lnK = lnK - 1
                      End If
                    End If
                End If
                llOtherwise = False
            Next i
            
            If lnK > 0 Then
                lnResult = -1
            End If
        End If
        Romantoarab = lnResult
    End Function
    
    'Addromannumbers() User-Defined Function is written just FYA
    'Returns the result of addition, subtraction, multiplication or division of two Roman numbers
    'Author:  Igor Nikiforov,  Montreal,  EMail: udfunctions@gmail.com ,  25 April 2005 or XXV April MMV :-)
    'Addromannumbers(tcRomanNumber1, tcRomanNumber2, tcOperator) Return Values String
    'Parameters tcRomanNumber1 String Roman number
    'tcRomanNumber2 String Roman number, tcOperator String operator
    'Example
    ' Print Addromannumbers("I", "I")                      ' Displays II
    ' Print Addromannumbers("MMMDCCCLXXXVIII", "MDCCCLXXXVIII", "-") ' Displays MM
    ' Print Addromannumbers("DCCCLXXXVIII", "VIII")                   ' Displays DCCCXCVI
    ' Print Addromannumbers("DCCCLXXXVIII", "VIII", "*")            ' Displays Out of range, must be between 1 and 3999
    ' Print Addromannumbers("MMMDCCCLXXXVIII", "II", "/")           ' Displays MCMXLIV
    'See also ROMANTOARAB(), ARABTOROMAN()
    Public Function Addromannumbers(ByVal tcRomanNumber1 As String, ByVal tcRomanNumber2 As String, Optional ByVal tcOperator As String = "+") As String
        Dim lcResult As String
        lcResult = "Wrong third parameter, must be +,-,*,/"
        If tcOperator = "+" Or tcOperator = "-" Or tcOperator = "*" Or tcOperator = "/" Then
            Dim lnN1 As Long, lnN2 As Long
            lnN1 = Romantoarab(tcRomanNumber1)
            lnN2 = Romantoarab(tcRomanNumber2)
            If lnN1 < 0 Or lnN2 < 0 Then
                lcResult = "Wrong " & IIf(lnN1 < 0, "first", "second") & " roman number"
            Else
                Select Case tcOperator
                    Case "+"
                        lnN1 = lnN1 + lnN2
                    Case "-"
                        lnN1 = lnN1 - lnN2
                    Case "*"
                        lnN1 = lnN1 * lnN2
                    Case Else
                        lnN1 = Int(lnN1 / lnN2)
                End Select
                If lnN1 < 1 Or lnN1 > 3999 Then
                   lnN1 = 0
                End If
                lcResult = Arabtoroman(CInt(lnN1))
            End If
        End If
        Addromannumbers = lcResult
    End Function

