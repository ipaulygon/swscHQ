VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmRecMRUMngt 
   Caption         =   "Manage MRUs"
   ClientHeight    =   9720
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15150
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9720
   ScaleWidth      =   15150
   Begin VB.CommandButton cmdSelectNone 
      Caption         =   "Select &None"
      Height          =   450
      Left            =   4080
      TabIndex        =   10
      Top             =   9000
      Width           =   1815
   End
   Begin VB.CommandButton cmdSelectAll 
      Caption         =   "Select &All"
      Height          =   450
      Left            =   1920
      TabIndex        =   9
      Top             =   9000
      Width           =   1815
   End
   Begin VB.CommandButton cmdRSO 
      Caption         =   "&Receive Upload"
      Height          =   450
      Left            =   8400
      TabIndex        =   8
      Top             =   9000
      Width           =   1815
   End
   Begin VB.CommandButton cmdSSO 
      Caption         =   "Create &Download"
      Height          =   450
      Left            =   6240
      TabIndex        =   7
      Top             =   9000
      Width           =   1815
   End
   Begin VB.Frame fraSelect 
      Height          =   7785
      Left            =   240
      TabIndex        =   1
      Top             =   960
      Width           =   14715
      Begin MSComctlLib.ListView lsvMRUMngt 
         Height          =   7395
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   14475
         _ExtentX        =   25532
         _ExtentY        =   13044
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   16
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "CAN"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "MRU"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Job ID"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Meter Number"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "PUA+Reopening Fee"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Current Charge"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Total Balance"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Payment Amount"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "Payment Date"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Text            =   "disctag"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Text            =   "lastrdg"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Text            =   "dcdate"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Text            =   "status"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Text            =   "seq"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Text            =   "name"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   15
            Text            =   "address"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.CommandButton cmdClose 
      Cancel          =   -1  'True
      Caption         =   "&Close"
      Height          =   450
      Left            =   10560
      TabIndex        =   0
      Top             =   9000
      Width           =   1815
   End
   Begin VB.Label lblNumMRU 
      Caption         =   "Number of MRUs : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10920
      TabIndex        =   6
      Top             =   600
      Width           =   2295
   End
   Begin VB.Label lblDayNo 
      Caption         =   "Day Number : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10920
      TabIndex        =   5
      Top             =   240
      Width           =   1815
   End
   Begin VB.Label lblCycle 
      Caption         =   "Cycle/Month : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   480
      TabIndex        =   4
      Top             =   600
      Width           =   1575
   End
   Begin VB.Label lblBusCtr 
      Caption         =   "Business Center : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   480
      TabIndex        =   3
      Top             =   240
      Width           =   6015
   End
End
Attribute VB_Name = "frmRecMRUMngt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim iChecked As Integer
Dim MRUArray(1000) As String
Dim AcctArray(1000) As String
Dim MRUList As String
Dim AcctList As String
Dim SRSO As String

Sub UpdateRSOUpload(ByVal sMRU As String, iCycle As Integer)

    Dim strSQL As String

    ' First Update status to "Received From SO" in T_SCHED
    strSQL = "UPDATE T_SCHED SET BKSTCODE = 'RSO' WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
    If Not DBExecute(strSQL, Me.Name, "UpdateRSOUpload") Then
        Rollback_Upload sMRU, iCycle
        Exit Sub
    End If

    ' Next place the timestamp in T_BOOK
     strSQL = "UPDATE T_BOOK SET RECV_SO_DT = getdate() WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
     If Not DBExecute(strSQL, Me.Name, "UpdateRSOUpload") Then
        Rollback_Upload sMRU, iCycle
        Exit Sub
    End If

End Sub

Sub UpdateDCSSODownload(ByVal sAcct As String, iCycle As Integer)

    Dim strSQL As String

    ' First Update status to "Sent To SO" in T_SCHED
    strSQL = "UPDATE T_SCHED SET BKSTCODE = 'SSO' WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
    If Not DBExecute(strSQL, Me.Name, "UpdateSSODownload") Then Exit Sub

    ' Next place the timestamp in T_BOOK
     strSQL = "UPDATE T_BOOK SET UPLD_SO_DT = getdate() WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
     If Not DBExecute(strSQL, Me.Name, "UpdateSSODownload") Then Exit Sub

End Sub

Sub UpdateSSODownload(ByVal sMRU As String, iCycle As Integer)

    Dim strSQL As String

    ' First Update status to "Sent To SO" in T_SCHED
    strSQL = "UPDATE T_SCHED SET BKSTCODE = 'SSO' WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
    If Not DBExecute(strSQL, Me.Name, "UpdateSSODownload") Then Exit Sub

    ' Next place the timestamp in T_BOOK
     strSQL = "UPDATE T_BOOK SET UPLD_SO_DT = getdate() WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
     If Not DBExecute(strSQL, Me.Name, "UpdateSSODownload") Then Exit Sub

End Sub

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub cmdRSO_Click()

Dim sPath As String
Dim lsql As String
Dim iCycle As Integer
Dim strSQL As String


iCycle = str(intBillCycle)
'lsql = "select DIRDLDISCON from S_PARAM where COMPANY_NAME='INDRA'"
strSQL = "SELECT WORKING_DIR FROM R_BUSCTR" 'WHERE BC_CODE = '" & sBC & "'"
OpenRecordset g_rs_BUSCTR, strSQL, "frmMRUMngt", "cmdRSO_Click"
g_rs_BUSCTR.MoveFirst

sPath = g_rs_BUSCTR.Fields(0) & "" & "\RECON\FRSO"


If UploadToRCDB(sPath, iCycle) Then
    MsgBox "Receive File from SO Successful!", vbInformation, "System Message"
End If

End Sub

Private Sub cmdSelectAll_Click()
SelectAll 1, 0
End Sub

Private Sub cmdSelectNone_Click()
SelectAll 0, 0
End Sub

Private Sub cmdSSO_Click()
Dim iCycle As Integer
Dim sMRU As String
Dim n As Integer
Dim iCount As Integer
Dim itm
Dim l_int_Index As Integer
Dim DFile As String
Dim DFile1 As String
Dim DFile2 As String
Dim sPath As String
Dim fs As Object, fso As Object
Dim a
Dim bccode As String
Dim BCDESC As String
Dim cdelim As String
Dim rsDL As Recordset
Dim jobid As String
Dim ACCTNUM As String
Dim dcdt As Date
On Error GoTo errhandler

bccode = Left(sBusCenter, 4)
BCDESC = Mid(sBusCenter, 7)
iCycle = str(intBillCycle)
cdelim = "|"

If Not OpenRecordset(g_rs_RBUSCTR, "SELECT WORKING_DIR, BC_DESC FROM R_BUSCTR WHERE BC_CODE ='" & bccode & "'", "", "") Then
    Exit Sub
End If

sPath = g_rs_RBUSCTR.Fields(0) & ""
If sPath = "" Then
    MsgBox "Business Center Directory not specified!", vbCritical, "System Message"
    Exit Sub
End If

Set fs = CreateObject("Scripting.FileSystemObject")

' create folder toso\bccode bcdesc\year\cycle monthname\sched mr day, i.e. TOSO\0100 Novaliches\2008\09 September\06
If Not fs.FolderExists(sPath) Then fs.CreateFolder (sPath) ' dl directory
If Not fs.FolderExists(sPath & "\RECON\TOSO\") Then fs.CreateFolder (sPath & "\RECON\TOSO\") ' TOSO folder
If Not fs.FolderExists(sPath & "\RECON\TOSO\" & bccode & " " & BCDESC) Then fs.CreateFolder (sPath & "\RECON\TOSO\" & bccode & " " & BCDESC) ' BC folder
If Not fs.FolderExists(sPath & "\RECON\TOSO\" & bccode & " " & BCDESC & "\" & Year(Date)) Then fs.CreateFolder (sPath & "\RECON\TOSO\" & bccode & " " & BCDESC & "\" & Year(Date)) ' year folder
If Not fs.FolderExists(sPath & "\RECON\TOSO\" & bccode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0")))) Then fs.CreateFolder (sPath & "\RECON\TOSO\" & bccode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0")))) ' cycle folder
If Not fs.FolderExists(sPath & "\RECON\TOSO\" & bccode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0"))) & "\" & Format(Date, "dd")) Then fs.CreateFolder (sPath & "\RECON\TOSO\" & bccode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0"))) & "\" & Format(Date, "dd")) ' sched mr day folder

DFile = sPath & "\RECON\TOSO\" & bccode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0"))) & "\" & Format(Date, "dd") & "\" & "RC" & Left(sBusCenter, 2) & Year(Date) & Format(Date, "mm") & Format(Date, "dd") & ".txt"
DFile1 = sPath & "\RECON\TOSO\" & bccode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0"))) & "\" & Format(Date, "dd") & "\" & "RC" & Left(sBusCenter, 2) & Year(Date) & Format(Date, "mm") & Format(Date, "dd")

Set rsDL = New ADODB.Recordset


Dim strsql1 As String

'strsql1 = "SELECT DISTINCT a.RCJOBID FROM T_RC_DOWNLOAD A " & _
'"INNER JOIN T_RC_BOOK B ON A.RCJOBID=B.RCJOBID " & _
'"AND A.CYCLE=B.CYCLE AND A.BC_CODE=B.BC_CODE " & _
'"AND A.DAYNO=B.DAYNO AND B.BC_CODE = '" & bccode & "' AND B.CYCLE = " & iCycle & " AND B.DAYNO = " & intDayNo
''& " AND B.UPLD_SO_DT IS NULL"
'rsDL.Open strsql1, g_Conn, adOpenStatic, adLockReadOnly

' Loop through download file records
'Do While Not rsDL.EOF

Open DFile For Output As #1

    ' Reset MRUList
'    MRUList = ""
    iChecked = 0

    ' Count the number of Checked items in the ListView and build the MRUList from the text items
    iCount = lsvMRUMngt.ListItems.Count

'    ' If no MRUs were selected, exit function with True, you will be using an "Empty" MRUList!
'    If iCount = 0 Then
'        Generate_Acct_List = False
'        Exit Function
'    End If

    For Each itm In lsvMRUMngt.ListItems
       With itm
'         If .checked Then
'            iChecked = iChecked + 1
            l_int_Index = .Index
            With lsvMRUMngt.ListItems(l_int_Index)
                
             '   jobid = rsDL.Fields(0).Value
                    'If rsDL.Fields(0).Value = .SubItems(2) Then
                    If .SubItems(9) = "?" Or .SubItems(9) = "X" Then
                       Print #1, .SubItems(1) & cdelim & .Text & cdelim & .SubItems(13) & cdelim & .SubItems(3) & cdelim & .SubItems(14) & cdelim & .SubItems(15)
                   ' End If
                End If

            End With


       End With
    Next

Close #1
Dim jobid2 As String
Dim counter As Integer
Dim sParam As String
Dim sparamRS As ADODB.Recordset
sParam = "SELECT BATCHID FROM S_PARAM WHERE COMPANY_NAME = 'INDRA'"
If OpenRecordset(sparamRS, sParam, "", "") = True Then
counter = CInt(sparamRS.Fields("BATCHID").Value)
End If

If counter = 99 Then
DBExecute "UPDATE S_PARAM SET BATCHID = 1 WHERE COMPANY_NAME = 'INDRA'", "", ""
End If



If counter < 10 Then
DFile2 = DFile1 & "0" & counter & ".txt"
Else
DFile2 = DFile1 & counter & ".txt"
End If

DBExecute "UPDATE S_PARAM SET BATCHID = " & counter + 1 & " WHERE COMPANY_NAME = 'INDRA'", "", ""


Set fso = CreateObject("Scripting.FileSystemObject")
' Delete file if exist
If fso.FileExists(DFile2) Then fso.DeleteFile DFile2

'fso.CopyFile fcfile, fcfile2
Name DFile As DFile2    ' Rename dfile.

If Not DBExecute("UPDATE T_RC_BOOK SET UPLD_SO_DT=GETDATE() WHERE CYCLE=" & iCycle & " AND BC_CODE ='" & bccode & "' AND DAYNO = " & intDayNo, "", "") Then
End If

'    rsDL.MoveNext
'Loop

MsgBox "Download File Creation Successful!", vbInformation, ""

errhandler:
If Err.Number <> 0 Then
    MsgBox Err.Description, , "Error"
End If

End Sub

Private Sub Form_Activate()
'frmMRUMngt.WindowState = 2
'    ListView_Load
End Sub

Private Sub Form_Load()

If WindowState = vbNormal Then
    Me.Move (0)
    Me.Top = 0
    Me.Height = 10230
    Me.Width = 15270
End If

    lblBusCtr.Caption = "Business Center : " & sBusCenter
    'lblCycle.Caption = "Cycle/Month : " & Right(g_BillMonth, 2)
    lblCycle.Caption = "Cycle/Month : " & str(intBillCycle)
    lblDayNo.Caption = "Day Number : " & str(intDayNo)

    ListView_Load

End Sub

Private Sub ListView_Load()

    Dim l_str_Sql As String
    Dim lBusCenter As String
    'Dim intCycle As Integer
    Dim intNumMRU As Integer

    ' Pass the variables from the MRU Scheduler Form
    lBusCenter = Left$(sBusCenter, 4)
    'intCycle = Right(g_BillMonth, 2)


    l_str_Sql = "SELECT A.ACCTNUM, A.BOOKNO , A.RCJOBID, A.SERIALNO, PUA+RECONFEE AS PUARECONFEE, CURRENT_AMT, TOTAL_AMT, AMOUNT_PAID, " & _
        "a.PYMT_DT , C.DC_STATUS, rdg, d.RCDATE, C.Status,SEQ,ACCTNAME,ADDRESS " & _
        "FROM T_RC_DOWNLOAD A " & _
        "LEFT JOIN T_CUST_MASTER C ON A.ACCTNUM=C.ACCTNUM LEFT JOIN T_RC_UPLOAD D ON " & _
        "a.ACCTNUM = d.ACCTNUM And a.cycle = d.cycle " & _
        "WHERE A.BC_CODE = '" & lBusCenter & "' AND A.CYCLE = " & intBillCycle & " AND A.DAYNO = " & intDayNo
    

    lsvMRUMngt.ListItems.Clear
    If Not OpenRecordset(g_rs_RFF, l_str_Sql, Me.Name, "ListView_Load") Then Exit Sub

    g_rs_RFF.MoveFirst
    While Not g_rs_RFF.EOF
       ListViewSet 16
       g_rs_RFF.MoveNext
    Wend

    g_rs_RFF.Close
    Set g_rs_RFF = Nothing

    intNumMRU = lsvMRUMngt.ListItems.Count

    lblNumMRU.Caption = "Number of MRUs : " & str(intNumMRU)

End Sub

Private Sub ListViewSet(intCols As Integer, Optional intColDate As Integer, Optional intColTime As Integer)
    Dim lobjItem As Object
    Dim l_int_Index As Integer

    Set lobjItem = lsvMRUMngt.ListItems.Add(, , CheckNull(g_rs_RFF.Fields(0)))

    For l_int_Index = 1 To intCols - 1
        Select Case l_int_Index
            Case intColDate
                lobjItem.SubItems(l_int_Index) = Format(CheckNull(g_rs_RFF.Fields(l_int_Index)), "mm/dd/yyyy")
            Case intColTime
                lobjItem.SubItems(l_int_Index) = Format(CheckNull(g_rs_RFF.Fields(l_int_Index)), "hh:mm:ss")
            Case Else
                lobjItem.SubItems(l_int_Index) = CheckNull(g_rs_RFF.Fields(l_int_Index))
        End Select
    Next

    Set lobjItem = Nothing

End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmRecMRUGrid.Enabled = True
End Sub

Private Sub lsvMRUMngt_Click()
If lsvMRUMngt.ListItems.Count > 0 Then
    If lsvMRUMngt.SelectedItem.SubItems(1) = "Downloaded from MWSI" Then
        cmdSSO.Enabled = True
        'cmdRSO.Enabled = False
    ElseIf lsvMRUMngt.SelectedItem.SubItems(1) = "Sent to Satellite" Then
        'cmdSSO.Enabled = False
        cmdRSO.Enabled = True
    Else
        'cmdSSO.Enabled = False
        'cmdRSO.Enabled = False
    End If



End If
End Sub

Private Sub lsvMRUMngt_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ListviewSort lsvMRUMngt, ColumnHeader.Index
End Sub

Private Function Generate_Acct_List() As Boolean
Dim iCount As Integer
'Dim iChecked As Integer
Dim itm
'Dim MRUArray(1000) As String
Dim n As Integer
Dim iCycle As Integer
Dim l_int_Index As Integer

    ' Reset MRUList
    MRUList = ""
    iChecked = 0

    'iCycle = Val(Right(g_BillMonth, 2))
    iCycle = str(intBillCycle)
    ' Count the number of Checked items in the ListView and build the MRUList from the text items
    iCount = lsvMRUMngt.ListItems.Count

    ' If no MRUs were selected, exit function with True, you will be using an "Empty" MRUList!
    If iCount = 0 Then
        Generate_Acct_List = False
        Exit Function
    End If

    For Each itm In lsvMRUMngt.ListItems
       With itm
         If .checked Then
            iChecked = iChecked + 1
            l_int_Index = .Index
            With lsvMRUMngt.ListItems(l_int_Index)
                MsgBox .Text
                MsgBox .SubItems(1)
                MsgBox .SubItems(3)
                MsgBox .SubItems(5)
            End With

         End If
       End With
    Next

    If iChecked = 0 Then
        MsgBox "No MRU selected!", vbCritical, "Manage MRUs"
        Generate_Acct_List = False
        Exit Function
    End If

    If SRSO = "SSO" Then
        n = 1
        For n = 1 To iChecked
            ' check if mru stat is RSO
            If Not OpenRecordset(g_rs_TSCHED, "SELECT BOOKNO FROM T_SCHED WHERE BOOKNO = '" & MRUArray(n) & "' and BKSTCODE= 'DLM' and CYCLE= " & iCycle & "", Me.Name, "Generate_MRU_List") Then
                MsgBox "Please uncheck MRU " & MRUArray(n) & ". Can only Send to SO books with status 'Downloaded from MWSI'!", vbCritical, "Manage MRUs"
                Generate_Acct_List = False
                Exit Function
            End If
        Next n
    ElseIf SRSO = "RSO" Then
        n = 1
        For n = 1 To iChecked
            ' check if mru stat is RSO
            If Not OpenRecordset(g_rs_TSCHED, "SELECT BOOKNO FROM T_SCHED WHERE BOOKNO = '" & MRUArray(n) & "' and BKSTCODE= 'SSO' and CYCLE= " & iCycle & "", Me.Name, "Generate_MRU_List") Then
                MsgBox "Please uncheck MRU " & MRUArray(n) & ". Can only Receive from SO books with status 'Sent to Satellite Office'!", vbCritical, "Manage MRUs"
                Generate_Acct_List = False
                Exit Function
            End If
        Next n
    End If


        Generate_Acct_List = True


End Function

Private Function SelectAll(ByVal DeOrSelectAll As Integer, ByVal Listbx As Integer) As Boolean ' Added the action that is needed
  Dim Item As ListItem

If Listbx = 0 Then
  SelectAll = True ' Default succes
  Select Case DeOrSelectAll
    Case 0 ' Unchecked Check1
      For Each Item In lsvMRUMngt.ListItems
        Item.checked = False ' Deselect
      Next
    Case 1 ' Checked Check1
      For Each Item In lsvMRUMngt.ListItems
        Item.checked = True ' Select
      Next
    Case Else ' 2 = Grayed or else so no action for now....
      SelectAll = False ' No succes reported back...
  End Select
End If

End Function

