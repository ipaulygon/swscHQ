VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmTariffSched 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Tariff Schedule"
   ClientHeight    =   9480
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11385
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9480
   ScaleWidth      =   11385
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   8640
      TabIndex        =   15
      Top             =   8760
      Width           =   2175
   End
   Begin VB.Frame frameLoader 
      Caption         =   "Select Folder to Process:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   9255
      Left            =   11880
      TabIndex        =   9
      Top             =   120
      Width           =   4455
      Begin VB.CommandButton cmdCancelLoad 
         Caption         =   "Cancel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   2040
         TabIndex        =   14
         Top             =   8520
         Width           =   2055
      End
      Begin VB.FileListBox Filelist 
         Height          =   3015
         Left            =   120
         TabIndex        =   13
         Top             =   5280
         Width           =   4215
      End
      Begin VB.DirListBox dir1 
         Height          =   4365
         Left            =   120
         TabIndex        =   12
         Top             =   840
         Width           =   4215
      End
      Begin VB.DriveListBox drive1 
         Height          =   315
         Left            =   120
         TabIndex        =   11
         Top             =   480
         Width           =   4215
      End
      Begin VB.CommandButton cmdLoad 
         Caption         =   "&Load"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   360
         TabIndex        =   10
         Top             =   8520
         Width           =   1455
      End
   End
   Begin VB.CommandButton cmdLoadTariff 
      Caption         =   "Load New Tariff"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3120
      TabIndex        =   7
      Top             =   8760
      Width           =   2175
   End
   Begin VB.CommandButton cmdGenerateTariffLayout 
      Caption         =   "Generate New Tariff Layout"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   6
      Top             =   8760
      Width           =   2175
   End
   Begin VB.Frame frameSelect 
      Caption         =   "Select Tariff"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   11055
      Begin VB.ComboBox cmbAreaType 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3840
         TabIndex        =   16
         Top             =   720
         Width           =   2535
      End
      Begin VB.CommandButton cmbReset 
         Caption         =   "Reset"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   9000
         TabIndex        =   8
         Top             =   600
         Width           =   1815
      End
      Begin VB.CommandButton cmdRetrieve 
         Caption         =   "Retrieve"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   6720
         TabIndex        =   5
         Top             =   600
         Width           =   2055
      End
      Begin VB.ComboBox cmbConsType 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   360
         TabIndex        =   4
         Top             =   720
         Width           =   3255
      End
      Begin VB.Label lblareatype 
         Caption         =   "Area Type"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4560
         TabIndex        =   18
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label lblconsumertype 
         Caption         =   "Consumer Type"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1080
         TabIndex        =   17
         Top             =   480
         Width           =   1695
      End
   End
   Begin VB.CommandButton cmdGenerateFileDs 
      Caption         =   "Generate Tariff for DS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5880
      TabIndex        =   2
      Top             =   8760
      Width           =   2175
   End
   Begin VB.Frame FrameTariff 
      Caption         =   "Select Folder to Process"
      Height          =   6975
      Left            =   120
      TabIndex        =   0
      Top             =   1560
      Width           =   11055
      Begin MSComctlLib.ListView lsvTariffSched 
         Height          =   6615
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   10815
         _ExtentX        =   19076
         _ExtentY        =   11668
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "TARIFF TYPE"
            Object.Width           =   4234
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   1
            Text            =   "RECNO"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   2
            Text            =   "AREA TYPE"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "LOW LIMIT"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "HIGH LIMIT"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "BASEAMOUNT"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "RATE"
            Object.Width           =   2540
         EndProperty
      End
   End
End
Attribute VB_Name = "frmTariffSched"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim stariff As String
Dim sAreatype As String
Dim RS_TARIFFTYPE As ADODB.Recordset
Dim iselect As String
Dim ilocation As String

'global variable for all
Dim Gstariff As String
Dim GsAreatype As String


Public Sub load_Tariff(Optional ByVal stariff$ = "ALL")
    Dim sql As String
    
    On Error GoTo errhandler
        With cmbConsType
            .Clear
            .AddItem "ALL"
            
'default retrieve all tariff
    sql = "select distinct(cast(A.CONSUMER_TYPE as varchar(1)) + ' - ' + b.CONSUMER_DESC) from dbo.R_TARIFF A " & _
          "inner join dbo.L_CONSUMERTYPE B on a.CONSUMER_TYPE = b.CONSUMERTYPEID"
            
            If OpenRecordset(RS_TARIFFTYPE, sql, "frmTariffSched", "load_Tariff") Then
                While Not RS_TARIFFTYPE.EOF
                    .AddItem RS_TARIFFTYPE.Fields(0)
                    RS_TARIFFTYPE.MoveNext
                Wend
            End If
            .ListIndex = 0
        End With
        
errhandler:
    If Err.Number <> 0 Then
        If Err.Number = 380 Then
        Else
            MsgBox Err.Description, , "System Message"
        End If
    End If
End Sub

Private Sub cmbAreaType_Click()
    GsAreatype = cmbAreaType.Text
End Sub

Private Sub cmbConsType_Click()
    Gstariff = cmbConsType.Text
End Sub

Private Sub cmbReset_Click()
    ListView_Reset
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdCancelLoad_Click()
    frmTariffSched.Width = 11475
    cmbConsType.Enabled = True
    cmbAreaType.Enabled = True
    cmdRetrieve.Enabled = True
    cmbReset.Enabled = True
    cmdGenerateTariffLayout.Enabled = True
    cmdGenerateFileDs.Enabled = True
End Sub

Private Sub cmdLoad_Click()
'Variables
Dim strFileFullPath As String
Dim strCurrFile As String
Dim fileName1 As String
Dim strSQL As String
Dim strLine As String

Dim objFSO As FileSystemObject

On Error GoTo errhandler

strFileFullPath = ilocation & "\"
strCurrFile = Filelist

Set objFSO = CreateObject("Scripting.FileSystemObject")

fileName1 = strFileFullPath & strCurrFile
If Right(fileName1, 4) = ".xls" Or Right(fileName1, 5) = ".xlsx" Then
    If LoaderValidate(fileName1) = True Then
        If TariffLoader(fileName1) = True Then
            MsgBox "New Tarriff has now been loaded"
        Else
            MsgBox "No file has been loaded"
        End If
    Else
        MsgBox "Invalid or Incorrect file"
    End If
End If


load_Tariff
load_AreaType
ListView_Load 1

cmbConsType.Enabled = True
cmbAreaType.Enabled = True
cmdRetrieve.Enabled = True
cmbReset.Enabled = True
cmdGenerateTariffLayout.Enabled = True
cmdGenerateFileDs.Enabled = True

errhandler:
    If Err.Number <> 0 Then
        If Err.Number = 380 Then
        Else
            MsgBox Err.Description, , "System Message"
        End If
    End If

End Sub

Private Sub cmdGenerateFileDs_Click()
ListView_Reset
    If GenerateFile(3) = True Then
        MsgBox "DS Tariff File for " & Gstariff & " has been created"
    Else
        MsgBox "No file has been created"
    End If
End Sub

Private Sub cmdGenerateTariffLayout_Click()
    If GenerateFile(2) = True Then
        MsgBox "File has been created"
    Else
        MsgBox "No file has been created"
    End If
End Sub

Private Sub cmdLoadTariff_Click()
Dim intResponse As Integer
Dim intResponse2 As Integer
Dim sqlDeleteTariff As String
Dim Criteria As Integer

cmbConsType.Enabled = False
cmbAreaType.Enabled = False
cmdRetrieve.Enabled = False
cmbReset.Enabled = False
cmdGenerateTariffLayout.Enabled = False
cmdGenerateFileDs.Enabled = False

If GenerateFile(1) = True Then
    If Gstariff = "ALL" And GsAreatype = "ALL" Then Criteria = 1        'criteria 1 all consumer and area
    If Gstariff <> "ALL" And GsAreatype = "ALL" Then Criteria = 2       'criteria 2 select consumer and all area
    If Gstariff = "ALL" And GsAreatype <> "ALL" Then Criteria = 3       'criteria 3 all consumer and select area
    If Gstariff <> "ALL" And GsAreatype <> "ALL" Then Criteria = 4      'criteria 1 select area and select area
        
    Select Case Criteria
    Case 1
        sqlDeleteTariff = "Delete from R_TARIFF"
        DBExecute sqlDeleteTariff, "", ""
        MsgBox "All Tariff has now been deleted"
        
    Case 2 ' Consumer type
        sqlDeleteTariff = "Delete from R_TARIFF where CONSUMER_TYPE = '" & Mid(Gstariff, 1, 1) & "'"
        DBExecute sqlDeleteTariff, "", ""
        MsgBox "All Tariff with Consumer Type " & Gstariff & " has now been deleted"
    
    Case 3 ' Area Type
        sqlDeleteTariff = "Delete from R_TARIFF where AREA_TYPE = '" & Mid(GsAreatype, 1, 1) & "'"
        DBExecute sqlDeleteTariff, "", ""
        MsgBox "All Tariff with Area Type " & GsAreatype & " has now been deleted"
    
    Case 4
        sqlDeleteTariff = "Delete from R_TARIFF where CONSUMER_TYPE = '" & Mid(Gstariff, 1, 1) & "' and AREA_TYPE = '" & Mid(GsAreatype, 1, 1) & "'"
        DBExecute sqlDeleteTariff, "", ""
        MsgBox "Tariff for " & Gstariff & " and Area Type " & GsAreatype & " has now been deleted"
    End Select
Else
    MsgBox "No Tariff have been deleted"
End If

load_Tariff
load_AreaType
ListView_Load 1

'display the loaded portion
intResponse2 = MsgBox("Proceed in loading the New Tariff?", vbYesNo + vbQuestion, "Quit")
If intResponse2 = vbYes Then
    frmTariffSched.Width = 16680
Else
    cmbConsType.Enabled = True
    cmbAreaType.Enabled = True
    cmdRetrieve.Enabled = True
    cmbReset.Enabled = True
    cmdGenerateTariffLayout.Enabled = True
    cmdGenerateFileDs.Enabled = True
    Exit Sub
End If

End Sub

Private Sub cmdRetrieve_Click()
    ListView_Load 1
End Sub

Private Sub dir1_Change()
    Filelist.Path = dir1.Path
End Sub

Private Sub drive1_Change()
    dir1.Path = drive1.Drive
End Sub

Private Sub Filelist_Click()
'since global on this form will be seen on others
ilocation = Filelist.Path
iselect = Filelist
 
End Sub

Private Sub Form_Load()
    load_Tariff
    load_AreaType
    ListView_Load 1
End Sub

Private Sub ListView_Load(Optional ByVal Mode As Integer = 0)
        
    Const NUMCRITERIA = 5

    Dim l_str_Sql As String, l_str_Where As String
    Dim l_str_Criteria As String
    Dim idxCriteria As Integer
    Dim intNumAcct As Integer
    Dim strdate As String
    Dim arrCriteria(NUMCRITERIA) As Boolean
    
    ' Initialize Strings
    l_str_Sql = "select distinct(cast(A.CONSUMER_TYPE as varchar(1)) + ' - ' + b.CONSUMER_DESC), " & _
                "A.TARIFF_RECNO, C.AREA_TYPE + ' - ' + C.AREA_TYPE_DESC, " & _
                "a.LOW_LIMIT , a.HIGH_LIMIT, a.BASEAMOUNT, a.Rate from dbo.R_TARIFF A " & _
                "inner join dbo.L_CONSUMERTYPE B on a.CONSUMER_TYPE = b.CONSUMERTYPEID " & _
                "inner join dbo.R_AREA_TYPE C on A.AREA_TYPE = c.area_type "
       
    l_str_Criteria = "":  l_str_Where = " WHERE "
   
    ' Initialize Criteria Array
    For idxCriteria = 1 To NUMCRITERIA
        arrCriteria(idxCriteria) = False
    Next
    
    'criteria 1 all consumer and area
        If Gstariff = "ALL" And GsAreatype = "ALL" Then arrCriteria(1) = True
    'criteria 2 select consumer and all area
        If Gstariff <> "ALL" And GsAreatype = "ALL" Then arrCriteria(2) = True
    'criteria 3 all consumer and select area
        If Gstariff = "ALL" And GsAreatype <> "ALL" Then arrCriteria(3) = True
    'criteria 1 select area and select area
        If Gstariff <> "ALL" And GsAreatype <> "ALL" Then arrCriteria(4) = True
    
    
    ' BUILD THE SQL STATEMENT based on the specified criteria
    For idxCriteria = 1 To NUMCRITERIA
        If arrCriteria(idxCriteria) Then
            Select Case idxCriteria
                Case 1
                    l_str_Criteria = l_str_Criteria & " a.CONSUMER_TYPE in (select CONSUMERTYPEID from dbo.L_CONSUMERTYPE) and a.area_type in " & _
                                    "(select area_type from dbo.R_AREA_TYPE)"
                Case 2 ' Consumer type
                    l_str_Criteria = l_str_Criteria & " a.CONSUMER_TYPE = '" & Mid(Gstariff, 1, 1) & "'"
                Case 3 ' Area Type
                    l_str_Criteria = l_str_Criteria & " A.AREA_TYPE = '" & Mid(GsAreatype, 1, 1) & "'"
                Case 4
                    l_str_Criteria = l_str_Criteria & " a.CONSUMER_TYPE = '" & Mid(Gstariff, 1, 1) & "'" & _
                                    " and A.AREA_TYPE = '" & Mid(GsAreatype, 1, 1) & "'"
            End Select
        End If
    Next
    
    l_str_Sql = l_str_Sql & l_str_Where & l_str_Criteria & " order by A.TARIFF_RECNO"

   lsvTariffSched.ListItems.Clear
    If Not OpenRecordset(RS_TARIFFTYPE, l_str_Sql, Me.Name, "ListView_Load") Then
        If Mode = 0 Then MsgBox "No accounts satisfy the retrieval criteria!", vbExclamation, "No Records Retrieved"
        Exit Sub
    End If
    
    RS_TARIFFTYPE.MoveFirst
    While Not RS_TARIFFTYPE.EOF
       ListView_Set 7
       RS_TARIFFTYPE.MoveNext
    Wend

    RS_TARIFFTYPE.Close
    Set RS_TARIFFTYPE = Nothing
    
    lsvTariffSched.SortKey = 1    'sort by MRU from 1
        lsvTariffSched.Sorted = True  'sort it
End Sub

Private Sub ListView_Set(intCols As Integer, Optional intColDate As Integer, Optional intColTime As Integer)
    Dim lobjItem As Object
       
    Set lobjItem = lsvTariffSched.ListItems.Add(, , CheckNull(RS_TARIFFTYPE.Fields(0)))
        lobjItem.SubItems(1) = Format(RS_TARIFFTYPE.Fields(1), "#00")
        lobjItem.SubItems(2) = RS_TARIFFTYPE.Fields(2)
        lobjItem.SubItems(3) = RS_TARIFFTYPE.Fields(3)
        lobjItem.SubItems(4) = RS_TARIFFTYPE.Fields(4)
        lobjItem.SubItems(5) = RS_TARIFFTYPE.Fields(5)
        lobjItem.SubItems(6) = RS_TARIFFTYPE.Fields(6)
    Set lobjItem = Nothing
    
End Sub

Private Sub Form_Activate()
    ListView_Load 1
End Sub

Private Sub lsvTariffSched_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
' Sort the list
    With lsvTariffSched
        .SortKey = ColumnHeader.Index - 1   'get the key using the column number
        If .SortOrder = lvwAscending Then   'reverse sort order from current setting
            .SortOrder = lvwDescending
        Else
            .SortOrder = lvwAscending
        End If
        .Sorted = True  'do the sort
    End With
End Sub

Private Sub ListView_Reset()
    Dim l_str_Sql As String
    
    ' Initialize Strings
    l_str_Sql = "select distinct(cast(A.CONSUMER_TYPE as varchar(1)) + ' - ' + b.CONSUMER_DESC), " & _
                "A.TARIFF_RECNO, C.AREA_TYPE + ' - ' + C.AREA_TYPE_DESC, " & _
                "a.LOW_LIMIT , a.HIGH_LIMIT, a.BASEAMOUNT, a.Rate from dbo.R_TARIFF A " & _
                "inner join dbo.L_CONSUMERTYPE B on a.CONSUMER_TYPE = b.CONSUMERTYPEID " & _
                "inner join dbo.R_AREA_TYPE C on A.AREA_TYPE = c.area_type "
   
   lsvTariffSched.ListItems.Clear
    If Not OpenRecordset(RS_TARIFFTYPE, l_str_Sql, Me.Name, "ListView_Reset") Then
        MsgBox "No accounts satisfy the retrieval criteria!", vbExclamation, "No Records Retrieved"
        Exit Sub
    End If
    
    RS_TARIFFTYPE.MoveFirst
    While Not RS_TARIFFTYPE.EOF
       ListView_Set 7
       RS_TARIFFTYPE.MoveNext
    Wend

    RS_TARIFFTYPE.Close
    Set RS_TARIFFTYPE = Nothing
    
    lsvTariffSched.SortKey = 1    'sort by tariff type from old 2
    lsvTariffSched.Sorted = True
    cmbConsType.ListIndex = 0
    cmbAreaType.ListIndex = 0
End Sub

Public Function TariffLoader(filenme As String) As Boolean
'columns in db
Dim new_CONSUMER_TYPE As String
Dim new_RECNO As String
Dim new_AREA_TYPE As String
Dim new_LOW_LIMIT As String
Dim new_HIGH_LIMIT As String
Dim new_BASEMOUNT As String
Dim new_RATE As String

'Excel Objects Variables
Dim objxlApp As Excel.Application
Dim objxlSht As Excel.Worksheet
Dim objxlRbg As Excel.Range

'FSO and Txt Stream Variables
Dim objFSO As FileSystemObject

'Function Variable of Integer
Dim intCtr As Integer
Dim intX As Integer

Dim strSQL As String

Set objFSO = New FileSystemObject
Set objxlApp = New Excel.Application
objxlApp.DisplayAlerts = False
objxlApp.Workbooks.Open (filenme)

intX = 2
intCtr = 0
While intCtr = 0
    If objxlApp.Cells(intX, 2) = "" Then intCtr = intX
    intX = intX + 1
Wend

    For intX = 2 To intCtr - 1
        new_CONSUMER_TYPE = Mid(objxlApp.Cells(intX, 1), 1, 1)
        new_RECNO = objxlApp.Cells(intX, 2)
        new_AREA_TYPE = Mid(objxlApp.Cells(intX, 3), 1, 1)
        new_LOW_LIMIT = objxlApp.Cells(intX, 4)
        new_HIGH_LIMIT = objxlApp.Cells(intX, 5)
        new_BASEMOUNT = Format(objxlApp.Cells(intX, 6), "###0.00")
        new_RATE = Format(objxlApp.Cells(intX, 7), "###0.00")
                      
        strSQL = "Insert INTO R_TARIFF (TARIFF_RECNO, CONSUMER_TYPE, AREA_TYPE, LOW_LIMIT, HIGH_LIMIT, BASEAMOUNT, RATE) " & _
                "values ('" & new_RECNO & "'," & _
                "'" & new_CONSUMER_TYPE & "'," & _
                "'" & new_AREA_TYPE & "'," & _
                "'" & new_LOW_LIMIT & "'," & _
                "'" & new_HIGH_LIMIT & "'," & _
                "'" & new_BASEMOUNT & "'," & _
                "'" & new_RATE & "')"
        DBExecute strSQL, "", ""
     Next
     
     objxlApp.Workbooks.Close
     Set objxlApp = Nothing
     TariffLoader = True
     
End Function

Public Function LoaderValidate(filenme1 As String) As Boolean
'columns in db
Dim validate_TARIFF_TYPE As String
Dim validate_RECNO As String
Dim validate_AREA_TYPE As String
Dim validate_LOW_LIMIT As String
Dim validate_HIGH_LIMIT As String
Dim validate_BASEMOUNT As String
Dim validate_RATE As String

'Excel Objects Variables
Dim objxlApp1 As Excel.Application
Dim objxlSht1 As Excel.Worksheet
Dim objxlRbg1 As Excel.Range

'FSO and Txt Stream Variables
Dim objFSO1 As FileSystemObject

'Function Variable of Integer
Dim intCtr1 As Integer
Dim intXY As Long
Dim errorcounter As Integer
Dim Counter1 As String

Set objFSO1 = New FileSystemObject
Set objxlApp1 = New Excel.Application
objxlApp1.DisplayAlerts = False
objxlApp1.Workbooks.Open (filenme1)

intXY = 2
intCtr1 = 0
While intCtr1 = 0
    If objxlApp1.Cells(intXY, 2) > 0 Then intCtr1 = intXY
    intXY = intXY + 1
Wend

errorcounter = 0
    For intXY = 2 To intCtr1 - 1
        validate_TARIFF_TYPE = objxlApp1.Cells(intXY, 1).Value
        If validate_TARIFF_TYPE = "" Then
            errorcounter = errorcounter + 1
        End If
        
        validate_RECNO = objxlApp1.Cells(intXY, 2).Value
        If validate_RECNO = "" Then
            errorcounter = errorcounter + 1
        End If
        
        validate_AREA_TYPE = objxlApp1.Cells(intXY, 3).Value
        If validate_AREA_TYPE = "" Then
            errorcounter = errorcounter + 1
        End If
        
        validate_LOW_LIMIT = objxlApp1.Cells(intXY, 4).Value
        If validate_LOW_LIMIT = "" Then
            errorcounter = errorcounter + 1
        End If
        
        validate_HIGH_LIMIT = objxlApp1.Cells(intXY, 5).Value
        If validate_HIGH_LIMIT = "" Then
            errorcounter = errorcounter + 1
        End If
        
        validate_BASEMOUNT = objxlApp1.Cells(intXY, 6).Value
        If validate_BASEMOUNT = "" Then
            errorcounter = errorcounter + 1
        End If
                        
        validate_RATE = objxlApp1.Cells(intXY, 8).Value
        If validate_RATE = "" Then
            errorcounter = errorcounter + 1
        End If
        
     Next

        If errorcounter = 0 Then
           objxlApp1.Workbooks.Close
           Set objxlApp1 = Nothing
           LoaderValidate = True
           
        Else
           MsgBox "Invalid Details Found. Please check"
           objxlApp1.Workbooks.Close
           Set objxlApp1 = Nothing
           LoaderValidate = False
        End If
     
End Function

Public Function GenerateFile(genSelector As String) As Boolean

Dim xlApp As Excel.Application
Dim xlWb As Excel.Workbook
Dim sPath As String 'directory is located at users app.path
Dim filenme As String
Dim listcounter As Integer
Dim X As Integer

Dim f
Dim dir As String

Dim nFilename As String
Dim nConstype As String

Set f = CreateObject("Scripting.FileSystemObject")

sPath = App.Path

On Error GoTo errhandler

Select Case genSelector
Case 1 'backup original tariff
    If sPath <> "" Then
        If Not f.FolderExists(sPath & "\Tariff Charges\") Then f.CreateFolder (sPath & "\Tariff Charges\")
        If Not f.FolderExists(sPath & "\Tariff Charges\" & "Original Tariff\") Then f.CreateFolder (sPath & "\Tariff Charges\" & "Original Tariff\")
        dir = sPath & "\Tariff Charges\" & "Original Tariff\" & Format(Date, "yyyy-mm-dd") & "\" ' & "_" & newnameload
        If Not f.FolderExists(dir) Then f.CreateFolder (dir)
    Else
        dir = ""
    End If

Case 2 'generate new tariff layout
    If sPath <> "" Then
        If Not f.FolderExists(sPath & "\Tariff Charges\") Then f.CreateFolder (sPath & "\Tariff Charges\")
        If Not f.FolderExists(sPath & "\Tariff Charges\" & "Layout\") Then f.CreateFolder (sPath & "\Tariff Charges\" & "Layout\")
        dir = sPath & "\Tariff Charges\" & "Layout\" & Format(Date, "mm-dd-yyyy") & "\"
        If Not f.FolderExists(dir) Then f.CreateFolder (dir)
    Else
        dir = ""
    End If

Case 3 'tariff for DS
    If sPath <> "" Then
        If Not f.FolderExists(sPath & "\Tariff Charges\") Then f.CreateFolder (sPath & "\Tariff Charges\")
        If Not f.FolderExists(sPath & "\Tariff Charges\" & "Tariff For DS\") Then f.CreateFolder (sPath & "\Tariff Charges\" & "Tariff For DS\")
        dir = sPath & "\Tariff Charges\" & "Tariff For DS\" & Format(Date, "mm-dd-yyyy") & "\"
        If Not f.FolderExists(dir) Then f.CreateFolder (dir)
    Else
        dir = ""
    End If
End Select

Set xlApp = New Excel.Application
Set xlWb = xlApp.Workbooks.Add
xlApp.Visible = False
     
If genSelector <> 3 Then
    xlWb.Sheets("Sheet1").Cells(1, 1).Value = "TARIFF TYPE"
    xlWb.Sheets("Sheet1").Cells(1, 2).Value = "RECNO"
    xlWb.Sheets("Sheet1").Cells(1, 3).Value = "AREA TYPE"
    xlWb.Sheets("Sheet1").Cells(1, 4).Value = "LOW LIMIT"
    xlWb.Sheets("Sheet1").Cells(1, 5).Value = "HIGH LIMIT"
    xlWb.Sheets("Sheet1").Cells(1, 6).Value = "BASEAMOUNT"
    xlWb.Sheets("Sheet1").Cells(1, 7).Value = "RATE"
End If
    
X = 2
For listcounter = 1 To lsvTariffSched.ListItems.Count

Select Case genSelector
Case 1 'backup original tariff
    xlWb.Sheets("Sheet1").Cells(X, 1).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X, 1).Value = lsvTariffSched.ListItems(listcounter).Text                 'TARIFF TYPE
    xlWb.Sheets("Sheet1").Cells(X, 2).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X, 2).Value = lsvTariffSched.ListItems(listcounter).ListSubItems(1).Text 'RECNO
    xlWb.Sheets("Sheet1").Cells(X, 3).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X, 3).Value = lsvTariffSched.ListItems(listcounter).ListSubItems(2).Text 'AREA TYPE
    xlWb.Sheets("Sheet1").Cells(X, 4).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X, 4).Value = lsvTariffSched.ListItems(listcounter).ListSubItems(3).Text 'LOW LIMIT
    xlWb.Sheets("Sheet1").Cells(X, 5).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X, 5).Value = lsvTariffSched.ListItems(listcounter).ListSubItems(4).Text 'HIGH LIMIT
    xlWb.Sheets("Sheet1").Cells(X, 6).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X, 6).Value = lsvTariffSched.ListItems(listcounter).ListSubItems(5).Text 'BASEMOUNT
    xlWb.Sheets("Sheet1").Cells(X, 7).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X, 7).Value = lsvTariffSched.ListItems(listcounter).ListSubItems(6).Text 'RATE

Case 2 'generate new tariff layout
    xlWb.Sheets("Sheet1").Cells(X, 1).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X, 1).Value = lsvTariffSched.ListItems(listcounter).Text                 'TARIFF TYPE
    xlWb.Sheets("Sheet1").Cells(X, 2).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X, 2).Value = lsvTariffSched.ListItems(listcounter).ListSubItems(1).Text 'RECNO
    xlWb.Sheets("Sheet1").Cells(X, 3).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X, 3).Value = lsvTariffSched.ListItems(listcounter).ListSubItems(2).Text 'AREA TYPE
    xlWb.Sheets("Sheet1").Cells(X, 4).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X, 4).Value = lsvTariffSched.ListItems(listcounter).ListSubItems(3).Text 'LOW LIMIT
    xlWb.Sheets("Sheet1").Cells(X, 5).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X, 5).Value = lsvTariffSched.ListItems(listcounter).ListSubItems(4).Text 'HIGH LIMIT
    xlWb.Sheets("Sheet1").Cells(X, 6).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X, 6).Value = ""                                                         'BASEMOUNT
    xlWb.Sheets("Sheet1").Cells(X, 7).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X, 7).Value = ""                                                         'RATE

Case 3 'tariff for DS
    xlWb.Sheets("Sheet1").Cells(X - 1, 1).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X - 1, 1).Value = lsvTariffSched.ListItems(listcounter).ListSubItems(1).Text                'TARIFF TYPE
    xlWb.Sheets("Sheet1").Cells(X - 1, 2).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X - 1, 2).Value = Mid(lsvTariffSched.ListItems(listcounter).Text, 1, 1)                     'RECNO
    xlWb.Sheets("Sheet1").Cells(X - 1, 3).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X - 1, 3).Value = Mid(lsvTariffSched.ListItems(listcounter).ListSubItems(2).Text, 1, 1)     'AREA TYPE
    xlWb.Sheets("Sheet1").Cells(X - 1, 4).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X - 1, 4).Value = lsvTariffSched.ListItems(listcounter).ListSubItems(3).Text                'LOW LIMIT
    xlWb.Sheets("Sheet1").Cells(X - 1, 5).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X - 1, 5).Value = lsvTariffSched.ListItems(listcounter).ListSubItems(4).Text                'HIGH LIMIT
    xlWb.Sheets("Sheet1").Cells(X - 1, 6).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X - 1, 6).Value = lsvTariffSched.ListItems(listcounter).ListSubItems(5).Text                'BASEMOUNT
    xlWb.Sheets("Sheet1").Cells(X - 1, 7).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X - 1, 7).Value = lsvTariffSched.ListItems(listcounter).ListSubItems(6).Text                'RATE
End Select

X = X + 1
Next

nConstype = Mid(Gstariff, 5, Len(Gstariff))

Select Case genSelector
    Case 1 'backup original tariff
        If Gstariff = "ALL" And GsAreatype = "ALL" Then
            nFilename = "original_" & "Consumer_Type_ALL" & "_" & "Area_Type_ALL" & ".xls"
        Else
            'nFilename = "original_" & Replace(Replace(Gstariff, " - ", "_"), " ", "_") & "_" & GsAreatype & ".xls"
            nFilename = "original_" & nConstype & "_" & GsAreatype & ".xls"
   
        End If
        
        xlWb.Close True, dir & nFilename
        Set xlWb = Nothing
        xlApp.Quit
        Set xlApp = Nothing
    GenerateFile = True
        
    Case 2 'generate new tariff layout
        If Gstariff = "ALL" And GsAreatype = "ALL" Then
            nFilename = "new_" & "Consumer_ALL" & "_" & "Area_ALL" & ".xls"
        Else
            nFilename = "new_" & nConstype & "_" & GsAreatype & ".xls"
        End If
        
        xlWb.Close True, dir & nFilename
        Set xlWb = Nothing
        xlApp.Quit
        Set xlApp = Nothing
    GenerateFile = True
        
    Case 3 'tariff for DS
        nFilename = "TARIFF" & ".xls"
        xlWb.Close True, dir & nFilename
        Set xlWb = Nothing
        xlApp.Quit
        Set xlApp = Nothing
    GenerateFile = True

End Select

errhandler:
    If Err.Number <> 0 Then                 'Added due to Error 1004
        xlWb.Close False
        xlApp.Quit
        Set xlApp = Nothing
    End If
   
End Function

Public Sub load_AreaType(Optional ByVal sAreatype$ = "ALL")
Dim sql1 As String
Dim RecAreaType As Recordset
       
On Error GoTo errhandler

With cmbAreaType
    .Clear
    .AddItem "ALL"

    sql1 = "select distinct(a.Area_Type_Desc) from dbo.R_AREA_TYPE a " & _
       "inner join dbo.R_TARIFF b on a.area_type = B.area_type"
       
    If OpenRecordset(RecAreaType, sql1, "frmTariffSched", "load_AreaType") Then
        While Not RecAreaType.EOF
            .AddItem RecAreaType.Fields(0)
            RecAreaType.MoveNext
        Wend
    End If
    
    .ListIndex = 0
End With
        
errhandler:
    If Err.Number <> 0 Then
        If Err.Number = 380 Then
        Else
            MsgBox Err.Description, , "System Message"
        End If
    End If
End Sub

