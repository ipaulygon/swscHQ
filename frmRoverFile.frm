VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmRoverFile 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Rover ID Table"
   ClientHeight    =   4455
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4935
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4455
   ScaleWidth      =   4935
   Begin VB.CommandButton cmdOk 
      Cancel          =   -1  'True
      Caption         =   "&Close"
      Height          =   390
      Left            =   3315
      TabIndex        =   4
      Top             =   3825
      Width           =   915
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "&Delete"
      Enabled         =   0   'False
      Height          =   390
      Left            =   2400
      TabIndex        =   3
      Top             =   3825
      Width           =   915
   End
   Begin VB.CommandButton cmdEdit 
      Caption         =   "&Edit"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   390
      Left            =   1485
      TabIndex        =   2
      Top             =   3825
      Width           =   915
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "&Add"
      Height          =   390
      Left            =   570
      TabIndex        =   1
      Top             =   3825
      Width           =   915
   End
   Begin MSComctlLib.ListView lsvRoverFile 
      Height          =   3525
      Left            =   60
      TabIndex        =   0
      Top             =   70
      Width           =   4800
      _ExtentX        =   8467
      _ExtentY        =   6218
      View            =   3
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Rover No"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Rover ID"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Date Acquired"
         Object.Width           =   2646
      EndProperty
   End
End
Attribute VB_Name = "frmRoverFile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAdd_Click()
    g_str_AddEdit = "ADD"
    frmAddRoverFile.Show 1
    
End Sub

Private Sub cmdDelete_Click()
    Dim l_str_Sql As String
    
    If Not ListViewDelete(lsvRoverFile) Then Exit Sub
    
    Select Case g_str_Table
        Case "R_FF"
            l_str_Sql = "DELETE FROM R_FF WHERE FFCODE = '" & lsvRoverFile.SelectedItem.Text & "'"
        
        Case "R_READER"
            l_str_Sql = "DELETE FROM R_READER WHERE READERNO = " & lsvRoverFile.SelectedItem.Text
    
        Case "R_HH"
            l_str_Sql = "DELETE FROM R_HH WHERE HHNO = " & lsvRoverFile.SelectedItem.Text
    
    End Select
    
    If Not DBExecute(l_str_Sql, Me.Name, "cmdDelete_Click") Then Exit Sub
    
    lsvRoverFile.ListItems.Remove (lsvRoverFile.SelectedItem.Index)
    lsvRoverFile.Refresh
    lsvRoverFile.SetFocus
    
End Sub

Private Sub cmdOk_Click()
    Unload Me
    
End Sub

Private Sub cmdEdit_Click()
    g_str_AddEdit = "EDIT"
    frmAddRoverFile.Show 1
        
End Sub

Private Sub Form_Activate()
    lsvRoverFile.Refresh
    lsvRoverFile.SetFocus
    
End Sub

Private Sub Form_Load()
    ListView_Setup
    ListView_Load
    
End Sub

Private Sub ListView_Setup()
    Select Case g_str_Table
        Case "R_FF"
            Me.Caption = "Field Finding Table"
            lsvRoverFile.ColumnHeaders.Item(1).Text = "Code"
            lsvRoverFile.ColumnHeaders.Item(2).Text = "Description"
            lsvRoverFile.ColumnHeaders.Item(2).Width = 3000
            lsvRoverFile.ColumnHeaders.Item(3).Width = 0
        
        Case "R_READER"
            Me.Caption = "Meter Reader Table"
            lsvRoverFile.ColumnHeaders.Item(1).Text = "Reader No."
            lsvRoverFile.ColumnHeaders.Item(2).Text = "Meter Reader Name"
            lsvRoverFile.ColumnHeaders.Item(2).Width = 3000
            lsvRoverFile.ColumnHeaders.Item(3).Width = 0
    
        Case "R_HH"
            Me.Caption = "Rover ID Table"
            lsvRoverFile.ColumnHeaders.Item(1).Text = "Rover No."
            lsvRoverFile.ColumnHeaders.Item(2).Text = "Rover ID"
            lsvRoverFile.ColumnHeaders.Item(3).Text = "Date Acquired"
            lsvRoverFile.ColumnHeaders.Item(2).Width = 1500
            lsvRoverFile.ColumnHeaders.Item(3).Width = 1500
        
    End Select

End Sub

Private Sub ListView_Load()
    Dim lobjItem As Object
    
    lsvRoverFile.ListItems.Clear
    If Not OpenRecordset(g_rs_RFF, "SELECT * FROM " & g_str_Table, Me.Name, "ListView_Load") Then Exit Sub
    
    g_rs_RFF.MoveFirst
    While Not g_rs_RFF.EOF
        If g_str_Table = "R_HH" Then
            Set lobjItem = lsvRoverFile.ListItems.Add(, , CheckNull(g_rs_RFF.Fields(0)))
                lobjItem.SubItems(1) = CheckNull(g_rs_RFF.Fields(2))
                lobjItem.SubItems(2) = Format(CheckNull(g_rs_RFF.Fields(1)), "mm/dd/yyyy")
        Else
            Set lobjItem = lsvRoverFile.ListItems.Add(, , CheckNull(g_rs_RFF.Fields(0)))
                lobjItem.SubItems(1) = CheckNull(g_rs_RFF.Fields(1))
        End If
        
        Set lobjItem = Nothing
        g_rs_RFF.MoveNext
    Wend

    g_rs_RFF.Close
    Set g_rs_RFF = Nothing
    
    If lsvRoverFile.ListItems.Count > 0 Then
        cmdEdit.Enabled = True
        cmdDelete.Enabled = True
    Else
        cmdEdit.Enabled = False
        cmdDelete.Enabled = False
    End If
    
End Sub

Private Sub lsvRoverFile_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ListviewSort lsvRoverFile, ColumnHeader.Index

End Sub
