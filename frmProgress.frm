VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmProgress 
   Caption         =   "Progress"
   ClientHeight    =   1530
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6885
   LinkTopic       =   "Form1"
   ScaleHeight     =   1530
   ScaleWidth      =   6885
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ProgressBar prgDownload 
      Height          =   375
      Left            =   240
      TabIndex        =   0
      Top             =   840
      Width           =   6375
      _ExtentX        =   11245
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.Label Label1 
      Caption         =   "Downloading...."
      Height          =   375
      Left            =   240
      TabIndex        =   1
      Top             =   120
      Width           =   4095
   End
End
Attribute VB_Name = "frmprogress"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Activate()
    While prgDownload.Value < prgDownload.Max
        prgDownload.Value = prgDownload.Value + 1
    Wend
End Sub

