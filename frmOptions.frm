VERSION 5.00
Begin VB.Form frmOptions 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Data Transfer Options"
   ClientHeight    =   2220
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4335
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2220
   ScaleWidth      =   4335
   Begin VB.Frame Frame3 
      BorderStyle     =   0  'None
      Height          =   300
      Left            =   2385
      TabIndex        =   8
      Top             =   915
      Width           =   1905
      Begin VB.OptionButton optOff3 
         Caption         =   "Off"
         Height          =   315
         Left            =   960
         TabIndex        =   10
         Top             =   0
         Width           =   765
      End
      Begin VB.OptionButton optOn3 
         Caption         =   "On"
         Height          =   315
         Left            =   0
         TabIndex        =   9
         Top             =   0
         Value           =   -1  'True
         Width           =   765
      End
   End
   Begin VB.Frame Frame2 
      BorderStyle     =   0  'None
      Height          =   330
      Left            =   2385
      TabIndex        =   5
      Top             =   525
      Width           =   1935
      Begin VB.OptionButton optOff2 
         Caption         =   "Off"
         Height          =   315
         Left            =   960
         TabIndex        =   7
         Top             =   30
         Width           =   765
      End
      Begin VB.OptionButton optOn2 
         Caption         =   "On"
         Height          =   315
         Left            =   15
         TabIndex        =   6
         Top             =   0
         Value           =   -1  'True
         Width           =   765
      End
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   2385
      TabIndex        =   2
      Top             =   180
      Width           =   1935
      Begin VB.OptionButton optOff1 
         Caption         =   "Off"
         Height          =   315
         Left            =   960
         TabIndex        =   4
         Top             =   0
         Width           =   765
      End
      Begin VB.OptionButton optOn1 
         Caption         =   "On"
         Height          =   315
         Left            =   30
         TabIndex        =   3
         Top             =   -15
         Value           =   -1  'True
         Width           =   765
      End
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   390
      Left            =   2340
      TabIndex        =   1
      Top             =   1545
      Width           =   915
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&OK"
      Height          =   390
      Left            =   1215
      TabIndex        =   0
      Top             =   1545
      Width           =   915
   End
   Begin VB.Label lblAutoDnldTables 
      Caption         =   "Auto Download Tables:"
      Height          =   315
      Left            =   255
      TabIndex        =   13
      Top             =   570
      Width           =   1845
   End
   Begin VB.Label lblPrompt 
      Caption         =   "Prompt Before Download:"
      Height          =   315
      Left            =   255
      TabIndex        =   12
      Top             =   915
      Width           =   1845
   End
   Begin VB.Label lblAutoDnldProg 
      Caption         =   "Auto Download Program:"
      Height          =   225
      Left            =   240
      TabIndex        =   11
      Top             =   240
      Width           =   1845
   End
End
Attribute VB_Name = "frmOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOk_Click()
    Unload Me
End Sub
