VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmErrorLogs 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Error Log"
   ClientHeight    =   7755
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11025
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7755
   ScaleWidth      =   11025
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   315
      Left            =   6360
      TabIndex        =   4
      Top             =   360
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   556
      _Version        =   393216
      Format          =   69206017
      CurrentDate     =   40380
   End
   Begin VB.ComboBox cmbProcess 
      Height          =   315
      ItemData        =   "frmErrorLogs.frx":0000
      Left            =   480
      List            =   "frmErrorLogs.frx":0010
      TabIndex        =   3
      Text            =   "Combo1"
      Top             =   360
      Width           =   2535
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Print to File"
      Height          =   495
      Left            =   4320
      TabIndex        =   2
      Top             =   7200
      Width           =   1935
   End
   Begin VB.Frame Frame1 
      Height          =   6375
      Left            =   120
      TabIndex        =   0
      Top             =   720
      Width           =   10815
      Begin VB.TextBox txtWindow 
         Height          =   6015
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   1
         Top             =   240
         Width           =   10575
      End
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   315
      Left            =   8640
      TabIndex        =   5
      Top             =   360
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   556
      _Version        =   393216
      Format          =   69206017
      CurrentDate     =   40380
   End
   Begin VB.Label Label2 
      Caption         =   "Date Range"
      Height          =   255
      Left            =   6360
      TabIndex        =   7
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Process"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "frmErrorLogs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim DateFrom As Date
Dim DateTo As Date
Dim Process As String
Dim SQLstring As String
Dim rsDB As ADODB.Recordset



Private Sub cmbProcess_Click()
    Process = cmbProcess.Text
    DisplayLogs
End Sub



Private Sub DTPFrom_Change()
    DateFrom = dtpFrom.Value
    DisplayLogs
End Sub

Private Sub DTPTo_Change()
    DateTo = dtpTo.Value
    DisplayLogs
End Sub



Private Sub Form_Load()

If WindowState = vbNormal Then
    Me.Move (0)
    Me.Top = 0
    Me.Height = 8325
    Me.Width = 11145
End If
    cmbProcess.Text = "ALL"
    Process = cmbProcess.Text
    DateTo = dtpTo.Value
    DateFrom = dtpFrom.Value

DisplayLogs
End Sub



Private Sub DisplayLogs()
txtWindow.Text = ""
If Process <> "ALL" Then
SQLstring = "SELECT B.PROCESSDESC,A.MRU,A.ACCTNUM,A.FILENAMEPATH,A.ERRORDESC,A.TRANSDATE FROM T_ERROR_LOG A INNER JOIN R_PROCESS B ON A.PROCESSID = B.PROCESSID WHERE B.PROCESSDESC LIKE '%" & Process & "%' AND A.TRANSDATE >= '" & DateFrom & "' AND A.TRANSDATE <= '" & DateAdd("d", 1, DateTo) & "'"
Else
SQLstring = "SELECT B.PROCESSDESC,A.MRU,A.ACCTNUM,A.FILENAMEPATH,A.ERRORDESC,A.TRANSDATE FROM T_ERROR_LOG A INNER JOIN R_PROCESS B ON A.PROCESSID = B.PROCESSID WHERE A.TRANSDATE > ='" & DateFrom & "' AND A.TRANSDATE <= '" & DateAdd("d", 1, DateTo) & "'"
End If

If OpenRecordset(rsDB, SQLstring, "", "") Then
rsDB.MoveFirst
Do While Not rsDB.EOF

    txtWindow.Text = txtWindow.Text & rsDB.Fields("PROCESSDESC").Value & vbTab & rsDB.Fields("MRU").Value & vbTab & rsDB.Fields("ACCTNUM").Value & vbTab & rsDB.Fields("FILENAMEPATH") & vbTab & rsDB.Fields("ERRORDESC").Value & vbTab & rsDB.Fields("TRANSDATE").Value & vbCrLf
    rsDB.MoveNext
Loop
End If

End Sub
