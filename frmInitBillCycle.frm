VERSION 5.00
Begin VB.Form frmInitBillCycle 
   Caption         =   "Initialize Bill Cycle"
   ClientHeight    =   2340
   ClientLeft      =   2325
   ClientTop       =   2610
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   2340
   ScaleWidth      =   4680
   Begin VB.CommandButton cmdInitialize 
      Caption         =   "&Initialize"
      Height          =   390
      Left            =   1920
      TabIndex        =   2
      Top             =   1800
      Width           =   975
   End
   Begin VB.ComboBox comboMonth 
      Height          =   315
      ItemData        =   "frmInitBillCycle.frx":0000
      Left            =   360
      List            =   "frmInitBillCycle.frx":0002
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   720
      Width           =   1815
   End
   Begin VB.Frame FrameBillCycle 
      Caption         =   "Specify Billing Month"
      Height          =   1335
      Left            =   120
      TabIndex        =   1
      Top             =   240
      Width           =   4455
      Begin VB.OptionButton OptionNextYear 
         Height          =   255
         Left            =   2520
         TabIndex        =   4
         Top             =   720
         Width           =   975
      End
      Begin VB.OptionButton OptionCurrYear 
         Height          =   255
         Left            =   2520
         TabIndex        =   3
         Top             =   360
         Width           =   975
      End
   End
End
Attribute VB_Name = "frmInitBillCycle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim curryear As Integer
Dim currmonth As Integer

Private Function Archive_Files(ByVal billmo As String) As Boolean

    Dim fso As Object, f As Object
   ' Dim fld_dn, f_dn
    Dim fld_up, f_up
    
    Dim dnldpath As String, upldpath As String
    Dim dnld_dest As String, upld_dest As String
    Dim dnld_current, upld_current As String
    Dim rsValid As Boolean
    Dim bm As String, iYr As Integer
    Dim prevBM As String
    
    ' Get host download and upload folders in S_PARAM (Settings)
    rsValid = OpenRecordset(g_rs_SPARAM, "SELECT DIRDNLD, DIRUPLD FROM S_PARAM", "frmInitBillCycle", "Archive_Files")
    If rsValid = False Then
        Archive_Files = False
        Exit Function
    Else
        g_rs_SPARAM.MoveFirst
        dnldpath = g_rs_SPARAM.Fields(0) & ""
        upldpath = g_rs_SPARAM.Fields(1) & ""
    End If
    
    Set fso = CreateObject("Scripting.FileSystemObject")
    
    ' First time run, check for "current" folder in download path
    '  if already existing, move it to the archive bill month
    dnld_current = dnldpath & "\current"
    If fso.FolderExists(dnld_current) Then
        dnld_dest = dnldpath & "\" & billmo
        fso.MoveFolder dnld_current, dnld_dest
    End If
    ' Create an empty "current" folder
    fso.CreateFolder (dnld_current)
    
'    ' Upload Archive Folder
'    upld_dest = upldpath & "\" & billmo
'    If Not fso.FolderExists(upld_dest) Then
'        fso.CreateFolder (upld_dest)
'    End If
'
'    ' Host Upload folder
'    Set fld_up = fso.GetFolder(upldpath)
'    Set f_up = fld_up.Files
'    For Each f2 In f_up
'        f2.Move upld_dest & "\" & f2.Name
'    Next

    ' Jasper 20080919 - Modify behavior of archiving uploaded files
    ' Set current folder
    upld_current = upldpath & "\current"
    If fso.FolderExists(upld_current) Then
        bm = Right(billmo, 2)                       ' current cycle
        iYr = Val(Left(billmo, 4))                  ' current year
        Set fld_up = fso.GetFolder(upld_current)    ' root folder: \<UPLOADDIR>\current
        ' Navigate through all files in all subfolders and move those upload files
        ' that do not match the current cycle month to their corresponding cycle
        For Each fldr In fld_up.SubFolders      ' subfolders: \<UPLOADDIR>\current\<day#>
            Set f_up = fldr.Files               ' the files in each <day#> subfolder in \<UPLOADDIR>\current
            ' Match cycle period of upload file to current, if unmatched,
            ' move the file to the corresponding PREVIOUS cycle month folder
            ' (assume it already exists!)
            For Each f2 In f_up
                If Left(f2.Name, 2) <> bm Then
                    ' special case, current month is January, last cycle is December of PREVIOUS year!
                    If bm = "01" Then
                        newBM = str$(iYr - 1) & "12"
                    Else
                        newBM = Left(billmo, 4) & Format$(Val(bm - 1), "0#")
                    End If
                    If fso.FolderExists(upldpath & "\" & Trim(newBM)) = False Then fso.CreateFolder (upldpath & "\" & Trim(newBM))
                    If fso.FolderExists(upldpath & "\" & Trim(newBM) & "\" & f2.Name) = False Then f2.Move upldpath & "\" & Trim(newBM) & "\" & f2.Name
                End If
            Next
        Next
        
        ' Move/rename current to the billmonth and recreate a new "blank" current folder!
        upld_dest = upldpath & "\" & billmo
        fso.MoveFolder upld_current, upld_dest
    End If
    
    fso.CreateFolder (upld_current)
    ' End Jasper 20080919

    ' Clean up
    g_rs_SPARAM.Close
    Set g_rs_SPARAM = Nothing
    
    Archive_Files = True
End Function

Private Sub Load_Months()

    comboMonth.AddItem "January"
    comboMonth.AddItem "February"
    comboMonth.AddItem "March"
    comboMonth.AddItem "April"
    comboMonth.AddItem "May"
    comboMonth.AddItem "June"
    comboMonth.AddItem "July"
    comboMonth.AddItem "August"
    comboMonth.AddItem "September"
    comboMonth.AddItem "October"
    comboMonth.AddItem "November"
    comboMonth.AddItem "December"
    
End Sub



Private Sub cmdInitialize_Click()
    Dim l_str_Sql As String
    Dim delkey As Integer
    Dim lsql As String
    Dim OldBillMonth As String
    Dim mon$, yr$
    
    OpenRecordset g_rs_SPARAM, "SELECT DIRDNLD, DIRUPLD FROM S_PARAM where COMPANY_NAME='SUBIC WATER'", "frmInitBillCycle", "cmdInitialize"

    If g_rs_SPARAM.Fields(0) = "" Or g_rs_SPARAM.Fields(1) = "" Then
        MsgBox "Please input values for download and upload directories first.", vbExclamation, "Initialize Error"
        Unload frmInitBillCycle
        Exit Sub
    End If
    
    mon$ = Padl(comboMonth.ListIndex + 1, 2, "0")
    
    If OptionCurrYear.Value Then
        yr$ = OptionCurrYear.Caption
    Else
        yr$ = OptionNextYear.Caption
    End If
  
    'On Error GoTo errhandler

    delkey = MsgBox("Are you sure?", vbOKCancel, "Initialize Bill Cycle")
    If delkey = vbOK Then
    
        ' Move writing new bill month to INI file INSIDE the confirm block
        OldBillMonth = g_BillMonth
        g_BillMonth = Trim(yr$ & mon$)
        writeINI App.Path & "\cfshq.ini", "Settings", "CurrentBillPeriod", g_BillMonth
        

        If Not (Archive_Files(OldBillMonth)) Then
            MsgBox "Could not archive last bill cycle's files!", vbCritical, "Initialize Bill Cycle"
            Unload frmInitBillCycle
            Exit Sub
        Else
            MsgBox "Bill cycle successfully initialized.", , "Initialize Bill Cycle"
        End If


    End If
    
    Unload frmInitBillCycle
    Exit Sub

errhandler:
    If Err.Number = 3049 Then  'compactdb error
        Unload frmInitBillCycle
        MsgBox "Bill cycle successfully initialized. The system needs to close, please reopen afterward.", vbExclamation, "Initialize Bill Cycle"
        End
    ElseIf Err.Number <> 0 Then
        Unload frmInitBillCycle
        MsgBox Err.Description
    End If

End Sub

Private Sub Form_Load()
    Load_Months
    
    curryear = Year(Date)
    currmonth = Month(Date)
    
    ' Initialize based on current date
    OptionCurrYear.Caption = str(curryear)
    OptionCurrYear.Value = True
    OptionNextYear.Caption = str(curryear + 1)
    comboMonth.ListIndex = currmonth - 1
        
End Sub

