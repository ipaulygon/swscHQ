VERSION 5.00
Begin VB.Form frmFileSource 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Download File Source"
   ClientHeight    =   3630
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3915
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3630
   ScaleWidth      =   3915
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   390
      Left            =   2040
      TabIndex        =   2
      Top             =   3105
      Width           =   915
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&OK"
      Default         =   -1  'True
      Height          =   390
      Left            =   1020
      TabIndex        =   1
      Top             =   3090
      Width           =   915
   End
   Begin VB.DirListBox dirSource 
      Height          =   2340
      Left            =   120
      TabIndex        =   0
      Top             =   580
      Width           =   3670
   End
   Begin VB.DriveListBox drvSource 
      Height          =   315
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   3670
   End
End
Attribute VB_Name = "frmFileSource"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOk_Click()
    If Me.Caption = "Download File Source" Then
        frmSettings.txtDSource = dirSource
    Else
        frmSettings.txtUDest = dirSource
    End If
    
    Unload Me

End Sub

Private Sub drvSource_Change()
    dirSource.Path = drvSource
End Sub

Private Sub Form_Activate()
If WindowState = vbNormal Then
    Me.Move (1000)
    Me.Top = 500
End If
    On Error GoTo DefaultSource
    If Me.Caption = "Download File Source" Then
        dirSource.Path = frmSettings.txtDSource
        drvSource.Drive = Trim(Left(frmSettings.txtDSource, 3))
    Else
        dirSource.Path = frmSettings.txtUDest
        drvSource.Drive = Trim(Left(frmSettings.txtUDest, 3))
    End If
    Exit Sub
    
DefaultSource:
    dirSource.Path = App.Path
    drvSource.Drive = Trim(Left(App.Path, 3))
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmSettings.Enabled = True
End Sub
