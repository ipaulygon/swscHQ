VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmMRUResponse 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3285
   ClientLeft      =   45
   ClientTop       =   360
   ClientWidth     =   5175
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3285
   ScaleWidth      =   5175
   Begin VB.TextBox txtMRU 
      Height          =   285
      Left            =   2400
      MaxLength       =   8
      TabIndex        =   7
      Top             =   1320
      Width           =   1935
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "&Close"
      Height          =   495
      Left            =   2760
      TabIndex        =   1
      Top             =   2520
      Width           =   1215
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save"
      Height          =   495
      Left            =   1440
      TabIndex        =   0
      Top             =   2520
      Width           =   1215
   End
   Begin MSComCtl2.DTPicker DTPSchedRdg 
      Height          =   375
      Left            =   2400
      TabIndex        =   6
      Top             =   1785
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   661
      _Version        =   393216
      Format          =   59899905
      CurrentDate     =   39582
   End
   Begin VB.Label lblSchedRdg 
      Caption         =   "Scheduled Reading Date : "
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   1800
      Width           =   2175
   End
   Begin VB.Label lblMRU 
      Caption         =   "MRU Number :"
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   1320
      Width           =   1815
   End
   Begin VB.Label lblCycle 
      Caption         =   "Cycle / Month : "
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   840
      Width           =   4335
   End
   Begin VB.Label lblBusCenter 
      Caption         =   "Business Center : "
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   360
      Width           =   4575
   End
End
Attribute VB_Name = "frmMRUResponse"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub cmdSave_Click()
Dim l_str_Sql As String
Dim MRUstat As String
Dim intCycle As Integer
Dim l_obj_Item As Object
Dim l_int_Index As Integer
Dim dl_date As Integer
Dim srdate As Date
Dim ans As Integer
Dim currdir, bcfc, bcmru, toso, frso, fcfrso, upfile As String
Dim fs
Dim BKStat As String

Dim inv(8) As String
Dim n As Integer
Dim BCDESC As String
Set g_rs_SPARAM = New ADODB.Recordset
'intCycle = Right(g_BillMonth, 2)
intCycle = intBillCycle

OpenRecordset g_rs_SPARAM, "select DL_TO_RDG_DIFF from S_PARAM where COMPANY_NAME = 'SUBIC WATER'", "frmMRUResponse", "MRU Response"
'number of days to be subtracted from sched_rdg_dt to get sched_dl_dt
dl_date = "-" & g_rs_SPARAM.Fields(0)



srdate = Padl(intBillCycle, 2, "0") & "-" & Padl(Trim(str(intDayNo)), 2, "0") & "-" & Left(g_BillMonth, 4)

If txtMRU.Text = "" Or IsNull(txtMRU.Text) Then
    MsgBox "MRU Number is a required field.", vbExclamation, "System Message"
    Exit Sub
End If

MRUstat = "SRD"
If g_str_AddEdit = "ADD" Then
    l_str_Sql = "SELECT BOOKNO FROM T_BOOK WHERE BOOKNO = '" & Trim(txtMRU.Text) & "' AND CYCLE = " & intCycle
    If OpenRecordset(g_rs_TBOOK, l_str_Sql, Me.Name, "cmdSave") Then
        MsgBox "MRU already scheduled for this cycle!", vbExclamation, "Add MRU Schedule"
        g_rs_TBOOK.Close
        Set g_rs_TBOOK = Nothing
        txtMRU.SetFocus
        Exit Sub
    End If
    
    'If OpenRecordset(g_rs_TBOOK, "select BC_CODE from R_BUSCTR where BC_CODE='" & Left(Trim(txtMRU.Text), 4) & "'", "frmMRUResponse", "MRU Response") Then
    ' JAPS 20070721 Change Left(Trim(txtMRU.Text), 4) to Left(sBusCenter, 4)
        l_str_Sql = "INSERT INTO T_BOOK(BOOKNO, CYCLE, DMZCODE_ID, SCHED_RDG_DT, SCHED_DL_DT)VALUES( '" & _
            Trim(txtMRU.Text) & "', " & intCycle & ",'" & Left(sBusCenter, 3) & "' , '" & CDate(DTPSchedRdg.Value) & "', '" & DateAdd("d", dl_date, CDate(DTPSchedRdg.Value)) & "')"
            If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub
        l_str_Sql = "INSERT INTO T_SCHED(BOOKNO, CYCLE, BKSTCODE, DAYNO)VALUES( '" & _
            Trim(txtMRU.Text) & "', " & intCycle & ",'" & MRUstat & "' , " & Day(CDate(DTPSchedRdg.Value)) & ")"
            If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub
            
            ' if insert fail, delete mru loaded to t_book
'            If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then
'                If Not DBExecute("DELETE FROM T_BOOK WHERE BOOKNO='" & Trim(txtMRU.Text) & "' AND  CYCLE= " & intCycle & " AND  BC_CODE='" & _
'                    Left(Trim(txtMRU.Text), 4) & "' AND SCHED_RDG_DT = '" & CDate(DTPSchedRdg.Value) & "' AND SCHED_DL_DT='" & DateAdd("d", dl_date, CDate(DTPSchedRdg.Value)) & "')", Me.Name, "cmdSave") Then Exit Sub
'                Exit Sub
'            End If
            
            ' set listview display
            'If Left(sBusCenter, 4) = Left(Trim(txtMRU.Text), 4) Then
                Set l_obj_Item = frmMRUMngt.lsvMRUMngt.ListItems.Add(, , Trim(txtMRU.Text))
                    l_obj_Item.SubItems(1) = "Scheduled Reading Date"
                    l_obj_Item.SubItems(2) = DTPSchedRdg.Value
                Set l_obj_Item = Nothing
                frmMRUMngt.cmdEdit.Enabled = True
                frmMRUMngt.cmdDelete.Enabled = True
            If Left(sBusCenter, 3) = Left(Trim(txtMRU.Text), 3) Then
                MsgBox "MRU Schedule saved successfully!", vbInformation, "Add MRU Schedule"
            Else
               ' MsgBox "MRU " & Trim(txtMRU.Text) & " has been successfully added to Business Center " & Left(Trim(txtMRU.Text), 4), vbInformation, "MRU Response"
                MsgBox "MRU " & Trim(txtMRU.Text) & " has been successfully added to Business Center " & Left(sBusCenter, 4), vbInformation, "MRU Response"
            End If
            'Unload Me
            Form_Load
            txtMRU.SetFocus
   ' Else
   '     MsgBox "Business Center does not exist", vbExclamation, "System Message"
  '  End If
'    If frmMRUMngt.lsvMRUMngt.ListItems.Count = 0 Then
'        frmMRUMngt.cmdEdit.Enabled = False
'        frmMRUMngt.cmdDelete.Enabled = False
'    End If
ElseIf g_str_AddEdit = "EDIT" Then
    l_str_Sql = "DELETE FROM T_SCHED WHERE BOOKNO = '" & frmMRUMngt.lsvMRUMngt.SelectedItem.Text & "' and CYCLE = " & intCycle & ""
        If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub
    l_str_Sql = "UPDATE T_BOOK SET BOOKNO = '" & Trim(txtMRU.Text) & "', SCHED_RDG_DT = '" & CDate(DTPSchedRdg.Value) & _
        "', SCHED_DL_DT = '" & DateAdd("d", dl_date, CDate(DTPSchedRdg.Value)) & "' WHERE BOOKNO = '" & frmMRUMngt.lsvMRUMngt.SelectedItem.Text & "' and CYCLE = " & intCycle & ""
        If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub
    l_str_Sql = "INSERT INTO T_SCHED(BOOKNO, CYCLE, BKSTCODE, DAYNO)VALUES( '" & _
        Trim(txtMRU.Text) & "', " & intCycle & ",'" & MRUstat & "' , " & Day(CDate(DTPSchedRdg.Value)) & ")"
        If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub
        

        
        ' set listview display
        If DTPSchedRdg.Value = srdate Then
            l_int_Index = frmMRUMngt.lsvMRUMngt.SelectedItem.Index
            frmMRUMngt.lsvMRUMngt.ListItems(l_int_Index).Text = Trim(txtMRU.Text)
            frmMRUMngt.lsvMRUMngt.ListItems(l_int_Index).SubItems(1) = "Scheduled Reading Date"
            frmMRUMngt.lsvMRUMngt.ListItems(l_int_Index).SubItems(2) = DTPSchedRdg.Value
        Else
            frmMRUMngt.lsvMRUMngt.ListItems.Remove (frmMRUMngt.lsvMRUMngt.SelectedItem.Index)
            frmMRUMngt.lsvMRUMngt.Refresh
        End If

        MsgBox "MRU Schedule modified.", vbInformation, "Edit MRU Schedule"
        Unload Me
ElseIf g_str_AddEdit = "DELETE" Then
    ans = MsgBox("Deleting a scheduled MRU will remove its related records and generated textfiles loaded for this cycle. " & vbCrLf & _
          "Are you sure you want to delete this MRU?", vbYesNo, "Confirm Delete MRU")
    If ans = 6 Then
        
        ' get download directory
        OpenRecordset g_rs_SPARAM, "SELECT DIRDNLD, DIRUPLD FROM S_PARAM where COMPANY_NAME='SUBIC WATER'", "frmMRUResponse", "cmdSave"
        ' get bc directory
        OpenRecordset g_rs_BUSCTR, "SELECT WORKING_DIR, DMZCODE_ID, DMZ_DESC FROM L_DMZCODE where DMZCODE_ID =(select DMZCODE_ID from T_BOOK WHERE BOOKNO = '" & Trim(txtMRU.Text) & "' and CYCLE = " & intCycle & ")", "frmMRUResponse", "cmdSave"
        ' get sched mr date
        OpenRecordset g_rs_TBOOK, "SELECT SCHED_RDG_DT, UPLD_SW_DT FROM T_BOOK WHERE BOOKNO = '" & Trim(txtMRU.Text) & "' and CYCLE = " & intCycle & "", "frmMRUResponse", "cmdSave"
        ' get status
        OpenRecordset g_rs_TSCHED, "SELECT BKSTCODE FROM T_SCHED WHERE BOOKNO = '" & Trim(txtMRU.Text) & "' and CYCLE = " & intCycle & " and DAYNO = " & intDayNo & "", "frmMRUResponse", "cmdSave"

'==================== invalid folder name
        ' invalid characters for folder name
        inv(0) = "\"
        inv(1) = "/"
        inv(2) = ":"
        inv(3) = "*"
        inv(4) = "?"
        inv(5) = """"
        inv(6) = "<"
        inv(7) = ">"
        inv(8) = "|"
        ' check for invalid folder name
        For n = 0 To 8
            If InStr(1, g_rs_BUSCTR.Fields(2).Value, inv(n), vbTextCompare) > 0 Then
                ' if first character is invalid, folder cannot be created and will exit sub
                If InStr(1, g_rs_BUSCTR.Fields(2).Value, inv(n), vbTextCompare) = 1 Then
                    MsgBox "A folder name cannot contain the character " & inv(n) & ". Please edit the business center name.", vbExclamation, "Invalid Folder Name"
                    Exit Sub
                ' folder name to be created will be chars before first occurrence of invalid character
                Else
                    'MsgBox "A folder name cannot contain the character " & inv(n) & ". Reports will be saved in folder " & Mid(g_rs_RBUSCTR.Fields(1).Value, 1, InStr(1, g_rs_RBUSCTR.Fields(1).Value, inv(n), vbTextCompare) - 1) & ".", vbExclamation, "Invalid Folder Name"
                    'BCDESC = Trim(Mid(g_rs_RBUSCTR.Fields(1).Value, 1, InStr(1, g_rs_RBUSCTR.Fields(1).Value, inv(n), vbTextCompare) - 1))
                    BCDESC = Replace(g_rs_BUSCTR.Fields(2).Value, inv(n), "")
                    Exit For
                End If
            Else
                BCDESC = Trim(g_rs_BUSCTR.Fields(2).Value)
            End If
        Next n
'=============== end
        
        BKStat = g_rs_TSCHED.Fields(0).Value
        'If frmMRUMngt.lsvMRUMngt.SelectedItem.SubItems(1) = "Scheduled Reading Date" Then
        If BKStat = "SRD" Then
            l_str_Sql = "DELETE FROM T_SCHED WHERE BOOKNO = '" & Trim(txtMRU.Text) & "' and CYCLE = " & intCycle & ""
                If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub
            l_str_Sql = "DELETE FROM T_BOOK WHERE BOOKNO = '" & Trim(txtMRU.Text) & "' and CYCLE = " & intCycle & ""
                If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub
                
            ' remove deleted row from listview display
            frmMRUMngt.lsvMRUMngt.ListItems.Remove (frmMRUMngt.lsvMRUMngt.SelectedItem.Index)
            MsgBox "MRU Schedule deleted.", vbInformation, "Delete MRU Schedule"
        'ElseIf frmMRUMngt.lsvMRUMngt.SelectedItem.SubItems(1) = "Downloaded from MWSI" Then
        ElseIf BKStat = "DLM" Then
            ' delete download file from \current folder if exists
            currdir = g_rs_SPARAM.Fields(0) & "\current\" & Trim(txtMRU.Text) & "D.txt"
            Set fs = CreateObject("Scripting.FileSystemObject")
            If fs.FileExists(currdir) Then fs.DeleteFile currdir

            l_str_Sql = "DELETE FROM T_DOWNLOAD WHERE SERIESNO = '" & Trim(txtMRU.Text) & "' and BILL_MONTH = " & intCycle & ""
                If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub
            l_str_Sql = "UPDATE T_SCHED SET BKSTCODE= 'SRD' WHERE BOOKNO = '" & Trim(txtMRU.Text) & "' and CYCLE = " & intCycle & ""
                If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub
            l_str_Sql = "DELETE FROM T_BKINFO WHERE BOOKNO = '" & Trim(txtMRU.Text) & "' and CYCLE = " & intCycle & ""
                If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub
            l_str_Sql = "UPDATE T_BOOK SET ACTUAL_DL_DT=null WHERE BOOKNO = '" & Trim(txtMRU.Text) & "' and CYCLE = " & intCycle & ""
                If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub
'            l_str_Sql = "DELETE FROM T_DLERROR WHERE MRU = '" & Trim(txtMRU.Text) & "' and CMONTH = " & intCycle & ""
'                If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub
                
            l_int_Index = frmMRUMngt.lsvMRUMngt.SelectedItem.Index
            frmMRUMngt.lsvMRUMngt.ListItems(l_int_Index).SubItems(1) = "Scheduled Reading Date"
            frmMRUMngt.lsvMRUMngt.ListItems(l_int_Index).SubItems(3) = ""
            MsgBox "Status for MRU " & Trim(txtMRU.Text) & " changed to Scheduled Reading Date.", vbInformation, "Delete MRU Schedule"
        'ElseIf frmMRUMngt.lsvMRUMngt.SelectedItem.SubItems(1) = "Sent to Satellite" Then
        ElseIf BKStat = "SSO" Then

            ' delete file from toso\bccode bcdesc\year\cycle monthname\sched mr day
            toso = g_rs_BUSCTR.Fields(0) & "\TOSO\" & g_rs_BUSCTR.Fields(1) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(0)) & "\" & Padl(intBillCycle, 2, "0") & " " & GetMonth(CInt(Padl(intBillCycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(0))) & "\" & Trim(txtMRU.Text) & ".txt"
            
            Set fs = CreateObject("Scripting.FileSystemObject")
            If fs.FileExists(toso) Then fs.DeleteFile toso

            l_str_Sql = "UPDATE T_SCHED SET BKSTCODE= 'DLM' WHERE BOOKNO = '" & Trim(txtMRU.Text) & "' and CYCLE = " & intCycle & ""
                If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub
            l_str_Sql = "UPDATE T_BOOK SET UPLD_SO_DT=null WHERE BOOKNO = '" & Trim(txtMRU.Text) & "' and CYCLE = " & intCycle & ""
                If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub
                
            l_int_Index = frmMRUMngt.lsvMRUMngt.SelectedItem.Index
            frmMRUMngt.lsvMRUMngt.ListItems(l_int_Index).SubItems(1) = "Downloaded from MWSI"
            frmMRUMngt.lsvMRUMngt.ListItems(l_int_Index).SubItems(4) = ""
            MsgBox "Status for MRU " & Trim(txtMRU.Text) & " changed to Downloaded from MWSI.", vbInformation, "Delete MRU Schedule"
        'ElseIf frmMRUMngt.lsvMRUMngt.SelectedItem.SubItems(1) = "Received from Satellite" Then
        ElseIf BKStat = "RSO" Or BKStat = "PAU" Then

            ' delete file from frso\bccode bcdesc\year\cycle monthname\sched mr day
            frso = g_rs_BUSCTR.Fields(0) & "\FRSO\" & g_rs_BUSCTR.Fields(1) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(0)) & "\" & Padl(intBillCycle, 2, "0") & " " & GetMonth(CInt(Padl(intBillCycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(0))) & "\" & "HQ" & Padl(intBillCycle, 2, "0") & Trim(txtMRU.Text) & ".txt"
            Set fs = CreateObject("Scripting.FileSystemObject")
            If fs.FileExists(frso) Then fs.DeleteFile frso

            ' delete fc file from frso\bccode bcdesc\year\cycle monthname\sched mr day
            fcfrso = g_rs_BUSCTR.Fields(0) & "\FRSO\" & g_rs_BUSCTR.Fields(1) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(0)) & "\" & Padl(intBillCycle, 2, "0") & " " & GetMonth(CInt(Padl(intBillCycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(0))) & "\" & "FC" & Padl(intBillCycle, 2, "0") & Trim(txtMRU.Text) & ".txt"
            Set fs = CreateObject("Scripting.FileSystemObject")
            If fs.FileExists(fcfrso) Then fs.DeleteFile fcfrso

            l_str_Sql = "DELETE FROM T_UPLOAD WHERE SERIESNO = '" & Trim(txtMRU.Text) & "' and BILL_MONTH = " & Padl(intCycle, 2, "0") & ""
                If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub
            l_str_Sql = "UPDATE T_SCHED SET BKSTCODE= 'SSO' WHERE BOOKNO = '" & Trim(txtMRU.Text) & "' and CYCLE = " & intCycle & ""
                If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub
            l_str_Sql = "UPDATE T_BKINFO SET UNREAD=ACCTS, READCNT=0, OORCNT=0 WHERE BOOKNO = '" & Trim(txtMRU.Text) & "' and CYCLE = " & intCycle & ""
                If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub
            l_str_Sql = "UPDATE T_BOOK SET RECV_SO_DT=null WHERE BOOKNO = '" & Trim(txtMRU.Text) & "' and CYCLE = " & intCycle & ""
                If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub
            l_str_Sql = "DELETE FROM T_FCONN WHERE BOOKNO = '" & Trim(txtMRU.Text) & "' and CYCLE = " & intCycle & ""
                If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub

                
            l_int_Index = frmMRUMngt.lsvMRUMngt.SelectedItem.Index
            frmMRUMngt.lsvMRUMngt.ListItems(l_int_Index).SubItems(1) = "Sent to Satellite"
            frmMRUMngt.lsvMRUMngt.ListItems(l_int_Index).SubItems(5) = ""
            MsgBox "Status for MRU " & Trim(txtMRU.Text) & " changed to Sent to Satellite.", vbInformation, "Delete MRU Schedule"
      
        ElseIf BKStat = "FWM" Then
            upfile = g_rs_SPARAM.Fields(1) & "\current\" & Format(DateValue(g_rs_TBOOK.Fields(1)), "dd") & "\" & Trim(txtMRU.Text) & ".txt"
            Set fs = CreateObject("Scripting.FileSystemObject")
            If fs.FileExists(upfile) Then fs.DeleteFile upfile

            l_str_Sql = "UPDATE T_SCHED SET BKSTCODE= 'RSO' WHERE BOOKNO = '" & Trim(txtMRU.Text) & "' and CYCLE = " & intCycle & ""
                If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub
            l_str_Sql = "UPDATE T_BOOK SET UPLD_SW_DT=null WHERE BOOKNO = '" & Trim(txtMRU.Text) & "' and CYCLE = " & intCycle & ""
                If Not DBExecute(l_str_Sql, Me.Name, "cmdSave") Then Exit Sub

            l_int_Index = frmMRUMngt.lsvMRUMngt.SelectedItem.Index
            frmMRUMngt.lsvMRUMngt.ListItems(l_int_Index).SubItems(1) = "Received from Satellite"
            frmMRUMngt.lsvMRUMngt.ListItems(l_int_Index).SubItems(6) = ""
            MsgBox "Status for MRU " & Trim(txtMRU.Text) & " changed to Received from Satellite.", vbInformation, "Delete MRU Schedule"
        Else
        End If
    
        frmMRUMngt.lsvMRUMngt.Refresh
    End If
    Unload Me


End If
        frmMRUMngt.cmdEdit.Enabled = False
        If frmMRUMngt.lsvMRUMngt.ListItems.Count = 0 Then
            frmMRUMngt.cmdDelete.Enabled = False
        End If
End Sub

Private Sub Form_Load()
    
On Error GoTo errhandler
    Select Case g_str_AddEdit
        Case "ADD"
            Me.Caption = "Add MRU Schedule"
            cmdSave.Caption = "&Save"
            txtMRU.Enabled = True
            DTPSchedRdg.Enabled = False
            txtMRU.Text = Left(sBusCenter, 3)
            'DTPSchedRdg.Value = Right(g_BillMonth, 2) & "-" & Trim(str(intDayNo)) & "-" & Left(g_BillMonth, 4)
            DTPSchedRdg.Value = Padl(intBillCycle, 2, "0") & "-" & Trim(str(intDayNo)) & "-" & Left(g_BillMonth, 4)
            'DTPSchedRdg.Value = Trim(str(intDayNo)) & "-" & Right(g_BillMonth, 2) & "-" & Left(g_BillMonth, 4)
        Case "EDIT"
            Me.Caption = "Edit MRU Schedule"
            cmdSave.Caption = "&Edit"
            txtMRU.Enabled = True
            txtMRU.Text = frmMRUMngt.lsvMRUMngt.SelectedItem
            DTPSchedRdg.Enabled = True
            DTPSchedRdg.Value = frmMRUMngt.lsvMRUMngt.SelectedItem.ListSubItems(2)
        Case "DELETE"
            Me.Caption = "Delete MRU Schedule"
            cmdSave.Caption = "&Delete"
            txtMRU.Enabled = False
            txtMRU.Text = frmMRUMngt.lsvMRUMngt.SelectedItem
            DTPSchedRdg.Enabled = False
            DTPSchedRdg.Value = frmMRUMngt.lsvMRUMngt.SelectedItem.ListSubItems(2)
    End Select
    
    lblBusCenter.Caption = "Business Center : " & sBusCenter
    'lblCycle.Caption = "Cycle/Month : " & Right(g_BillMonth, 2)
    lblCycle.Caption = "Cycle/Month : " & Padl(intBillCycle, 2, "0")
    
errhandler:
If Err.Number <> 0 Then
    MsgBox Err.Description, vbExclamation, "Error Message"
    cmdSave.Enabled = False
End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmMRUMngt.Enabled = True
End Sub
