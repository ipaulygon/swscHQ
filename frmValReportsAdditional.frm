VERSION 5.00
Object = "{3C62B3DD-12BE-4941-A787-EA25415DCD27}#10.0#0"; "crviewer.dll"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmValReportsAdditional 
   Caption         =   "Validation Reports - Additional"
   ClientHeight    =   10545
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15240
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   10545
   ScaleWidth      =   15240
   Begin VB.Frame Frame1 
      Caption         =   "Report Parameters"
      Height          =   855
      Left            =   120
      TabIndex        =   1
      Top             =   0
      Width           =   15015
      Begin VB.ComboBox cmbStatus 
         Height          =   315
         ItemData        =   "frmValReportsAdditional.frx":0000
         Left            =   14040
         List            =   "frmValReportsAdditional.frx":0013
         TabIndex        =   16
         Text            =   "ALL"
         Top             =   360
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.ComboBox cmbDays 
         Height          =   315
         ItemData        =   "frmValReportsAdditional.frx":004A
         Left            =   14040
         List            =   "frmValReportsAdditional.frx":0057
         TabIndex        =   13
         Top             =   720
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.ComboBox cmbReportType 
         Height          =   315
         ItemData        =   "frmValReportsAdditional.frx":006C
         Left            =   120
         List            =   "frmValReportsAdditional.frx":0079
         TabIndex        =   5
         Text            =   "ADDITIONAL"
         Top             =   600
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.CommandButton cmdGenerate 
         Caption         =   "&Generate"
         Height          =   495
         Left            =   11880
         TabIndex        =   4
         Top             =   240
         Width           =   1215
      End
      Begin VB.ComboBox cmbBillMonth 
         Height          =   315
         ItemData        =   "frmValReportsAdditional.frx":009C
         Left            =   1440
         List            =   "frmValReportsAdditional.frx":00C4
         TabIndex        =   3
         Top             =   360
         Width           =   1095
      End
      Begin VB.ComboBox cmbBusCen 
         Height          =   315
         Left            =   3480
         TabIndex        =   2
         Text            =   "ALL"
         Top             =   360
         Width           =   2295
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   315
         Left            =   9720
         TabIndex        =   9
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   16449537
         CurrentDate     =   39939
      End
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   315
         Left            =   7080
         TabIndex        =   11
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   16449537
         CurrentDate     =   39939
      End
      Begin VB.Label lblStatus 
         Caption         =   "Status"
         Height          =   255
         Left            =   13440
         TabIndex        =   15
         Top             =   360
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Label lbldays 
         Caption         =   "Days"
         Height          =   255
         Left            =   13440
         TabIndex        =   14
         Top             =   720
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Label Label5 
         Caption         =   "Date To"
         Height          =   255
         Left            =   8880
         TabIndex        =   12
         Top             =   360
         Width           =   975
      End
      Begin VB.Label Label3 
         Caption         =   "Date From"
         Height          =   255
         Left            =   6120
         TabIndex        =   10
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Report Type"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   360
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "Cycle"
         Height          =   255
         Left            =   840
         TabIndex        =   7
         Top             =   360
         Width           =   495
      End
      Begin VB.Label Label4 
         Caption         =   "BC"
         Height          =   255
         Left            =   3000
         TabIndex        =   6
         Top             =   360
         Width           =   615
      End
   End
   Begin CrystalActiveXReportViewerLib10Ctl.CrystalActiveXReportViewer CRViewer 
      Height          =   9495
      Left            =   120
      TabIndex        =   0
      Top             =   960
      Width           =   15015
      lastProp        =   600
      _cx             =   26485
      _cy             =   16748
      DisplayGroupTree=   0   'False
      DisplayToolbar  =   -1  'True
      EnableGroupTree =   0   'False
      EnableNavigationControls=   -1  'True
      EnableStopButton=   -1  'True
      EnablePrintButton=   -1  'True
      EnableZoomControl=   -1  'True
      EnableCloseButton=   -1  'True
      EnableProgressControl=   -1  'True
      EnableSearchControl=   -1  'True
      EnableRefreshButton=   0   'False
      EnableDrillDown =   -1  'True
      EnableAnimationControl=   -1  'True
      EnableSelectExpertButton=   0   'False
      EnableToolbar   =   -1  'True
      DisplayBorder   =   -1  'True
      DisplayTabs     =   -1  'True
      DisplayBackgroundEdge=   -1  'True
      SelectionFormula=   ""
      EnablePopupMenu =   -1  'True
      EnableExportButton=   0   'False
      EnableSearchExpertButton=   0   'False
      EnableHelpButton=   0   'False
      LaunchHTTPHyperlinksInNewBrowser=   -1  'True
      EnableLogonPrompts=   -1  'True
   End
End
Attribute VB_Name = "frmValReportsAdditional"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ReportType As String
Dim BC_Code As String
Dim cycle As Integer
Dim date_to As Date
Dim date_from As Date
Dim numCount As Integer
Dim Status As String




Private Sub cmbBillMonth_Click()
Dim lastday As Integer
Dim curdate As String
Dim curdate1 As String
DTPicker1.Refresh
DTPicker2.Refresh
curdate = cmbBillMonth.Text & "/1" & "/" & Year(Now)

'DTPicker1.Value = CDate(curdate)
DTPicker2.Value = CDate(curdate)


'DTPicker2.Value = DateAdd("d", -(Day(dtpicker1.value) - 1), DTPicker1.Value)
curdate1 = cmbBillMonth & "/" & getNumberOfDays(CDate(DTPicker2.Value)) & "/" & CStr(Year(Now))

DTPicker1.Value = CDate(curdate1)



End Sub

'Get the number of days each month is having
Public Function getNumberOfDays(date1 As Date) As String
    Select Case DateTime.Month(date1)
        Case 1, 3, 5, 7, 8, 10, 12
            getNumberOfDays = 31
        Case 4, 6, 9, 11
            getNumberOfDays = 30
        Case 2
            'logic for checking leap years
            If (Year(date1) Mod 4) = 0 Then
                If (Year(date1) Mod 100) = 0 Then
                    If (Year(date1) Mod 400) = 0 Then
                        getNumberOfDays = 29
                    Else
                        getNumberOfDays = 28
                    End If
                Else
                    getNumberOfDays = 29
                End If
            Else
                getNumberOfDays = 28
            End If
    End Select
End Function


Private Sub cmbBusCen_Click()
If cmbBusCen.Text <> "ALL" Then
BC_Code = Mid(cmbBusCen.Text, 1, 4)
Else
BC_Code = "ALL"
End If

End Sub

Private Sub cmbReportType_Click()
If cmbReportType.Text = "DETAILED" Then
lbldays.Visible = True
cmbDays.Visible = True
lblStatus.Visible = True
cmbStatus.Visible = True
ReportType = "DETAILED"
ElseIf cmbReportType.Text = "ADDITIONAL" Then
lbldays.Visible = False
cmbDays.Visible = False
lblStatus.Visible = False
cmbStatus.Visible = False
ReportType = "ADDITIONAL"
Else
lbldays.Visible = False
cmbDays.Visible = False
lblStatus.Visible = False
cmbStatus.Visible = False
ReportType = "SUMMARY"
End If

End Sub

Private Sub cmbStatus_Click()
If cmbStatus.Text = "FORCED CLOSED" Then
    cmbDays.Visible = True
    lbldays.Visible = True
Else
    cmbDays.Visible = False
    cmbDays.Visible = False
End If

End Sub

Private Sub cmdGenerate_Click()
    Dim i As Integer
    Dim sCond As String
    
    GetParameters
    If ReportType = "" Then
    MsgBox "Please select report type!"
    Exit Sub
    End If
    
    Set Appn = CreateObject("CrystalRunTime.Application")
    

    If ReportType = "SUMMARY" Then

    Set cReport = Appn.OpenReport(App.Path & "\crystal reports\" & g_str_rep)
    
    cReport.Database.LogOnServer "p2ssql.dll", "SZ-MWSI-SUBIC", "MCFSDB", "sa", "sqladmin"
    For i = 1 To cReport.Database.Tables.Count
        cReport.Database.Tables(i).ConnectionProperties.Item("Password") = "sqladmin"
    Next i
   
    cReport.DiscardSavedData
    ' Common Parameters
    cReport.ParameterFields(1).AddCurrentValue BC_Code
    cReport.ParameterFields(2).AddCurrentValue cycle
    cReport.ParameterFields(3).AddCurrentValue date_from
    cReport.ParameterFields(4).AddCurrentValue date_to
    CRViewer.ReportSource = cReport
    CRViewer.EnableExportButton = True
    CRViewer.ViewReport
    End If
    
    If ReportType = "DETAILED" Then
    
    Set cReport = Appn.OpenReport(App.Path & "\crystal reports\" & g_str_rep)
    
    cReport.Database.LogOnServer "p2ssql.dll", "SZ-MWSI-SUBIC", "MCFSDB", "sa", "sqladmin"
    For i = 1 To cReport.Database.Tables.Count
        cReport.Database.Tables(i).ConnectionProperties.Item("Password") = "sqladmin"
    Next i
    
    If Status = "PENDING" Then numCount = 2
    cReport.DiscardSavedData
    ' Common Parameters
    cReport.ParameterFields(1).AddCurrentValue BC_Code
    cReport.ParameterFields(2).AddCurrentValue cycle
    cReport.ParameterFields(3).AddCurrentValue date_from
    cReport.ParameterFields(4).AddCurrentValue date_to
    cReport.ParameterFields(5).AddCurrentValue CInt(numCount)
    'cReport.ParameterFields(6).AddCurrentValue Status
        If cmbStatus.Text = "PENDING" Then
            sCond = "0"
        ElseIf cmbStatus.Text = "CORRECTED" Then
            sCond = "1"
        ElseIf cmbStatus.Text = "ERRONEOUS" Then
            sCond = "2"
        ElseIf cmbStatus.Text = "FORCED CLOSED" Then
            sCond = "3"
        Else
            sCond = "ALL"
        End If
    cReport.ParameterFields(6).AddCurrentValue sCond
    cReport.ParameterFields(7).AddCurrentValue 0
    
    CRViewer.ReportSource = cReport
    CRViewer.EnableExportButton = True
    CRViewer.ViewReport
    End If
    
    If ReportType = "ADDITIONAL" Then
        Set Appn = CreateObject("CrystalRunTime.Application")

    Set cReport = Appn.OpenReport(App.Path & "\crystal reports\" & g_str_rep)
    
    cReport.Database.LogOnServer "p2ssql.dll", "SZ-MWSI-SUBIC", "MCFSDB", "sa", "sqladmin"
    For i = 1 To cReport.Database.Tables.Count
        cReport.Database.Tables(i).ConnectionProperties.Item("Password") = "sqladmin"
    Next i
   
    cReport.DiscardSavedData
    ' Common Parameters
    cReport.ParameterFields(1).AddCurrentValue BC_Code
    cReport.ParameterFields(2).AddCurrentValue cycle
    cReport.ParameterFields(3).AddCurrentValue CStr(date_from)
    cReport.ParameterFields(4).AddCurrentValue CStr(date_to)
    CRViewer.ReportSource = cReport
    CRViewer.EnableExportButton = True
    CRViewer.ViewReport
    
    
    End If
    
    
End Sub

Private Sub Form_Activate()
If WindowState = vbNormal Then
    Me.Height = 11055
    Me.Width = 15435
End If
'cmbDays.Visible = True
'lbldays.Visible = True
cmbBillMonth.Text = Month(Now)
DTPicker1.Value = Date
DTPicker2.Value = Date
load_BC
cmbReportType.Text = "ADDITIONAL"
ReportType = "ADDITIONAL"
End Sub

Public Sub load_BC()
    
On Error GoTo errhandler
    With cmbBusCen
        .Clear
        .AddItem "ALL"
        If OpenRecordset(g_rs_RBUSCTR, "select * from R_BUSCTR order by BC_CODE", "frmDailyBatchGen", "Daily Batch Generation") Then
            While Not g_rs_RBUSCTR.EOF
                .AddItem g_rs_RBUSCTR.Fields(0) & " - " & g_rs_RBUSCTR.Fields(1)
                g_rs_RBUSCTR.MoveNext
            Wend
        Else
            .ListIndex = 0
        End If
    End With
cmbBusCen.Text = "ALL"
errhandler:
If Err.Number <> 0 Then
    If Err.Number = 380 Then
    Else
        MsgBox Err.Description, , "System Message"
    End If
End If
End Sub

Private Sub GetParameters()
If ReportType = "DETAILED" Then
g_str_rep = "VALIDATION DETAILED REPORT.rpt"
ElseIf ReportType = "SUMMARY" Then
g_str_rep = "VALIDATION SUMMARY REPORT.rpt"
Else
g_str_rep = "ADDITIONAL VALIDATION REPORT.rpt"
End If

If cmbBusCen.Text <> "ALL" Then
BC_Code = Mid(cmbBusCen.Text, 1, 4)
Else
BC_Code = "ALL"
End If

If cmbDays.Text = "One" Then numCount = 1
If cmbDays.Text = "> One" Then numCount = 0
If cmbDays.Text = "All" Then numCount = 2

cycle = CInt(cmbBillMonth.Text)
date_to = DTPicker1.Value
date_from = DTPicker2.Value

Status = cmbStatus.Text

End Sub

