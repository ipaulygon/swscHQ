VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPWMgmt 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "User ID Table"
   ClientHeight    =   3705
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3885
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   12
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3705
   ScaleWidth      =   3885
   Begin VB.Frame Frame1 
      Caption         =   " Select User ID "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2840
      Left            =   120
      TabIndex        =   5
      Top             =   60
      Width           =   3660
      Begin MSComctlLib.ListView lsvUsers 
         Height          =   2460
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   3420
         _ExtentX        =   6033
         _ExtentY        =   4339
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "User ID"
            Object.Width           =   2822
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Access Level"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Password"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "&Add"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   120
      TabIndex        =   1
      Top             =   3100
      Width           =   915
   End
   Begin VB.CommandButton cmdEdit 
      Caption         =   "&Edit"
      Default         =   -1  'True
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   1035
      TabIndex        =   2
      Top             =   3100
      Width           =   915
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "&Delete"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   1950
      TabIndex        =   3
      Top             =   3100
      Width           =   915
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   2865
      TabIndex        =   4
      Top             =   3100
      Width           =   915
   End
End
Attribute VB_Name = "frmPWMgmt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAdd_Click()
    g_str_AddEdit = "ADD"
    Me.Enabled = False
    frmAddUser.Show
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdDelete_Click()
    Dim l_str_Sql As String
    
    If Not ListViewDelete(lsvUsers, 1) Then Exit Sub
    l_str_Sql = "DELETE FROM S_USER WHERE USERID = '" & lsvUsers.SelectedItem.Text & "'"
        
    If Not DBExecute(l_str_Sql, Me.Name, "cmdDelete_Click") Then Exit Sub
    
    lsvUsers.ListItems.Remove (lsvUsers.SelectedItem.Index)
    lsvUsers.Refresh
    lsvUsers.SetFocus
    
    If lsvUsers.ListItems.Count < 1 Then
        cmdEdit.Enabled = False
        cmdDelete.Enabled = False
    End If
    
End Sub

Private Sub cmdEdit_Click()
    g_str_AddEdit = "EDIT"
    Me.Enabled = False
    frmAddUser.Show
End Sub

Private Sub Form_Load()
    Dim l_obj_Item As Object

If WindowState = vbNormal Then
    Me.Move (1000)
    Me.Top = 500
End If
    lsvUsers.ListItems.Clear

'old code starts here 07-17-2013 jums
'If Not OpenRecordset(g_rs_SUSER, "SELECT * FROM S_USER", Me.Name, "Form_Load") Then Exit Sub
'   g_rs_SUSER.MoveFirst
'    While Not g_rs_SUSER.EOF
'        Set l_obj_Item = lsvUsers.ListItems.Add(, , CheckNull(g_rs_SUSER.Fields(0)))
'            l_obj_Item.SubItems(1) = "User"
'            If IsNumeric(CheckNull(g_rs_SUSER.Fields(1), True)) Then
'                If CheckNull(g_rs_SUSER.Fields(1), True) > 1 Then
'                    l_obj_Item.SubItems(1) = "Administrator"
'                End If
'            End If
'            l_obj_Item.SubItems(2) = CheckNull(g_rs_SUSER.Fields(2))
'        Set l_obj_Item = Nothing
'        g_rs_SUSER.MoveNext
'    Wend
'old code ends here 07-17-2013 jums

'parameterized settings
'new code starts here 07-17-2013 jums
If g_rs_userlevel = 3 Then
    If Not OpenRecordset(g_rs_SUSER, "SELECT * FROM S_USER", Me.Name, "Form_Load") Then Exit Sub
       g_rs_SUSER.MoveFirst
        While Not g_rs_SUSER.EOF
            Set l_obj_Item = lsvUsers.ListItems.Add(, , CheckNull(g_rs_SUSER.Fields(0)))
                l_obj_Item.SubItems(1) = "User"
                If IsNumeric(CheckNull(g_rs_SUSER.Fields(1), True)) Then
                    If CheckNull(g_rs_SUSER.Fields(1), True) > 1 Then
                        If CheckNull(g_rs_SUSER.Fields(1), True) = 3 Then
                            l_obj_Item.SubItems(1) = "Super User"
                            Else
                            l_obj_Item.SubItems(1) = "Administrator"
                        End If
                    End If
                End If
                l_obj_Item.SubItems(2) = CheckNull(g_rs_SUSER.Fields(2))
            Set l_obj_Item = Nothing
            g_rs_SUSER.MoveNext
        Wend
Else
    If Not OpenRecordset(g_rs_SUSER, "SELECT * FROM S_USER WHERE [level] <> 3", Me.Name, "Form_Load") Then Exit Sub
       g_rs_SUSER.MoveFirst
        While Not g_rs_SUSER.EOF
            Set l_obj_Item = lsvUsers.ListItems.Add(, , CheckNull(g_rs_SUSER.Fields(0)))
                l_obj_Item.SubItems(1) = "User"
                If IsNumeric(CheckNull(g_rs_SUSER.Fields(1), True)) Then
                    If CheckNull(g_rs_SUSER.Fields(1), True) > 1 Then
                        l_obj_Item.SubItems(1) = "Administrator"
                    End If
                End If
                l_obj_Item.SubItems(2) = CheckNull(g_rs_SUSER.Fields(2))
            Set l_obj_Item = Nothing
            g_rs_SUSER.MoveNext
        Wend
End If
'new code ends here 07-17-2013 jums


    g_rs_SUSER.Close
    Set g_rs_SUSER = Nothing
    
    If lsvUsers.ListItems.Count > 0 Then
        cmdEdit.Enabled = True
        cmdDelete.Enabled = True
    Else
        cmdEdit.Enabled = False
        cmdDelete.Enabled = False
    End If

End Sub

Private Sub lsvUsers_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ListviewSort lsvUsers, ColumnHeader.Index
End Sub
