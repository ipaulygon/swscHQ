VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmInquiry 
   Caption         =   "MRU Inquire"
   ClientHeight    =   6060
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10890
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6060
   ScaleWidth      =   10890
   Begin MSFlexGridLib.MSFlexGrid msgridView 
      Height          =   3375
      Left            =   120
      TabIndex        =   16
      Top             =   1920
      Width           =   10600
      _ExtentX        =   18706
      _ExtentY        =   5953
      _Version        =   393216
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   495
      Left            =   9360
      TabIndex        =   14
      Top             =   5400
      Width           =   1335
   End
   Begin VB.CommandButton cmdViewUL 
      Caption         =   "View UL File"
      Height          =   495
      Left            =   8040
      TabIndex        =   13
      Top             =   5400
      Width           =   1215
   End
   Begin VB.CommandButton cmdViewDL 
      Caption         =   "View DL File"
      Height          =   495
      Left            =   6720
      TabIndex        =   12
      Top             =   5400
      Width           =   1215
   End
   Begin VB.Frame frmSearch 
      Caption         =   "Search Criteria"
      Height          =   1695
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   10575
      Begin VB.ComboBox cmbBusCen 
         Height          =   315
         Left            =   1800
         TabIndex        =   15
         Top             =   480
         Width           =   2415
      End
      Begin VB.ComboBox cmbCycle 
         Height          =   315
         Left            =   1800
         TabIndex        =   11
         Top             =   840
         Width           =   1215
      End
      Begin VB.CommandButton cmdReset 
         Caption         =   "Reset"
         Height          =   375
         Left            =   8520
         TabIndex        =   10
         Top             =   960
         Width           =   1215
      End
      Begin VB.CommandButton cmdRetrieve 
         Caption         =   "Retrieve"
         Height          =   375
         Left            =   8520
         TabIndex        =   9
         Top             =   480
         Width           =   1215
      End
      Begin VB.ComboBox cmbStat 
         Height          =   315
         Left            =   5640
         TabIndex        =   8
         Top             =   480
         Width           =   2175
      End
      Begin VB.TextBox txtMRU 
         Height          =   285
         Left            =   1800
         TabIndex        =   4
         Top             =   1200
         Width           =   1695
      End
      Begin MSComCtl2.DTPicker dtpFrom 
         Height          =   285
         Left            =   5640
         TabIndex        =   17
         Top             =   840
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   503
         _Version        =   393216
         Format          =   20709377
         CurrentDate     =   39589
      End
      Begin MSComCtl2.DTPicker dtpTo 
         Height          =   285
         Left            =   5640
         TabIndex        =   18
         Top             =   1200
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   503
         _Version        =   393216
         Format          =   20709377
         CurrentDate     =   39589
      End
      Begin VB.Label Label6 
         Caption         =   "Date To:"
         Height          =   255
         Left            =   4680
         TabIndex        =   7
         Top             =   1200
         Width           =   1215
      End
      Begin VB.Label Label5 
         Caption         =   "Date From:"
         Height          =   255
         Left            =   4680
         TabIndex        =   6
         Top             =   840
         Width           =   1215
      End
      Begin VB.Label Label4 
         Caption         =   "Status:"
         Height          =   255
         Left            =   4680
         TabIndex        =   5
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "MRU:"
         Height          =   255
         Left            =   360
         TabIndex        =   3
         Top             =   1200
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Cycle/Month:"
         Height          =   255
         Left            =   360
         TabIndex        =   2
         Top             =   840
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Business Center:"
         Height          =   255
         Left            =   360
         TabIndex        =   1
         Top             =   480
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmInquiry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim txtbuscen As String
Dim txtcymonth As String
Dim ANDCycle As String
Dim ANDBusCen As String
Dim ANDBKStat As String
Dim ANDMRU As String
Dim ANDDate As String
Dim rowSel As Integer
Dim MRUSel As String





Private Sub cmbBusCen_Click()
Dim rsBC_CODE As ADODB.Recordset
Dim BC_Code As String
Dim BCSql As String

BCSql = "SELECT * FROM R_BUSCTR WHERE BC_DESC = '" & cmbBusCen.Text & "'"
If OpenRecordset(rsBC_CODE, BCSql, "frmInquiry", "MRU Management") Then
BC_Code = rsBC_CODE.Fields(0).Value
'sBusCenter = BC_Code & " - " & cmbBusCen.Text
End If
Set rsBC_CODE = Nothing

ANDBusCen = " AND T_BOOK.BC_CODE = '" & BC_Code & "'"
End Sub

Private Sub cmbCycle_Click()
ANDCycle = " AND T_BOOK.CYCLE = " & CInt(Mid(cmbCycle.Text, 1, 2))
DTPFrom.Month = Mid(cmbCycle.Text, 1, 2)
DTPTo.Month = Mid(cmbCycle.Text, 1, 2)


'MsgBox WhereClause
End Sub

Private Sub cmbStat_Click()
Dim rs_Bkstat As ADODB.Recordset
Dim BKStat As String
Dim BKstatSQL As String

DateSelection (1)
BKstatSQL = "SELECT * FROM R_BOOKSTAT WHERE BKSTDESC = '" & cmbStat.Text & "'"
If OpenRecordset(rs_Bkstat, BKstatSQL, "frmInquiry", "MRU Management") Then
BKStat = rs_Bkstat.Fields(0).Value
End If
Set rs_Bkstat = Nothing


ANDBKStat = " AND T_SCHED.BKSTCODE = '" & BKStat & "'"



End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdReset_Click()
FormatDataGrid
ANDBusCen = ""
ANDBKStat = ""
ANDCycle = ""
ANDMRU = ""
ANDDate = ""
DateSelection (0)
cmbStat.Text = ""
cmbBusCen.Text = ""
cmbCycle.Text = ""
txtMRU.Text = ""


End Sub

Private Sub cmdRetrieve_Click()
Dim strSQLRet As String
Dim DtFrom As String
Dim DtTo As String

If cmbCycle.Text = "" Then
    MsgBox "No value inputted for Cycle/Month.", vbCritical, "MRU Inquire"
    Exit Sub
End If

GetMRU

DtFrom = Format(CDate(DTPFrom.Value), "mm-dd-yyyy")
DtTo = Format(CDate(DTPTo.Value), "mm-dd-yyyy")
If ANDBKStat <> "" Then
    If DTPFrom.Value > DTPTo.Value Then
        MsgBox ("Please enter valid dates for criteria!"), vbCritical
        Exit Sub
    Else
        Select Case cmbStat.Text
'            Case "Scheduled Reading Date": ANDDate = " AND T_BOOK.SCHED_RDG_DT >= Datevalue('" & CDate(DTPFrom.Value) & "') AND T_BOOK.SCHED_RDG_DT <= DateValue('" & CDate(DTPTo.Value) & "')"
'            Case "Downloaded from MWSI": ANDDate = " AND T_BOOK.ACTUAL_DL_DT >= Datevalue('" & CDate(DTPFrom.Value) & "') AND T_BOOK.ACTUAL_DL_DT <= DateValue('" & CDate(DTPTo.Value) & "')"
'            Case "Sent to Satellite": ANDDate = " AND T_BOOK.UPLD_SO_DT >= Datevalue('" & CDate(DTPFrom.Value) & "') AND T_BOOK.UPLD_SO_DT <= DateValue('" & CDate(DTPTo.Value) & "')"
'            Case "Uploaded from Satellite": ANDDate = " AND T_BOOK.RECV_SO_DT >= Datevalue('" & CDate(DTPFrom.Value) & "') AND T_BOOK.RECV_SO_DT <= DateValue('" & CDate(DTPTo.Value) & "')"
'            Case "Forwarded to MWSI": ANDDate = " AND T_BOOK.UPLD_MWSI_DT >= Datevalue('" & CDate(DTPFrom.Value) & "') AND T_BOOK.UPLD_MWSI_DT <= DateValue('" & CDate(DTPTo.Value) & "')"
            Case "Scheduled Reading Date": ANDDate = " AND convert(char(12),T_BOOK.SCHED_RDG_DT, 110) >= '" & DtFrom & "' AND convert(char(12),T_BOOK.SCHED_RDG_DT, 110) <= '" & DtTo & "'"
            Case "Downloaded from MWSI": ANDDate = " AND convert(char(12),T_BOOK.ACTUAL_DL_DT, 110) >= '" & DtFrom & "' AND convert(char(12),T_BOOK.ACTUAL_DL_DT, 110) <= '" & DtTo & "'"
            Case "Sent to Satellite": ANDDate = " AND convert(char(12),T_BOOK.UPLD_SO_DT, 110) >= '" & DtFrom & "' AND convert(char(12),T_BOOK.UPLD_SO_DT, 110) <= '" & DtTo & "'"
            Case "Uploaded from Satellite": ANDDate = " AND convert(char(12),T_BOOK.RECV_SO_DT, 110) >= '" & DtFrom & "' AND convert(char(12),T_BOOK.RECV_SO_DT, 110) <= '" & DtTo & "'"
            Case "Forwarded to MWSI": ANDDate = " AND convert(char(12),T_BOOK.UPLD_MWSI_DT, 110) >= '" & DtFrom & "' AND convert(char(12),T_BOOK.UPLD_MWSI_DT, 110) <= '" & DtTo & "'"
        End Select
    End If
End If

strSQLRet = "SELECT DISTINCT T_BOOK.BOOKNO, R_BOOKSTAT.BKSTDESC, T_BOOK.SCHED_RDG_DT, T_BOOK.ACTUAL_DL_DT, " & _
"T_BOOK.UPLD_SO_DT, T_BOOK.RECV_SO_DT, T_BOOK.UPLD_MWSI_DT From T_BOOK INNER JOIN (T_SCHED " & _
"INNER JOIN R_BOOKSTAT ON T_SCHED.BKSTCODE = R_BOOKSTAT.BKSTCODE) ON T_BOOK.BookNo = T_SCHED.BookNo and T_BOOK.CYCLE=T_SCHED.CYCLE WHERE T_BOOK.BOOKNO <> '' " & ANDCycle & ANDBusCen & ANDBKStat & ANDMRU & ANDDate

'strSQLRet = "SELECT DISTINCT T_BOOK.BOOKNO, R_BOOKSTAT.BKSTDESC, T_BOOK.SCHED_RDG_DT, T_BOOK.ACTUAL_DL_DT, " & _
'"T_BOOK.UPLD_SO_DT, T_BOOK.RECV_SO_DT, T_BOOK.UPLD_MWSI_DT From T_BOOK INNER JOIN (T_SCHED " & _
'"INNER JOIN R_BOOKSTAT ON T_SCHED.BKSTCODE = R_BOOKSTAT.BKSTCODE) ON T_BOOK.BookNo = T_SCHED.BookNo WHERE T_BOOK.BOOKNO <> '' " & ANDCycle & ANDBusCen & ANDBKStat & ANDMRU & ANDDate

FormatDataGrid
GenerateView (strSQLRet)

intBillCycle = CInt(Mid(cmbCycle.Text, 1, 2))

End Sub

Private Sub cmdViewDL_Click()
Dim sqlBC As String
Dim rs_BC As ADODB.Recordset

If sMRU <> "" Then
    sqlBC = "SELECT * FROM R_BUSCTR WHERE BC_CODE = '" & Mid(sMRU, 1, 4) & "'"
    If OpenRecordset(rs_BC, sqlBC, "frmInquiry", "MRU Management") Then
        sBusCenter = rs_BC.Fields(0).Value & " - " & rs_BC.Fields(1).Value
    End If
    Set rs_BC = Nothing
        g_str_DLUL = "DL"
        frmViewDLUL.Show
Else
    MsgBox "No MRU Selected", vbCritical
    
End If

End Sub

Private Sub cmdViewUL_Click()
Dim sqlBC As String
Dim rs_BC As ADODB.Recordset
If sMRU <> "" Then
    sqlBC = "SELECT * FROM R_BUSCTR WHERE BC_CODE = '" & Mid(sMRU, 1, 4) & "'"
    If OpenRecordset(rs_BC, sqlBC, "frmInquiry", "MRU Management") Then
        sBusCenter = rs_BC.Fields(0).Value & " - " & rs_BC.Fields(1).Value
    End If
    Set rs_BC = Nothing
        g_str_DLUL = "UL"
        frmViewDLUL.Show
Else
    MsgBox "No MRU Selected", vbCritical
    
End If
End Sub

Private Sub Form_Load()
sMRU = ""
cmdViewUL.Enabled = False
frmInquiry.Width = 11010
frmInquiry.Height = 6570
BuildCombo
FormatDataGrid
DateSelection (0)
SetCycleMonth


End Sub

Private Sub BuildCombo()
Dim strSQL1 As String
Dim strSQL2 As String

'BUILD COMBOBOX FOR BRANCHES
strSQL1 = "SELECT R_BUSCTR.BC_CODE, R_BUSCTR.BC_DESC FROM R_BUSCTR ORDER BY BC_CODE"
If OpenRecordset(g_rs_BUSCTR, strSQL1, "frmInquiry", "MRU Management") Then
    Do While Not g_rs_BUSCTR.EOF
        With cmbBusCen
            .AddItem (g_rs_BUSCTR.Fields(1).Value)
        End With
        g_rs_BUSCTR.MoveNext
    Loop
    Set g_rs_BUSCTR = Nothing
 End If
'BUILD COMBOBOX FOR BOOK STATUS
strSQL2 = "SELECT * FROM R_BOOKSTAT"
If OpenRecordset(g_rs_BOOKSTAT, strSQL2, "frmInquiry", "MRU Management") Then
    Do While Not g_rs_BOOKSTAT.EOF
        With cmbStat
            .AddItem (g_rs_BOOKSTAT(1).Value)
        End With
        g_rs_BOOKSTAT.MoveNext
    Loop
    Set g_rs_BOOKSTAT = Nothing
End If

'BUILD CYCLE/MONTH
cmbCycle.AddItem ("01 - Jan")
cmbCycle.AddItem ("02 - Feb")
cmbCycle.AddItem ("03 - Mar")
cmbCycle.AddItem ("04 - Apr")
cmbCycle.AddItem ("05 - May")
cmbCycle.AddItem ("06 - Jun")
cmbCycle.AddItem ("07 - Jul")
cmbCycle.AddItem ("08 - Aug")
cmbCycle.AddItem ("09 - Sep")
cmbCycle.AddItem ("10 - Oct")
cmbCycle.AddItem ("11 - Nov")
cmbCycle.AddItem ("12 - Dec")
End Sub

Private Sub GenerateView(SQLQuery As String)
Dim X As Integer
Dim y As Integer
Dim Count As Integer

    If OpenRecordset(g_rs_BOOKINQ, SQLQuery, "frmMRUGrid", "MRU Management") Then

     y = 1
        Do While Not g_rs_BOOKINQ.EOF
        
            Count = Count + 1
            
            If Count >= msgridView.Rows Then
                'increase the amount of rows by 100 at a time - makes everything faster
                msgridView.Rows = msgridView.Rows + 100
            End If
            
                For X = 0 To g_rs_BOOKINQ.Fields.Count - 1
                    If Not IsNull(g_rs_BOOKINQ.Fields(X).Value) Then
                        msgridView.TextMatrix(y, X) = g_rs_BOOKINQ.Fields(X).Value
                    Else
                        msgridView.TextMatrix(y, X) = ""
                    End If
                Next X
        y = y + 1
        g_rs_BOOKINQ.MoveNext
        Loop
        'LoadRecordsetIntoGrid g_rs_BOOKINQ, msgridView
        
        
        
    End If

End Sub

Private Sub FormatDataGrid()
Dim ColNames As String
Dim X As Integer

    msgridView.Clear
    'msgridView.Rows = 20
    msgridView.FixedRows = 1
    msgridView.FixedCols = 0
    ColNames = "<MRU|<Status|<Sched. Rdg Date|<Dnld frm MWSI|<Sent to SO|<Recv from SO|<Fwd to MWSI"
    msgridView.FormatString = ColNames
    For X = 0 To 6
    msgridView.ColWidth(X) = 1500
    Next X
    
End Sub

Private Sub SetCycleMonth()
Dim cycle As Integer
Dim cymonth As String

cycle = Right(g_BillMonth, 2)

Select Case cycle
    Case 1: cymonth = "01 - Jan"
    Case 2: cymonth = "02 - Feb"
    Case 3: cymonth = "03 - Mar"
    Case 4: cymonth = "04 - Apr"
    Case 5: cymonth = "05 - May"
    Case 6: cymonth = "06 - Jun"
    Case 7: cymonth = "07 - Jul"
    Case 8: cymonth = "08 - Aug"
    Case 9: cymonth = "09 - Sep"
    Case 10: cymonth = "10 - Oct"
    Case 11: cymonth = "11 - Nov"
    Case 12: cymonth = "12 - Dec"
End Select

cmbCycle.Text = cymonth

ANDCycle = " AND T_BOOK.CYCLE = " & CInt(Mid(cymonth, 1, 2))
'MsgBox WhereClause

End Sub

Private Sub GetMRU()
If txtMRU.Text <> "" Then
ANDMRU = " AND T_BOOK.BOOKNO = '" & txtMRU.Text & "'"
End If

End Sub


Private Sub msgridView_Click()
Dim i As Integer
Dim GridRow As Integer
Dim gridCol As Integer
Dim bStat As String

GridRow = CInt(msgridView.Row)
gridCol = CInt(msgridView.Col)
sMRU = msgridView.TextMatrix(GridRow, 0)
bStat = msgridView.TextMatrix(GridRow, 1)

If bStat = "Forwarded to MWSI" Or bStat = "Received from Satellite" Then
cmdViewUL.Enabled = True
Else
cmdViewUL.Enabled = False
End If

'set previous selected forecolor to black
If rowSel <> 0 Then
    With msgridView
        .Row = rowSel
        For i = 0 To .Cols - 1
         .Col = i
         .CellForeColor = RGB(30, 0, 0) 'black
         .CellBackColor = RGB(255, 255, 255)
        Next
    End With
End If

'set current selected forecolor to red
      With msgridView
        .Row = GridRow
        For i = 0 To .Cols - 1
         .Col = i
         .CellForeColor = RGB(255, 255, 255) 'white
         .CellBackColor = RGB(30, 0, 0)
        Next
        .Col = gridCol
    End With


'assign current selected to previous
rowSel = GridRow


End Sub

Private Sub DateSelection(trigger As Integer)
If trigger = 0 Then
    Label5.Visible = False
    Label6.Visible = False
    DTPFrom.Visible = False
    DTPTo.Visible = False
Else
    Label5.Visible = True
    Label6.Visible = True
    DTPFrom.Visible = True
    DTPTo.Visible = True

End If

End Sub

Private Sub Grid_Click()

End Sub
