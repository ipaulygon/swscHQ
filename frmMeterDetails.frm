VERSION 5.00
Begin VB.Form frmMeterDetails 
   Caption         =   "Meter Details"
   ClientHeight    =   3720
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8835
   LinkTopic       =   "Form1"
   ScaleHeight     =   3720
   ScaleWidth      =   8835
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdClose 
      Caption         =   "&Close"
      Height          =   495
      Left            =   3480
      TabIndex        =   0
      Top             =   3120
      Width           =   1215
   End
   Begin VB.Label lblprevcons 
      Caption         =   "10"
      Height          =   255
      Left            =   5760
      TabIndex        =   25
      Top             =   2160
      Width           =   735
   End
   Begin VB.Label Label13 
      Caption         =   "Prev Cons:"
      Height          =   375
      Left            =   4680
      TabIndex        =   24
      Top             =   2160
      Width           =   1215
   End
   Begin VB.Label lblremark 
      Caption         =   "Closed Source, not found"
      Height          =   255
      Left            =   5760
      TabIndex        =   23
      Top             =   2640
      Width           =   2175
   End
   Begin VB.Label lblCons 
      Caption         =   "20"
      Height          =   255
      Left            =   7920
      TabIndex        =   22
      Top             =   2160
      Width           =   615
   End
   Begin VB.Label lblCurRdg 
      Caption         =   "165"
      Height          =   255
      Left            =   7440
      TabIndex        =   21
      Top             =   1680
      Width           =   735
   End
   Begin VB.Label lblPrevRdg 
      Caption         =   "145"
      Height          =   255
      Left            =   5640
      TabIndex        =   20
      Top             =   1680
      Width           =   615
   End
   Begin VB.Label lblRdgDate 
      Caption         =   "05/12/2008"
      Height          =   255
      Left            =   6600
      TabIndex        =   19
      Top             =   840
      Width           =   1215
   End
   Begin VB.Label lblInstallNo 
      Caption         =   "8002-3999145"
      Height          =   255
      Left            =   6600
      TabIndex        =   18
      Top             =   360
      Width           =   1215
   End
   Begin VB.Label lblOC 
      Caption         =   "CLOSED WSC"
      Height          =   255
      Left            =   1920
      TabIndex        =   17
      Top             =   2280
      Width           =   1935
   End
   Begin VB.Label lblAddress 
      Caption         =   "12 Don Pepot St, Sampaloc Manila"
      Height          =   375
      Left            =   1920
      TabIndex        =   16
      Top             =   1800
      Width           =   2655
   End
   Begin VB.Label lblCustName 
      Caption         =   "Juan dela Cruz"
      Height          =   375
      Left            =   1920
      TabIndex        =   15
      Top             =   1320
      Width           =   2535
   End
   Begin VB.Label lblMeterNo 
      Caption         =   "AJ15-00399134"
      Height          =   255
      Left            =   1920
      TabIndex        =   14
      Top             =   840
      Width           =   1215
   End
   Begin VB.Label lblMRU 
      Caption         =   "10020039"
      Height          =   255
      Left            =   960
      TabIndex        =   13
      Top             =   360
      Width           =   1215
   End
   Begin VB.Label Label12 
      Caption         =   "Consumption:"
      Height          =   255
      Left            =   6600
      TabIndex        =   12
      Top             =   2160
      Width           =   1215
   End
   Begin VB.Label Label11 
      Caption         =   "Remarks:"
      Height          =   255
      Left            =   4680
      TabIndex        =   11
      Top             =   2640
      Width           =   1215
   End
   Begin VB.Label Label10 
      Caption         =   "Current:"
      Height          =   255
      Left            =   6600
      TabIndex        =   10
      Top             =   1680
      Width           =   1215
   End
   Begin VB.Label Label9 
      Caption         =   "Previous:"
      Height          =   255
      Left            =   4680
      TabIndex        =   9
      Top             =   1680
      Width           =   1215
   End
   Begin VB.Label Label8 
      Caption         =   "READINGS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4680
      TabIndex        =   8
      Top             =   1320
      Width           =   1215
   End
   Begin VB.Label Label7 
      Caption         =   "Reading Date:"
      Height          =   375
      Left            =   4680
      TabIndex        =   7
      Top             =   840
      Width           =   1575
   End
   Begin VB.Label Label6 
      Caption         =   "Installation Number"
      Height          =   255
      Left            =   4680
      TabIndex        =   6
      Top             =   360
      Width           =   1935
   End
   Begin VB.Label Label5 
      Caption         =   "Observation Code:"
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   2280
      Width           =   1815
   End
   Begin VB.Label Label4 
      Caption         =   "Customer Address:"
      Height          =   375
      Left            =   240
      TabIndex        =   4
      Top             =   1800
      Width           =   2055
   End
   Begin VB.Label Label3 
      Caption         =   "Customer Name:"
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   1320
      Width           =   1575
   End
   Begin VB.Label Label2 
      Caption         =   "Meter/Device No:"
      Height          =   375
      Left            =   240
      TabIndex        =   2
      Top             =   840
      Width           =   1695
   End
   Begin VB.Label Label1 
      Caption         =   "MRU: "
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   360
      Width           =   1215
   End
End
Attribute VB_Name = "frmMeterDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub Form_Load()
Dim SQLMeter As String
Dim rs_BC As ADODB.Recordset


'MsgBox gInstallNo & " " & gMeterNo & " " & gRdgDate

SQLMeter = "SELECT UPLD.BOOKNO, UPLD.METERNO, T_DOWNLOAD.ACCTNAME, T_DOWNLOAD.ADDRESS, UPLD.FFCODE, UPLD.ACCTNUM, UPLD.RDGDATE, T_DOWNLOAD.BILL_PREVRDG, " & _
        " UPLD.PRESRDG , UPLD.PRESRDG - T_DOWNLOAD.BILL_PREVRDG , UPLD.REMARKS, T_DOWNLOAD.PREVCONS " & _
        " FROM (SELECT c.* From t_upload c Where Not Exists (SELECT a.bookno,a.ulcycle,a.uldoc_no,a.ffcode,a.rangecode,a.deviceno,a.presrdg,a.rdgdate, " & _
        "a.rdgtime , a.METERNO, a.REMARKS, a.ACCTNUM, a.readtag, a.SEQNO, a.tries, a.newmtrbrand, a.newmtrnum " & _
        "From t_upload_his a inner join t_ver_extract b on a.bookno=b.bookno and a.ulcycle=b.ulcycle " & _
        "and a.acctnum=b.acctnum Where " & _
        "c.BookNo = a.BookNo And c.ulcycle = a.ulcycle And c.ACCTNUM = a.ACCTNUM " & _
        "AND a.origdata=0 and b.condition=1) " & _
        "Union " & _
        "SELECT a.bookno,a.ulcycle,a.uldoc_no,a.ffcode,a.rangecode,a.deviceno,a.presrdg,a.rdgdate, " & _
        "a.rdgtime , a.METERNO, a.REMARKS, a.ACCTNUM, a.readtag, a.SEQNO, a.tries, a.newmtrbrand, a.newmtrnum " & _
        "From t_upload_his a inner join t_ver_extract b on a.bookno=b.bookno and a.ulcycle=b.ulcycle " & _
        "and a.acctnum=b.acctnum WHERE " & _
        "a.origdata=0 and b.condition=1) AS UPLD " & _
        "INNER JOIN T_DOWNLOAD ON UPLD.METERNO = T_DOWNLOAD.SERIALNO AND UPLD.ULCYCLE = T_DOWNLOAD.DLCYCLE " & _
        " WHERE UPLD.BOOKNO <> '' AND UPLD.RDGDATE = '" & gRdgDate & "'" & _
        " AND UPLD.METERNO = '" & gMeterNo & "'" & _
        " AND UPLD.ACCTNUM = '" & gInstallNo & "'"
        

If OpenRecordset(rs_BC, SQLMeter, "frmMeterDeails", "MRU Management") Then
lblMRU.Caption = rs_BC.Fields(0).Value
lblMeterNo.Caption = rs_BC.Fields(1).Value
lblCustName.Caption = rs_BC.Fields(2).Value
lblAddress.Caption = rs_BC.Fields(3).Value
lblOC.Caption = rs_BC.Fields(4).Value
lblInstallNo.Caption = rs_BC.Fields(5).Value
lblRdgDate.Caption = rs_BC.Fields(6).Value
lblPrevRdg.Caption = rs_BC.Fields(7).Value
lblCurRdg.Caption = rs_BC.Fields(8).Value
lblCons.Caption = rs_BC.Fields(9).Value
lblremark.Caption = rs_BC.Fields(10).Value
lblprevcons.Caption = rs_BC.Fields(11).Value

End If

End Sub

