VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmDiscMRUMngt 
   Caption         =   "Manage MRUs"
   ClientHeight    =   9720
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15150
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9720
   ScaleWidth      =   15150
   Begin VB.CommandButton cmdSelectNone 
      Caption         =   "Select &None"
      Height          =   450
      Left            =   4080
      TabIndex        =   10
      Top             =   9000
      Width           =   1815
   End
   Begin VB.CommandButton cmdSelectAll 
      Caption         =   "Select &All"
      Height          =   450
      Left            =   1920
      TabIndex        =   9
      Top             =   9000
      Width           =   1815
   End
   Begin VB.CommandButton cmdRSO 
      Caption         =   "&Receive Upload"
      Height          =   450
      Left            =   8400
      TabIndex        =   8
      Top             =   9000
      Width           =   1815
   End
   Begin VB.CommandButton cmdSSO 
      Caption         =   "Create &Download"
      Height          =   450
      Left            =   6240
      TabIndex        =   7
      Top             =   9000
      Width           =   1815
   End
   Begin VB.Frame fraSelect 
      Height          =   7785
      Left            =   240
      TabIndex        =   1
      Top             =   960
      Width           =   14715
      Begin MSComctlLib.ListView lsvMRUMngt 
         Height          =   7395
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   14475
         _ExtentX        =   25532
         _ExtentY        =   13044
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   16
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "CAN"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Disconnection Reason"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "MRU"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Job ID"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Meter Number"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "PUA"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Current"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Total Amount"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "SEQ"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Text            =   "NAME"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Text            =   "ADDRESS"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Text            =   "RECONFEE"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Text            =   "DISCTAG"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Text            =   "LASTRDG"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Text            =   "DCDATE"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   15
            Text            =   "STATUS"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.CommandButton cmdClose 
      Cancel          =   -1  'True
      Caption         =   "&Close"
      Height          =   450
      Left            =   10560
      TabIndex        =   0
      Top             =   9000
      Width           =   1815
   End
   Begin VB.Label lblNumMRU 
      Caption         =   "Number of MRUs : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10920
      TabIndex        =   6
      Top             =   600
      Width           =   2295
   End
   Begin VB.Label lblDayNo 
      Caption         =   "Day Number : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10920
      TabIndex        =   5
      Top             =   240
      Width           =   1815
   End
   Begin VB.Label lblCycle 
      Caption         =   "Cycle/Month : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   480
      TabIndex        =   4
      Top             =   600
      Width           =   1575
   End
   Begin VB.Label lblBusCtr 
      Caption         =   "Business Center : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   480
      TabIndex        =   3
      Top             =   240
      Width           =   6015
   End
End
Attribute VB_Name = "frmDiscMRUMngt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim iChecked As Integer
Dim MRUArray(1000) As String
Dim AcctArray(1000) As String
Dim MRUList As String
Dim AcctList As String
Dim SRSO As String

Sub UpdateRSOUpload(ByVal sMRU As String, iCycle As Integer)

    Dim strSQL As String

    ' First Update status to "Received From SO" in T_SCHED
    strSQL = "UPDATE T_SCHED SET BKSTCODE = 'RSO' WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
    If Not DBExecute(strSQL, Me.Name, "UpdateRSOUpload") Then
        Rollback_Upload sMRU, iCycle
        Exit Sub
    End If

    ' Next place the timestamp in T_BOOK
     strSQL = "UPDATE T_BOOK SET RECV_SO_DT = getdate() WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
     If Not DBExecute(strSQL, Me.Name, "UpdateRSOUpload") Then
        Rollback_Upload sMRU, iCycle
        Exit Sub
    End If

End Sub

Sub UpdateDCSSODownload(ByVal sAcct As String, iCycle As Integer)

    Dim strSQL As String

    ' First Update status to "Sent To SO" in T_SCHED
    strSQL = "UPDATE T_SCHED SET BKSTCODE = 'SSO' WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
    If Not DBExecute(strSQL, Me.Name, "UpdateSSODownload") Then Exit Sub

    ' Next place the timestamp in T_BOOK
     strSQL = "UPDATE T_BOOK SET UPLD_SO_DT = getdate() WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
     If Not DBExecute(strSQL, Me.Name, "UpdateSSODownload") Then Exit Sub

End Sub

Sub UpdateSSODownload(ByVal sMRU As String, iCycle As Integer)

    Dim strSQL As String

    ' First Update status to "Sent To SO" in T_SCHED
    strSQL = "UPDATE T_SCHED SET BKSTCODE = 'SSO' WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
    If Not DBExecute(strSQL, Me.Name, "UpdateSSODownload") Then Exit Sub

    ' Next place the timestamp in T_BOOK
     strSQL = "UPDATE T_BOOK SET UPLD_SO_DT = getdate() WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
     If Not DBExecute(strSQL, Me.Name, "UpdateSSODownload") Then Exit Sub

End Sub

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub cmdRSO_Click()

Dim sPath As String
Dim lsql As String
Dim iCycle As Integer
Dim strSQL As String


iCycle = str(intBillCycle)
'lsql = "select DIRDLDISCON from S_PARAM where COMPANY_NAME='INDRA'"
strSQL = "SELECT WORKING_DIR FROM R_BUSCTR" 'WHERE BC_CODE = '" & sBC & "'"
OpenRecordset g_rs_BUSCTR, strSQL, "frmMRUMngt", "cmdRSO_Click"
g_rs_BUSCTR.MoveFirst

sPath = g_rs_BUSCTR.Fields(0) & "" & "\DISCON\FRSO"

If UploadToDCDB(sPath, iCycle) Then
    MsgBox "Receive File from SO Successful!", vbInformation, "System Message"
End If

End Sub

Private Sub cmdSelectAll_Click()
SelectAll 1, 0
End Sub

Private Sub cmdSelectNone_Click()
SelectAll 0, 0
End Sub

Private Sub cmdSSO_Click()
Dim iCycle As Integer
Dim sMRU As String
Dim n As Integer
Dim iCount As Integer
Dim itm
Dim l_int_Index As Integer
Dim DFile As String
Dim DFile1 As String
Dim DFile2 As String
Dim sPath As String
Dim fs As Object, fso As Object
Dim a
Dim bcCode As String
Dim BCDESC As String
Dim cdelim As String
Dim rsDL As Recordset
Dim rsGRID As Recordset
Dim jobid As String
Dim ACCTNUM As String
Dim dcdt As Date
Dim sqlstring2 As String

On Error GoTo errhandler

bcCode = Left(sBusCenter, 4)
BCDESC = Mid(sBusCenter, 7)
iCycle = str(intBillCycle)
cdelim = "|"

If Not OpenRecordset(g_rs_RBUSCTR, "SELECT WORKING_DIR, BC_DESC FROM R_BUSCTR WHERE BC_CODE ='" & bcCode & "'", "", "") Then
    Exit Sub
End If

sPath = g_rs_RBUSCTR.Fields(0) & ""
If sPath = "" Then
    MsgBox "Business Center Directory not specified!", vbCritical, "System Message"
    Exit Sub
End If
'for testing
'sPath = "C:\HQ\Satellite"

Set fs = CreateObject("Scripting.FileSystemObject")

' create folder toso\bccode bcdesc\year\cycle monthname\sched mr day, i.e. TOSO\0100 Novaliches\2008\09 September\06
If Not fs.FolderExists(sPath) Then fs.CreateFolder (sPath) ' dl directory
If Not fs.FolderExists(sPath & "\DISCON\TOSO\") Then fs.CreateFolder (sPath & "\DISCON\TOSO\") ' TOSO folder
If Not fs.FolderExists(sPath & "\DISCON\TOSO\" & bcCode & " " & BCDESC) Then fs.CreateFolder (sPath & "\DISCON\TOSO\" & bcCode & " " & BCDESC) ' BC folder
If Not fs.FolderExists(sPath & "\DISCON\TOSO\" & bcCode & " " & BCDESC & "\" & Year(Date)) Then fs.CreateFolder (sPath & "\DISCON\TOSO\" & bcCode & " " & BCDESC & "\" & Year(Date)) ' year folder
If Not fs.FolderExists(sPath & "\DISCON\TOSO\" & bcCode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0")))) Then fs.CreateFolder (sPath & "\DISCON\TOSO\" & bcCode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0")))) ' cycle folder
If Not fs.FolderExists(sPath & "\DISCON\TOSO\" & bcCode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0"))) & "\" & Format(Date, "dd")) Then fs.CreateFolder (sPath & "\DISCON\TOSO\" & bcCode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0"))) & "\" & Format(Date, "dd")) ' sched mr day folder

DFile = sPath & "\DISCON\TOSO\" & bcCode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0"))) & "\" & Format(Date, "dd") & "\" & "DC" & Left(sBusCenter, 2) & Year(Date) & Format(Date, "mm") & Format(Date, "dd") & ".txt"
DFile1 = sPath & "\DISCON\TOSO\" & bcCode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0"))) & "\" & Format(Date, "dd") & "\" & "DC" & Left(sBusCenter, 2) & Year(Date) & Format(Date, "mm") & Format(Date, "dd")

Set rsDL = New ADODB.Recordset

Dim dldate As Date
Dim batchid As Integer

'CHECK IF DATAGRID HAS VALUES
If lsvMRUMngt.ListItems.Count > 1 Then
    'CHECK IF DLDATE AND BATCHID HAS BEEN USED
        sqlstring2 = "SELECT DLDATE,BATCHID FROM S_PARAM"
            rsDL.Open sqlstring2, g_Conn, adOpenStatic, adLockReadOnly
            If IsNull(rsDL.Fields("DLDATE").Value) Then
                DBExecute "UPDATE S_PARAM SET DLDATE = '" & Month(Now) & "/" & Day(Now) & "/" & Year(Now) & "',BATCHID = 0", "", ""
            End If
            rsDL.Close
        
        sqlstring2 = "SELECT BATCHID FROM S_PARAM WHERE DLDATE = '" & Month(Now) & "/" & Day(Now) & "/" & Year(Now) & "'"
        rsDL.Open sqlstring2, g_Conn, adOpenStatic, adLockReadOnly
            If rsDL.RecordCount <> 1 Then
                batchid = 1
            Else
                batchid = rsDL.Fields("BATCHID").Value + 1
            End If
                DBExecute "UPDATE S_PARAM SET DLDATE = '" & Month(Now) & "/" & Day(Now) & "/" & Year(Now) & "',BATCHID = " & batchid, "", ""
            rsDL.Close
    Dim downloadString As String
    DBExecute "EXEC sp_CREATE_DC_DOWNLOAD " & intDayNo & "," & iCycle & ",'" & bcCode & "'", "", ""

        Open DFile For Output As #1
    
               If OpenRecordset(rsGRID, "SELECT DISTINCT ACCTNUM, DISCREASON, BOOKNO, SERIALNO, PUA, CURR_AMOUNT, DUE_AMT, SEQ, CUSTNAME, ADDRESS, RECONFEE, DC_STATUS, RDG, DCDATE, STATUS FROM  DCDOWNLOAD ", "", "") Then
                rsGRID.MoveFirst
               
                   While Not rsGRID.EOF
                       
                       downloadString = Padl(rsGRID.Fields("BOOKNO").Value, 8, "0") & cdelim & _
                       rsGRID.Fields("ACCTNUM").Value & cdelim & _
                       rsGRID.Fields("SEQ").Value & cdelim & _
                       rsGRID.Fields("SERIALNO").Value & cdelim & _
                       rsGRID.Fields("CUSTNAME").Value & cdelim & _
                       rsGRID.Fields("ADDRESS").Value & cdelim & _
                       rsGRID.Fields("RECONFEE").Value & cdelim & _
                       rsGRID.Fields("PUA").Value & cdelim & _
                       rsGRID.Fields("CURR_AMOUNT").Value & cdelim & _
                       rsGRID.Fields("DUE_AMT").Value & cdelim & _
                       IIf(rsGRID.Fields("DC_STATUS").Value = "?", "1", IIf(rsGRID.Fields("DC_STATUS").Value = "P", "2", "")) & cdelim & _
                       IIf((rsGRID.Fields("DC_STATUS").Value = "P" And rsGRID.Fields("STATUS").Value = "DISCONN") Or ((rsGRID.Fields("DC_STATUS").Value = "S" Or rsGRID.Fields("DC_STATUS").Value = "F") And rsGRID.Fields("STATUS").Value = "DISCONN" And (DateDiff("d", rsGRID.Fields("DCDATE").Value, Now)) >= 4), rsGRID.Fields("rdg").Value, "") & cdelim & _
                       IIf(rsGRID.Fields("DC_STATUS").Value = "P", Year(rsGRID.Fields("DCDATE").Value) & IIf(Len(Month(rsGRID.Fields("DCDATE").Value)) = 1, "0" & Month(rsGRID.Fields("DCDATE").Value), Month(rsGRID.Fields("DCDATE").Value)) & IIf(Len(Day(rsGRID.Fields("DCDATE").Value)) = 1, "0" & Day(rsGRID.Fields("DCDATE").Value), Day(rsGRID.Fields("DCDATE").Value)), "")
                        Print #1, downloadString
                       rsGRID.MoveNext
                   Wend
               End If
            
        Close #1

    ' Rename the filename
    DFile2 = DFile1 & Padl(batchid, 2, "0") & ".txt"
    
    
    Set fso = CreateObject("Scripting.FileSystemObject")
    ' Delete file if exist
    If fso.FileExists(DFile2) Then fso.DeleteFile DFile2
    
    'fso.CopyFile fcfile, fcfile2
    Name DFile As DFile2    ' Rename dfile.
    If Not DBExecute("UPDATE T_DC_BOOK SET UPLD_SO_DT=GETDATE() WHERE CYCLE=" & iCycle & " AND BC_CODE ='" & bcCode & "' AND DAYNO = " & intDayNo, "", "") Then
    End If
    MsgBox "Download File Creation Successful!", vbInformation, ""

End If

errhandler:
If Err.Number <> 0 Then
    MsgBox Err.Description, , "Error"
End If

End Sub

Private Sub Form_Activate()
'frmMRUMngt.WindowState = 2
'    ListView_Load
End Sub

Private Sub Form_Load()

If WindowState = vbNormal Then
    Me.Move (0)
    Me.Top = 0
    Me.Height = 10230
    Me.Width = 15270
End If

    lblBusCtr.Caption = "Business Center : " & sBusCenter
    'lblCycle.Caption = "Cycle/Month : " & Right(g_BillMonth, 2)
    lblCycle.Caption = "Cycle/Month : " & str(intBillCycle)
    lblDayNo.Caption = "Day Number : " & str(intDayNo)

    ListView_Load

End Sub

Private Sub ListView_Load()

    Dim l_str_Sql As String
    Dim lBusCenter As String
    'Dim intCycle As Integer
    Dim intNumMRU As Integer

    ' Pass the variables from the MRU Scheduler Form
    lBusCenter = Left$(sBusCenter, 4)

    l_str_Sql = "EXEC sp_GRID_DISPLAY_DISCON " & intDayNo & "," & intBillCycle & ",'" & lBusCenter & "'"
               

    lsvMRUMngt.ListItems.Clear
    If Not OpenRecordset(g_rs_RFF, l_str_Sql, Me.Name, "ListView_Load") Then Exit Sub

    g_rs_RFF.MoveFirst
    While Not g_rs_RFF.EOF
       ListViewSet 16
       g_rs_RFF.MoveNext
    Wend

    g_rs_RFF.Close
    Set g_rs_RFF = Nothing

    intNumMRU = lsvMRUMngt.ListItems.Count

    lblNumMRU.Caption = "Number of MRUs : " & str(intNumMRU)

End Sub

Private Sub ListViewSet(intCols As Integer, Optional intColDate As Integer, Optional intColTime As Integer)
    Dim lobjItem As Object
    Dim l_int_Index As Integer

    Set lobjItem = lsvMRUMngt.ListItems.Add(, , CheckNull(g_rs_RFF.Fields(0)))

    For l_int_Index = 1 To intCols - 1
        Select Case l_int_Index
            Case intColDate
                lobjItem.SubItems(l_int_Index) = Format(CheckNull(g_rs_RFF.Fields(l_int_Index)), "mm/dd/yyyy")
            Case intColTime
                lobjItem.SubItems(l_int_Index) = Format(CheckNull(g_rs_RFF.Fields(l_int_Index)), "hh:mm:ss")
            Case Else
                lobjItem.SubItems(l_int_Index) = CheckNull(g_rs_RFF.Fields(l_int_Index))
        End Select
    Next

    Set lobjItem = Nothing

End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmDiscMRUGrid.Enabled = True
End Sub

Private Sub lsvMRUMngt_Click()
If lsvMRUMngt.ListItems.Count > 0 Then
    If lsvMRUMngt.SelectedItem.SubItems(1) = "Downloaded from MWSI" Then
        cmdSSO.Enabled = True
        'cmdRSO.Enabled = False
    ElseIf lsvMRUMngt.SelectedItem.SubItems(1) = "Sent to Satellite" Then
        'cmdSSO.Enabled = False
        cmdRSO.Enabled = True
    Else
        'cmdSSO.Enabled = False
        'cmdRSO.Enabled = False
    End If

'    If lsvMRUMngt.SelectedItem.SubItems(2) = "Sent to Satellite" Then
'        cmdSSO.Enabled = False
'        cmdRSO.Enabled = True
'    ElseIf lsvMRUMngt.SelectedItem.SubItems(2) = "Received from Satellite" Then
'        cmdSSO.Enabled = True
'        cmdRSO.Enabled = False
'    Else
'        cmdSSO.Enabled = False
'        cmdRSO.Enabled = False
'    End If

End If
End Sub

Private Sub lsvMRUMngt_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ListviewSort lsvMRUMngt, ColumnHeader.Index
End Sub

Private Function Generate_Acct_List() As Boolean
Dim iCount As Integer
'Dim iChecked As Integer
Dim itm
'Dim MRUArray(1000) As String
Dim n As Integer
Dim iCycle As Integer
Dim l_int_Index As Integer

    ' Reset MRUList
    MRUList = ""
    iChecked = 0

    'iCycle = Val(Right(g_BillMonth, 2))
    iCycle = str(intBillCycle)
    ' Count the number of Checked items in the ListView and build the MRUList from the text items
    iCount = lsvMRUMngt.ListItems.Count

    ' If no MRUs were selected, exit function with True, you will be using an "Empty" MRUList!
    If iCount = 0 Then
        Generate_Acct_List = False
        Exit Function
    End If

    For Each itm In lsvMRUMngt.ListItems
       With itm
         If .checked Then
            iChecked = iChecked + 1
            l_int_Index = .Index
            With lsvMRUMngt.ListItems(l_int_Index)
                MsgBox .Text
                MsgBox .SubItems(1)
                MsgBox .SubItems(3)
                MsgBox .SubItems(5)
            End With

'            ' Add the MRU to the list; if first MRU, no comma in front
'            If MRUList <> "" Then
'                MRUList = MRUList & ",'" & .Text & "'"
'            Else
'                MRUList = MRUList & "'" & .Text & "'"
'            End If
'            MRUArray(iChecked) = itm
         End If
       End With
    Next

    If iChecked = 0 Then
        MsgBox "No MRU selected!", vbCritical, "Manage MRUs"
        Generate_Acct_List = False
        Exit Function
    End If

    If SRSO = "SSO" Then
        n = 1
        For n = 1 To iChecked
            ' check if mru stat is RSO
            If Not OpenRecordset(g_rs_TSCHED, "SELECT BOOKNO FROM T_SCHED WHERE BOOKNO = '" & MRUArray(n) & "' and BKSTCODE= 'DLM' and CYCLE= " & iCycle & "", Me.Name, "Generate_MRU_List") Then
                MsgBox "Please uncheck MRU " & MRUArray(n) & ". Can only Send to SO books with status 'Downloaded from MWSI'!", vbCritical, "Manage MRUs"
                'g_rs_TSCHED.Close
                'Set g_rs_TSCHED = Nothing
                Generate_Acct_List = False
                Exit Function
            End If
        Next n
    ElseIf SRSO = "RSO" Then
        n = 1
        For n = 1 To iChecked
            ' check if mru stat is RSO
            If Not OpenRecordset(g_rs_TSCHED, "SELECT BOOKNO FROM T_SCHED WHERE BOOKNO = '" & MRUArray(n) & "' and BKSTCODE= 'SSO' and CYCLE= " & iCycle & "", Me.Name, "Generate_MRU_List") Then
                MsgBox "Please uncheck MRU " & MRUArray(n) & ". Can only Receive from SO books with status 'Sent to Satellite Office'!", vbCritical, "Manage MRUs"
                'g_rs_TSCHED.Close
                'Set g_rs_TSCHED = Nothing
                Generate_Acct_List = False
                Exit Function
            End If
        Next n
    End If

    ' If ALL MRUs were selected, then do NOT use the list
'    If iCount = iChecked Then
'       Generate_MRU_List = False
'    Else
        Generate_Acct_List = True
'    End If

End Function

Private Function SelectAll(ByVal DeOrSelectAll As Integer, ByVal Listbx As Integer) As Boolean ' Added the action that is needed
  Dim Item As ListItem

If Listbx = 0 Then
  SelectAll = True ' Default succes
  Select Case DeOrSelectAll
    Case 0 ' Unchecked Check1
      For Each Item In lsvMRUMngt.ListItems
        Item.checked = False ' Deselect
      Next
    Case 1 ' Checked Check1
      For Each Item In lsvMRUMngt.ListItems
        Item.checked = True ' Select
      Next
    Case Else ' 2 = Grayed or else so no action for now....
      SelectAll = False ' No succes reported back...
  End Select
End If

End Function

