VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form frmAddSched 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Add Schedule"
   ClientHeight    =   4365
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8385
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4365
   ScaleWidth      =   8385
   Begin VB.CommandButton cmdAccept 
      Caption         =   "&Ok"
      Height          =   390
      Left            =   3480
      TabIndex        =   9
      Top             =   3690
      Width           =   915
   End
   Begin VB.CommandButton cmdMulti 
      Caption         =   "&Multi"
      Height          =   390
      Left            =   4830
      TabIndex        =   8
      Top             =   3690
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   390
      Left            =   5910
      TabIndex        =   7
      Top             =   3690
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.ComboBox cmbRoverNo 
      Height          =   315
      Left            =   990
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   180
      Width           =   1815
   End
   Begin VB.ComboBox cmbReader 
      Height          =   315
      Left            =   3660
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   180
      Width           =   1815
   End
   Begin VB.ComboBox cmbStat 
      Height          =   315
      Left            =   6480
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   180
      Visible         =   0   'False
      Width           =   1815
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid flxSched 
      Height          =   2715
      Left            =   150
      TabIndex        =   0
      Top             =   720
      Width           =   8085
      _ExtentX        =   14261
      _ExtentY        =   4789
      _Version        =   393216
      Cols            =   31
      FixedRows       =   0
      FixedCols       =   0
      WordWrap        =   -1  'True
      TextStyleFixed  =   1
      GridLinesUnpopulated=   1
      ScrollBars      =   1
      _NumberOfBands  =   1
      _Band(0).Cols   =   31
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
      _Band(0).ColHeader=   1
   End
   Begin VB.Label lblRoverNo 
      Caption         =   "Rover No."
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   210
      Width           =   975
   End
   Begin VB.Label lblReader 
      Caption         =   "Reader"
      Height          =   255
      Left            =   3000
      TabIndex        =   5
      Top             =   210
      Width           =   615
   End
   Begin VB.Label lblStatus 
      Caption         =   "Status"
      Height          =   255
      Left            =   5880
      TabIndex        =   4
      Top             =   210
      Visible         =   0   'False
      Width           =   615
   End
End
Attribute VB_Name = "frmAddSched"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim tHHID As String 'temporary HHID
Dim tReader As String 'temporary Reader

Private Sub cmbReader_Click()
    
If cmbReader.Text = "" Then
    flxSched.Enabled = False
    cmdMulti.Enabled = False
Else
    flxSched.Enabled = True
    cmdMulti.Enabled = True
End If
    
End Sub

Private Sub cmbRoverNo_Click()
If cmbRoverNo.Text = "" Then
    cmbReader.Enabled = False
    flxSched.Enabled = False
    cmdMulti.Enabled = False
    cmbReader.Clear
    load_cmb_Reader cmbReader, ""
Else
    cmbReader.Enabled = True
End If

End Sub

'Private Sub cmbStat_Change()
'    flxSched.SetFocus
'End Sub
'
'Private Sub cmbStat_Click()
'If cmbRoverNo.ListCount > 3 Then
'    flxSched.SetFocus
'End If
'End Sub

Private Sub flxSched_Click()
    
    If flxSched.Text <> "" Then
        cmdAccept.Enabled = True
    End If
    
    DayNo = flxSched.Col + 1
    sql = "select schedstat from T_SCHED where bookno='" & flxSched.Text & "' and dayno=" & DayNo
    If OpenRecordset(g_rs_TBOOK, sql, "frmAddSched", "Host") Then
        Select Case UCase(CheckNull(g_rs_TBOOK("schedstat")))
            Case ""
                cmbStat.ListIndex = 0
            Case "NO"
                cmbStat.ListIndex = 1
            Case "OK", "UP"
                cmbStat.ListIndex = 2
            Case Else
                cmbStat.ListIndex = 0
        End Select
    Else
        cmbStat.ListIndex = 0
    End If
End Sub

Private Sub flxSched_DblClick()
Me.Enabled = False
'    MsgBox "x=" & flxSched.Col & " y=" & flxSched.Row
    'frmMultiBook.get_Sched cmbRoverNo.Text, flxSched.Col + 1, flxSched.Text, cmbReader.Text
     g_str_Table = "T_SCHED"
    frmBookSched.Show
End Sub

Sub load_Sched_Add(HHID As String, Reader As String)
    tHHID = HHID
    tReader = Reader
    frmAddSched.Caption = "Add Schedule"
    frmAddSched.Show
End Sub

Sub load_Sched_Edit(HHID As String, Reader As String)
    tHHID = HHID
    tReader = Reader
    frmAddSched.Caption = "Edit Schedule"
    frmAddSched.Show
End Sub

Private Sub Form_Activate()
Dim i, j As Integer

If WindowState = vbNormal Then
    Me.Move (1000)
    Me.Top = 500
End If
    flxSched.Clear
    For i = 0 To flxSched.Rows - 1
        For j = 0 To flxSched.Cols - 1
            flxSched.ColHeader(0) = flexColHeaderOn
            flxSched.Col = j
            flxSched.Row = 0
    '       flxSched.Text = "Day " & (j + 1)
            flxSched.ColHeaderCaption(0, j) = "Day " & (j + 1)
    '       End If
    '       flxSched.ColWidth(j) = 1000
        Next j
    Next i

    If frmAddSched.Caption = "Add Schedule" Then
        load_AddSched_Combos
    Else
        load_EditSched_Combos
    End If
    flxSched_Click
    
    If cmbRoverNo.Text = "" Then
        cmdMulti.Enabled = False
        cmbReader.Enabled = False
        flxSched.Enabled = False
        cmdAccept.Enabled = False
    End If
        
End Sub

Private Sub Form_Load()
    Dim i, j As Integer

    For i = 0 To flxSched.Rows - 1
        For j = 0 To flxSched.Cols - 1
            If i > 0 Then
                flxSched.RowHeight(i) = 1000
            Else
'                flxSched.ColHeader(0) = "Day " & (j + 1)
                flxSched.Col = j
                flxSched.Row = 0
'                flxSched.Text = "Day " & (j + 1)
                flxSched.ColHeaderCaption(0, j) = "Day " & (j + 1)
            End If
            flxSched.ColWidth(j) = 1000
        Next j
    Next i

End Sub

Private Sub load_AddSched_Combos()
'    load_cmb_HHID cmbRoverNo, ""
'    load_cmb_Reader cmbReader, ""
'    load_lst_Sched flxSched, ""
    load_cmb_HHID cmbRoverNo, cmbRoverNo.Text
    load_cmb_Reader cmbReader, cmbReader.Text
    load_lst_Sched flxSched, cmbRoverNo.Text
    
    cmbRoverNo.Enabled = True
    cmbReader.Enabled = True

    With cmbStat
        .Clear
        .AddItem ""
        .AddItem "Download"
        .AddItem "Upload"
    End With
End Sub

Private Sub load_EditSched_Combos()
    load_cmb_HHID cmbRoverNo, tHHID
    load_cmb_Reader cmbReader, tReader
    load_lst_Sched flxSched, tHHID
    
    cmbRoverNo.Enabled = False
    cmbReader.Enabled = False
    
    With cmbStat
        .Clear
        .AddItem ""
        .AddItem "Download"
        .AddItem "Upload"
    End With
End Sub

Private Sub cmdAccept_Click()
Dim DayNo As Integer
Dim HHNo As String
Dim l_obj_Item As Object
'    Unload Me
    DayNo = flxSched.Col + 1
    sql = "select * from F_HH where hhid='" & cmbRoverNo.Text & "'"
'    If OpenRecordset(g_rs_RHH, sql, "frmAddSched", "Host") Then
'        HHNo = CheckNull(g_rs_RHH("HHNo"))
'    Else
'        HHNo = ""
'    End If

'    Select Case cmbStat.ListIndex
'        Case 0
'            sql = "update T_SCHED set schedstat='' where dayno=" & DayNo & " and hhno='" & HHNo & "'"
'        Case 1
'            sql = "update T_SCHED set schedstat='NO' where dayno=" & DayNo & " and hhno='" & HHNo & "'"
'        Case 2
'            sql = "update T_SCHED set schedstat='OK' where dayno=" & DayNo & " and hhno='" & HHNo & "'"
'    End Select
    
    If DBExecute(sql, "frmAddSched", "Host") Then
        If frmAddSched.Caption = "Add Schedule" Then
            Set l_obj_Item = frmScheduler.lsvSched.ListItems.Add(, , Trim(cmbRoverNo))
                l_obj_Item.SubItems(1) = Trim(cmbReader)
                l_obj_Item.SubItems(2) = Trim(cmbStat)
            Set l_obj_Item = Nothing
        End If
            frmScheduler.cmdEdit.Enabled = True
            frmScheduler.lsvSched.Refresh
            Unload Me
'            MsgBox "Schedule saved.", vbOKOnly, "MRMS"
    Else
        MsgBox "Error in saving Schedule", vbCritical, "Error"
    End If
           
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdMulti_Click()
    frmMultiBook.Show
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmScheduler.Enabled = True
End Sub

