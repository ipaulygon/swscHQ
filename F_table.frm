VERSION 5.00
Begin VB.Form f_table 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Download Table"
   ClientHeight    =   2055
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   3240
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2055
   ScaleWidth      =   3240
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton btn_stop 
      Cancel          =   -1  'True
      Caption         =   "Stop"
      Height          =   375
      Left            =   1620
      TabIndex        =   1
      Top             =   1470
      Width           =   1215
   End
   Begin VB.TextBox ent_rec 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2010
      Locked          =   -1  'True
      TabIndex        =   0
      Top             =   900
      Width           =   825
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Insert Rover"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   345
      Left            =   390
      TabIndex        =   3
      Top             =   360
      Width           =   2445
   End
   Begin VB.Shape Shape2 
      BorderStyle     =   0  'Transparent
      FillColor       =   &H0000C0C0&
      FillStyle       =   0  'Solid
      Height          =   105
      Left            =   420
      Top             =   600
      Width           =   2295
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Record#"
      Height          =   255
      Left            =   900
      TabIndex        =   2
      Top             =   960
      Width           =   1035
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00808080&
      BackStyle       =   1  'Opaque
      Height          =   405
      Left            =   390
      Shape           =   4  'Rounded Rectangle
      Top             =   330
      Width           =   2445
   End
End
Attribute VB_Name = "f_table"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const gc_red = vbRed '&HFF&
Const gc_green = &HFF00&
Const gc_gray = &H808080
Const gc_yellow = &HFFFF&
Const gc_dyellow = 32896
Const gc_cyan = 16776960
Const gc_dcyan = 8421376
Const gc_violet = 16711935
Const gc_dviolet = 8388736

Dim mv_Fast%, mv_Filename$, mv_FName$
 
Sub DnTable(Fil$, Optional Fast%)
    mv_Filename$ = Fil$
    mv_FName$ = Mid$(Fil$, InStrRev(Fil$, "\") + 1)
    mv_Fast% = Fast%
    gv_Invalid% = 1
    Me.Show 1
End Sub

Private Sub book90(st As String, co As Long)
    If Comm_Err% > 0 Then
        Label1 = "Comm Error"
        Shape1.BackColor = gc_red
        Exit Sub
    End If
    
    If Flg_Cancel% = 0 Then
        TX ("$" & Chr$(7) & Chr$(7))
        Label1 = st & " - OK"
        Shape1.BackColor = gc_cyan
        gv_Invalid% = 0
        Exit Sub
    End If
    
        Label1 = st & " Stopped"
        Shape1.BackColor = gc_gray

End Sub
Private Sub book_fe()
    Label1 = "File Error!"
    Shape1.BackColor = vbRed
End Sub
Private Sub dtable()
    On Error GoTo dpr90
    
    Open mv_Filename$ For Input As #1
    f1& = LOF(1) ' / Shape1.Width
    f2& = 0
    Shape2.Width = 0
    'ent_book.AddItem ent_book.Text
    'List_Add ent_book
    
    F_Main.RVR_Insert Me
    If Flg_Cancel% Then
        Close #1
        Exit Sub
    End If
    
    Label1 = "Downloading..."
    Shape1.BackColor = gc_yellow
    DoEvents

    send3 ("@DOWNLOADING...")
    send3 ("$File: " & mv_FName$)
    send3 ("x") 'beep
    'If comm_err% Then GoTo dpr80
    
    'HEADER
    ent_rec.Text = "Table"
    send3 ("U" & mv_FName$)
    'If comm_err% Then GoTo dpr80

    'RECORDS
    i% = 0
    While (Not EOF(1)) And (Flg_Cancel% = 0) And (Comm_Err% = 0)
        Line Input #1, a$
        f2& = f2& + Len(a$)
        Shape2.Width = f2& / f1& * Shape1.Width
        
        i% = i% + 1
        a1$ = Format(i%, "000#") 'Right$(Str$(i% + 10000), 4)
        ent_rec.Text = a1$
        
        If mv_Fast% = 0 Then
            send3 ("V" & a1$ & Chr$(4) & a$)
        Else
            send3 ("v" & a1$ & a$)
        End If
    Wend
    
    If Comm_Err% > 0 Then
        Label1 = "Comm Error! " & CStr(Comm_Err%)
        Shape1.BackColor = gc_red

    ElseIf Flg_Cancel% = 1 Then
        'Label1 = "Cancelled"
        'Shape1.BackColor = gc_violet
        send3 ("X" & a1$ & Chr$(4))
        send3 ("@DOWNLOAD-Cancel")
        send3 ("$File: " & mv_FName$)
        send3 ("x") 'beep
        send3 ("x") 'beep
        send3 ("x") 'beep
        
    ElseIf Flg_Cancel% = 0 Then
        Label1 = "Download -OK!"
        Shape1.BackColor = gc_cyan
        Shape2.Visible = False
        send3 ("W" & a1$ & Chr$(4))
        send3 ("@DOWNLOAD -OK")
        send3 ("$File: " & mv_FName$)
        send3 ("x") 'beep
        send3 ("x") 'beep
        
    End If
    
    'Call book90("Download", gc_yellow)
    Close #1
    Exit Sub
    
dpr90:
    book_fe
    Close #1
End Sub

Private Sub btn_stop_Click()
    Flg_Cancel% = 1
    Unload Me
End Sub

Private Sub Form_Activate()
    dtable
    btn_stop.Caption = "Close"
End Sub

Private Sub Form_Load()
    Label1 = "- - -"
    Shape2.Width = 0
End Sub
