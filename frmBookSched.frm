VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmBookSched 
   Caption         =   "Edit Books"
   ClientHeight    =   2850
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3285
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2850
   ScaleWidth      =   3285
   Begin VB.CommandButton cmdDelete 
      Caption         =   "&Delete"
      Enabled         =   0   'False
      Height          =   390
      Left            =   1650
      TabIndex        =   3
      Top             =   2265
      Width           =   675
   End
   Begin VB.CommandButton cmdClose 
      Cancel          =   -1  'True
      Caption         =   "&Close"
      Height          =   390
      Left            =   2355
      TabIndex        =   4
      Top             =   2265
      Width           =   675
   End
   Begin VB.CommandButton cmdEdit 
      Caption         =   "&Edit"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   390
      Left            =   945
      TabIndex        =   2
      Top             =   2265
      Width           =   675
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "&Add"
      Height          =   390
      Left            =   240
      TabIndex        =   1
      Top             =   2265
      Width           =   675
   End
   Begin VB.Frame fraSelect 
      Caption         =   " Select Book No."
      Height          =   2025
      Left            =   120
      TabIndex        =   5
      Top             =   60
      Width           =   3045
      Begin MSComctlLib.ListView lsvFileMain 
         Height          =   1605
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   2805
         _ExtentX        =   4948
         _ExtentY        =   2831
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Book No."
            Object.Width           =   4762
         EndProperty
      End
   End
End
Attribute VB_Name = "frmBookSched"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAdd_Click()
Me.Enabled = False
If lsvFileMain.ListItems.Count = 5 Then
    MsgBox "Only five book nos. are allowed."
    Exit Sub
End If

    g_str_AddEdit = "ADD"
    frmAddBookFile.Show
End Sub

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub cmdDelete_Click()
    Dim l_str_Sql As String
    Dim lsql As String
    Dim i As Integer
    
    lsql = "select schedstat from T_SCHED where bookno='" & lsvFileMain.SelectedItem.Text & "' and schedstat='P' and HHTNO=(select HHTNO from F_HH where hhid='" & frmAddSched.cmbRoverNo & "')"
    If OpenRecordset(g_rs_TSCHED, lsql, "frmBookSched", "Host") Then
        If g_rs_TSCHED.Fields(0) = "P" Then
            MsgBox "Record cannot be deleted, book already uploaded.", vbExclamation, "Edit Book"
            Exit Sub
        End If
    Else
        If Not ListViewDelete(lsvFileMain, 1) Then Exit Sub
        For i = 1 To lsvFileMain.ListItems.Count
            l_str_Sql = "UPDATE T_BOOK SET BOOKSTAT = '', DLROVERDT= '0:00' WHERE BOOKNO = '" & lsvFileMain.ListItems(i) & "'"
            If Not DBExecute(l_str_Sql, Me.Name, "MRU Management") Then Exit Sub
            l_str_Sql = "UPDATE T_BKINFO SET READERNAME = '', RDGDATE= '' WHERE BOOKNO = '" & lsvFileMain.ListItems(i) & "'"
            If Not DBExecute(l_str_Sql, Me.Name, "MRU Management") Then Exit Sub
            l_str_Sql = "UPDATE T_SCHED SET SCHEDSTAT='D', ALT_HHTNO='' WHERE BOOKNO = '" & lsvFileMain.ListItems(i) & "' and HHTNO=(select HHTNO from F_HH where hhid='" & frmAddSched.cmbRoverNo & "')"
            If Not DBExecute(l_str_Sql, Me.Name, "cmdDelete_Click") Then Exit Sub
        Next i
            l_str_Sql = "DELETE FROM T_SCHED WHERE BOOKNO = '" & lsvFileMain.SelectedItem.Text & "' and HHTNO=(select HHTNO from F_HH where hhid='" & frmAddSched.cmbRoverNo & "')"
            If Not DBExecute(l_str_Sql, Me.Name, "cmdDelete_Click") Then Exit Sub
        
        lsvFileMain.ListItems.Remove (lsvFileMain.SelectedItem.Index)
        lsvFileMain.Refresh
        If lsvFileMain.ListItems.Count < 1 Then
            cmdEdit.Enabled = False
            cmdDelete.Enabled = False
        End If
        
        If lsvFileMain.ListItems.Count = 5 Then
            cmdAdd.Enabled = False
        Else
            cmdAdd.Enabled = True
        End If
    
        lsvFileMain.SetFocus
    End If
End Sub

Private Sub cmdEdit_Click()
    Dim lsql As String
    
    lsql = "select schedstat from T_SCHED where bookno='" & lsvFileMain.SelectedItem.Text & "' and schedstat='P' and HHTNO=(select HHTNO from F_HH where hhid='" & frmAddSched.cmbRoverNo & "')"
    If OpenRecordset(g_rs_TSCHED, lsql, "frmBookSched", "Host") Then
        If g_rs_TSCHED.Fields(0) = "P" Then
            MsgBox "Record cannot be modified, book already uploaded.", vbExclamation, "Edit Book"
            Exit Sub
        End If
    Else
        Me.Enabled = False
        g_str_AddEdit = "EDIT"
        frmAddBookFile.Show
    End If
End Sub

Private Sub Form_Activate()
Dim l_int_Index As Integer
    'testing
If WindowState = vbNormal Then
    Me.Move (1000)
    Me.Top = 500
End If

fraSelect.Caption = "Day" & " " & frmAddSched.flxSched.Col + 1
    ListView_Load
    If lsvFileMain.ListItems.Count = 5 Then
        cmdAdd.Enabled = False
    End If

End Sub

Private Sub ListView_Load()
    Dim lobjItem As Object
    Dim l_str_Sql As String
    
   l_str_Sql = "select bookno, schedstat from T_SCHED where HHTNO=(select HHTNO from F_HH where hhid='" & frmAddSched.cmbRoverNo & "') and DAYNO=" & frmAddSched.flxSched.Col + 1 & ""
      
    lsvFileMain.ListItems.Clear
    If Not OpenRecordset(g_rs_RFF, l_str_Sql, Me.Name, "ListView_Load") Then Exit Sub
    
    'disable add button if book already uploaded
    If g_rs_RFF.Fields(1) = "P" Then
        cmdAdd.Enabled = False
    End If

    g_rs_RFF.MoveFirst
    While Not g_rs_RFF.EOF
        Set lobjItem = lsvFileMain.ListItems.Add(, , CheckNull(g_rs_RFF.Fields(0)))
            'lobjItem.SubItems(1) = CheckNull(g_rs_RFF.Fields(6))
            'lobjItem.SubItems(2) = CheckNull(g_rs_RFF.Fields(7))
        Set lobjItem = Nothing
        g_rs_RFF.MoveNext
    Wend

    g_rs_RFF.Close
    Set g_rs_RFF = Nothing
    
    If lsvFileMain.ListItems.Count > 0 Then
        cmdEdit.Enabled = True
        cmdDelete.Enabled = True
    Else
        cmdEdit.Enabled = False
        cmdDelete.Enabled = False
    End If

End Sub

Private Sub Form_Resize()
If WindowState = vbNormal Then
    Me.Width = 3405
    Me.Height = 3360
End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmAddSched.Enabled = True
End Sub

