VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmScheduler 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Scheduler"
   ClientHeight    =   9645
   ClientLeft      =   45
   ClientTop       =   885
   ClientWidth     =   15150
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9645
   ScaleWidth      =   15150
   Begin VB.Frame Frame2 
      Caption         =   "Stat Legend:"
      Height          =   615
      Left            =   5880
      TabIndex        =   8
      Top             =   8760
      Width           =   3495
      Begin VB.Label Label3 
         Caption         =   "P - Posting"
         Height          =   255
         Left            =   2520
         TabIndex        =   11
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "U - Uploading"
         Height          =   255
         Left            =   1440
         TabIndex        =   10
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "D - Downloading"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   1335
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Functions"
      Height          =   1095
      Left            =   1200
      TabIndex        =   1
      Top             =   8520
      Width           =   12735
      Begin VB.CommandButton cmdAdd 
         Caption         =   "&Add"
         Height          =   390
         Left            =   720
         TabIndex        =   7
         Top             =   360
         Width           =   915
      End
      Begin VB.CommandButton cmdEdit 
         Caption         =   "&Edit"
         Height          =   390
         Left            =   1920
         TabIndex        =   6
         Top             =   360
         Width           =   915
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "&Delete"
         Height          =   390
         Left            =   3120
         TabIndex        =   5
         Top             =   360
         Width           =   915
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "&Close"
         Height          =   390
         Left            =   11160
         TabIndex        =   4
         Top             =   360
         Width           =   915
      End
      Begin VB.CommandButton cmdDownload 
         Caption         =   "Do&wnload"
         Height          =   390
         Left            =   8760
         TabIndex        =   3
         Top             =   360
         Width           =   915
      End
      Begin VB.CommandButton cmdUpload 
         Caption         =   "&Upload"
         Height          =   390
         Left            =   9960
         TabIndex        =   2
         Top             =   360
         Width           =   915
      End
   End
   Begin MSComctlLib.ListView lsvSched 
      Height          =   7695
      Left            =   90
      TabIndex        =   0
      Top             =   600
      Width           =   15015
      _ExtentX        =   26485
      _ExtentY        =   13573
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   36
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "BC"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "BC Desc"
         Object.Width           =   2794
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Status"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Current"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Next"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Day 1"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "Day 2"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "Day 3"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Text            =   "Day 4"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "Day 5"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Text            =   "Day 6"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Text            =   "Day 7"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Text            =   "Day 8"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   13
         Text            =   "Day 9"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   14
         Text            =   "Day 10"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   15
         Text            =   "Day 11"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   16
         Text            =   "Day 12"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   17
         Text            =   "Day 13"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   18
         Text            =   "Day 14"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(20) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   19
         Text            =   "Day 15"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(21) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   20
         Text            =   "Day 16"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(22) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   21
         Text            =   "Day 17"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(23) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   22
         Text            =   "Day 18"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(24) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   23
         Text            =   "Day 19"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(25) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   24
         Text            =   "Day 20"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(26) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   25
         Text            =   "Day 21"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(27) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   26
         Text            =   "Day 22"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(28) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   27
         Text            =   "Day 23"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(29) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   28
         Text            =   "Day 24"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(30) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   29
         Text            =   "Day 25"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(31) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   30
         Text            =   "Day 26"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(32) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   31
         Text            =   "Day 27"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(33) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   32
         Text            =   "Day 28"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(34) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   33
         Text            =   "Day 29"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(35) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   34
         Text            =   "Day 30"
         Object.Width           =   1397
      EndProperty
      BeginProperty ColumnHeader(36) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   35
         Text            =   "Day 31"
         Object.Width           =   1397
      EndProperty
   End
   Begin VB.Label lblCycle 
      Caption         =   "XX"
      Height          =   255
      Left            =   2160
      TabIndex        =   13
      Top             =   240
      Width           =   495
   End
   Begin VB.Label Label4 
      Caption         =   "CYCLE MONTH :"
      Height          =   255
      Left            =   480
      TabIndex        =   12
      Top             =   240
      Width           =   1455
   End
End
Attribute VB_Name = "frmScheduler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Form-level variable to get the day number that is current for a particular rover/reader schedule
Dim sked As Integer

Private Sub cmdAdd_Click()
'    frmAddSched.Caption = "Add Schedule"
'    frmAddSched.Show
Me.Enabled = False
On Error GoTo errhandler
    frmAddSched.load_Sched_Add lsvSched.SelectedItem.Text, lsvSched.SelectedItem.SubItems(1)
errhandler:
If Err.Number = 91 Then
    frmAddSched.load_Sched_Add "", ""
End If
End Sub

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub cmdCopy_Click()
    With frmCopySched
        .Caption = "Copy Schedule"
        .lblCopy = "Copy"
        .lblTo = "to"
    End With
    frmCopySched.Show
End Sub

Private Sub cmdDelete_Click()
    Dim l_str_Sql As String
    Dim delkey As String
    Dim lsql As String
    
On Error GoTo errhandler

    lsql = "select SCHEDSTAT from T_SCHED WHERE SCHEDSTAT='P' AND HHTNO=(select HHTNO from F_HH where hhid='" & lsvSched.SelectedItem.Text & "')"
    If OpenRecordset(g_rs_TSCHED, lsql, "frmScheduler", "Scheduler") Then
        If g_rs_TSCHED.Fields(0) = "P" Then
            MsgBox "Record cannot be deleted, book already uploaded.", vbExclamation, "Edit Book"
            Exit Sub
        End If
    Else
        delkey = MsgBox("Will delete Rover No. " & lsvSched.SelectedItem.Text, vbOKCancel, "Delete")
        If delkey = vbOK Then
            l_str_Sql = "UPDATE T_BOOK SET BOOKSTAT = '', DLROVERDT= '0:00' WHERE BOOKNO IN (select bookno from T_SCHED where HHTNO = (SELECT HHTNO FROM F_HH WHERE HHID = '" & lsvSched.SelectedItem.Text & "' ))"
            If Not DBExecute(l_str_Sql, Me.Name, "cmdDelete_Click") Then Exit Sub
            l_str_Sql = "DELETE FROM T_SCHED WHERE HHTNO = (SELECT HHTNO FROM F_HH WHERE HHID = '" & lsvSched.SelectedItem.Text & "' )"
            If Not DBExecute(l_str_Sql, Me.Name, "cmdDelete_Click") Then Exit Sub
            l_str_Sql = "UPDATE T_BKINFO SET READERNAME = '', RDGDATE= '' WHERE BOOKNO IN (select bookno from T_SCHED where HHTNO = (SELECT HHTNO FROM F_HH WHERE HHID = '" & lsvSched.SelectedItem.Text & "' ))"
            If Not DBExecute(l_str_Sql, Me.Name, "cmdDelete_Click") Then Exit Sub
            lsvSched.ListItems.Remove (lsvSched.SelectedItem.Index)
            lsvSched.Refresh
            lsvSched.SetFocus
        Else
            Exit Sub
        End If
                
        If lsvSched.ListItems.Count = 0 Then
            frmScheduler.cmdEdit.Enabled = False
            cmdDownload.Enabled = False
            cmdUpload.Enabled = False
        End If
    End If
    
errhandler:
If Err.Number = 91 Then
    MsgBox "No record to delete.", , "System Message"
ElseIf Err.Number <> 0 Then
    MsgBox Err.Description, vbExclamation, "Error Message"
End If
End Sub

Private Sub cmdDownload_Click()
    Dim bResult As Boolean
    Dim BookNo As String
    Dim RoverNo As String
    Dim ReaderName As String
    Dim lsql As String
    
    BookNo = Right(Trim(lsvSched.SelectedItem.SubItems(3)), lBookNo)
    RoverNo = lsvSched.SelectedItem.Text
    ReaderName = lsvSched.SelectedItem.SubItems(1)
    
    lsql = "select top 1 dayno from T_SCHED where schedstat<> 'P' and hhtno=(select hhtno from F_HH where hhid= '" & RoverNo & "') and readerno=(select readerno from r_reader where rdrname= '" & ReaderName & "') GROUP BY  DAYNO ORDER BY DAYNO"
    OpenRecordset g_rs_TSCHED, lsql, "frmScheduler", "Rover Management"
    
    sked = g_rs_TSCHED.Fields(0)
    If (BookNo = "") Then
        MsgBox "Please select a valid book to process!!", vbCritical, "Rover Download"
    Else
        ItinDload
    End If

End Sub

Sub ItinDload()
    Dim lsql As String
    Dim BookNo As String
    Dim RoverNo As String
    Dim ReaderName As String
    Dim ItiArray(5) As String
    Dim i As Integer
    Dim sPath As String
    Dim l_sql As String
    
    RoverNo = lsvSched.SelectedItem.Text
    ReaderName = lsvSched.SelectedItem.SubItems(1)

    lsql = "select bookno from T_SCHED where dayno= " & sked & " and hhtno='" & RoverNo & "'"
    OpenRecordset g_rs_TSCHED, lsql, "frmScheduler", "Rover Management"
    
    i = 0
    For i = 0 To 4
        While Not g_rs_TSCHED.EOF
            ItiArray(i) = g_rs_TSCHED.Fields(0)
            l_sql = "select bookno from T_BKINFO where bookno='" & ItiArray(i) & "'"
            If OpenRecordset(g_rs_TBKINFO, l_sql, "frmScheduler", "ItinDload") Then
            Else
                MsgBox "Please download MRU " & ItiArray(i) & " from host first!", vbCritical, "Error Message"
                Exit Sub
            End If
            i = i + 1
            g_rs_TSCHED.MoveNext
        Wend
    Next i

    If lsvSched.SelectedItem.SubItems(3) = "MULTIBOOK" Then
        sPath = App.Path & "\MULTI"
    Else
        sPath = App.Path & "\CURRENT"
    End If
    
    BookNo = Right(Trim(ItiArray(0)), lBookNo)
    If CreateDLFileMulti(ItiArray, ReaderName, sPath) Then
            If Download(BookNo, RoverNo, sPath) = "SUCCESS" Then
                UpdateDownload RoverNo
                MsgBox "Download Successful!", vbInformation, "Rover Download"
                lsvSched.SelectedItem.SubItems(2) = "U"
                lsvSched.Refresh
                    If lsvSched.SelectedItem.SubItems(2) = "D" Then
                        cmdUpload.Enabled = False
                        cmdDownload.Enabled = True
                    Else
                        cmdUpload.Enabled = True
                        cmdDownload.Enabled = False
                    End If
            Else
                MsgBox "Download Failed or Aborted!", vbCritical, "Rover Download"
            End If
'        MsgBox "Download Successful!", vbInformation, "Rover Download"
    Else
        MsgBox "Download Failed or Aborted!", vbCritical, "Rover Download"
    End If

End Sub

Sub UpdateUpload(ByVal ULRoverNo As String)
    Dim l_str_Sql As String, lsql As String, l_sql As String
    Dim HHNo As String
    Dim BookNo As String
    Dim RoverNo As String
    Dim ItiArray(5) As String
    Dim i As Integer
    
    RoverNo = lsvSched.SelectedItem.Text

    lsql = "select HHTNO from F_HH where hhtno='" & RoverNo & "'"
    If OpenRecordset(g_rs_RHH, lsql, "frmScheduler", "Rover Management") Then
        HHNo = g_rs_RHH.Fields(0)
    Else
        HHNo = ""
    End If
    
    l_sql = "select bookno from T_SCHED where dayno= " & sked & " and hhtno='" & RoverNo & "'"
    OpenRecordset g_rs_TSCHED, l_sql, "frmScheduler", "Rover Management"
    
    i = 0
    For i = 0 To 4
        While Not g_rs_TSCHED.EOF
            ItiArray(i) = g_rs_TSCHED.Fields(0)
            l_str_Sql = "UPDATE T_SCHED SET SCHEDSTAT = 'P' WHERE HHTNO = '" & HHNo & "' and BOOKNO IN ('" & ItiArray(i) & "')"
            If Not DBExecute(l_str_Sql, Me.Name, "Rover Management") Then Exit Sub
            'l_str_Sql = "UPDATE T_BOOK SET BOOKSTAT = 'OK' WHERE BOOKNO IN ('" & ItiArray(i) & "')"
            l_str_Sql = "UPDATE T_BOOK SET BOOKSTAT = 'OK', UPROVERDT = Now() WHERE BOOKNO IN ('" & ItiArray(i) & "')"
            If Not DBExecute(l_str_Sql, Me.Name, "Rover Management") Then Exit Sub
            i = i + 1
            g_rs_TSCHED.MoveNext
        Wend
    Next i

Refresh_Sched

End Sub

Sub UpdateDownload(ByVal DLRoverNo As String)
    Dim l_str_Sql As String, lsql As String, l_sql As String
    Dim HHNo As String
    Dim BookNo As String
    Dim RoverNo As String
    Dim ItiArray(5) As String
    Dim i As Integer
    
    RoverNo = lsvSched.SelectedItem.Text

    lsql = "select HHTNO from F_HH where hhtno='" & RoverNo & "'"
    If OpenRecordset(g_rs_RHH, lsql, "frmScheduler", "Rover Management") Then
        HHNo = g_rs_RHH.Fields(0)
    Else
        HHNo = ""
    End If
    
    lsql = "select bookno from T_SCHED where dayno= " & sked & " and hhtno='" & RoverNo & "'"
    OpenRecordset g_rs_TSCHED, lsql, "frmScheduler", "Rover Management"
    
    i = 0
    For i = 0 To 4
        While Not g_rs_TSCHED.EOF
            ItiArray(i) = g_rs_TSCHED.Fields(0)
            '  If the book was downloaded to the assigned Rover No, just update status,
            ' otherwise place the new rover number in the alternate slot
            If HHNo = DLRoverNo Then
                l_str_Sql = "UPDATE T_SCHED SET SCHEDSTAT = 'U' WHERE HHTNO = '" & HHNo & "' and BOOKNO IN ('" & ItiArray(i) & "')"
            Else
                l_str_Sql = "UPDATE T_SCHED SET SCHEDSTAT = 'U', ALT_HHTNO = '" & DLRoverNo & "' WHERE HHTNO = '" & HHNo & "' and BOOKNO IN ('" & ItiArray(i) & "')"
            End If
            If Not DBExecute(l_str_Sql, Me.Name, "Rover Management") Then Exit Sub
            'l_str_Sql = "UPDATE T_BOOK SET BOOKSTAT = 'NO' WHERE BOOKNO IN ('" & ItiArray(i) & "')"
            l_str_Sql = "UPDATE T_BOOK SET BOOKSTAT = 'NO', DLROVERDT = Now() WHERE BOOKNO IN ('" & ItiArray(i) & "')"
            If Not DBExecute(l_str_Sql, Me.Name, "Rover Management") Then Exit Sub
            i = i + 1
            g_rs_TSCHED.MoveNext
        Wend
    Next i

End Sub

Private Sub cmdEdit_Click()
'    frmAddSched.Caption = "Edit Schedule"
'    frmAddSched.Show
Me.Enabled = False
frmAddSched.load_Sched_Edit lsvSched.SelectedItem.Text, lsvSched.SelectedItem.SubItems(1)
End Sub

Private Sub cmdMulti_Click()
    frmMultiBookSched.Show
End Sub

Private Sub cmdUpload_Click()
    Dim bResult As Boolean
    Dim BookNo As String
    Dim Alt_HHTNo As String
    Dim RoverNo As String
    Dim ReaderName As String
    Dim sPath As String
    Dim l_sql As String
    
    BookNo = Right(Trim(lsvSched.SelectedItem.SubItems(3)), lBookNo)
    RoverNo = lsvSched.SelectedItem.Text
    ReaderName = lsvSched.SelectedItem.SubItems(1)
    sPath = App.Path & "\CURRENT"
    
    If RecordExist Then Exit Sub
        
    l_sql = "select schedstat from T_SCHED where bookno='" & lsvSched.SelectedItem.SubItems(3) & "' and schedstat='P'"
    If OpenRecordset(g_rs_TSCHED, l_sql, "frmBookSched", "Host") Then
        If g_rs_TSCHED.Fields(0) = "P" Then
            MsgBox "This book has been uploaded.", vbExclamation, "Edit Book"
            Exit Sub
        End If
    Else
        If (BookNo = "") Then
            MsgBox "Please select a valid book to process!!", vbCritical, "Rover Upload"
        Else
            'Get day number from sched that is current!
            lsql = "select top 1 dayno from T_SCHED where schedstat<> 'P' and hhtno=(select hhtno from F_HH where hhid= '" & RoverNo & "') and readerno=(select readerno from r_reader where rdrname= '" & ReaderName & "') GROUP BY  DAYNO ORDER BY DAYNO"
            OpenRecordset g_rs_TSCHED, lsql, "frmScheduler", "Rover Management"
            
            sked = g_rs_TSCHED.Fields(0)
            
            l_sql = "select bookno, alt_hhtno from T_SCHED where dayno= " & sked & " and hhtno = '" & RoverNo & "'"
            ' Multibook
            If lsvSched.SelectedItem.SubItems(3) = "MULTIBOOK" Then
                OpenRecordset g_rs_TSCHED, l_sql, "frmScheduler", "Rover Management"
                BookNo = Right(Trim(g_rs_TSCHED.Fields(0)), lBookNo)
'                Alt_HHTNo = Trim(g_rs_TSCHED.Fields(1)) & ""
'                If (Not IsNull(Alt_HHTNo) And Alt_HHTNo <> "") Then
                Alt_HHTNo = IIf(IsNull(Trim(g_rs_TSCHED.Fields(1))) = True, "", Trim(g_rs_TSCHED.Fields(1)))
                If (Not IsNull(Alt_HHTNo) And Alt_HHTNo <> "") Then
                    RoverNo = Alt_HHTNo
                End If
                sPath = App.Path & "\MULTI"
            Else
            ' Single book
                OpenRecordset g_rs_TSCHED, l_sql, "frmScheduler", "Rover Management"
'                Alt_HHTNo = Trim(g_rs_TSCHED.Fields(1)) & ""
'                If (Not IsNull(Alt_HHTNo) And Alt_HHTNo <> "") Then
                Alt_HHTNo = IIf(IsNull(Trim(g_rs_TSCHED.Fields(1))) = True, "", Trim(g_rs_TSCHED.Fields(1)))
                If (Not IsNull(Alt_HHTNo) And Alt_HHTNo <> "") Then
                    RoverNo = Alt_HHTNo
                End If
            End If
            If Upload(BookNo, RoverNo, sPath) = "SUCCESS" Then
                If UploadToDB(BookNo, sPath) = True Then
                    UpdateUpload RoverNo
                    MsgBox "Upload Successful!", vbInformation, "Rover Upload"
                    'BookNo = ""
                    'lsvSched.SelectedItem.SubItems(2) = "P"
                    lsvSched.Refresh
                        If lsvSched.SelectedItem.SubItems(2) = "P" Then
                            cmdUpload.Enabled = False
                            cmdDownload.Enabled = False
                        Else
                            cmdUpload.Enabled = True
                            cmdDownload.Enabled = True
                        End If
                End If
            Else
                MsgBox "Upload Failed or Aborted!", vbCritical, "Rover Upload"
            End If
        End If
    End If
End Sub

Function RecordExist() As Boolean
    Dim l_str_Sql As String
    Dim RoverNo As String
    Dim ItiArray(5) As String
    Dim n As Integer
    Dim BookNo As String
    Dim ReaderName As String
    
    BookNo = Right(Trim(lsvSched.SelectedItem.SubItems(3)), lBookNo)
    RoverNo = lsvSched.SelectedItem.Text
    ReaderName = lsvSched.SelectedItem.SubItems(1)

    'Get day number from sched that is current!
    lsql = "select top 1 dayno from T_SCHED where schedstat<> 'P' and hhtno=(select hhtno from F_HH where hhid= '" & RoverNo & "') and readerno=(select readerno from r_reader where rdrname= '" & ReaderName & "') GROUP BY  DAYNO ORDER BY DAYNO"
    OpenRecordset g_rs_TSCHED, lsql, "frmScheduler", "Rover Management"
    
    sked = g_rs_TSCHED.Fields(0)

    lsql = "select bookno from T_SCHED where dayno= " & sked & " and hhtno='" & RoverNo & "'"
    OpenRecordset g_rs_TSCHED, lsql, "frmScheduler", "Rover Management"
    
    i = 0
    For i = 0 To 4
        While Not g_rs_TSCHED.EOF
            ItiArray(i) = g_rs_TSCHED.Fields(0)
            i = i + 1
            g_rs_TSCHED.MoveNext
        Wend
    Next i
            
    n = 0
    For n = 0 To 4
        l_str_Sql = "SELECT BOOKNO FROM T_UPLOAD WHERE BOOKNO = '" & ItiArray(n) & "'" 'and HHTNO=(select HHTNO from F_HH where hhid='" & frmAddSched.cmbRoverNo & "')"    ' and DAYNO=" & frmAddSched.flxSched.Col + 1 & "
        If OpenRecordset(g_rs_TUPLOAD, l_str_Sql, Me.Name, "RecordExist") Then
            MsgBox "MRU " & ItiArray(n) & " already uploaded!", vbExclamation, "Rover Upload"
            g_rs_TUPLOAD.Close
            Set g_rs_TUPLOAD = Nothing
            RecordExist = True
            Exit Function
        End If
    Next n

    RecordExist = False
        
End Function

Sub Refresh_Sched()

    '  Refresh Scheduler Display
    '    by Dareen Maguad
    '  Updates status of downloaded/uploaded books and schedules next books for download

    Dim lobjItem As Object
    Dim cnt As Integer
    'Dim sked As Integer

    On Error GoTo errhandler

    ' Select all available schedules from T_SCHED to lay out on the MSFlexGrid
    SQL = "select hhid,rdrname,currflag,bookno,dayno,schedstat from F_HH right join (T_SCHED left join R_READER on T_SCHED.readerno=R_READER.readerno) on F_HH.hhtno = T_SCHED.hhtno" & _
        " where T_SCHED.readerno = R_READER.readerno " & _
        " and F_HH.hhtno in (select T_SCHED.hhtno from T_SCHED group by T_SCHED.hhtno) " & _
        " order by T_SCHED.hhtno, T_SCHED.dayno"
    OpenRecordset g_rs_TSCHED, SQL, "frmScheduler", "Rover Management"

    lsvSched.ListItems.Clear
    
    temphhid = ""
    
'     MSFlexGrid lobjItem.SubItems(n) columns, where n = :
'       0 - Rover
'       1 - Reader
'       2 - Stat
'       3 - Current Book
'       4 - Next Book
'       5 to 35 - Day n-4  (see logic of lobjItem.SubItems(n) + 4)

'       g_rs_TSCHED Recordset:
'       g_rs_TSCHED.Fields(0) HHT ID
'       g_rs_TSCHED.Fields(1) Reader Name
'       g_rs_TSCHED.Fields(2) Current Flag (not used)
'       g_rs_TSCHED.Fields(3) Book Number
'       g_rs_TSCHED.Fields(4) Day Number
'       g_rs_TSCHED.Fields(5) Status (D = for download, U = for upload, P = for posting to host)
    
    ' Begin Loop
    While Not g_rs_TSCHED.EOF
        ' Control break for the scheduled books of a particular rover/reader combination
        If temphhid <> g_rs_TSCHED.Fields(0) Then
            Set lobjItem = lsvSched.ListItems.Add(, , g_rs_TSCHED.Fields(0))
            temphhid = g_rs_TSCHED.Fields(0)
            cnt = 0
        End If
        ' Reader Name
        lobjItem.SubItems(1) = g_rs_TSCHED.Fields(1)
        
        ' Current Book
        If g_rs_TSCHED.Fields(2) Then
            'lobjItem.SubItems(3) = g_rs_TSCHED.Fields(4)
            lobjItem.SubItems(3) = g_rs_TSCHED.Fields(3)
        End If
        
        ' Current and Next Scheduled Books (Single)
        If lsvSched.ListItems(lsvSched.ListItems.Count).SubItems(g_rs_TSCHED.Fields(4) + 4) = "" Then
            If g_rs_TSCHED.Fields(5) = "D" Or g_rs_TSCHED.Fields(5) = "U" Then
                ' Get first instance with status <> 'P', then assign to Current
                cnt = cnt + 1
                If cnt = 1 Then
                    lobjItem.SubItems(2) = g_rs_TSCHED.Fields(5)
                    lobjItem.SubItems(3) = g_rs_TSCHED.Fields(3)
                    sked = g_rs_TSCHED.Fields(4)
                ' next instance with status <> 'P', assign to Next
                ElseIf cnt = 2 Then
                    lobjItem.SubItems(4) = g_rs_TSCHED.Fields(3)
                End If
            End If
            ' Other day numbers in schedule
            lobjItem.SubItems(g_rs_TSCHED.Fields(4) + 4) = g_rs_TSCHED.Fields(3)
        Else
        ' Current and Next Scheduled Books (MULTIBOOK)
            If g_rs_TSCHED.Fields(5) = "D" Or g_rs_TSCHED.Fields(5) = "U" Then
                ' Get first instance with status <> 'P", then assign to Current
                If cnt = 1 Then
                    lobjItem.SubItems(2) = g_rs_TSCHED.Fields(5)
                    lobjItem.SubItems(3) = "MULTIBOOK"
                    sked = g_rs_TSCHED.Fields(4)
               ' next instance with status <> 'P', assign to Next
                ElseIf cnt = 2 Then
                    lobjItem.SubItems(4) = "MULTIBOOK"
                End If
            End If
            ' Other day numbers in schedule
            lobjItem.SubItems(g_rs_TSCHED.Fields(4) + 4) = "MULTIBOOK"
        End If
               
        g_rs_TSCHED.MoveNext
    Wend
    ' End Loop
    
    ' Enable/Disable buttons
    If lsvSched.SelectedItem.SubItems(2) = "D" Then
        cmdUpload.Enabled = False
        cmdDownload.Enabled = True
    ElseIf lsvSched.SelectedItem.SubItems(2) = "P" Then
        cmdUpload.Enabled = False
        cmdDownload.Enabled = False
    Else
        cmdUpload.Enabled = True
        cmdDownload.Enabled = False
    End If

errhandler:
' Trap "Object Variable not set error" for no data in T_SCHEDULE table, since recordset returns 0 rows!
If Err.Number = 91 Then
    cmdEdit.Enabled = False
    cmdDownload.Enabled = False
    cmdUpload.Enabled = False
End If

End Sub

Private Sub Form_Activate()
    Me.Move (0)
    Me.Top = 0
    Refresh_Sched
End Sub

Private Sub Form_Load()
'    Dim lobjItem As Object
''    Set lobjItem = lsvSched.ListItems.Add(, , "043426")
''        lobjItem.SubItems(1) = "Juan dela Cruz"
''        lobjItem.SubItems(2) = "D"
''        lobjItem.SubItems(3) = "3"
''        lobjItem.SubItems(4) = ""
''        lobjItem.SubItems(5) = ""
''        lobjItem.SubItems(6) = "00011"
''        lobjItem.SubItems(7) = "00012"
''    Set lobjItem = lsvSched.ListItems.Add(, , "045695")
''        lobjItem.SubItems(1) = "Pedro Santos"
''        lobjItem.SubItems(2) = "D"
''        lobjItem.SubItems(3) = "1"
''        lobjItem.SubItems(4) = "Multi"
''        lobjItem.SubItems(5) = "00006"
''        lobjItem.SubItems(6) = ""
''        lobjItem.SubItems(7) = "00012"
''    Set lobjItem = lsvSched.ListItems.Add(, , "045697")
''        lobjItem.SubItems(1) = "Jose Rizal"
''        lobjItem.SubItems(2) = "U"
''        lobjItem.SubItems(3) = ""
''        lobjItem.SubItems(4) = ""
''        lobjItem.SubItems(5) = "00009"
''        lobjItem.SubItems(6) = "00011"
''        lobjItem.SubItems(7) = "00012"
'
''    sql = "select hhid,name,currflag,bookno,dayno from R_HH,T_SCHED,R_READER "
'On Error GoTo errhandler
'    SQL = "select hhid,name,currflag,bookno,dayno from R_HH right join (T_SCHED left join R_READER on T_SCHED.readerno=R_READER.readerno) on R_HH.hhno = T_SCHED.hhno" & _
'        " where T_SCHED.readerno = R_READER.readerno " & _
'        " and R_HH.hhno in (select T_SCHED.hhno from T_SCHED group by T_SCHED.hhno) " & _
'        " order by T_SCHED.hhno"
'    OpenRecordset g_rs_TSCHED, SQL, "frmScheduler", "Rover Management"
'
'    lsvSched.ListItems.Clear
'
'    temphhid = ""
'    While Not g_rs_TSCHED.EOF
'        If temphhid <> g_rs_TSCHED.Fields(0) Then
'            Set lobjItem = lsvSched.ListItems.Add(, , g_rs_TSCHED.Fields(0))
'            temphhid = g_rs_TSCHED.Fields(0)
'        End If
'        lobjItem.SubItems(1) = g_rs_TSCHED.Fields(1)
'        If g_rs_TSCHED.Fields(2) Then
'            lobjItem.SubItems(3) = g_rs_TSCHED.Fields(4)
'            lobjItem.SubItems(4) = g_rs_TSCHED.Fields(3)
'        Else
'        End If
'
'        If lsvSched.ListItems(lsvSched.ListItems.Count).SubItems(g_rs_TSCHED.Fields(4) + 5) = "" Then
'            lobjItem.SubItems(g_rs_TSCHED.Fields(4) + 5) = g_rs_TSCHED.Fields(3)
'        Else
'            lobjItem.SubItems(4) = "MULTIBOOK"
'            lobjItem.SubItems(g_rs_TSCHED.Fields(4) + 5) = "MULTIBOOK"
'        End If
'        g_rs_TSCHED.MoveNext
'    Wend
'errhandler:
'If Err.Number = 91 Then
'    cmdEdit.Enabled = False
'End If
End Sub

Private Sub lsvSched_ItemClick(ByVal Item As MSComctlLib.ListItem)
    If lsvSched.SelectedItem.SubItems(2) = "D" Then
        cmdUpload.Enabled = False
        cmdDownload.Enabled = True
    ElseIf lsvSched.SelectedItem.SubItems(2) = "U" Then
        cmdUpload.Enabled = True
        cmdDownload.Enabled = False
    Else
        cmdUpload.Enabled = False
        cmdDownload.Enabled = False
    End If

End Sub
