VERSION 5.00
Begin VB.Form frmAddBookFile 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Add Book Record"
   ClientHeight    =   1200
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3810
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1200
   ScaleWidth      =   3810
   Begin VB.ComboBox cmbBookNo 
      Height          =   315
      Left            =   1500
      TabIndex        =   3
      Text            =   "Combo1"
      Top             =   120
      Width           =   2000
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   390
      Left            =   2010
      TabIndex        =   1
      Top             =   615
      Width           =   915
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Save"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   390
      Left            =   960
      TabIndex        =   0
      Top             =   600
      Width           =   915
   End
   Begin VB.Label lblBookNo 
      Alignment       =   1  'Right Justify
      Caption         =   "Book No. : "
      Height          =   285
      Left            =   120
      TabIndex        =   2
      Top             =   130
      Width           =   1300
   End
End
Attribute VB_Name = "frmAddBookFile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub cmbBookNo_Click()
If cmbBookNo <> "" Then
    cmdOk.Enabled = True
End If

End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOk_Click()
    Dim l_obj_Item As Object
    Dim l_int_Index As Integer
    Dim lsql As String
    
    lsql = "select schedstat from T_SCHED where bookno='" & Trim(cmbBookNo) & "' and schedstat='P'"
    If OpenRecordset(g_rs_TSCHED, lsql, "frmAddSched", "Host") Then
        If g_rs_TSCHED.Fields(0) = "P" Then
            MsgBox "Please choose another book, this book has been uploaded.", vbExclamation, "Edit Book"
            Exit Sub
        End If
    Else
        If DuplicateRecords Then Exit Sub
        
        If g_str_AddEdit = "ADD" Then
            If Not Save_Add Then Exit Sub
                    
            Set l_obj_Item = frmBookSched.lsvFileMain.ListItems.Add(, , Trim(cmbBookNo))
                'l_obj_Item.SubItems(1) = Trim(txtBookFrom)
                'l_obj_Item.SubItems(2) = Trim(txtBookTo)
            Set l_obj_Item = Nothing
    '        frmEditBooks.cmdEdit.Enabled = True
    '        frmEditBooks.cmdDelete.Enabled = True
        Else
            If Not Save_Edit Then Exit Sub
            
            l_int_Index = frmBookSched.lsvFileMain.SelectedItem.Index
            frmBookSched.lsvFileMain.ListItems(l_int_Index).Text = Trim(cmbBookNo)
            'frmBookSched.lsvFileMain.ListItems(l_int_Index).SubItems(1) = Trim(txtBookFrom)
            'frmBookSched.lsvFileMain.ListItems(l_int_Index).SubItems(2) = Trim(txtBookTo)
        End If
    
        If frmBookSched.lsvFileMain.ListItems.Count = 5 Then
            frmBookSched.cmdAdd.Enabled = False
        End If
        frmBookSched.lsvFileMain.Refresh
        Unload Me
    End If
End Sub

Function DuplicateRecords() As Boolean
    Dim l_str_Sql As String
    
    DuplicateRecords = True
    
    If g_str_AddEdit = "EDIT" Then
        If g_str_Table = "T_SCHED" Then
            If Trim(cmbBookNo) = frmBookSched.lsvFileMain.SelectedItem.Text Then
                Unload Me
                Exit Function
            End If
        Else
            If Trim(cmbBookNo) = frmBookSched.lsvFileMain.SelectedItem.Text Then
                Unload Me
                Exit Function
            End If
        End If
         
        If Trim(cmbBookNo) = frmBookSched.lsvFileMain.SelectedItem.Text Then
            DuplicateRecords = False
            Exit Function
        End If
    End If
    
    Select Case g_str_Table
        Case "T_SCHED"
            l_str_Sql = "SELECT BOOKNO FROM T_SCHED WHERE BOOKNO = '" & Trim(cmbBookNo) & "'" 'and HHTNO=(select HHTNO from F_HH where hhid='" & frmAddSched.cmbRoverNo & "')"    ' and DAYNO=" & frmAddSched.flxSched.Col + 1 & "
    End Select

    If OpenRecordset(g_rs_RHH, l_str_Sql, Me.Name, "DuplicateRecords") Then
        MsgBox "Record already exists!", vbExclamation, "Duplicate Record"
        g_rs_RHH.Close
        Set g_rs_RHH = Nothing
        cmbBookNo.SetFocus
        Exit Function
    End If

    DuplicateRecords = False
    
End Function

Function Save_Add() As Boolean
    Dim l_str_Sql As String
    Dim HHNo As String
    Dim ReaderNo As String
    Dim SchedStat As String
    Dim lsql As String
    Dim i As Integer
    
    lsql = "select HHTNO from F_HH where hhid='" & frmAddSched.cmbRoverNo & "'"
    If OpenRecordset(g_rs_RHH, lsql, "frmAddBookFile", "MRU Management") Then
        HHNo = g_rs_RHH.Fields(0)
    Else
        HHNo = ""
    End If
    
    lsql = "select readerno from R_READER where rdrname='" & frmAddSched.cmbReader & "'"
    If OpenRecordset(g_rs_RREADER, lsql, "frmAddBookFile", "MRU Management") Then
        ReaderNo = g_rs_RREADER.Fields(0)
    Else
        ReaderNo = ""
    End If
    
    If frmBookSched.lsvFileMain.ListItems.Count > 0 Then
        For i = 1 To frmBookSched.lsvFileMain.ListItems.Count
            l_str_Sql = "UPDATE T_BOOK SET BOOKSTAT = '', DLROVERDT= '0:00' WHERE BOOKNO = '" & frmBookSched.lsvFileMain.ListItems(i) & "'"
            If Not DBExecute(l_str_Sql, Me.Name, "MRU Management") Then Exit Function
            l_str_Sql = "UPDATE T_BKINFO SET READERNAME = '', RDGDATE= '' WHERE BOOKNO = '" & frmBookSched.lsvFileMain.ListItems(i) & "'"
            If Not DBExecute(l_str_Sql, Me.Name, "MRU Management") Then Exit Function
            l_str_Sql = "UPDATE T_SCHED SET SCHEDSTAT='D', ALT_HHTNO='' WHERE BOOKNO = '" & frmBookSched.lsvFileMain.ListItems(i) & "' and HHTNO=(select HHTNO from F_HH where hhid='" & frmAddSched.cmbRoverNo & "')"
            If Not DBExecute(l_str_Sql, Me.Name, "cmdDelete_Click") Then Exit Function
        Next i
    End If
    
    SchedStat = "D"

    l_str_Sql = "INSERT INTO T_SCHED(BOOKNO, HHTNO,DAYNO,SCHEDSTAT,READERNO)VALUES( '" & _
        Trim(cmbBookNo) & "','" & HHNo & "', " & frmAddSched.flxSched.Col + 1 & ",'" & SchedStat & "','" & ReaderNo & "')"
    
    Save_Add = DBExecute(l_str_Sql, Me.Name, "Save_Add")

End Function

Function Save_Edit() As Boolean
    Dim l_str_Sql As String
    Dim lsql As String
    Dim HHNo As String
    Dim i As Integer
    
    lsql = "select HHTNO from F_HH where hhid='" & frmAddSched.cmbRoverNo & "'"
    If OpenRecordset(g_rs_RHH, lsql, "frmAddBookFile", "MRU Management") Then
        HHNo = g_rs_RHH.Fields(0)
    Else
        HHNo = ""
    End If
        
    If frmBookSched.lsvFileMain.ListItems.Count > 1 Then
        For i = 1 To frmBookSched.lsvFileMain.ListItems.Count
            l_str_Sql = "UPDATE T_BOOK SET BOOKSTAT = '', DLROVERDT= '0:00' WHERE BOOKNO = '" & frmBookSched.lsvFileMain.ListItems(i) & "'"
            If Not DBExecute(l_str_Sql, Me.Name, "MRU Management") Then Exit Function
            l_str_Sql = "UPDATE T_BKINFO SET READERNAME = '', RDGDATE= '' WHERE BOOKNO = '" & frmBookSched.lsvFileMain.ListItems(i) & "'"
            If Not DBExecute(l_str_Sql, Me.Name, "MRU Management") Then Exit Function
            l_str_Sql = "UPDATE T_SCHED SET SCHEDSTAT='D', ALT_HHTNO='' WHERE BOOKNO = '" & frmBookSched.lsvFileMain.ListItems(i) & "' and HHTNO=(select HHTNO from F_HH where hhid='" & frmAddSched.cmbRoverNo & "')"
            If Not DBExecute(l_str_Sql, Me.Name, "cmdDelete_Click") Then Exit Function
        Next i
    End If

    l_str_Sql = "UPDATE T_BOOK SET BOOKSTAT = '', DLROVERDT= '0:00' WHERE BOOKNO = '" & frmBookSched.lsvFileMain.SelectedItem.Text & "'"
    DBExecute l_str_Sql, Me.Name, "MRU Management"
    l_str_Sql = "UPDATE T_BKINFO SET READERNAME = '', RDGDATE= '' WHERE BOOKNO = '" & frmBookSched.lsvFileMain.SelectedItem.Text & "'"
    DBExecute l_str_Sql, Me.Name, "MRU Management"
    l_str_Sql = "UPDATE T_SCHED SET BOOKNO = '" & Trim(cmbBookNo) & _
        "', ALT_HHTNO='', SCHEDSTAT='D' WHERE HHTNO = '" & HHNo & "' and BOOKNO = '" & frmBookSched.lsvFileMain.SelectedItem.Text & "' and DAYNO = " & frmAddSched.flxSched.Col + 1 & ""
                    
    Save_Edit = DBExecute(l_str_Sql, Me.Name, "Save_Edit")

End Function


Private Sub Form_Activate()

If WindowState = vbNormal Then
    Me.Move (1000)
    Me.Top = 500
End If

    frmAddBookFile.Caption = "Day" & " " & frmAddSched.flxSched.Col + 1
    cmbBookNo.SetFocus
    If g_str_AddEdit = "ADD" Then
        load_BookNo cmbBookNo, ""
        'txtBookFrom = ""
        'txtBookTo = ""
    Else
        load_BookNo cmbBookNo, ""  'frmBookSched.lsvFileMain.SelectedItem.Text
        cmbBookNo.Text = frmBookSched.lsvFileMain.SelectedItem
        'txtBookFrom.Text = frmBookSched.lsvFileMain.SelectedItem.SubItems(1)
        'txtBookTo.Text = frmBookSched.lsvFileMain.SelectedItem.SubItems(2)
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmBookSched.Enabled = True
End Sub
