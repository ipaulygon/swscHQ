Attribute VB_Name = "modDataIntoGrid"
Public Sub LoadTSchedGrid(rs As Recordset, grd As MSFlexGrid, frm As String)
    Dim Count As Long
    Dim stat As Integer

    grd.Redraw = False
   
    ' Retrieve data in T_SCHED, determine which BC and Day no the MRU count should go
    If rs.CursorLocation = adUseClient Then
        For Count = 1 To rs.RecordCount
            With grd
                .Col = 0
                For i = 0 To .Rows
                    If rs.Fields("BC_CODE") = Left(.Text, 4) Then
                        Exit For
                    End If
                    .Row = .Row + 1
                Next i

                '.Row = Val(rs.Fields("BC_CODE")) / 100 ' this is the row
                .Col = rs.Fields("DAYNO") ' this is the column
                If frm = "Discon" Then
                    .TextMatrix(.Row, .Col) = "" & str$(rs.Fields("NUM_MRU")) & " (" & str$(rs.Fields("TOTREC")) & ")" ' this is the DCJOBID and record count
                Else
                    .TextMatrix(.Row, .Col) = "" & str$(rs.Fields("NUM_MRU")) ' this is the MRU count
                End If
            End With
            rs.MoveNext
        Next
    ElseIf rs.CursorLocation = adUseServer Then
        Do While Not rs.EOF
            With grd
                
                .Col = 0
                For i = 0 To .Rows
                    If rs.Fields("DMZCODE_ID") = Left(.Text, 3) Then
                        Exit For
                    End If
                    .Row = .Row + 1
                Next i

                '.Row = Val(rs.Fields("BC_CODE")) / 100 ' this is the row
                .Col = rs.Fields("DAYNO") ' this is the column
                
                Select Case (rs.Fields("MAXMRUSTAT"))
                    Case 40
                        .CellForeColor = &HC00000
                    Case 30
                        .CellForeColor = &HFF&
                    Case 20
                        .CellForeColor = &HC000&
                    'Received from Satellite
                    Case 15
                        .CellForeColor = &HC000&
                    Case 10
                        .CellFontBold = True
                End Select
                If frm = "Discon" Then
                    .TextMatrix(.Row, .Col) = "" & str$(rs.Fields("NUM_MRU")) & " (" & str$(rs.Fields("TOTREC")) & ")" ' this is the DCJOBID and record count
                Else
                    .TextMatrix(.Row, .Col) = "" & str$(rs.Fields("NUM_MRU")) ' this is the MRU count
                End If
            End With
            rs.MoveNext
        Loop
    End If
  
    'redraw the entire grid
    grd.Redraw = True
End Sub

Public Sub LoadMRUSchedGrid(rs As Recordset, grd As MSFlexGrid)
                
    Dim X As Long
    Dim Count As Long
    Dim fmtString As String

    grd.Redraw = False
    grd.Clear
    
    'Set Format String for flexgrid; justify headers
    fmtString = "<DMZ|<Day 1|<Day 2|<Day 3|<Day 4|<Day 5|<Day 6|<Day 7|<Day 8|<Day 9|<Day 10|<Day 11|<Day 12|<Day 13|<Day 14|<Day 15|<Day 16|<Day 17|<Day 18|<Day 19|<Day 20|<Day 21|<Day 22|<Day 23|<Day 24|<Day 25|<Day 26|<Day 27|<Day 28|<Day 29|<Day 30|<Day 31"
    grd.FormatString = fmtString
    
    'setup the minimum number of rows
    grd.Rows = 2
    ' Headers
    grd.FixedRows = 1
    ' Business Centers
    grd.FixedCols = 1
    
    grd.Row = 0
    
    ' Add 31 days of schedule
    'grd.Cols = rs.Fields.Count + 1
    grd.Cols = 32
    For X = 0 To rs.Fields.Count - 1
        grd.Col = X + 1
        grd.ColData(X + 1) = rs.Fields(X).Type
    Next

    ' Retrieve all listed business centers in R_BUSCTR
    If rs.CursorLocation = adUseClient Then
        grd.Rows = rs.RecordCount + 1
        For Count = 1 To rs.RecordCount
            
            For X = 0 To rs.Fields.Count - 1
                'we use Variant conversion to avoid any possible NULL errors
                'grd.TextMatrix(Count, X + 1) = "" & CVar(rs.Fields(X).Value)
                grd.TextMatrix(Count, 0) = "" & CVar(rs.Fields(X).Value)
            Next
            rs.MoveNext
        Next
    ElseIf rs.CursorLocation = adUseServer Then
        Do While Not rs.EOF
            Count = Count + 1
            
            If Count >= grd.Rows Then
                'increase the amount of rows by 100 at a time - makes everything faster
                grd.Rows = grd.Rows + 100
            End If
            
            For X = 0 To rs.Fields.Count - 1
                'we use Variant conversion to avoid any possible NULL errors
                'grd.TextMatrix(Count, X + 1) = "" & CVar(rs.Fields(X).Value)
                grd.TextMatrix(Count, 0) = "" & CVar(rs.Fields(X).Value)
            Next
    
            rs.MoveNext
        Loop
        grd.Rows = Count + 1    'reset the number of rows to the number of records
    End If
    
    'autosize the grid
    SetGridColumnWidth grd
  
    'redraw the entire grid
    grd.Redraw = True
End Sub

Public Sub SelectGridCell(grd As MSFlexGrid, GridRow As Integer)
Dim i As Integer
Dim j As Integer
Dim NumRows As Integer
Dim NumCols As Integer

NumRows = grd.Rows - 1 '.rows returns num of rows
NumCols = grd.Cols - 1 '.cols reutrns num of columns
'since this Sub takes
'care of highlighting we tell it to never highlight so only 1 row
'is selected at a time
grd.HighLight = flexHighlightNever
grd.Redraw = False

For i = 1 To NumRows
    If i <> GridRow Then
        grd.Row = i
    
        For j = 1 To NumCols
            grd.Col = j
            If grd.CellBackColor = vbHighlight Then
                grd.CellBackColor = vbWindowBackground
                grd.CellForeColor = vbWindowText
            Else
                Exit For
            End If
        Next j
    End If
Next i

grd.Row = GridRow 'set the row To the clicked row

For i = 1 To NumCols 'setting the clicked row to highlighted
    grd.Col = i
    grd.CellBackColor = vbHighlight
    grd.CellForeColor = vbHighlightText
Next i

grd.Col = 1
grd.Redraw = True

End Sub

Private Sub HighlightGridRow(grd As MSFlexGrid, iRow As Long)
    With grd
        If .Rows > 1 Then
            .Row = iRow
            .Col = 0
            .ColSel = .Cols - 1
            .rowSel = iRow
        End If
    End With
End Sub
Public Sub LoadRecordsetIntoGrid(rs As Recordset, grd As MSFlexGrid, _
                Optional AutosizeColumns As Boolean = True, _
                Optional HighlightFirstRow As Boolean = True)
                
    Dim X As Long
    Dim Count As Long

    grd.Redraw = False
    grd.Clear
    
    'setup the minimum number of rows & add column headers
    grd.Rows = 2
    grd.FixedRows = 1
    grd.Row = 0
    grd.Cols = rs.Fields.Count + 1
    For X = 0 To rs.Fields.Count - 1
        grd.Col = X + 1
        'grd.Text = rs.Fields(X).Name
        grd.ColData(X + 1) = rs.Fields(X).Type
    Next

    If rs.CursorLocation = adUseClient Then
        grd.Rows = rs.RecordCount + 1
        For Count = 1 To rs.RecordCount
            
            'grd.TextMatrix(Count, 0) = Count    'assign line number
            For X = 0 To rs.Fields.Count - 1
                'we use Variant conversion to avoid any possible NULL errors
                grd.TextMatrix(Count, X + 1) = "" & CVar(rs.Fields(X).Value)
            Next
            rs.MoveNext
        Next
    ElseIf rs.CursorLocation = adUseServer Then
        Do While Not rs.EOF
            Count = Count + 1
            
            If Count >= grd.Rows Then
                'increase the amount of rows by 100 at a time - makes everything faster
                grd.Rows = grd.Rows + 100
            End If
            
            'grd.TextMatrix(Count, 0) = Count    'assign line number
            For X = 0 To rs.Fields.Count - 1
                'we use Variant conversion to avoid any possible NULL errors
                grd.TextMatrix(Count, X + 1) = "" & CVar(rs.Fields(X).Value)
            Next
    
            rs.MoveNext
        Loop
        grd.Rows = Count + 1    'reset the number of rows to the number of records
    End If
    
    'autosize the grid
    If AutosizeColumns Then SetGridColumnWidth grd

    'highlight the first row
    If HighlightFirstRow Then
        If grd.Rows > 1 Then HighlightGridRow grd, 1
    End If
    
    'redraw the entire grid
    grd.Redraw = True
End Sub
Public Sub LoadRecordsetIntoGridNoCr(rs As Recordset, grd As MSFlexGrid, _
                Optional AutosizeColumns As Boolean = True, _
                Optional HighlightFirstRow As Boolean = True)
                
    'This sub assumes that the records doesn't have any
    'data in it with Carriage Returns
    'This knowledge allows the routine to dump the
    'entire recordset wholesale into the MsFlexGrid via the .Clip property
    
    Dim X As Long
    Dim Count As Long

    'this prevents the grid from repainting everytime something is added
    grd.Redraw = False
    grd.Clear

    'setup the minimum number of rows & add column headers
    grd.Rows = 2
    grd.FixedRows = 1
    grd.Row = 0
    grd.Cols = rs.Fields.Count + 1
    For X = 0 To rs.Fields.Count - 1
        grd.Col = X + 1
        grd.Text = rs.Fields(X).Name
        grd.ColData(X + 1) = rs.Fields(X).Type
    Next

    If rs.CursorLocation = adUseClient Then
        If rs.RecordCount > 0 Then
            'setup all the rows necessary for this recordset
            grd.Rows = rs.RecordCount + 1
            grd.Row = 1
            grd.Col = 1
                        
            grd.ColSel = rs.Fields.Count
            grd.rowSel = rs.RecordCount
            
            'fill in the recordset
            grd.Clip = rs.GetString(, , Chr$(9), Chr$(13))
            
            'remove the highlight
            grd.rowSel = 0
            grd.ColSel = 0
            grd.Row = 1
            grd.Col = 1
            
            'assign record numbers
            For X = 1 To grd.Rows - 1
                grd.TextMatrix(X, 0) = X
            Next
            
        End If
    ElseIf rs.CursorLocation = adUseServer Then
        Do While Not rs.EOF
            Count = Count + 1
            
            If Count >= grd.Rows Then
                'increase the amount of rows by 100 at a time - makes everything faster
                grd.Rows = grd.Rows + 100
            End If
            
            grd.TextMatrix(Count, 0) = Count    'assign line number
            For X = 0 To rs.Fields.Count - 1
                'we use Variant conversion to avoid any possible NULL errors
                grd.TextMatrix(Count, X + 1) = "" & CVar(rs.Fields(X).Value)
            Next
    
            rs.MoveNext
        Loop
        grd.Rows = Count + 1    'reset the number of rows to the number of records
    End If
    
    'autosize the grid
    If AutosizeColumns Then SetGridColumnWidth grd

    'highlight the first row
    If HighlightFirstRow Then
        If grd.Rows > 1 Then HighlightGridRow grd, 1
    End If
    
    'redraw the entire grid
    grd.Redraw = True
End Sub
Private Sub SetGridColumnWidth(grd As MSFlexGrid)
    'params:    ms flexgrid control
    'purpose:   sets the column widths to the lengths of the longest string in the column
    'requirements:  the grid must have the same font as the underlying form

    Dim InnerLoopCount As Long
    Dim OuterLoopCount As Long
    Dim lngLongestLen As Long
    Dim sLongestString As String
    Dim lngColWidth As Long
    Dim szCellText As String

    For OuterLoopCount = 0 To grd.Cols - 1
        sLongestString = ""
        lngLongestLen = 0

        'grd.Col = OuterLoopCount
        For InnerLoopCount = 0 To grd.Rows - 1
            szCellText = grd.TextMatrix(InnerLoopCount, OuterLoopCount)
            'grd.Row = InnerLoopCount
            'szCellText = Trim$(grd.Text)
            If Len(szCellText) > lngLongestLen Then
                lngLongestLen = Len(szCellText)
                sLongestString = szCellText
            End If
        Next
        lngColWidth = grd.Parent.TextWidth(sLongestString)
        grd.ColWidth(OuterLoopCount) = lngColWidth + 200    'add 100 for more readable spreadsheet
    Next
End Sub





