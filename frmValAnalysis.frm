VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmValAnalysis 
   Caption         =   "Validation Analysis - Observation Codes"
   ClientHeight    =   9795
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   15210
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   9795
   ScaleWidth      =   15210
   Begin VB.Frame Frame2 
      Height          =   5175
      Left            =   0
      TabIndex        =   15
      Top             =   4780
      Width           =   15615
      Begin VB.CommandButton Command1 
         Caption         =   "A&dd Action"
         Height          =   375
         Left            =   4080
         TabIndex        =   36
         Top             =   4500
         Width           =   1335
      End
      Begin VB.ComboBox cmbStatus 
         Height          =   315
         ItemData        =   "frmValAnalysis.frx":0000
         Left            =   11640
         List            =   "frmValAnalysis.frx":0002
         TabIndex        =   35
         Text            =   "PENDING"
         Top             =   900
         Width           =   1700
      End
      Begin VB.ComboBox cmbMOC 
         Height          =   315
         Left            =   7440
         Sorted          =   -1  'True
         TabIndex        =   34
         Text            =   "ALL"
         Top             =   900
         Width           =   1500
      End
      Begin VB.ComboBox cmbMMRU 
         Height          =   315
         Left            =   2840
         TabIndex        =   33
         Text            =   "ALL"
         Top             =   900
         Width           =   1545
      End
      Begin VB.ComboBox cmbMBC 
         Height          =   315
         ItemData        =   "frmValAnalysis.frx":0004
         Left            =   385
         List            =   "frmValAnalysis.frx":000B
         Sorted          =   -1  'True
         TabIndex        =   32
         Text            =   "ALL"
         Top             =   900
         Width           =   1980
      End
      Begin VB.CommandButton cmdRemove 
         Caption         =   "&Remove  from Masterlist"
         Height          =   375
         Left            =   12000
         TabIndex        =   27
         Top             =   4500
         Width           =   2295
      End
      Begin VB.CommandButton cmdFOpen 
         Caption         =   "Force &Open"
         Height          =   375
         Left            =   10800
         TabIndex        =   26
         Top             =   4500
         Width           =   1095
      End
      Begin VB.CommandButton cmdFClose 
         Caption         =   "Force &Close"
         Height          =   375
         Left            =   9600
         TabIndex        =   25
         Top             =   4500
         Width           =   1095
      End
      Begin VB.CommandButton cmdGenReport 
         Caption         =   "&Generate Report"
         Height          =   375
         Left            =   2400
         TabIndex        =   24
         Top             =   4500
         Width           =   1335
      End
      Begin VB.CommandButton cmdmasNone 
         Caption         =   "Select &None"
         Height          =   375
         Left            =   1200
         TabIndex        =   23
         Top             =   4500
         Width           =   1095
      End
      Begin VB.CommandButton cmdMasAll 
         Caption         =   "Select &All"
         Height          =   375
         Left            =   120
         TabIndex        =   22
         Top             =   4500
         Width           =   975
      End
      Begin MSComCtl2.DTPicker DTPicker3 
         Height          =   375
         Left            =   4800
         TabIndex        =   17
         Top             =   480
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   661
         _Version        =   393216
         Format          =   16449537
         CurrentDate     =   40037
      End
      Begin MSComCtl2.DTPicker DTPicker4 
         Height          =   375
         Left            =   7800
         TabIndex        =   18
         Top             =   480
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   661
         _Version        =   393216
         Format          =   16449537
         CurrentDate     =   40037
      End
      Begin MSComctlLib.ListView lsvMaster 
         Height          =   3255
         Left            =   120
         TabIndex        =   21
         Top             =   1200
         Width           =   15000
         _ExtentX        =   26458
         _ExtentY        =   5741
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "BusinessCenter"
            Object.Width           =   3881
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "MRU"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Installation No"
            Object.Width           =   2716
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Initial Rdg"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Initial OC"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Initial Remarks"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Status"
            Object.Width           =   2999
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Action"
            Object.Width           =   6174
         EndProperty
      End
      Begin VB.Label Label16 
         Caption         =   "MRU:"
         Height          =   255
         Left            =   2400
         TabIndex        =   44
         Top             =   960
         Width           =   495
      End
      Begin VB.Label Label15 
         Caption         =   "BC:"
         Height          =   255
         Left            =   120
         TabIndex        =   43
         Top             =   960
         Width           =   255
      End
      Begin VB.Label Label14 
         Caption         =   "Initial OC:"
         Height          =   255
         Left            =   6720
         TabIndex        =   42
         Top             =   960
         Width           =   855
      End
      Begin VB.Label Label13 
         Caption         =   "Status:"
         Height          =   255
         Left            =   11040
         TabIndex        =   41
         Top             =   960
         Width           =   615
      End
      Begin VB.Label lblCountMaster 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   7560
         TabIndex        =   31
         Top             =   4570
         Width           =   375
      End
      Begin VB.Label Label9 
         Caption         =   "Records"
         Height          =   255
         Left            =   8280
         TabIndex        =   30
         Top             =   4570
         Width           =   735
      End
      Begin VB.Label Label7 
         Caption         =   "Extract Date from:"
         Height          =   255
         Left            =   3240
         TabIndex        =   20
         Top             =   480
         Width           =   1455
      End
      Begin VB.Label Label6 
         Caption         =   "Extract Date to:"
         Height          =   255
         Left            =   6480
         TabIndex        =   19
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label5 
         Alignment       =   2  'Center
         BackColor       =   &H80000003&
         Caption         =   "MASTERLIST FOR VALIDATION"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   0
         TabIndex        =   16
         Top             =   120
         Width           =   15615
      End
   End
   Begin VB.Frame Frame1 
      Height          =   5055
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   15615
      Begin VB.CommandButton cmdRemovefrExtractd 
         Caption         =   "Remove from E&xtracted"
         Height          =   375
         Left            =   9360
         TabIndex        =   37
         Top             =   4410
         Width           =   2295
      End
      Begin VB.ComboBox cmbOC 
         Height          =   315
         Left            =   8300
         TabIndex        =   13
         Text            =   "ALL"
         Top             =   840
         Width           =   2040
      End
      Begin VB.ComboBox cmbMRU 
         Height          =   315
         Left            =   2880
         TabIndex        =   12
         Text            =   "ALL"
         Top             =   840
         Width           =   1560
      End
      Begin VB.ComboBox cmbBusinessCenter 
         Height          =   315
         ItemData        =   "frmValAnalysis.frx":0014
         Left            =   480
         List            =   "frmValAnalysis.frx":001B
         Sorted          =   -1  'True
         TabIndex        =   11
         Text            =   "ALL"
         Top             =   840
         Width           =   1920
      End
      Begin VB.CommandButton cmdAdd 
         Caption         =   "&Add to Masterlist"
         Height          =   375
         Left            =   12120
         TabIndex        =   9
         Top             =   4410
         Width           =   2295
      End
      Begin VB.CommandButton cmdExNone 
         Caption         =   "Select &None"
         Height          =   375
         Left            =   1440
         TabIndex        =   8
         Top             =   4410
         Width           =   1215
      End
      Begin VB.CommandButton cmdExAll 
         Caption         =   "Select &All"
         Height          =   375
         Left            =   120
         TabIndex        =   7
         Top             =   4410
         Width           =   1215
      End
      Begin VB.ComboBox cmbCycle 
         Height          =   315
         ItemData        =   "frmValAnalysis.frx":0024
         Left            =   720
         List            =   "frmValAnalysis.frx":004C
         TabIndex        =   2
         Top             =   480
         Width           =   1215
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   375
         Left            =   7200
         TabIndex        =   3
         Top             =   480
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   661
         _Version        =   393216
         Format          =   16449537
         CurrentDate     =   40037
      End
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   375
         Left            =   10680
         TabIndex        =   4
         Top             =   480
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   661
         _Version        =   393216
         Format          =   16449537
         CurrentDate     =   40037
      End
      Begin MSComctlLib.ListView lsvExtract 
         Height          =   3255
         Left            =   120
         TabIndex        =   14
         Top             =   1155
         Width           =   14985
         _ExtentX        =   26432
         _ExtentY        =   5741
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "BusinessCenter"
            Object.Width           =   3881
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "MRU"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Installation No"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Reading"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "OC"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Remarks"
            Object.Width           =   7620
         EndProperty
      End
      Begin VB.Label Label12 
         Caption         =   "OC:"
         Height          =   255
         Left            =   7920
         TabIndex        =   40
         Top             =   885
         Width           =   375
      End
      Begin VB.Label Label11 
         Caption         =   "MRU:"
         Height          =   255
         Left            =   2400
         TabIndex        =   39
         Top             =   880
         Width           =   495
      End
      Begin VB.Label Label10 
         Caption         =   "BC:"
         Height          =   255
         Left            =   120
         TabIndex        =   38
         Top             =   880
         Width           =   375
      End
      Begin VB.Label lblCountExtract 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   7200
         TabIndex        =   29
         Top             =   4470
         Width           =   375
      End
      Begin VB.Label Label8 
         Caption         =   "Records"
         Height          =   255
         Left            =   7800
         TabIndex        =   28
         Top             =   4470
         Width           =   735
      End
      Begin VB.Label Label4 
         Caption         =   "Cycle"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   480
         Width           =   615
      End
      Begin VB.Label Label3 
         Caption         =   "Extract Date to:"
         Height          =   255
         Left            =   9240
         TabIndex        =   6
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Extract Date from:"
         Height          =   255
         Left            =   5640
         TabIndex        =   5
         Top             =   480
         Width           =   1455
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H80000003&
         Caption         =   "EXTRACTED ACCOUNTS"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   0
         TabIndex        =   1
         Top             =   120
         Width           =   15495
      End
   End
End
Attribute VB_Name = "frmValAnalysis"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim valCycle As Integer
Dim valDate1 As Date
Dim valDate2 As Date
Dim valDate3 As Date
Dim valDate4 As Date
Dim valBC As String
Dim valOC As String
Dim valMRU As String
Dim valMBCOde As String
Dim valMBC As String
Dim valMOC As String
Dim valMMRU As String
Dim valStatus As String
Dim PDate As Date


Private Sub cmbBusinessCenter_Click()
valBC = cmbBusinessCenter.Text
ClearComboExtract
genlsvExtract
cmbBusinessCenter.Text = valMRU
End Sub



Private Sub cmbCycle_Click()
valCycle = CInt(cmbCycle.Text)
DTPicker1.Month = valCycle
DTPicker2.Month = valCycle
DTPicker3.Month = valCycle
DTPicker4.Month = valCycle
valDate1 = DTPicker1.Value
valDate2 = DTPicker2.Value
valDate3 = DTPicker3.Value
valDate4 = DTPicker4.Value
ClearComboExtract
genlsvExtract
genMasterList

End Sub



Private Sub cmbMBC_Click()
Dim rsbccode As ADODB.Recordset
valMBC = cmbMBC.Text
ClearComboMaster
genMasterList
cmbMBC.Text = valMBC
If cmbMBC.Text <> "ALL" Then
  
  If OpenRecordset(rsbccode, "SELECT BC_CODE FROM R_BUSCTR WHERE BC_DESC = '" & cmbMBC.Text & "'", "", "") Then
   valMBCOde = rsbccode.Fields(0)
  End If
  rsbccode.Close
  Set rsbccode = Nothing
  
ElseIf cmbMBC.Text = "ALL" Then
  valMBCOde = "ALL"
End If



End Sub

Private Sub cmbMMRU_Click()
valMMRU = cmbMMRU.Text
ClearComboMaster
genMasterList
cmbMMRU.Text = valMMRU
End Sub



Private Sub cmbMOC_Click()
valMOC = cmbMOC.Text
ClearComboMaster
genMasterList
cmbMOC.Text = valMOC
End Sub

Private Sub cmbMRU_Click()
valMRU = cmbMRU.Text
ClearComboExtract
genlsvExtract
cmbMRU.Text = valMRU
End Sub


Public Sub ClearComboExtract()
cmbBusinessCenter.Clear
cmbBusinessCenter.AddItem "ALL"
cmbMRU.Clear
cmbMRU.AddItem "ALL"
cmbOC.Clear
cmbOC.AddItem "ALL"
End Sub

Public Sub ClearComboMaster()
cmbMBC.Clear
cmbMBC.AddItem "ALL"
cmbMMRU.Clear
cmbMMRU.AddItem "ALL"
cmbMOC.Clear
cmbMOC.AddItem "ALL"
cmbStatus.Clear
cmbStatus.AddItem "ALL"
End Sub

Private Sub cmbOC_Click()
valOC = cmbOC.Text
ClearComboExtract
genlsvExtract
cmbOC.Text = valOC
End Sub



Private Sub cmbStatus_Click()
valStatus = cmbStatus.Text
ClearComboMaster
genMasterList
cmbStatus.Text = valStatus
End Sub

Private Sub cmdAdd_Click()
Dim X As Integer
Dim MRU As String
Dim ACCTNUM As String
Dim SQLUpdate As String
Dim SQLUpdate1 As String
Dim Count As String

Count = 0
For X = 1 To lsvExtract.ListItems.Count
   If lsvExtract.ListItems(X).checked = True Then
     'for checked items
      MRU = lsvExtract.ListItems.Item(X).ListSubItems(1)
      ACCTNUM = lsvExtract.ListItems.Item(X).ListSubItems(2)
      SQLUpdate = "UPDATE dbo.T_VER_EXTRACT SET MASTERLIST = 1 WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle
      DBExecute SQLUpdate, "", ""
      If OpenRecordset(g_rs_TUPLOADHIS, "SELECT * FROM dbo.T_UPLOAD_HIS WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle, Me.Name, "") Then
        SQLUpdate1 = "UPDATE dbo.T_UPLOAD_HIS SET INMASTERLIST = 1 WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle
        DBExecute SQLUpdate1, "", ""
      End If
      Count = Count + 1
   End If
   
Next X
MsgBox Count & " Records added to Masterlist", vbInformation

genlsvExtract
genMasterList


End Sub

Private Sub cmdExAll_Click()
SelectAll 1, 0
End Sub

Private Sub cmdExNone_Click()
SelectAll 0, 0
End Sub

Private Sub cmdFClose_Click()
Dim X As Integer
Dim txtStatus As String
Dim checked As Integer
Dim MRU As String
Dim ACCTNUM As String
Dim SQLUpdate As String
Dim Count As Integer
Dim ans As Integer
checked = 0
Count = 0
For X = 1 To lsvMaster.ListItems.Count
  If lsvMaster.ListItems(X).checked = True Then
    If lsvMaster.ListItems.Item(X).ListSubItems(6) <> "PENDING" Then
     checked = 1
    End If
    If lsvMaster.ListItems.Item(X).ListSubItems(6) = "PENDING" Then
     Count = Count + 1
    End If
  End If
Next X

If checked = 1 Then
MsgBox "Only Accounts with Pending Status can be force closed"
ElseIf checked = 0 And Count > 0 Then
ans = MsgBox("Are you sure you want to Force Close this account?", vbYesNo, "Force Close")

If ans = 6 Then
'UPDATE STATUSES TO CLOSED
   For X = 1 To lsvMaster.ListItems.Count
     If lsvMaster.ListItems(X).checked = True Then
       MRU = lsvMaster.ListItems.Item(X).ListSubItems(1)
       ACCTNUM = lsvMaster.ListItems.Item(X).ListSubItems(2)
       SQLUpdate = "UPDATE dbo.T_VER_EXTRACT SET CONDITION = 3 WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle
       DBExecute SQLUpdate, "", ""
     End If
   Next X
   If Count <> 0 Then
   MsgBox Count & " Records Updated!", vbInformation
   genMasterList
   checked = 0
   Count = 0
   End If
 End If
End If

End Sub

Private Sub cmdFOpen_Click()

Dim X As Integer
Dim txtStatus As String
Dim checked As Integer
Dim MRU As String
Dim ACCTNUM As String
Dim SQLUpdate As String
Dim SQLUpdate2 As String
Dim SQLDelete As String
Dim SQLDelete2 As String
Dim Count As Integer
Dim rngecode As String
Dim rngecodehis As String
Dim RDTAG As String
Dim rdtaghis As String
Dim oorcount As String
Dim unrdcnt As String
Dim rdcnt As String

checked = 0
Count = 0
For X = 1 To lsvMaster.ListItems.Count
  If lsvMaster.ListItems(X).checked = True Then
    If lsvMaster.ListItems.Item(X).ListSubItems(6) = "CORRECTED" Or lsvMaster.ListItems.Item(X).ListSubItems(6) = "PENDING" Then
        checked = 1
        Exit For
    End If
    If lsvMaster.ListItems.Item(X).ListSubItems(6) = "CLOSED" Or lsvMaster.ListItems.Item(X).ListSubItems(6) = "FORCED CLOSED" Then
        Count = Count + 1
    End If
  End If
Next X

If checked = 1 Then
   MsgBox "Only Accounts with Closed and Forced Closed Status can be Force Opened!"
ElseIf checked = 0 And Count > 0 Then
    'UPDATE STATUSES TO PENDING
    For X = 1 To lsvMaster.ListItems.Count
        If lsvMaster.ListItems(X).checked = True Then
            MRU = lsvMaster.ListItems.Item(X).ListSubItems(1)
            ACCTNUM = lsvMaster.ListItems.Item(X).ListSubItems(2)
            'UPDATE STATUS IN MASTERLIST
            SQLUpdate = "UPDATE dbo.T_VER_EXTRACT SET CONDITION = 0 WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle
            DBExecute SQLUpdate, "", ""
            
            '---------------------------------
            'UPDATE T_BKINFO_HIS counts
            '---------------------------------
            If OpenRecordset(g_rs_TUPLOAD, "SELECT RANGECODE, READTAG FROM dbo.T_UPLOAD WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle, Me.Name, "") Then
                rngecode = g_rs_TUPLOAD.Fields(0)
                RDTAG = g_rs_TUPLOAD.Fields(1)
                If OpenRecordset(g_rs_TUPLOADHIS, "SELECT RANGECODE, READTAG FROM dbo.T_UPLOAD_HIS WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle & " AND ORIGDATA = 0", Me.Name, "") Then
                    rngecodehis = g_rs_TUPLOADHIS.Fields(0)
                    rdtaghis = g_rs_TUPLOADHIS.Fields(1)
                    
                    '--IF T_UPLOAD RANGECODE IS OUT OF RANGE AND T_UPLOAD_HIS RANGECODE ISNT, subtract 1 TO OORCNT
                    If (rngecode = "3" Or rngecode = "4") And (rngecodehis <> "3" Or rngecodehis <> "4") Then
                        oorcount = "OORCNT - 1"
                        'sqlrange = "UPDATE dbo.T_BKINFO_HIS SET OORCNT = '" & oorcount & "' WHERE BOOKNO = '" & MRU & "' and CYCLE = " & valCycle & " AND ORIGDATA = 0"
                        'DBExecute sqlrange, "", ""
                        '--IF T_UPLOAD_HIS RANGECODE IS OUT OF RANGE AND T_UPLOAD RANGECODE ISNT, add 1 TO OORCNT
                    ElseIf (rngecode <> "3" Or rngecode <> "4") And (rngecodehis = "3" Or rngecodehis = "4") Then
                        oorcount = "OORCNT + 1"
                        'sqlrange = "UPDATE dbo.T_BKINFO_HIS SET OORCNT = '" & oorcount & "' WHERE BOOKNO = '" & MRU & "' and CYCLE = " & valCycle & " AND ORIGDATA = 0"
                        'DBExecute sqlrange, "", ""
                    Else
                        oorcount = "OORCNT"
                    End If
                    
                    '--IF T_UPLOAD READTAG IS 01 AND T_UPLOAD_HIS READTAG ISNT, subtract 1 TO READCNT AND add 1 TO UNREAD
                    If RDTAG = "01" And rdtaghis <> "01" Then
                        rdcnt = "READCNT - 1"
                        unrdcnt = "UNREAD + 1"
                        'sqlread = "UPDATE dbo.T_BKINFO_HIS SET READCNT = '" & rdcnt & "', UNREAD = '" & unrdcnt & "' WHERE BOOKNO = '" & MRU & "' and CYCLE = " & valCycle & " AND ORIGDATA = 0"
                        'DBExecute sqlread, "", ""
                        '--IF T_UPLOAD_HIS READTAG IS 01 AND T_UPLOAD READTAG ISNT, subtract 1 TO UNREAD AND add 1 TO READCNT
                    ElseIf RDTAG <> "01" And rdtaghis = "01" Then
                        rdcnt = "READCNT + 1"
                        unrdcnt = "UNREAD - 1"
                        'sqlread = "UPDATE dbo.T_BKINFO_HIS SET READCNT = '" & rdcnt & "', UNREAD = '" & unrdcnt & "' WHERE BOOKNO = '" & MRU & "' and CYCLE = " & valCycle & " AND ORIGDATA = 0"
                        'DBExecute sqlread, "", ""
                    Else
                        rdcnt = "READCNT"
                        unrdcnt = "UNREAD"
                    End If
                    g_rs_TUPLOADHIS.Close
                    Set g_rs_TUPLOADHIS = Nothing
                    
                    'DELETE VALIDATED READINGS FROM T_UPLOAD_HIS
                    SQLDelete = "DELETE FROM dbo.T_UPLOAD_HIS WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle
                    DBExecute SQLDelete, "", ""
                    'DELETE ROW FROM T_BKINFO_HIS IF NO OTHER VALIDATED READINGS FOR THE BOOKNO AND CYCLE
                    'EXIST IN T_UPLOAD_HIS OTHERWISE UPDATE T_BKINFO_HIS OORCNT, UNREAD AND READ COUNT
                    If Not OpenRecordset(g_rs_TUPLOADHIS, "SELECT BOOKNO FROM T_UPLOAD_HIS WHERE BOOKNO = '" & MRU & "' AND ULCYCLE = " & valCycle, Me.Name, "") Then
                        'NO OTHER DATA EXIST IN T_UPLOAD HIS FOR BOOKNO AND CYCLE
                        SQLDelete2 = "DELETE FROM dbo.T_BKINFO_HIS WHERE BOOKNO = '" & MRU & "' AND CYCLE = " & valCycle
                        DBExecute SQLDelete2, "", ""
                    Else
                        'GET COUNTS FOR UNREAD, READCNT AND OORCNT
                        'SQLUpdate2 = "UPDATE dbo.T_BKINFO_HIS SET UNREAD = " & unrdcnt & " , READCNT = " & rdcnt & ", OORCNT = " & RCode & " WHERE BOOKNO = '" & MRU & "' AND CYCLE = " & valCycle
                        SQLUpdate2 = "UPDATE dbo.T_BKINFO_HIS SET READCNT = " & rdcnt & ", UNREAD = " & unrdcnt & ", OORCNT = " & oorcount & " WHERE BOOKNO = '" & MRU & "' and CYCLE = " & valCycle & " AND ORIGDATA = 0"
                        DBExecute SQLUpdate2, "", ""
                        g_rs_TUPLOADHIS.Close
                        Set g_rs_TUPLOADHIS = Nothing
                    End If
                
                End If
                g_rs_TUPLOAD.Close
                Set g_rs_TUPLOAD = Nothing
            End If
        End If
    Next X
    If Count <> 0 Then
        MsgBox Count & " Records Updated!", vbInformation
        genMasterList
        checked = 0
        Count = 0
    End If
End If

End Sub

Private Sub cmdGenReport_Click()
Dim Dateparam As String
Dim sFName As String
Dim s As Integer
Dim driveSource As String
Dim Appl As New CRAXDRT.Application
Dim Report As New CRAXDRT.Report
Dim fileName As String
Dim fso As Object
Dim X As Integer
Dim tempBC As String
Dim sCond As String

Set fso = CreateObject("Scripting.FileSystemObject")
driveSource = Mid(App.Path, 1, 2)

If lsvMaster.ListItems.Count > 0 Then

    If fso.FolderExists(driveSource & "\VALIDATION REPORTS") = False Then
    fso.CreateFolder (driveSource & "\VALIDATION REPORTS")
    End If
    
    If fso.FolderExists(driveSource & "\VALIDATION REPORTS\OC") = False Then
    fso.CreateFolder (driveSource & "\VALIDATION REPORTS\OC")
    End If
    
    If fso.FolderExists(driveSource & "\VALIDATION REPORTS\OC\" & Year(Now)) = False Then
    fso.CreateFolder (driveSource & "\VALIDATION REPORTS\OC\" & Year(Now))
    End If
    
    If fso.FolderExists(driveSource & "\VALIDATION REPORTS\OC\" & Year(Now) & "\" & Padl(Month(Now), 2, 0)) = False Then
    fso.CreateFolder (driveSource & "\VALIDATION REPORTS\OC\" & Year(Now) & "\" & Padl(Month(Now), 2, 0))
    End If
    
    If fso.FolderExists(driveSource & "\VALIDATION REPORTS\OC\" & Year(Now) & "\" & Padl(Month(Now), 2, 0) & "\" & Padl(Day(Now), 2, 0)) = False Then
    fso.CreateFolder (driveSource & "\VALIDATION REPORTS\OC\" & Year(Now) & "\" & Padl(Month(Now), 2, 0) & "\" & Padl(Day(Now), 2, 0))
    End If
    
    
 If valMBC <> "ALL" Then
 
         fileName = driveSource & "\VALIDATION REPORTS\OC\" & Year(Now) & "\" & Padl(Month(Now), 2, 0) & "\" & Padl(Day(Now), 2, 0) & "\"
         
         Set Appn = CreateObject("CrystalRunTime.Application")
         Set cReport = Appn.OpenReport(App.Path & "\crystal reports\" & "VALIDATION DETAILED REPORT genrep.rpt")
         'Or get your report name into the report somehow
         cReport.Database.LogOnServer "p2ssql.dll", "SZ-MWSI-SUBIC", "MCFSDB", "sa", "sqladmin"
         For s = 1 To cReport.Database.Tables.Count
         cReport.Database.Tables(s).ConnectionProperties.Item("Password") = "sqladmin"
         Next s
        
        
        sFName = valMBC & "_OC Validation_" & valStatus & "_" & Padl(Month(valDate3), 2, 0) & Padl(Day(valDate3), 2, 0) & Year(valDate3) & ".xls"
        cReport.DiscardSavedData
        cReport.ParameterFields(1).AddCurrentValue valMBCOde
        cReport.ParameterFields(2).AddCurrentValue valCycle
        cReport.ParameterFields(3).AddCurrentValue valDate3
        cReport.ParameterFields(4).AddCurrentValue valDate4
        cReport.ParameterFields(5).AddCurrentValue 2
            If valStatus = "PENDING" Then
                sCond = "0"
            ElseIf valStatus = "CORRECTED" Then
                sCond = "1"
            ElseIf valStatus = "CLOSED" Then
                sCond = "2"
            ElseIf valStatus = "FORCED CLOSED" Then
                sCond = "3"
            Else
                sCond = "ALL"
            End If
        cReport.ParameterFields(6).AddCurrentValue sCond
        cReport.ParameterFields(7).AddCurrentValue 1
        
        cReport.ExportOptions.DiskFileName = fileName & sFName
        cReport.ExportOptions.DestinationType = crEDTDiskFile
        cReport.ExportOptions.FormatType = crEFTExcelDataOnly
        cReport.ExportOptions.ExcelAreaType = crDetail
        cReport.ExportOptions.ExcelUseFormatInDataOnly = True
        cReport.ExportOptions.ExcelMaintainRelativeObjectPosition = True
        cReport.ExportOptions.ExcelMaintainColumnAlignment = True
        cReport.ExportOptions.ExcelChopPageHeader = False
        cReport.Export False
Else
        For X = 1 To cmbMBC.ListCount - 1
            tempBC = cmbMBC.List(X)
            Dim rsbccode As ADODB.Recordset
            Dim bcCodes As String
            If OpenRecordset(rsbccode, "SELECT BC_CODE FROM R_BUSCTR WHERE BC_DESC = '" & tempBC & "'", "", "") Then
            bcCodes = rsbccode.Fields(0)
            End If
            rsbccode.Close
            Set rsbccode = Nothing
            
            fileName = driveSource & "\VALIDATION REPORTS\OC\" & Year(Now) & "\" & Padl(Month(Now), 2, 0) & "\" & Padl(Day(Now), 2, 0) & "\"
         
             Set Appn = CreateObject("CrystalRunTime.Application")
             Set cReport = Appn.OpenReport(App.Path & "\crystal reports\" & "VALIDATION DETAILED REPORT genrep.rpt")
            'Or get your report name into the report somehow
             cReport.Database.LogOnServer "p2ssql.dll", "SZ-MWSI-SUBIC", "MCFSDB", "sa", "sqladmin"
             For s = 1 To cReport.Database.Tables.Count
             cReport.Database.Tables(s).ConnectionProperties.Item("Password") = "sqladmin"
             Next s
        
        
            sFName = tempBC & "_OC Validation_" & valStatus & "_" & Padl(Month(valDate3), 2, 0) & Padl(Day(valDate3), 2, 0) & Year(valDate3) & ".xls"
            cReport.DiscardSavedData
            cReport.ParameterFields(1).AddCurrentValue bcCodes
            cReport.ParameterFields(2).AddCurrentValue valCycle
            cReport.ParameterFields(3).AddCurrentValue valDate3
            cReport.ParameterFields(4).AddCurrentValue valDate4
            cReport.ParameterFields(5).AddCurrentValue 2
            If valStatus = "PENDING" Then
                sCond = "0"
            ElseIf valStatus = "CORRECTED" Then
                sCond = "1"
            ElseIf valStatus = "CLOSED" Then
                sCond = "2"
            ElseIf valStatus = "FORCED CLOSED" Then
                sCond = "3"
            Else
                sCond = "ALL"
            End If
            cReport.ParameterFields(6).AddCurrentValue sCond
            cReport.ParameterFields(7).AddCurrentValue 1
            
            cReport.ExportOptions.DiskFileName = fileName & Replace(sFName, "/", "_")
            cReport.ExportOptions.DestinationType = crEDTDiskFile
            cReport.ExportOptions.FormatType = crEFTExcelDataOnly
            cReport.ExportOptions.ExcelAreaType = crDetail
            cReport.ExportOptions.ExcelUseFormatInDataOnly = True
            cReport.ExportOptions.ExcelMaintainRelativeObjectPosition = True
            cReport.ExportOptions.ExcelMaintainColumnAlignment = True
            cReport.ExportOptions.ExcelChopPageHeader = False
            cReport.Export False
        
        
        
        Next X
 End If
MsgBox "The reports have been successfully saved.", , "Generation Successful"
End If

End Sub

Private Sub cmdMasAll_Click()
SelectAll 1, 1
End Sub

Private Sub cmdmasNone_Click()
SelectAll 0, 1
End Sub

Private Sub cmdRemove_Click()
Dim X As Integer
Dim MRU As String
Dim ACCTNUM As String
Dim SQLUpdate As String
Dim SQLUpdate1 As String
Dim Count As String
Dim txtStatus As String
Dim checked As Integer
Dim ans As Integer
checked = 0
Count = 0
For X = 1 To lsvMaster.ListItems.Count
  If lsvMaster.ListItems(X).checked = True Then
    If lsvMaster.ListItems.Item(X).ListSubItems(6) <> "PENDING" Then
     checked = 1
    End If
    If lsvMaster.ListItems.Item(X).ListSubItems(6) = "PENDING" Then
     Count = Count + 1
    End If
  End If
Next X

If checked = 1 Then
MsgBox "Only Accounts with Pending Status can be removed from masterlist"
ElseIf checked = 0 And Count > 0 Then
ans = MsgBox("Are you sure you want to remove this account from masterlist?", vbYesNo, "Remove from Masterlist")

If ans = 6 Then
'UPDATE MASTERLIST TO EXTRACTED
   For X = 1 To lsvMaster.ListItems.Count
     If lsvMaster.ListItems(X).checked = True Then
       MRU = lsvMaster.ListItems.Item(X).ListSubItems(1)
       ACCTNUM = lsvMaster.ListItems.Item(X).ListSubItems(2)
       SQLUpdate = "UPDATE dbo.T_VER_EXTRACT SET MASTERLIST = 0 WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle
       DBExecute SQLUpdate, "", ""
      If OpenRecordset(g_rs_TUPLOADHIS, "SELECT * FROM dbo.T_UPLOAD_HIS WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle, Me.Name, "") Then
       SQLUpdate1 = "UPDATE dbo.T_UPLOAD_HIS SET MASTERLIST = 0 WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle
       DBExecute SQLUpdate1, "", ""
      End If
     End If
   Next X
   If Count <> 0 Then
        MsgBox Count & " Records removed from Masterlist", vbInformation
        genlsvExtract
        ClearComboMaster
        genMasterList
   checked = 0
   Count = 0
   End If
 End If
End If

End Sub

Private Sub cmdRemovefrExtractd_Click()
Dim X As Integer
Dim MRU As String
Dim ACCTNUM As String
Dim SQLUpdate As String
Dim Count As String

Count = 0
For X = 1 To lsvExtract.ListItems.Count
   If lsvExtract.ListItems(X).checked = True Then
     'for checked items
      MRU = lsvExtract.ListItems.Item(X).ListSubItems(1)
      ACCTNUM = lsvExtract.ListItems.Item(X).ListSubItems(2)
      SQLUpdate = "DELETE from dbo.T_VER_EXTRACT WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle
      DBExecute SQLUpdate, "", ""
      Count = Count + 1
   End If
   
Next X
MsgBox Count & " Records Deleted!", vbInformation

genlsvExtract
'genMasterList

End Sub

Private Sub Command1_Click()
Dim X As Integer
Dim Count As Integer
Dim action(500) As String
Dim count1 As Integer
Dim checker As Integer

Count = 0
checker = 0
For X = 1 To lsvMaster.ListItems.Count
    If lsvMaster.ListItems(X).checked Then
    action(Count) = lsvMaster.ListItems.Item(X).ListSubItems(7)
     g_ActionItem = action(Count)
    Count = Count + 1
   
    End If
Next X

For X = 1 To Count - 1
   If action(0) <> action(X) Then
   checker = 1
   End If
  
Next X

If Count > 0 And checker = 0 Then
frmAction.Show

Else

MsgBox "Kindly select record to add action item, action items must be the same", vbCritical

End If


End Sub

Private Sub DTPicker1_Change()
   valDate1 = DTPicker1.Value
   ClearComboExtract
   genlsvExtract
End Sub



Private Sub DTPicker2_Change()
   valDate2 = DTPicker2.Value
   ClearComboExtract
   genlsvExtract
End Sub



Private Sub DTPicker3_Change()
   valDate3 = DTPicker3.Value
   ClearComboMaster
   genMasterList
End Sub

Private Sub DTPicker4_Change()
   valDate4 = DTPicker4.Value
   ClearComboMaster
   genMasterList
End Sub

Private Sub Form_Activate()
If WindowState = vbNormal Then
    Me.Move (0)
    Me.Top = 0
    Me.Height = 10325
    Me.Width = 15290
End If
'Me.Height = 11625
'Me.Width = 15810

'Me.Move (0)
'Me.Top = 0
'Me.Height = 10230
'Me.Width = 15270

'initialize form
InitForm
'check parameters for extraction
checkParam
'generate extracted accounts
genlsvExtract
genMasterList
cmbStatus.AddItem "ALL"
End Sub

Private Sub InitForm()
cmbCycle.Text = Month(Date)
DTPicker1.Value = Date
DTPicker2.Value = Date
DTPicker3.Value = Date
DTPicker4.Value = Date



End Sub

Function genlsvExtract()
Dim rsExtract As ADODB.Recordset
Dim SQLextract As String
Dim l_obj_Item As Object

lsvExtract.ListItems.Clear


SQLextract = "EXEC sp_GEN_EXTRACTACCOUNTS " & CStr(valCycle) & ",'" & valBC & "','" & valMRU & "','" & valOC & "','" & valDate1 & "','" & valDate2 & "', 0 ,'ALL'"

If OpenRecordset(rsExtract, SQLextract, "", "") Then
    While Not rsExtract.EOF
   Set l_obj_Item = lsvExtract.ListItems.Add(, , CheckNull(rsExtract.Fields("BC_DESC")))
            l_obj_Item.SubItems(1) = CheckNull(rsExtract.Fields("BOOKNO"))
            l_obj_Item.SubItems(2) = CheckNull(rsExtract.Fields("ACCTNUM"))
            l_obj_Item.SubItems(3) = CheckNull(rsExtract.Fields("PRESRDG"))
            l_obj_Item.SubItems(4) = CheckNull(rsExtract.Fields("FFCODE"))
            l_obj_Item.SubItems(5) = CheckNull(rsExtract.Fields("REMARKS"))
    Set l_obj_Item = Nothing
    
    'for the combo box
    genComboBC (rsExtract.Fields("BC_DESC"))
    genComboMRU (rsExtract.Fields("BOOKNO"))
    genComboOC (rsExtract.Fields("FFCODE"))
    rsExtract.MoveNext
    Wend
rsExtract.Close
End If

lblCountExtract.Caption = lsvExtract.ListItems.Count
cmbBusinessCenter.Text = valBC
cmbOC.Text = valOC
cmbMRU.Text = valMRU

End Function
Private Sub genComboBC(bcname As String)
Dim X As Integer
Dim checker As Integer

checker = 0
For X = 0 To cmbBusinessCenter.ListCount
   If bcname = cmbBusinessCenter.List(X) Then
    checker = 1
   End If
Next X

If checker = 0 Then cmbBusinessCenter.AddItem bcname

End Sub


Private Sub genComboMBC(bcname As String)
Dim X As Integer
Dim checker As Integer

checker = 0
For X = 0 To cmbMBC.ListCount
   If bcname = cmbMBC.List(X) Then
    checker = 1
   End If
Next X

If checker = 0 Then cmbMBC.AddItem bcname

End Sub

Private Sub genComboMRU(booknum As String)
Dim X As Integer
Dim checker As Integer

checker = 0
For X = 0 To cmbMRU.ListCount
    If booknum = cmbMRU.List(X) Then
      checker = 1
    End If
Next X

If checker = 0 Then cmbMRU.AddItem booknum
End Sub

Private Sub genComboOC(bcname As String)
Dim X As Integer
Dim checker As Integer

checker = 0
For X = 0 To cmbOC.ListCount
   If bcname = cmbOC.List(X) Then
    checker = 1
   End If
Next X

If checker = 0 Then cmbOC.AddItem bcname

End Sub
Private Sub genComboMOC(bcname As String)
Dim X As Integer
Dim checker As Integer

checker = 0
For X = 0 To cmbMOC.ListCount
   If bcname = cmbMOC.List(X) Then
    checker = 1
   End If
Next X

If checker = 0 Then cmbMOC.AddItem bcname

End Sub

Private Sub genComboMMRU(booknum As String)
Dim X As Integer
Dim checker As Integer

checker = 0
For X = 0 To cmbMMRU.ListCount
    If booknum = cmbMMRU.List(X) Then
      checker = 1
    End If
Next X

If checker = 0 Then cmbMMRU.AddItem booknum
End Sub

Private Sub genComboStatus(booknum As String)
Dim X As Integer
Dim checker As Integer

checker = 0
For X = 0 To cmbStatus.ListCount
    If booknum = cmbStatus.List(X) Then
      checker = 1
    End If
Next X

If checker = 0 Then cmbStatus.AddItem booknum
End Sub

Function checkParam()
valCycle = cmbCycle.Text
valDate1 = DTPicker1.Value
valDate2 = DTPicker2.Value
valDate3 = DTPicker3.Value
valDate4 = DTPicker4.Value

valBC = cmbBusinessCenter.Text
valMRU = cmbMRU.Text
valOC = cmbOC.Text
valMBC = cmbMBC.Text
valMMRU = cmbMMRU.Text
valMOC = cmbMOC.Text
valStatus = cmbStatus.Text


If valCycle = Null Or valDate1 = Null Or valDate2 = Null Or valBC = Null Or valMRU = Null Or valOC = Null Then
MsgBox "Kindly supply all parameters needed to generate extracted accounts!", vbCritical
End If




End Function

Private Function SelectAll(ByVal DeOrSelectAll As Integer, ByVal Listbx As Integer) As Boolean ' Added the action that is needed
  Dim Item As ListItem

'to handle extracted window
If Listbx = 0 Then
  SelectAll = True ' Default succes
  Select Case DeOrSelectAll
    Case 0 ' Unchecked Check1
      For Each Item In lsvExtract.ListItems
        Item.checked = False ' Deselect
      Next
    Case 1 ' Checked Check1
      For Each Item In lsvExtract.ListItems
        Item.checked = True ' Select
      Next
    Case Else ' 2 = Grayed or else so no action for now....
      SelectAll = False ' No succes reported back...
  End Select
End If

'to handle masterlist window
If Listbx = 1 Then
  SelectAll = True ' Default succes
  Select Case DeOrSelectAll
    Case 0 ' Unchecked Check1
      For Each Item In lsvMaster.ListItems
        Item.checked = False ' Deselect
      Next
    Case 1 ' Checked Check1
      For Each Item In lsvMaster.ListItems
        Item.checked = True ' Select
      Next
    Case Else ' 2 = Grayed or else so no action for now....
      SelectAll = False ' No succes reported back...
  End Select
End If

End Function


Function genMasterList()
Dim rsExtract As ADODB.Recordset
Dim SQLextract As String
Dim l_obj_Item As Object

lsvMaster.ListItems.Clear


SQLextract = "EXEC sp_GEN_EXTRACTACCOUNTS " & CStr(valCycle) & ",'" & valMBC & "','" & valMMRU & "','" & valMOC & "','" & valDate3 & "','" & valDate4 & "',1,'" & valStatus & "'"

If OpenRecordset(rsExtract, SQLextract, "", "") Then
    While Not rsExtract.EOF
   Set l_obj_Item = lsvMaster.ListItems.Add(, , CheckNull(rsExtract.Fields("BC_DESC")))
            l_obj_Item.SubItems(1) = CheckNull(rsExtract.Fields("BOOKNO"))
            l_obj_Item.SubItems(2) = CheckNull(rsExtract.Fields("ACCTNUM"))
            l_obj_Item.SubItems(3) = CheckNull(rsExtract.Fields("PRESRDG"))
            l_obj_Item.SubItems(4) = CheckNull(rsExtract.Fields("FFCODE"))
            l_obj_Item.SubItems(5) = CheckNull(rsExtract.Fields("REMARKS"))
            l_obj_Item.SubItems(6) = CheckNull(rsExtract.Fields("STATUS"))
            l_obj_Item.SubItems(7) = CheckNull((rsExtract.Fields(8).Value))
    Set l_obj_Item = Nothing
    
    'for the combo box
    genComboMBC (rsExtract.Fields("BC_DESC"))
    genComboMMRU (rsExtract.Fields("BOOKNO"))
    genComboMOC (rsExtract.Fields("FFCODE"))
    genComboStatus (rsExtract.Fields("STATUS"))
    rsExtract.MoveNext
    Wend
rsExtract.Close

End If

lblCountMaster.Caption = lsvMaster.ListItems.Count
cmbMBC.Text = valMBC
cmbMMRU.Text = valMMRU
cmbMOC.Text = valMOC
cmbStatus.Text = valStatus
End Function
