VERSION 5.00
Begin VB.Form frmAbout 
   BorderStyle     =   0  'None
   Caption         =   "<App Version Number>"
   ClientHeight    =   6405
   ClientLeft      =   3150
   ClientTop       =   585
   ClientWidth     =   6795
   Icon            =   "frmAbout.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6405
   ScaleWidth      =   6795
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdOk 
      Caption         =   "&OK"
      Height          =   390
      Left            =   2970
      TabIndex        =   7
      Top             =   5910
      Width           =   915
   End
   Begin VB.Frame Frame1 
      Height          =   5655
      Left            =   150
      TabIndex        =   0
      Top             =   180
      Width           =   6465
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Indra Philippines, Inc."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   1500
         TabIndex        =   6
         Top             =   4320
         Width           =   3135
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Developed by"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   1496
         TabIndex        =   5
         Top             =   3960
         Width           =   3135
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "SubicWater"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   1309
         TabIndex        =   4
         Top             =   2460
         Width           =   3630
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Developed for"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   1309
         TabIndex        =   3
         Top             =   2130
         Width           =   3630
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "<App Version Number>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   1920
         TabIndex        =   2
         Top             =   1200
         Width           =   2595
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "COMMERCIAL SERVICES SYSTEM HEADQUARTERS"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   1
         Left            =   480
         TabIndex        =   1
         Top             =   600
         Width           =   5490
      End
   End
End
Attribute VB_Name = "frmAbout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdOk_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Label1(4).Caption = g_Company
    Label1(2).Caption = "Version " & App.Major & "." & App.Minor & "." & App.Revision
End Sub

