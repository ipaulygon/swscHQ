VERSION 5.00
Begin VB.Form frmValSettings 
   Caption         =   "Validation Settings"
   ClientHeight    =   1815
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4785
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   1815
   ScaleWidth      =   4785
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save"
      Default         =   -1  'True
      Height          =   390
      Left            =   1500
      TabIndex        =   5
      Top             =   1320
      Width           =   915
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   390
      Left            =   2520
      TabIndex        =   4
      Top             =   1320
      Width           =   915
   End
   Begin VB.Frame Frame1 
      Caption         =   "Directories"
      Height          =   1095
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4575
      Begin VB.TextBox txtDSource 
         Enabled         =   0   'False
         Height          =   285
         Left            =   240
         TabIndex        =   2
         Top             =   600
         Width           =   3735
      End
      Begin VB.CommandButton cmdDSource 
         Caption         =   "..."
         Height          =   285
         Left            =   4080
         TabIndex        =   1
         Top             =   585
         Width           =   385
      End
      Begin VB.Label Label5 
         Caption         =   "Verified File Destination : "
         Height          =   285
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   2475
      End
   End
End
Attribute VB_Name = "frmValSettings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim driveSource As String

Private Sub cmdCancel_Click()
Unload Me
End Sub

Private Sub cmdDSource_Click()
frmFileSourcebilled.Show

End Sub

Private Sub cmdSave_Click()
Dim SQLInsert As String
If txtDSource.Text <> "" Then
    SQLInsert = "INSERT INTO dbo.T_VALSETTINGS(Drivesource) VALUES ('" & txtDSource.Text & "')"
    g_Conn.Execute ("DELETE FROM dbo.T_VALSETTINGS")
    g_Conn.Execute (SQLInsert)
    MsgBox "Settings Saved!"
Else
    MsgBox "Please select source drive!"
End If

End Sub

Private Sub Form_Load()
If WindowState = vbNormal Then
    Me.Move (1000)
    Me.Top = 500
    Me.Height = 2325
    Me.Width = 4905
End If
GetSettings
End Sub


Private Sub GetSettings()
Dim RSDrv As ADODB.Recordset
Dim SQLstring As String

SQLstring = "SELECT DriveSource FROM T_VALSETTINGS"



If OpenRecordset(RSDrv, SQLstring, "", "") Then
    While Not RSDrv.EOF
    driveSource = RSDrv.Fields(0).Value
    RSDrv.MoveNext
    Wend
End If
RSDrv.Close
Set RSDrv = Nothing
txtDSource.Text = driveSource
End Sub
