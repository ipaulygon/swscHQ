VERSION 5.00
Begin VB.Form frmdnupVal 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Download Upload Text Validation"
   ClientHeight    =   7035
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13200
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7035
   ScaleWidth      =   13200
   Begin VB.TextBox Text2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5400
      TabIndex        =   1
      Top             =   120
      Width           =   5895
   End
   Begin VB.ComboBox cmbProcess 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmdnupVal.frx":0000
      Left            =   1320
      List            =   "frmdnupVal.frx":0007
      TabIndex        =   0
      Top             =   120
      Width           =   2535
   End
   Begin VB.Frame frameValidation 
      Caption         =   "VALIDATION"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6255
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   12975
      Begin VB.DriveListBox drvVal 
         Height          =   315
         Left            =   120
         TabIndex        =   8
         Top             =   360
         Width           =   3015
      End
      Begin VB.FileListBox fileVal 
         Height          =   4965
         Left            =   3240
         Pattern         =   "*.txt"
         TabIndex        =   7
         Top             =   360
         Width           =   2895
      End
      Begin VB.DirListBox dirVal 
         Height          =   4590
         Left            =   120
         TabIndex        =   6
         Top             =   720
         Width           =   3015
      End
      Begin VB.TextBox Text1 
         Height          =   5655
         Left            =   6240
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   5
         Top             =   360
         Width           =   6615
      End
      Begin VB.CommandButton cmdVerify 
         Caption         =   "&Validate"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   120
         TabIndex        =   4
         Top             =   5520
         Width           =   6015
      End
   End
   Begin VB.Label Label1 
      Caption         =   "Filename:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4080
      TabIndex        =   9
      Top             =   120
      Width           =   1335
   End
   Begin VB.Label lblProcess 
      Caption         =   "Process:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "frmdnupVal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim txtUDest As String
Dim txtDSource As String

Private Sub cmbProcess_Click()
DisableVerify
    If cmbProcess.Text = "Upload File" Then
        drvVal.Drive = Mid(txtUDest, 1, 3)
        dirVal.Path = txtUDest & "\" & "current"
    Else
        drvVal.Drive = Mid(txtDSource, 1, 3)
        dirVal.Path = txtDSource
    End If
    
End Sub

Private Sub cmdVerify_Click()
Dim X As Integer

If fileVal.ListCount = 0 Then
    'Unload frmProg
    MsgBox "No data to load, please check the directory.", vbExclamation, "Validation"
    
End If

If cmbProcess.Text = "Upload File" Then
For X = 0 To fileVal.ListCount - 1
    If Mid(fileVal.Path, Len(fileVal.Path), 1) <> "\" Then
    Text2.Text = fileVal.Path & "\" & fileVal.List(X)
    Else
    Text2.Text = fileVal.Path & fileVal.List(X)
    End If
        Text1.Text = Text1.Text & "Analyzing Upload file: " & fileVal.List(X) & vbCrLf
        Text1.Text = Text1.Text & "=====================================" & vbCrLf
        analyze_upload (Text2.Text)
Next X
Else
For X = 0 To fileVal.ListCount - 1
    If Mid(fileVal.Path, Len(fileVal.Path), 1) <> "\" Then
    Text2.Text = fileVal.Path & "\" & fileVal.List(X)
    Else
    Text2.Text = fileVal.Path & fileVal.List(X)
    End If
        Text1.Text = Text1.Text & "Analyzing Download file: " & fileVal.List(X) & vbCrLf
        Text1.Text = Text1.Text & "=====================================" & vbCrLf
        CheckDataIntegrity (Text2.Text)
Next X
End If


End Sub

Private Sub analyze_upload(fileName1)

Dim nFileNum As Integer, sText As String, sNextLine As String, lLineCount As Long
Dim lineLength As Integer
Dim i As Integer
Dim j As Integer
Dim position1 As Integer
Dim c As Integer
Dim position2 As Integer
Dim position3 As Integer
Dim totals As Double
Dim subTotals As Double
Dim subtotal As Double
Dim subDetails As Double
Dim disc As Double
Dim zavPrc As Integer
Dim beginning As Integer
Dim docno As String
Dim pos1 As Integer
Dim pos2 As Integer
Dim msgString As String
Dim vat As Double
i = 1
j = 1
' Get a free file number
nFileNum = FreeFile
beginning = 0
' Open a text file for input. inputbox returns the path to read the file
Open fileName1 For Input As nFileNum

' Read the contents of the file
Do While Not EOF(nFileNum)
   Line Input #nFileNum, sNextLine
   
        If Mid(sNextLine, 1, 1) = "H" Then
            'Header
            'Get the Value of the Total Amount
            lineLength = Len(sNextLine)
                i = 0
                j = 0
                c = 0
                position1 = 0
                position2 = 0
                position3 = 0
                
               
'                If zavPrc = 1 Then
'                    If totals <> Subtotal Then Text1.Text = Text1.Text & docno & " -  Wrong totals: " & CStr(totals) & " <> " & CStr(Subtotal) & vbCrLf
'                End If
'                If zavPrc = 0 Then
'                    If Val(totals) <> Val(Subtotal + vat) Then Text1.Text = Text1.Text & docno & " -  Wrong totals: " & CStr(totals) & " <> " & CStr(Subtotal + disc) & vbCrLf
'                End If
                If Val(totals) <> Val(subtotal + vat) Then Text1.Text = Text1.Text & docno & " -  Wrong totals: " & CStr(totals) & " <> " & CStr(subtotal + vat) & vbCrLf
                
                zavPrc = 0
                subtotal = 0
  
            For i = 1 To lineLength
               If Mid(sNextLine, i, 1) = "|" Then
                 j = i
                 c = c + 1
                    If c = 3 Then
                        pos1 = j
                    End If
                    If c = 4 Then
                        pos2 = j
                    End If
                    If c = 19 Then
                        position1 = j
                    End If
                    If c = 20 Then
                        position2 = j
                    End If
                    If c = 21 Then
                        position3 = j
                    End If
               End If
            Next i
            totals = CDbl(Mid(sNextLine, position1 + 1, position2 - position1 - 1))
            vat = CDbl(Mid(sNextLine, position2 + 1, position3 - position2 - 1))
            docno = Mid(sNextLine, pos1 + 1, pos2 - pos1 - 1)
        Else
            subDetails = 0
        'Details
            If InStr(sNextLine, "ZAVPRC") <> 0 Then
                zavPrc = 1
            Else
                'If InStr(sNextLine, "ZDISCN") = 0 Then
                    lineLength = Len(sNextLine)
                    i = 0
                    j = 0
                    c = 0
                    For i = 1 To lineLength
                        If Mid(sNextLine, i, 1) = "|" Then
                                j = i
                                c = c + 1
                                If c = 5 Then
                                    position1 = j
                                End If
                                If c = 6 Then
                                    position2 = j
                                End If
                         End If
                     Next i
                     subDetails = CDbl(Mid(sNextLine, position1 + 1, position2 - position1 - 1))
                     subtotal = subtotal + subDetails
'               ' Else 'discount
'                    If zavPrc = 1 Then
'                        Subtotal = Subtotal + subDetails
'                    Else
'                        lineLength = Len(sNextLine)
'                        i = 0
'                        j = 0
'                        c = 0
'                        For i = 1 To lineLength
'                            If Mid(sNextLine, i, 1) = "|" Then
'                                    j = i
'                                    c = c + 1
'                                    If c = 5 Then
'                                        position1 = j
'                                    End If
'                                    If c = 6 Then
'                                        position2 = j
'                                    End If
'                             End If
'                         Next i
'                         subDetails = CDbl(Mid(sNextLine, position1 + 1, position2 - position1 - 1))
'                         Subtotal = Subtotal + subDetails
'                    End If
                'End If
            End If
            
            
        End If
        
   sNextLine = sNextLine & vbCrLf
   sText = sText & sNextLine

Loop
'Text1.Text = sText

' Close the file
Close nFileNum
Text1.Text = Text1.Text & "Done........" & vbCrLf & vbCrLf
cmdVerify.Enabled = False


End Sub

Private Sub dirVal_Change()
fileVal.Path = dirVal.Path
cmdVerify.Enabled = False
Text1.Text = ""
Text2.Text = ""
If fileVal.ListCount > 0 Then
    cmdVerify.Enabled = True
Else
    cmdVerify.Enabled = False
End If

End Sub

Private Sub dirVal_Click()
cmdVerify.Enabled = False
Text1.Text = ""
Text2.Text = ""
End Sub

Private Sub drvVal_Change()
dirVal.Path = drvVal.Drive
cmdVerify.Enabled = False
Text1.Text = ""
Text2.Text = ""
End Sub

Private Sub fileVal_Click()
If Mid(fileVal.Path, Len(fileVal.Path), 1) <> "\" Then
Text2.Text = fileVal.Path & "\" & fileVal.fileName
Else
Text2.Text = fileVal.Path & fileVal.fileName
End If
cmdVerify.Enabled = True
Text1.Text = ""
End Sub

Private Sub Form_Load()

cmdVerify.Enabled = False

If OpenRecordset(g_rs_SPARAM, "SELECT * FROM S_PARAM", Me.Name, "cmdSave_Click") Then
g_rs_SPARAM.MoveFirst

txtDSource = CheckNull(g_rs_SPARAM.Fields("DIRDNLD"))
txtUDest = CheckNull(g_rs_SPARAM.Fields("DIRUPLD"))
End If


'CheckDataIntegrity "C:\HQ\Download\"
End Sub

Private Sub DisableVerify()
Text1.Text = ""
Text2.Text = ""
cmdVerify.Enabled = False

End Sub

Public Function CheckDataIntegrity(fileName1 As String) As Boolean
' use to copy the source file to "download.txt"
Dim f As Object, fso As Object
Dim fs, fc, BookList$, ans%

Dim rstxt As Recordset, rsDB As Recordset
Dim cnTxt As ADODB.Connection

'Dim FileFullPath As String

Dim ErrorCount As Integer

Dim val_SeriesNo As String
Dim fileseries As String

'new code db checking account 8-23-2013 jums
Dim ChkAccountRec As ADODB.Recordset
Dim ChkAccountSQl As String
Dim ChkAccount As String
Dim TxtExsistAccount As String
Dim TxtExsistTemp As String
Dim linecounter As Integer

Set fs = CreateObject("Scripting.FileSystemObject")
'Set f = fs.GetFolder(fileName1)
'Set fc = f.Files
    
'FileFullPath = filepath1 & "02000810.txt"
 
' Text file cn and rs
Set cnTxt = New ADODB.Connection
Set rstxt = New ADODB.Recordset
Set fso = CreateObject("Scripting.FileSystemObject")

fs.CopyFile fileName1, App.Path & "\download.txt"

'Open connections and recordsets
cnTxt.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & ";Extended Properties='text;HDR=No;FMT=Delimited'"
cnTxt.Open

ErrorCount = 0
'linecounter = -1 '08-27-2013 jums
linecounter = 1

fileseries = Right(fileName1, 11)
    
'rstxt.Open "SELECT * FROM download.txt ORDER BY SequenceNo", cnTxt, adOpenStatic, adLockOptimistic, adCmdText '08-27-2013 jums
rstxt.Open "SELECT * FROM download.txt ORDER BY AccountNum", cnTxt, adOpenStatic, adLockOptimistic, adCmdText
Do While Not rstxt.EOF



val_SeriesNo = rstxt.Fields("Series")
TxtExsistTemp = rstxt.Fields("AccountNum")

    If val_SeriesNo <> Mid(fileseries, 1, 6) Then
        ErrorCount = ErrorCount + 1
        'Text1.Text = Text1.Text & "Error on Line Item " & linecounter & " according by sequence number " & vbCrLf '08-27-2013 jums
        Text1.Text = Text1.Text & "Error on Line Item " & linecounter & " according by account number " & vbCrLf
    End If
    
    ChkAccountSQl = "Select acctnum from t_download where acctnum = '" & TxtExsistTemp & "'" & _
                    " and bill_month = '" & Mid(g_BillMonth, 5, 2) & "'"
    
    If OpenRecordset(ChkAccountRec, ChkAccountSQl, "", "") = True Then
        ErrorCount = ErrorCount + 1
        Text1.Text = Text1.Text & "Duplicate Record Found on Database Account number " & ChkAccountRec.Fields("acctnum") & vbCrLf
    Else
        If TxtExsistTemp = TxtExsistAccount Then
            ErrorCount = ErrorCount + 1
            Text1.Text = Text1.Text & "Duplicate Record Found on File Account number " & TxtExsistTemp & vbCrLf
        Else
        End If
    End If
    
    TxtExsistAccount = rstxt.Fields("AccountNum")
    linecounter = linecounter + 1
    
rstxt.MoveNext
Loop
rstxt.Close


If ErrorCount = 0 Then
    CheckDataIntegrity = True
    Text1.Text = Text1.Text & vbCrLf & "Done...." & vbCrLf & vbCrLf
Else
    CheckDataIntegrity = False
    'Text1.Text = Text1.Text & vbCrLf & "Series Number: " & val_SeriesNo & " has " & ErrorCount & " Errors" & vbCrLf & vbCrLf & "Done...." & vbCrLf & vbCrLf '08-27-2013 jums
    Text1.Text = Text1.Text & vbCrLf & "Download File: " & fileseries & " has " & ErrorCount & " Errors" & vbCrLf & vbCrLf & "Done...." & vbCrLf & vbCrLf
End If
'Text1.Text = Text1.Text & "Series Number: " & val_SeriesNo & " has " & ErrorCount & " Errors" & vbCrLf & vbCrLf & "Done...." & vbCrLf

End Function
