VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmDailyBatchGen 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Daily Batch Generation"
   ClientHeight    =   7935
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6510
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7935
   ScaleWidth      =   6510
   Begin VB.TextBox txtPDate 
      Height          =   315
      Left            =   1200
      TabIndex        =   7
      Top             =   420
      Width           =   950
   End
   Begin VB.CommandButton cmdGenerate 
      Caption         =   "&Generate"
      Enabled         =   0   'False
      Height          =   375
      Left            =   2490
      TabIndex        =   6
      Top             =   7320
      Width           =   1575
   End
   Begin MSComctlLib.ListView lsvBatchGen 
      Height          =   6135
      Left            =   1680
      TabIndex        =   5
      Top             =   960
      Width           =   3255
      _ExtentX        =   5741
      _ExtentY        =   10821
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "MRU"
         Object.Width           =   2470
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "SchedMRDate"
         Object.Width           =   2435
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Cycle"
         Object.Width           =   0
      EndProperty
   End
   Begin VB.CommandButton cmdFilter 
      Caption         =   "&Filter"
      Height          =   375
      Left            =   5400
      TabIndex        =   4
      Top             =   360
      Width           =   975
   End
   Begin MSComCtl2.DTPicker DTPDate 
      Height          =   315
      Left            =   1200
      TabIndex        =   3
      Top             =   420
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      _Version        =   393216
      Format          =   59244545
      CurrentDate     =   39615
   End
   Begin VB.ComboBox cmbBC 
      Height          =   315
      Left            =   2880
      TabIndex        =   2
      Top             =   420
      Width           =   2415
   End
   Begin VB.Label Label2 
      Caption         =   "BC:"
      Height          =   315
      Left            =   2520
      TabIndex        =   1
      Top             =   420
      Width           =   495
   End
   Begin VB.Label Label1 
      Caption         =   "Posting Date:"
      Height          =   315
      Left            =   120
      TabIndex        =   0
      Top             =   420
      Width           =   1095
   End
End
Attribute VB_Name = "frmDailyBatchGen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim varRec As Variant
Dim RecBool As Boolean

Dim bc As String
Dim PDate As Date
Dim BCCDE As String
Dim intCycle As Integer

Private Sub cmdFilter_Click()
    Dim l_obj_Item As Object
    Dim l_str_Stat As String
    Dim PDte As String
    intCycle = Right(g_BillMonth, 2)
    
    PDate = CDate(txtPDate.Text)
    
    PDte = Format(PDate, "mm-dd-yyyy")
    cmdGenerate.Enabled = False
    lsvBatchGen.ListItems.Clear
    'If Not OpenRecordset(g_rs_TBOOK, "SELECT T_BOOK.BOOKNO, T_BOOK.SCHED_RDG_DT FROM T_BOOK INNER JOIN T_SCHED ON (T_BOOK.CYCLE = T_SCHED.CYCLE) AND (T_BOOK.BOOKNO = T_SCHED.BOOKNO) WHERE (((T_BOOK.BC_CODE)='" & Left(cmbBC, 4) & "') AND (((Format(UPLD_MWSI_DT,'mm-dd-yyyy')))='" & txtPDate.Text & "') AND ((T_SCHED.BKSTCODE)='FWM') AND ((T_BOOK.CYCLE)=" & intCycle & "))", Me.Name, "Form_Load") Then
    'If Not OpenRecordset(g_rs_TBOOK, "SELECT T_BOOK.BOOKNO, T_BOOK.SCHED_RDG_DT, T_BOOK.CYCLE FROM T_BOOK INNER JOIN T_SCHED ON (T_BOOK.CYCLE = T_SCHED.CYCLE) AND (T_BOOK.BOOKNO = T_SCHED.BOOKNO) WHERE (((T_BOOK.BC_CODE)='" & Left(cmbBC, 4) & "') AND (((Format(UPLD_MWSI_DT,'mm-dd-yyyy')))='" & txtPDate.Text & "') AND ((T_SCHED.BKSTCODE)='FWM'))", Me.Name, "Form_Load") Then
    If Not OpenRecordset(g_rs_TBOOK, "SELECT T_BOOK.BOOKNO, T_BOOK.SCHED_RDG_DT, T_BOOK.CYCLE FROM T_BOOK INNER JOIN T_SCHED ON (T_BOOK.CYCLE = T_SCHED.CYCLE) AND (T_BOOK.BOOKNO = T_SCHED.BOOKNO) WHERE (((T_BOOK.BC_CODE)='" & Left(cmbBC, 4) & "') AND (convert(char(12),UPLD_MWSI_DT, 110)='" & PDte & "') AND ((T_SCHED.BKSTCODE)='FWM'))", Me.Name, "Form_Load") Then
        MsgBox "No MRUs found for the selected parameters.", vbExclamation, "Warning"
        Exit Sub
    Else
        cmdGenerate.Enabled = True
        g_rs_TBOOK.MoveFirst
        While Not g_rs_TBOOK.EOF
            Set l_obj_Item = lsvBatchGen.ListItems.Add(, , CheckNull(g_rs_TBOOK.Fields("BOOKNO")))
                l_obj_Item.SubItems(1) = CheckNull(g_rs_TBOOK.Fields("SCHED_RDG_DT"))
                l_obj_Item.SubItems(2) = CheckNull(g_rs_TBOOK.Fields("CYCLE"))
            Set l_obj_Item = Nothing
            g_rs_TBOOK.MoveNext
        Wend
    End If
    
    bc = Mid(cmbBC.Text, 8, Len(cmbBC.Text) - 7)
    BCCDE = Trim(Left(cmbBC.Text, 4))
End Sub

Private Sub DTPDate_CloseUp()
    txtPDate.Text = Format(DTPDate.Month & "-" & DTPDate.Day & "-" & DTPDate.Year, "mm-dd-yyyy")
End Sub

Private Sub Form_Load()

If WindowState = vbNormal Then
    Me.Move (1000)
    Me.Top = 500
    Me.Height = 8520
    Me.Width = 6735
End If
    DTPDate.Value = Now
    load_BC
End Sub

Public Sub load_BC()
    
On Error GoTo errhandler
    With cmbBC
        .Clear
        '.AddItem ""
        If OpenRecordset(g_rs_RBUSCTR, "select * from R_BUSCTR order by BC_CODE", "frmDailyBatchGen", "Daily Batch Generation") Then
            While Not g_rs_RBUSCTR.EOF
                .AddItem g_rs_RBUSCTR.Fields(0) & " - " & g_rs_RBUSCTR.Fields(1)
                g_rs_RBUSCTR.MoveNext
            Wend
        Else
            .ListIndex = 0
        End If
    End With
    
errhandler:
If Err.Number <> 0 Then
    If Err.Number = 380 Then
    Else
        MsgBox Err.Description, , "System Message"
    End If
End If
End Sub

Private Sub cmdGenerate_Click()
Dim sMRU As String
Dim sRep(6) As String
Dim i As Integer
Dim sDir As String
Dim sFName As String
Dim inv(8) As String
Dim n As Integer
Dim BCDESC As String
Dim mrucnt As Integer
Dim fs, d, drivename

Dim Appl As New CRAXDRT.Application
Dim Report As New CRAXDRT.Report
Dim s As Integer

'On Error GoTo errhandler

' get the drive name
Set fs = CreateObject("Scripting.FileSystemObject")
Set d = fs.GetDrive(fs.GetDriveName(App.Path))
drivename = d.DriveLetter & ":"

' reports to be generated
sRep(0) = "OUT OF RANGE REPORT.rpt"
sRep(1) = "REPLACED METER REPORT.rpt"
sRep(2) = "FOUND CONNECTED REPORT.rpt"
sRep(3) = "OBSERVATION CODES REPORT.rpt"
sRep(4) = "DETAILED METER READING REPORT.rpt"
'sRep(5) = "FIELD FINDINGS SUMMARY REPORT.rpt"

    ' invalid characters for folder name
    inv(0) = "\"
    inv(1) = "/"
    inv(2) = ":"
    inv(3) = "*"
    inv(4) = "?"
    inv(5) = """"
    inv(6) = "<"
    inv(7) = ">"
    inv(8) = "|"
    ' check for invalid folder name
    For n = 0 To 8
        If InStr(1, bc, inv(n), vbTextCompare) > 0 Then
            ' if first character is invalid, folder cannot be created and will exit sub
            If InStr(1, bc, inv(n), vbTextCompare) = 1 Then
                MsgBox "A folder name cannot contain the character " & inv(n) & ". Please edit the business center name.", vbExclamation, "Invalid Folder Name"
                Exit Sub
            ' folder name to be created will be chars before first occurrence of invalid character
            Else
                MsgBox "A folder name cannot contain the character " & inv(n) & ". Reports will be saved in folder " & Mid(bc, 1, InStr(1, bc, inv(n), vbTextCompare) - 1) & ".", vbExclamation, "Invalid Folder Name"
                BCDESC = Trim(Mid(bc, 1, InStr(1, bc, inv(n), vbTextCompare) - 1))
                Exit For
            End If
        Else
            BCDESC = Trim(bc)
        End If
    Next n

' loop through all the mrus
For mrucnt = 1 To lsvBatchGen.ListItems.Count
    
    intCycle = lsvBatchGen.ListItems(mrucnt).SubItems(2)
    sMRU = lsvBatchGen.ListItems(mrucnt) 'lsvBatchGen.SelectedItem.Text
    ' loop through the six reports
    For i = 0 To 4
        Set Appn = CreateObject("CrystalRunTime.Application")
        Set cReport = Appn.OpenReport(App.Path & "\crystal reports\" & sRep(i)) 'Or get your report name into the report somehow
        cReport.Database.LogOnServer "p2ssql.dll", "SZ-MWSI-SUBIC", "MCFSDB", "sa", "sqladmin"
        For s = 1 To cReport.Database.Tables.Count
            cReport.Database.Tables(s).ConnectionProperties.Item("Password") = "sqladmin"
        Next s

        cReport.DiscardSavedData
    
        cReport.ParameterFields(1).AddCurrentValue g_Company
        cReport.ParameterFields(2).AddCurrentValue g_User
        cReport.ParameterFields(3).AddCurrentValue Left(g_BillMonth, 4) & Padl(intCycle, 2, "0") 'g_BillMonth
        cReport.ParameterFields(4).AddCurrentValue BCCDE
        cReport.ParameterFields(5).AddCurrentValue intCycle
        cReport.ParameterFields(6).AddCurrentValue sMRU
    
        'If (sRep(i) <> "FOUND CONNECTED REPORT.rpt") And (sRep(i) <> "FIELD FINDINGS SUMMARY REPORT.rpt") And (sRep(i) <> "OBSERVATION CODES REPORT.rpt") Then
            'cReport.ParameterFields(6).AddCurrentRange CDate(Month(Now) & "-" & "01" & "-" & Year(Now)), CDate(Format(Now, "mm-dd-yyyy")), crRangeIncludeLowerBound + crRangeIncludeUpperBound 'CDate(txtDateFrom.Text), CDate(txtDateTo.Text), crRangeIncludeLowerBound + crRangeIncludeUpperBound
            cReport.ParameterFields(7).AddCurrentRange CDate("01-01-1000"), CDate(Format(Now, "mm-dd-yyyy")), crRangeNoUpperBound
        'End If
        'cReport.Export
        
        ' /MWSIReports/BusinessCenter/YYYY/MM/DD
        sDir = drivename & "\MWSIReports\" & BCDESC & "\" & Year(PDate) & "\" & Mid(Format(PDate, "mm-dd-yyyy"), 1, 2) & "\" & Mid(Format(PDate, "mm-dd-yyyy"), 4, 2) & "\"
        If Len(dir(drivename & "\MWSIReports", vbDirectory)) = 0 Then MkDir (drivename & "\MWSIReports")
        If Len(dir(drivename & "\MWSIReports\" & BCDESC, vbDirectory)) = 0 Then MkDir (drivename & "\MWSIReports\" & BCDESC)
        If Len(dir(drivename & "\MWSIReports\" & BCDESC & "\" & Year(PDate), vbDirectory)) = 0 Then MkDir (drivename & "\MWSIReports\" & BCDESC & "\" & Year(PDate))
        If Len(dir(drivename & "\MWSIReports\" & BCDESC & "\" & Year(PDate) & "\" & Mid(Format(PDate, "mm-dd-yyyy"), 1, 2), vbDirectory)) = 0 Then MkDir (drivename & "\MWSIReports\" & BCDESC & "\" & Year(PDate) & "\" & Mid(Format(PDate, "mm-dd-yyyy"), 1, 2))
        If Len(dir(sDir, vbDirectory)) = 0 Then MkDir (sDir)
        
        Select Case sRep(i)
            Case "OUT OF RANGE REPORT.rpt"
                sFName = Format(PDate, "yyyymmdd") & "_" & sMRU & "_Out of Range.xls"
            Case "REPLACED METER REPORT.rpt"
                sFName = Format(PDate, "yyyymmdd") & "_" & sMRU & "_Replaced Meter.xls"
            Case "FOUND CONNECTED REPORT.rpt"
                sFName = Format(PDate, "yyyymmdd") & "_" & sMRU & "_Found Connected.xls"
            Case "OBSERVATION CODES REPORT.rpt"
                sFName = Format(PDate, "yyyymmdd") & "_" & sMRU & "_Field Findings.xls"
            Case "DETAILED METER READING REPORT.rpt"
                sFName = Format(PDate, "yyyymmdd") & "_" & sMRU & "_Detailed Accomplished.xls"
          '  Case "FIELD FINDINGS SUMMARY REPORT.rpt"
          '      sFName = Format(PDate, "yyyymmdd") & "_" & sMRU & "_Field Findings Summary.xls"
            Case Else
        End Select
    
    ' move to next report if no data
    varRec = cReport.GetNextRows(0, 1)
    RecBool = IsEmpty(varRec(0, 0))
        
    If RecBool = True Then GoTo nodatarep
    
        cReport.ExportOptions.DiskFileName = sDir & sFName
        cReport.ExportOptions.DestinationType = crEDTDiskFile
        cReport.ExportOptions.FormatType = crEFTExcelDataOnly
        cReport.ExportOptions.ExcelAreaType = crDetail
        cReport.ExportOptions.ExcelUseFormatInDataOnly = True
        cReport.ExportOptions.ExcelMaintainRelativeObjectPosition = True
        cReport.ExportOptions.ExcelMaintainColumnAlignment = True
        cReport.ExportOptions.ExcelChopPageHeader = False
        cReport.Export False
    
nodatarep:
    Next i

Next mrucnt

'FIELD FINDINGS SUMMARY SHOULD START HERE
sFName = ""
Dim Dateparam As String
Set Appn = CreateObject("CrystalRunTime.Application")
Set cReport = Appn.OpenReport(App.Path & "\crystal reports\" & "FIELD FINDINGS SUMMARY REPORT Daily Batch.rpt") 'Or get your report name into the report somehow
'sFName = Format(PDate, "yyyymmdd") & "_" & sMRU & "_Field Findings Summary.xls"
sFName = Format(PDate, "yyyymmdd") & "_Field Findings Summary.xls"
cReport.DiscardSavedData
cReport.ParameterFields(1).AddCurrentValue g_Company
cReport.ParameterFields(2).AddCurrentValue g_User
cReport.ParameterFields(3).AddCurrentValue Left(g_BillMonth, 4) & Padl(intCycle, 2, "0") 'g_BillMonth
cReport.ParameterFields(4).AddCurrentValue BCCDE
cReport.ParameterFields(5).AddCurrentValue intCycle
cReport.ParameterFields(6).AddCurrentValue "%"
'Dateparam = Padl(CStr(DTPDate.Month), 2, "0") & "/" & Padl(CStr(DTPDate.Day), 2, "0") & "/" & CStr(DTPDate.Year)
Dateparam = Padl(Month(PDate), 2, "0") & "/" & Padl(Day(PDate), 2, "0") & "/" & Year(PDate)
cReport.ParameterFields(7).AddCurrentValue Dateparam

cReport.ExportOptions.DiskFileName = sDir & sFName
cReport.ExportOptions.DestinationType = crEDTDiskFile
cReport.ExportOptions.FormatType = crEFTExcelDataOnly
cReport.ExportOptions.ExcelAreaType = crDetail
cReport.ExportOptions.ExcelUseFormatInDataOnly = True
cReport.ExportOptions.ExcelMaintainRelativeObjectPosition = True
cReport.ExportOptions.ExcelMaintainColumnAlignment = True
cReport.ExportOptions.ExcelChopPageHeader = False
cReport.Export False

MsgBox "The reports have been successfully saved.", , "Generation Successful"

errhandler:
If Err.Number <> 0 Then
    MsgBox Err.Description, vbExclamation, "Error Message"
End If
End Sub
