VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmGlobalCharges 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Global Charges"
   ClientHeight    =   6525
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9345
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6525
   ScaleWidth      =   9345
   Begin VB.CommandButton cmdGenGlobalCharges 
      Caption         =   "Generate Global Charges"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   7080
      TabIndex        =   16
      Top             =   5640
      Width           =   1935
   End
   Begin VB.CommandButton cmdDeleteGlobalCharges 
      Caption         =   "Delete Global Charges Selected"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4800
      TabIndex        =   5
      Top             =   5640
      Width           =   1935
   End
   Begin VB.CommandButton cmdEditGlobalCharges 
      Caption         =   "Edit Selected Global Charges"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   2520
      TabIndex        =   4
      Top             =   5640
      Width           =   1935
   End
   Begin VB.Frame frameGlobalChargesOptions 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5415
      Left            =   9480
      TabIndex        =   3
      Top             =   120
      Width           =   4575
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   2760
         TabIndex        =   15
         Top             =   3240
         Width           =   1575
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "OK"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   240
         TabIndex        =   14
         Top             =   3240
         Width           =   1575
      End
      Begin VB.TextBox txtratemult 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   13
         Top             =   2400
         Width           =   2775
      End
      Begin VB.TextBox txtFileOrder 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   12
         Top             =   1920
         Width           =   2775
      End
      Begin VB.TextBox txtChargeDesc 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   11
         Top             =   1440
         Width           =   4335
      End
      Begin VB.TextBox txtChargeCode 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1560
         MaxLength       =   4
         TabIndex        =   10
         Top             =   600
         Width           =   2895
      End
      Begin VB.Label lblratemult 
         Caption         =   "Rate Mult:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   2520
         Width           =   1335
      End
      Begin VB.Label lblfileorder 
         Caption         =   "File Order:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   2040
         Width           =   1095
      End
      Begin VB.Label lblchargedesc 
         Caption         =   "Charge Description:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1080
         Width           =   2175
      End
      Begin VB.Label lblChargeCode 
         Caption         =   "Charge Code:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   720
         Width           =   1575
      End
   End
   Begin VB.CommandButton cmdAddGlobalCharge 
      Caption         =   "Add New Global Charges"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   2
      Top             =   5640
      Width           =   1935
   End
   Begin VB.Frame frameGlobal 
      Caption         =   "Global Charges"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5415
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   9015
      Begin MSComctlLib.ListView lsvGlobalCharges 
         Height          =   4935
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Width           =   8775
         _ExtentX        =   15478
         _ExtentY        =   8705
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Charge Code"
            Object.Width           =   2822
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Charge Description"
            Object.Width           =   6174
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "File Order"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Rate Mult"
            Object.Width           =   2540
         EndProperty
      End
   End
End
Attribute VB_Name = "frmGlobalCharges"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim RS_GlobalCharges As ADODB.Recordset
Dim iAddEditDelete As Integer

'get values for all
Dim stxtChargeCode As String
Dim stxtChargeDesc As String
Dim stxtFileOrder As String
Dim stxtRateMult As String
Dim checkFileOrder As String

Private Sub ListView_Load(Optional ByVal Mode As Integer = 0)
Dim l_str_Sql As String, l_str_Where As String
    
l_str_Sql = "select * from R_GLOBAL_CHARGES"
    
lsvGlobalCharges.ListItems.Clear
    If Not OpenRecordset(RS_GlobalCharges, l_str_Sql, Me.Name, "ListView_Load") Then
        If Mode = 0 Then MsgBox "No Global Charges Found!", vbExclamation, "No Global Charges Found!"
        Exit Sub
    End If
    
RS_GlobalCharges.MoveFirst

While Not RS_GlobalCharges.EOF
    ListView_Set 4
    RS_GlobalCharges.MoveNext
Wend
    
RS_GlobalCharges.Close
Set RS_GlobalCharges = Nothing
    
lsvGlobalCharges.SortKey = 0    'sort by tariff type from old 2
lsvGlobalCharges.Sorted = True  'sort it
        
End Sub

Private Sub cmdAddGlobalCharge_Click()
    txtChargeCode.Text = ""
    txtChargeDesc.Text = ""
    txtFileOrder.Text = ""
    txtratemult.Text = ""
    frmGlobalCharges.Width = 14340
    iAddEditDelete = 1
End Sub

Private Sub cmdClose_Click()
    frmGlobalCharges.Width = 9420
End Sub

Private Sub ListView_Set(intCols As Integer, Optional intColDate As Integer, Optional intColTime As Integer)
    Dim lobjItem As Object
        
    Set lobjItem = lsvGlobalCharges.ListItems.Add(, , CheckNull(RS_GlobalCharges.Fields(0)))
        lobjItem.SubItems(1) = CheckNull(RS_GlobalCharges.Fields(1))
        lobjItem.SubItems(2) = CheckNull(RS_GlobalCharges.Fields(2))
        lobjItem.SubItems(3) = CheckNull(RS_GlobalCharges.Fields(3))
    Set lobjItem = Nothing
End Sub

Private Sub cmdDeleteGlobalCharges_Click()
Dim listcounter As Integer
Dim intResponse As Integer

For listcounter = 1 To lsvGlobalCharges.SelectedItem.Index
    txtChargeCode.Text = CheckNull(lsvGlobalCharges.ListItems(listcounter).Text)
    txtChargeDesc.Text = CheckNull(lsvGlobalCharges.ListItems(listcounter).ListSubItems(1).Text)
    txtFileOrder.Text = CheckNull(lsvGlobalCharges.ListItems(listcounter).ListSubItems(2).Text)
    txtratemult.Text = CheckNull(lsvGlobalCharges.ListItems(listcounter).ListSubItems(3).Text)
Next

intResponse = MsgBox("Confirm Deletion of Selected Global Charge?", vbYesNo + vbQuestion, "Quit")
    If intResponse = vbYes Then
        If SelectionGlobal(3) = True Then
            MsgBox "Deleted Selected Global Charges"
            ListView_Load 1
        End If
    Else
        MsgBox "No Global Charges deleted"
    End If
End Sub

Private Sub cmdEditGlobalCharges_Click()
Dim listcounter As Integer
    
frmGlobalCharges.Width = 14340

For listcounter = 1 To lsvGlobalCharges.SelectedItem.Index
    txtChargeCode.Text = CheckNull(lsvGlobalCharges.ListItems(listcounter).Text)
    txtChargeDesc.Text = CheckNull(lsvGlobalCharges.ListItems(listcounter).ListSubItems(1).Text)
    txtFileOrder.Text = CheckNull(lsvGlobalCharges.ListItems(listcounter).ListSubItems(2).Text)
    txtratemult.Text = CheckNull(lsvGlobalCharges.ListItems(listcounter).ListSubItems(3).Text)
    checkFileOrder = txtFileOrder.Text
Next
    iAddEditDelete = 2
    
End Sub

Private Sub cmdGenGlobalCharges_Click()
Dim xlApp As Excel.Application
Dim xlWb As Excel.Workbook
Dim sPath As String 'directory is located at users app.path
Dim filenme As String
Dim listcounter As Integer
Dim X As Integer
Dim fname As String
Dim iselectG As String

Dim f
Dim dir As String

Dim CHARGECODE As String
Dim CHARGEDESC As String
Dim FILEORDER As String
Dim RATEMMULT As String
Dim Y As Integer

Set f = CreateObject("Scripting.FileSystemObject")

sPath = App.Path

On Error GoTo errhandler

If sPath <> "" Then
  If Not f.FolderExists(sPath & "\Global Charges\") Then f.CreateFolder (sPath & "\Global Charges\")
  If Not f.FolderExists(sPath & "\Global Charges\" & "For DS\") Then f.CreateFolder (sPath & "\Global Charges\" & "For DS\")
  dir = sPath & "\Global Charges\" & "For DS\" & Format(Date, "yyyy-mm-dd") & "\"
  If Not f.FolderExists(dir) Then f.CreateFolder (dir)
Else
    dir = ""
End If

Set xlApp = New Excel.Application
Set xlWb = xlApp.Workbooks.Add
xlApp.Visible = False

X = 1
Y = 1

For listcounter = 1 To lsvGlobalCharges.ListItems.Count
    xlWb.Sheets("Sheet1").Cells(X, 1).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X, 1).Value = lsvGlobalCharges.ListItems(listcounter).Text
    xlWb.Sheets("Sheet1").Cells(X, 2).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X, 2).Value = lsvGlobalCharges.ListItems(listcounter).ListSubItems(1).Text
    xlWb.Sheets("Sheet1").Cells(X, 3).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X, 3).Value = lsvGlobalCharges.ListItems(listcounter).ListSubItems(2).Text
    xlWb.Sheets("Sheet1").Cells(X, 4).NumberFormat = "@"
    xlWb.Sheets("Sheet1").Cells(X, 4).Value = lsvGlobalCharges.ListItems(listcounter).ListSubItems(3).Text
    X = X + Y
Next
    
fname = "GLOBALCHARGES.xls"
xlWb.Close True, dir & fname
Set xlApp = Nothing
MsgBox "Global Charges File Generated"

errhandler:

    If Err.Number <> 0 Then                 'Added due to Error 1004
    xlWb.Close False
    xlApp.Quit
    Set xlApp = Nothing
    End If

End Sub

Private Sub cmdOk_Click()
Dim intResponse As Integer

If ValueChecker(txtFileOrder.Text, txtratemult.Text) = True Then
    Select Case iAddEditDelete
    Case 1
        If SelectionGlobal(1) = True Then
            MsgBox "Add New Global Charges"
            ListView_Load 1
            intResponse = MsgBox("Do you want to add another Charges:", vbYesNo + vbQuestion, "Quit")
            If intResponse = vbYes Then
                txtChargeCode.Text = ""
                txtChargeDesc.Text = ""
                txtFileOrder.Text = ""
                txtratemult.Text = ""
                ListView_Load 1
            Else
                frmGlobalCharges.Width = 9420
                ListView_Load 1
            End If
        Else
        frmGlobalCharges.Width = 9420
        ListView_Load 1
        End If
        
    Case 2
        If SelectionGlobal(2) = True Then
            MsgBox "Updated Selected Global Charges"
            frmGlobalCharges.Width = 9420
            ListView_Load 1
        End If
    End Select
Else
    MsgBox "Unable to add new Global Charges"
End If

End Sub

Private Sub Form_Load()
    ListView_Load 1
End Sub

Private Sub lsvGlobalCharges_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
' Sort the list
    With lsvGlobalCharges
        .SortKey = ColumnHeader.Index - 1   'get the key using the column number
        If .SortOrder = lvwAscending Then   'reverse sort order from current setting
            .SortOrder = lvwDescending
        Else
            .SortOrder = lvwAscending
        End If
        .Sorted = True  'do the sort
    End With
End Sub

Public Function SelectionGlobal(AddEditChoices As Integer) As Boolean

Dim SQLQuery As String
Dim SQLQueryDelete As String

On Error GoTo errhandler

Select Case AddEditChoices
    Case 1
        SQLQuery = "Insert into R_GLOBAL_CHARGES (CHARGECODE, CHARGEDESC, FILE_ORDER, RATE_MULT) values " & _
        "('" & stxtChargeCode & "','" & stxtChargeDesc & "','" & stxtFileOrder & "','" & stxtRateMult & "')"
        DBExecute SQLQuery, "", ""
        SelectionGlobal = True
        
    Case 2
        SQLQueryDelete = "Delete From R_GLOBAL_CHARGES where CHARGECODE = '" & stxtChargeCode & "' and FILE_ORDER = '" & stxtFileOrder & "'"
        DBExecute SQLQueryDelete, "", ""
        
        SQLQuery = "Insert into R_GLOBAL_CHARGES (CHARGECODE, CHARGEDESC, FILE_ORDER, RATE_MULT) values " & _
        "('" & stxtChargeCode & "','" & stxtChargeDesc & "','" & stxtFileOrder & "','" & stxtRateMult & "')"
        DBExecute SQLQuery, "", ""
        SelectionGlobal = True
        
    Case 3
        SQLQueryDelete = "Delete From R_GLOBAL_CHARGES where CHARGECODE = '" & stxtChargeCode & "' and FILE_ORDER = '" & stxtFileOrder & "'"
        DBExecute SQLQueryDelete, "", ""
        SelectionGlobal = True
    End Select
    
errhandler:
If Err.Number <> 0 Then
    If Err.Number = 380 Then
    Else
        MsgBox Err.Description, , "System Message"
    End If
End If
    
End Function

Public Function ValueChecker(chkFileOrder As String, chkRatemult As String) As Boolean
Dim sqlcheckvalue As String
Dim rs_checkvalues As Recordset
Dim scheckvalue As String
Dim counterError As Integer

counterError = 0

If IsNumeric(chkFileOrder) = False Then
    MsgBox "Invalid File Order"
    counterError = counterError + 1
Else 'add button
    If IsNumeric(chkFileOrder) = True And iAddEditDelete = 1 Then
        sqlcheckvalue = "select file_order from dbo.R_GLOBAL_CHARGES where file_order = '" & chkFileOrder & "'"
        If OpenRecordset(rs_checkvalues, sqlcheckvalue, Me.Name, "ListView_Load") Then
            MsgBox "File Order already exsist"
            counterError = counterError + 1
        End If
    Else 'edit button
        If IsNumeric(chkFileOrder) = True And iAddEditDelete = 2 Then
            sqlcheckvalue = "select file_order from dbo.R_GLOBAL_CHARGES where file_order = '" & chkFileOrder & "'"
            If OpenRecordset(rs_checkvalues, sqlcheckvalue, Me.Name, "ListView_Load") Then
            scheckvalue = rs_checkvalues.Fields(0)
                If checkFileOrder <> scheckvalue Then
                    MsgBox "Another Charge Code with file order already exsist"
                    counterError = counterError + 1
                End If
            End If
        End If
    End If
End If

If IsNumeric(chkRatemult) = False Then
    MsgBox "Invalid Rate"
    counterError = counterError + 1
End If

If counterError = 0 Then
    ValueChecker = True
Else
    ValueChecker = False
End If

End Function

Private Sub txtChargeCode_Change()
    stxtChargeCode = txtChargeCode.Text
End Sub

Private Sub txtChargeDesc_Change()
    stxtChargeDesc = txtChargeDesc.Text
End Sub

Private Sub txtFileOrder_Change()
    stxtFileOrder = txtFileOrder.Text
End Sub

Private Sub txtratemult_Change()
    stxtRateMult = txtratemult.Text
End Sub
