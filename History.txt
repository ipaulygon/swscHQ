Aug 16 - Changed Discon Download to select distint accounts
Aug 17 - Updated Upload to Host Stored Procedure for TBASICHRG computation to NEW SP FOR 34 Days
Aug 18 - Changed Field Name in Download Schema from CustRate to RateCat
Aug 20 - Modified entire HQ application to replicate transactions to MCFSDBBACKUP
Aug 23 - Changed Filename for Recon to parameter in S_PARAM
Aug 24 - Added MCFSDBBACKUP in INI FILE as PARAMETER
Aug 31 - Added ReconDate and REcon_Rdg in download file. Removed checking of Business Center in creation of new schedule.
Sept 1 - Removed BC_CODE checking in download batch to host for MRU with different BC codes
Sept 3 - Edited get_bc_code function in loadbcpayments to include top 1 bc_code found in t_book for RC_Download Pay File
Sept 3 - Changed 2nd Discount Label to ZDISPA as Requested by maynilad in upload file creation.
Sept 3 - Modified UploadtoDCDB to get BC from T_DC_DOWNLOAD instead from the filename.
Sept 3 - Modified creation of RCDownload - Get RCJoibId - removed other joins to tcustmaster and tdcupload
Sept 8 - Modified Upload of Recon File to insert records with valid RCJoibId
Sept 9 - Adjusted Get BC Code and Get Bookno Function in LoadBCPayments to consider data in T_RC_Download if account is missing in T_DOWNLOAD
Sept 14 - Added Senior Citizen to Upload File generation and new business rules to PrevRdg Date and Prev Rdg in Download File.
Sept 14 - Adjusted billperiod for greater than 34 days to use Scheduled Reading Date in T_BOOK instead of reading date in t_upload.
Sept 28 - Updated Consumption * noMonths for 34 days to match total in cera
Oct 6 - Added PadL to MRU for Disconnection Download File

