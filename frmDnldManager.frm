VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmDnldManager 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Download Manager"
   ClientHeight    =   4440
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10410
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4440
   ScaleWidth      =   10410
   Begin VB.CommandButton cmdGenerate 
      Caption         =   "&Generate DL for RAP"
      Height          =   495
      Left            =   8160
      TabIndex        =   23
      Top             =   3840
      Width           =   2055
   End
   Begin MSComCtl2.MonthView MonthView1 
      Height          =   2370
      Left            =   120
      TabIndex        =   22
      Top             =   1320
      Width           =   2700
      _ExtentX        =   4763
      _ExtentY        =   4180
      _Version        =   393216
      ForeColor       =   -2147483630
      BackColor       =   -2147483633
      Appearance      =   1
      StartOfWeek     =   59965441
      CurrentDate     =   40506
   End
   Begin VB.TextBox txtval 
      Height          =   2415
      Left            =   7920
      MultiLine       =   -1  'True
      TabIndex        =   20
      Top             =   1320
      Width           =   2295
   End
   Begin VB.ListBox lstDown 
      Height          =   2400
      ItemData        =   "frmDnldManager.frx":0000
      Left            =   5400
      List            =   "frmDnldManager.frx":0002
      TabIndex        =   18
      Top             =   1320
      Width           =   2415
   End
   Begin VB.ListBox lstVal 
      Height          =   2400
      ItemData        =   "frmDnldManager.frx":0004
      Left            =   2880
      List            =   "frmDnldManager.frx":0006
      TabIndex        =   17
      Top             =   1320
      Width           =   2415
   End
   Begin VB.Frame Frame3 
      Height          =   855
      Left            =   120
      TabIndex        =   6
      Top             =   0
      Width           =   10215
      Begin VB.ComboBox cmbDownloadType 
         Height          =   315
         ItemData        =   "frmDnldManager.frx":0008
         Left            =   120
         List            =   "frmDnldManager.frx":000A
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   480
         Width           =   2415
      End
      Begin VB.Label lblDatabase 
         Alignment       =   2  'Center
         BackColor       =   &H80000009&
         Caption         =   "MCFSDB"
         Height          =   255
         Left            =   7800
         TabIndex        =   14
         Top             =   480
         Width           =   2295
      End
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         Caption         =   "DATABASE"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   7920
         TabIndex        =   13
         Top             =   240
         Width           =   2055
      End
      Begin VB.Label lblProdTable 
         Alignment       =   2  'Center
         BackColor       =   &H80000009&
         Height          =   255
         Left            =   5160
         TabIndex        =   12
         Top             =   480
         Width           =   2415
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   "PROD TABLE DESTINATION"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5160
         TabIndex        =   11
         Top             =   240
         Width           =   2535
      End
      Begin VB.Label lblDumpTable 
         Alignment       =   2  'Center
         BackColor       =   &H80000009&
         Height          =   255
         Left            =   2640
         TabIndex        =   10
         Top             =   480
         Width           =   2295
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "DUMP TABLE DESTINATION"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2520
         TabIndex        =   9
         Top             =   240
         Width           =   2535
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "DOWNLOAD  TYPE"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   2295
      End
   End
   Begin VB.Frame Frame1 
      Height          =   6015
      Left            =   120
      TabIndex        =   2
      Top             =   9120
      Width           =   2655
      Begin VB.CommandButton cmbSelectAll1 
         Caption         =   "&Select All"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   5640
         Width           =   2415
      End
      Begin VB.DriveListBox Drive1 
         Enabled         =   0   'False
         Height          =   315
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   2415
      End
      Begin VB.DirListBox Dir1 
         Enabled         =   0   'False
         Height          =   1665
         Left            =   120
         TabIndex        =   4
         Top             =   600
         Width           =   2415
      End
      Begin VB.FileListBox File1 
         Height          =   3210
         Left            =   120
         MultiSelect     =   2  'Extended
         TabIndex        =   3
         Top             =   2280
         Width           =   2415
      End
   End
   Begin VB.CommandButton cmdValidate 
      Caption         =   "&Validate"
      Height          =   495
      Left            =   3000
      TabIndex        =   1
      Top             =   3840
      Width           =   2415
   End
   Begin VB.CommandButton cmdDownload 
      Caption         =   "&Download"
      Height          =   495
      Left            =   5520
      TabIndex        =   0
      Top             =   3840
      Width           =   2415
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Caption         =   "STATUS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   8160
      TabIndex        =   21
      Top             =   960
      Width           =   1935
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "DOWNLOAD DATE"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   480
      TabIndex        =   19
      Top             =   960
      Width           =   1935
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      Caption         =   "FILE SOURCE VIEW"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4560
      TabIndex        =   16
      Top             =   960
      Width           =   1935
   End
End
Attribute VB_Name = "frmDnldManager"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Downloadtype As String
Dim rsProcess As ADODB.Recordset
Dim rsCombo As ADODB.Recordset
Dim FolderSource As String
Dim FolderDest As String
Dim fileCount As Integer
Dim filenme As String
Dim fileDir As String
Dim rowCount As Integer
Dim procDate As Date
Dim fileSel As String







Private Sub cmbDownloadType_Click()
Downloadtype = cmbDownloadType.Text
If Downloadtype = "Disconnection" Then
cmdGenerate.Visible = True
cmdGenerate.Enabled = False
End If
If Downloadtype <> "Disconnection" Then cmdGenerate.Visible = False

lblDumpTable.Caption = ""
lblProdTable.Caption = ""
lstVal.Clear
InitForm (Downloadtype)
GenDownList MonthView1.Value, Downloadtype
End Sub

Private Sub cmbSelectAll1_Click()
Dim i As Integer
For i = 0 To fileCount - 1
File1.Selected(i) = True
Next
End Sub

Private Sub cmdDownload_Click()
Dim sqlString As String
Dim files As String
Dim X As Integer

For X = 0 To lstDown.ListCount - 1
files = lstDown.List(X)
sqlString = "EXEC sp_INSERT_DISCDOWNLOAD '" & files & "'"
DBExecute sqlString, "", ""
txtval.Text = txtval.Text & vbCrLf & files & " - Download Successful!"
Next X

txtval.Text = txtval.Text & vbCrLf & lstDown.ListCount & ": Files Downloaded"
End Sub

Private Sub cmdGenerate_Click()
Dim sqlRAP As String
Dim disString As String
Dim rsRap As ADODB.Recordset
Dim rsPath As ADODB.Recordset
Dim Dfile As String
Dim cdelim As String
Dim pathName As String
Dim BCCode As String

Set rsRap = New ADODB.Recordset
Set rsPath = New ADODB.Recordset

If OpenRecordset(rsPath, "SELECT FOLDERDEST FROM R_PROCESS WHERE PROCESSDET = 'Disconnection - Rap'", "", "") = True Then
pathName = rsPath.Fields("FOLDERDEST")
End If

cdelim = "|"
sqlRAP = "SELECT BOOKNO,ACCTNUM,SEQ,SERIALNO,CUSTNAME,ADDRESS,RECONFEE,PUA,CURRENT_AMT,DUE_AMT,'1' as 'TYPE' FROM T_DISCDOWNLOAD_RAW WHERE FILESID = " & fileSel
Dfile = pathName & "\" & "DPsample.txt"
'& Left(sBusCenter, 2) & Year(Date) & Format(Date, "mm") & Format(Date, "dd") & ".txt"

Open Dfile For Output As #1

If OpenRecordset(rsRap, sqlRAP, "", "") = True Then
rsRap.MoveFirst
Do While Not rsRap.EOF
    'disString = rsRap.Fields("BOOKNO") & cdelim & rsRap.Fields("ACCTNUM") & cdelim & rsRap.Fields("SEQ") & cdelim & rsRap.Fields("SERIALNO") & cdelim & rsRap.Fields("CUSTNAME") & cdelim & rsRap.Fields("ADDRESS") & cdelim & rsRap.Fields("RECONFEE") & cdelim & rsRap.Fields("PUA") & cdelim & rsRap.Fields("CURRENT_AMT") & cdelim & rsRap.Fields("DUE_AMT") & cdelim & "1" & cdelim & cdelim
    disString = rsRap.Fields("BOOKNO") & cdelim & rsRap.Fields("ACCTNUM") & cdelim & rsRap.Fields("SEQ") & cdelim & rsRap.Fields("SERIALNO") & cdelim & rsRap.Fields("CUSTNAME") & cdelim & rsRap.Fields("ADDRESS") & cdelim & g_ReOpeningFee & cdelim & rsRap.Fields("PUA") & cdelim & rsRap.Fields("CURRENT_AMT") & cdelim & rsRap.Fields("DUE_AMT") & cdelim & "1" & cdelim & cdelim
    
    Print #1, disString
    rsRap.MoveNext
Loop
End If

sqlRAP = "SELECT FILENAME FROM T_FILES WHERE FILENAMEID = " & fileSel
If OpenRecordset(rsRap, sqlRAP, "", "") = True Then
BCCode = Mid(rsRap.Fields("FILENAME"), 4, 4)
End If

'Print #1, disString

            
Close #1

    Name Dfile As pathName & "\" & "DP" & BCCode & Year(Now()) & Format(Now(), "mm") & Format(Now(), "dd") & "00" & ".txt"
    ' Rename the filename
   ' DfileFull = DfileFull & Padl(batchid, 2, "0") & ".txt"
   ' DFile2 = DFile1 & Padl(batchid, 2, "0") & ".txt"

MsgBox "File created!"
cmdGenerate.Enabled = False

End Sub

Private Sub cmdValidate_Click()
Dim fileCount As Integer
Dim X As Integer
Dim sqlDeleteRaw As String

sqlDeleteRaw = "DELETE FROM T_DISCDOWNLOAD_RAW"
DBExecute sqlDeleteRaw, "", ""

fileCount = File1.ListCount
For X = 0 To fileCount - 1
    filenme = File1.List(X)
    fileDir = File1.Path
    If ValidateDiscon(filenme, fileDir, Downloadtype, FolderDest, MonthView1.Month & "/" & MonthView1.Day & "/" & MonthView1.Year) = True Then
    txtval.Text = txtval.Text & vbCrLf & filenme & " - Validation Successful!"
    lstDown.AddItem filenme
    lstVal.RemoveItem (0)
    
    End If
Next X

txtval.Text = txtval.Text & vbCrLf & fileCount & ": Files Validated!"

End Sub

Private Sub File1_PathChange()
fileCount = File1.ListCount



End Sub


Private Sub Form_Activate()
Dim SQLcmbDownloadType As String

Set rsProcess = New ADODB.Recordset
Set rsCombo = New ADODB.Recordset

Me.Width = 10500
Me.Height = 4920
lblDatabase.Caption = g_Database & "/" & g_DBBackup
cmbSelectAll1.Enabled = False
MonthView1.Value = Now
procDate = MonthView1.Value
SQLcmbDownloadType = "SELECT PROCESSDET FROM dbo.R_PROCESS WHERE PROCESSFLAG = 1"

rsCombo.Open SQLcmbDownloadType, g_Conn, adOpenStatic, adLockReadOnly
rsCombo.MoveFirst
Do While Not rsCombo.EOF
    cmbDownloadType.AddItem rsCombo.Fields("PROCESSDET").Value
rsCombo.MoveNext
Loop
cmdGenerate.Visible = False

End Sub

Public Sub InitForm(processType As String)
Dim InitSQL As String
Dim X As Integer

InitSQL = "SELECT DUMPTABLE,PRODTABLE,FOLDERSOURCE,FOLDERDEST FROM dbo.R_PROCESS WHERE PROCESS = 'Download' AND PROCESSDET = '" & processType & "'"

If OpenRecordset(rsProcess, InitSQL, "", "") Then
    lblDumpTable.Caption = rsProcess.Fields("DUMPTABLE").Value
    lblProdTable.Caption = rsProcess.Fields("PRODTABLE").Value
    FolderSource = rsProcess.Fields("FOLDERSOURCE").Value
    FolderDest = rsProcess.Fields("FOLDERDEST").Value
    File1.Path = FolderSource
    Dir1.Path = FolderSource
    Drive1.Drive = Mid(FolderSource, 1, 1) & ":" & "/"
End If


For X = 0 To File1.ListCount - 1
    lstVal.AddItem File1.List(X)
Next X

End Sub


Private Sub GenDownList(processDate As Date, processType As String)

Dim sqlDownList As String
Dim rsDownList As ADODB.Recordset

Set rsDownList = New ADODB.Recordset
lstDown.Clear
txtval.Text = ""
sqlDownList = "EXEC sp_GET_DLVIEW_FDEST '" & processDate & "','" & processType & "'"

If OpenRecordset(rsDownList, sqlDownList, "", "") = True Then
lstDown.AddItem rsDownList.Fields("FILENAME")
End If

Set rsDownList = Nothing
End Sub

Private Sub lstDown_Click()
Dim sqlDLinfo As String
Dim rsDLinfo As ADODB.Recordset
txtval.Text = ""
Set rsDLinfo = New ADODB.Recordset
Dim sqlstring1 As String

If OpenRecordset(rsDLinfo, "SELECT TOP 1 * FROM T_FILES WHERE FILENAME = '" & lstDown.Text & "' ORDER BY FILENAMEID DESC", "", "") = True Then
fileSel = rsDLinfo.Fields("FILENAMEID")
txtval.Text = txtval.Text & "DOWNLOADED" & vbCrLf & "Download Date: " & rsDLinfo.Fields("PROCESSDATE") & vbCrLf & "Number of records: " & rsDLinfo.Fields("LINECOUNT")
End If
Set rsDLinfo = Nothing
cmdGenerate.Enabled = True
End Sub





Private Sub MonthView1_DateClick(ByVal DateClicked As Date)
GenDownList MonthView1.Value, Downloadtype
End Sub
