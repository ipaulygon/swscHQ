VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmVerified 
   Caption         =   "Verified Readings"
   ClientHeight    =   6855
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11115
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6855
   ScaleWidth      =   11115
   Begin VB.Frame Frame1 
      Caption         =   "VALIDATED RDG FILE SOURCE"
      Height          =   6735
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11055
      Begin MSComctlLib.ListView lsvdbfsource 
         Height          =   2805
         Left            =   240
         TabIndex        =   6
         Top             =   3720
         Width           =   10605
         _ExtentX        =   18706
         _ExtentY        =   4948
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Records Verified"
            Object.Width           =   10583
         EndProperty
      End
      Begin VB.FileListBox File1 
         Height          =   2625
         Left            =   2880
         Pattern         =   "*.xls"
         TabIndex        =   5
         Top             =   240
         Width           =   8055
      End
      Begin VB.DriveListBox drvSource 
         Height          =   315
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Width           =   2595
      End
      Begin VB.DirListBox dirSource 
         Height          =   2340
         Left            =   240
         TabIndex        =   3
         Top             =   600
         Width           =   2595
      End
      Begin VB.TextBox txtdbfsource 
         Height          =   735
         Left            =   2640
         TabIndex        =   2
         Top             =   3840
         Width           =   2655
      End
      Begin VB.CommandButton cmdDownload 
         Caption         =   "&Download"
         Height          =   375
         Left            =   2880
         TabIndex        =   1
         Top             =   3120
         Width           =   5175
      End
   End
End
Attribute VB_Name = "frmVerified"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim fileName As String
Dim verMonth As String
Dim verYear As String
Dim driveSource As String

Private Sub cmdDownload_Click()
Dim xlApp As Excel.Application
Dim xlSht As Excel.Worksheet
Dim xlRng As Excel.Range

Dim SEQNO As Integer
Dim ACCTNUM As String
Dim METERNO As String
Dim CUSTNAME As String
Dim ADDRESS As String
Dim PRESRDG As String
Dim NEWSEQNO As Integer
Dim OCREMARKS As String
Dim REMARKS As String
Dim VERRDG As String
Dim VEROC As String
Dim VERREMARKS As String
Dim RDTAG As String
Dim SQLstring As String
Dim sourcePath As String
Dim targetPath As String
Dim partPath As String
Dim yearPath As String
Dim dnld_dest
Dim X As Integer
Dim Y As Integer
Dim rowCount As Integer
Dim recCount As Integer
Dim f As Object, fso As Object
Dim rsCheck As ADODB.Recordset
Dim rsSQL1 As ADODB.Recordset
Dim sqlCheck As String
Dim sql1 As String
Dim Status As String
Dim ans As Long
Dim rejacct(5000) As String
Dim rejacctcnt As Integer
Dim lstcnt As Integer
Dim lstxt As String
Dim sqldir As String
Dim rejacctsec(5000) As String
Dim rejacctcntsec As Integer
Dim rsSQLsec As ADODB.Recordset
Dim sqlsec As String
Dim rejacctnoterr(5000) As String
Dim rejacctcntnoterr As Integer
Dim sqlnoterr As String
Dim rsSQLnoterr As ADODB.Recordset
Dim rejacctnull(5000) As String
Dim rejacctcntnull As Integer
Dim rsSQLnull As ADODB.Recordset
Dim sqlnull As String
Dim lstxtsec As String
Dim lstxtnull As String
Dim lstxtnoterr As String
Dim lstcntnull As Integer
Dim lstcntnoterr As Integer
Dim lstcntsec As Integer
Dim rejacctinv(5000) As String
Dim rejacctcntinv As Integer
Dim sqlinv As String
Dim rsSQLinv As ADODB.Recordset
Dim lstxtinv As String
Dim lstcntinv As Integer
Dim rejacctdup(5000) As String
Dim rejacctcntdup As Integer
Dim rsSQLdup As ADODB.Recordset
Dim sqldup As String
Dim lstxtdup As String
Dim lstcntdup As Integer
Dim rs As Recordset
Dim docno1, docno2, mtrno1, mtrno2, rdg1, rdg2, oc1, oc2, rem1, rem2 As String
Dim ansdup As Long
Dim docno, mtrno, rdg, oc, rmrk As String

Set fso = CreateObject("Scripting.FileSystemObject")
txtdbfsource.Text = ""

'For y = 0 To File1.ListCount - 1
Set xlApp = New Excel.Application

sqldir = "UPDATE dbo.T_VALSETTINGS SET FileSource = '" & dirSource.Path & "'"
DBExecute sqldir, "", ""

If fileName <> "" Then
    verMonth = Mid(File1.fileName, 5, 2)
    verYear = Mid(File1.fileName, 1, 4)
    
'    recCount = 0
'    rejacctcnt = 0
'    rejacctcntnoterr = 0
'    rejacctcntsec = 0
'    sourcePath = dirSource.Path & "\" & fileName
'    targetPath = driveSource & "\VALIDATIONS\" & verYear & "\" & verMonth & "\" & Day(Date)
'    partPath = driveSource & "\VALIDATIONS\" & verYear & "\" & verMonth
'    'check if folder exists
'    'If fso.FolderExists(driveSource) = False Then
'    'fso.CreateFolder (driveSource)
'    'End If
'
'    If fso.FolderExists(driveSource & "\VALIDATIONS") = False Then
'    fso.CreateFolder (driveSource & "\VALIDATIONS")
'    End If
'
'
'    If fso.FolderExists(driveSource & "\VALIDATIONS\2008") = False Then
'    fso.CreateFolder (driveSource & "\VALIDATIONS\2008")
'    End If
'
'    If fso.FolderExists(driveSource & "\VALIDATIONS\2009") = False Then
'    fso.CreateFolder (driveSource & "\VALIDATIONS\2009")
'    End If
'
'    If fso.FolderExists(partPath) = False Then
'    fso.CreateFolder (partPath)
'    End If
    
    recCount = 0
    rejacctcnt = 0
    rejacctcntnoterr = 0
    rejacctcntsec = 0
    sourcePath = dirSource.Path & "\" & fileName
    targetPath = driveSource & "\" & verYear & "\" & verMonth & "\" & Day(Date)
    yearPath = driveSource & "\" & verYear
    partPath = driveSource & "\" & verYear & "\" & verMonth
    'check if folder exists
    'If fso.FolderExists(driveSource) = False Then
    'fso.CreateFolder (driveSource)
    'End If
    
'    If fso.FolderExists(driveSource & "\VALIDATIONS") = False Then
'    fso.CreateFolder (driveSource & "\VALIDATIONS")
'    End If
    
    
'    If fso.FolderExists(driveSource & "\VALIDATIONS\2008") = False Then
'    fso.CreateFolder (driveSource & "\VALIDATIONS\2008")
'    End If
'
'    If fso.FolderExists(driveSource & "\VALIDATIONS\2009") = False Then
'    fso.CreateFolder (driveSource & "\VALIDATIONS\2009")
'    End If
    
    If fso.FolderExists(yearPath) = False Then
    fso.CreateFolder (yearPath)
    End If
    
    If fso.FolderExists(partPath) = False Then
    fso.CreateFolder (partPath)
    End If
    
    '--OPEN EXCEL FILE---
    xlApp.DisplayAlerts = False
    xlApp.Workbooks.Open (sourcePath)
    'Find the limit for row count
    X = 1
    rowCount = 0
    While rowCount = 0
        If xlApp.Cells(X, 2) = "" Then rowCount = X
        X = X + 1
    Wend
    
    'get individual cell value and insert to table
    For X = 2 To rowCount
        SEQNO = xlApp.Cells(X, 1)
        ACCTNUM = xlApp.Cells(X, 2)
        METERNO = xlApp.Cells(X, 3)
        CUSTNAME = Replace(xlApp.Cells(X, 4), "'", "")
        ADDRESS = Replace(xlApp.Cells(X, 5), "'", "")
        PRESRDG = xlApp.Cells(X, 6)
        NEWSEQNO = xlApp.Cells(X, 7)
        OCREMARKS = xlApp.Cells(X, 8)
        REMARKS = xlApp.Cells(X, 9)
        VERRDG = xlApp.Cells(X, 10)
        If Len(xlApp.Cells(X, 11)) = 1 Then
              VEROC = "0" & xlApp.Cells(X, 11)
        Else
             VEROC = xlApp.Cells(X, 11)
        End If
        VERREMARKS = xlApp.Cells(X, 12)
        
        '------------- begin - get uldoc_no and check for duplicate acctnum ---------
        Set rs = New ADODB.Recordset
        rs.Open "SELECT ULDOC_NO, METERNO, PRESRDG, FFCODE, REMARKS FROM dbo.T_UPLOAD WHERE ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & CInt(verMonth) & "", g_Conn, adOpenStatic, adLockReadOnly
        If rs.RecordCount > 0 Then
            docno1 = rs.Fields("ULDOC_NO").Value
            mtrno1 = rs.Fields("METERNO").Value
            rdg1 = rs.Fields("PRESRDG").Value
            oc1 = rs.Fields("FFCODE").Value
            rem1 = rs.Fields("REMARKS").Value
            If rs.RecordCount > 1 Then
                rs.MoveLast
                docno2 = rs.Fields("ULDOC_NO").Value
                mtrno2 = rs.Fields("METERNO").Value
                rdg2 = rs.Fields("PRESRDG").Value
                oc2 = rs.Fields("FFCODE").Value
                rem2 = rs.Fields("REMARKS").Value
                ansdup = CustomMsgBox("Account No.: " & ACCTNUM & vbCrLf & vbCrLf & "RECORD 1  -  Meter No.: " & Padr(mtrno1, 20 - Len(mtrno1) + 18, "") & " Reading: " & Padr(rdg1, 12 - Len(rdg1) + 10, "") & " OC: " & Padr(oc1, 5 - Len(oc1) + 3, "") & " Remarks: " & Padr(rem1, 42 - Len(rem1) + 40, "") & vbCrLf & "RECORD 2  -  Meter No.: " & Padr(mtrno2, 20 - Len(mtrno2) + 18, "") & " Reading: " & Padr(rdg2, 12 - Len(rdg2) + 10, "") & " OC: " & Padr(oc2, 5 - Len(oc2) + 3, "") & " Remarks: " & Padr(rem2, 42 - Len(rem2) + 40, "") & vbCrLf & vbCrLf & "Please choose to which record the verified reading should be downloaded. ", vbYesNo, "Verified Reading", "Record 1", "Record 2")
                If ansdup = 6 Then 'Record 1
                    docno = docno1
                Else 'ans=7 Record 2
                    docno = docno2
                End If
            Else
                docno = docno1
            End If
        End If
        rs.Close
        '------------- end - get uldoc_no and check for duplicate acctnum ----------
        
        ' check if account exist in t_upload - move to next row
        sqlinv = "SELECT * FROM dbo.T_UPLOAD WHERE ULDOC_NO = '" & docno & "' AND ULCYCLE = " & CInt(verMonth) & ""
        If Not OpenRecordset(rsSQLinv, sqlinv, "", "") And ACCTNUM <> "" Then
            rejacctinv(rejacctcntinv) = ACCTNUM
            rejacctcntinv = rejacctcntinv + 1
            GoTo lastline
        End If
        
        ' check if account had been verified previously/accounts already exist in t_upload_his - move to next row
        sql1 = "SELECT * FROM dbo.T_UPLOAD_HIS WHERE ULDOC_NO = '" & docno & "' AND ULCYCLE = " & CInt(verMonth) & ""
        If OpenRecordset(rsSQL1, sql1, "", "") Then
            rejacct(rejacctcnt) = ACCTNUM
            rejacctcnt = rejacctcnt + 1
            GoTo lastline
        End If
                
        ' check if condition is not erroneous or forced closed - move to next row
        sqlnoterr = "SELECT * FROM dbo.T_VER_EXTRACT WHERE ULDOC_NO = '" & docno & "' AND ULCYCLE = " & CInt(verMonth) & " and CONDITION = 3"
        If OpenRecordset(rsSQLnoterr, sqlnoterr, "", "") Then
            rejacctnoterr(rejacctcntnoterr) = ACCTNUM
            rejacctcntnoterr = rejacctcntnoterr + 1
            GoTo lastline
        End If
        
        ' check if verrdg is numeric or null - if not move to next row
        ' check if veroc exists in r_ff - if not move to next row
        ' check if length of remarks is greater than 40 - move to next row
        sqlsec = "SELECT * FROM dbo.R_FF WHERE FFCODE = '" & VEROC & "'"
        If (IsNumeric(VERRDG) = False And VERRDG <> "") Or (Not OpenRecordset(rsSQLsec, sqlsec, "", "")) Or (Len(VERREMARKS) > 40) Then
            rejacctsec(rejacctcntsec) = ACCTNUM
            rejacctcntsec = rejacctcntsec + 1
            GoTo lastline
        End If
        
'        ' check for duplicate acctnum
'        sqldup = "SELECT ULDOC_NO FROM dbo.T_UPLOAD WHERE ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & CInt(verMonth) & ""
'        If OpenRecordset(rsSQLdup, sqldup, "", "") Then
'            'If rsSQLdup.Fields(0) > 1 Then
'            If rsSQLdup.RecordCount > 1 Then
'                rejacctdup(rejacctcntdup) = ACCTNUM
'                rejacctcntdup = rejacctcntdup + 1
'                GoTo lastline
'            End If
'        End If
        ' if verified reading is 0, display msgbox with buttons if entry is no reading or zero rdg, readtag 92 for null entry and no reading, 01 for zero rdg
        If VERRDG = "0" Then
            ans = CustomMsgBox("Installation No.: " & ACCTNUM & "   Verified OC: " & VEROC & "   Verified Remarks: " & VERREMARKS & vbCrLf & "Please click the appropriate button if 0 entry is no reading or zero reading. ", vbYesNo, "Verified Reading", "No Reading", "Zero Reading")
            If ans = 6 Then 'no reading
                RDTAG = "92"
            Else 'ans=7 zero rdg
                RDTAG = "01"
            End If
        ElseIf Trim(VERRDG) = "" Then
            RDTAG = "92"
        Else
            RDTAG = "01"
        End If
        
        ' check if both verrdg and veroc are null - move to next row
        If ((Trim(VERRDG) = "" Or RDTAG = "92") And Trim(VEROC) = "") And ACCTNUM <> "" Then
            rejacctnull(rejacctcntnull) = ACCTNUM
            rejacctcntnull = rejacctcntnull + 1
            GoTo lastline
        End If
                
        sqlCheck = "SELECT * FROM dbo.T_VER_EXTRACT WHERE ULDOC_NO = '" & docno & "' AND ULCYCLE = " & CInt(verMonth) & " AND MASTERLIST IN (1, 2)"
        If OpenRecordset(rsCheck, sqlCheck, "", "") Then
          Status = rsCheck.Fields("CONDITION").Value
        'ACCOUNTS ARE INCLUDED IN MASTERLIST WITH MASTERLIST = 1 OR 2
        '--------------------------------------------------------
         'exclude verified readings with null readings and OC = 07 or 03
         'remove exlcusion based on change request #4 aug 19,2009
         ' If Not ((VEROC = "07" Or VEROC = "03") And (VERRDG = "")) Then
           'If Status = "PENDING" Then
           If Status = 0 Then
             'SQLstring = "EXEC sp_VERIFIEDRDG " & SEQNO & ",'" & ACCTNUM & "','" & METERNO & "','" & CUSTNAME & "','" & ADDRESS & "','" & PRESRDG & "'," & NEWSEQNO & ",'" & OCREMARKS & "','" & REMARKS & "','" & VERRDG & "','" & VEROC & "','" & VERREMARKS & "'," & CInt(verMonth) & "," & CInt(verYear) & ",'" & Now() & "',1"
             SQLstring = "EXEC sp_VERIFIEDRDG '" & ACCTNUM & "','" & VERRDG & "','" & VEROC & "','" & VERREMARKS & "'," & CInt(verMonth) & "," & CInt(verYear) & ",'" & Now() & "',1, '" & RDTAG & "', '" & docno & "'"
             DBExecute SQLstring, "", ""
           Else
             MsgBox "Only Accounts with Status = Pending can be verified!" & vbCrLf & ACCTNUM & " is already closed!", vbCritical
           End If
        Else
        'ACCOUNT DOES NOT EXISTS OR ACCOUNT HAS MASTERLIST = 0
        '-------------------------------------------------------
            'SQLstring = "EXEC sp_VERIFIEDRDG " & SEQNO & ",'" & ACCTNUM & "','" & METERNO & "','" & CUSTNAME & "','" & ADDRESS & "','" & PRESRDG & "'," & NEWSEQNO & ",'" & OCREMARKS & "','" & REMARKS & "','" & VERRDG & "','" & VEROC & "','" & VERREMARKS & "'," & CInt(verMonth) & "," & CInt(verYear) & ",'" & Now() & "',0"
            SQLstring = "EXEC sp_VERIFIEDRDG '" & ACCTNUM & "','" & VERRDG & "','" & VEROC & "','" & VERREMARKS & "'," & CInt(verMonth) & "," & CInt(verYear) & ",'" & Now() & "',0, '" & RDTAG & "', '" & docno & "'"
           DBExecute SQLstring, "", ""
        End If
        recCount = recCount + 1
lastline:
        docno = ""
        Next X
       'lsvdbfsource.ColumnHeaders.Item(1).Text = fileName & ": " & recCount - 1 & " Records Verified!"
       lsvdbfsource.ListItems.Add , , fileName & ": " & recCount - 1 & " Records Verified!"
       lsvdbfsource.ListItems.Add , , ""
       
       ' acct does not exist in acctnum
       If rejacctinv(0) <> Trim("") Then
            lsvdbfsource.ListItems.Add , , rejacctcntinv & " Invalid Account(s):"
            'lsvdbfsource.ListItems.Add , , "Accounts Rejected:"
            lstcntinv = 0
            For lstcntinv = 0 To rejacctcntinv - 1
             lsvdbfsource.ListItems.Add , , rejacctinv(lstcntinv)
              lstxtinv = lstxtinv & vbCrLf & rejacctinv(lstcntinv)
            Next lstcntinv
            lstxtinv = "Invalid Account(s):" & lstxtinv & vbCrLf & vbCrLf
            lsvdbfsource.ListItems.Add , , ""
            'MsgBox "Both Verified Reading and Verified OC are null for accounts:" & lstxtnull, vbOKOnly, "Rejected Accounts"
        End If
        ' accounts already exist in t_upload_his
       If rejacct(0) <> Trim("") Then
            lsvdbfsource.ListItems.Add , , rejacctcnt & " Account(s) already exist/verified:"
            'lsvdbfsource.ListItems.Add , , "Accounts Rejected:"
            lstcnt = 0
            For lstcnt = 0 To rejacctcnt - 1
             lsvdbfsource.ListItems.Add , , rejacct(lstcnt)
              lstxt = lstxt & vbCrLf & rejacct(lstcnt)
            Next lstcnt
            lstxt = "Account(s) already exist/verified:" & lstxt & vbCrLf & vbCrLf
            lsvdbfsource.ListItems.Add , , ""
            'MsgBox "Accounts already exist/verified:" & lstxt, vbOKOnly, "Rejected Accounts"
        End If
       ' condition not erroneous or forced closed
       If rejacctnoterr(0) <> Trim("") Then
            lsvdbfsource.ListItems.Add , , rejacctcntnoterr & " Account(s) tagged as not erroneous:"
            'lsvdbfsource.ListItems.Add , , "Accounts Rejected:"
            lstcntnoterr = 0
            For lstcntnoterr = 0 To rejacctcntnoterr - 1
             lsvdbfsource.ListItems.Add , , rejacctnoterr(lstcntnoterr)
              lstxtnoterr = lstxtnoterr & vbCrLf & rejacctnoterr(lstcntnoterr)
            Next lstcntnoterr
            lstxtnoterr = "Account(s) tagged as not erroneous:" & lstxtnoterr & vbCrLf & vbCrLf
            lsvdbfsource.ListItems.Add , , ""
            'MsgBox "Accounts have been tagged as not erroneous:" & lstxtnoterr, vbOKOnly, "Rejected Accounts"
        End If
'       ' duplicate acctnum
'       If rejacctdup(0) <> Trim("") Then
'            lsvdbfsource.ListItems.Add , , rejacctcntdup & " Account(s) with duplicate account numbers:"
'            'lsvdbfsource.ListItems.Add , , "Accounts Rejected:"
'            lstcntdup = 0
'            For lstcntdup = 0 To rejacctcntdup - 1
'             lsvdbfsource.ListItems.Add , , rejacctdup(lstcntdup)
'              lstxtdup = lstxtdup & vbCrLf & rejacctdup(lstcntdup)
'            Next lstcntdup
'            lstxtdup = "Account(s) with duplicate account numbers:" & lstxtdup & vbCrLf & vbCrLf
'            lsvdbfsource.ListItems.Add , , ""
'            'MsgBox "Accounts have been tagged as not erroneous:" & lstxtnoterr, vbOKOnly, "Rejected Accounts"
'        End If
       ' verrdg not int, veroc not in r_ff, rem len>40
       If rejacctsec(0) <> Trim("") Then
            lsvdbfsource.ListItems.Add , , rejacctcntsec & " Account(s) with invalid verified rdg, oc or remarks:"
            'lsvdbfsource.ListItems.Add , , "Accounts Rejected:"
            lstcntsec = 0
            For lstcntsec = 0 To rejacctcntsec - 1
             lsvdbfsource.ListItems.Add , , rejacctsec(lstcntsec)
              lstxtsec = lstxtsec & vbCrLf & rejacctsec(lstcntsec)
            Next lstcntsec
            lstxtsec = "Account(s) with invalid verified rdg, oc or remarks:" & lstxtsec & vbCrLf & vbCrLf
            lsvdbfsource.ListItems.Add , , ""
            'MsgBox "Please check format of verified rdg, oc or remarks for accounts:" & lstxtsec, vbOKOnly, "Rejected Accounts"
        End If
       ' verrdg and veroc both null
       If rejacctnull(0) <> Trim("") Then
            lsvdbfsource.ListItems.Add , , rejacctcntnull & " Account(s) with null Verified Reading and OC:"
            'lsvdbfsource.ListItems.Add , , "Accounts Rejected:"
            lstcntnull = 0
            For lstcntnull = 0 To rejacctcntnull - 1
             lsvdbfsource.ListItems.Add , , rejacctnull(lstcntnull)
              lstxtnull = lstxtnull & vbCrLf & rejacctnull(lstcntnull)
            Next lstcntnull
            lstxtnull = "Account(s) with null Verified Reading and OC:" & lstxtnull & vbCrLf & vbCrLf
            lsvdbfsource.ListItems.Add , , ""
            'MsgBox "Both Verified Reading and Verified OC are null for accounts:" & lstxtnull, vbOKOnly, "Rejected Accounts"
        End If

        If lstcnt <> 0 Or lstcntsec <> 0 Or lstcntnoterr <> 0 Or lstcntinv <> 0 Or lstcntnull <> 0 Then
            MsgBox lstxtinv & lstxt & lstxtnoterr & lstxtsec & lstxtnull, vbOKOnly, "Rejected Accounts"
        End If
'        If lstcnt <> 0 Or lstcntsec <> 0 Or lstcntdup <> 0 Or lstcntnoterr <> 0 Or lstcntinv <> 0 Or lstcntnull <> 0 Then
'            MsgBox lstxtinv & lstxt & lstxtnoterr & lstxtdup & lstxtsec & lstxtnull, vbOKOnly, "Rejected Accounts"
'        End If
        'txtdbfsource.Text = fileName & ": " & recCount - 1 & " Records Verified!"
        
    xlApp.Workbooks.Close
    Set xlApp = Nothing
        ' Move file
        If fso.FolderExists(targetPath) Then
            dnld_dest = targetPath & "\" & fileName
            fso.MoveFile sourcePath, dnld_dest
        Else
            fso.CreateFolder (targetPath)
            dnld_dest = targetPath & "\" & fileName
            fso.MoveFile sourcePath, dnld_dest
        End If
    File1.Refresh
    fileName = ""
    
ElseIf fileName = "" Then
    MsgBox "Kindly click on the file to be uploaded!"
    Exit Sub
End If

End Sub

Private Sub dirSource_Change()
File1.Path = dirSource.Path
End Sub

Private Sub drvSource_Change()
dirSource.Path = drvSource.Drive

End Sub

Private Sub File1_Click()
fileName = File1.fileName
verMonth = Mid(File1.fileName, 5, 2)
verYear = Mid(File1.fileName, 1, 4)

End Sub

Private Sub Form_Activate()
If WindowState = vbNormal Then
    Me.Move (1000)
    Me.Top = 500
    Me.Height = 7365 '5310
    Me.Width = 11235 '5400
End If
End Sub

Private Sub Form_Load()
Dim SQLstring As String
Dim RSDrv As ADODB.Recordset

GetSettings

'On Error GoTo DefaultSource
'sqlstring = "SELECT FileSource FROM T_VALSETTINGS"
'If OpenRecordset(RSDrv, sqlstring, "", "") Then
'    dirSource.Path = RSDrv.Fields(0)
'    drvSource.Drive = Trim(Left(RSDrv.Fields(0), 3))
'Else
'    dirSource.Path = App.Path
'    drvSource.Drive = Trim(Left(App.Path, 3))
'End If
'RSDrv.Close
'Set RSDrv = Nothing
'Exit Sub
'
'DefaultSource:
'    dirSource.Path = App.Path
'    drvSource.Drive = Trim(Left(App.Path, 3))

End Sub

Private Sub GetSettings()
Dim RSDrv As ADODB.Recordset
Dim SQLstring As String

SQLstring = "SELECT * FROM T_VALSETTINGS"

On Error GoTo DefaultSource
If OpenRecordset(RSDrv, SQLstring, "", "") Then
    While Not RSDrv.EOF
    'driveSource = Mid(RSDrv.Fields(0).Value, 1, 2)
    dirSource.Path = RSDrv.Fields(1).Value
    driveSource = RSDrv.Fields(0).Value
    
    RSDrv.MoveNext
    Wend
End If
RSDrv.Close
Set RSDrv = Nothing
'dirSource.Path = drvSource
Exit Sub

DefaultSource:
    dirSource.Path = App.Path
    'drvSource.Drive = Trim(Left(App.Path, 3))

End Sub
