VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmMRUGrid 
   Caption         =   "Series Scheduler"
   ClientHeight    =   9720
   ClientLeft      =   60
   ClientTop       =   750
   ClientWidth     =   15150
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   9720
   ScaleWidth      =   15150
   Begin VB.Frame frameLegend 
      Caption         =   "Color/Status Legend"
      Height          =   975
      Left            =   240
      TabIndex        =   1
      Top             =   8520
      Width           =   14535
      Begin VB.CommandButton cmbClose 
         Caption         =   "&Close"
         Height          =   375
         Left            =   12840
         TabIndex        =   8
         Top             =   360
         Width           =   1335
      End
      Begin VB.CommandButton cmbManage 
         Caption         =   "&Manage"
         Height          =   375
         Left            =   11040
         TabIndex        =   7
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label Label4 
         Caption         =   "Sent to Satellite Office"
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   3600
         TabIndex        =   6
         Top             =   360
         Width           =   1935
      End
      Begin VB.Label Label2 
         Caption         =   "Downloaded from Subic Water"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   600
         TabIndex        =   5
         Top             =   600
         Width           =   2895
      End
      Begin VB.Label Label1 
         Caption         =   "Scheduled for Reading"
         Height          =   255
         Left            =   600
         TabIndex        =   4
         Top             =   360
         Width           =   1935
      End
      Begin VB.Label Label6 
         Caption         =   "Forwarded to Subic Water"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5880
         TabIndex        =   3
         Top             =   360
         Width           =   3495
      End
      Begin VB.Label Label5 
         Caption         =   "Received from Satellite"
         ForeColor       =   &H0000C000&
         Height          =   255
         Left            =   3600
         TabIndex        =   2
         Top             =   600
         Width           =   1935
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdMRU 
      Height          =   6855
      Left            =   240
      TabIndex        =   0
      Top             =   840
      Width           =   14655
      _ExtentX        =   25850
      _ExtentY        =   12091
      _Version        =   393216
      Rows            =   3
      Cols            =   3
      AllowBigSelection=   0   'False
      FocusRect       =   2
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblPendCount 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9240
      TabIndex        =   14
      Top             =   7920
      Width           =   3375
   End
   Begin VB.Label lblUpCount 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5280
      TabIndex        =   13
      Top             =   7920
      Width           =   3375
   End
   Begin VB.Label lblTotalAccts 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1320
      TabIndex        =   12
      Top             =   7920
      Width           =   3375
   End
   Begin VB.Label lblDay 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   12840
      TabIndex        =   11
      Top             =   240
      Width           =   1815
   End
   Begin VB.Label lblBusCtr 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4920
      TabIndex        =   10
      Top             =   240
      Width           =   4095
   End
   Begin VB.Label lblCycle 
      Caption         =   "Cycle/Month :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   240
      Width           =   1695
   End
End
Attribute VB_Name = "frmMRUGrid"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub PopulateGridMRUCount(grd As MSFlexGrid, Query As String)
    On Error GoTo errFillGrid

    If OpenRecordset(g_rs_TSCHED, Query, "frmMRUGrid", "MRU Management") Then
        Screen.MousePointer = vbHourglass
        LoadTSchedGrid g_rs_TSCHED, grd, "MRU"
        Screen.MousePointer = vbDefault
        Set g_rs_TSCHED = Nothing
    End If
    
    Exit Sub
    
errFillGrid:
    MsgBox Err.Description
End Sub
Private Sub BuildMRUSchedGrid(grd As MSFlexGrid, Query As String)
    On Error GoTo errFillGrid

    If OpenRecordset(g_rs_GRIDMRU, Query, "frmMRUGrid", "MRU Management") Then
        Screen.MousePointer = vbHourglass
        ' Build the flexgrid
        LoadMRUSchedGrid g_rs_GRIDMRU, grd
        Screen.MousePointer = vbDefault
        Set g_rs_GRIDMRU = Nothing
        Exit Sub
    End If
    
errFillGrid:
    MsgBox Err.Description
End Sub

Private Sub cmbClose_Click()
    Unload Me
End Sub

Private Sub cmbManage_Click()

grdMRU_DblClick
'    With grdMRU
'        sBusCenter = .TextMatrix(.rowSel, 0)
'        intDayNo = .ColSel
'        lblMouseLoc.Caption = "Business Center = " & sBusCenter & "  Day = " & Val(intDayNo)
'    End With
'    Me.Enabled = False
'    frmMRUMngt.Show
End Sub

Private Sub Form_Activate()
    Dim strSQL As String
    Dim strCycle As String
    
    'frmMRUGrid.WindowState = 2
If WindowState = vbNormal Then
    Me.Move (0)
    Me.Top = 0
    Me.Height = 10230
    Me.Width = 15270
End If
    'strCycle = Right(g_BillMonth, 2)
    strCycle = str(intBillCycle)
    lblCycle.Caption = "Cycle/Month : " & strCycle
    strSQL = "SELECT DMZCODE_ID +" + "' - '" + "+ DMZ_DESC as BUSINESS_CENTER FROM dbo.L_DMZCODE ORDER BY DMZCODE_ID"
    BuildMRUSchedGrid grdMRU, strSQL
    
    'Populate with the MRU counts FOR THIS CYCLE
    strSQL = "SELECT  T_BOOK.DMZCODE_ID, " & _
    " L_DMZCODE.DMZ_DESC, T_SCHED.DAYNO, " & _
    " Max(R_BOOKSTAT.BKSTLEVEL) AS MAXMRUSTAT, " & _
    " Count(T_BOOK.Bookno) As NUM_MRU FROM (L_DMZCODE INNER JOIN T_BOOK ON L_DMZCODE.DMZCODE_ID=T_BOOK.DMZCODE_ID) " & _
    " INNER JOIN (R_BOOKSTAT INNER JOIN T_SCHED ON R_BOOKSTAT.BKSTCODE=T_SCHED.BKSTCODE) " & _
    " ON (T_BOOK.BOOKNO=T_SCHED.BOOKNO) AND (T_BOOK.CYCLE=T_SCHED.CYCLE)" & _
    " Where T_BOOK.cycle = " & strCycle & " GROUP BY T_BOOK.DMZCODE_ID, L_DMZCODE.DMZ_DESC, T_SCHED.DAYNO" & _
    " ORDER BY T_BOOK.DMZCODE_ID, L_DMZCODE.DMZ_DESC, T_SCHED.DAYNO "
    PopulateGridMRUCount grdMRU, strSQL
End Sub

Private Sub Form_Unload(Cancel As Integer)
    MDIMain.mnuCurrentCycle.Enabled = True
    MDIMain.mnuPreviousCycle.Enabled = True
End Sub

Private Sub grdMRU_Click()
'old code 08-12-2013 jums
'    With grdMRU
'        sBusCenter = .TextMatrix(.rowSel, 0)
'        intDayNo = .ColSel
'        lblBusCtr.Caption = "Business Center : " & sBusCenter
'        lblDay.Caption = "Day : " & Val(intDayNo)
'    End With

'new code view counts 08-12-2013 jums
Dim TotAcctQuery As String
Dim TotAcctRec As ADODB.Recordset
Dim TotAcctCount As String
Dim TotUpCountQuery As String
Dim TotUpAcctRec As ADODB.Recordset
Dim TotUpCount As String

    With grdMRU
        sBusCenter = .TextMatrix(.rowSel, 0)
        intDayNo = .ColSel
        lblBusCtr.Caption = "Business Center : " & sBusCenter
        lblDay.Caption = "Day : " & Val(intDayNo)

' old code 10-18-2013 jums
'        TotAcctQuery = "Select sum(accts) from t_bkinfo A inner join t_sched b on a.bookno = b.bookno " & _
'              "and a.cycle = b.cycle inner join t_book C on a.bookno = c.bookno and a.cycle = b.cycle " & _
'              "where a.cycle = " & Mid(g_BillMonth, 5, 2) & " and B.DayNo = '" & intDayNo & "'" 'and c.dmzcode_id = '" & Mid(sBusCenter, 1, 3) & "'"

        TotAcctQuery = "Select sum(accts) from t_bkinfo A inner join t_sched b on a.bookno = b.bookno " & _
              "and a.cycle = b.cycle inner join t_book C on a.bookno = c.bookno and a.cycle = c.cycle " & _
              "where a.cycle = " & Mid(g_BillMonth, 5, 2) & " and B.DayNo = '" & intDayNo & "'" 'and c.dmzcode_id = '" & Mid(sBusCenter, 1, 3) & "'"

        If OpenRecordset(TotAcctRec, TotAcctQuery, "frmMRUGrid", "MRU Management") Then
            If IsNull(TotAcctRec.Fields(0)) = True Then
                TotAcctCount = 0
            Else
                TotAcctCount = TotAcctRec.Fields(0)
            End If
        End If
        lblTotalAccts = "Total Records : " & TotAcctCount
        
' old code 10-22-2013 jums
'        TotUpCountQuery = "Select count(acctnum) from t_upload A inner join t_sched b on a.seriesno = b.bookno " & _
'           "and a.bill_month = b.cycle inner join t_book C on a.seriesno = c.bookno " & _
'           "where a.bill_month = " & Mid(g_BillMonth, 5, 2) & " and B.DayNo = '" & intDayNo & "'" 'and c.dmzcode_id = '" & Mid(sBusCenter, 1, 3) & "'"


        TotUpCountQuery = " Select count(acctnum) from t_upload A " & _
            "inner join t_sched b on a.seriesno = b.bookno and a.bill_month = b.cycle " & _
            "inner join t_book C on a.seriesno = c.bookno  and a.bill_month = c.cycle " & _
            "where a.bill_month = " & Mid(g_BillMonth, 5, 2) & " and B.DayNo = '" & intDayNo & "'" 'and c.dmzcode_id = '" & Mid(sBusCenter, 1, 3) & "'"

        If OpenRecordset(TotUpAcctRec, TotUpCountQuery, "frmMRUGrid", "MRU Management") Then
            If IsNull(TotUpAcctRec.Fields(0)) = True Then
                TotUpCount = 0
            Else
                TotUpCount = TotUpAcctRec.Fields(0)
            End If
        End If
        lblUpCount = "Total Records Uploaded : " & TotUpCount
        
        lblPendCount = "Total Records Pending : " & TotAcctCount - TotUpCount
    End With
End Sub

Private Sub grdMRU_DblClick()

    With grdMRU
        sBusCenter = .TextMatrix(.rowSel, 0)
        intDayNo = .ColSel
        lblBusCtr.Caption = "Business Center : " & sBusCenter
        lblDay.Caption = "Day : " & Val(intDayNo)
    End With
    
    Me.Enabled = False
    frmMRUMngt.Show
    
End Sub

