VERSION 5.00
Begin VB.Form frmAddUser 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Add User ID"
   ClientHeight    =   2325
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3510
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2325
   ScaleWidth      =   3510
   Begin VB.ComboBox cmbUserAccess 
      Height          =   315
      Left            =   1700
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   1140
      Width           =   1600
   End
   Begin VB.TextBox txtConfirmPw 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1700
      MaxLength       =   8
      PasswordChar    =   "*"
      TabIndex        =   2
      Top             =   800
      Width           =   1600
   End
   Begin VB.TextBox txtPW 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1700
      MaxLength       =   8
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   460
      Width           =   1600
   End
   Begin VB.TextBox txtUserID 
      Height          =   285
      Left            =   1710
      MaxLength       =   8
      TabIndex        =   0
      Top             =   120
      Width           =   1600
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   390
      Left            =   1845
      TabIndex        =   5
      Top             =   1695
      Width           =   915
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   390
      Left            =   825
      TabIndex        =   4
      Top             =   1695
      Width           =   915
   End
   Begin VB.Label lblUserAccess 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      Caption         =   "User Access : "
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   120
      TabIndex        =   9
      Top             =   1150
      Width           =   1500
   End
   Begin VB.Label lblConfirmPw 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      Caption         =   "Confirm Password : "
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   120
      TabIndex        =   8
      Top             =   810
      Width           =   1500
   End
   Begin VB.Label lblPW 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      Caption         =   "Password : "
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   120
      TabIndex        =   7
      Top             =   470
      Width           =   1500
   End
   Begin VB.Label lblUserID 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      Caption         =   "User ID : "
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   120
      TabIndex        =   6
      Top             =   130
      Width           =   1500
   End
End
Attribute VB_Name = "frmAddUser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub cmbUserAccess_Click()
    txtPW.Text = ""
    txtConfirmPw.Text = ""
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdSave_Click()
    Dim l_obj_Item As Object
    Dim l_int_Index As Integer
    
    If DuplicateRecords Then Exit Sub
    
    If txtPW <> txtConfirmPw Then
        MsgBox "Passwords do not match.  Please re-enter passwords.", vbExclamation, "Passwords"
        txtPW.SetFocus
        Exit Sub
    End If
    
    If g_str_AddEdit = "ADD" Then
        If Not SaveAdd Then Exit Sub

        Set l_obj_Item = frmPWMgmt.lsvUsers.ListItems.Add(, , Trim(txtUserID))
            l_obj_Item.SubItems(1) = Trim(Left(cmbUserAccess.Text, 50))
            l_obj_Item.SubItems(2) = Trim(txtPW)
        Set l_obj_Item = Nothing

        frmPWMgmt.cmdEdit.Enabled = True
        frmPWMgmt.cmdDelete.Enabled = True
    Else
        If Not SaveEdit Then Exit Sub

        l_int_Index = frmPWMgmt.lsvUsers.SelectedItem.Index
        frmPWMgmt.lsvUsers.ListItems(l_int_Index).Text = Trim(txtUserID)
        frmPWMgmt.lsvUsers.ListItems(l_int_Index).SubItems(1) = Trim(Left(cmbUserAccess.Text, 50))
        frmPWMgmt.lsvUsers.ListItems(l_int_Index).SubItems(2) = Trim(txtPW)
    End If

    frmPWMgmt.lsvUsers.Refresh
    Unload Me

End Sub

Function DuplicateRecords() As Boolean
    Dim l_str_Sql As String

    DuplicateRecords = True
    
    If g_str_AddEdit = "EDIT" Then
        If Trim(txtUserID) = frmPWMgmt.lsvUsers.SelectedItem.Text And _
                Trim(Left(cmbUserAccess.Text, 50)) = frmPWMgmt.lsvUsers.SelectedItem.SubItems(1) And _
                Trim(txtPW) = frmPWMgmt.lsvUsers.SelectedItem.SubItems(2) And _
                Trim(txtConfirmPw) = frmPWMgmt.lsvUsers.SelectedItem.SubItems(2) Then
            Unload Me
            Exit Function
        End If
    
        If Trim(txtUserID) = frmPWMgmt.lsvUsers.SelectedItem.Text Then
            DuplicateRecords = False
            Exit Function
        End If
    End If

    l_str_Sql = "SELECT USERID FROM S_USER WHERE USERID = '" & Trim(txtUserID) & "'"

    If OpenRecordset(g_rs_SUSER, l_str_Sql, Me.Name, "DuplicateRecords") Then
        MsgBox "Record already exists!", vbExclamation, "Duplicate Record"
        g_rs_SUSER.Close
        Set g_rs_SUSER = Nothing
        txtUserID.SetFocus
        Exit Function
    End If

    DuplicateRecords = False

End Function

Function SaveAdd() As Boolean
    Dim l_str_Sql, l_str_Access As String
    
    'added new condition below for the password 05-08-2012 jums
    'changed Left(Trim(txtPW) to Left(Trim(sPassword)
    Dim sPassword As String
    
    sPassword = Trim(UCase(txtPW))
    sPassword = PWDEncrypt(sPassword, 0)


    l_str_Sql = "INSERT INTO S_USER (USERID, [LEVEL], [PASSWORD]) VALUES ('" & _
        Left(Trim(txtUserID), 8) & "', '" & _
        Trim(Right(cmbUserAccess.Text, 2)) & "', '" & _
        Left(Trim(sPassword), 8) & "')"

    SaveAdd = DBExecute(l_str_Sql, Me.Name, "SaveAdd")

End Function

Function SaveEdit() As Boolean
    Dim l_str_Sql As String
    
    'added new condition below for the password 05-08-2012 jums
    'changed Left(Trim(txtPW) to Left(Trim(sPassword)
    Dim sPassword As String
    
    sPassword = Trim(UCase(txtPW))
    sPassword = PWDEncrypt(sPassword, 0)
    
    l_str_Sql = "UPDATE S_USER SET USERID = '" & Left(Trim(txtUserID), 8) & _
        "', [LEVEL] = '" & Trim(Right(cmbUserAccess.Text, 2)) & _
        "', [PASSWORD] = '" & Left(Trim(sPassword), 8) & _
        "' WHERE USERID = '" & frmPWMgmt.lsvUsers.SelectedItem.Text & "'"

    SaveEdit = DBExecute(l_str_Sql, Me.Name, "SaveEdit")

End Function

Private Sub Form_Load()
    
If WindowState = vbNormal Then
    Me.Move (1000)
    Me.Top = 500
End If
'drop down box on the User Access :
    cmbUserAccess.AddItem "User" & Space(50) & "1"
    cmbUserAccess.AddItem "Administrator" & Space(50) & "2"
    'added new code 07-17-2013 jums for super user option enabled only for super user account
    If g_rs_userlevel = 3 Then
        cmbUserAccess.AddItem "Super User" & Space(50) & "3"
    End If
    cmbUserAccess.ListIndex = 0
    
    If g_str_AddEdit = "ADD" Then
        Me.Caption = "Add User ID"
        txtUserID = ""
        txtPW = ""
        txtConfirmPw = ""
        If Not OpenRecordset(g_rs_SUSER, "SELECT USERID FROM S_USER", Me.Name, "Form_Load") Then
            cmbUserAccess.ListIndex = 1
            cmbUserAccess.Enabled = False
        Else
            g_rs_SUSER.Close
            Set g_rs_SUSER = Nothing
            cmbUserAccess.Enabled = True
        End If
    Else
        Me.Caption = "Edit User ID"
        txtUserID = frmPWMgmt.lsvUsers.SelectedItem.Text
        MatchCombo cmbUserAccess, frmPWMgmt.lsvUsers.SelectedItem.SubItems(1), False
        txtPW = frmPWMgmt.lsvUsers.SelectedItem.SubItems(2)
        txtConfirmPw = frmPWMgmt.lsvUsers.SelectedItem.SubItems(2)
    End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmPWMgmt.Enabled = True
End Sub

Private Sub txt_Change()
    If Len(Trim(txtUserID)) > 0 And Len(Trim(txtPW)) > 0 And Len(Trim(txtConfirmPw)) > 0 Then
        cmdSave.Enabled = True
    Else
        cmdSave.Enabled = False
    End If

End Sub

Private Sub txtConfirmPw_Change()
    txt_Change
End Sub

Private Sub txtConfirmPw_GotFocus()
    TextHighlight txtConfirmPw
End Sub

Private Sub txtPW_Change()
    txt_Change
End Sub

Private Sub txtPW_GotFocus()
    TextHighlight txtPW
End Sub

Private Sub txtUserID_Change()
    txt_Change
End Sub

Private Sub txtUserID_GotFocus()
    TextHighlight txtUserID
End Sub
