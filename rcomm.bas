Attribute VB_Name = "rcomm"
    Global Flg_Cancel%
    
    'Const DS_id = 240   '0xF0
    'Const DS_ack = 224  '0xE0
    Const DS_id = 241   '0xF0
    Const DS_ack = 225  '0xE0
    Const RVR_id = 241  '0xF1
    Const RVR_ack = 225 '0xE1
    
    Const ETX = 3       '0x03
    Const stat_ok = 4 '0x04
    Const stat_err = 16 '0x10

    Global mv_Cradle%
    Global rc As MSComm
    Global Port%
    Global Comm_Err%
    Global RX_Msg$
    Global rx_byte As String

    Global gv_Command$
    
    Dim OutBuf As Variant
    Dim byt() As Byte
    'Dim rc1 As MSComm
    Dim TXCount, RXCount As Byte

Sub MainComm(stat As String)
'    gv_Command$ = Command$
    
    On Error Resume Next
    Port% = 1
'    i% = InStr(gv_Command$, " p")
'    If i% > 0 Then
'        Port% = Val(Mid$(gv_Command$, i% + 2, 1))
'        If Port% = 0 Then Port% = 1
'    End If
    
    Set rc = F_Main.MSComm1
    Comm_Init
    mv_Cradle% = 0
    
    F_Main.Show
    F_Main.Read_Paths (stat)
    
End Sub

Sub Comm_Init()
    On Error GoTo err1
    rc.CommPort = Port%
    rc.Settings = "57600,N,8,2"
    rc.InputLen = 1
    'rc.InBufferSize = 10
    rc.InputMode = comInputModeBinary
    rc.Handshaking = comNone
        
    'RC.RTSEnable = False
    'RC.DTREnable = False
    'RC.EOFEnable = False

    'RC.RThreshold = 0
    rc.PortOpen = True
    rc.RTSEnable = True
    
    TXCount = 1
    Exit Sub
err1:
    MsgBox Err.Description, vbCritical, "Comm Port Error"
    Exit Sub
    'End
End Sub

Sub comm_reinit()
    On Error GoTo err1
    rc.PortOpen = False
    rc.CommPort = Port%
    rc.Settings = "57600,N,8,2"
    rc.InputLen = 1
    'RC.InBufferSize = 10
    rc.InputMode = comInputModeBinary
    rc.Handshaking = comNone
    'RC.RThreshold = 0
    rc.PortOpen = True
    Exit Sub
err1:
    MsgBox Err.Description, vbCritical, "Comm Port Error"
    Exit Sub
    'End
End Sub

Sub comm_reinit_IR()
    'On Error GoTo err1
    If mv_Cradle% > 0 Then Exit Sub
    
    'F_Main.List1.AddItem "Detecting Cradle/IR ..."
    'F_Main.List1.ListIndex = F_Main.List1.ListCount - 1
    'F_Main.Label1 = ""
    'F_Main.Shape1.BackColor = &H808080
    'F_Main.ENT_Rec = "Detecting Cradle."
    'F_Main.MousePointer = 11

    'rc.PortOpen = True
    rc.DTREnable = True
    
'    i& = Timer + 0.2
'    While i& > Timer
'        DoEvents
'    Wend
    
    If Comm_TestIR("57600,N,8,1") Then GoTo com60
    If Comm_TestIR("38400,N,8,1") Then GoTo com50
    If Comm_TestIR("115200,N,8,1") Then GoTo com50
    If Comm_TestIR("19200,N,8,1") Then GoTo com50
    If Comm_TestIR("9600,N,8,1") Then GoTo com50
    
    'F_Main.ENT_Rec = "No Cradle/IR detected."
    'F_Main.Label1 = "No Cradle/IR"
    'F_Main.Shape1.BackColor = vbRed
    'F_Main.List1.AddItem "No Cradle/IR detected."

com30:
    DoEvents
    'F_Main.List1.AddItem "Switching to Cable Direct."
    'F_Main.List1.ListIndex = F_Main.List1.ListCount - 1
    'comm_reinit
    rc.PortOpen = False
    rc.Settings = "57600,N,8,1"
    rc.PortOpen = True
    rc.RTSEnable = True
    'F_Main.MousePointer = 0
    mv_Cradle% = 0
    Exit Sub

com50:
    rc.RTSEnable = False
    
    rc.Output = Chr$(&H7)
    i& = Timer + 0.1
    While i& > Timer
        DoEvents
    Wend
    rc.Output = Chr$(&H35)
    i& = Timer + 0.6
    While i& > Timer
        DoEvents
    Wend
    rc.Output = Chr$(&H51)
        
    rc.RTSEnable = True
    
    rc.PortOpen = False
    rc.Settings = "57600,N,8,1"
    rc.PortOpen = True
    
    rc.RTSEnable = False
    rc.Output = Chr$(&HF)
    rx_byt
    If byt(0) <> &HF Then
        F_Main.List1.AddItem "Bad Cradle/IR configuration!"
        GoTo com30
    End If


com60:
    'Enable RTS, DTS
    rc.RTSEnable = True
    rc.DTREnable = True
    F_Main.List1.AddItem "Cradle/IR -OK"
    'F_Main.List1.ListIndex = F_Main.List1.ListCount - 1
    'F_Main.ENT_Rec = "Cradle/IR -OK"
    'F_Main.Shape1.BackColor = &H808080
    'F_Main.MousePointer = 0
    mv_Cradle% = 1
    Exit Sub
    
'err1:
'    MsgBox Err.Description, vbCritical, "IR Port Error"
End Sub

Sub comm_end()
    rc.PortOpen = False
End Sub

Function Comm_TestIR(st$) As Boolean
    On Error Resume Next
    
    'F_Main.ENT_Rec = F_Main.ENT_Rec & "."
    'F_Main.List1.AddItem st$
    'F_Main.List1.ListIndex = F_Main.List1.ListCount - 1
      
    rc.PortOpen = False
    rc.Settings = st$ '"9600,N,8,2"
    rc.Handshaking = comNone 'comRTS '
    rc.PortOpen = True
    
    rc.RTSEnable = False
    rc.DTREnable = True
    
'    i& = Timer + 0.2
'    While i& > Timer
'        DoEvents
'    Wend
   
    a$ = rc.Input
    a$ = ""
    
    rc.Output = Chr$(&HF)
    
    'rx_byt
    
    t& = Timer + 0.4
    rx_byte = ""
    While rx_byte = "" And t& > Timer
        rx_byte = rc.Input
        DoEvents
    Wend
    If rx_byte = "" Then
        rx_byte = "x"
        'Comm_Err% = 1
    End If
    byt() = rx_byte

    Comm_TestIR = byt(0) = &HF
End Function

Sub send3(msg$)
retry:
    For i% = 1 To 2
        TX (msg$)
        If Comm_Err% = 0 Then Exit Sub
    Next i%
    If MsgBox("HHT Comm Error", vbCritical + vbRetryCancel, "HHT Error") = vbRetry Then GoTo retry
End Sub

Sub recv3()
    For i% = 1 To 3
        RX
        If Comm_Err% = 0 Then Exit For
    Next i%
End Sub
Sub sendrecv(msg$)
    send3 (msg$)
    If Comm_Err% = 0 Then recv3
End Sub

Sub TX(msg$)
    'F_Main.Timer1.Interval = 400
    Comm_Err% = 0
    
    'Clear input buffer
    a$ = rc.Input
    'rc.InBufferSize = 1024 'prevent rc.OnComm event
    
    While a$ <> ""
        a$ = rc.Input
        DoEvents
    Wend

    'compute checksum
    csum% = 0
    For i% = 1 To Len(msg$)
        csum% = csum% Xor Asc(Mid$(msg$, i%, 1))
    Next i%
    'If Asc(Left$(msg$, 1)) < 128 Then csum% = csum% Xor ETX 'Include ETX in checksum
    If csum% = 0 Then csum% = 1

    'send attention & message id
    rc.Output = Chr$(RVR_id) & Chr$(TXCount)
    'get reply
    rx_byt
    If Comm_Err% > 0 Then Exit Sub      'timeout error
    If byt(0) = stat_ok Then GoTo TX90  'received already
        
    If byt(0) <> RVR_ack Then
        Comm_Err% = 2
        Exit Sub
    End If

    If Asc(Left$(msg$, 1)) < 128 Then
        rc.Output = msg$ & Chr$(ETX) & Chr$(csum%)
    Else
        rc.Output = msg$ & Chr$(csum%)
    End If
    
    rx_byt
    If Comm_Err% > 0 Then Exit Sub
    If byt(0) <> stat_ok Then
        Comm_Err% = 3
        Exit Sub
    End If
    
'    rc.Output = Chr$(TXCount)
'    rx_byt
'    If Comm_Err% > 0 Then Exit Sub
'    If byt(0) <> stat_ok Then
'        Comm_Err% = 3
'        Exit Sub
'    End If

TX90:
    TXCount = TXCount + 1
    If TXCount > 100 Then TXCount = 1
End Sub

Sub RX_Fast()
    'F_Main.Timer1.Interval = 400
    Comm_Err% = 0

    rx_byte = ""
    rx_byte = rc.Input
    If rx_byte = "" Then
        Comm_Err% = 1
        Exit Sub
    End If
    
    byt() = rx_byte
       
    RX0
End Sub

Sub RX()
    'F_Main.Timer1.Interval = 400
    Comm_Err% = 0

    rx_byt
    If Comm_Err% > 0 Then
        Comm_Err% = 10
        Exit Sub
    End If
    
    RX0
End Sub


Sub RX0()
    
    'rc.InBufferSize = 1024 'prevent rc.OnComm event
    If byt(0) <> DS_id Then
        Comm_Err% = 1 '11
        Exit Sub
    End If
    
    ' receive tx id
    rx_byt
    If Comm_Err% > 0 Then Exit Sub
    If byt(0) = RXCount Then
        rc.Output = Chr$(stat_ok)
        Comm_Err% = 13
        Exit Sub
    End If
    
    RXCount0 = byt(0) 'Save TX id temporary
    
    rc.Output = Chr$(DS_ack)
    'get function code
    
    csum% = 0
    RX_Msg$ = ""
    
    rx_byt
    If Comm_Err% > 0 Then Exit Sub
    
    If byt(0) < 128 Then
    
        While byt(0) <> ETX
            csum% = csum% Xor byt(0)
            RX_Msg$ = RX_Msg$ & Chr$(byt(0))
            rx_byt
            If Comm_Err% > 0 Then Exit Sub
        Wend
        
    Else
    
        rx_byt
        If Comm_Err% > 0 Then Exit Sub
        csum% = csum% Xor byt(0)
        RX_Msg$ = RX_Msg$ & Chr$(byt(0))
        i% = byt(0)
        While i% > 0
            rx_byt
            If Comm_Err% > 0 Then Exit Sub
            csum% = csum% Xor byt(0)
            RX_Msg$ = RX_Msg$ & Chr$(byt(0))
            i% = i% - 1
        Wend
        
    End If
    
    ' receive checksum
    If csum% = 0 Then csum% = 1
    rx_byt
    If Comm_Err% > 0 Then Exit Sub
    If byt(0) <> csum% Then
        rc.Output = Chr$(stat_err)
        Comm_Err% = 12
        Exit Sub
    End If
    rc.Output = Chr$(stat_ok)
    'RX_Msg$ = Left$(RX_Msg$, Len(RX_Msg$) - 1)

    RXCount = RXCount0 'Update TX id
    
'    ' receive tx id
'    rx_byt
'    If Comm_Err% > 0 Then Exit Sub
'    rc.Output = Chr$(stat_ok)
'    If byt(0) = RXCount Then
'        Comm_Err% = 13
'        Exit Sub
'    End If
    
End Sub

Sub rx_byt()
    Dim t&
    
    t& = Timer + 1 '+0.4
    'f_main.Timer1.Enabled = True
    rx_byte = ""
    
    'rx_byte = RC.Input
    While rx_byte = "" And t& > Timer
        rx_byte = rc.Input
        DoEvents
    Wend
    If rx_byte = "" Then
        rx_byte = "x"
        Comm_Err% = 1
    End If
    'f_main.Timer1.Enabled = False
    byt() = rx_byte
    
    
End Sub

Sub tx_byt0(a$)
    rc.Output = a$
End Sub

Sub rx_byt0(a$)
    a$ = rc.Input
End Sub
