VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMRUMngt 
   Caption         =   "Manage Series"
   ClientHeight    =   9720
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15150
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9720
   ScaleWidth      =   15150
   Begin VB.CommandButton cmdUncheckAll 
      Caption         =   "&Uncheck All"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   8880
      TabIndex        =   17
      Top             =   8880
      Width           =   1335
   End
   Begin VB.CommandButton cmdCheckAll 
      Caption         =   "C&heck All"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   7560
      TabIndex        =   16
      Top             =   8880
      Width           =   1335
   End
   Begin VB.CommandButton cmdRSO 
      Caption         =   "&Recv from SO"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   11760
      TabIndex        =   13
      Top             =   8880
      Width           =   1335
   End
   Begin VB.CommandButton cmdSSO 
      Caption         =   "Send to &SO"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   10440
      TabIndex        =   12
      Top             =   8880
      Width           =   1335
   End
   Begin VB.CommandButton cmdViewUL 
      Caption         =   "View &UL File"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   6000
      TabIndex        =   11
      Top             =   8880
      Width           =   1335
   End
   Begin VB.CommandButton cmdViewDL 
      Caption         =   "View D&L File"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   4680
      TabIndex        =   10
      Top             =   8880
      Width           =   1335
   End
   Begin VB.Frame fraSelect 
      Height          =   7785
      Left            =   240
      TabIndex        =   4
      Top             =   960
      Width           =   14715
      Begin MSComctlLib.ListView lsvMRUMngt 
         Height          =   7395
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   14475
         _ExtentX        =   25532
         _ExtentY        =   13044
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "MRU Number"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Status"
            Object.Width           =   3881
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Sched. Reading Date"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Dnld from MWSI"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Sent to SO"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Recv from SO"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Fwd to MWSI"
            Object.Width           =   3705
         EndProperty
      End
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "&Add"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   480
      TabIndex        =   3
      Top             =   8880
      Width           =   1335
   End
   Begin VB.CommandButton cmdEdit 
      Caption         =   "&Edit"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   1800
      TabIndex        =   2
      Top             =   8880
      Width           =   1335
   End
   Begin VB.CommandButton cmdClose 
      Cancel          =   -1  'True
      Caption         =   "&Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   13320
      TabIndex        =   1
      Top             =   8880
      Width           =   1335
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "&Delete"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   3120
      TabIndex        =   0
      Top             =   8880
      Width           =   1335
   End
   Begin VB.Label lblTotalPending 
      Caption         =   "Total Record Pending : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10560
      TabIndex        =   15
      Top             =   600
      Width           =   4215
   End
   Begin VB.Label lblTotalRec 
      Caption         =   "Total Records : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10560
      TabIndex        =   14
      Top             =   240
      Width           =   4095
   End
   Begin VB.Label lblNumMRU 
      Caption         =   "Number of MRUs : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6240
      TabIndex        =   9
      Top             =   600
      Width           =   2295
   End
   Begin VB.Label lblDayNo 
      Caption         =   "Day Number : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6240
      TabIndex        =   8
      Top             =   240
      Width           =   1815
   End
   Begin VB.Label lblCycle 
      Caption         =   "Cycle/Month : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   480
      TabIndex        =   7
      Top             =   600
      Width           =   1575
   End
   Begin VB.Label lblBusCtr 
      Caption         =   "Business Center : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   480
      TabIndex        =   6
      Top             =   240
      Width           =   5535
   End
End
Attribute VB_Name = "frmMRUMngt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim iChecked As Integer
Dim MRUArray(1000) As String
Dim MRUList As String
Dim SRSO As String

Sub UpdateRSOUpload(ByVal sMRU As String, iCycle As Integer)

    Dim strSQL As String

    ' First Update status to "Received From SO" in T_SCHED
    strSQL = "UPDATE T_SCHED SET BKSTCODE = 'RSO' WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
    If Not DBExecute(strSQL, Me.Name, "UpdateRSOUpload") Then
        Rollback_Upload sMRU, iCycle
        Exit Sub
    End If
    
    ' Next place the timestamp in T_BOOK
     strSQL = "UPDATE T_BOOK SET RECV_SO_DT = getdate() WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
     If Not DBExecute(strSQL, Me.Name, "UpdateRSOUpload") Then
        Rollback_Upload sMRU, iCycle
        Exit Sub
    End If
    
End Sub
Sub UpdatePAUUpload(ByVal sMRU As String, iCycle As Integer)

    Dim strSQL As String

    ' First Update status to "Received From SO" in T_SCHED
    strSQL = "UPDATE T_SCHED SET BKSTCODE = 'PAU' WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
    If Not DBExecute(strSQL, Me.Name, "UpdateRSOUpload") Then
        Rollback_Upload sMRU, iCycle
        Exit Sub
    End If
    
    ' Next place the timestamp in T_BOOK
     strSQL = "UPDATE T_BOOK SET RECV_SO_DT = getdate() WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
     If Not DBExecute(strSQL, Me.Name, "UpdateRSOUpload") Then
        Rollback_Upload sMRU, iCycle
        Exit Sub
    End If
    
End Sub


Sub UpdateSSODownload(ByVal sMRU As String, iCycle As Integer)

    Dim strSQL As String

    ' First Update status to "Sent To SO" in T_SCHED
    strSQL = "UPDATE T_SCHED SET BKSTCODE = 'SSO' WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
    If Not DBExecute(strSQL, Me.Name, "UpdateSSODownload") Then Exit Sub
    
    ' Next place the timestamp in T_BOOK
     strSQL = "UPDATE T_BOOK SET UPLD_SO_DT = getdate() WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
     If Not DBExecute(strSQL, Me.Name, "UpdateSSODownload") Then Exit Sub
     
End Sub


Private Sub cmdAdd_Click()
    g_str_AddEdit = "ADD"
    Me.Enabled = False
    
    frmMRUResponse.Show
    frmMRUResponse.txtMRU.SetFocus
    'Me.Enabled = True
   
End Sub

Private Sub cmdCheckAll_Click()
Dim Item As ListItem

For Each Item In lsvMRUMngt.ListItems
    Item.checked = True
Next

End Sub

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub cmdDelete_Click()

    g_str_AddEdit = "DELETE"
    Me.Enabled = False
    
    frmMRUResponse.Show
    
    'Me.Enabled = True

End Sub

Private Sub cmdEdit_Click()

    g_str_AddEdit = "EDIT"
    Me.Enabled = False
    
    frmMRUResponse.Show
    frmMRUResponse.DTPSchedRdg.SetFocus
    'Me.Enabled = True
    
End Sub

Private Sub cmdRSO_Click()
Dim iCycle As Integer
Dim sMRU As String, sBC As String
Dim strSQL As String
Dim sBCPath As String
Dim n As Integer

    SRSO = "RSO"
    If Generate_MRU_List = False Then
        Exit Sub
    End If

    'sMRU = lsvMRUMngt.SelectedItem.Text
    'iCycle = Val(Right(g_BillMonth, 2))
    iCycle = str(intBillCycle)
    
    n = 1
    For n = 1 To iChecked
        sMRU = MRUArray(n)
        sBC = Left$(sMRU, 4)
    
'        strSQL = "SELECT WORKING_DIR FROM R_BUSCTR WHERE BC_CODE = '" & sBC & "'"

        strSQL = "SELECT B.WORKING_DIR FROM T_BOOK A INNER JOIN L_DMZCODE B ON A.DMZCODE_ID = B.DMZCODE_ID WHERE A.BOOKNO = '" & sMRU & "' AND A.CYCLE = " & iCycle
        OpenRecordset g_rs_BUSCTR, strSQL, "frmMRUMngt", "cmdRSO_Click"
        g_rs_BUSCTR.MoveFirst
        sBCPath = g_rs_BUSCTR.Fields(0) & ""

        'If UploadToDB2(sMRU, iCycle, sBCPath) Then
        If UploadToDB(sMRU, iCycle, sBCPath) Then
            'UpdateRSOUpload sMRU, iCycle
'            MsgBox "Receive File from SO Successful!", vbInformation, "Receive File from Satellite Office"
'            ListView_Load
        Else
            MsgBox "Receive File from SO for MRU " & sMRU & " Failed or Aborted!", vbCritical, "Receive File from Satellite Office"
            Exit Sub
        End If
    Next n
    
    ListView_Load
    MsgBox "Receive File from SO Successful!", vbInformation, "Receive File from Satellite Office"

    
End Sub

Private Sub cmdSSO_Click()
Dim iCycle As Integer
Dim sMRU As String
Dim n As Integer

    SRSO = "SSO"
    If Generate_MRU_List = False Then
        Exit Sub
    End If
    
    'sMRU = lsvMRUMngt.SelectedItem.Text
    'iCycle = Val(Right(g_BillMonth, 2))
    iCycle = str(intBillCycle)
    
    n = 1
    For n = 1 To iChecked
         sMRU = MRUArray(n)
         'If CreateDLFile(sMRU, iCycle, "|") Then
         If CreateDLFile(sMRU, iCycle, intDayNo, "|") Then
             UpdateSSODownload sMRU, iCycle
'             MsgBox "Download File Creation Successful!", vbInformation, "Send File to Satellite Office"
'             ListView_Load
         Else
             MsgBox "Download File Creation for MRU " & sMRU & " Failed or Aborted!", vbCritical, "Send File to Satellite Office"
             Exit Sub
         End If
    Next n
    
    ListView_Load
    MsgBox "Download File Creation Successful!", vbInformation, "Send File to Satellite Office"

End Sub

Private Sub cmdUncheckAll_Click()
Dim Item As ListItem

For Each Item In lsvMRUMngt.ListItems
    Item.checked = False
Next
End Sub

Private Sub cmdViewDL_Click()
    g_str_DLUL = "DL"
    sMRU = frmMRUMngt.lsvMRUMngt.SelectedItem.Text
    'intBillCycle = Right(g_BillMonth, 2)
    'Me.Enabled = False
    frmViewDLUL.Show
    
End Sub

Private Sub cmdViewUL_Click()
    g_str_DLUL = "UL"
    sMRU = frmMRUMngt.lsvMRUMngt.SelectedItem.Text
    'intBillCycle = Right(g_BillMonth, 2)
    'Me.Enabled = False
    frmViewDLUL.Show
End Sub

Private Sub Form_Activate()
'frmMRUMngt.WindowState = 2
'ListView_Load
End Sub

Private Sub Form_Load()

If WindowState = vbNormal Then
    Me.Move (0)
    Me.Top = 0
    Me.Height = 10230
    Me.Width = 15270
End If

    lblBusCtr.Caption = "Business Center : " & sBusCenter
    'lblCycle.Caption = "Cycle/Month : " & Right(g_BillMonth, 2)
    lblCycle.Caption = "Cycle/Month : " & str(intBillCycle)
    lblDayNo.Caption = "Day Number : " & str(intDayNo)
    
    'new code total numbers 08-13-2013 jums
    lblTotalRec = "Total Records : " & 0
    lblTotalPending = "Total Record Pending : " & 0
 
    ListView_Load

    If frmMRUGrid.grdMRU.Text <> "" Then
        If lsvMRUMngt.SelectedItem.ListSubItems(1) = "Scheduled Reading Date" Then
            cmdEdit.Enabled = True
            cmdDelete.Enabled = True
        Else
            cmdEdit.Enabled = False
'            cmdDelete.Enabled = False
        End If
    Else
        cmdEdit.Enabled = False
        cmdDelete.Enabled = False
    End If

End Sub

Private Sub ListView_Load()

    Dim l_str_Sql As String
    Dim lBusCenter As String
    'Dim intCycle As Integer
    Dim intNumMRU As Integer
    
    ' Pass the variables from the MRU Scheduler Form
    lBusCenter = Left$(sBusCenter, 3)
    'intCycle = Right(g_BillMonth, 2)
    
    l_str_Sql = "SELECT T_BOOK.BOOKNO, R_BOOKSTAT.BKSTDESC, T_BOOK.SCHED_RDG_DT, T_BOOK.ACTUAL_DL_DT, T_BOOK.UPLD_SO_DT, T_BOOK.RECV_SO_DT, T_BOOK.UPLD_SW_DT " & _
                "FROM R_BOOKSTAT INNER JOIN (T_BOOK INNER JOIN T_SCHED ON (T_BOOK.CYCLE = T_SCHED.CYCLE) AND (T_BOOK.BOOKNO = T_SCHED.BOOKNO)) ON R_BOOKSTAT.BKSTCODE = T_SCHED.BKSTCODE " & _
                "WHERE T_BOOK.DMZCODE_ID = '" & lBusCenter & "' AND T_BOOK.CYCLE = " & intBillCycle & " AND T_SCHED.DAYNO = " & intDayNo
    
    lsvMRUMngt.ListItems.Clear
    If Not OpenRecordset(g_rs_RFF, l_str_Sql, Me.Name, "ListView_Load") Then Exit Sub
    
    g_rs_RFF.MoveFirst
    While Not g_rs_RFF.EOF
       ListViewSet 7
       g_rs_RFF.MoveNext
    Wend

    g_rs_RFF.Close
    Set g_rs_RFF = Nothing
    
    intNumMRU = lsvMRUMngt.ListItems.Count
    
    If intNumMRU > 0 Then
        cmdEdit.Enabled = True
        cmdDelete.Enabled = True
    Else
        intNumMRU = 0
        cmdEdit.Enabled = False
        cmdDelete.Enabled = False
    End If
    
    lblNumMRU.Caption = "Number of MRUs : " & str(intNumMRU)

End Sub

Private Sub ListViewSet(intCols As Integer, Optional intColDate As Integer, Optional intColTime As Integer)
    Dim lobjItem As Object
    Dim l_int_Index As Integer
                
    Set lobjItem = lsvMRUMngt.ListItems.Add(, , CheckNull(g_rs_RFF.Fields(0)))
                    
    For l_int_Index = 1 To intCols - 1
        Select Case l_int_Index
            Case intColDate
                lobjItem.SubItems(l_int_Index) = Format(CheckNull(g_rs_RFF.Fields(l_int_Index)), "mm/dd/yyyy")
            Case intColTime
                lobjItem.SubItems(l_int_Index) = Format(CheckNull(g_rs_RFF.Fields(l_int_Index)), "hh:mm:ss")
            Case Else
                lobjItem.SubItems(l_int_Index) = CheckNull(g_rs_RFF.Fields(l_int_Index))
        End Select
    Next
                    
    Set lobjItem = Nothing

End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmMRUGrid.Enabled = True
End Sub



Private Sub lsvMRUMngt_Click()
'new code total accounts 08-13-2013 jums
Dim TotalRecCount As String
Dim TotalRecQuery As String
Dim TotalRec As ADODB.Recordset

Dim TotalUploadCount As String
Dim TotalUploadQuery As String
Dim TotalUpload As ADODB.Recordset

Dim sSeriesNo As String
'end here new code


If lsvMRUMngt.ListItems.Count > 0 Then
    If lsvMRUMngt.SelectedItem.SubItems(1) = "Downloaded from MWSI" Then
        cmdSSO.Enabled = True
        'cmdRSO.Enabled = False
    ElseIf lsvMRUMngt.SelectedItem.SubItems(1) = "Sent to Satellite" Then
        'cmdSSO.Enabled = False
        cmdRSO.Enabled = True
    Else
        'cmdSSO.Enabled = False
        'cmdRSO.Enabled = False
    End If
    
'    If lsvMRUMngt.SelectedItem.SubItems(2) = "Sent to Satellite" Then
'        cmdSSO.Enabled = False
'        cmdRSO.Enabled = True
'    ElseIf lsvMRUMngt.SelectedItem.SubItems(2) = "Received from Satellite" Then
'        cmdSSO.Enabled = True
'        cmdRSO.Enabled = False
'    Else
'        cmdSSO.Enabled = False
'        cmdRSO.Enabled = False
'    End If
        
    If lsvMRUMngt.SelectedItem.ListSubItems(1) = "Scheduled Reading Date" Then
        cmdEdit.Enabled = True
        cmdDelete.Enabled = True
        cmdViewDL.Enabled = False
        cmdViewUL.Enabled = False
    Else
        cmdViewUL.Enabled = False
        If lsvMRUMngt.SelectedItem.ListSubItems(1) = "Received from Satellite" Or lsvMRUMngt.SelectedItem.ListSubItems(1) = "Forwarded to MWSI" Then
            cmdViewUL.Enabled = True
        End If
        cmdViewDL.Enabled = True
        cmdEdit.Enabled = False
        'cmdDelete.Enabled = False
    End If
End If
    
'new code for the total count 08-13-2013 jums

If lsvMRUMngt.ListItems.Count > 0 Then
    sSeriesNo = lsvMRUMngt.SelectedItem.Text
Else
    MsgBox "There are No Series listed"
End If

If sSeriesNo <> "" Then
TotalRecQuery = "Select accts from t_bkinfo where bookno = '" & sSeriesNo & "' And cycle = '" & Mid(g_BillMonth, 5, 2) & "'"
    If OpenRecordset(TotalRec, TotalRecQuery, "", "") = True Then
        TotalRecCount = TotalRec.Fields(0)
    Else
        TotalRecCount = 0
    End If
Else
    TotalRecCount = 0
End If
    
If sSeriesNo <> "" Then
TotalUploadQuery = "select count(acctnum) from t_upload where seriesno = '" & sSeriesNo & "' And bill_month = '" & Mid(g_BillMonth, 5, 2) & "'"
    If OpenRecordset(TotalUpload, TotalUploadQuery, "", "") = True Then
        TotalUploadCount = TotalUpload.Fields(0)
    Else
        TotalUploadCount = 0
    End If
Else
    TotalUploadCount = 0
End If

lblTotalRec = "Total Records: " & TotalRecCount & " for Series " & sSeriesNo
lblTotalPending = "Total Record Pending: " & TotalRecCount - TotalUploadCount & " for Series " & sSeriesNo

End Sub

Private Sub lsvMRUMngt_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ListviewSort lsvMRUMngt, ColumnHeader.Index
End Sub

Private Function Generate_MRU_List() As Boolean
Dim iCount As Integer
'Dim iChecked As Integer
Dim itm
'Dim MRUArray(1000) As String
Dim n As Integer
Dim iCycle As Integer

    ' Reset MRUList
    MRUList = ""
    iChecked = 0
    
    'iCycle = Val(Right(g_BillMonth, 2))
    iCycle = str(intBillCycle)
    ' Count the number of Checked items in the ListView and build the MRUList from the text items
    iCount = lsvMRUMngt.ListItems.Count
    
    ' If no MRUs were selected, exit function with True, you will be using an "Empty" MRUList!
    If iCount = 0 Then
        Generate_MRU_List = False
        Exit Function
    End If
    
    For Each itm In lsvMRUMngt.ListItems
       With itm
         If .checked Then
            iChecked = iChecked + 1
            ' Add the MRU to the list; if first MRU, no comma in front
            If MRUList <> "" Then
                MRUList = MRUList & ",'" & .Text & "'"
            Else
                MRUList = MRUList & "'" & .Text & "'"
            End If
            MRUArray(iChecked) = itm
         End If
       End With
    Next

    If iChecked = 0 Then
        MsgBox "No MRU selected!", vbCritical, "Manage MRUs"
        Generate_MRU_List = False
        Exit Function
    End If
    
    If SRSO = "SSO" Then
        n = 1
        For n = 1 To iChecked
            ' check if mru stat is RSO
            If Not OpenRecordset(g_rs_TSCHED, "SELECT BOOKNO FROM T_SCHED WHERE BOOKNO = '" & MRUArray(n) & "' and BKSTCODE= 'DLM' and CYCLE= " & iCycle & "", Me.Name, "Generate_MRU_List") Then
                MsgBox "Please uncheck MRU " & MRUArray(n) & ". Can only Send to SO books with status 'Downloaded from MWSI'!", vbCritical, "Manage MRUs"
                'g_rs_TSCHED.Close
                'Set g_rs_TSCHED = Nothing
                Generate_MRU_List = False
                Exit Function
            End If
        Next n
    ElseIf SRSO = "RSO" Then
        n = 1
        For n = 1 To iChecked
            ' check if mru stat is RSO
            If Not OpenRecordset(g_rs_TSCHED, "SELECT BOOKNO FROM T_SCHED WHERE BOOKNO = '" & MRUArray(n) & "' and BKSTCODE IN ('SSO','PAU') and CYCLE= " & iCycle & "", Me.Name, "Generate_MRU_List") Then
                MsgBox "Please uncheck MRU " & MRUArray(n) & ". Can only Receive from SO books with status 'Sent to Satellite Office'!", vbCritical, "Manage MRUs"
                'g_rs_TSCHED.Close
                'Set g_rs_TSCHED = Nothing
                Generate_MRU_List = False
                Exit Function
            End If
        Next n
    End If
    
    ' If ALL MRUs were selected, then do NOT use the list
'    If iCount = iChecked Then
'       Generate_MRU_List = False
'    Else
        Generate_MRU_List = True
'    End If

End Function


