VERSION 5.00
Begin VB.MDIForm MDIMain 
   BackColor       =   &H8000000C&
   ClientHeight    =   10710
   ClientLeft      =   780
   ClientTop       =   840
   ClientWidth     =   14760
   Icon            =   "MDIMain.frx":0000
   LinkTopic       =   "MDIForm1"
   WindowState     =   2  'Maximized
   Begin VB.Menu mnuHost 
      Caption         =   "&Host"
      Begin VB.Menu mnuDL 
         Caption         =   "&Download"
         Begin VB.Menu mnuDnldBatch 
            Caption         =   "&Series from Subic Water"
         End
         Begin VB.Menu mnuDLDisconnect 
            Caption         =   "&Disconnection"
         End
         Begin VB.Menu mnuDLReconnect 
            Caption         =   "&Reconnection"
         End
      End
      Begin VB.Menu mnuCreate 
         Caption         =   "&Create Download Files from Batch"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDwnldManager 
         Caption         =   "Download Manager"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuUL 
         Caption         =   "&Upload"
         Begin VB.Menu mnuUpldBooksToHost 
            Caption         =   "&Series to Subic Water"
         End
         Begin VB.Menu mnuULDCRC 
            Caption         =   "Disconnection"
            Visible         =   0   'False
         End
         Begin VB.Menu mnuRecon 
            Caption         =   "Reconnection"
            Visible         =   0   'False
         End
      End
   End
   Begin VB.Menu mnuMRUMgmnt 
      Caption         =   "&Series Management"
      Begin VB.Menu mnuScheduler 
         Caption         =   "&Scheduler"
         Begin VB.Menu mnuMRU 
            Caption         =   "&Series"
            Begin VB.Menu mnuCurrentCycle 
               Caption         =   "&Current Cycle"
            End
            Begin VB.Menu mnuPreviousCycle 
               Caption         =   "&Previous Cycle"
            End
         End
         Begin VB.Menu mnuDisconSched 
            Caption         =   "&Disconnection"
         End
         Begin VB.Menu mnuReconSched 
            Caption         =   "&Reconnection"
         End
      End
      Begin VB.Menu mnuDisconMgr 
         Caption         =   "Disconnection Manager"
         Visible         =   0   'False
      End
   End
   Begin VB.Menu mnuReports 
      Caption         =   "Re&ports"
      Visible         =   0   'False
      Begin VB.Menu mnuDailyBatchGen 
         Caption         =   "Daily &Batch Generation"
      End
      Begin VB.Menu mnuDownloadedBooks 
         Caption         =   "Do&wnloaded Books"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuMeterRdgSheet 
         Caption         =   "&Meter Reading Sheet"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDetDnldList 
         Caption         =   "&Detailed Download List"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuBookStat 
         Caption         =   "&MRU Status"
      End
      Begin VB.Menu mnuFieldFinding 
         Caption         =   "&MR Notes"
         Begin VB.Menu mnuInactiveAccts 
            Caption         =   "&Inactive Accounts"
            Visible         =   0   'False
         End
         Begin VB.Menu mnuFieldFindings 
            Caption         =   "&Observation Codes"
         End
         Begin VB.Menu mnuOCSummary 
            Caption         =   "Observation Codes &Summary"
         End
         Begin VB.Menu mnuMissCode 
            Caption         =   "&Miss Code"
            Visible         =   0   'False
         End
         Begin VB.Menu mnuOutofRange 
            Caption         =   "&Failed Tolerance Limits"
         End
         Begin VB.Menu mnuFoundConnect 
            Caption         =   "Found &Connected"
         End
         Begin VB.Menu mnuReplacedMtr 
            Caption         =   "Re&placed Meter"
         End
      End
      Begin VB.Menu mnuAccomplishRdg 
         Caption         =   "&Accomplished Meter Reading"
         Begin VB.Menu mnuSummary 
            Caption         =   "&Summary"
         End
         Begin VB.Menu mnuDetailed 
            Caption         =   "De&tailed"
         End
         Begin VB.Menu sep6 
            Caption         =   "-"
            Visible         =   0   'False
         End
      End
      Begin VB.Menu mnuUnmatchedRdg 
         Caption         =   "&Unmatched Readings"
      End
      Begin VB.Menu mnuConsecutiveNoRdg 
         Caption         =   "Consecutive &No Readings"
      End
      Begin VB.Menu mnuNegativeCons 
         Caption         =   "&Negative Consumption"
      End
      Begin VB.Menu mnuForceEdit 
         Caption         =   "Edite&d Readings"
      End
      Begin VB.Menu mnuConsecMonths 
         Caption         =   "Consecutive Months Same &OC"
      End
      Begin VB.Menu mnuBilledReports 
         Caption         =   "&Billing Reports"
      End
      Begin VB.Menu mnuConsumption 
         Caption         =   "&Consumption Report"
      End
   End
   Begin VB.Menu mnuFileMaint 
      Caption         =   "&File Maintenance"
      Begin VB.Menu mnuEditDnldBooks 
         Caption         =   "&Download Books"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuEditUpldBooks 
         Caption         =   "&Upload Books"
         Visible         =   0   'False
      End
      Begin VB.Menu sep9 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFieldFindngCodes 
         Caption         =   "&Field Finding Codes"
      End
      Begin VB.Menu mnuMtrRdr 
         Caption         =   "&Meter Reader"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuRoverFile 
         Caption         =   "&Rover File"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuRange 
         Caption         =   "Ran&ge Table"
      End
      Begin VB.Menu mnuBusCtr 
         Caption         =   "Business Center"
      End
      Begin VB.Menu mnuRatesTable 
         Caption         =   "Ra&tes Table"
         Visible         =   0   'False
      End
      Begin VB.Menu sep10 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFoundConnected 
         Caption         =   "F&ound Connected"
         Visible         =   0   'False
      End
   End
   Begin VB.Menu mnuUtilities 
      Caption         =   "&Utilities"
      Begin VB.Menu mnuSettings 
         Caption         =   "&Settings"
      End
      Begin VB.Menu sep11 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuSParam 
         Caption         =   "&System Parameters"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuInitialize 
         Caption         =   "&Initialize Bill Cycle"
      End
      Begin VB.Menu mnuErrorLogs 
         Caption         =   "&Error Logs"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuPasswordMgmnt 
         Caption         =   "&Password Management"
      End
      Begin VB.Menu mnuDnldProgram 
         Caption         =   "&Download Program to Rover"
         Visible         =   0   'False
         Begin VB.Menu mnuDLProgCable 
            Caption         =   "&Cable - Direct"
         End
         Begin VB.Menu mnuDLProgCradle 
            Caption         =   "Cradle/&IR"
         End
      End
      Begin VB.Menu mnuBilledReports1 
         Caption         =   "&Billing Reports"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuBilledSettings 
         Caption         =   "&Settings"
         Enabled         =   0   'False
         Visible         =   0   'False
      End
   End
   Begin VB.Menu mnuParameter 
      Caption         =   "&Parameter"
      Begin VB.Menu mnuTariff 
         Caption         =   "&Tariff"
      End
      Begin VB.Menu mnuGlobalCharges 
         Caption         =   "&Global Charges"
      End
   End
   Begin VB.Menu mnu_validation 
      Caption         =   "&Validation"
      Begin VB.Menu mnu_ValPRoc 
         Caption         =   "Validate Process"
      End
   End
   Begin VB.Menu mnuabout 
      Caption         =   "&About"
   End
   Begin VB.Menu mnuExit 
      Caption         =   "E&xit"
   End
End
Attribute VB_Name = "MDIMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub MDIForm_Activate()
    Me.Caption = g_AppName & "  - " & g_rs_headername
End Sub

Private Sub MDIForm_Load()
Dim lsql As String
lsql = "SELECT REOPENINGFEE FROM S_PARAM WHERE COMPANY_NAME = 'INDRA'"

If OpenRecordset(g_rs_SPARAM, lsql, "", "") Then
    If g_rs_SPARAM.Fields(0) = "" Then
        g_ReOpeningFee = "457"
    Else
        g_ReOpeningFee = g_rs_SPARAM.Fields(0)
    End If
End If


End Sub

Private Sub MDIForm_Unload(Cancel As Integer)
    If MsgBox("Exit MRU Management System?", vbQuestion + vbYesNo, "Exit MRMS HQ") = vbNo Then
        Cancel = True
    Else
        End
    End If
End Sub

Private Sub mnu_ValPRoc_Click()
    frmdnupVal.Show
End Sub



Private Sub mnuabout_Click()
    frmAbout.Show
End Sub


Private Sub mnuBilledReports_Click()
frmBilledReports.Show

End Sub

Private Sub mnuBilledSettings_Click()
frmBillSettings.Show
End Sub

Private Sub mnuBookStat_Click()
    g_str_rep = "MRU STATUS REPORT.rpt"
    frmReportParm.Show
End Sub

Private Sub mnuConfigFile_Click()
    frmConfig.Show
End Sub

Private Sub mnuBusCtr_Click()
    g_str_Table = "L_DMZCODE"
    frmEditBooks.Show
End Sub

Private Sub mnuConsecMonths_Click()
    g_str_rep = "CONSECUTIVE MONTHS SAME OC REPORT.rpt"
    frmReportParm.Show
End Sub

Private Sub mnuConsecutiveNoRdg_Click()
    g_str_rep = "CONSECUTIVE NO READINGS REPORT.rpt"
    frmReportParm.Show
End Sub

Private Sub mnuConsRep_Click()
    frmConsRep.Show
End Sub

Private Sub mnuConsumption_Click()
    g_str_rep = "CONSUMPTION REPORT.rpt"
    frmConsRep.Show
End Sub


Private Sub mnuCurrentCycle_Click()
    intBillCycle = Right(g_BillMonth, 2)
    mnuPreviousCycle.Enabled = False
    frmMRUGrid.Show
End Sub

Private Sub mnuDailyBatchGen_Click()
    frmDailyBatchGen.Show
End Sub

Private Sub mnuDailyBilledAccts_Click()
frmDailyBilledAccts.Show
End Sub

Private Sub mnuDBF_Click()
frmMonthlyBilled.Show

End Sub

Private Sub mnuDetailed_Click()
    g_str_rep = "DETAILED METER READING REPORT.rpt"
    'frmCRViewer.Show
    frmReportParm.Show
End Sub

Private Sub mnuDetDnldList_Click()
    g_str_rep = "DETAILED DOWNLOAD REPORT.rpt"
    frmCRViewer.Show
'    PrintReport "Detailed Download"
End Sub

Private Sub mnuDetMtrRdng_Click()
g_str_rep = "DETAILED METER READING REPORT.rpt"
frmCRViewer.Show
'    PrintReport "Detailed Meter Reading"
End Sub

Private Sub mnuDetUpldList_Click()
g_str_rep = "DETAILED UPLOAD REPORT.rpt"
frmCRViewer.Show
'    PrintReport "Detailed Upload"
End Sub

Private Sub mnuDisconMgr_Click()
frmDCManager.Show
End Sub

Private Sub mnuDisconSched_Click()
frmDCManager.Show
End Sub

Private Sub mnuDLDisconnect_Click()
Dim sPath As String
Dim cycle As Integer
Dim lsql As String

lsql = "select DIRDLDISCON from S_PARAM where COMPANY_NAME='SUBIC WATER'"

If OpenRecordset(g_rs_SPARAM, lsql, "mnuDLDisconnect", "MRU Management") Then
    If g_rs_SPARAM.Fields(0) = "" Then
        sPath = ""
        MsgBox "Please specify the download directory in the settings sub-menu.", vbExclamation, "Download Error"
        Exit Sub
    Else
        sPath = g_rs_SPARAM.Fields(0)
    End If
End If
    
cycle = CInt(Right$(g_BillMonth, 2))



If LoadDisconToHQ(sPath, cycle) Then
    MsgBox "Download Completed!", vbInformation, "System Message"
End If

End Sub

Private Sub mnuDLProgCable_Click()
    Dim j%
       
    MsgBox "Connect the HHT using the cable and press OK.", vbInformation, "Cable Program Download"
    
     ' Shell HHT Loader Executable
     j% = ShellAndWait(App.Path & "\HHTPROG\dl.bat", vbNormalFocus)
     Screen.MousePointer = vbDefault
     
     If (j% <> 0) Then
        MsgBox "Program Download Terminated Abnormally", vbCritical, "MWSI"
        Exit Sub
     End If
     
     'MsgBox "Program Download Successful!", , "Cable Program Download"
        
End Sub

Private Sub mnuDLProgCradle_Click()
    Dim j%
    
    MsgBox "Plug the HHT into the cradle and press OK.", vbInformation, "Cradle Program Download"
    
     ' Shell HHT Loader Executable
     j% = ShellAndWait(App.Path & "\HHTPROG\IRLoad.exe", vbNormalFocus)
     Screen.MousePointer = vbDefault
     
     If (j% <> 0) Then
        MsgBox "Program Download Terminated Abnormally", vbCritical, "MWSI"
        Exit Sub
     End If
        
End Sub

Private Sub mnuDLReconnect_Click()
Dim sPath As String
Dim cycle As Integer
Dim lsql As String
Dim ExecSql As String

lsql = "select DIRDLRECON from S_PARAM where COMPANY_NAME='SUBIC WATER'"

If OpenRecordset(g_rs_SPARAM, lsql, "mnuDLDisconnect", "MRU Management") Then
    If g_rs_SPARAM.Fields(0) = "" Then
        sPath = ""
        MsgBox "Please specify the download directory in the settings sub-menu.", vbExclamation, "Download Error"
        Exit Sub
    Else
        sPath = g_rs_SPARAM.Fields(0)
    End If
End If
    
cycle = CInt(Right$(g_BillMonth, 2))

If LoadReconToHQ(sPath, cycle) Then
'    ExecSql = "exec sp_INSERT_RECDOWNLOAD " & cycle & ""
'    DBExecute ExecSql, "", ""
    
    MsgBox "Download Completed!", vbInformation, "System Message"
End If


End Sub

Private Sub mnuDnldBatch_Click()
Dim sPath As String
Dim cycle As Integer
Dim lsql As String

lsql = "select DIRDNLD from S_PARAM where COMPANY_NAME='SUBIC WATER'"


If OpenRecordset(g_rs_SPARAM, lsql, "mnuDnldBatch", "MRU Management") Then
    If g_rs_SPARAM.Fields(0) = "" Then
        sPath = ""
        MsgBox "Please specify the download directory in the settings sub-menu.", vbExclamation, "Download Error"
        Exit Sub
    Else
        sPath = g_rs_SPARAM.Fields(0)
    End If
End If
    
cycle = CInt(Right$(g_BillMonth, 2))

If LoadHostToDB(sPath, cycle) Then
    MsgBox "Download Completed!", vbInformation, "System Message"
End If

End Sub

Private Sub mnuDownloadedBookks_Click()

End Sub

Private Sub mnuDnldPrevRdg_Click()

Dim strSQL As String
Dim sMRU As String
Dim iCycle As Integer
Dim bResult
Dim i As Integer

    iCycle = Val(Right(g_BillMonth, 2))
    
    ' Batch Process is assumed to be run daily or every day there is a downloaded MRU from MWSI
    'strSQL = "SELECT BOOKNO FROM T_BOOK WHERE DateValue(ACTUAL_DL_DT) = DateValue(Now)"
    'strSQL = "SELECT BOOKNO FROM T_BOOK WHERE Format(ACTUAL_DL_DT, 'mm/dd/yyyy') = DateValue(Now)"
    strSQL = "SELECT BOOKNO FROM T_BOOK WHERE T_BOOK.CYCLE = " & iCycle & ""
    
    If Not OpenRecordset(g_rs_TBOOK, strSQL, "MDIMain", "Validate Previous Reading") Then
        MsgBox "No MRUs downloaded for the current cycle!", vbInformation, "Validate Previous Reading"
        Exit Sub
    Else
        sMRU = g_rs_TBOOK.Fields(0) & ""
        bResult = Validate_Prev_Rdg(sMRU, iCycle)
        If bResult = False Then
            i = 1
        End If
'        Do While Not g_rs_TBOOK.EOF
'            sMRU = g_rs_TBOOK.Fields(0) & ""
'            'MsgBox sMRU, vbInformation, "MRU to Process"
'            bResult = Validate_Prev_Rdg(sMRU, iCycle)
'            If bResult = False Then
'                i = 1
'            End If
'            g_rs_TBOOK.MoveNext
'        Loop
    End If
    
    If Not bResult Or i = 1 Then
        MsgBox "Mismatched Previous Readings to Last Cycle Current Readings Found!" & vbCrLf & _
               "Please see the Download Errors Report.", vbInformation, "Validate Previous Reading"
    Else
        MsgBox "Validation Successful!", vbInformation, "Validate Previous Reading"
    End If

End Sub

Private Sub mnudnupVal_Click()
frmdnupVal.Show

End Sub

Private Sub mnuDownloadedBooks_Click()
    g_str_rep = "DOWNLOADED BOOKS REPORT.rpt"
    frmCRViewer.Show
End Sub


Private Sub mnuDownloadTables_Click()
    With frmProg
        .Label1.Caption = "Copying"
        .Label2.Caption = "Download Tables"
'        .Label1.Alignment = 1
'        .Label3.Visible = False
        .Label4.Visible = False
        .Label5.Visible = False
        .Show
    End With
End Sub



Private Sub mnuEditDnldBooks_Click()
'    g_str_Table = "T_CUST"
'    frmEditBooks.Show
    
End Sub

Private Sub mnuEditUpldBooks_Click()
'    g_str_Table = "T_UPBOOK"
'    frmEditBooks.Show
    
End Sub

Private Sub mnuErrorLogs_Click()
frmErrorLogs.Show
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuExVal_Click()
Dim SQLstring As String
Dim sysMonth As Integer
Dim sysDate As String

'CHANGE REQUEST Extract accounts from t_upload with the following
'           conditions based on Change Request Dated July 27, 2009
'        -  Accounts with OC 03, 07, 32. These accounts should
'           also have 92 readtag, otherwise, do not include in
'           the extraction process.
'        -  Accounts with OC 29 should be included in the
'           extraction process regardless of the readtag.

sysMonth = CInt(Month(Date))
sysDate = Date

SQLstring = "EXEC sp_EXTRACTACCOUNTS_RDG " & sysMonth & ", '" & sysDate & "'"
DBExecute SQLstring, "", ""

MsgBox "Extraction of Accounts Done!", vbInformation


End Sub

Private Sub mnuFieldFindings_Click()
    g_str_rep = "OBSERVATION CODES REPORT.rpt"
    frmReportParm.Show
End Sub

Private Sub mnuFieldFindngCodes_Click()
    g_str_Table = "L_FFCODE"
    frmEditBooks.Show
    
End Sub

Private Sub mnuForceEdit_Click()
    g_str_rep = "EDITED READING REPORT.rpt"
    frmReportParm.Show
End Sub

Private Sub mnuFoundConnect_Click()
    g_str_rep = "FOUND CONNECTED REPORT.rpt"
    frmReportParm.Show
End Sub

Private Sub mnuFoundConnected_Click()
    g_str_Table = "T_FCONN"
    frmEditBooks.Show
    
End Sub

Private Sub mnuGlobalCharges_Click()
    frmGlobalCharges.Show
End Sub

Private Sub mnuInactiveAccts_Click()
g_str_rep = "INACTIVE ACCOUNTS REPORT.rpt"
frmCRViewer.Show
'    PrintReport "Inactive Accounts"
End Sub

Private Sub mnuInitialize_Click()
    frmInitBillCycle.Show
End Sub

Private Sub mnuItinerary_Click()
    g_str_rep = "ITINERARY REMARKS.rpt"
    frmCRViewer.Show
End Sub

Private Sub mnuMeterInquire_Click()
frmMeterInquire.Show

End Sub

Private Sub mnuMeterRdgSheet_Click()
    g_str_rep = "METER READING SHEET.rpt"
    frmCRViewer.Show
End Sub

Private Sub mnuMissCode_Click()
'    PrintReport "Miss Code"
g_str_rep = "MISS CODE REPORT.rpt"
frmCRViewer.Show
End Sub

Private Sub mnuMRUInquire_Click()
frmInquiry.Show
End Sub

Private Sub mnuMtrRdr_Click()
    g_str_Table = "R_READER"
    frmEditBooks.Show
    
End Sub

Private Sub mnuNegativeCons_Click()
    g_str_rep = "NEGATIVE CONSUMPTION REPORT.rpt"
    frmReportParm.Show
End Sub

Private Sub mnuNewAccounts_Click()
   Dim sqlAveConsNew As String
   Dim iCycle As Integer
   iCycle = Val(Right(g_BillMonth, 2))
   sqlAveConsNew = "EXEC sp_COMPUTE_AVECONS_NEW " & iCycle
   DBExecute sqlAveConsNew, "", ""
   MsgBox "New accounts added to Masterlist", vbInformation, "CFSHQ"
End Sub

Private Sub mnuOCSummary_Click()
    g_str_rep = "FIELD FINDINGS SUMMARY REPORT.rpt"
    frmReportParm.Show
End Sub

Private Sub mnuOutofRange_Click()
    g_str_rep = "FAILED TOLERANCE LIMIT REPORT.rpt"
    frmReportParm.Show
End Sub

Private Sub mnuPasswordMgmnt_Click()
    frmPWMgmt.Show
End Sub

Private Sub mnuPostUpldBk_Click()
    frmBookStat.Caption = "Post Uploaded Books"
    frmBookStat.Show
End Sub

Private Sub mnuPostMaster_Click()

Dim nowDate As String
Dim nowMonth As String
Dim nowDay As String
Dim iCycle As String
Dim SQLstring As String

    '******* ARRANGE DATE PARAMETER FOR STORED PROCEDURE ************
    If Len(Month(Now)) = 1 Then
        nowMonth = "0" & Month(Now)
    Else
        nowMonth = Month(Now)
    End If
    
    If Len(Day(Now)) = 1 Then
        nowDay = "0" & Day(Now)
    Else
        nowDay = Day(Now)
    End If
    
    
    nowDate = Year(Now) & nowMonth & nowDay
    iCycle = Val(Right(g_BillMonth, 2))
    
        SQLstring = "EXEC sp_T_CUST_MASTER_POSTING '" & nowDate & "'," & iCycle
    
    If DBExecute(SQLstring, "", "") Then
        MsgBox "Posting to MasterList Successful", vbInformation, "CFSHQ"
    End If




End Sub

Private Sub mnuPreviousCycle_Click()
    intBillCycle = Right(g_BillMonth, 2)
    
    intBillCycle = intBillCycle - 1
    If intBillCycle = 0 Then intBillCycle = 12
    
    mnuCurrentCycle.Enabled = False
    frmMRUGrid.Show
End Sub

Private Sub mnuRange_Click()
    g_str_Table = "L_RANGECODE"
    frmEditBooks.Show
End Sub

Private Sub mnuRatesTable_Click()
    g_str_Table = "R_RATE"
    frmEditBooks.Show

End Sub

Private Sub mnuReplacedMeter_Click()
    g_str_rep = "REPLACED METER REPORT.rpt"
    frmCRViewer.Show
End Sub

Private Sub mnuReconBayadCtr_Click()

    Const Filler4 = "    "
    Const Filler40 = "                                        "
    Const mtrrdrcode = "SZP"

    Dim cn As Connection, rs As Recordset
    Dim UFile As String, fcfile As String
    Dim sPath As String
    Dim fs As Object
    Dim a, sval, mon$
    Dim dir As String
    Dim f As Object
    Dim lsql As String
    'Data Fields for host upload file
    Dim cac As String
    Dim billamt As String
    Dim iCycle As Integer
    Dim CDelimiter As String
    'End Fields for host upload file

'On Error GoTo errhandler

    Set cn = CreateObject("ADODB.Connection")
    Set rs = CreateObject("ADODB.recordset")

    cn.ConnectionString = "Provider=SQLOLEDB.1; Data Source=" & g_DataSource & ";database=" & g_Database & "; Initial Catalog=;User Id=sa;Password=sqladmin;"
    cn.Open

    Set f = CreateObject("Scripting.FileSystemObject")
    lsql = "select DIRULDISCON from S_PARAM where COMPANY_NAME='INDRA'"
    If OpenRecordset(g_rs_SPARAM, lsql, "", "") Then
        'dir = g_rs_SPARAM.Fields(0).Value & "\" & Format(Date, "dd") & "\"
        If Not f.FolderExists(g_rs_SPARAM.Fields(0).Value & "\current\") Then f.CreateFolder (g_rs_SPARAM.Fields(0).Value & "\current\") ' upload directory
        dir = g_rs_SPARAM.Fields(0).Value & "\current\" & Format(Date, "dd") & "\"
        If Not f.FolderExists(dir) Then f.CreateFolder (dir) ' upload directory
    Else
        dir = ""
    End If

    iCycle = CInt(Right(g_BillMonth, 2))
    mon$ = Padl(iCycle, 2, "0") 'Format(Date, "MM")

    UFile = "MWSIN" & Format(Date, "yyyymmdd") 'mon$ & Format(Date, "dd")
    sPath = dir & UFile & ".UPLD"
    
    CDelimiter = "|"
    Set fs = CreateObject("Scripting.FileSystemObject")
    Set a = fs.CreateTextFile(sPath, True) '--create a textfile
    a.Close

    Open sPath For Output As #1  '--open txtfile for data insert

    rs.Open "SELECT ACCTNUM, BILLAMT From T_CUST_MASTER WHERE STATUS='DISCONN' AND DCDATE IS NOT NULL", cn, adOpenStatic, adLockReadOnly
    ' Create host upload file - accomplished readings
    Do While Not rs.EOF
    
        '--assign values to variables
        cac = Trim(rs.Fields("ACCTNUM"))
        billamt = IIf(IsNull(rs.Fields("BILLAMT").Value) = True, "0", Trim(rs.Fields("BILLAMT")))
        '--end assign values to variables

    '--insert data to a pipe delimited (fixed length) text file
    Print #1, UFile & CDelimiter & cac & CDelimiter & billamt & CDelimiter & "" & CDelimiter & "" & CDelimiter & _
    "" & CDelimiter & "" & CDelimiter & "" & CDelimiter & ""

        rs.MoveNext
    Loop

    rs.Close
    cn.Close

    Set rs = Nothing
    Set cn = Nothing

    Close #1 '--close txtfile for itinerary remarks
MsgBox "Upload Successful!", vbInformation, ""

errhandler:
If Err.Number <> 0 Then
    MsgBox Err.Description, , "Error"
End If
End Sub

Private Sub mnuReconSched_Click()
    
    frmRCManager.Show
End Sub

Private Sub mnuReplacedMtr_Click()
    g_str_rep = "REPLACED METER REPORT.rpt"
    'frmCRViewer.Show
    frmReportParm.Show
End Sub

Private Sub mnuRoverFile_Click()
    g_str_Table = "F_HH"
    frmEditBooks.Show
End Sub

Private Sub mnuSchedule_Click()
    g_str_rep = "SCHEDULE.rpt"
    frmCRViewer.Show
    '    PrintReport "Schedule"
End Sub



Private Sub mnuSettings_Click()
    frmSettings.Show
End Sub

Private Sub mnuSParam_Click()
    frmConfig.Show
End Sub

Private Sub mnuSummMtrRdng_Click()
g_str_rep = "SUMMARY METER READING REPORT.rpt"
frmCRViewer.Show
'    PrintReport "Summary Meter Reading"
End Sub



Private Sub mnuTimeRead_Click()
g_str_rep = "TIME READ REPORT.rpt"
frmCRViewer.Show
'    PrintReport "Time Read"
End Sub

Private Sub mnuSummary_Click()
    g_str_rep = "SUMMARY METER READING REPORT.rpt"
    'frmCRViewer.Show
    frmReportParm.Show
End Sub

Private Sub mnuTariff_Click()
    frmTariffSched.Show
End Sub

Private Sub mnuULDCRC_Click()

    Const Filler4 = "    "
    Const Filler40 = "                                        "
    Const mtrrdrcode = "SZP"

    Dim cn As Connection, rs As Recordset
    Dim UFile As String, fcfile As String
    Dim sPath As String
    Dim fs As Object
    Dim a, sval, mon$
    Dim dir As String
    Dim f As Object
    Dim lsql As String
    'Data Fields for host upload file
    Dim cac As String
    Dim stat_cd As String
    Dim dt As String
    Dim rdg As String
    Dim sealno As String
    Dim act_meter As String
    Dim disc_reason As String
    Dim reason_cd As String
    Dim resp_person As String
    Dim REMARKS As String
    Dim iCycle As Integer
    Dim CDelimiter As String
    'End Fields for host upload file

'On Error GoTo errhandler

    Set cn = CreateObject("ADODB.Connection")
    Set rs = CreateObject("ADODB.recordset")

    cn.ConnectionString = "Provider=SQLOLEDB.1; Data Source=" & g_DataSource & ";database=" & g_Database & "; Initial Catalog=;User Id=sa;Password=sqladmin;"
    cn.Open

    Set f = CreateObject("Scripting.FileSystemObject")
    lsql = "select DIRULDISCON from S_PARAM where COMPANY_NAME='INDRA'"
    If OpenRecordset(g_rs_SPARAM, lsql, "", "") Then

        If Not f.FolderExists(g_rs_SPARAM.Fields(0).Value & "\current\") Then f.CreateFolder (g_rs_SPARAM.Fields(0).Value & "\current\") ' upload directory
        dir = g_rs_SPARAM.Fields(0).Value & "\current\" & Format(Date, "dd") & "\"
        If Not f.FolderExists(dir) Then f.CreateFolder (dir) ' upload directory
    Else
        dir = ""
    End If

    iCycle = CInt(Right(g_BillMonth, 2))
    mon$ = Padl(iCycle, 2, "0") 'Format(Date, "MM")

    UFile = "Dun_" & Format(Date, "yyyymmdd") 'mon$ & Format(Date, "dd")
    sPath = dir & UFile & ".TXT"
    
    CDelimiter = "|"
    Set fs = CreateObject("Scripting.FileSystemObject")
    Set a = fs.CreateTextFile(sPath, True) '--create a textfile
    a.Close

    Open sPath For Output As #1  '--open txtfile for data insert


Dim SQLstring1 As String
SQLstring1 = "EXEC sp_CREATE_UPLOAD_DISCONRECON ' " & Now() & "'"
DBExecute SQLstring1, "", ""
    rs.Open "SELECT * FROM T_DCRC_UPLOAD", cn, adOpenStatic, adLockReadOnly
    ' Create host upload file - accomplished readings
    Do While Not rs.EOF
    
        '--assign values to variables
        cac = Trim(rs.Fields("ACCTNUM"))
        stat_cd = Trim(rs.Fields("STATUS"))
        'dt = Format(Date, "YYYYMMDD") 'Right(Trim(rs.Fields("cdate")), 4) & Left(Trim(rs.Fields("cdate")), 2) & Mid(Trim(rs.Fields("cdate")), 4, 2)
        dt = Right(Trim(rs.Fields("DCRCDATE")), 4) & Left(Trim(rs.Fields("DCRCDATE")), 2) & Mid(Trim(rs.Fields("DCRCDATE")), 4, 2)
        rdg = Trim(rs.Fields("reading"))
        sealno = Trim(rs.Fields("SEALNO"))
        act_meter = IIf(IsNull(rs.Fields("METERNO").Value) = True, "0", Trim(rs.Fields("METERNO")))
        disc_reason = IIf(IsNull(Trim(rs.Fields("DISC_REASON").Value)) = True, "", rs.Fields("DISC_REASON"))
        reason_cd = Trim(rs.Fields("NDCR_REASON"))
        resp_person = Trim(rs.Fields("RESP_PERSON"))
        REMARKS = Trim(rs.Fields("REMARKS"))
        '--end assign values to variables

    '--insert data to a pipe delimited (fixed length) text file
    Print #1, cac & CDelimiter & stat_cd & CDelimiter & dt & CDelimiter & rdg & CDelimiter & sealno & CDelimiter & _
    act_meter & CDelimiter & disc_reason & CDelimiter & reason_cd & CDelimiter & resp_person & CDelimiter & _
    REMARKS




        rs.MoveNext
    Loop

    rs.Close
    cn.Close

    Set rs = Nothing
    Set cn = Nothing

    Close #1 '--close txtfile for itinerary remarks
MsgBox "Upload Successful!", vbInformation, ""

errhandler:
If Err.Number <> 0 Then
    MsgBox Err.Description, , "Error"
End If
End Sub

Private Sub mnuUnmatchedRdg_Click()
    g_str_rep = "UNMATCHED READINGS REPORT.rpt"
    frmReportParm.Show
End Sub

Private Sub mnuUpldBooksToHost_Click()
    frmBookStat.Show

End Sub



Private Sub mnuUpldMatchDnld_Click()

Dim strSQL As String
Dim SQLstring As String
Dim sMRU As String
Dim iCycle As Integer
Dim bResult
Dim i As Integer
Dim rs_Validation As ADODB.Recordset
Dim msgAccountNo As String
Dim msgDocNo As String
Dim msgYear As String
Dim msgMonth As String
Dim msgDay As String
Dim ErrorString As String
Dim ErrorLogFilename As String
Dim DrvPath As String
Dim fso As Object
Dim procDate As Date
   
Dim fs As FileSystemObject
Dim ts As TextStream
Set fs = New FileSystemObject
Set fso = CreateObject("Scripting.FileSystemObject")
    
msgAccountNo = "number of accounts do not match"
msgDocNo = "document numbers do not match"
msgYear = "reading year not equal to current year"
msgMonth = "reading month not equal to current cycle"
msgDay = "reading day not equal to current day"

procDate = Now
iCycle = Val(Right(g_BillMonth, 2))
    
    strSQL = "SELECT T_BOOK.BOOKNO " & _
             "FROM T_BOOK INNER JOIN T_SCHED ON (T_BOOK.BOOKNO = T_SCHED.BOOKNO) AND (T_BOOK.CYCLE = T_SCHED.CYCLE) " & _
             "WHERE convert(char(12),T_BOOK.RECV_SO_DT, 110) = convert(char(12),getdate(), 110) and (T_SCHED.BKSTCODE='RSO' or T_SCHED.BKSTCODE='FWM')"

    If Not OpenRecordset(g_rs_TBOOK, strSQL, "MDIMain", "Match Document Number") Then
        MsgBox "No MRUs downloaded for the current cycle!", vbInformation, "Match Document Number"
        Exit Sub
    End If

    g_rs_TBOOK.Close
    Set g_rs_TBOOK = Nothing
    
SQLstring = "EXEC sp_MATCH_DOWNLOAD_UPLOAD " & CInt(iCycle) & ",'" & procDate & "'"
DBExecute SQLstring, "", ""


DrvPath = Mid(App.Path, 1, 3)
'check folder if exists then create folders

    If fso.FolderExists(DrvPath & "Validation Logs") = False Then
    fso.CreateFolder (DrvPath & "Validation Logs")
    End If
    
    
    If fso.FolderExists(DrvPath & "Validation Logs\" & Year(Now)) = False Then
    fso.CreateFolder (DrvPath & "Validation Logs\" & Year(Now))
    End If
    
    If fso.FolderExists(DrvPath & "Validation Logs\" & Year(Now) & "\" & GetMonth()) = False Then
    fso.CreateFolder (DrvPath & "Validation Logs\" & Year(Now) & "\" & GetMonth())
    End If
    

ErrorLogFilename = DrvPath & "Validation Logs\" & Year(Now) & "\" & GetMonth() & "\Log_" & Day(Now) & ".txt"
If OpenRecordset(rs_Validation, "SELECT * FROM dbo.T_CHKUPLOAD", "", "") Then
    rs_Validation.MoveFirst
    '--create log file
    Set ts = fs.OpenTextFile(ErrorLogFilename, ForWriting, True)
    While Not rs_Validation.EOF
       
       Select Case rs_Validation.Fields(2)
            Case "COUNT"
                ErrorString = rs_Validation.Fields(0) & " " & rs_Validation.Fields(1) & " - " & msgAccountNo
            Case "DOCUMENT"
                ErrorString = rs_Validation.Fields(0) & " " & rs_Validation.Fields(1) & " - " & msgDocNo
            Case "YEAR"
                ErrorString = rs_Validation.Fields(0) & " " & rs_Validation.Fields(1) & " - " & msgYear
            Case "MONTH"
                ErrorString = rs_Validation.Fields(0) & " " & rs_Validation.Fields(1) & " - " & msgMonth
            Case "DAY"
                ErrorString = rs_Validation.Fields(0) & " " & rs_Validation.Fields(1) & " - " & msgDay
        End Select
        
        ts.WriteLine ErrorString
        'MsgBox ErrorString
    rs_Validation.MoveNext
    Wend

ts.Close
MsgBox "Errors found in Validation Process: Kindly see Log File for details", vbCritical

Else
'--no errors found
MsgBox "No Errors found in Validation Process"
End If


'end of change request - JSE

    
End Sub


Function GetMonth() As String

Select Case Month(Now)
    Case "1"
        GetMonth = "01 January"
    Case "2"
        GetMonth = "02 February"
    Case "3"
        GetMonth = "03 March"
    Case "4"
        GetMonth = "04 April"
    Case "5"
        GetMonth = "05 May"
    Case "6"
        GetMonth = "06 June"
    Case "7"
        GetMonth = "07 July"
    Case "8"
        GetMonth = "08 August"
    Case "9"
        GetMonth = "09 September"
    Case "10"
        GetMonth = "10 October"
    Case "11"
        GetMonth = "11 November"
    Case "12"
        GetMonth = "12 December"
End Select

End Function

Private Sub mnuValRep_Click()
frmValReports.Show
End Sub

Private Sub mnuValSet_Click()
frmValSettings.Show

End Sub


Private Sub mnuVerified_Click()
frmVerified.Show

End Sub

Private Sub mnuvaloc_Click()
    g_str_rep = "OC SUMMARY VALIDATION REPORT.rpt"
    frmReportParm.Show
End Sub

Private Sub mnuValRepAdditional_Click()
    frmValReportsAdditional.Show
End Sub

Private Sub mnuValRepOC_Click()
    frmValReports.Show
End Sub

Private Sub mnuValRepRdng_Click()
    frmValReportsRdg.Show
End Sub

Private Sub mnuValSettings_Click()
frmValSettings.Show
End Sub

Private Sub mnuVAOC_Click()
frmValAnalysis.Show
End Sub

Private Sub mnuVaReading_Click()
frmValAnalysisRdg.Show
End Sub

Private Sub mnuVerReadings_Click()
frmVerified.Show
End Sub


