VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmEditBooks 
   Caption         =   "Edit Books"
   ClientHeight    =   6090
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4995
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6090
   ScaleWidth      =   4995
   Begin VB.CommandButton cmdAdd 
      Caption         =   "&Add"
      Height          =   450
      Left            =   210
      TabIndex        =   7
      Top             =   5265
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.CommandButton cmdEdit 
      Caption         =   "&Edit"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   450
      Left            =   1125
      TabIndex        =   6
      Top             =   5280
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.CommandButton cmdClose 
      Cancel          =   -1  'True
      Caption         =   "&Close"
      Height          =   450
      Left            =   3870
      TabIndex        =   5
      Top             =   5265
      Width           =   915
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "&Delete"
      Enabled         =   0   'False
      Height          =   450
      Left            =   2040
      TabIndex        =   4
      Top             =   5280
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.CommandButton cmdGenerate 
      Caption         =   "&Generate File"
      Height          =   450
      Left            =   2950
      TabIndex        =   3
      Top             =   5280
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.CommandButton cmdReplMtr 
      Caption         =   "&Repl Mtr"
      Enabled         =   0   'False
      Height          =   390
      Left            =   2400
      TabIndex        =   2
      Top             =   6360
      Width           =   915
   End
   Begin VB.Frame fraSelect 
      Caption         =   " Select Download Book "
      Height          =   5025
      Left            =   60
      TabIndex        =   1
      Top             =   60
      Width           =   4840
      Begin MSComctlLib.ListView lsvFileMain 
         Height          =   4605
         Left            =   120
         TabIndex        =   0
         Top             =   255
         Width           =   4600
         _ExtentX        =   8123
         _ExtentY        =   8123
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   30
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Code"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Description"
            Object.Width           =   2999
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Basic Rate"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Other Rate 1"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Other Rate 2"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Other Rate 3"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Other Rate 4"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Demand Rate"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "Min Demand"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Text            =   "Min Demand Charge"
            Object.Width           =   2999
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Text            =   "Min Consumption"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Text            =   "Min Charge"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Text            =   "Limit 1"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Text            =   "Rate Limit 1"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Text            =   "Limit 2"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   15
            Text            =   "Rate Limit 2"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   16
            Text            =   "Limit 3"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   17
            Text            =   "Rate Limit 3"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   18
            Text            =   "Limit 4"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(20) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   19
            Text            =   "Rate Limit 4"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(21) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   20
            Text            =   "Limit 5"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(22) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   21
            Text            =   "Rate Limit 5"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(23) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   22
            Text            =   "Limit 6"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(24) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   23
            Text            =   "Rate Limit 6"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(25) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   24
            Text            =   "Limit 7"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(26) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   25
            Text            =   "Rate Limit 7"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(27) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   26
            Text            =   "Limit 8"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(28) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   27
            Text            =   "Rate Limit 8"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(29) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   28
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(30) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   29
            Object.Width           =   2540
         EndProperty
      End
   End
End
Attribute VB_Name = "frmEditBooks"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAdd_Click()
    g_str_AddEdit = "ADD"
    Me.Enabled = False
    
    Select Case g_str_Table
        Case "L_FFCODE"
            frmAddRoverFile.Show
        Case "R_RANGE"
            frmAddRoverFile.Show
        Case "R_READER"
            frmAddRoverFile.Show
        Case "F_HH"
            frmAddRoverFile.Show
        Case "R_BUSCTR"
            frmAddRoverFile.Show
        Case "R_RATE"
            frmRatesCode.Show
    End Select
    
End Sub

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub cmdDelete_Click()
    Dim l_str_Sql As String
    Dim lsql As String
    
    Select Case g_str_Table
        Case "L_FFCODE"
            If OpenRecordset(g_rs_TDOWNLOAD, "SELECT FF1 FROM T_UPLOAD where FFCODE='" & lsvFileMain.SelectedItem & "'", Me.Name, "cmdDelete") Then
                MsgBox "The record cannot be deleted.", vbExclamation, "Delete Record"
                Exit Sub
            Else
                If Not ListViewDelete(lsvFileMain, 1) Then Exit Sub
                l_str_Sql = "DELETE FROM R_FF WHERE FFCODE = '" & lsvFileMain.SelectedItem.Text & "'"
            End If

        Case "R_RANGE"
            If OpenRecordset(g_rs_TUPLOAD, "SELECT RANGECODE FROM T_UPLOAD where RANGECODE='" & lsvFileMain.SelectedItem & "'", Me.Name, "cmdDelete") Then
                MsgBox "The record cannot be deleted.", vbExclamation, "Delete Record"
                Exit Sub
            Else
                If Not ListViewDelete(lsvFileMain, 1) Then Exit Sub
                l_str_Sql = "DELETE FROM R_RANGE WHERE RANGECODE = '" & lsvFileMain.SelectedItem.Text & "'"
            End If

        Case "R_READER"
            lsql = "select READERNO from T_SCHED where READERNO = '" & lsvFileMain.SelectedItem.Text & "'"
            If OpenRecordset(g_rs_TSCHED, lsql, "frmEditBooks", "MRU Management") Then
                MsgBox "The record cannot be deleted. Delete record with reader name " & lsvFileMain.SelectedItem.SubItems(1) & " from the Scheduler first before deleting in the File Maintenance.", vbExclamation, "Delete Record"
                Exit Sub
            Else
                If Not ListViewDelete(lsvFileMain, 1) Then Exit Sub
                l_str_Sql = "DELETE FROM R_READER WHERE READERNO = '" & lsvFileMain.SelectedItem.Text & "'"
            End If
        Case "F_HH"
            lsql = "select HHTNO from T_SCHED where HHTNO = '" & lsvFileMain.SelectedItem.Text & "'"
            If OpenRecordset(g_rs_TSCHED, lsql, "frmEditBooks", "MRU Management") Then
                MsgBox "The record cannot be deleted. Delete record with rover no. " & lsvFileMain.SelectedItem.Text & " from the Scheduler first before deleting in the File Maintenance.", vbExclamation, "Delete Record"
                Exit Sub
            Else
                If Not ListViewDelete(lsvFileMain, 1) Then Exit Sub
                l_str_Sql = "DELETE FROM F_HH WHERE HHTNO = '" & lsvFileMain.SelectedItem.Text & "'"
            End If
        Case "R_BUSCTR"
            If OpenRecordset(g_rs_TBOOK, "SELECT BC_CODE FROM T_BOOK where BC_CODE='" & lsvFileMain.SelectedItem & "'", Me.Name, "cmdSave") Then
                MsgBox "The record cannot be deleted. Delete record with BC CODE " & lsvFileMain.SelectedItem.Text & " from the Scheduler first before deleting in the File Maintenance.", vbExclamation, "Delete Record"
                Exit Sub
            Else
                If Not ListViewDelete(lsvFileMain, 1) Then Exit Sub
                l_str_Sql = "DELETE FROM R_BUSCTR WHERE BC_CODE = '" & lsvFileMain.SelectedItem.Text & "'"
            End If

        Case "R_RATE"
            If Not ListViewDelete(lsvFileMain, 1) Then Exit Sub
            l_str_Sql = "DELETE FROM R_RATE WHERE RATECODE = '" & lsvFileMain.SelectedItem.Text & "'"
        Case "T_FCONN"
            If Not ListViewDelete(lsvFileMain, 1, 2) Then Exit Sub
            l_str_Sql = "DELETE FROM T_FCONN WHERE BOOKNO = '" & lsvFileMain.SelectedItem.Text & _
                "' AND MTRNO = '" & lsvFileMain.SelectedItem.SubItems(1) & "'"
                '"' AND ACCTNUM = '" & lsvFileMain.SelectedItem.SubItems(2) & "'"
    End Select
    
'    If g_str_Table = "T_CUST" Then
'        If Not DeleteDnldBk Then Exit Sub
'    Else
        If Not DBExecute(l_str_Sql, Me.Name, "cmdDelete_Click") Then Exit Sub
'    End If
    
    lsvFileMain.ListItems.Remove (lsvFileMain.SelectedItem.Index)
    lsvFileMain.Refresh
    If lsvFileMain.ListItems.Count < 1 Then
        cmdEdit.Enabled = False
        cmdDelete.Enabled = False
        cmdReplMtr.Enabled = False
    End If
    lsvFileMain.SetFocus

End Sub

Private Sub cmdEdit_Click()
Dim lsql As String

    g_str_AddEdit = "EDIT"
    Me.Enabled = False
    
    Select Case g_str_Table
        Case "L_FFCODE"
            frmAddRoverFile.Show
        Case "R_RANGE"
            frmAddRoverFile.Show
        Case "R_READER"
            lsql = "select READERNO from T_SCHED where READERNO = '" & lsvFileMain.SelectedItem.Text & "'"
            If OpenRecordset(g_rs_TSCHED, lsql, "frmEditBooks", "MRU Management") Then
                Me.Enabled = True
                MsgBox "The record cannot be changed. Delete record with reader name " & lsvFileMain.SelectedItem.SubItems(1) & " from the Scheduler first before modifying in the File Maintenance.", vbExclamation, "Edit Record"
                Exit Sub
            Else
                frmAddRoverFile.Show
            End If
        Case "F_HH"
            lsql = "select HHTNO from T_SCHED where HHTNO = '" & lsvFileMain.SelectedItem.Text & "'"
            If OpenRecordset(g_rs_TSCHED, lsql, "frmEditBooks", "MRU Management") Then
                Me.Enabled = True
                MsgBox "The record cannot be changed. Delete record with HHT No. " & lsvFileMain.SelectedItem.Text & " from the Scheduler first before modifying in the File Maintenance.", vbExclamation, "Edit Record"
                Exit Sub
            Else
                frmAddRoverFile.Show
            End If
        Case "R_BUSCTR"
            frmAddRoverFile.Show
        Case "R_RATE"
            frmRatesCode.Show
        Case "T_FCONN"
            frmFoundConn.Show
    End Select
    
End Sub

Private Sub cmdGenerate_Click()

    Select Case g_str_Table
        Case "L_FFCODE"
'            If GenerateFF = True Then
'                MsgBox "Field Finding file successfully generated"
'            Else
'                MsgBox "File generation failed."
'            End If
        Case "R_RANGE"
            If GenerateRange = True Then
                MsgBox "Range file successfully generated"
            Else
                MsgBox "File generation failed."
            End If
        Case "R_RATE"
            If GenerateRate = True Then
                MsgBox "Rate file successfully generated"
            Else
                MsgBox "File generation failed."
            End If
    End Select

End Sub

Private Sub Form_Load()
    Dim l_int_Index As Integer

If WindowState = vbNormal Then
    Me.Move (1000)
    Me.Top = 500
End If

    With lsvFileMain.ColumnHeaders
        Select Case g_str_Table
             Case "L_FFCODE"
                Form_Setup "Field Finding Table", " Select Field Finding ", True, 3860, 3
                .Item(1).Width = 1000
                .Item(2).Width = 2500
                .Item(1).Text = "Code"
                .Item(2).Text = "Description"
            
             Case "L_RANGECODE"
                Form_Setup "Range Table", " Select Range ", True, 3860, 3
                .Item(1).Width = 1000
                .Item(2).Width = 2500
                .Item(1).Text = "Code"
                .Item(2).Text = "Description"

            Case "R_READER"
                Form_Setup "Meter Reader Table", " Select Meter Reader ", True, 4400, 3
                .Item(1).Width = 1500
                .Item(2).Width = 2500
                .Item(1).Text = "Reader No."
                .Item(2).Text = "Meter Reader Name"

            Case "F_HH"
                Form_Setup "Rover ID Table", " Select Rover ID ", True, 4400, 4
                .Item(1).Width = 1500
                .Item(2).Width = 1000
                .Item(3).Width = 1500
                .Item(1).Text = "Rover No."
                .Item(2).Text = "Rover ID"
                .Item(3).Text = "Date Acquired"

            Case "R_RATE"
                Form_Setup "Rates Table", " Select Rate ", True, 9380, 29
                .Item(1).Width = 700
                .Item(2).Width = 1700
                .Item(3).Width = 1100
                .Item(4).Width = 1200
                .Item(5).Width = 1200
                .Item(6).Width = 1200
                .Item(7).Width = 1200
                .Item(8).Width = 1300
                .Item(9).Width = 1200
                .Item(10).Width = 1700
                .Item(11).Width = 1500
                .Item(12).Width = 1100
                .Item(13).Width = 1000
                .Item(14).Width = 1200
                .Item(15).Width = 1000
                .Item(16).Width = 1200
                .Item(17).Width = 1000
                .Item(18).Width = 1200
                .Item(19).Width = 1000
                .Item(20).Width = 1200
                .Item(21).Width = 1000
                .Item(22).Width = 1200
                .Item(23).Width = 1000
                .Item(24).Width = 1200
                .Item(25).Width = 1000
                .Item(26).Width = 1200
                .Item(27).Width = 1000
                .Item(28).Width = 1200
                .Item(1).Text = "Code"
                .Item(2).Text = "Description"
                .Item(3).Text = "Basic Rate"
                .Item(4).Text = "Other Rate 1"
                .Item(5).Text = "Other Rate 2"
                .Item(6).Text = "Other Rate 3"
                .Item(7).Text = "Other Rate 4"
                .Item(8).Text = "Demand Rate"
                .Item(9).Text = "Min Demand"
                .Item(10).Text = "Min Demand Charge"
                .Item(11).Text = "Min Consumption"
                .Item(12).Text = "Min Charge"
                .Item(13).Text = "Limit 1"
                .Item(14).Text = "Rate Limit 1"
                .Item(15).Text = "Limit 2"
                .Item(16).Text = "Rate Limit 2"
                .Item(17).Text = "Limit 3"
                .Item(18).Text = "Rate Limit 3"
                .Item(19).Text = "Limit 4"
                .Item(20).Text = "Rate Limit 4"
                .Item(21).Text = "Limit 5"
                .Item(22).Text = "Rate Limit 5"
                .Item(23).Text = "Limit 6"
                .Item(24).Text = "Rate Limit 6"
                .Item(25).Text = "Limit 7"
                .Item(26).Text = "Rate Limit 7"
                .Item(27).Text = "Limit 8"
                .Item(28).Text = "Rate Limit 8"

            Case "T_FCONN"
                Form_Setup "Found Connected", " Select Found Connected ", False, 9380, 9
                .Item(1).Width = 1000
                '.Item(2).Width = 1300
                '.Item(3).Width = 1500
                .Item(2).Width = 1200
                .Item(3).Width = 1300
                .Item(4).Width = 1200
                .Item(5).Width = 3000
                .Item(6).Width = 3000
                .Item(7).Width = 1300
                .Item(8).Width = 1500
                .Item(1).Text = "Book No."
                '.Item(2).Text = "Sequence No."
                '.Item(3).Text = "Account No."
                .Item(2).Text = "Meter No."
                .Item(3).Text = "Reading Date"
                .Item(4).Text = "Reading Time"
                .Item(5).Text = "Customer Name"
                .Item(6).Text = "Customer Address"
                .Item(7).Text = "KWH Reading"
                .Item(8).Text = "Meter Brand"
            
            Case "T_REPMTR"
                Form_Setup "Replaced Meter", " Select Replaced Meter ", False, 9380, 9
                .Item(1).Width = 1000
                .Item(2).Width = 1500
                .Item(3).Width = 1300
                .Item(4).Width = 1300
                .Item(5).Width = 1000
                .Item(6).Width = 1300
                .Item(7).Width = 1500
                .Item(8).Width = 1400
                .Item(1).Text = "Book No."
                .Item(2).Text = "Account No."
                .Item(3).Text = "Meter No."
                .Item(4).Text = "Date Replaced"
                .Item(5).Text = "Multiplier"
                .Item(6).Text = "Reading Date"
                .Item(7).Text = "Previous Reading"
                .Item(8).Text = "Discon Reading"
            
            Case "L_DMZCODE"
                Form_Setup "Business Center Table", " Select Business Center ", True, 4400, 4
                .Item(1).Width = 850
                .Item(2).Width = 1900
                .Item(3).Width = 1900
                .Item(1).Text = "BC Code"
                .Item(2).Text = "BC Description"
                .Item(3).Text = "Directory"
                
        End Select
    End With
    
    cmdReplMtr.Visible = False
    ListView_Load

End Sub

Private Sub Form_Setup(strFormCaption As String, strFrameCaption As String, bolAddVisible As Boolean, intLVWidth As Integer, Optional intColIndex As Integer)
    Dim l_int_Index As Integer
    
    Me.Caption = strFormCaption
    fraSelect.Caption = strFrameCaption
    'cmdAdd.Visible = bolAddVisible
If WindowState = vbNormal Then
    Me.Height = 6270
End If
    If intLVWidth < 4500 Then intLVWidth = 4500
    If intLVWidth > 9380 Then intLVWidth = 9380
If WindowState = vbNormal Then
    Me.Width = intLVWidth + 620
End If
    fraSelect.Width = intLVWidth + 300
    lsvFileMain.Width = intLVWidth

    If intColIndex = 0 Then Exit Sub
    For l_int_Index = intColIndex To lsvFileMain.ColumnHeaders.Count
        lsvFileMain.ColumnHeaders.Item(l_int_Index).Width = 0
    Next

    If g_str_Table = "R_FF" Or g_str_Table = "R_RATE" Or g_str_Table = "R_RANGE" Then
        cmdGenerate.Visible = True
    Else
        cmdGenerate.Visible = False
    End If
    
'    If g_str_Table = "T_CUST" Then
'        cmdReplMtr.Visible = True
'    Else
'        cmdReplMtr.Visible = False
'    End If
    
End Sub

Private Sub ListView_Load()
    Dim lobjItem As Object
    Dim l_str_Sql As String
    
    If g_str_Table = "T_FCONN" Then
        l_str_Sql = "SELECT BOOKNO, MTRNO, RDGDATE, RDGTIME, CUSTNAME, ADDRESS, READING, MTRBRAND FROM T_FCONN"
    Else
        l_str_Sql = "SELECT * FROM " & g_str_Table
    End If
    
    lsvFileMain.ListItems.Clear
    If Not OpenRecordset(g_rs_RFF, l_str_Sql, Me.Name, "ListView_Load") Then Exit Sub
    
    g_rs_RFF.MoveFirst
    While Not g_rs_RFF.EOF
        Select Case g_str_Table
            Case "L_FFCODE"
                ListViewSet 2
            Case "L_RANGECODE"
                ListViewSet 2
            Case "R_READER"
                ListViewSet 2
            Case "F_HH"
                Set lobjItem = lsvFileMain.ListItems.Add(, , CheckNull(g_rs_RFF.Fields(0)))
                    lobjItem.SubItems(1) = CheckNull(g_rs_RFF.Fields(2))
                    lobjItem.SubItems(2) = Format(CheckNull(g_rs_RFF.Fields(1)), "mm/dd/yyyy")
                Set lobjItem = Nothing
            Case "R_RATE"
                ListViewSet 28
            Case "T_FCONN"
                'ListViewSet 10, 4, 5
                ListViewSet 8, 2, 3
            Case "T_REPMTR"
                ListViewSet 8, 5
            Case "L_DMZCODE"
                ListViewSet 3
                
        End Select
        g_rs_RFF.MoveNext
    Wend

    g_rs_RFF.Close
    Set g_rs_RFF = Nothing
    
    If lsvFileMain.ListItems.Count > 0 Then
'        If g_str_Table = "R_BUSCTR" Then
'            cmdEdit.Enabled = False
'            cmdDelete.Enabled = False
'        Else
            cmdEdit.Enabled = True
            cmdDelete.Enabled = True
            cmdReplMtr.Enabled = True
'        End If
    Else
        cmdEdit.Enabled = False
        cmdDelete.Enabled = False
        cmdReplMtr.Enabled = False
    End If

End Sub

Private Sub ListViewSet(intCols As Integer, Optional intColDate As Integer, Optional intColTime As Integer)
    Dim lobjItem As Object
    Dim l_int_Index As Integer
                
    Set lobjItem = lsvFileMain.ListItems.Add(, , CheckNull(g_rs_RFF.Fields(0)))
                    
    For l_int_Index = 1 To intCols - 1
        Select Case l_int_Index
            Case intColDate
                lobjItem.SubItems(l_int_Index) = Format(CheckNull(g_rs_RFF.Fields(l_int_Index)), "mm/dd/yyyy")
            Case intColTime
                lobjItem.SubItems(l_int_Index) = Format(CheckNull(g_rs_RFF.Fields(l_int_Index)), "hh:mm:ss")
            Case Else
                lobjItem.SubItems(l_int_Index) = CheckNull(g_rs_RFF.Fields(l_int_Index))
        End Select
    Next
                    
    Set lobjItem = Nothing

End Sub

Private Sub Form_Resize()
    On Error GoTo ErrorExit
    If WindowState = vbNormal Then
        If Me.Width < 5100 Then Me.Width = 5100
        If Me.Height < 2460 Then Me.Height = 2460
    End If
    fraSelect.Width = Me.Width - 260
    lsvFileMain.Width = Me.Width - 510
    
    If Me.Height > 2460 Then
        fraSelect.Height = Me.Height - 1290
        lsvFileMain.Height = Me.Height - 1710
        cmdAdd.Top = Me.Height - 1020
        cmdEdit.Top = Me.Height - 1020
        cmdDelete.Top = Me.Height - 1020
        cmdGenerate.Top = Me.Height - 1020
        cmdReplMtr.Top = Me.Height - 1020
        cmdClose.Top = Me.Height - 1020
    End If
    
ErrorExit:
    
End Sub

Private Sub lsvFileMain_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ListviewSort lsvFileMain, ColumnHeader.Index
End Sub

Private Sub lsvFileMain_Click()

If frmEditBooks.Caption = "Business Center Table" Then
    If OpenRecordset(g_rs_TBOOK, "SELECT DMZCODE_ID FROM T_BOOK where DMZCODE_ID='" & lsvFileMain.SelectedItem & "'", Me.Name, "cmdSave") Then
        'MsgBox g_rs_TBOOK.Fields(0)
        'cmdEdit.Enabled = False
        cmdDelete.Enabled = False
    Else
        'cmdEdit.Enabled = True
        cmdDelete.Enabled = True
    End If
ElseIf frmEditBooks.Caption = "Field Finding Table" Then
    If OpenRecordset(g_rs_TDOWNLOAD, "SELECT FF1 FROM T_UPLOAD where FF1='" & lsvFileMain.SelectedItem & "'", Me.Name, "cmdSave") Then
        'MsgBox g_rs_TBOOK.Fields(0)
        'cmdEdit.Enabled = False
        cmdDelete.Enabled = False
    Else
        'cmdEdit.Enabled = True
        cmdDelete.Enabled = True
    End If
End If

'If OpenRecordset(g_rs_TUPLOAD, "SELECT RANGECODE FROM T_UPLOAD where RANGECODE='" & lsvFileMain.SelectedItem & "'", Me.Name, "cmdSave") Then
'    'MsgBox g_rs_TBOOK.Fields(0)
'    'cmdEdit.Enabled = False
'    cmdDelete.Enabled = False
'Else
'    'cmdEdit.Enabled = True
'    cmdDelete.Enabled = True
'End If
End Sub
