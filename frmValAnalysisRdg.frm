VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmValAnalysisRdg 
   Caption         =   "Validation Analysis - Reading"
   ClientHeight    =   9975
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15165
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   9975
   ScaleWidth      =   15165
   Begin VB.Frame Frame2 
      Height          =   4935
      Left            =   0
      TabIndex        =   18
      Top             =   4920
      Width           =   15615
      Begin VB.CommandButton cmdRemoveMaster 
         Caption         =   "&Remove from Masterlist"
         Height          =   375
         Left            =   12480
         TabIndex        =   39
         Top             =   4490
         Width           =   2295
      End
      Begin VB.ComboBox cmbMConclusion 
         Height          =   315
         Left            =   13200
         TabIndex        =   38
         Text            =   " "
         ToolTipText     =   "Conclusion"
         Top             =   900
         Width           =   1815
      End
      Begin VB.CommandButton cmdMasAll 
         Caption         =   "Select &All"
         Height          =   375
         Left            =   120
         TabIndex        =   27
         Top             =   4490
         Width           =   975
      End
      Begin VB.CommandButton cmdmasNone 
         Caption         =   "Select &None"
         Height          =   375
         Left            =   1200
         TabIndex        =   26
         Top             =   4490
         Width           =   1095
      End
      Begin VB.CommandButton cmdGenReport 
         Caption         =   "&Generate Report"
         Height          =   375
         Left            =   2400
         TabIndex        =   25
         Top             =   4490
         Width           =   1335
      End
      Begin VB.CommandButton cmdTagNotError 
         Caption         =   "Tag as Not &Error"
         Height          =   375
         Left            =   9360
         TabIndex        =   24
         Top             =   4490
         Width           =   1335
      End
      Begin VB.CommandButton cmdRemoveTag 
         Caption         =   "Remove &Tag"
         Height          =   375
         Left            =   10920
         TabIndex        =   23
         Top             =   4490
         Width           =   1335
      End
      Begin VB.ComboBox cmbMBC 
         Height          =   315
         ItemData        =   "frmValAnalysisRdg.frx":0000
         Left            =   480
         List            =   "frmValAnalysisRdg.frx":0007
         Sorted          =   -1  'True
         TabIndex        =   22
         Text            =   "ALL"
         ToolTipText     =   "Business Center"
         Top             =   900
         Width           =   1740
      End
      Begin VB.ComboBox cmbMMRU 
         Height          =   315
         Left            =   2760
         TabIndex        =   21
         Text            =   "ALL"
         ToolTipText     =   "MRU"
         Top             =   900
         Width           =   1380
      End
      Begin VB.ComboBox cmbMRate 
         Height          =   315
         Left            =   7500
         Sorted          =   -1  'True
         TabIndex        =   20
         Text            =   "ALL"
         ToolTipText     =   "Rate"
         Top             =   900
         Width           =   1500
      End
      Begin VB.ComboBox cmbMOC 
         Height          =   315
         Left            =   10440
         TabIndex        =   19
         Text            =   "ALL"
         ToolTipText     =   "OC"
         Top             =   900
         Width           =   1335
      End
      Begin MSComCtl2.DTPicker DTPicker3 
         Height          =   375
         Left            =   4800
         TabIndex        =   28
         Top             =   480
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   661
         _Version        =   393216
         Format          =   59244545
         CurrentDate     =   40037
      End
      Begin MSComCtl2.DTPicker DTPicker4 
         Height          =   375
         Left            =   7800
         TabIndex        =   29
         Top             =   480
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   661
         _Version        =   393216
         Format          =   59244545
         CurrentDate     =   40037
      End
      Begin MSComctlLib.ListView lsvMaster 
         Height          =   3245
         Left            =   120
         TabIndex        =   30
         Top             =   1200
         Width           =   14985
         _ExtentX        =   26432
         _ExtentY        =   5715
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   14
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Business Center"
            Object.Width           =   3881
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "MRU"
            Object.Width           =   2910
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Installation No"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Meter Number"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Rate"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Ave Cons"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Consumption"
            Object.Width           =   2291
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Prev OC"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "Billed Prev Rdg"
            Object.Width           =   2558
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Text            =   "Present Rdg"
            Object.Width           =   2291
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Text            =   "OC"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Text            =   "Remarks"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Text            =   "Tries"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Text            =   "Conclusion"
            Object.Width           =   3175
         EndProperty
      End
      Begin VB.Label Label19 
         Caption         =   "Conclusion:"
         Height          =   255
         Left            =   12300
         TabIndex        =   50
         Top             =   960
         Width           =   1215
      End
      Begin VB.Label Label18 
         Caption         =   "OC:"
         Height          =   255
         Left            =   10080
         TabIndex        =   49
         Top             =   960
         Width           =   495
      End
      Begin VB.Label Label17 
         Caption         =   "Rate:"
         Height          =   255
         Left            =   7000
         TabIndex        =   48
         Top             =   960
         Width           =   855
      End
      Begin VB.Label Label16 
         Caption         =   "MRU:"
         Height          =   255
         Left            =   2280
         TabIndex        =   47
         Top             =   960
         Width           =   495
      End
      Begin VB.Label Label15 
         Caption         =   "BC:"
         Height          =   255
         Left            =   120
         TabIndex        =   46
         Top             =   960
         Width           =   255
      End
      Begin VB.Label Label5 
         Alignment       =   2  'Center
         BackColor       =   &H80000003&
         Caption         =   "MASTERLIST FOR VALIDATION"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   0
         TabIndex        =   35
         Top             =   240
         Width           =   15855
      End
      Begin VB.Label Label6 
         Caption         =   "Extract Date to:"
         Height          =   255
         Left            =   6480
         TabIndex        =   34
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label7 
         Caption         =   "Extract Date from:"
         Height          =   255
         Left            =   3240
         TabIndex        =   33
         Top             =   480
         Width           =   1455
      End
      Begin VB.Label Label9 
         Caption         =   "Records"
         Height          =   255
         Left            =   8040
         TabIndex        =   32
         Top             =   4530
         Width           =   735
      End
      Begin VB.Label lblCountMaster 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   7200
         TabIndex        =   31
         Top             =   4530
         Width           =   735
      End
   End
   Begin VB.Frame Frame1 
      Height          =   4935
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   15615
      Begin MSComCtl2.DTPicker DTPicker5 
         Height          =   30
         Left            =   8280
         TabIndex        =   40
         Top             =   4200
         Width           =   30
         _ExtentX        =   53
         _ExtentY        =   53
         _Version        =   393216
         Format          =   59244545
         CurrentDate     =   40082
      End
      Begin VB.ComboBox cmbCondition 
         Height          =   315
         Left            =   12660
         TabIndex        =   37
         Text            =   "ALL"
         ToolTipText     =   "Condition"
         Top             =   480
         Width           =   2295
      End
      Begin VB.ComboBox cmbRate 
         Height          =   315
         Left            =   7500
         TabIndex        =   36
         Text            =   "ALL"
         ToolTipText     =   "Rate"
         Top             =   840
         Width           =   1305
      End
      Begin VB.CommandButton cmdRemove 
         Caption         =   "&Remove from Extracted"
         Height          =   375
         Index           =   0
         Left            =   3000
         TabIndex        =   17
         Top             =   4440
         Width           =   2535
      End
      Begin VB.ComboBox cmbCycle 
         Height          =   315
         ItemData        =   "frmValAnalysisRdg.frx":0010
         Left            =   720
         List            =   "frmValAnalysisRdg.frx":0038
         TabIndex        =   7
         Top             =   480
         Width           =   1215
      End
      Begin VB.CommandButton cmdExAll 
         Caption         =   "Select &All"
         Height          =   375
         Left            =   120
         TabIndex        =   6
         Top             =   4440
         Width           =   1215
      End
      Begin VB.CommandButton cmdExNone 
         Caption         =   "Select &None"
         Height          =   375
         Left            =   1440
         TabIndex        =   5
         Top             =   4440
         Width           =   1215
      End
      Begin VB.CommandButton cmdAdd 
         Caption         =   "&Add to Masterlist"
         Height          =   375
         Left            =   12600
         TabIndex        =   4
         Top             =   4440
         Width           =   2295
      End
      Begin VB.ComboBox cmbBusinessCenter 
         Height          =   315
         ItemData        =   "frmValAnalysisRdg.frx":0063
         Left            =   480
         List            =   "frmValAnalysisRdg.frx":006A
         Sorted          =   -1  'True
         TabIndex        =   3
         Text            =   "ALL"
         ToolTipText     =   "Business Center"
         Top             =   840
         Width           =   1800
      End
      Begin VB.ComboBox cmbMRU 
         Height          =   315
         Left            =   2760
         TabIndex        =   2
         Text            =   "ALL"
         ToolTipText     =   "MRU"
         Top             =   840
         Width           =   1305
      End
      Begin VB.ComboBox cmbOC 
         Height          =   315
         Left            =   13635
         TabIndex        =   1
         Text            =   "ALL"
         ToolTipText     =   "OC"
         Top             =   840
         Width           =   1320
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   375
         Left            =   5640
         TabIndex        =   8
         Top             =   480
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   661
         _Version        =   393216
         Format          =   59244545
         CurrentDate     =   40037
      End
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   375
         Left            =   9120
         TabIndex        =   9
         Top             =   480
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   661
         _Version        =   393216
         Format          =   59244545
         CurrentDate     =   40037
      End
      Begin MSComctlLib.ListView lsvExtract 
         Height          =   3255
         Left            =   120
         TabIndex        =   10
         Top             =   1155
         Width           =   14985
         _ExtentX        =   26432
         _ExtentY        =   5741
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   14
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Business Center"
            Object.Width           =   3881
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "MRU"
            Object.Width           =   2910
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Installation No"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Meter Number"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Rate"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Ave Cons"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Consumption"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Prev OC"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "Previous Rdg"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Text            =   "Billed Prev Rdg"
            Object.Width           =   2291
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Text            =   "Present Rdg"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Text            =   "OC"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Text            =   "Remarks"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Text            =   "Tries"
            Object.Width           =   2293
         EndProperty
      End
      Begin VB.Label Label14 
         Caption         =   "OC:"
         Height          =   255
         Left            =   13320
         TabIndex        =   45
         Top             =   900
         Width           =   375
      End
      Begin VB.Label Label13 
         Caption         =   "Rate:"
         Height          =   255
         Left            =   7000
         TabIndex        =   44
         Top             =   900
         Width           =   855
      End
      Begin VB.Label Label12 
         Caption         =   "MRU:"
         Height          =   255
         Left            =   2280
         TabIndex        =   43
         Top             =   900
         Width           =   615
      End
      Begin VB.Label Label11 
         Caption         =   "BC:"
         Height          =   255
         Left            =   120
         TabIndex        =   42
         Top             =   900
         Width           =   375
      End
      Begin VB.Label Label10 
         Caption         =   "Condition:"
         Height          =   255
         Left            =   11880
         TabIndex        =   41
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H80000003&
         Caption         =   "EXTRACTED ACCOUNTS"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   0
         TabIndex        =   16
         Top             =   120
         Width           =   15495
      End
      Begin VB.Label Label2 
         Caption         =   "Extract Date from:"
         Height          =   255
         Left            =   4080
         TabIndex        =   15
         Top             =   480
         Width           =   1455
      End
      Begin VB.Label Label3 
         Caption         =   "Extract Date to:"
         Height          =   255
         Left            =   7680
         TabIndex        =   14
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label4 
         Caption         =   "Cycle"
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   480
         Width           =   615
      End
      Begin VB.Label Label8 
         Caption         =   "Records"
         Height          =   255
         Left            =   8280
         TabIndex        =   12
         Top             =   4560
         Width           =   735
      End
      Begin VB.Label lblCountExtract 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   7440
         TabIndex        =   11
         Top             =   4560
         Width           =   615
      End
   End
End
Attribute VB_Name = "frmValAnalysisRdg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim valCycle As Integer
Dim valDate1 As Date
Dim valDate2 As Date
Dim valDate3 As Date
Dim valDate4 As Date
Dim valBC As String
Dim valOC As String
Dim valRate As String
Dim valMRU As String
Dim valMBCOde As String
Dim valMBC As String
Dim valMOC As String
Dim valMMRU As String
Dim valMRate As String
Dim valMConclusion As String
Dim valCondition As String

Private Sub cmbBusinessCenter_Click()
valBC = cmbBusinessCenter.Text
ClearComboExtract
genlsvExtract
cmbBusinessCenter.Text = valMRU
End Sub

Private Sub cmbCondition_Click()
valCondition = cmbCondition.Text
ClearComboExtract
genlsvExtract
cmbCondition.Text = valCondition

End Sub

Private Sub cmbCycle_Click()
valCycle = CInt(cmbCycle.Text)
DTPicker1.Month = valCycle
DTPicker2.Month = valCycle
DTPicker3.Month = valCycle
DTPicker4.Month = valCycle
valDate1 = DTPicker1.Value
valDate2 = DTPicker2.Value
valDate3 = DTPicker3.Value
valDate4 = DTPicker4.Value
ClearComboExtract
genlsvExtract
genMasterList

End Sub

Private Sub cmbMBC_Click()
Dim rsbccode As ADODB.Recordset
valMBC = cmbMBC.Text
ClearComboMaster
genMasterList
cmbMBC.Text = valMBC
If cmbMBC.Text <> "ALL" Then
  
  If OpenRecordset(rsbccode, "SELECT BC_CODE FROM R_BUSCTR WHERE BC_DESC = '" & cmbMBC.Text & "'", "", "") Then
   valMBCOde = rsbccode.Fields(0)
  End If
  rsbccode.Close
  Set rsbccode = Nothing
  
ElseIf cmbMBC.Text = "ALL" Then
  valMBCOde = "ALL"
End If



End Sub

Private Sub cmbMConclusion_Click()
valMConclusion = cmbMConclusion.Text
ClearComboMaster
genMasterList
cmbMConclusion.Text = valMConclusion

End Sub

Private Sub cmbMMRU_Click()
valMMRU = cmbMMRU.Text
ClearComboMaster
genMasterList
cmbMMRU.Text = valMMRU
End Sub



Private Sub cmbMOC_Click()
valMOC = cmbMOC.Text
ClearComboMaster
genMasterList
cmbMOC.Text = valMOC
End Sub

Private Sub cmbMRate_Click()
valMRate = cmbMRate.Text
ClearComboMaster
genMasterList
cmbMRate.Text = valMRate
End Sub

Private Sub cmbMRU_Click()
valMRU = cmbMRU.Text
ClearComboExtract
genlsvExtract
cmbMRU.Text = valMRU
End Sub


Public Sub ClearComboExtract()
cmbBusinessCenter.Clear
cmbBusinessCenter.AddItem "ALL"
cmbMRU.Clear
cmbMRU.AddItem "ALL"
cmbOC.Clear
cmbOC.AddItem "ALL"
cmbRate.Clear
cmbRate.AddItem "ALL"
cmbCondition.Clear
cmbCondition.AddItem "ALL"

End Sub

Public Sub ClearComboMaster()
cmbMBC.Clear
cmbMBC.AddItem "ALL"
cmbMMRU.Clear
cmbMMRU.AddItem "ALL"
cmbMOC.Clear
cmbMOC.AddItem "ALL"
cmbMRate.Clear
cmbMRate.AddItem "ALL"
cmbMConclusion.Clear
'cmbMConclusion.AddItem ""
cmbMConclusion.AddItem "ALL"

End Sub

Private Sub cmbOC_Click()
valOC = cmbOC.Text
ClearComboExtract
genlsvExtract
cmbOC.Text = valOC
End Sub

Private Sub cmbRate_Click()
valRate = cmbRate.Text
ClearComboExtract
genlsvExtract
cmbRate.Text = valRate
End Sub

Private Sub cmdAdd_Click()
Dim X As Integer
Dim MRU As String
Dim ACCTNUM As String
Dim SQLUpdate As String
Dim SQLUpdate1 As String
Dim Count As String

Count = 0
For X = 1 To lsvExtract.ListItems.Count
   If lsvExtract.ListItems(X).checked = True Then
     'for checked items
      MRU = lsvExtract.ListItems.Item(X).ListSubItems(1)
      ACCTNUM = lsvExtract.ListItems.Item(X).ListSubItems(2)
      SQLUpdate = "UPDATE dbo.T_VER_EXTRACT SET MASTERLIST = 2 WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle
      DBExecute SQLUpdate, "", ""
      If OpenRecordset(g_rs_TUPLOADHIS, "SELECT * FROM dbo.T_UPLOAD_HIS WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle, Me.Name, "") Then
        SQLUpdate1 = "UPDATE dbo.T_UPLOAD_HIS SET INMASTERLIST = 1 WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle
        DBExecute SQLUpdate1, "", ""
      End If
      Count = Count + 1
   End If
   
Next X
MsgBox Count & " Records added to Masterlist", vbInformation

genlsvExtract
genMasterList

End Sub

Private Sub cmdExAll_Click()
SelectAll 1, 0
End Sub

Private Sub cmdExNone_Click()
SelectAll 0, 0
End Sub

Private Sub cmdGenReport_Click()
Dim Dateparam As String
Dim sFName As String
Dim s As Integer
Dim driveSource As String
Dim Appl As New CRAXDRT.Application
Dim Report As New CRAXDRT.Report
Dim fileName As String
Dim fso As Object
Dim X As Integer
Dim tempBC As String
Dim sConc As String


Set fso = CreateObject("Scripting.FileSystemObject")
driveSource = Mid(App.Path, 1, 2)

If lsvMaster.ListItems.Count > 0 Then

    If fso.FolderExists(driveSource & "\VALIDATION REPORTS") = False Then
        fso.CreateFolder (driveSource & "\VALIDATION REPORTS")
    End If
    
    If fso.FolderExists(driveSource & "\VALIDATION REPORTS\READING") = False Then
        fso.CreateFolder (driveSource & "\VALIDATION REPORTS\READING")
    End If
    
    If fso.FolderExists(driveSource & "\VALIDATION REPORTS\READING\" & Year(Now)) = False Then
        fso.CreateFolder (driveSource & "\VALIDATION REPORTS\READING\" & Year(Now))
    End If
    
    If fso.FolderExists(driveSource & "\VALIDATION REPORTS\READING\" & Year(Now) & "\" & Padl(Month(Now), 2, 0)) = False Then
        fso.CreateFolder (driveSource & "\VALIDATION REPORTS\READING\" & Year(Now) & "\" & Padl(Month(Now), 2, 0))
    End If
    
    If fso.FolderExists(driveSource & "\VALIDATION REPORTS\READING\" & Year(Now) & "\" & Padl(Month(Now), 2, 0) & "\" & Padl(Day(Now), 2, 0)) = False Then
        fso.CreateFolder (driveSource & "\VALIDATION REPORTS\READING\" & Year(Now) & "\" & Padl(Month(Now), 2, 0) & "\" & Padl(Day(Now), 2, 0))
    End If
    
    If valMBC <> "ALL" Then
 
         fileName = driveSource & "\VALIDATION REPORTS\READING\" & Year(Now) & "\" & Padl(Month(Now), 2, 0) & "\" & Padl(Day(Now), 2, 0) & "\"
         
         Set Appn = CreateObject("CrystalRunTime.Application")
         Set cReport = Appn.OpenReport(App.Path & "\crystal reports\" & "VALIDATION DETAILED READING REPORT.rpt")
         'Or get your report name into the report somehow
         cReport.Database.LogOnServer "p2ssql.dll", "SZ-MWSI-SUBIC", "MCFSDB", "sa", "sqladmin"
         For s = 1 To cReport.Database.Tables.Count
         cReport.Database.Tables(s).ConnectionProperties.Item("Password") = "sqladmin"
         Next s
        
        sFName = valMBC & "_Reading Validation_" & valMConclusion & "_" & Padl(Month(valDate3), 2, 0) & Padl(Day(valDate3), 2, 0) & Year(valDate3) & ".xls"
        cReport.DiscardSavedData
        cReport.ParameterFields(1).AddCurrentValue valMBCOde
        cReport.ParameterFields(2).AddCurrentValue valCycle
        cReport.ParameterFields(3).AddCurrentValue valDate3
        cReport.ParameterFields(4).AddCurrentValue valDate4
        cReport.ParameterFields(5).AddCurrentValue valMRU
            If valMConclusion = " " Then
                sConc = "0"
            ElseIf valMConclusion = "CORRECTED" Then
                sConc = "1"
            ElseIf valMConclusion = "ERRONEOUS" Then
                sConc = "2"
            ElseIf valMConclusion = "NOT ERRONEOUS" Then
                sConc = "3"
            Else
                sConc = "ALL"
            End If
        cReport.ParameterFields(6).AddCurrentValue sConc
        cReport.ParameterFields(7).AddCurrentValue 1
        
        cReport.ExportOptions.DiskFileName = fileName & sFName
        cReport.ExportOptions.DestinationType = crEDTDiskFile
        cReport.ExportOptions.FormatType = crEFTExcelDataOnly
        cReport.ExportOptions.ExcelAreaType = crDetail
        cReport.ExportOptions.ExcelUseFormatInDataOnly = True
        cReport.ExportOptions.ExcelMaintainRelativeObjectPosition = True
        cReport.ExportOptions.ExcelMaintainColumnAlignment = True
        cReport.ExportOptions.ExcelChopPageHeader = False
        cReport.Export False
    Else
        For X = 1 To cmbMBC.ListCount - 1
            tempBC = cmbMBC.List(X)
            Dim rsbccode As ADODB.Recordset
            Dim bcCodes As String
            If OpenRecordset(rsbccode, "SELECT BC_CODE FROM R_BUSCTR WHERE BC_DESC = '" & tempBC & "'", "", "") Then
            bcCodes = rsbccode.Fields(0)
            End If
            rsbccode.Close
            Set rsbccode = Nothing
            
            fileName = driveSource & "\VALIDATION REPORTS\READING\" & Year(Now) & "\" & Padl(Month(Now), 2, 0) & "\" & Padl(Day(Now), 2, 0) & "\"
         
             Set Appn = CreateObject("CrystalRunTime.Application")
             Set cReport = Appn.OpenReport(App.Path & "\crystal reports\" & "VALIDATION DETAILED READING REPORT.rpt")
            'Or get your report name into the report somehow
             cReport.Database.LogOnServer "p2ssql.dll", "SZ-MWSI-SUBIC", "MCFSDB", "sa", "sqladmin"
             For s = 1 To cReport.Database.Tables.Count
             cReport.Database.Tables(s).ConnectionProperties.Item("Password") = "sqladmin"
             Next s
        
        
            sFName = tempBC & "_Reading Validation_" & valMConclusion & "_" & Padl(Month(valDate3), 2, 0) & Padl(Day(valDate3), 2, 0) & Year(valDate3) & ".xls"
            cReport.DiscardSavedData
            cReport.ParameterFields(1).AddCurrentValue bcCodes
            cReport.ParameterFields(2).AddCurrentValue valCycle
            cReport.ParameterFields(3).AddCurrentValue valDate3
            cReport.ParameterFields(4).AddCurrentValue valDate4
            cReport.ParameterFields(5).AddCurrentValue valMRU
            If valMConclusion = " " Then
                sConc = "0"
            ElseIf valMConclusion = "CORRECTED" Then
                sConc = "1"
            ElseIf valMConclusion = "ERRONEOUS" Then
                sConc = "2"
            ElseIf valMConclusion = "NOT ERRONEOUS" Then
                sConc = "3"
            Else
                sConc = "ALL"
            End If
            cReport.ParameterFields(6).AddCurrentValue sConc
            cReport.ParameterFields(7).AddCurrentValue 1
            
            cReport.ExportOptions.DiskFileName = fileName & Replace(sFName, "/", "_")
            cReport.ExportOptions.DestinationType = crEDTDiskFile
            cReport.ExportOptions.FormatType = crEFTExcelDataOnly
            cReport.ExportOptions.ExcelAreaType = crDetail
            cReport.ExportOptions.ExcelUseFormatInDataOnly = True
            cReport.ExportOptions.ExcelMaintainRelativeObjectPosition = True
            cReport.ExportOptions.ExcelMaintainColumnAlignment = True
            cReport.ExportOptions.ExcelChopPageHeader = False
            cReport.Export False
        
        
        
        Next X
    End If
MsgBox "The reports have been successfully saved.", , "Generation Successful"
End If

End Sub

Private Sub cmdMasAll_Click()
SelectAll 1, 1
End Sub

Private Sub cmdmasNone_Click()
SelectAll 0, 1
End Sub

Private Sub cmdRemove_Click(Index As Integer)
Dim X As Integer
Dim MRU As String
Dim ACCTNUM As String
Dim SQLUpdate As String
Dim Count As String

Count = 0
For X = 1 To lsvExtract.ListItems.Count
   If lsvExtract.ListItems(X).checked = True Then
     'for checked items
      MRU = lsvExtract.ListItems.Item(X).ListSubItems(1)
      ACCTNUM = lsvExtract.ListItems.Item(X).ListSubItems(2)
      SQLUpdate = "DELETE from dbo.T_VER_EXTRACT WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle
      DBExecute SQLUpdate, "", ""
      Count = Count + 1
   End If
   
Next X
MsgBox Count & " Records Deleted!", vbInformation

genlsvExtract
'genMasterList

End Sub

Private Sub cmdRemoveMaster_Click()
Dim X As Integer
Dim MRU As String
Dim ACCTNUM As String
Dim SQLUpdate As String
Dim SQLUpdate1 As String
Dim Count As String
Dim txtStatus As String
Dim checked As Integer
Dim ans As Integer
checked = 0
Count = 0
For X = 1 To lsvMaster.ListItems.Count
  If lsvMaster.ListItems(X).checked = True Then
    If lsvMaster.ListItems.Item(X).ListSubItems(13) <> " " Then
     checked = 1
    End If
    If lsvMaster.ListItems.Item(X).ListSubItems(13) = " " Then
     Count = Count + 1
    End If
  End If
Next X

If checked = 1 Then
MsgBox "Only Accounts with Null Status can be removed from masterlist"
ElseIf checked = 0 And Count > 0 Then
ans = MsgBox("Are you sure you want to remove this account from masterlist?", vbYesNo, "Remove from Masterlist")

If ans = 6 Then
'UPDATE MASTERLIST TO EXTRACTED
   For X = 1 To lsvMaster.ListItems.Count
     If lsvMaster.ListItems(X).checked = True Then
       MRU = lsvMaster.ListItems.Item(X).ListSubItems(1)
       ACCTNUM = lsvMaster.ListItems.Item(X).ListSubItems(2)
       SQLUpdate = "UPDATE dbo.T_VER_EXTRACT SET MASTERLIST = 0 WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle
       DBExecute SQLUpdate, "", ""
      If OpenRecordset(g_rs_TUPLOADHIS, "SELECT * FROM dbo.T_UPLOAD_HIS WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle, Me.Name, "") Then
        SQLUpdate1 = "UPDATE dbo.T_UPLOAD_HIS SET INMASTERLIST = 0 WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle
        DBExecute SQLUpdate1, "", ""
      End If
     End If
   Next X
   If Count <> 0 Then
        MsgBox Count & " Records removed from Masterlist", vbInformation
        genlsvExtract
        ClearComboMaster
        genMasterList
   checked = 0
   Count = 0
   End If
 End If
End If

End Sub

Private Sub cmdRemoveTag_Click()
Dim X As Integer
Dim txtStatus As String
Dim checked As Integer
Dim MRU As String
Dim ACCTNUM As String
Dim SQLUpdate As String
Dim SQLUpdate2 As String
Dim SQLDelete As String
Dim SQLDelete2 As String
Dim Count As Integer
'Dim TUpldOOR As Boolean
'Dim TUpldHisOOR As Boolean
'Dim TUpldRTag As Boolean
'Dim TUpldHisRTag As Boolean
'Dim RCode As String
'Dim unrdcnt As String
'Dim rdcnt As String
Dim rngecode As String
Dim rngecodehis As String
Dim RDTAG As String
Dim rdtaghis As String
Dim oorcount As String
Dim unrdcnt As String
Dim rdcnt As String

checked = 0
Count = 0
For X = 1 To lsvMaster.ListItems.Count
  If lsvMaster.ListItems(X).checked = True Then
    If lsvMaster.ListItems.Item(X).ListSubItems(13) = "CORRECTED" Or lsvMaster.ListItems.Item(X).ListSubItems(13) = " " Then
     checked = 1
     Exit For
    End If
    If lsvMaster.ListItems.Item(X).ListSubItems(13) = "ERRONEOUS" Or lsvMaster.ListItems.Item(X).ListSubItems(13) = "NOT ERRONEOUS" Then
    Count = Count + 1
    End If
  End If
Next X

If checked = 1 Then
    MsgBox "Only Accounts with Erroneous and Not Erroneous Status are allowed for Remove Tag!"
ElseIf checked = 0 And Count > 0 Then
'UPDATE STATUSES TO PENDING
   For X = 1 To lsvMaster.ListItems.Count
     If lsvMaster.ListItems(X).checked = True Then
       MRU = lsvMaster.ListItems.Item(X).ListSubItems(1)
       ACCTNUM = lsvMaster.ListItems.Item(X).ListSubItems(2)
       'UPDATE STATUS IN MASTERLIST
       SQLUpdate = "UPDATE dbo.T_VER_EXTRACT SET CONDITION = 0 WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle
       DBExecute SQLUpdate, "", ""
       
            '---------------------------------
            'UPDATE T_BKINFO_HIS counts
            '---------------------------------
            If OpenRecordset(g_rs_TUPLOAD, "SELECT RANGECODE, READTAG FROM dbo.T_UPLOAD WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle, Me.Name, "") Then
                rngecode = g_rs_TUPLOAD.Fields(0)
                RDTAG = g_rs_TUPLOAD.Fields(1)
                If OpenRecordset(g_rs_TUPLOADHIS, "SELECT RANGECODE, READTAG FROM dbo.T_UPLOAD_HIS WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle & " AND ORIGDATA = 0", Me.Name, "") Then
                    rngecodehis = g_rs_TUPLOADHIS.Fields(0)
                    rdtaghis = g_rs_TUPLOADHIS.Fields(1)
                    
                    '--IF T_UPLOAD RANGECODE IS OUT OF RANGE AND T_UPLOAD_HIS RANGECODE ISNT, subtract 1 TO OORCNT
                    If (rngecode = "3" Or rngecode = "4") And (rngecodehis <> "3" Or rngecodehis <> "4") Then
                        oorcount = "OORCNT - 1"
                        'sqlrange = "UPDATE dbo.T_BKINFO_HIS SET OORCNT = '" & oorcount & "' WHERE BOOKNO = '" & MRU & "' and CYCLE = " & valCycle & " AND ORIGDATA = 0"
                        'DBExecute sqlrange, "", ""
                        '--IF T_UPLOAD_HIS RANGECODE IS OUT OF RANGE AND T_UPLOAD RANGECODE ISNT, add 1 TO OORCNT
                    ElseIf (rngecode <> "3" Or rngecode <> "4") And (rngecodehis = "3" Or rngecodehis = "4") Then
                        oorcount = "OORCNT + 1"
                        'sqlrange = "UPDATE dbo.T_BKINFO_HIS SET OORCNT = '" & oorcount & "' WHERE BOOKNO = '" & MRU & "' and CYCLE = " & valCycle & " AND ORIGDATA = 0"
                        'DBExecute sqlrange, "", ""
                    Else
                        oorcount = "OORCNT"
                    End If
                    
                    '--IF T_UPLOAD READTAG IS 01 AND T_UPLOAD_HIS READTAG ISNT, subtract 1 TO READCNT AND add 1 TO UNREAD
                    If RDTAG = "01" And rdtaghis <> "01" Then
                        rdcnt = "READCNT - 1"
                        unrdcnt = "UNREAD + 1"
                        'sqlread = "UPDATE dbo.T_BKINFO_HIS SET READCNT = '" & rdcnt & "', UNREAD = '" & unrdcnt & "' WHERE BOOKNO = '" & MRU & "' and CYCLE = " & valCycle & " AND ORIGDATA = 0"
                        'DBExecute sqlread, "", ""
                        '--IF T_UPLOAD_HIS READTAG IS 01 AND T_UPLOAD READTAG ISNT, subtract 1 TO UNREAD AND add 1 TO READCNT
                    ElseIf RDTAG <> "01" And rdtaghis = "01" Then
                        rdcnt = "READCNT + 1"
                        unrdcnt = "UNREAD - 1"
                        'sqlread = "UPDATE dbo.T_BKINFO_HIS SET READCNT = '" & rdcnt & "', UNREAD = '" & unrdcnt & "' WHERE BOOKNO = '" & MRU & "' and CYCLE = " & valCycle & " AND ORIGDATA = 0"
                        'DBExecute sqlread, "", ""
                    Else
                        rdcnt = "READCNT"
                        unrdcnt = "UNREAD"
                    End If
                    g_rs_TUPLOADHIS.Close
                    Set g_rs_TUPLOADHIS = Nothing
                    
                    'DELETE VALIDATED READINGS FROM T_UPLOAD_HIS
                    SQLDelete = "DELETE FROM dbo.T_UPLOAD_HIS WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle
                    DBExecute SQLDelete, "", ""
                    'DELETE ROW FROM T_BKINFO_HIS IF NO OTHER VALIDATED READINGS FOR THE BOOKNO AND CYCLE
                    'EXIST IN T_UPLOAD_HIS OTHERWISE UPDATE T_BKINFO_HIS OORCNT, UNREAD AND READ COUNT
                    If Not OpenRecordset(g_rs_TUPLOADHIS, "SELECT BOOKNO FROM T_UPLOAD_HIS WHERE BOOKNO = '" & MRU & "' AND ULCYCLE = " & valCycle, Me.Name, "") Then
                        'NO OTHER DATA EXIST IN T_UPLOAD HIS FOR BOOKNO AND CYCLE
                        SQLDelete2 = "DELETE FROM dbo.T_BKINFO_HIS WHERE BOOKNO = '" & MRU & "' AND CYCLE = " & valCycle
                        DBExecute SQLDelete2, "", ""
                    Else
                        'GET COUNTS FOR UNREAD, READCNT AND OORCNT
                        'SQLUpdate2 = "UPDATE dbo.T_BKINFO_HIS SET UNREAD = " & unrdcnt & " , READCNT = " & rdcnt & ", OORCNT = " & RCode & " WHERE BOOKNO = '" & MRU & "' AND CYCLE = " & valCycle
                        SQLUpdate2 = "UPDATE dbo.T_BKINFO_HIS SET READCNT = " & rdcnt & ", UNREAD = " & unrdcnt & ", OORCNT = " & oorcount & " WHERE BOOKNO = '" & MRU & "' and CYCLE = " & valCycle & " AND ORIGDATA = 0"
                        DBExecute SQLUpdate2, "", ""
                        g_rs_TUPLOADHIS.Close
                        Set g_rs_TUPLOADHIS = Nothing
                    End If
                
                End If
                g_rs_TUPLOAD.Close
                Set g_rs_TUPLOAD = Nothing
            End If
        End If
    Next X
                    
'''''       'GET RANGECODE AND READTAG FOR T_BKINFO_HIS COUNT BEFORE DELETING
'''''       TUpldOOR = False
'''''       TUpldHisOOR = False
'''''       TUpldRCode = False
'''''       TUpldHisRCode = False
'''''       If OpenRecordset(g_rs_TUPLOAD, "SELECT RANGECODE, READTAG FROM T_UPLOAD WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle, "", "") Then
'''''            If (g_rs_TUPLOAD.Fields("RANGECODE") = "3") Or (g_rs_TUPLOAD.Fields("RANGECODE") = "4") Then TUpldOOR = True
'''''            If g_rs_TUPLOADHIS.Fields("READTAG") = "01" Then TUpldRCode = True
'''''            g_rs_TUPLOAD.Close
'''''            Set g_rs_TUPLOAD = Nothing
'''''       End If
'''''       If OpenRecordset(g_rs_TUPLOADHIS, "SELECT RANGECODE, READTAG FROM T_UPLOAD_HIS WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle & " AND ORIGDATA = 1", "", "") Then
'''''            If (g_rs_TUPLOADHIS.Fields("RANGECODE") = "3") Or (g_rs_TUPLOADHIS.Fields("RANGECODE") = "4") Then TUpldHisOOR = True
'''''            If g_rs_TUPLOADHIS.Fields("READTAG") = "01" Then TUpldHisRCode = True
'''''            g_rs_TUPLOADHIS.Close
'''''            Set g_rs_TUPLOADHIS = Nothing
'''''       End If
'''''       If OpenRecordset(g_rs_TUPLOADHIS, "SELECT RANGECODE, READTAG FROM T_UPLOAD_HIS WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle & " AND ORIGDATA = 1", "", "") Then
'''''            If (g_rs_TUPLOADHIS.Fields("RANGECODE") = "3") Or (g_rs_TUPLOADHIS.Fields("RANGECODE") = "4") Then TUpldHisOOR = True
'''''            If g_rs_TUPLOADHIS.Fields("READTAG") = "01" Then TUpldHisRCode = True
'''''            g_rs_TUPLOADHIS.Close
'''''            Set g_rs_TUPLOADHIS = Nothing
'''''       End If
'''''
'''''        'IF T_UPLOAD RANGECODE IS OUT OF RANGE AND T_UPLOAD_HIS RANGECODE ISNT, ADD 1 TO OORCNT
'''''        If TUpldOOR = True And TUpldHisOOR = False Then
'''''            RCode = "OORCNT + 1"
'''''        'IF T_UPLOAD_HIS RANGECODE IS OUT OF RANGE AND T_UPLOAD RANGECODE ISNT, SUBTRACT 1 TO OORCNT
'''''        ElseIf TUpldOOR = False And TUpldHisOOR = True Then
'''''            RCode = "OORCNT - 1"
'''''        'IF BOTH T_UPLOAD AND T_UPLOAD_HIS RANGECODE ARE OUT OF RANGE, DONT UPDATE OORCNT
'''''        Else
'''''            RCode = "OORCNT"
'''''        End If
'''''        'IF T_UPLOAD READTAG IS 01 AND T_UPLOAD_HIS READTAG ISNT, ADD 1 TO READCNT AND SUBTRACT 1 TO UNREAD
'''''        If TUpldRTag = True And TUpldHisRTag = False Then
'''''            unrdcnt = "UNREAD - 1"
'''''            rdcnt = "READCNT + 1"
'''''        'IF T_UPLOAD_HIS READTAG IS 01 AND T_UPLOAD READTAG ISNT, ADD 1 TO UNREAD AND SUBTRACT 1 TO READCNT
'''''        ElseIf TUpldRTag = False And TUpldHisRTag = True Then
'''''            unrdcnt = "UNREAD + 1"
'''''            rdcnt = "READCNT - 1"
'''''        Else
'''''            unrdcnt = "UNREAD"
'''''            rdcnt = "READCNT"
'''''        End If

'''''       'DELETE VALIDATED READINGS FROM T_UPLOAD_HIS
'''''       SQLDelete = "DELETE FROM dbo.T_UPLOAD_HIS WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle
'''''       DBExecute SQLDelete, "", ""
'''''       'DELETE ROW FROM T_BKINFO_HIS IF NO OTHER VALIDATED READINGS FOR THE BOOKNO AND CYCLE
'''''       'EXIST IN T_UPLOAD_HIS OTHERWISE UPDATE T_BKINFO_HIS OORCNT, UNREAD AND READ COUNT
'''''        If Not OpenRecordset(g_rs_TUPLOADHIS, "SELECT BOOKNO FROM T_UPLOAD_HIS WHERE BOOKNO = '" & MRU & "' AND ULCYCLE = " & valCycle, Me.Name, "") Then
'''''            'NO OTHER DATA EXIST IN T_UPLOAD HIS FOR BOOKNO AND CYCLE
'''''            SQLDelete2 = "DELETE FROM dbo.T_BKINFO_HIS WHERE BOOKNO = '" & MRU & "' AND CYCLE = " & valCycle
'''''            DBExecute SQLDelete2, "", ""
'''''        Else
'''''            'GET COUNTS FOR UNREAD, READCNT AND OORCNT
'''''            SQLUpdate2 = "UPDATE dbo.T_BKINFO_HIS SET UNREAD = " & unrdcnt & " , READCNT = " & rdcnt & ", OORCNT = " & RCode & " WHERE BOOKNO = '" & MRU & "' AND CYCLE = " & valCycle
'''''            DBExecute SQLUpdate2, "", ""
'''''            g_rs_TUPLOADHIS.Close
'''''            Set g_rs_TUPLOADHIS = Nothing
'''''        End If
'''''     End If
'''''   Next X
    If Count <> 0 Then
      MsgBox Count & " Records Updated!", vbInformation
      genMasterList
      checked = 0
      Count = 0
    End If

End If

End Sub

Private Sub cmdTagNotError_Click()
Dim X As Integer
Dim txtStatus As String
Dim checked As Integer
Dim MRU As String
Dim ACCTNUM As String
Dim SQLUpdate As String
Dim Count As Integer
Dim ans As Integer
checked = 0
Count = 0
For X = 1 To lsvMaster.ListItems.Count
  If lsvMaster.ListItems(X).checked = True Then
    If lsvMaster.ListItems.Item(X).ListSubItems(13) <> " " Then
     checked = 1
    End If
    If lsvMaster.ListItems.Item(X).ListSubItems(13) = " " Then
     Count = Count + 1
    End If
  End If
Next X

If checked = 1 Then
MsgBox "Only Accounts with Null Status can be tagged as not erroneous"
ElseIf checked = 0 And Count > 0 Then
ans = MsgBox("Are you sure you want to tag this account as not erroneous?", vbYesNo, "Tag as Not Error")

If ans = 6 Then
'UPDATE CONDITION AS NOT ERRONEOUS
   For X = 1 To lsvMaster.ListItems.Count
     If lsvMaster.ListItems(X).checked = True Then
       MRU = lsvMaster.ListItems.Item(X).ListSubItems(1)
       ACCTNUM = lsvMaster.ListItems.Item(X).ListSubItems(2)
       SQLUpdate = "UPDATE dbo.T_VER_EXTRACT SET CONDITION = 3 WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "' AND ULCYCLE = " & valCycle
       DBExecute SQLUpdate, "", ""
     End If
   Next X
   If Count <> 0 Then
   MsgBox Count & " Records Updated!", vbInformation
   genMasterList
   checked = 0
   Count = 0
   End If
 End If
End If

End Sub

Private Sub DTPicker1_Change()
   valDate1 = DTPicker1.Value
   ClearComboExtract
   genlsvExtract
End Sub

Private Sub DTPicker2_Change()
   valDate2 = DTPicker2.Value
   ClearComboExtract
   genlsvExtract
End Sub

Private Sub DTPicker3_Change()
   valDate3 = DTPicker3.Value
   ClearComboMaster
   genMasterList
End Sub

Private Sub DTPicker4_Change()
   valDate4 = DTPicker4.Value
   ClearComboMaster
   genMasterList
End Sub

Private Sub Form_Activate()
If WindowState = vbNormal Then
    Me.Move (0)
    Me.Top = 0
    Me.Height = 10325
    Me.Width = 15290
End If
'Me.Height = 11625
'Me.Width = 15825

'initialize form
InitForm
'check parameters for extraction
checkParam
'generate extracted accounts
genlsvExtract
genMasterList
'ClearComboMaster
cmbMConclusion.AddItem "ALL"
'cmbMConclusion.AddItem ""

End Sub

Private Sub InitForm()
cmbCycle.Text = Month(Date)
DTPicker1.Value = Date
DTPicker2.Value = Date
DTPicker3.Value = Date
DTPicker4.Value = Date

End Sub

Function genlsvExtract()
Dim rsExtract As ADODB.Recordset
Dim SQLextract As String
Dim l_obj_Item As Object

lsvExtract.ListItems.Clear


SQLextract = "EXEC sp_GEN_EXTRACTACCOUNTS_RDG " & CStr(valCycle) & ",'" & valBC & "','" & valMRU & "', '" & valRate & "' ,'" & valOC & "','" & valDate1 & "','" & valDate2 & "','" & valCondition & "','ALL',0"
'--EXEC sp_GEN_EXTRACTACCOUNTS_RDG 9, 'ALL', 'ALL', 'ALL', 'ALL','10/19/2008','10/19/2009','ALL','ALL',0
If OpenRecordset(rsExtract, SQLextract, "", "") Then
    While Not rsExtract.EOF
   Set l_obj_Item = lsvExtract.ListItems.Add(, , CheckNull(rsExtract.Fields("BC_DESC")))
            l_obj_Item.SubItems(1) = CheckNull(rsExtract.Fields("BOOKNO"))
            l_obj_Item.SubItems(2) = CheckNull(rsExtract.Fields("ACCTNUM"))
            l_obj_Item.SubItems(3) = CheckNull(rsExtract.Fields("METERNO"))
            l_obj_Item.SubItems(4) = CheckNull(rsExtract.Fields("RATE"))
            l_obj_Item.SubItems(5) = CheckNull(rsExtract.Fields("AVEUSAGE"))
            l_obj_Item.SubItems(6) = CheckNull(rsExtract.Fields("CONSUMPTION"))
            l_obj_Item.SubItems(7) = CheckNull(rsExtract.Fields("PREVOC"))
            l_obj_Item.SubItems(8) = CheckNull(rsExtract.Fields("PREVRDG"))
            l_obj_Item.SubItems(9) = CheckNull(rsExtract.Fields("BILL_PREVRDG"))
            l_obj_Item.SubItems(10) = CheckNull(rsExtract.Fields("PRESRDG"))
            l_obj_Item.SubItems(11) = CheckNull(rsExtract.Fields("FFCODE"))
            l_obj_Item.SubItems(12) = CheckNull(rsExtract.Fields("REMARKS"))
            l_obj_Item.SubItems(13) = CheckNull(rsExtract.Fields("TRIES"))
    Set l_obj_Item = Nothing
    
    'for the combo box
    genComboBC (rsExtract.Fields("BC_DESC"))
    genComboMRU (rsExtract.Fields("BOOKNO"))
    genComboOC (rsExtract.Fields("FFCODE"))
    genComboRate (rsExtract.Fields("RATE"))
    If rsExtract.Fields("PRESRDG") - rsExtract.Fields("BILL_PREVRDG") = 0 Then
        genComboCondition ("ZERO")
    ElseIf rsExtract.Fields("RANGECODE") = "-" Then
        genComboCondition ("NEGATIVE")
    ElseIf rsExtract.Fields("RANGECODE") = "3" Then
        genComboCondition ("VERY LOW")
    ElseIf rsExtract.Fields("RANGECODE") = "4" Then
        genComboCondition ("VERY HIGH")
    End If
    
    rsExtract.MoveNext
    Wend
rsExtract.Close
End If

lblCountExtract.Caption = lsvExtract.ListItems.Count
cmbBusinessCenter.Text = valBC
cmbOC.Text = valOC
cmbMRU.Text = valMRU
cmbRate.Text = valRate
cmbCondition.Text = valCondition
End Function
Private Sub genComboBC(bcname As String)
Dim X As Integer
Dim checker As Integer

checker = 0
For X = 0 To cmbBusinessCenter.ListCount
   If bcname = cmbBusinessCenter.List(X) Then
    checker = 1
   End If
Next X

If checker = 0 Then cmbBusinessCenter.AddItem bcname

End Sub

Private Sub genComboMBC(mbcname As String)
Dim X As Integer
Dim checker As Integer

checker = 0
For X = 0 To cmbMBC.ListCount
   If mbcname = cmbMBC.List(X) Then
    checker = 1
   End If
Next X

If checker = 0 Then cmbMBC.AddItem mbcname

End Sub

Private Sub genComboCondition(scondition As String)
Dim X As Integer
Dim checker As Integer

checker = 0
For X = 0 To cmbCondition.ListCount
    If scondition = cmbCondition.List(X) Then
      checker = 1
    End If
Next X

If checker = 0 Then cmbCondition.AddItem scondition
End Sub

Private Sub genComboRate(srate As String)
Dim X As Integer
Dim checker As Integer

checker = 0
For X = 0 To cmbRate.ListCount
    If srate = cmbRate.List(X) Then
      checker = 1
    End If
Next X

If checker = 0 Then cmbRate.AddItem srate
End Sub

Private Sub genComboMConclusion(mconclusion As String)
Dim X As Integer
Dim checker As Integer

checker = 0
For X = 0 To cmbMConclusion.ListCount
   If mconclusion = cmbMConclusion.List(X) Then
    checker = 1
   End If
Next X

If checker = 0 Then cmbMConclusion.AddItem mconclusion

End Sub
Private Sub genComboMRate(mrate As String)
Dim X As Integer
Dim checker As Integer

checker = 0
For X = 0 To cmbMRate.ListCount
   If mrate = cmbMRate.List(X) Then
    checker = 1
   End If
Next X

If checker = 0 Then cmbMRate.AddItem mrate

End Sub

Private Sub genComboMRU(booknum As String)
Dim X As Integer
Dim checker As Integer

checker = 0
For X = 0 To cmbMRU.ListCount
    If booknum = cmbMRU.List(X) Then
      checker = 1
    End If
Next X

If checker = 0 Then cmbMRU.AddItem booknum
End Sub

Private Sub genComboOC(soc As String)
Dim X As Integer
Dim checker As Integer

checker = 0
For X = 0 To cmbOC.ListCount
   If soc = cmbOC.List(X) Then
    checker = 1
   End If
Next X

If checker = 0 Then cmbOC.AddItem soc

End Sub
Private Sub genComboMOC(moc As String)
Dim X As Integer
Dim checker As Integer

checker = 0
For X = 0 To cmbMOC.ListCount
   If moc = cmbMOC.List(X) Then
    checker = 1
   End If
Next X

If checker = 0 Then cmbMOC.AddItem moc

End Sub

Private Sub genComboMMRU(booknum As String)
Dim X As Integer
Dim checker As Integer

checker = 0
For X = 0 To cmbMMRU.ListCount
    If booknum = cmbMMRU.List(X) Then
      checker = 1
    End If
Next X

If checker = 0 Then cmbMMRU.AddItem booknum
End Sub

Function checkParam()
valCycle = cmbCycle.Text
valDate1 = DTPicker1.Value
valDate2 = DTPicker2.Value
valDate3 = DTPicker3.Value
valDate4 = DTPicker4.Value

valBC = cmbBusinessCenter.Text
valMRU = cmbMRU.Text
valOC = cmbOC.Text
valRate = cmbRate.Text
valCondition = cmbCondition.Text
valMBC = cmbMBC.Text
valMMRU = cmbMMRU.Text
valMOC = cmbMOC.Text
valMRate = cmbMRate.Text
valMConclusion = cmbMConclusion.Text

If valCycle = Null Or valDate1 = Null Or valDate2 = Null Or valBC = Null Or valMRU = Null Or valOC = Null Then
MsgBox "Kindly supply all parameters needed to generate extracted accounts!", vbCritical
End If

End Function

Private Function SelectAll(ByVal DeOrSelectAll As Integer, ByVal Listbx As Integer) As Boolean ' Added the action that is needed
  Dim Item As ListItem

'to handle extracted window
If Listbx = 0 Then
  SelectAll = True ' Default succes
  Select Case DeOrSelectAll
    Case 0 ' Unchecked Check1
      For Each Item In lsvExtract.ListItems
        Item.checked = False ' Deselect
      Next
    Case 1 ' Checked Check1
      For Each Item In lsvExtract.ListItems
        Item.checked = True ' Select
      Next
    Case Else ' 2 = Grayed or else so no action for now....
      SelectAll = False ' No succes reported back...
  End Select
End If

'to handle masterlist window
If Listbx = 1 Then
  SelectAll = True ' Default succes
  Select Case DeOrSelectAll
    Case 0 ' Unchecked Check1
      For Each Item In lsvMaster.ListItems
        Item.checked = False ' Deselect
      Next
    Case 1 ' Checked Check1
      For Each Item In lsvMaster.ListItems
        Item.checked = True ' Select
      Next
    Case Else ' 2 = Grayed or else so no action for now....
      SelectAll = False ' No succes reported back...
  End Select
End If

End Function

Function genMasterList()
Dim rsExtract As ADODB.Recordset
Dim SQLextract As String
Dim l_obj_Item As Object
Dim sconclusion As String

lsvMaster.ListItems.Clear

If valMConclusion = " " Then
    sconclusion = "PENDING"
Else
    sconclusion = valMConclusion
End If

'SQLextract = "EXEC sp_GEN_EXTRACTACCOUNTS " & CStr(valCycle) & ",'" & valMBC & "','" & valMMRU & "','" & valMOC & "','" & valDate3 & "','" & valDate4 & "',1,'"""
SQLextract = "EXEC sp_GEN_EXTRACTACCOUNTS_RDG " & CStr(valCycle) & ",'" & valMBC & "','" & valMMRU & "', '" & valMRate & "' ,'" & valMOC & "','" & valDate3 & "','" & valDate4 & "','ALL','" & sconclusion & "', 2"
'--EXEC sp_GEN_EXTRACTACCOUNTS_RDG 9, 'ALL', 'ALL', 'ALL', 'ALL','10/19/2008','10/19/2009','ALL','ALL',0

If OpenRecordset(rsExtract, SQLextract, "", "") Then
    While Not rsExtract.EOF
   Set l_obj_Item = lsvMaster.ListItems.Add(, , CheckNull(rsExtract.Fields("BC_DESC")))
            l_obj_Item.SubItems(1) = CheckNull(rsExtract.Fields("BOOKNO"))
            l_obj_Item.SubItems(2) = CheckNull(rsExtract.Fields("ACCTNUM"))
            l_obj_Item.SubItems(3) = CheckNull(rsExtract.Fields("METERNO"))
            l_obj_Item.SubItems(4) = CheckNull(rsExtract.Fields("RATE"))
            l_obj_Item.SubItems(5) = CheckNull(rsExtract.Fields("AVEUSAGE"))
            l_obj_Item.SubItems(6) = CheckNull(rsExtract.Fields("CONSUMPTION"))
            l_obj_Item.SubItems(7) = CheckNull(rsExtract.Fields("PREVOC"))
            l_obj_Item.SubItems(8) = CheckNull(rsExtract.Fields("BILL_PREVRDG"))
            l_obj_Item.SubItems(9) = CheckNull(rsExtract.Fields("PRESRDG"))
            l_obj_Item.SubItems(10) = CheckNull(rsExtract.Fields("FFCODE"))
            l_obj_Item.SubItems(11) = CheckNull(rsExtract.Fields("REMARKS"))
            l_obj_Item.SubItems(12) = CheckNull(rsExtract.Fields("TRIES"))
            If rsExtract.Fields("CONDITION") = 1 Then
                l_obj_Item.SubItems(13) = "CORRECTED"
            ElseIf rsExtract.Fields("CONDITION") = 2 Then
                l_obj_Item.SubItems(13) = "ERRONEOUS"
            ElseIf rsExtract.Fields("CONDITION") = 3 Then
                l_obj_Item.SubItems(13) = "NOT ERRONEOUS"
            Else
                l_obj_Item.SubItems(13) = " "
            End If
    Set l_obj_Item = Nothing

    
    'for the combo box
    genComboMBC (rsExtract.Fields("BC_DESC"))
    genComboMMRU (rsExtract.Fields("BOOKNO"))
    genComboMOC (rsExtract.Fields("FFCODE"))
    genComboMRate (rsExtract.Fields("RATE"))
     
    If rsExtract.Fields("CONDITION") = 1 Then
        genComboMConclusion ("CORRECTED")
    ElseIf rsExtract.Fields("CONDITION") = 2 Then
        genComboMConclusion ("ERRONEOUS")
    ElseIf rsExtract.Fields("CONDITION") = 3 Then
        genComboMConclusion ("NOT ERRONEOUS")
    Else
        genComboMConclusion (" ")
    End If

    rsExtract.MoveNext
    Wend
rsExtract.Close

End If

lblCountMaster.Caption = lsvMaster.ListItems.Count
cmbMBC.Text = valMBC
cmbMMRU.Text = valMMRU
cmbMOC.Text = valMOC
cmbMRate.Text = valMRate
cmbMConclusion.Text = valMConclusion
'cmbMConclusion.AddItem "ALL"
End Function
