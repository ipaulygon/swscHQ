VERSION 5.00
Begin VB.Form frmRatesCode 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Rates Code Table"
   ClientHeight    =   6720
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6795
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6720
   ScaleWidth      =   6795
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   390
      Left            =   2400
      TabIndex        =   28
      Top             =   6165
      Width           =   915
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   390
      Left            =   3435
      TabIndex        =   29
      Top             =   6165
      Width           =   915
   End
   Begin VB.Frame Frame2 
      Height          =   3120
      Left            =   120
      TabIndex        =   43
      Top             =   2890
      Width           =   6520
      Begin VB.TextBox txtRateLim 
         Height          =   285
         Index           =   7
         Left            =   4820
         MaxLength       =   9
         TabIndex        =   27
         Top             =   2620
         Width           =   1500
      End
      Begin VB.TextBox txtRateLim 
         Height          =   285
         Index           =   6
         Left            =   4820
         MaxLength       =   9
         TabIndex        =   25
         Top             =   1940
         Width           =   1500
      End
      Begin VB.TextBox txtRateLim 
         Height          =   285
         Index           =   5
         Left            =   4820
         MaxLength       =   9
         TabIndex        =   23
         Top             =   1260
         Width           =   1500
      End
      Begin VB.TextBox txtRateLim 
         Height          =   285
         Index           =   4
         Left            =   4820
         MaxLength       =   9
         TabIndex        =   21
         Top             =   580
         Width           =   1500
      End
      Begin VB.TextBox txtRateLim 
         Height          =   285
         Index           =   3
         Left            =   1400
         MaxLength       =   9
         TabIndex        =   19
         Top             =   2620
         Width           =   1500
      End
      Begin VB.TextBox txtRateLim 
         Height          =   285
         Index           =   2
         Left            =   1400
         MaxLength       =   9
         TabIndex        =   17
         Top             =   1940
         Width           =   1500
      End
      Begin VB.TextBox txtRateLim 
         Height          =   285
         Index           =   1
         Left            =   1400
         MaxLength       =   9
         TabIndex        =   15
         Top             =   1260
         Width           =   1500
      End
      Begin VB.TextBox txtRateLim 
         Height          =   285
         Index           =   0
         Left            =   1400
         MaxLength       =   9
         TabIndex        =   13
         Top             =   580
         Width           =   1500
      End
      Begin VB.TextBox txtLimit 
         Height          =   285
         Index           =   7
         Left            =   4800
         MaxLength       =   9
         TabIndex        =   26
         Top             =   2280
         Width           =   1500
      End
      Begin VB.TextBox txtLimit 
         Height          =   285
         Index           =   6
         Left            =   4820
         MaxLength       =   9
         TabIndex        =   24
         Top             =   1600
         Width           =   1500
      End
      Begin VB.TextBox txtLimit 
         Height          =   285
         Index           =   5
         Left            =   4820
         MaxLength       =   9
         TabIndex        =   22
         Top             =   920
         Width           =   1500
      End
      Begin VB.TextBox txtLimit 
         Height          =   285
         Index           =   4
         Left            =   4820
         MaxLength       =   9
         TabIndex        =   20
         Top             =   240
         Width           =   1500
      End
      Begin VB.TextBox txtLimit 
         Height          =   285
         Index           =   3
         Left            =   1400
         MaxLength       =   9
         TabIndex        =   18
         Top             =   2280
         Width           =   1500
      End
      Begin VB.TextBox txtLimit 
         Height          =   285
         Index           =   2
         Left            =   1400
         MaxLength       =   9
         TabIndex        =   16
         Top             =   1600
         Width           =   1500
      End
      Begin VB.TextBox txtLimit 
         Height          =   285
         Index           =   1
         Left            =   1400
         MaxLength       =   9
         TabIndex        =   14
         Top             =   920
         Width           =   1500
      End
      Begin VB.TextBox txtLimit 
         Height          =   285
         Index           =   0
         Left            =   1400
         MaxLength       =   9
         TabIndex        =   12
         Top             =   240
         Width           =   1500
      End
      Begin VB.Label lblRateLim 
         Alignment       =   1  'Right Justify
         Caption         =   "Rate Limit 8 : "
         Height          =   165
         Index           =   7
         Left            =   3000
         TabIndex        =   59
         Top             =   2630
         Width           =   1740
      End
      Begin VB.Label lblRateLim 
         Alignment       =   1  'Right Justify
         Caption         =   "Rate Limit 7 : "
         Height          =   165
         Index           =   6
         Left            =   3000
         TabIndex        =   58
         Top             =   1950
         Width           =   1740
      End
      Begin VB.Label lblRateLim 
         Alignment       =   1  'Right Justify
         Caption         =   "Rate Limit 6 : "
         Height          =   165
         Index           =   5
         Left            =   3000
         TabIndex        =   57
         Top             =   1270
         Width           =   1740
      End
      Begin VB.Label lblRateLim 
         Alignment       =   1  'Right Justify
         Caption         =   "Rate Limit 5 : "
         Height          =   165
         Index           =   4
         Left            =   3000
         TabIndex        =   56
         Top             =   590
         Width           =   1740
      End
      Begin VB.Label lblRateLim 
         Alignment       =   1  'Right Justify
         Caption         =   "Rate Limit 4 : "
         Height          =   165
         Index           =   3
         Left            =   120
         TabIndex        =   55
         Top             =   2630
         Width           =   1200
      End
      Begin VB.Label lblRateLim 
         Alignment       =   1  'Right Justify
         Caption         =   "Rate Limit 3 : "
         Height          =   165
         Index           =   2
         Left            =   120
         TabIndex        =   54
         Top             =   1950
         Width           =   1200
      End
      Begin VB.Label lblRateLim 
         Alignment       =   1  'Right Justify
         Caption         =   "Rate Limit 2 : "
         Height          =   165
         Index           =   1
         Left            =   120
         TabIndex        =   53
         Top             =   1270
         Width           =   1200
      End
      Begin VB.Label lblRateLim 
         Alignment       =   1  'Right Justify
         Caption         =   "Rate Limit 1 : "
         Height          =   165
         Index           =   0
         Left            =   120
         TabIndex        =   52
         Top             =   590
         Width           =   1200
      End
      Begin VB.Label lblLimit 
         Alignment       =   1  'Right Justify
         Caption         =   "Limit 8 : "
         Height          =   165
         Index           =   7
         Left            =   3000
         TabIndex        =   51
         Top             =   2290
         Width           =   1740
      End
      Begin VB.Label lblLimit 
         Alignment       =   1  'Right Justify
         Caption         =   "Limit 7 : "
         Height          =   165
         Index           =   6
         Left            =   3000
         TabIndex        =   50
         Top             =   1610
         Width           =   1740
      End
      Begin VB.Label lblLimit 
         Alignment       =   1  'Right Justify
         Caption         =   "Limit 6 : "
         Height          =   165
         Index           =   5
         Left            =   3000
         TabIndex        =   49
         Top             =   930
         Width           =   1740
      End
      Begin VB.Label lblLimit 
         Alignment       =   1  'Right Justify
         Caption         =   "Limit 5 : "
         Height          =   165
         Index           =   4
         Left            =   3000
         TabIndex        =   48
         Top             =   250
         Width           =   1740
      End
      Begin VB.Label lblLimit 
         Alignment       =   1  'Right Justify
         Caption         =   "Limit 4 : "
         Height          =   165
         Index           =   3
         Left            =   120
         TabIndex        =   47
         Top             =   2290
         Width           =   1200
      End
      Begin VB.Label lblLimit 
         Alignment       =   1  'Right Justify
         Caption         =   "Limit 3 : "
         Height          =   165
         Index           =   2
         Left            =   120
         TabIndex        =   46
         Top             =   1620
         Width           =   1200
      End
      Begin VB.Label lblLimit 
         Alignment       =   1  'Right Justify
         Caption         =   "Limit 2 : "
         Height          =   165
         Index           =   1
         Left            =   120
         TabIndex        =   45
         Top             =   930
         Width           =   1200
      End
      Begin VB.Label lblLimit 
         Alignment       =   1  'Right Justify
         Caption         =   "Limit 1 : "
         Height          =   165
         Index           =   0
         Left            =   120
         TabIndex        =   44
         Top             =   250
         Width           =   1200
      End
   End
   Begin VB.Frame Frame1 
      Height          =   2770
      Left            =   120
      TabIndex        =   30
      Top             =   60
      Width           =   6520
      Begin VB.TextBox txtMinCharge 
         Height          =   285
         Left            =   4820
         MaxLength       =   9
         TabIndex        =   11
         Top             =   2280
         Width           =   1500
      End
      Begin VB.TextBox txtMinCons 
         Height          =   285
         Left            =   4820
         MaxLength       =   9
         TabIndex        =   10
         Top             =   1940
         Width           =   1500
      End
      Begin VB.TextBox txtMinDemandCharge 
         Height          =   285
         Left            =   4820
         MaxLength       =   9
         TabIndex        =   9
         Top             =   1600
         Width           =   1500
      End
      Begin VB.TextBox txtMinDemand 
         Height          =   285
         Left            =   4820
         MaxLength       =   9
         TabIndex        =   8
         Top             =   1260
         Width           =   1500
      End
      Begin VB.TextBox txtDemandRate 
         Height          =   285
         Left            =   4820
         MaxLength       =   9
         TabIndex        =   7
         Top             =   920
         Width           =   1500
      End
      Begin VB.TextBox txtOtherRate 
         Height          =   285
         Index           =   3
         Left            =   1400
         MaxLength       =   9
         TabIndex        =   6
         Top             =   2280
         Width           =   1500
      End
      Begin VB.TextBox txtOtherRate 
         Height          =   285
         Index           =   2
         Left            =   1400
         MaxLength       =   9
         TabIndex        =   5
         Top             =   1940
         Width           =   1500
      End
      Begin VB.TextBox txtOtherRate 
         Height          =   285
         Index           =   1
         Left            =   1400
         MaxLength       =   9
         TabIndex        =   4
         Top             =   1600
         Width           =   1500
      End
      Begin VB.TextBox txtOtherRate 
         Height          =   285
         Index           =   0
         Left            =   1400
         MaxLength       =   9
         TabIndex        =   3
         Top             =   1260
         Width           =   1500
      End
      Begin VB.TextBox txtBasicRate 
         Height          =   285
         Left            =   1400
         MaxLength       =   9
         TabIndex        =   2
         Top             =   920
         Width           =   1500
      End
      Begin VB.TextBox txtRateDesc 
         Height          =   285
         Left            =   1400
         MaxLength       =   15
         TabIndex        =   1
         Top             =   580
         Width           =   1500
      End
      Begin VB.TextBox txtRateCode 
         Height          =   285
         Left            =   1400
         MaxLength       =   2
         TabIndex        =   0
         Top             =   240
         Width           =   1500
      End
      Begin VB.Label lblMinCharge 
         Alignment       =   1  'Right Justify
         Caption         =   "Min Charge : "
         Height          =   195
         Left            =   3000
         TabIndex        =   42
         Top             =   2290
         Width           =   1740
      End
      Begin VB.Label lblMinCons 
         Alignment       =   1  'Right Justify
         Caption         =   "Min Consumption : "
         Height          =   195
         Left            =   3000
         TabIndex        =   41
         Top             =   1950
         Width           =   1740
      End
      Begin VB.Label lblMinDemandCharge 
         Alignment       =   1  'Right Justify
         Caption         =   "Min Demand Charge : "
         Height          =   195
         Left            =   3000
         TabIndex        =   40
         Top             =   1610
         Width           =   1740
      End
      Begin VB.Label lblMinDemand 
         Alignment       =   1  'Right Justify
         Caption         =   "Min Demand : "
         Height          =   195
         Left            =   3000
         TabIndex        =   39
         Top             =   1270
         Width           =   1740
      End
      Begin VB.Label lblDemandRate 
         Alignment       =   1  'Right Justify
         Caption         =   "Demand Rate : "
         Height          =   195
         Left            =   3000
         TabIndex        =   38
         Top             =   930
         Width           =   1740
      End
      Begin VB.Label lblOtherRate 
         Alignment       =   1  'Right Justify
         Caption         =   "Other Rate 4 : "
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   37
         Top             =   2290
         Width           =   1200
      End
      Begin VB.Label lblOtherRate 
         Alignment       =   1  'Right Justify
         Caption         =   "Other Rate 3 : "
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   36
         Top             =   1950
         Width           =   1200
      End
      Begin VB.Label lblOtherRate 
         Alignment       =   1  'Right Justify
         Caption         =   "Other Rate 2 : "
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   35
         Top             =   1610
         Width           =   1200
      End
      Begin VB.Label lblOtherRate 
         Alignment       =   1  'Right Justify
         Caption         =   "Other Rate 1 : "
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   34
         Top             =   1270
         Width           =   1200
      End
      Begin VB.Label lblBasicRate 
         Alignment       =   1  'Right Justify
         Caption         =   "Basic Rate : "
         Height          =   195
         Left            =   120
         TabIndex        =   33
         Top             =   930
         Width           =   1200
      End
      Begin VB.Label lblRateDesc 
         Alignment       =   1  'Right Justify
         Caption         =   "Description : "
         Height          =   195
         Left            =   120
         TabIndex        =   32
         Top             =   590
         Width           =   1200
      End
      Begin VB.Label lblRateCode 
         Alignment       =   1  'Right Justify
         Caption         =   "Rate Code : "
         Height          =   195
         Left            =   120
         TabIndex        =   31
         Top             =   250
         Width           =   1200
      End
   End
End
Attribute VB_Name = "frmRatesCode"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdSave_Click()
    Dim l_int_Index, l_int_Ctr, l_int_Cnt As Integer
    Dim l_obj_Item As Object
    
    If Not ValidEntries Then Exit Sub
    If DuplicateRecords Then Exit Sub
    
    If g_str_AddEdit = "ADD" Then
        If Not SaveAdd Then Exit Sub
                
        Set l_obj_Item = frmEditBooks.lsvFileMain.ListItems.Add(, , Trim(txtRateCode))
            l_obj_Item.SubItems(1) = Trim(txtRateDesc)
            l_obj_Item.SubItems(2) = CheckNull(txtBasicRate, True)
        
            For l_int_Ctr = 0 To 3
                l_obj_Item.SubItems(l_int_Ctr + 3) = CheckNull(txtOtherRate(l_int_Ctr), True)
            Next
        
            l_obj_Item.SubItems(7) = CheckNull(txtDemandRate, True)
            l_obj_Item.SubItems(8) = CheckNull(txtMinDemand, True)
            l_obj_Item.SubItems(9) = CheckNull(txtMinDemandCharge, True)
            l_obj_Item.SubItems(10) = CheckNull(txtMinCons, True)
            l_obj_Item.SubItems(11) = CheckNull(txtMinCharge, True)
        
            l_int_Cnt = 0
            For l_int_Ctr = 12 To 27
                If l_int_Ctr Mod 2 = 0 Then
                    l_obj_Item.SubItems(l_int_Ctr) = CheckNull(txtLimit(l_int_Cnt), True)
                Else
                    l_obj_Item.SubItems(l_int_Ctr) = CheckNull(txtRateLim(l_int_Cnt), True)
                    l_int_Cnt = l_int_Cnt + 1
                End If
            Next
        Set l_obj_Item = Nothing
        
        frmEditBooks.cmdEdit.Enabled = True
        frmEditBooks.cmdDelete.Enabled = True
        
    Else
        If Not SaveEdit Then Exit Sub
        
        l_int_Index = frmEditBooks.lsvFileMain.SelectedItem.Index
        With frmEditBooks.lsvFileMain.ListItems(l_int_Index)
            .Text = Trim(txtRateCode)
            .SubItems(1) = Trim(txtRateDesc)
            .SubItems(2) = CheckNull(txtBasicRate, True)
        
            For l_int_Ctr = 0 To 3
                .SubItems(l_int_Ctr + 3) = CheckNull(txtOtherRate(l_int_Ctr), True)
            Next
        
            .SubItems(7) = CheckNull(txtDemandRate, True)
            .SubItems(8) = CheckNull(txtMinDemand, True)
            .SubItems(9) = CheckNull(txtMinDemandCharge, True)
            .SubItems(10) = CheckNull(txtMinCons, True)
            .SubItems(11) = CheckNull(txtMinCharge, True)
        
            l_int_Cnt = 0
            For l_int_Ctr = 12 To 27
                If l_int_Ctr Mod 2 = 0 Then
                    .SubItems(l_int_Ctr) = CheckNull(txtLimit(l_int_Cnt), True)
                Else
                    .SubItems(l_int_Ctr) = CheckNull(txtRateLim(l_int_Cnt), True)
                    l_int_Cnt = l_int_Cnt + 1
                End If
            Next
        End With
    End If
    
    frmEditBooks.lsvFileMain.Refresh
    Unload Me

End Sub

Function ValidEntries() As Boolean
    Dim l_int_Ctr As Integer
    
    ValidEntries = False
    If Not NumericEntry(txtBasicRate, lblBasicRate) Then Exit Function
    For l_int_Ctr = 0 To 3
        If Not NumericEntry(txtOtherRate(l_int_Ctr), lblOtherRate(l_int_Ctr)) Then Exit Function
    Next
    If Not NumericEntry(txtDemandRate, lblDemandRate) Then Exit Function
    If Not NumericEntry(txtMinDemand, lblMinDemand) Then Exit Function
    If Not NumericEntry(txtMinDemandCharge, lblMinDemandCharge) Then Exit Function
    If Not NumericEntry(txtMinCons, lblMinCons) Then Exit Function
    If Not NumericEntry(txtMinCharge, lblMinCharge) Then Exit Function
    For l_int_Ctr = 0 To 7
        If Not NumericEntry(txtLimit(l_int_Ctr), lblLimit(l_int_Ctr)) Then Exit Function
        If Not NumericEntry(txtRateLim(l_int_Ctr), lblRateLim(l_int_Ctr)) Then Exit Function
    Next
    ValidEntries = True
    
End Function

Function DuplicateRecords() As Boolean
    Dim l_str_Sql As String
    
    DuplicateRecords = True
    
    If g_str_AddEdit = "EDIT" Then
        With frmEditBooks.lsvFileMain.SelectedItem
            If .Text = Trim(txtRateCode) And .SubItems(1) = Trim(txtRateDesc) And _
                    .SubItems(2) = Trim(txtBasicRate) And .SubItems(3) = Trim(txtOtherRate(0)) And _
                    .SubItems(4) = Trim(txtOtherRate(1)) And .SubItems(5) = Trim(txtOtherRate(2)) And _
                    .SubItems(6) = Trim(txtOtherRate(3)) And .SubItems(7) = Trim(txtDemandRate) And _
                    .SubItems(8) = Trim(txtMinDemand) And .SubItems(9) = Trim(txtMinDemandCharge) And _
                    .SubItems(10) = Trim(txtMinCons) And .SubItems(11) = Trim(txtMinCharge) And _
                    .SubItems(12) = Trim(txtLimit(0)) And .SubItems(13) = Trim(txtRateLim(0)) And _
                    .SubItems(14) = Trim(txtLimit(1)) And .SubItems(15) = Trim(txtRateLim(1)) And _
                    .SubItems(16) = Trim(txtLimit(2)) And .SubItems(17) = Trim(txtRateLim(2)) And _
                    .SubItems(18) = Trim(txtLimit(3)) And .SubItems(19) = Trim(txtRateLim(3)) And _
                    .SubItems(20) = Trim(txtLimit(4)) And .SubItems(21) = Trim(txtRateLim(4)) And _
                    .SubItems(22) = Trim(txtLimit(5)) And .SubItems(23) = Trim(txtRateLim(5)) And _
                    .SubItems(24) = Trim(txtLimit(6)) And .SubItems(25) = Trim(txtRateLim(6)) And _
                    .SubItems(26) = Trim(txtLimit(7)) And .SubItems(27) = Trim(txtRateLim(7)) Then
                Unload Me
                Exit Function
            End If
        End With
    
        If frmEditBooks.lsvFileMain.SelectedItem.Text = Trim(txtRateCode) Then
            DuplicateRecords = False
            Exit Function
        End If
    End If
    
    l_str_Sql = "SELECT RATECODE FROM R_RATE WHERE RATECODE = '" & Trim(txtRateCode) & "'"
            
    If OpenRecordset(g_rs_RRATE, l_str_Sql, Me.Name, "DuplicateRecords") Then
        MsgBox "Record already exists!", vbExclamation, "Duplicate Record"
        g_rs_RRATE.Close
        Set g_rs_RRATE = Nothing
        txtRateCode.SetFocus
        Exit Function
    End If

    DuplicateRecords = False

End Function

Function SaveAdd() As Boolean
    Dim l_str_Sql As String
    
    l_str_Sql = "INSERT INTO R_RATE(RATECODE, RATEDESC, BASICRATE, OTHRATE1, " & _
        "OTHRATE2, OTHRATE3, OTHRATE4, DEMRATE, MINDEM) VALUES( '" & _
        Left(Trim(txtRateCode), 2) & "', '" & Left(Trim(txtRateDesc), 15) & "', " & _
        CheckNull(txtBasicRate, True) & ", " & CheckNull(txtOtherRate(0), True) & ", " & _
        CheckNull(txtOtherRate(1), True) & ", " & CheckNull(txtOtherRate(2), True) & ", " & _
        CheckNull(txtOtherRate(3), True) & ", " & CheckNull(txtDemandRate, True) & ", " & _
        CheckNull(txtMinDemand, True) & ")"
    
    SaveAdd = DBExecute(l_str_Sql, Me.Name, "SaveAdd")
    
    l_str_Sql = "UPDATE R_RATE SET MINDEMCHG = " & CheckNull(txtMinDemandCharge, True) & ", MINCONS = " & CheckNull(txtMinCons, True) & _
        ", MINCHARGE = " & CheckNull(txtMinCharge, True) & ", LIMIT_1 = " & CheckNull(txtLimit(0), True) & _
        ", RATE_LIMIT_1 = " & CheckNull(txtRateLim(0), True) & ", LIMIT_2 = " & CheckNull(txtLimit(1), True) & _
        ", RATE_LIMIT_2 = " & CheckNull(txtRateLim(1), True) & ", LIMIT_3 = " & CheckNull(txtLimit(2), True) & _
        " WHERE RATECODE = '" & Left(Trim(txtRateCode), 2) & "'"
    
    SaveAdd = DBExecute(l_str_Sql, Me.Name, "SaveAdd")

    l_str_Sql = "UPDATE R_RATE SET RATE_LIMIT_3 = " & CheckNull(txtRateLim(2), True) & ", LIMIT_4 = " & CheckNull(txtLimit(3), True) & _
        ", RATE_LIMIT_4 = " & CheckNull(txtRateLim(3), True) & ", LIMIT_5 = " & CheckNull(txtLimit(4), True) & _
        ", RATE_LIMIT_5 = " & CheckNull(txtRateLim(4), True) & ", LIMIT_6 = " & CheckNull(txtLimit(5), True) & _
        ", RATE_LIMIT_6 = " & CheckNull(txtRateLim(5), True) & ", LIMIT_7 = " & CheckNull(txtLimit(6), True) & _
        ", RATE_LIMIT_7 = " & CheckNull(txtRateLim(6), True) & ", LIMIT_8 = " & CheckNull(txtLimit(7), True) & _
        ", RATE_LIMIT_8 = " & CheckNull(txtRateLim(7), True) & _
        " WHERE RATECODE = '" & Left(Trim(txtRateCode), 2) & "'"
    
    SaveAdd = DBExecute(l_str_Sql, Me.Name, "SaveAdd")

End Function

Function SaveEdit() As Boolean
    Dim l_str_Sql As String
    
    l_str_Sql = "UPDATE R_RATE SET RATECODE = '" & Left(Trim(txtRateCode), 2) & _
        "', RATEDESC = '" & Left(Trim(txtRateDesc), 15) & "', BASICRATE = " & CheckNull(txtBasicRate, True) & _
        ", OTHRATE1 = " & CheckNull(txtOtherRate(0), True) & ", OTHRATE2 = " & CheckNull(txtOtherRate(1), True) & _
        ", OTHRATE3 = " & CheckNull(txtOtherRate(2), True) & ", OTHRATE4 = " & CheckNull(txtOtherRate(3), True) & _
        ", DEMRATE = " & CheckNull(txtDemandRate, True) & ", MINDEM = " & CheckNull(txtMinDemand, True) & _
        ", MINDEMCHG = " & CheckNull(txtMinDemandCharge, True) & ", MINCONS = " & CheckNull(txtMinCons, True) & _
        ", MINCHARGE = " & CheckNull(txtMinCharge, True) & ", LIMIT_1 = " & CheckNull(txtLimit(0), True) & _
        ", RATE_LIMIT_1 = " & CheckNull(txtRateLim(0), True) & ", LIMIT_2 = " & CheckNull(txtLimit(1), True) & _
        ", RATE_LIMIT_2 = " & CheckNull(txtRateLim(1), True) & ", LIMIT_3 = " & CheckNull(txtLimit(2), True) & _
        ", RATE_LIMIT_3 = " & CheckNull(txtRateLim(2), True) & ", LIMIT_4 = " & CheckNull(txtLimit(3), True) & _
        ", RATE_LIMIT_4 = " & CheckNull(txtRateLim(3), True) & ", LIMIT_5 = " & CheckNull(txtLimit(4), True) & _
        ", RATE_LIMIT_5 = " & CheckNull(txtRateLim(4), True) & ", LIMIT_6 = " & CheckNull(txtLimit(5), True) & _
        ", RATE_LIMIT_6 = " & CheckNull(txtRateLim(5), True) & ", LIMIT_7 = " & CheckNull(txtLimit(6), True) & _
        ", RATE_LIMIT_7 = " & CheckNull(txtRateLim(6), True) & ", LIMIT_8 = " & CheckNull(txtLimit(7), True) & _
        ", RATE_LIMIT_8 = " & CheckNull(txtRateLim(7), True) & _
        " WHERE RATECODE = '" & frmEditBooks.lsvFileMain.SelectedItem.Text & "'"
        
    SaveEdit = DBExecute(l_str_Sql, Me.Name, "SaveEdit")

End Function

Private Sub Form_Load()
    Dim l_int_Ctr, l_int_Cnt As Integer
    
If WindowState = vbNormal Then
    Me.Move (1000)
    Me.Top = 500
End If
    If g_str_AddEdit = "ADD" Then
        Me.Caption = "Add Rate"
        txtRateCode = ""
        txtRateDesc = ""
        txtBasicRate = ""
        For l_int_Ctr = 0 To 3
            txtOtherRate(l_int_Ctr) = ""
        Next
        txtDemandRate = ""
        txtMinDemand = ""
        txtMinDemandCharge = ""
        txtMinCons = ""
        txtMinCharge = ""
        For l_int_Ctr = 0 To 7
            txtLimit(l_int_Ctr) = ""
            txtRateLim(l_int_Ctr) = ""
        Next l_int_Ctr
    Else
        Me.Caption = "Edit Rate"
        With frmEditBooks.lsvFileMain.SelectedItem
            txtRateCode = .Text
            txtRateDesc = .SubItems(1)
            txtBasicRate = .SubItems(2)
            For l_int_Ctr = 0 To 3
                txtOtherRate(l_int_Ctr) = .SubItems(l_int_Ctr + 3)
            Next
            txtDemandRate = .SubItems(7)
            txtMinDemand = .SubItems(8)
            txtMinDemandCharge = .SubItems(9)
            txtMinCons = .SubItems(10)
            txtMinCharge = .SubItems(11)
            
            l_int_Cnt = 0
            For l_int_Ctr = 12 To 27
                If l_int_Ctr Mod 2 = 0 Then
                    txtLimit(l_int_Cnt) = .SubItems(l_int_Ctr)
                Else
                    txtRateLim(l_int_Cnt) = .SubItems(l_int_Ctr)
                    l_int_Cnt = l_int_Cnt + 1
                End If
            Next
        
        End With
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmEditBooks.Enabled = True
End Sub

Private Sub txt_Change()
    If Len(Trim(txtRateCode)) > 0 And Len(Trim(txtRateDesc)) > 0 Then
        cmdSave.Enabled = True
    Else
        cmdSave.Enabled = False
    End If

End Sub

Private Sub txtBasicRate_GotFocus()
    TextHighlight txtBasicRate
End Sub

Private Sub txtDemandRate_GotFocus()
    TextHighlight txtDemandRate
End Sub

Private Sub txtLimit_GotFocus(Index As Integer)
    TextHighlight txtLimit(Index)
End Sub

Private Sub txtMinCharge_GotFocus()
    TextHighlight txtMinCharge
End Sub

Private Sub txtMinCons_GotFocus()
    TextHighlight txtMinCons
End Sub

Private Sub txtMinDemand_GotFocus()
    TextHighlight txtMinDemand
End Sub

Private Sub txtMinDemandCharge_GotFocus()
    TextHighlight txtMinDemandCharge
End Sub

Private Sub txtOtherRate_GotFocus(Index As Integer)
    TextHighlight txtOtherRate(Index)
End Sub

Private Sub txtRateCode_Change()
    txt_Change
End Sub

Private Sub txtRateCode_GotFocus()
    TextHighlight txtRateCode
End Sub

Private Sub txtRateDesc_Change()
    txt_Change
End Sub

Private Sub txtRateDesc_GotFocus()
    TextHighlight txtRateDesc
End Sub

Private Sub txtRateLim_GotFocus(Index As Integer)
    TextHighlight txtRateLim(Index)
End Sub
