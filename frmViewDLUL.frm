VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmViewDLUL 
   Caption         =   "View Download File"
   ClientHeight    =   9720
   ClientLeft      =   60
   ClientTop       =   825
   ClientWidth     =   15150
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   9720
   ScaleWidth      =   15150
   Begin VB.CommandButton cmdClose 
      Caption         =   "&Close"
      Height          =   375
      Left            =   13080
      TabIndex        =   6
      Top             =   9120
      Width           =   1215
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      Height          =   375
      Left            =   11400
      TabIndex        =   5
      Top             =   9120
      Width           =   1335
   End
   Begin VB.Frame Frame1 
      Height          =   6975
      Left            =   240
      TabIndex        =   0
      Top             =   1800
      Width           =   14655
      Begin MSComctlLib.ListView lsvViewDLUL 
         Height          =   6615
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   14415
         _ExtentX        =   25426
         _ExtentY        =   11668
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   17
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Document No."
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   1
            Text            =   "Seq No."
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Serial No."
            Object.Width           =   3175
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Meter Brand"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "FF Code"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Account Name"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Address"
            Object.Width           =   8819
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Previous Remark"
            Object.Width           =   4586
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "Account No."
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   9
            Text            =   "Prev Rdg Date"
            Object.Width           =   2258
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   10
            Text            =   "Prev Reading"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   11
            Text            =   "Prev Consumption"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   12
            Text            =   "Average Usage"
            Object.Width           =   2381
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Text            =   "Rate"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   14
            Text            =   "Expected Reading"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   15
            Text            =   "Billed Prev Rdg"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   16
            Text            =   "Sched Rdg Date"
            Object.Width           =   2469
         EndProperty
      End
   End
   Begin VB.Label lblAccts 
      Caption         =   "No. of Accounts:"
      Height          =   255
      Left            =   8760
      TabIndex        =   12
      Top             =   360
      Width           =   3015
   End
   Begin VB.Label lblBusCtr 
      Caption         =   "Business Center:"
      Height          =   255
      Left            =   960
      TabIndex        =   11
      Top             =   360
      Width           =   6015
   End
   Begin VB.Label lblCycle 
      Caption         =   "Cycle/Month:"
      Height          =   255
      Left            =   960
      TabIndex        =   10
      Top             =   720
      Width           =   4095
   End
   Begin VB.Label lblMRU 
      Caption         =   "MRU #:"
      Height          =   255
      Left            =   960
      TabIndex        =   9
      Top             =   1080
      Width           =   4095
   End
   Begin VB.Label lblOOR 
      Caption         =   "Out of Range:"
      Height          =   255
      Left            =   8760
      TabIndex        =   8
      Top             =   1440
      Width           =   2295
   End
   Begin VB.Label lblUnread 
      Caption         =   "Unread:"
      Height          =   255
      Left            =   10560
      TabIndex        =   7
      Top             =   1080
      Width           =   2655
   End
   Begin VB.Label lblRead 
      Caption         =   "Read:"
      Height          =   255
      Left            =   8760
      TabIndex        =   3
      Top             =   1080
      Width           =   2295
   End
   Begin VB.Label lblInactive 
      Caption         =   "Inactive:"
      Height          =   255
      Left            =   10560
      TabIndex        =   2
      Top             =   720
      Width           =   2655
   End
   Begin VB.Label lblActive 
      Caption         =   "Active:"
      Height          =   255
      Left            =   8760
      TabIndex        =   1
      Top             =   720
      Width           =   2295
   End
End
Attribute VB_Name = "frmViewDLUL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim intCycle As Integer

' Return as much text as will fit in this width.
Private Function FittedText(ByVal txt As String, ByVal wid As Single) As String
    Do While Printer.TextWidth(txt) > wid
        txt = Left$(txt, Len(txt) - 1)
    Loop
    FittedText = txt
End Function

Private Sub cmdClose_Click()
    Unload Me
'    frmInquiry.Show
End Sub

Private Sub cmdPrint_Click()

Dim Appl As New CRAXDRT.Application
Dim Report As New CRAXDRT.Report
Dim i As Integer

If frmViewDLUL.Caption = "View Download File" Then
    g_str_rep = "VIEW DOWNLOAD FILE.rpt"
Else
    g_str_rep = "VIEW UPLOAD FILE.rpt"
End If

Set Appn = CreateObject("CrystalRunTime.Application")

Set cReport = Appn.OpenReport(App.Path & "\crystal reports\" & g_str_rep) 'Or get your report name into the report somehow

cReport.Database.LogOnServer "p2ssql.dll", "SZ-MWSI-SUBIC", "MCFSDB", "sa", "sqladmin"
For i = 1 To cReport.Database.Tables.Count
    cReport.Database.Tables(i).ConnectionProperties.Item("Password") = "sqladmin"
Next i

cReport.DiscardSavedData

cReport.ParameterFields(1).AddCurrentValue g_Company
cReport.ParameterFields(2).AddCurrentValue g_User
cReport.ParameterFields(3).AddCurrentValue Left(g_BillMonth, 4) & Padl(intBillCycle, 2, "0")
cReport.ParameterFields(4).AddCurrentValue sMRU
cReport.ParameterFields(5).AddCurrentValue intBillCycle

cReport.PrintOut

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''''    Printer.FontSize = 7
''''    Printer.Font = "MS Sans Serif"
''''    Printer.Orientation = 2 ' landscape
''''    Printer.Print " "
''''    Printer.Print " "
''''    Printer.CurrentX = 200
''''    Printer.CurrentY = 200
''''    Printer.Print lblBusCtr.Caption
''''    Printer.CurrentX = 6000
''''    Printer.CurrentY = 200
''''    Printer.Print lblAccts.Caption
''''
''''    Printer.CurrentX = 200
''''    Printer.CurrentY = 400
''''    Printer.Print "Cycle/Month : " & intCycle
''''    Printer.CurrentX = 6000
''''    Printer.CurrentY = 400
''''    Printer.Print lblActive.Caption
''''    Printer.CurrentX = 8000
''''    Printer.CurrentY = 400
''''    Printer.Print lblInactive.Caption
''''
''''    Printer.CurrentX = 200
''''    Printer.CurrentY = 600
''''    Printer.Print "MRU Number : " & sMRU
''''    Printer.CurrentX = 6000
''''    Printer.CurrentY = 600
''''    Printer.Print lblRead.Caption
''''    Printer.CurrentX = 8000
''''    Printer.CurrentY = 600
''''    Printer.Print lblUnread.Caption
''''
''''    Printer.CurrentX = 6000
''''    Printer.CurrentY = 800
''''    Printer.Print lblOOR.Caption
''''
''''    Printer.CurrentY = 0
''''    PrintMultiPageListView lsvViewDLUL
''''    Printer.EndDoc
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
End Sub
Private Sub CRViewer_CloseButtonClicked(UseDefault As Boolean)
Set cReport = Nothing
Set Appn = Nothing
End Sub

Private Sub PrintMultiPageListView(ByVal lvw As ListView)
' Use 1 inch margins.
Const TOP_MARGIN = 1100
Const LEFT_MARGIN = 0 '1440
Const COL_MARGIN = 240

Dim bottom_margin As Single
Dim xmax As Single
Dim num_cols As Integer
Dim column_header As ColumnHeader
Dim list_item As ListItem
Dim i As Integer
Dim num_subitems As Integer
Dim col_wid() As Single
Dim X As Single
Dim Y As Single
Dim line_hgt As Single
Dim need_top As Boolean
Dim start_new_page As Boolean

    bottom_margin = Printer.ScaleTop + _
        Printer.ScaleHeight - TOP_MARGIN

    ' Get column widths.
    If g_str_DLUL = "UL" Then
        num_cols = 15
    Else
        num_cols = lvw.ColumnHeaders.Count
    End If
    num_subitems = num_cols - 1
    ReDim col_wid(1 To num_cols)

    ' Check the column headers.
    For i = 1 To num_cols
        col_wid(i) = Printer.TextWidth(lvw.ColumnHeaders(i).Text)
    Next i

    ' Check the items.
    num_subitems = num_cols - 1
    For Each list_item In lvw.ListItems
        ' Check the item.
        If col_wid(1) < Printer.TextWidth(list_item.Text) Then _
           col_wid(1) = Printer.TextWidth(list_item.Text)

        ' Check the subitems.
        For i = 1 To num_subitems
            If col_wid(i + 1) < Printer.TextWidth(list_item.SubItems(i)) Then _
               col_wid(i + 1) = Printer.TextWidth(list_item.SubItems(i))
        Next i
    Next list_item

    ' Add a column margin.
    xmax = LEFT_MARGIN
    For i = 1 To num_cols
        col_wid(i) = col_wid(i) + COL_MARGIN
        xmax = xmax + col_wid(i)
    Next i

    ' Get a line's height.
    line_hgt = Printer.TextHeight("X")

    ' Print the pages.
    need_top = True         ' Start printing a new page.
    start_new_page = False  ' Use the NewPage method.
    For Each list_item In lvw.ListItems
        ' See if we need to print the top.
        If need_top Then
            need_top = False

            ' Start a new page if necessary.
            If start_new_page Then Printer.NewPage
            start_new_page = False

            ' Print the column headers.
            Printer.CurrentY = TOP_MARGIN + line_hgt / 2
            X = LEFT_MARGIN + COL_MARGIN / 2
            For i = 1 To num_cols
                Printer.CurrentX = X
                Printer.Print FittedText( _
                    lvw.ColumnHeaders(i).Text, col_wid(i));
                X = X + col_wid(i)
            Next i
            Printer.Print

            Y = Printer.CurrentY + line_hgt / 2
            Printer.Line (LEFT_MARGIN, Y)-(xmax, Y)
            Y = Y + line_hgt / 2
        End If

        ' Print the next item.
        ' Print the item.
        X = LEFT_MARGIN + COL_MARGIN / 2
        Printer.CurrentX = X
        Printer.CurrentY = Y
        Printer.Print FittedText(list_item.Text, col_wid(1));
        X = X + col_wid(1)

        ' Print the subitems.
        For i = 1 To num_subitems
            Printer.CurrentX = X
            Printer.Print FittedText( _
                list_item.SubItems(i), col_wid(i + 1));
            X = X + col_wid(i + 1)
        Next i
        Y = Y + line_hgt * 1.5

        ' See if we've reached the bottom of the page.
        If Y >= bottom_margin Then
            ' We've reached the bottom.
            ' Draw lines around it all.
            Printer.Line (LEFT_MARGIN, TOP_MARGIN)-(xmax, Y), , B

            X = LEFT_MARGIN
            For i = 1 To num_cols - 1
                X = X + col_wid(i)
                Printer.Line (X, TOP_MARGIN)-(X, Y)
            Next i

            ' Start a new page for the next item.
            need_top = True
            start_new_page = True
        End If
    Next list_item

    ' See if we finished the last page.
    If Not start_new_page Then
        ' We did not finish the last page.
        ' Draw lines around it all.
        Printer.Line (LEFT_MARGIN, TOP_MARGIN)-(xmax, Y), , B

        X = LEFT_MARGIN
        For i = 1 To num_cols - 1
            X = X + col_wid(i)
            Printer.Line (X, TOP_MARGIN)-(X, Y)
        Next i
    End If
End Sub

Private Sub Form_Load()
    Dim l_obj_Item As Object
    Dim i As Integer
    Dim l_int_Index As Integer
    Dim totaccts As Integer
    Dim read As Integer
    Dim rsDL As Recordset
    Dim rsTDL As Recordset
    Dim rsRead As Recordset
    Dim rsOOR As Recordset
    Dim rsUL As Recordset

If WindowState = vbNormal Then
    Me.Move (0)
    Me.Top = 0
    Me.Height = 10230
    Me.Width = 15270
End If
    OpenRecordset g_rs_TBOOK, "SELECT T_BOOK.BC_CODE, R_BUSCTR.BC_DESC, T_BKINFO.ACCTS, T_BKINFO.ACTIVE, T_BKINFO.INACTIVE FROM (R_BUSCTR INNER JOIN T_BOOK ON R_BUSCTR.BC_CODE = T_BOOK.BC_CODE) INNER JOIN T_BKINFO ON (T_BOOK.CYCLE = T_BKINFO.CYCLE) AND (T_BOOK.BOOKNO = T_BKINFO.BOOKNO) WHERE (((T_BOOK.BOOKNO)='" & sMRU & "') AND ((T_BOOK.CYCLE)=" & intBillCycle & "))", Me.Name, "Form_Load"
'    OpenRecordset g_rs_TBOOK, "SELECT T_BOOK.BC_CODE, R_BUSCTR.BC_DESC FROM R_BUSCTR INNER JOIN T_BOOK ON R_BUSCTR.BC_CODE = T_BOOK.BC_CODE Where T_BOOK.BookNo = '" & sMRU & "' And T_BOOK.cycle = " & intBillCycle & "", Me.Name, "Form_Load"
    'OpenRecordset g_rs_TBOOK, "SELECT T_BOOK.BC_CODE R_BUSCTR.BC_DESC FROM T_BOOK where BOOKNO='" & sMRU & "' and CYCLE=" & intBillCycle & "", Me.Name, "Form_Load"
    'intCycle = Right(g_BillMonth, 2)
    lblBusCtr.Caption = "Business Center : " & g_rs_TBOOK.Fields(0) & " - " & g_rs_TBOOK.Fields(1)
    lblCycle.Caption = "Cycle/Month : " & Padl(CStr(intBillCycle), 2, "0")
    'lblMRU.Caption = "MRU Number : " & frmMRUMngt.lsvMRUMngt.SelectedItem.Text
    lblMRU.Caption = "MRU Number : " & sMRU
    lsvViewDLUL.ListItems.Clear
    
If g_str_DLUL = "DL" Then

    'OpenRecordset g_rs_TDOWNLOAD, "SELECT Count(T_DOWNLOAD.BOOKNO) FROM T_DOWNLOAD where BOOKNO='" & sMRU & "' and DLCYCLE=" & intBillCycle & "", Me.Name, "Form_Load"
        lblAccts.Caption = "No. of Accounts: " & g_rs_TBOOK.Fields(2) 'g_rs_TDOWNLOAD.Fields(0)
        lblUnread.Caption = "Unread: " & g_rs_TBOOK.Fields(2) 'g_rs_TDOWNLOAD.Fields(0)

'    Set rsDL = New ADODB.Recordset
    'OpenRecordset g_rs_TDOWNLOAD, "SELECT Count(T_DOWNLOAD.BOOKNO) FROM T_DOWNLOAD where BOOKNO='" & sMRU & "' and DLCYCLE=" & intBillCycle & " and MTRTYPE='01'", Me.Name, "Form_Load"
    ' rsDL.Open "SELECT Count(T_DOWNLOAD.BOOKNO) FROM T_DOWNLOAD where BOOKNO='" & sMRU & "' and DLCYCLE=" & intBillCycle & " and MTRTYPE='01'", g_Conn, adOpenStatic, adLockReadOnly
        lblActive.Caption = "Active: " & g_rs_TBOOK.Fields(3) 'rsDL.Fields(0) 'g_rs_TDOWNLOAD.Fields(0)
    'OpenRecordset g_rs_TDOWNLOAD, "SELECT Count(T_DOWNLOAD.BOOKNO) FROM T_DOWNLOAD where BOOKNO='" & sMRU & "' and DLCYCLE=" & intBillCycle & " and MTRTYPE<>'01'", Me.Name, "Form_Load"
        lblInactive.Caption = "Inactive: " & g_rs_TBOOK.Fields(4) 'g_rs_TBOOK.Fields(2) - rsDL.Fields(0) 'g_rs_TDOWNLOAD.Fields(0)
        lblRead.Caption = "Read: 0"
        lblOOR.Caption = "Out of Range: 0"
'        rsDL.Close
'        Set rsDL = Nothing
    
    Set rsTDL = New ADODB.Recordset
    rsTDL.Open "SELECT * FROM T_DOWNLOAD where BOOKNO='" & sMRU & "' and DLCYCLE=" & intBillCycle & "", g_Conn, adOpenStatic, adLockReadOnly
    rsTDL.MoveFirst
    While Not rsTDL.EOF
        Set l_obj_Item = lsvViewDLUL.ListItems.Add(, , CheckNull(rsTDL.Fields(2))) ' dldoc_no
            l_obj_Item.SubItems(1) = CheckNull(rsTDL.Fields(3)) ' seqno
            l_obj_Item.SubItems(2) = CheckNull(rsTDL.Fields(4)) ' serialno
            l_obj_Item.SubItems(3) = CheckNull(rsTDL.Fields(6)) ' mtrbrand
            l_obj_Item.SubItems(4) = CheckNull(rsTDL.Fields(8)) ' ffcode
            l_obj_Item.SubItems(5) = CheckNull(rsTDL.Fields(9)) ' acctname
            l_obj_Item.SubItems(6) = CheckNull(rsTDL.Fields(10)) ' address
            'l_obj_Item.SubItems(5) = Left(CheckNull(g_rs_TDOWNLOAD.Fields(9)), 15) ' acctname
            'l_obj_Item.SubItems(6) = Left(CheckNull(g_rs_TDOWNLOAD.Fields(10)), 25) ' address
            l_obj_Item.SubItems(7) = CheckNull(rsTDL.Fields(11) & " " & rsTDL.Fields(12)) ' prevrem1 + prevrem2
            'l_obj_Item.SubItems(7) = Left(CheckNull(g_rs_TDOWNLOAD.Fields(11) & " " & g_rs_TDOWNLOAD.Fields(12)), 15) ' prevrem1 + prevrem2
            l_obj_Item.SubItems(8) = CheckNull(rsTDL.Fields(13)) ' acctnum
            l_obj_Item.SubItems(9) = CheckNull(rsTDL.Fields(15)) ' prdgdate
            l_obj_Item.SubItems(10) = CheckNull(rsTDL.Fields(16)) ' prevrdg
            l_obj_Item.SubItems(11) = CheckNull(rsTDL.Fields(17)) ' prevcons
            l_obj_Item.SubItems(12) = CheckNull(rsTDL.Fields(18)) ' aveusage
            l_obj_Item.SubItems(13) = CheckNull(rsTDL.Fields(19)) ' rate
            l_obj_Item.SubItems(14) = CheckNull(rsTDL.Fields(21)) ' expctdrdg
            l_obj_Item.SubItems(15) = CheckNull(rsTDL.Fields(23)) ' bill_prevrdg
            l_obj_Item.SubItems(16) = CheckNull(rsTDL.Fields(25)) ' sched_rdg_date
        Set l_obj_Item = Nothing
        rsTDL.MoveNext
    Wend

    rsTDL.Close
    Set rsTDL = Nothing
    
    'If Not OpenRecordset(g_rs_TDOWNLOAD, "SELECT * FROM T_DOWNLOAD where BOOKNO='" & frmMRUMngt.lsvMRUMngt.SelectedItem.Text & "' and DLCYCLE=" & intCycle & "", Me.Name, "Form_Load") Then Exit Sub
    'If Not OpenRecordset(g_rs_TDOWNLOAD, "SELECT * FROM T_DOWNLOAD where BOOKNO='" & sMRU & "' and DLCYCLE=" & intBillCycle & "", Me.Name, "Form_Load") Then Exit Sub
    'g_rs_TDOWNLOAD.MoveFirst
    'While Not g_rs_TDOWNLOAD.EOF
'        Set l_obj_Item = lsvViewDLUL.ListItems.Add(, , CheckNull(g_rs_TDOWNLOAD.Fields(2))) ' dldoc_no
'            l_obj_Item.SubItems(1) = CheckNull(g_rs_TDOWNLOAD.Fields(3)) ' seqno
'            l_obj_Item.SubItems(2) = CheckNull(g_rs_TDOWNLOAD.Fields(4)) ' serialno
'            l_obj_Item.SubItems(3) = CheckNull(g_rs_TDOWNLOAD.Fields(6)) ' mtrbrand
'            l_obj_Item.SubItems(4) = CheckNull(g_rs_TDOWNLOAD.Fields(8)) ' ffcode
'            l_obj_Item.SubItems(5) = CheckNull(g_rs_TDOWNLOAD.Fields(9)) ' acctname
'            l_obj_Item.SubItems(6) = CheckNull(g_rs_TDOWNLOAD.Fields(10)) ' address
'            'l_obj_Item.SubItems(5) = Left(CheckNull(g_rs_TDOWNLOAD.Fields(9)), 15) ' acctname
'            'l_obj_Item.SubItems(6) = Left(CheckNull(g_rs_TDOWNLOAD.Fields(10)), 25) ' address
'            l_obj_Item.SubItems(7) = CheckNull(g_rs_TDOWNLOAD.Fields(11) & " " & g_rs_TDOWNLOAD.Fields(12)) ' prevrem1 + prevrem2
'            'l_obj_Item.SubItems(7) = Left(CheckNull(g_rs_TDOWNLOAD.Fields(11) & " " & g_rs_TDOWNLOAD.Fields(12)), 15) ' prevrem1 + prevrem2
'            l_obj_Item.SubItems(8) = CheckNull(g_rs_TDOWNLOAD.Fields(13)) ' acctnum
'            l_obj_Item.SubItems(9) = CheckNull(g_rs_TDOWNLOAD.Fields(15)) ' prdgdate
'            l_obj_Item.SubItems(10) = CheckNull(g_rs_TDOWNLOAD.Fields(16)) ' prevrdg
'            l_obj_Item.SubItems(11) = CheckNull(g_rs_TDOWNLOAD.Fields(17)) ' prevcons
'            l_obj_Item.SubItems(12) = CheckNull(g_rs_TDOWNLOAD.Fields(18)) ' aveusage
'            l_obj_Item.SubItems(13) = CheckNull(g_rs_TDOWNLOAD.Fields(19)) ' rate
'            l_obj_Item.SubItems(14) = CheckNull(g_rs_TDOWNLOAD.Fields(21)) ' expctdrdg
'            l_obj_Item.SubItems(15) = CheckNull(g_rs_TDOWNLOAD.Fields(23)) ' bill_prevrdg
'            l_obj_Item.SubItems(16) = CheckNull(g_rs_TDOWNLOAD.Fields(25)) ' sched_rdg_date
'        Set l_obj_Item = Nothing
'        g_rs_TDOWNLOAD.MoveNext
'    Wend
'
'    g_rs_TDOWNLOAD.Close
'    Set g_rs_TDOWNLOAD = Nothing

Else
    
'    OpenRecordset g_rs_TUPLOAD, "SELECT Count(T_UPLOAD.BOOKNO) FROM T_UPLOAD where BOOKNO='" & sMRU & "' and ULCYCLE=" & intBillCycle & "", Me.Name, "Form_Load"
        totaccts = g_rs_TBOOK.Fields(2) 'g_rs_TUPLOAD.Fields(0)
        lblAccts.Caption = "No. of Accounts: " & g_rs_TBOOK.Fields(2) 'g_rs_TUPLOAD.Fields(0)
'    OpenRecordset g_rs_TUPLOAD, "SELECT Count(T_UPLOAD.BOOKNO) FROM T_UPLOAD where BOOKNO='" & sMRU & "' and ULCYCLE=" & intBillCycle & " and READTAG<>''", Me.Name, "Form_Load"
        lblActive.Caption = "Active: " & g_rs_TBOOK.Fields(3) 'g_rs_TUPLOAD.Fields(0)
'    OpenRecordset g_rs_TUPLOAD, "SELECT Count(T_UPLOAD.BOOKNO) FROM T_UPLOAD where BOOKNO='" & sMRU & "' and ULCYCLE=" & intBillCycle & " and READTAG=''", Me.Name, "Form_Load"
        lblInactive.Caption = "Inactive: " & g_rs_TBOOK.Fields(4) 'g_rs_TUPLOAD.Fields(0)
'    OpenRecordset g_rs_TUPLOAD, "SELECT Count(T_UPLOAD.BOOKNO) FROM T_UPLOAD where BOOKNO='" & sMRU & "' and ULCYCLE=" & intBillCycle & " and (PRESRDG<>'0' or FFCODE<>'')", Me.Name, "Form_Load"
    Set rsRead = New ADODB.Recordset
     'rsRead.Open "SELECT Count(T_UPLOAD.BOOKNO) FROM T_UPLOAD where BOOKNO='" & sMRU & "' and ULCYCLE=" & intBillCycle & " and (PRESRDG<>'0' or FFCODE<>'')", g_Conn, adOpenStatic, adLockReadOnly
      rsRead.Open "SELECT Count(BOOKNO) FROM (SELECT a.* From t_upload a Where Not Exists " & _
        "(SELECT * From t_ver_extract b " & _
        "Where a.BookNo = B.BookNo And a.ulcycle = B.ulcycle " & _
        "And a.ACCTNUM = b.ACCTNUM and b.condition = 1) " & _
        "Union " & _
        "SELECT a.bookno, a.ulcycle, a.uldoc_no, " & _
        "a.ffcode, a.rangecode, a.deviceno, a.presrdg, " & _
        "a.rdgdate, a.rdgtime , a.METERNO, a.REMARKS, " & _
        "a.ACCTNUM, " & _
        "a.readtag, a.SEQNO, a.tries, a.newmtrbrand, " & _
        "a.newmtrnum From t_upload_his a Where Exists " & _
        "(SELECT * From t_ver_extract b " & _
        "Where a.BookNo = B.BookNo And a.ulcycle = B.ulcycle " & _
        "And a.ACCTNUM = b.ACCTNUM and b.condition = 1 and " & _
        "a.origdata=0)) as A where BOOKNO='" & sMRU & "' and ULCYCLE=" & intBillCycle & " and (READTAG = '01')", g_Conn, adOpenStatic, adLockReadOnly
        read = rsRead.Fields(0) 'g_rs_TUPLOAD.Fields(0)
        lblRead.Caption = "Read: " & read 'g_rs_TUPLOAD.Fields(0)
        lblUnread.Caption = "Unread: " & totaccts - read
        rsRead.Close
        Set rsRead = Nothing
'    OpenRecordset g_rs_TUPLOAD, "SELECT Count(T_UPLOAD.BOOKNO) FROM T_UPLOAD where BOOKNO='" & sMRU & "' and ULCYCLE=" & intBillCycle & " and (RANGECODE<>'' and RANGECODE<>'0')", Me.Name, "Form_Load"
    Set rsOOR = New ADODB.Recordset
     'rsOOR.Open "SELECT Count(T_UPLOAD.BOOKNO) FROM T_UPLOAD where BOOKNO='" & sMRU & "' and ULCYCLE=" & intBillCycle & " and (RANGECODE<>'' and RANGECODE<>'0')", g_Conn, adOpenStatic, adLockReadOnly
     rsOOR.Open "SELECT Count(BOOKNO) FROM (SELECT a.* From t_upload a Where Not Exists " & _
        "(SELECT * From t_ver_extract b " & _
        "Where a.BookNo = B.BookNo And a.ulcycle = B.ulcycle " & _
        "And a.ACCTNUM = b.ACCTNUM and b.condition = 1) " & _
        "Union " & _
        "SELECT a.bookno, a.ulcycle, a.uldoc_no, " & _
        "a.ffcode, a.rangecode, a.deviceno, a.presrdg, " & _
        "a.rdgdate, a.rdgtime , a.METERNO, a.REMARKS, " & _
        "a.ACCTNUM, " & _
        "a.readtag, a.SEQNO, a.tries, a.newmtrbrand, " & _
        "a.newmtrnum From t_upload_his a Where Exists " & _
        "(SELECT * From t_ver_extract b " & _
        "Where a.BookNo = B.BookNo And a.ulcycle = B.ulcycle " & _
        "And a.ACCTNUM = b.ACCTNUM and b.condition = 1 and " & _
        "a.origdata=0)) as A where BOOKNO='" & sMRU & "' and ULCYCLE=" & intBillCycle & " and (RANGECODE<>'' and RANGECODE<>'0')", g_Conn, adOpenStatic, adLockReadOnly
        lblOOR.Caption = "Out of Range: " & rsOOR.Fields(0) 'g_rs_TUPLOAD.Fields(0)
        rsOOR.Close
        Set rsOOR = Nothing
        
    frmViewDLUL.Caption = "View Upload File"
    
    lsvViewDLUL.ColumnHeaders.Item(16).Width = 0
    lsvViewDLUL.ColumnHeaders.Item(17).Width = 0
    
    lsvViewDLUL.ColumnHeaders.Item(2).Text = "FF Code"
    lsvViewDLUL.ColumnHeaders.Item(2).Width = 800
    lsvViewDLUL.ColumnHeaders.Item(2).Alignment = lvwColumnLeft
    lsvViewDLUL.ColumnHeaders.Item(3).Text = "Range"
    lsvViewDLUL.ColumnHeaders.Item(3).Width = 660
    lsvViewDLUL.ColumnHeaders.Item(3).Alignment = lvwColumnLeft
    lsvViewDLUL.ColumnHeaders.Item(4).Text = "Device No."
    lsvViewDLUL.ColumnHeaders.Item(4).Width = 1600
    lsvViewDLUL.ColumnHeaders.Item(4).Alignment = lvwColumnLeft
    lsvViewDLUL.ColumnHeaders.Item(5).Text = "Reading"
    lsvViewDLUL.ColumnHeaders.Item(5).Width = 900
    lsvViewDLUL.ColumnHeaders.Item(5).Alignment = lvwColumnRight
    lsvViewDLUL.ColumnHeaders.Item(6).Text = "Reading Date"
    lsvViewDLUL.ColumnHeaders.Item(6).Width = 1200
    lsvViewDLUL.ColumnHeaders.Item(6).Alignment = lvwColumnRight
    lsvViewDLUL.ColumnHeaders.Item(7).Text = "Rdg Time"
    lsvViewDLUL.ColumnHeaders.Item(7).Width = 900
    lsvViewDLUL.ColumnHeaders.Item(7).Alignment = lvwColumnRight
    lsvViewDLUL.ColumnHeaders.Item(8).Text = "Meter No."
    lsvViewDLUL.ColumnHeaders.Item(8).Width = 1500
    lsvViewDLUL.ColumnHeaders.Item(8).Alignment = lvwColumnLeft
    lsvViewDLUL.ColumnHeaders.Item(9).Text = "Remarks"
    lsvViewDLUL.ColumnHeaders.Item(9).Width = 2800
    lsvViewDLUL.ColumnHeaders.Item(9).Alignment = lvwColumnLeft
    lsvViewDLUL.ColumnHeaders.Item(10).Text = "Account No."
    lsvViewDLUL.ColumnHeaders.Item(10).Width = 1300
    lsvViewDLUL.ColumnHeaders.Item(10).Alignment = lvwColumnLeft
    lsvViewDLUL.ColumnHeaders.Item(11).Text = "Read Tag"
    lsvViewDLUL.ColumnHeaders.Item(11).Width = 900
    lsvViewDLUL.ColumnHeaders.Item(11).Alignment = lvwColumnLeft
    lsvViewDLUL.ColumnHeaders.Item(12).Text = "Sequence"
    lsvViewDLUL.ColumnHeaders.Item(12).Width = 950
    lsvViewDLUL.ColumnHeaders.Item(12).Alignment = lvwColumnRight
    lsvViewDLUL.ColumnHeaders.Item(13).Text = "Tries"
    lsvViewDLUL.ColumnHeaders.Item(13).Width = 580
    lsvViewDLUL.ColumnHeaders.Item(13).Alignment = lvwColumnRight
    lsvViewDLUL.ColumnHeaders.Item(14).Text = "New Meter No."
    lsvViewDLUL.ColumnHeaders.Item(14).Width = 1400
    lsvViewDLUL.ColumnHeaders.Item(14).Alignment = lvwColumnLeft
    lsvViewDLUL.ColumnHeaders.Item(15).Text = "New Meter Brand"
    lsvViewDLUL.ColumnHeaders.Item(15).Width = 1500
    lsvViewDLUL.ColumnHeaders.Item(15).Alignment = lvwColumnLeft
    
'    'If Not OpenRecordset(g_rs_TUPLOAD, "SELECT * FROM T_UPLOAD where BOOKNO='" & frmMRUMngt.lsvMRUMngt.SelectedItem.Text & "' and ULCYCLE=" & intCycle & "", Me.Name, "Form_Load") Then Exit Sub
'    If Not OpenRecordset(g_rs_TUPLOAD, "SELECT * FROM T_UPLOAD where BOOKNO='" & sMRU & "' and ULCYCLE=" & intBillCycle & "", Me.Name, "Form_Load") Then Exit Sub
'
'    g_rs_TUPLOAD.MoveFirst
'    While Not g_rs_TUPLOAD.EOF
'        Set l_obj_Item = lsvViewDLUL.ListItems.Add(, , CheckNull(g_rs_TUPLOAD.Fields(2)))
'            For i = 3 To 16
'                l_obj_Item.SubItems(i - 2) = CheckNull(g_rs_TUPLOAD.Fields(i))
'            Next i
'        Set l_obj_Item = Nothing
'        g_rs_TUPLOAD.MoveNext
'    Wend
'
'    g_rs_TUPLOAD.Close
'    Set g_rs_TUPLOAD = Nothing
    
        Set rsUL = New ADODB.Recordset
'     rsUL.Open "SELECT * FROM T_UPLOAD where BOOKNO='" & sMRU & "' and ULCYCLE=" & intBillCycle & "", g_Conn, adOpenStatic, adLockReadOnly
      rsUL.Open "SELECT * FROM (SELECT a.* From t_upload a Where Not Exists " & _
        "(SELECT * From t_ver_extract b " & _
        "Where a.BookNo = B.BookNo And a.ulcycle = B.ulcycle " & _
        "And a.ACCTNUM = b.ACCTNUM and b.condition = 1) " & _
        "Union " & _
        "SELECT a.bookno, a.ulcycle, a.uldoc_no, " & _
        "a.ffcode, a.rangecode, a.deviceno, a.presrdg, " & _
        "a.rdgdate, a.rdgtime , a.METERNO, a.REMARKS, " & _
        "a.ACCTNUM, " & _
        "a.readtag, a.SEQNO, a.tries, a.newmtrbrand, " & _
        "a.newmtrnum From t_upload_his a Where Exists " & _
        "(SELECT * From t_ver_extract b " & _
        "Where a.BookNo = B.BookNo And a.ulcycle = B.ulcycle " & _
        "And a.ACCTNUM = b.ACCTNUM and b.condition = 1 and " & _
        "a.origdata=0)) as A where BOOKNO='" & sMRU & "' and ULCYCLE=" & intBillCycle & "", g_Conn, adOpenStatic, adLockReadOnly
    rsUL.MoveFirst
    While Not rsUL.EOF
        Set l_obj_Item = lsvViewDLUL.ListItems.Add(, , CheckNull(rsUL.Fields(2)))
            For i = 3 To 16
                l_obj_Item.SubItems(i - 2) = CheckNull(rsUL.Fields(i))
            Next i
        Set l_obj_Item = Nothing
        rsUL.MoveNext
    Wend
        rsUL.Close
        Set rsUL = Nothing

    'get count from T_BKINFO
'    If OpenRecordset(g_rs_TBKINFO, "select * from T_BKINFO where BOOKNO='" & sMRU & "' and CYCLE=" & intCycle & "", Me.Name, "Form_Load") Then
'        lblAccts.Caption = "No. of Accounts: " & g_rs_TBKINFO.Fields(2)
'        lblActive.Caption = "Active: " & g_rs_TBKINFO.Fields(3)
'        lblInactive.Caption = "Inactive: " & g_rs_TBKINFO.Fields(4)
'        lblUnread.Caption = "Unread: " & g_rs_TBKINFO.Fields(5)
'        lblRead.Caption = "Read: " & g_rs_TBKINFO.Fields(6)
'        lblOOR.Caption = "Out of Range: " & g_rs_TBKINFO.Fields(7)
'    Else
'    End If

'    OpenRecordset g_rs_TDOWNLOAD, "SELECT Count(T_DOWNLOAD.BOOKNO) FROM T_DOWNLOAD where BOOKNO='" & sMRU & "' and DLCYCLE=" & intBillCycle & "", Me.Name, "Form_Load"
'        lblAccts.Caption = "No. of Accounts: " & g_rs_TDOWNLOAD.Fields(0)
'        lblUnread.Caption = "Unread: " & g_rs_TDOWNLOAD.Fields(0)
'    OpenRecordset g_rs_TDOWNLOAD, "SELECT Count(T_DOWNLOAD.BOOKNO) FROM T_DOWNLOAD where BOOKNO='" & sMRU & "' and DLCYCLE=" & intBillCycle & " and MTRTYPE='01'", Me.Name, "Form_Load"
'        lblActive.Caption = "Active: " & g_rs_TDOWNLOAD.Fields(0)
'    OpenRecordset g_rs_TDOWNLOAD, "SELECT Count(T_DOWNLOAD.BOOKNO) FROM T_DOWNLOAD where BOOKNO='" & sMRU & "' and DLCYCLE=" & intBillCycle & " and MTRTYPE<>'01'", Me.Name, "Form_Load"
'        lblInactive.Caption = "Inactive: " & g_rs_TDOWNLOAD.Fields(0)
'        lblRead.Caption = "Read: 0"
'        lblOOR.Caption = "Out of Range: 0"
'
'    'If Not OpenRecordset(g_rs_TDOWNLOAD, "SELECT * FROM T_DOWNLOAD where BOOKNO='" & frmMRUMngt.lsvMRUMngt.SelectedItem.Text & "' and DLCYCLE=" & intCycle & "", Me.Name, "Form_Load") Then Exit Sub
'    If Not OpenRecordset(g_rs_TDOWNLOAD, "SELECT * FROM T_DOWNLOAD where BOOKNO='" & sMRU & "' and DLCYCLE=" & intBillCycle & "", Me.Name, "Form_Load") Then Exit Sub
'    g_rs_TDOWNLOAD.MoveFirst
'    While Not g_rs_TDOWNLOAD.EOF
'        Set l_obj_Item = lsvViewDLUL.ListItems.Add(, , CheckNull(g_rs_TDOWNLOAD.Fields(2))) ' dldoc_no
'            l_obj_Item.SubItems(1) = CheckNull(g_rs_TDOWNLOAD.Fields(3)) ' seqno
'            l_obj_Item.SubItems(2) = CheckNull(g_rs_TDOWNLOAD.Fields(4)) ' serialno
'            l_obj_Item.SubItems(3) = CheckNull(g_rs_TDOWNLOAD.Fields(6)) ' mtrbrand
'            l_obj_Item.SubItems(4) = CheckNull(g_rs_TDOWNLOAD.Fields(8)) ' ffcode
'            l_obj_Item.SubItems(5) = CheckNull(g_rs_TDOWNLOAD.Fields(9)) ' acctname
'            l_obj_Item.SubItems(6) = CheckNull(g_rs_TDOWNLOAD.Fields(10)) ' address
'            'l_obj_Item.SubItems(5) = Left(CheckNull(g_rs_TDOWNLOAD.Fields(9)), 15) ' acctname
'            'l_obj_Item.SubItems(6) = Left(CheckNull(g_rs_TDOWNLOAD.Fields(10)), 25) ' address
'            l_obj_Item.SubItems(7) = CheckNull(g_rs_TDOWNLOAD.Fields(11) & " " & g_rs_TDOWNLOAD.Fields(12)) ' prevrem1 + prevrem2
'            'l_obj_Item.SubItems(7) = Left(CheckNull(g_rs_TDOWNLOAD.Fields(11) & " " & g_rs_TDOWNLOAD.Fields(12)), 15) ' prevrem1 + prevrem2
'            l_obj_Item.SubItems(8) = CheckNull(g_rs_TDOWNLOAD.Fields(13)) ' acctnum
'            l_obj_Item.SubItems(9) = CheckNull(g_rs_TDOWNLOAD.Fields(15)) ' prdgdate
'            l_obj_Item.SubItems(10) = CheckNull(g_rs_TDOWNLOAD.Fields(16)) ' prevrdg
'            l_obj_Item.SubItems(11) = CheckNull(g_rs_TDOWNLOAD.Fields(17)) ' prevcons
'            l_obj_Item.SubItems(12) = CheckNull(g_rs_TDOWNLOAD.Fields(18)) ' aveusage
'            l_obj_Item.SubItems(13) = CheckNull(g_rs_TDOWNLOAD.Fields(19)) ' rate
'            l_obj_Item.SubItems(14) = CheckNull(g_rs_TDOWNLOAD.Fields(21)) ' expctdrdg
'            l_obj_Item.SubItems(15) = CheckNull(g_rs_TDOWNLOAD.Fields(23)) ' bill_prevrdg
'            l_obj_Item.SubItems(16) = CheckNull(g_rs_TDOWNLOAD.Fields(25)) ' sched_rdg_date
'        Set l_obj_Item = Nothing
'        g_rs_TDOWNLOAD.MoveNext
'    Wend
'
'    g_rs_TDOWNLOAD.Close
'    Set g_rs_TDOWNLOAD = Nothing
'
'Else
'
'    OpenRecordset g_rs_TUPLOAD, "SELECT Count(T_UPLOAD.BOOKNO) FROM T_UPLOAD where BOOKNO='" & sMRU & "' and ULCYCLE=" & intBillCycle & "", Me.Name, "Form_Load"
'        totaccts = g_rs_TUPLOAD.Fields(0)
'        lblAccts.Caption = "No. of Accounts: " & g_rs_TUPLOAD.Fields(0)
'    OpenRecordset g_rs_TUPLOAD, "SELECT Count(T_UPLOAD.BOOKNO) FROM T_UPLOAD where BOOKNO='" & sMRU & "' and ULCYCLE=" & intBillCycle & " and READTAG<>''", Me.Name, "Form_Load"
'        lblActive.Caption = "Active: " & g_rs_TUPLOAD.Fields(0)
'    OpenRecordset g_rs_TUPLOAD, "SELECT Count(T_UPLOAD.BOOKNO) FROM T_UPLOAD where BOOKNO='" & sMRU & "' and ULCYCLE=" & intBillCycle & " and READTAG=''", Me.Name, "Form_Load"
'        lblInactive.Caption = "Inactive: " & g_rs_TUPLOAD.Fields(0)
'    OpenRecordset g_rs_TUPLOAD, "SELECT Count(T_UPLOAD.BOOKNO) FROM T_UPLOAD where BOOKNO='" & sMRU & "' and ULCYCLE=" & intBillCycle & " and (PRESRDG<>'0' or FFCODE<>'')", Me.Name, "Form_Load"
'        read = g_rs_TUPLOAD.Fields(0)
'        lblRead.Caption = "Read: " & g_rs_TUPLOAD.Fields(0)
'        lblUnread.Caption = "Unread: " & totaccts - read
'    OpenRecordset g_rs_TUPLOAD, "SELECT Count(T_UPLOAD.BOOKNO) FROM T_UPLOAD where BOOKNO='" & sMRU & "' and ULCYCLE=" & intBillCycle & " and (RANGECODE<>'' and RANGECODE<>'0')", Me.Name, "Form_Load"
'        lblOOR.Caption = "Out of Range: " & g_rs_TUPLOAD.Fields(0)
'
'    frmViewDLUL.Caption = "View Upload File"
'
'    lsvViewDLUL.ColumnHeaders.Item(16).Width = 0
'    lsvViewDLUL.ColumnHeaders.Item(17).Width = 0
'
'    lsvViewDLUL.ColumnHeaders.Item(2).Text = "FF Code"
'    lsvViewDLUL.ColumnHeaders.Item(2).Width = 800
'    lsvViewDLUL.ColumnHeaders.Item(2).Alignment = lvwColumnLeft
'    lsvViewDLUL.ColumnHeaders.Item(3).Text = "Range"
'    lsvViewDLUL.ColumnHeaders.Item(3).Width = 660
'    lsvViewDLUL.ColumnHeaders.Item(3).Alignment = lvwColumnLeft
'    lsvViewDLUL.ColumnHeaders.Item(4).Text = "Device No."
'    lsvViewDLUL.ColumnHeaders.Item(4).Width = 1600
'    lsvViewDLUL.ColumnHeaders.Item(4).Alignment = lvwColumnLeft
'    lsvViewDLUL.ColumnHeaders.Item(5).Text = "Reading"
'    lsvViewDLUL.ColumnHeaders.Item(5).Width = 900
'    lsvViewDLUL.ColumnHeaders.Item(5).Alignment = lvwColumnRight
'    lsvViewDLUL.ColumnHeaders.Item(6).Text = "Reading Date"
'    lsvViewDLUL.ColumnHeaders.Item(6).Width = 1200
'    lsvViewDLUL.ColumnHeaders.Item(6).Alignment = lvwColumnRight
'    lsvViewDLUL.ColumnHeaders.Item(7).Text = "Rdg Time"
'    lsvViewDLUL.ColumnHeaders.Item(7).Width = 900
'    lsvViewDLUL.ColumnHeaders.Item(7).Alignment = lvwColumnRight
'    lsvViewDLUL.ColumnHeaders.Item(8).Text = "Meter No."
'    lsvViewDLUL.ColumnHeaders.Item(8).Width = 1500
'    lsvViewDLUL.ColumnHeaders.Item(8).Alignment = lvwColumnLeft
'    lsvViewDLUL.ColumnHeaders.Item(9).Text = "Remarks"
'    lsvViewDLUL.ColumnHeaders.Item(9).Width = 2800
'    lsvViewDLUL.ColumnHeaders.Item(9).Alignment = lvwColumnLeft
'    lsvViewDLUL.ColumnHeaders.Item(10).Text = "Account No."
'    lsvViewDLUL.ColumnHeaders.Item(10).Width = 1300
'    lsvViewDLUL.ColumnHeaders.Item(10).Alignment = lvwColumnLeft
'    lsvViewDLUL.ColumnHeaders.Item(11).Text = "Read Tag"
'    lsvViewDLUL.ColumnHeaders.Item(11).Width = 900
'    lsvViewDLUL.ColumnHeaders.Item(11).Alignment = lvwColumnLeft
'    lsvViewDLUL.ColumnHeaders.Item(12).Text = "Sequence"
'    lsvViewDLUL.ColumnHeaders.Item(12).Width = 950
'    lsvViewDLUL.ColumnHeaders.Item(12).Alignment = lvwColumnRight
'    lsvViewDLUL.ColumnHeaders.Item(13).Text = "Tries"
'    lsvViewDLUL.ColumnHeaders.Item(13).Width = 580
'    lsvViewDLUL.ColumnHeaders.Item(13).Alignment = lvwColumnRight
'    lsvViewDLUL.ColumnHeaders.Item(14).Text = "New Meter No."
'    lsvViewDLUL.ColumnHeaders.Item(14).Width = 1400
'    lsvViewDLUL.ColumnHeaders.Item(14).Alignment = lvwColumnLeft
'    lsvViewDLUL.ColumnHeaders.Item(15).Text = "New Meter Brand"
'    lsvViewDLUL.ColumnHeaders.Item(15).Width = 1500
'    lsvViewDLUL.ColumnHeaders.Item(15).Alignment = lvwColumnLeft
'
'    'If Not OpenRecordset(g_rs_TUPLOAD, "SELECT * FROM T_UPLOAD where BOOKNO='" & frmMRUMngt.lsvMRUMngt.SelectedItem.Text & "' and ULCYCLE=" & intCycle & "", Me.Name, "Form_Load") Then Exit Sub
'    If Not OpenRecordset(g_rs_TUPLOAD, "SELECT * FROM T_UPLOAD where BOOKNO='" & sMRU & "' and ULCYCLE=" & intBillCycle & "", Me.Name, "Form_Load") Then Exit Sub
'
'    g_rs_TUPLOAD.MoveFirst
'    While Not g_rs_TUPLOAD.EOF
'        Set l_obj_Item = lsvViewDLUL.ListItems.Add(, , CheckNull(g_rs_TUPLOAD.Fields(2)))
'            For i = 3 To 16
'                l_obj_Item.SubItems(i - 2) = CheckNull(g_rs_TUPLOAD.Fields(i))
'            Next i
'        Set l_obj_Item = Nothing
'        g_rs_TUPLOAD.MoveNext
'    Wend
'
'    g_rs_TUPLOAD.Close
'    Set g_rs_TUPLOAD = Nothing
End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  '  frmMRUMngt.Enabled = True
End Sub
