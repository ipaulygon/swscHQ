VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmRCManager 
   Caption         =   "Reconnection Manager"
   ClientHeight    =   8115
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15240
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8115
   ScaleWidth      =   15240
   Begin VB.CommandButton cmdDiscUpload 
      Caption         =   "Send Upload to Subic Water"
      Height          =   375
      Left            =   240
      TabIndex        =   13
      Top             =   5040
      Width           =   2655
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Receive Upload File"
      Height          =   375
      Left            =   240
      TabIndex        =   9
      Top             =   4560
      Width           =   2655
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Generate Download File"
      Height          =   375
      Left            =   240
      TabIndex        =   8
      Top             =   4080
      Width           =   2655
   End
   Begin VB.Frame Frame6 
      Height          =   8055
      Left            =   3120
      TabIndex        =   7
      Top             =   0
      Width           =   13935
      Begin MSComctlLib.ListView lstViewAccount 
         Height          =   7695
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   13665
         _ExtentX        =   24104
         _ExtentY        =   13573
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "SERIES"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "ACCTNUM"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "ACCTNAME"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "ADDRESS"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "SERIALNO"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "RCDATE"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "STATUS"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Frame Frame5 
      Height          =   2535
      Left            =   120
      TabIndex        =   4
      Top             =   5520
      Width           =   2895
      Begin VB.ListBox lstFilesource 
         Height          =   1815
         ItemData        =   "frmRCManager.frx":0000
         Left            =   120
         List            =   "frmRCManager.frx":0002
         TabIndex        =   5
         Top             =   600
         Width           =   2655
      End
      Begin VB.Label Label5 
         Alignment       =   2  'Center
         BackColor       =   &H8000000A&
         Caption         =   "FILE SOURCE"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   2655
      End
   End
   Begin VB.CommandButton cmdView 
      Caption         =   "&View"
      Height          =   375
      Left            =   240
      TabIndex        =   3
      Top             =   3600
      Width           =   2655
   End
   Begin VB.Frame Frame1 
      Height          =   975
      Left            =   240
      TabIndex        =   0
      Top             =   0
      Width           =   2775
      Begin VB.ComboBox cmbBC 
         Height          =   315
         Left            =   120
         TabIndex        =   2
         Top             =   480
         Width           =   2535
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H8000000A&
         Caption         =   "DMZ CODE"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   2535
      End
   End
   Begin MSComCtl2.MonthView MonthView1 
      Height          =   2370
      Left            =   240
      TabIndex        =   11
      Top             =   1080
      Width           =   2700
      _ExtentX        =   4763
      _ExtentY        =   4180
      _Version        =   393216
      ForeColor       =   -2147483630
      BackColor       =   -2147483633
      Appearance      =   1
      StartOfWeek     =   60096513
      CurrentDate     =   40471
   End
   Begin VB.Label lblCount 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   15480
      TabIndex        =   12
      Top             =   2760
      Width           =   735
   End
End
Attribute VB_Name = "frmRCManager"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim dctype As String
Dim selDate As Date
Dim dcstatus As String
Dim BCCode As String
Dim rCount As Integer


Private Sub cmbBC_Click()
Dim sqlGenBC As String
Dim rsGenBC As ADODB.Recordset


Set rsGenBC = New ADODB.Recordset

If cmbBC.Text <> "ALL" Then
    sqlGenBC = "SELECT DMZCODE_ID FROM dbo.L_DMZCODE WHERE DMZ_DESC = '" & cmbBC.Text & "'"
    If OpenRecordset(rsGenBC, sqlGenBC, "", "") = True Then
        BCCode = rsGenBC.Fields("DMZCODE_ID")
    End If
Else
BCCode = "ALL"
End If


ResetSearch

End Sub
Private Sub ResetSearch()
lstFilesource.Clear
lstViewAccount.ListItems.Clear

Command2.Enabled = False
lblCount.Caption = "0"
End Sub



Private Sub cmdDiscUpload_Click()
Dim rsUpload As ADODB.Recordset
Dim sqlUpload As String
Dim selectedDate As String
Dim uploadString As String
Dim cdelim As String
Dim rsDIR As ADODB.Recordset
Dim Dfile As String
Dim filePath As String
Dim f As Object

Set rsUpload = New ADODB.Recordset
Set rsDIR = New ADODB.Recordset



If OpenRecordset(rsDIR, "SELECT DIRUPLD FROM S_PARAM WHERE COMPANY_NAME = 'SUBIC WATER'", "", "") Then
filePath = rsDIR.Fields("DIRUPLD")
End If
Set f = CreateObject("Scripting.FileSystemObject")

If Not f.FolderExists(filePath & "\RECON") Then f.CreateFolder (filePath & "\RECON") ' upload directory
Dfile = filePath & "\RECON\RCU_" & Padl(Month(selDate), 2, "0") & Padl(Day(selDate), 2, "0") & Mid(Year(selDate), 3, 2) & ".txt"
cdelim = "|"
selectedDate = Year(selDate) & Padl(Month(selDate), 2, "0") & Padl(Day(selDate), 2, "0")
sqlUpload = "SELECT ACCTNUM, SERIALNO,RECONDATE,RECONTIME,RCSTAT,INITIALRDG,RESP_PERSON,NONRC_CODE,REMARKS,SERVICEMAN FROM T_RECUPLOAD WHERE RECONDATE = '" & selectedDate & "'"

If OpenRecordset(rsUpload, sqlUpload, "", "") = True Then
    rsUpload.MoveFirst
Do While Not rsUpload.EOF
    
    uploadString = uploadString & rsUpload.Fields("ACCTNUM") & cdelim & rsUpload.Fields("SERIALNO") & cdelim & rsUpload.Fields("RECONDATE") & cdelim & rsUpload.Fields("RECONTIME") & cdelim & rsUpload.Fields("RCSTAT") & cdelim & rsUpload.Fields("INITIALRDG") & cdelim & rsUpload.Fields("RESP_PERSON") & cdelim & rsUpload.Fields("NONRC_CODE") & cdelim & rsUpload.Fields("REMARKS") & cdelim & rsUpload.Fields("SERVICEMAN") & vbCrLf
    rsUpload.MoveNext
Loop
End If
Open Dfile For Output As #1
Print #1, uploadString
Close #1
MsgBox "Upload Disconnection File Created!"
End Sub

Private Sub cmdView_Click()
genlsvExtract BCCode, selDate
End Sub

Private Sub Command2_Click()

Dim iCycle As Integer
Dim sMRU As String
Dim n As Integer
Dim iCount As Integer
Dim itm
Dim l_int_Index As Integer
Dim Dfile As String
Dim DFile1 As String
Dim DFile2 As String
Dim DfileFull As String
Dim sPath As String
Dim fs As Object, fso As Object
Dim BCDESC As String
Dim cdelim As String
Dim rsDL As Recordset
Dim rsGRID As Recordset
Dim jobid As String
Dim ACCTNUM As String
Dim dcdt As Date
Dim sqlstring2 As String
Dim disString As String
'On Error GoTo errhandler

'new code 10-18-2013 status check
Dim uStatus As String

BCDESC = cmbBC.Text
iCycle = MonthView1.Month
cdelim = "|"


    If Not OpenRecordset(g_rs_RBUSCTR, "SELECT WORKING_DIR, DMZ_DESC FROM dbo.L_DMZCODE", "", "") Then
        Exit Sub
    End If
    sPath = g_rs_RBUSCTR.Fields(0) & ""

'for testing
'sPath = "C:\HQ\Satellite"

Set fs = CreateObject("Scripting.FileSystemObject")

' create folder toso\bccode bcdesc\year\cycle monthname\sched mr day, i.e. TOSO\0100 Novaliches\2008\09 September\06
If Not fs.FolderExists(sPath) Then fs.CreateFolder (sPath) ' dl directory
If Not fs.FolderExists(sPath & "\RECON\TOSO\") Then fs.CreateFolder (sPath & "\RECON\TOSO\") ' TOSO folder
If Not fs.FolderExists(sPath & "\RECON\TOSO\" & BCCode & " " & BCDESC) Then fs.CreateFolder (sPath & "\RECON\TOSO\" & BCCode & " " & BCDESC) ' BC folder
If Not fs.FolderExists(sPath & "\RECON\TOSO\" & BCCode & " " & BCDESC & "\" & Year(Date)) Then fs.CreateFolder (sPath & "\RECON\TOSO\" & BCCode & " " & BCDESC & "\" & Year(Date)) ' year folder
If Not fs.FolderExists(sPath & "\RECON\TOSO\" & BCCode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0")))) Then fs.CreateFolder (sPath & "\RECON\TOSO\" & BCCode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0")))) ' cycle folder
If Not fs.FolderExists(sPath & "\RECON\TOSO\" & BCCode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0"))) & "\" & Format(Date, "dd")) Then fs.CreateFolder (sPath & "\RECON\TOSO\" & BCCode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0"))) & "\" & Format(Date, "dd")) ' sched mr day folder

DfileFull = "RC" & Year(selDate) & Format(selDate, "mm") & Format(selDate, "dd") & Hour(Now())
Dfile = sPath & "\RECON\TOSO\" & BCCode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0"))) & "\" & Format(Date, "dd") & "\" & "RC" & Year(selDate) & Format(selDate, "mm") & Format(selDate, "dd") & Hour(Now()) & ".txt"
DFile1 = sPath & "\RECON\TOSO\" & BCCode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0"))) & "\" & Format(Date, "dd") & "\" & DfileFull

Set rsDL = New ADODB.Recordset

Dim dldate As Date
Dim batchid As Integer

'CHECK IF DATAGRID HAS VALUES
If lstViewAccount.ListItems.Count > 0 Then
    'CHECK IF DLDATE AND BATCHID HAS BEEN USED

    Open Dfile For Output As #1
    
    For Each itm In lstViewAccount.ListItems
        With itm
            l_int_Index = .Index
            With lstViewAccount.ListItems(l_int_Index)
            'update the status of the account to downloaded 10-18-2013 jums
                If lstViewAccount.ListItems(l_int_Index).SubItems(6) = "" Then
                    disString = Padl(lstViewAccount.ListItems(l_int_Index), 6, "0") & cdelim & _
                    .SubItems(1) & cdelim & _
                    .SubItems(2) & cdelim & _
                    .SubItems(3) & cdelim & _
                    .SubItems(4)
                    
                Print #1, disString
                Else
                
                End If
            End With
        End With

    'ADDED UPDATE OF THE TAG STATUS 10-18-2013 JUMS
    uStatus = "Update t_recdownload set status = 1 " & _
    "where acctnum = '" & lstViewAccount.ListItems(l_int_Index).SubItems(1) & "' " & _
    "and rcdate = '" & lstViewAccount.ListItems(l_int_Index).SubItems(5) & "'"
    
    DBExecute uStatus, "", ""
    
    Next
    
     Close #1
    
'    'ADDED UPDATE OF THE TAG STATUS 10-18-2013 JUMS
'    uStatus = "Update t_recdownload set status = 1 where status = 0"
'    DBExecute uStatus, "", ""

    ' Rename the filename
    DfileFull = DfileFull & ".txt"
    DFile2 = DFile1 & ".txt"
    
    Set fso = CreateObject("Scripting.FileSystemObject")
    ' Delete file if exist
    'If fso.FileExists(DFile2) Then fso.DeleteFile DFile2
    
    'fso.CopyFile fcfile, fcfile2
    'Name Dfile As DFile2    ' Rename dfile.
    MsgBox "Download File Creation Successful!", vbInformation, ""

End If

errhandler:
If Err.Number <> 0 Then
    MsgBox Err.Description, , "Error"
End If

End Sub


Private Sub Command3_Click()
Dim sPath As String
Dim lsql As String
Dim iCycle As Integer
Dim strSQL As String


iCycle = str(intBillCycle)
lsql = "select DIRULRECON from S_PARAM where COMPANY_NAME='SUBIC WATER'"
If OpenRecordset(g_rs_BUSCTR, lsql, "frmMRUMngt", "cmdRSO_Click") = True Then
sPath = g_rs_BUSCTR.Fields(0) & ""
End If

If UploadRecon(sPath) Then
    MsgBox "Receive File from SO Successful!", vbInformation, "System Message"
End If

End Sub

Private Sub Command4_Click()

Dim xlApp As Excel.Application
Dim xlWb As Excel.Workbook
Dim SQLPending As String
Dim filenme As String
Dim destFolder As String
Dim rsPending As ADODB.Recordset
Dim rsdestFolder As ADODB.Recordset
Dim X As Integer


If BCCode = "" Then
MsgBox "Select Business Center"
Exit Sub
End If



Set rsPending = New ADODB.Recordset
Set rsdestFolder = New ADODB.Recordset
If OpenRecordset(rsdestFolder, "SELECT FOLDERDEST FROM R_PROCESS WHERE PROCESSDET = 'Disconnection - Pending'", "", "") = True Then
destFolder = rsdestFolder.Fields("FOLDERDEST")
End If



Set xlApp = New Excel.Application
Set xlWb = xlApp.Workbooks.Add
xlApp.Visible = False
xlWb.Sheets("Sheet1").Cells(1, 1).Value = "Meter Reading Unit"
xlWb.Sheets("Sheet1").Cells(1, 2).Value = "BP"
xlWb.Sheets("Sheet1").Cells(1, 3).Value = "Contract Account Number"
xlWb.Sheets("Sheet1").Cells(1, 4).Value = "Meter Number"
xlWb.Sheets("Sheet1").Cells(1, 5).Value = "Sequence"
xlWb.Sheets("Sheet1").Cells(1, 6).Value = "Customer Name"
xlWb.Sheets("Sheet1").Cells(1, 7).Value = "Customer Address"
xlWb.Sheets("Sheet1").Cells(1, 8).Value = "pua"
xlWb.Sheets("Sheet1").Cells(1, 9).Value = "Current"
xlWb.Sheets("Sheet1").Cells(1, 10).Value = "Installation Charge"
xlWb.Sheets("Sheet1").Cells(1, 11).Value = "Meter Charge"
xlWb.Sheets("Sheet1").Cells(1, 12).Value = "Reconnection Fee"
xlWb.Sheets("Sheet1").Cells(1, 13).Value = "Total"
xlWb.Sheets("Sheet1").Cells(1, 14).Value = "Pia"
xlWb.Sheets("Sheet1").Cells(1, 15).Value = "Prepay Adjustment"


    '--OPEN EXCEL FILE---


X = 2
SQLPending = "SELECT BOOKNO,BP,ACCTNUM,SERIALNO,SEQ,CUSTNAME,ADDRESS,PUA,CURRENT_AMT,INSTCHARGE,METERCHARGE,DUE_AMT,PIA,PREPAYADJ,RECONFEE FROM T_DISCDOWNLOAD WHERE STATUS = 'OPEN' AND BC_CODE = '" & BCCode & "'"
If OpenRecordset(rsPending, SQLPending, "", "") = True Then
rsPending.MoveFirst
Do While Not rsPending.EOF
xlWb.Sheets("Sheet1").Cells(X, 1).Value = rsPending.Fields("BOOKNO")
xlWb.Sheets("Sheet1").Cells(X, 2).Value = rsPending.Fields("BP")
xlWb.Sheets("Sheet1").Cells(X, 3).Value = rsPending.Fields("ACCTNUM")
xlWb.Sheets("Sheet1").Cells(X, 4).Value = rsPending.Fields("SERIALNO")
xlWb.Sheets("Sheet1").Cells(X, 5).Value = rsPending.Fields("SEQ")
xlWb.Sheets("Sheet1").Cells(X, 6).Value = rsPending.Fields("CUSTNAME")
xlWb.Sheets("Sheet1").Cells(X, 7).Value = rsPending.Fields("ADDRESS")
xlWb.Sheets("Sheet1").Cells(X, 8).Value = rsPending.Fields("PUA")
xlWb.Sheets("Sheet1").Cells(X, 9).Value = rsPending.Fields("CURRENT_AMT")
xlWb.Sheets("Sheet1").Cells(X, 10).Value = rsPending.Fields("INSTCHARGE")
xlWb.Sheets("Sheet1").Cells(X, 11).Value = rsPending.Fields("METERCHARGE")
xlWb.Sheets("Sheet1").Cells(X, 12).Value = rsPending.Fields("RECONFEE")
xlWb.Sheets("Sheet1").Cells(X, 13).Value = rsPending.Fields("DUE_AMT")
xlWb.Sheets("Sheet1").Cells(X, 14).Value = rsPending.Fields("PIA")
xlWb.Sheets("Sheet1").Cells(X, 15).Value = rsPending.Fields("PREPAYADJ")
X = X + 1
rsPending.MoveNext
Loop
End If

Set rsPending = Nothing
filenme = "DUN" & BCCode & "_" & Year(Now()) & Padl(Month(Now()), 2, "0") & Padl(Day(Now()), 2, "0") & "_P.xls"

DBExecute "EXEC sp_INSERT_FILESOURCE 'Disconnection - Pending','" & filenme & "'," & Count & ",'" & Now() & "'", "", ""
DBExecute "UPDATE T_DISCDOWNLOAD SET STATUS = 'FCLOSED', CLOSEDT = '" & Now() & "' WHERE BC_CODE = '" & BCCode & "' AND STATUS = 'OPEN'", "", ""

xlWb.Close True, destFolder & "\" & filenme
Set xlApp = Nothing
MsgBox "Pending List File Created with " & X - 1 & " records!"

End Sub

Private Sub Form_Activate()
Me.Width = 17265
Me.Height = 8625
GenBC
MonthView1.Value = Now
dctype = "BOTH"


End Sub



Private Sub GenBC()
Dim sqlGenBC As String
Dim rsGenBC As ADODB.Recordset

Set rsGenBC = New ADODB.Recordset

sqlGenBC = "SELECT DMZ_DESC FROM dbo.L_DMZCODE ORDER BY DMZCODE_ID"
If OpenRecordset(rsGenBC, sqlGenBC, "", "") = True Then
        cmbBC.AddItem "ALL"
    Do While Not rsGenBC.EOF
        cmbBC.AddItem rsGenBC("DMZ_DESC").Value
        rsGenBC.MoveNext
    Loop
End If

End Sub







Private Sub MonthView1_DateClick(ByVal DateClicked As Date)
selDate = MonthView1.Value
ResetSearch
End Sub

Private Sub optBoth_Click()
dctype = "BOTH"
ResetSearch
End Sub

Private Sub optDisconnect_Click()
dctype = "DIS"
ResetSearch
End Sub

Private Sub optValidation_Click()
dctype = "VAL"
ResetSearch
End Sub

Private Sub genlsvExtract(bcCode1 As String, dcdate As Date)
Dim rsExtract As ADODB.Recordset
Dim rsFileExtract As ADODB.Recordset
Dim SQLextract As String
Dim SQLFileExtract As String
Dim l_obj_Item As Object
Dim dcdate2 As String
'new code 10-18-2013 jums
Dim iStatus As Integer


rCount = 0
lstViewAccount.ListItems.Clear
lstFilesource.Clear

dcdate2 = Year(dcdate) & Padl(Month(dcdate), 2, "0") & Padl(Day(dcdate), 2, "0")

If bcCode1 <> "ALL" Then
SQLextract = "SELECT * FROM dbo.T_RECDOWNLOAD WHERE SUBSTRING(SERIES,1,3) = '" & bcCode1 & "' AND RCDATE = '" & dcdate2 & "'"
Else
SQLextract = "SELECT * FROM dbo.T_RECDOWNLOAD WHERE RCDATE = '" & dcdate2 & "'"
End If

If OpenRecordset(rsExtract, SQLextract, "", "") Then
    While Not rsExtract.EOF
   Set l_obj_Item = lstViewAccount.ListItems.Add(, , CheckNull(rsExtract.Fields("SERIES")))
            rCount = rCount + 1
            l_obj_Item.SubItems(1) = CheckNull(rsExtract.Fields("ACCTNUM"))
            l_obj_Item.SubItems(2) = CheckNull(rsExtract.Fields("ACCTNAME"))
            l_obj_Item.SubItems(3) = CheckNull(rsExtract.Fields("ADDRESS"))
            l_obj_Item.SubItems(4) = CheckNull(rsExtract.Fields("SERIALNO"))
            l_obj_Item.SubItems(5) = CheckNull(rsExtract.Fields("RCDATE"))
            'new code 10-18-2013 jums for adding status of the account
            iStatus = CheckNull(rsExtract.Fields("STATUS"))
                If iStatus = 0 Then
                    l_obj_Item.SubItems(6) = ""
                Else
                    l_obj_Item.SubItems(6) = "Downloaded"
                End If
            
            
    Set l_obj_Item = Nothing
    
    
    rsExtract.MoveNext
    Wend
    rsExtract.Close
    Command2.Enabled = True
    lblCount.Caption = rCount
Else
  MsgBox "No records found!"
  lblCount.Caption = "0"
End If


If bcCode1 <> "ALL" Then
    SQLFileExtract = "SELECT DISTINCT B.FILENAME,B.LINECOUNT FROM T_RECDOWNLOAD A INNER JOIN T_FILES B ON A.FILEID = B.FILENAMEID WHERE RCDATE = '" & dcdate2 & "' AND SUBSTRING(SERIES,1,3) = '" & bcCode1 & "'"
Else
    SQLFileExtract = "SELECT DISTINCT B.FILENAME,B.LINECOUNT FROM T_RECDOWNLOAD A INNER JOIN T_FILES B ON A.FILEID = B.FILENAMEID WHERE RCDATE = '" & dcdate2 & "'"
End If

If OpenRecordset(rsFileExtract, SQLFileExtract, "", "") = True Then
    rsFileExtract.MoveFirst
    Do While Not rsFileExtract.EOF
        lstFilesource.AddItem rsFileExtract.Fields("FILENAME")
        rsFileExtract.MoveNext
    Loop
End If


End Sub
