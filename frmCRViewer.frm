VERSION 5.00
Object = "{3C62B3DD-12BE-4941-A787-EA25415DCD27}#10.0#0"; "crviewer.dll"
Begin VB.Form frmCRViewer 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Report Viewer"
   ClientHeight    =   9780
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   15180
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9780
   ScaleWidth      =   15180
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Caption         =   "Current Report Parameters"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   120
      TabIndex        =   1
      Top             =   0
      Width           =   15000
      Begin VB.Label lblParameters 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   360
         Width           =   14535
      End
   End
   Begin CrystalActiveXReportViewerLib10Ctl.CrystalActiveXReportViewer CRViewer 
      Height          =   8775
      Left            =   120
      TabIndex        =   0
      Top             =   720
      Width           =   15015
      lastProp        =   600
      _cx             =   26485
      _cy             =   15478
      DisplayGroupTree=   0   'False
      DisplayToolbar  =   -1  'True
      EnableGroupTree =   0   'False
      EnableNavigationControls=   -1  'True
      EnableStopButton=   -1  'True
      EnablePrintButton=   -1  'True
      EnableZoomControl=   -1  'True
      EnableCloseButton=   -1  'True
      EnableProgressControl=   -1  'True
      EnableSearchControl=   -1  'True
      EnableRefreshButton=   0   'False
      EnableDrillDown =   -1  'True
      EnableAnimationControl=   -1  'True
      EnableSelectExpertButton=   0   'False
      EnableToolbar   =   -1  'True
      DisplayBorder   =   -1  'True
      DisplayTabs     =   -1  'True
      DisplayBackgroundEdge=   -1  'True
      SelectionFormula=   ""
      EnablePopupMenu =   -1  'True
      EnableExportButton=   0   'False
      EnableSearchExpertButton=   0   'False
      EnableHelpButton=   0   'False
      LaunchHTTPHyperlinksInNewBrowser=   -1  'True
      EnableLogonPrompts=   -1  'True
   End
End
Attribute VB_Name = "frmCRViewer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim boolNoData As Boolean
Private Sub GetParameters()
    Dim i As Integer
    Dim arrLabel(6) As String
    Dim sDesc As String, sDesc2 As String
    
    arrLabel(0) = "Business Center: "
    arrLabel(1) = "Cycle: "
    arrLabel(2) = "MRU: "
    arrLabel(3) = "Option: "
    arrLabel(4) = "Date From: "
    arrLabel(5) = "Date To: "
    arrLabel(6) = "No. of Consecutive Months: "
    
    'Get the passed parameters
    For i = 0 To UBound(arrReportParm)
        sDesc = arrReportParm(i)
        If sDesc <> "*" Then
            ' For Option
            If i = 3 Then
                If g_str_rep = "MRU STATUS REPORT.rpt" Then
                    Select Case sDesc
                        Case "1"
                            sDesc2 = "Scheduled MR Date"
                        Case "2"
                            sDesc2 = "Downloaded From MWSI"
                        Case "3"
                            sDesc2 = "Sent to Satellite Office"
                        Case "4"
                            sDesc2 = "Received From Satellite Office"
                        Case "5"
                            sDesc2 = "Forwarded to MWSI"
                        Case "6"
                            sDesc2 = "ALL"
                    End Select
                    sDesc = sDesc2
                End If
            End If
            arrLabel(i) = arrLabel(i) & sDesc
            lblParameters = lblParameters & arrLabel(i) & "; "
        End If
    Next
    
End Sub

Private Sub CRViewer_CloseButtonClicked(UseDefault As Boolean)
    Set cReport = Nothing
    Set Appn = Nothing
End Sub

Private Sub Form_Activate()
    If boolNoData Then
        MsgBox "No data found for the specified parameters!", vbExclamation, "Report Viewer"
        Unload Me
    End If
End Sub

Private Sub Form_Load()
Dim sMRU As String
Dim sBusCtr As String
Dim varRec As Variant
Dim Appl As New CRAXDRT.Application
Dim Report As New CRAXDRT.Report

If WindowState = vbNormal Then
    Me.Move (0)
    Me.Top = 0
End If
    Me.Caption = "Report Viewer - " & Left(g_str_rep, Len(g_str_rep) - 4)
    
    ' Parameters in arrReportParm from frmReportParm screen
    GetParameters
    
    Set Appn = CreateObject("CrystalRunTime.Application")

    Set cReport = Appn.OpenReport(App.Path & "\crystal reports\" & g_str_rep)
    
    cReport.Database.LogOnServer "p2ssql.dll", "SZ-MWSI-SUBIC", "MCFSDB", "sa", "sqladmin"
    For i = 1 To cReport.Database.Tables.Count
        cReport.Database.Tables(i).ConnectionProperties.Item("Password") = "sqladmin"
    Next i
    'CrystalActiveXReportViewer1.EnableGroupTree = False
    'CrystalActiveXReportViewer1.ReportSource = cReport
    'CrystalActiveXReportViewer1.ViewReport
    
    cReport.DiscardSavedData

    ' Common Parameters
    cReport.ParameterFields(1).AddCurrentValue g_Company
    cReport.ParameterFields(2).AddCurrentValue g_User
    cReport.ParameterFields(3).AddCurrentValue g_BillMonth
      
    'Passed Report Parameters from arrReportParm array
    
    'BC
    If arrReportParm(0) = "ALL" And g_str_rep <> "CONSECUTIVE MONTHS SAME OC REPORT.rpt" Then
        'If g_str_rep = "FOUND CONNECTED REPORT.rpt" Or g_str_rep = "MRU STATUS REPORT.rpt" Then
        If g_str_rep = "FOUND CONNECTED REPORT.rpt" Then
            cReport.ParameterFields(4).AddCurrentValue "*"
        Else
            cReport.ParameterFields(4).AddCurrentValue "%"
        End If
'        If g_str_rep = "FIELD FINDINGS SUMMARY REPORT.rpt" Then
'            cReport.ParameterFields(4).AddCurrentValue "%"
'        Else
'            cReport.ParameterFields(4).AddCurrentValue "*"
'        End If
    Else
        cReport.ParameterFields(4).AddCurrentValue Left$(arrReportParm(0), 4)
    End If
    
    'Cycle
    cReport.ParameterFields(5).AddCurrentValue Val(arrReportParm(1))
    
    'MRU
    If arrReportParm(2) = "ALL" And g_str_rep <> "CONSECUTIVE MONTHS SAME OC REPORT.rpt" Then
        'If g_str_rep = "FOUND CONNECTED REPORT.rpt" Or g_str_rep = "MRU STATUS REPORT.rpt" Then
        If g_str_rep = "FOUND CONNECTED REPORT.rpt" Then
            cReport.ParameterFields(6).AddCurrentValue "*"
        Else
            ' % counterpart of * to select all in sql server
            cReport.ParameterFields(6).AddCurrentValue "%"
        End If
'        If g_str_rep = "FIELD FINDINGS SUMMARY REPORT.rpt" Then
'            cReport.ParameterFields(6).AddCurrentValue "%"
'        Else
'            cReport.ParameterFields(6).AddCurrentValue "*"
'        End If

    Else
        cReport.ParameterFields(6).AddCurrentValue arrReportParm(2)
    End If
    
    'Option and Date Range
    If g_str_rep = "MRU STATUS REPORT.rpt" Then
       cReport.ParameterFields(7).AddCurrentValue arrReportParm(3)
       cReport.ParameterFields(8).AddCurrentRange CDate(arrReportParm(4)), CDate(arrReportParm(5)), _
                                crRangeIncludeLowerBound + crRangeIncludeUpperBound
    ElseIf g_str_rep = "EDITED READING REPORT.rpt" Then
        cReport.ParameterFields(7).AddCurrentValue Val(arrReportParm(3))
       cReport.ParameterFields(8).AddCurrentRange CDate(arrReportParm(4)), CDate(arrReportParm(5)), _
                                crRangeIncludeLowerBound + crRangeIncludeUpperBound
    ElseIf g_str_rep = "SUMMARY METER READING REPORT.rpt" Then
        cReport.ParameterFields(7).AddCurrentRange CDate(arrReportParm(4)), CDate(arrReportParm(5)), _
                                crRangeIncludeLowerBound + crRangeIncludeUpperBound
    ElseIf g_str_rep = "DETAILED METER READING REPORT.rpt" Then
        cReport.ParameterFields(7).AddCurrentRange CDate(arrReportParm(4)), CDate(arrReportParm(5)), _
                                crRangeIncludeLowerBound + crRangeIncludeUpperBound
    ElseIf g_str_rep = "REPLACED METER REPORT.rpt" Then
        cReport.ParameterFields(7).AddCurrentRange CDate(arrReportParm(4)), CDate(arrReportParm(5)), _
                                crRangeIncludeLowerBound + crRangeIncludeUpperBound
    ElseIf g_str_rep = "FIELD FINDINGS SUMMARY REPORT.rpt" Then
        'cReport.ParameterFields(7).AddCurrentValue Format(CStr(arrReportParm(4)), "mm/dd/yyyy")
        cReport.ParameterFields(7).AddCurrentValue CDate(arrReportParm(4))
        cReport.ParameterFields(8).AddCurrentValue CDate(arrReportParm(5))
    ElseIf g_str_rep = "OC SUMMARY VALIDATION REPORT.rpt" Then
        'cReport.ParameterFields(7).AddCurrentValue Format(CStr(arrReportParm(4)), "mm/dd/yyyy")
        cReport.ParameterFields(7).AddCurrentValue CDate(arrReportParm(4))
        cReport.ParameterFields(8).AddCurrentValue CDate(arrReportParm(5))
    ElseIf g_str_rep = "CONSECUTIVE MONTHS SAME OC REPORT.rpt" Then
        'cReport.ParameterFields(7).AddCurrentValue Format(CStr(arrRepotParm(4)), "mm/dd/yyyy")
        cReport.ParameterFields(7).AddCurrentValue CDate(arrReportParm(4))
        cReport.ParameterFields(8).AddCurrentValue CDate(arrReportParm(5))
        cReport.ParameterFields(9).AddCurrentValue arrReportParm(6)
    Else
        cReport.ParameterFields(7).AddCurrentRange CDate(arrReportParm(4)), CDate(arrReportParm(5)), _
                        crRangeIncludeLowerBound + crRangeIncludeUpperBound
    End If
    
    cReport.ReadRecords
    
    varRec = cReport.GetNextRows(0, 1)
    If IsEmpty(varRec(0, 0)) Then
        boolNoData = True
    Else
        boolNoData = False
    End If

    If Not boolNoData Then
        CRViewer.ReportSource = cReport
        CRViewer.EnableExportButton = True
    
        CRViewer.ViewReport
    End If
End Sub
