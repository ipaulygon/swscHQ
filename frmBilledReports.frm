VERSION 5.00
Object = "{3C62B3DD-12BE-4941-A787-EA25415DCD27}#10.0#0"; "crviewer.dll"
Begin VB.Form frmBilledReports 
   Caption         =   "Billing Reports"
   ClientHeight    =   10545
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15240
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   10545
   ScaleWidth      =   15240
   Begin VB.Frame Frame1 
      Caption         =   "Report Parameters"
      Height          =   855
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   15015
      Begin VB.ComboBox cmbBusCen 
         Height          =   315
         Left            =   10560
         TabIndex        =   10
         Text            =   "ALL"
         Top             =   360
         Width           =   2295
      End
      Begin VB.ComboBox cmbBillYear 
         Height          =   315
         ItemData        =   "frmBilledReports.frx":0000
         Left            =   7080
         List            =   "frmBilledReports.frx":002B
         TabIndex        =   8
         Top             =   360
         Width           =   1455
      End
      Begin VB.ComboBox cmbBillMonth 
         Height          =   315
         ItemData        =   "frmBilledReports.frx":007D
         Left            =   3840
         List            =   "frmBilledReports.frx":00A5
         TabIndex        =   6
         Top             =   360
         Width           =   2055
      End
      Begin VB.CommandButton cmdGenerate 
         Caption         =   "&Generate"
         Height          =   495
         Left            =   13320
         TabIndex        =   4
         Top             =   240
         Width           =   1215
      End
      Begin VB.ComboBox cmbReportType 
         Height          =   315
         ItemData        =   "frmBilledReports.frx":00D0
         Left            =   1320
         List            =   "frmBilledReports.frx":00E0
         TabIndex        =   3
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label Label4 
         Caption         =   "Business Center"
         Height          =   255
         Left            =   9120
         TabIndex        =   9
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "Bill Year"
         Height          =   255
         Left            =   6240
         TabIndex        =   7
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "BillMonth"
         Height          =   255
         Left            =   3000
         TabIndex        =   5
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Report Type"
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   480
         Width           =   1215
      End
   End
   Begin CrystalActiveXReportViewerLib10Ctl.CrystalActiveXReportViewer CRViewer 
      Height          =   9375
      Left            =   120
      TabIndex        =   0
      Top             =   1080
      Width           =   15015
      lastProp        =   600
      _cx             =   26485
      _cy             =   16536
      DisplayGroupTree=   0   'False
      DisplayToolbar  =   -1  'True
      EnableGroupTree =   0   'False
      EnableNavigationControls=   -1  'True
      EnableStopButton=   -1  'True
      EnablePrintButton=   -1  'True
      EnableZoomControl=   -1  'True
      EnableCloseButton=   -1  'True
      EnableProgressControl=   -1  'True
      EnableSearchControl=   -1  'True
      EnableRefreshButton=   0   'False
      EnableDrillDown =   -1  'True
      EnableAnimationControl=   -1  'True
      EnableSelectExpertButton=   0   'False
      EnableToolbar   =   -1  'True
      DisplayBorder   =   -1  'True
      DisplayTabs     =   -1  'True
      DisplayBackgroundEdge=   -1  'True
      SelectionFormula=   ""
      EnablePopupMenu =   -1  'True
      EnableExportButton=   0   'False
      EnableSearchExpertButton=   0   'False
      EnableHelpButton=   0   'False
      LaunchHTTPHyperlinksInNewBrowser=   -1  'True
      EnableLogonPrompts=   -1  'True
   End
End
Attribute VB_Name = "frmBilledReports"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ReportType As String
Dim billMonth As Integer
Dim billYear As Integer
Dim busCent As String






Private Sub cmbReportType_Click()
ReportType = cmbReportType.Text
If cmbReportType.Text = "SUMMARY" Then
cmbBusCen.Visible = False
Label4.Visible = False
Else
cmbBusCen.Visible = True
Label4.Visible = True
End If

End Sub

Private Sub cmdGenerate_Click()
Dim i As Integer


If cmbReportType.Text = "" Then
MsgBox "Please select Report type"
Exit Sub
End If
If cmbBusCen.Text = "ALL" Then
busCent = "ALL"
Else
busCent = Mid(cmbBusCen.Text, 1, 4)
End If



    GetParameters
    
    Set Appn = CreateObject("CrystalRunTime.Application")
If ReportType <> "" Then
If ReportType = "SUMMARY" Then

    Set cReport = Appn.OpenReport(App.Path & "\crystal reports\" & g_str_rep)
    
    cReport.Database.LogOnServer "p2ssql.dll", "SZ-MWSI-SUBIC", "MCFSDB", "sa", "sqladmin"
    For i = 1 To cReport.Database.Tables.Count
        cReport.Database.Tables(i).ConnectionProperties.Item("Password") = "sqladmin"
    Next i
   
    cReport.DiscardSavedData
    ' Common Parameters
    cReport.ParameterFields(1).AddCurrentValue billMonth
    cReport.ParameterFields(2).AddCurrentValue billYear
    CRViewer.ReportSource = cReport
    CRViewer.EnableExportButton = True
    CRViewer.ViewReport
Else
    
    Set cReport = Appn.OpenReport(App.Path & "\crystal reports\" & g_str_rep)
    
    cReport.Database.LogOnServer "p2ssql.dll", "SZ-MWSI-SUBIC", "MCFSDB", "sa", "sqladmin"
    For i = 1 To cReport.Database.Tables.Count
        cReport.Database.Tables(i).ConnectionProperties.Item("Password") = "sqladmin"
    Next i
   
    cReport.DiscardSavedData
    ' Common Parameters
    cReport.ParameterFields(1).AddCurrentValue billMonth
    cReport.ParameterFields(2).AddCurrentValue billYear
    cReport.ParameterFields(3).AddCurrentValue busCent
    cReport.ParameterFields(4).AddCurrentValue ReportType
    CRViewer.ReportSource = cReport
    CRViewer.EnableExportButton = True
    CRViewer.ViewReport

End If
End If

    

End Sub

Private Sub Form_Activate()
If WindowState = vbNormal Then
    'Me.Move (1000)
    'Me.Top = 500
    Me.Height = 11055
    Me.Width = 15435
End If
cmbBillYear.Text = Year(Now)
cmbBillMonth.Text = Month(Now)
load_BC


End Sub


Private Sub GetParameters()

'test
billMonth = Int(cmbBillMonth.Text)
billYear = Int(cmbBillYear.Text)

If cmbReportType.Text = "" Then
MsgBox "Please select the report type"
Exit Sub
Else
ReportType = cmbReportType.Text
End If

If ReportType = "SUMMARY" Then
g_str_rep = "BILLED REPORT SUMMARY.rpt"
Else: g_str_rep = "BILLED REPORT.rpt"
End If


End Sub


Public Sub load_BC()
    
On Error GoTo errhandler
    With cmbBusCen
        .Clear
        .AddItem "ALL"
        If OpenRecordset(g_rs_RBUSCTR, "select * from R_BUSCTR order by BC_CODE", "frmDailyBatchGen", "Daily Batch Generation") Then
            While Not g_rs_RBUSCTR.EOF
                .AddItem g_rs_RBUSCTR.Fields(0) & " - " & g_rs_RBUSCTR.Fields(1)
                g_rs_RBUSCTR.MoveNext
            Wend
        Else
            .ListIndex = 0
        End If
    End With
cmbBusCen.Text = "ALL"
errhandler:
If Err.Number <> 0 Then
    If Err.Number = 380 Then
    Else
        MsgBox Err.Description, , "System Message"
    End If
End If
End Sub

