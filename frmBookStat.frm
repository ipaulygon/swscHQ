VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmBookStat 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3930
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6435
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3930
   ScaleWidth      =   6435
   Begin VB.Frame Frame1 
      Caption         =   "Series Status Codes"
      Height          =   3015
      Left            =   3120
      TabIndex        =   3
      Top             =   120
      Width           =   3120
      Begin VB.Label Label6 
         Caption         =   "PAU -  Partial Uploaded"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   1560
         Width           =   2925
      End
      Begin VB.Label Label5 
         Caption         =   "SRD - Scheduled for Reading "
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   480
         Width           =   2925
      End
      Begin VB.Label Label4 
         Caption         =   "FWM - Forwarded to Subic Water"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   120
         TabIndex        =   7
         Top             =   2280
         Width           =   2565
      End
      Begin VB.Label Label3 
         Caption         =   "RSO  - Received from Satellite"
         ForeColor       =   &H0000C000&
         Height          =   300
         Left            =   120
         TabIndex        =   6
         Top             =   1920
         Width           =   2805
      End
      Begin VB.Label Label2 
         Caption         =   "SSO  -  Sent to Satellite"
         ForeColor       =   &H000000FF&
         Height          =   180
         Left            =   120
         TabIndex        =   5
         Top             =   1200
         Width           =   2805
      End
      Begin VB.Label Label1 
         Caption         =   "DLM -  Downloaded from Subic  Water"
         ForeColor       =   &H00C00000&
         Height          =   180
         Index           =   0
         Left            =   120
         TabIndex        =   4
         Top             =   840
         Width           =   2805
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Close"
      Height          =   390
      Left            =   3435
      TabIndex        =   2
      Top             =   3360
      Width           =   915
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&OK"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   390
      Left            =   2400
      TabIndex        =   0
      Top             =   3360
      Width           =   915
   End
   Begin MSComctlLib.ListView lsvBooks 
      Height          =   3015
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   2775
      _ExtentX        =   4895
      _ExtentY        =   5318
      View            =   3
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "MRU"
         Object.Width           =   2647
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Status"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Cycle"
         Object.Width           =   0
      EndProperty
   End
End
Attribute VB_Name = "frmBookStat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim iChecked As Integer
Dim MRUArray(1000) As String
Dim MRUList As String
Dim iCycle As Integer
Dim sMRU As String
Dim CycleArray(1000) As Integer

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOk_Click()
    Dim l_int_Ctr As Integer
    Dim n As Integer
    Dim intCycle As Integer
    Dim dlCount As Integer
    Dim ulCount As Integer
    Dim dlulCount As ADODB.Recordset
    Set dlulCount = New ADODB.Recordset
    Dim cnDB As Connection
    Set cnDB = New ADODB.Connection
    
    
    
    
    If Generate_MRU_List = False Then
        Exit Sub
    End If
    
    cnDB.ConnectionString = "Provider=SQLOLEDB.1; Data Source=" & g_DataSource & ";database=" & g_Database & "; Initial Catalog=;User Id=sa;Password=sqladmin;"
    cnDB.Open
    
    
    'sMRU = lsvBooks.SelectedItem.Text

            For l_int_Ctr = 1 To lsvBooks.ListItems.Count
                If lsvBooks.ListItems(l_int_Ctr).Selected = True Then
'                    If lsvBooks.ListItems(l_int_Ctr).SubItems(1) = "RSO" Or lsvBooks.ListItems(l_int_Ctr).SubItems(1) = "FWM" Then
'                        If Not UpldBookToHost(lsvBooks.ListItems(l_int_Ctr).Text) Then
'                            Exit Sub
'                        Else
'                            MsgBox lsvBooks.ListItems(l_int_Ctr).Text & " - Book Uploaded to Host!", vbInformation, "Upload Books to Host"
'                        End If
'                    End If

'                    If lsvBooks.ListItems(l_int_Ctr).SubItems(1) = "RSO" Then
                        n = 1
                        For n = 1 To iChecked
                            sMRU = MRUArray(n)
                            intCycle = CycleArray(n)
                            If UploadToHost(sMRU, intCycle, "|") = True Then 'UploadToHost(BookNo, "~")
                                'OpenRecordset g_rs_TSCHED, "SELECT BKSTCODE FROM T_SCHED WHERE BOOKNO = '" & sMRU & "' and CYCLE= " & intCycle & "", Me.Name, ""
                                'If g_rs_TSCHED.Fields(0).Value = "RSO" Or g_rs_TSCHED.Fields(0).Value = "PAU" Then
                                    dlulCount.Open "select * from t_download where seriesno='" & sMRU & "' and bill_month= " & Padl(str(intCycle), 2, "0") & "", cnDB, adOpenStatic, adLockReadOnly
                                    dlCount = dlulCount.RecordCount
                                    dlulCount.Close
                                    ' check total accounts loaded in t_upload
                                    dlulCount.Open "select * from t_upload where seriesno='" & sMRU & "' and bill_month= " & Padl(str(intCycle), 2, "0") & "", cnDB, adOpenStatic, adLockReadOnly
                                    ulCount = dlulCount.RecordCount
                                    dlulCount.Close
                                    If ulCount = dlCount Then
                                        UpdateBookStat sMRU, intCycle
                                        lsvBooks.ListItems.Clear
                                        Form_Load
                                    End If

                            Else
                                MsgBox "Upload for MRU " & sMRU & " Failed!", vbCritical, "Upload MRU to MWSI"
                                Exit Sub
                            End If
                        Next n
'                    Else
'                        MsgBox "Can only upload books with status 'RSO'!", vbCritical, "Upload MRU to MWSI"
'                    End If
                    MsgBox "Upload Successful!", vbInformation, "Upload MRU to MWSI"
                    Exit Sub
                End If
            Next
   
    cnDB.Close
    
    'Unload Me
End Sub

Sub UpdateBookStat(MRU As String, cycle As Integer)
    Dim l_str_Sql As String
    
    ' Update Uploaded to MWSI date
    l_str_Sql = "UPDATE T_BOOK SET UPLD_SW_DT = getdate() WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(cycle)
    If Not DBExecute(l_str_Sql, Me.Name, "MRU Management") Then Exit Sub
    
    ' Update Status in T_SCHED
    l_str_Sql = "UPDATE T_SCHED SET BKSTCODE = 'FWM' WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(cycle)
    If Not DBExecute(l_str_Sql, Me.Name, "MRU Management") Then Exit Sub

End Sub



Private Sub Form_Load()
    Dim l_obj_Item As Object
    Dim l_str_Stat As String
    Dim l_str_Sql As String
    Dim dlCount As Integer
    Dim ulCount As Integer
    Dim RSCount As ADODB.Recordset
    Dim sqlCount As String
    
    Set RSCount = New ADODB.Recordset

If WindowState = vbNormal Then
    Me.Move (1000)
    Me.Top = 500
End If

   
    Me.Caption = "Upload SERIES to SUBIC WATER"
    
    iCycle = CInt(Right(g_BillMonth, 2))
    
    l_str_Sql = "SELECT T_BOOK.BOOKNO, T_SCHED.BKSTCODE, T_SCHED.CYCLE FROM T_BOOK " & _
                "INNER JOIN T_SCHED ON (T_BOOK.CYCLE = T_SCHED.CYCLE) AND (T_BOOK.BOOKNO = T_SCHED.BOOKNO) WHERE T_SCHED.CYCLE=" & str(iCycle) & "" 'or T_SCHED.BKSTCODE<>'FWM' " removed 07-20-2011 Jums
'    l_str_sql = "SELECT T_BOOK.BOOKNO, T_SCHED.BKSTCODE FROM T_BOOK " & _
'                "INNER JOIN T_SCHED ON (T_BOOK.CYCLE = T_SCHED.CYCLE) AND (T_BOOK.BOOKNO = T_SCHED.BOOKNO)"
    
    If Not OpenRecordset(g_rs_TBOOK, l_str_Sql, Me.Name, "Form_Load") Then Exit Sub
    g_rs_TBOOK.MoveFirst
    While Not g_rs_TBOOK.EOF
        l_str_Stat = CheckNull(g_rs_TBOOK.Fields("BKSTCODE"))
        Set l_obj_Item = lsvBooks.ListItems.Add(, , CheckNull(g_rs_TBOOK.Fields("BOOKNO")))
            l_obj_Item.SubItems(1) = l_str_Stat
            l_obj_Item.SubItems(2) = g_rs_TBOOK.Fields("CYCLE")
            
            sqlCount = "SELECT COUNT (*) as 'DLCOUNT' FROM T_DOWNLOAD WHERE SERIESNO = '" & g_rs_TBOOK.Fields("BOOKNO") & "' AND BILL_MONTH = " & g_rs_TBOOK.Fields("CYCLE")
            If OpenRecordset(RSCount, sqlCount, "", "") Then
            dlCount = RSCount.Fields("DLCOUNT")
            End If
            sqlCount = "SELECT COUNT (*) as 'ULCOUNT' FROM T_UPLOAD WHERE SERIESNO = '" & g_rs_TBOOK.Fields("BOOKNO") & "' AND BILL_MONTH = " & g_rs_TBOOK.Fields("CYCLE")
            If OpenRecordset(RSCount, sqlCount, "", "") Then
            ulCount = RSCount.Fields("ULCOUNT")
            End If
            
            Select Case l_str_Stat
                Case "DLM"
                    l_obj_Item.ForeColor = &HC00000
                Case "SSO"
                    l_obj_Item.ForeColor = &HFF
                Case "RSO"
                    If ulCount = dlCount Then
                    l_obj_Item.ForeColor = &HC000&
                    End If
                    If ulCount <> dlCount Then
                    l_obj_Item.ForeColor = &H10000
                    End If
                Case "PAU"
                    If ulCount = dlCount Then
                    l_obj_Item.ForeColor = &HC000&
                    End If
                    If ulCount <> dlCount Then
                    l_obj_Item.ForeColor = &H10000
                    End If
                Case "FWM"
                    l_obj_Item.Bold = True
                Case Else
            End Select
        Set l_obj_Item = Nothing
        g_rs_TBOOK.MoveNext
    Wend
    
    If lsvBooks.ListItems.Count > 0 Then cmdOK.Enabled = True
    
End Sub

Private Sub lsvBooks_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ListviewSort lsvBooks, ColumnHeader.Index
End Sub

Private Function Generate_MRU_List() As Boolean
Dim iCount As Integer
'Dim iChecked As Integer
Dim itm
'Dim MRUArray(1000) As String
Dim n As Integer
Dim i As Integer

    ' Reset MRUList
    MRUList = ""
    iChecked = 0
    
    ' Count the number of Checked items in the ListView and build the MRUList from the text items
    iCount = lsvBooks.ListItems.Count
    
    ' If no MRUs were selected, exit function with True, you will be using an "Empty" MRUList!
    If iCount = 0 Then
        Generate_MRU_List = False
        Exit Function
    End If
    
    i = 0
    For Each itm In lsvBooks.ListItems
       With itm
       i = i + 1
         If .checked Then
            iChecked = iChecked + 1
            
            ' Add the MRU to the list; if first MRU, no comma in front
            If MRUList <> "" Then
                MRUList = MRUList & ",'" & .Text & "'"
            Else
                MRUList = MRUList & "'" & .Text & "'"
            End If
            MRUArray(iChecked) = itm
            CycleArray(iChecked) = lsvBooks.ListItems(i).SubItems(2)
         End If
       End With
    Next

    If iChecked = 0 Then
        MsgBox "No MRU selected for upload!", vbCritical, "Upload MRU to MWSI"
        Generate_MRU_List = False
        Exit Function
    End If
    
    n = 1
    For n = 1 To iChecked
        ' check if mru stat is RSO
        'If Not OpenRecordset(g_rs_TSCHED, "SELECT BOOKNO FROM T_SCHED WHERE BOOKNO = '" & MRUArray(n) & "' and BKSTCODE= 'RSO' and cycle=" & iCycle & "", Me.Name, "Generate_MRU_List") Then
'        If Not OpenRecordset(g_rs_TSCHED, "SELECT BOOKNO FROM T_SCHED WHERE BOOKNO = '" & MRUArray(n) & "' and BKSTCODE= 'RSO'", Me.Name, "Generate_MRU_List") Then
        Dim SQLstring As String
        SQLstring = "SELECT SERIESNO FROM T_UPLOAD WHERE SERIESNO = '" & MRUArray(n) & "' and BILL_MONTH= '" & Padl(CycleArray(n), 2, "0") & "'"
        If Not OpenRecordset(g_rs_TUPLOAD, SQLstring, Me.Name, "Generate_MRU_List") Then
'            MsgBox "Please uncheck MRU " & MRUArray(n) & ". Can only upload books with status 'RSO'!", vbCritical, "Upload MRU to MWSI"
            MsgBox "Please uncheck MRU " & MRUArray(n) & ". Can only upload books with partial or complete upload data!", vbCritical, "Upload MRU to MWSI"
            'g_rs_TSCHED.Close
            'Set g_rs_TSCHED = Nothing
            Generate_MRU_List = False
            Exit Function
        End If
    Next n

    ' If ALL MRUs were selected, then do NOT use the list
'    If iCount = iChecked Then
'       Generate_MRU_List = False
'    Else
        Generate_MRU_List = True
'    End If

End Function
