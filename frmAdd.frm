VERSION 5.00
Begin VB.Form frmAdd 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Add"
   ClientHeight    =   5985
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8430
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5985
   ScaleWidth      =   8430
   StartUpPosition =   1  'CenterOwner
   Begin VB.ComboBox cmbStat 
      Height          =   315
      Left            =   6480
      TabIndex        =   73
      Top             =   210
      Width           =   1815
   End
   Begin VB.ComboBox cmbReader 
      Height          =   315
      Left            =   3660
      TabIndex        =   72
      Top             =   210
      Width           =   1815
   End
   Begin VB.ComboBox cmbRoverNo 
      Height          =   315
      Left            =   930
      TabIndex        =   71
      Top             =   210
      Width           =   1815
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   390
      Left            =   4853
      TabIndex        =   70
      Top             =   5385
      Width           =   915
   End
   Begin VB.CommandButton cmdMulti 
      Caption         =   "&Multi"
      Height          =   390
      Left            =   3758
      TabIndex        =   69
      Top             =   5385
      Width           =   915
   End
   Begin VB.CommandButton cmdAccept 
      Caption         =   "&Accept"
      Height          =   390
      Left            =   2663
      TabIndex        =   68
      Top             =   5385
      Width           =   915
   End
   Begin VB.Frame Frame1 
      Height          =   4305
      Left            =   135
      TabIndex        =   3
      Top             =   810
      Width           =   7935
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   31
         Left            =   6480
         TabIndex        =   67
         Top             =   3765
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   30
         Left            =   6480
         TabIndex        =   66
         Top             =   3420
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   29
         Left            =   6480
         TabIndex        =   65
         Top             =   3080
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   28
         Left            =   6480
         TabIndex        =   64
         Top             =   2740
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   27
         Left            =   6480
         TabIndex        =   63
         Top             =   2400
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   26
         Left            =   6480
         TabIndex        =   62
         Top             =   2060
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   25
         Left            =   6480
         TabIndex        =   61
         Top             =   1720
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   24
         Left            =   6480
         TabIndex        =   60
         Top             =   1380
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   23
         Left            =   6480
         TabIndex        =   59
         Top             =   1040
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   22
         Left            =   6480
         TabIndex        =   58
         Top             =   700
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   21
         Left            =   6480
         TabIndex        =   57
         Top             =   360
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   20
         Left            =   3840
         TabIndex        =   56
         Top             =   3765
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   19
         Left            =   3840
         TabIndex        =   55
         Top             =   3420
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   18
         Left            =   3840
         TabIndex        =   54
         Top             =   3080
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   17
         Left            =   3840
         TabIndex        =   53
         Top             =   2740
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   16
         Left            =   3840
         TabIndex        =   52
         Top             =   2400
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   15
         Left            =   3840
         TabIndex        =   51
         Top             =   2060
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   14
         Left            =   3840
         TabIndex        =   50
         Top             =   1720
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   13
         Left            =   3840
         TabIndex        =   49
         Top             =   1380
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   12
         Left            =   3840
         TabIndex        =   48
         Top             =   1040
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   11
         Left            =   3840
         TabIndex        =   47
         Top             =   700
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   10
         Left            =   3840
         TabIndex        =   46
         Top             =   360
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   9
         Left            =   1200
         TabIndex        =   45
         Top             =   3765
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   8
         Left            =   1200
         TabIndex        =   44
         Top             =   3422
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   7
         Left            =   1200
         TabIndex        =   43
         Top             =   3081
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   6
         Left            =   1200
         TabIndex        =   42
         Top             =   2740
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   5
         Left            =   1200
         TabIndex        =   41
         Top             =   2399
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   4
         Left            =   1200
         TabIndex        =   40
         Top             =   2058
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   3
         Left            =   1200
         TabIndex        =   39
         Top             =   1717
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   2
         Left            =   1200
         TabIndex        =   38
         Top             =   1376
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   1
         Left            =   1200
         TabIndex        =   37
         Top             =   1035
         Width           =   1095
      End
      Begin VB.TextBox txtDayNo 
         Height          =   285
         Index           =   0
         Left            =   1200
         TabIndex        =   4
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 31"
         Height          =   255
         Index           =   31
         Left            =   5640
         TabIndex        =   36
         Top             =   3765
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 30"
         Height          =   255
         Index           =   30
         Left            =   5640
         TabIndex        =   35
         Top             =   3420
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 29"
         Height          =   255
         Index           =   29
         Left            =   5640
         TabIndex        =   34
         Top             =   3075
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 28"
         Height          =   255
         Index           =   28
         Left            =   5640
         TabIndex        =   33
         Top             =   2730
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 27"
         Height          =   255
         Index           =   27
         Left            =   5640
         TabIndex        =   32
         Top             =   2370
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 26"
         Height          =   255
         Index           =   26
         Left            =   5640
         TabIndex        =   31
         Top             =   2025
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 25"
         Height          =   255
         Index           =   25
         Left            =   5640
         TabIndex        =   30
         Top             =   1695
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 24"
         Height          =   255
         Index           =   24
         Left            =   5640
         TabIndex        =   29
         Top             =   1365
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 23"
         Height          =   255
         Index           =   23
         Left            =   5640
         TabIndex        =   28
         Top             =   1035
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 22"
         Height          =   255
         Index           =   22
         Left            =   5640
         TabIndex        =   27
         Top             =   690
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 21"
         Height          =   255
         Index           =   21
         Left            =   5640
         TabIndex        =   26
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 20"
         Height          =   255
         Index           =   20
         Left            =   3000
         TabIndex        =   25
         Top             =   3765
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 19"
         Height          =   255
         Index           =   19
         Left            =   3000
         TabIndex        =   24
         Top             =   3420
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 18"
         Height          =   255
         Index           =   18
         Left            =   3000
         TabIndex        =   23
         Top             =   3075
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 17"
         Height          =   255
         Index           =   17
         Left            =   3000
         TabIndex        =   22
         Top             =   2730
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 16"
         Height          =   255
         Index           =   16
         Left            =   3000
         TabIndex        =   21
         Top             =   2370
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 15"
         Height          =   255
         Index           =   15
         Left            =   3000
         TabIndex        =   20
         Top             =   2025
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 14"
         Height          =   255
         Index           =   14
         Left            =   3000
         TabIndex        =   19
         Top             =   1695
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 13"
         Height          =   255
         Index           =   13
         Left            =   3000
         TabIndex        =   18
         Top             =   1365
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 12"
         Height          =   255
         Index           =   12
         Left            =   3000
         TabIndex        =   17
         Top             =   1035
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 11"
         Height          =   255
         Index           =   11
         Left            =   3000
         TabIndex        =   16
         Top             =   690
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 10"
         Height          =   255
         Index           =   10
         Left            =   3000
         TabIndex        =   15
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 9"
         Height          =   255
         Index           =   9
         Left            =   360
         TabIndex        =   14
         Top             =   3765
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 8"
         Height          =   255
         Index           =   8
         Left            =   360
         TabIndex        =   13
         Top             =   3420
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 7"
         Height          =   255
         Index           =   7
         Left            =   360
         TabIndex        =   12
         Top             =   3075
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 6"
         Height          =   255
         Index           =   6
         Left            =   360
         TabIndex        =   11
         Top             =   2730
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 5"
         Height          =   255
         Index           =   5
         Left            =   360
         TabIndex        =   10
         Top             =   2370
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 4"
         Height          =   255
         Index           =   4
         Left            =   360
         TabIndex        =   9
         Top             =   2025
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 3"
         Height          =   255
         Index           =   3
         Left            =   360
         TabIndex        =   8
         Top             =   1695
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 2"
         Height          =   255
         Index           =   2
         Left            =   360
         TabIndex        =   7
         Top             =   1365
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day 1"
         Height          =   255
         Index           =   1
         Left            =   360
         TabIndex        =   6
         Top             =   1035
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Day No."
         Height          =   255
         Index           =   0
         Left            =   360
         TabIndex        =   5
         Top             =   360
         Width           =   615
      End
   End
   Begin VB.Label lblStatus 
      Caption         =   "Status"
      Height          =   255
      Left            =   5880
      TabIndex        =   2
      Top             =   240
      Width           =   615
   End
   Begin VB.Label lblReader 
      Caption         =   "Reader"
      Height          =   255
      Left            =   3000
      TabIndex        =   1
      Top             =   240
      Width           =   615
   End
   Begin VB.Label lblRoverNo 
      Caption         =   "Rover No."
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   240
      Width           =   975
   End
End
Attribute VB_Name = "frmAdd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdAccept_Click()
    Unload Me
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdMulti_Click()
    frmMultiBook.Show
End Sub

Private Sub Form_Load()
    cmbRoverNo.Text = "043426"
    cmbRoverNo.AddItem "045695"
    cmbRoverNo.AddItem "045697"
    
    cmbReader.Text = "Juan dela Cruz"
    cmbReader.AddItem "Pedro Santos"
    cmbReader.AddItem "Jose Rizal"
    
    cmbStat.Text = "D"
    cmbStat.AddItem "U"
End Sub
