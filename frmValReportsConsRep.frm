VERSION 5.00
Object = "{3C62B3DD-12BE-4941-A787-EA25415DCD27}#10.0#0"; "crviewer.dll"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmConsRep 
   Caption         =   "Consumption Report"
   ClientHeight    =   10545
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15240
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   10545
   ScaleWidth      =   15240
   Begin VB.Frame Frame1 
      Caption         =   "Report Parameters"
      Height          =   2295
      Left            =   120
      TabIndex        =   1
      Top             =   0
      Width           =   15015
      Begin VB.TextBox txtCondition5 
         Height          =   285
         Left            =   9600
         TabIndex        =   29
         Top             =   1800
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.TextBox txtCondition4 
         Height          =   285
         Left            =   9600
         TabIndex        =   28
         Top             =   1440
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.ComboBox cmbCondition5 
         Height          =   315
         ItemData        =   "frmValReportsConsRep.frx":0000
         Left            =   7560
         List            =   "frmValReportsConsRep.frx":0013
         Style           =   2  'Dropdown List
         TabIndex        =   27
         Top             =   1800
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.ComboBox cmbCondition4 
         Height          =   315
         ItemData        =   "frmValReportsConsRep.frx":0053
         Left            =   7560
         List            =   "frmValReportsConsRep.frx":0066
         Style           =   2  'Dropdown List
         TabIndex        =   26
         Top             =   1440
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.CommandButton cmdDeleteCondition 
         Caption         =   "Delete Condition"
         Enabled         =   0   'False
         Height          =   495
         Left            =   11520
         TabIndex        =   25
         Top             =   1320
         Width           =   1455
      End
      Begin VB.CommandButton cmdAddCondition 
         Caption         =   "Add Condition"
         Height          =   495
         Left            =   11520
         TabIndex        =   24
         Top             =   480
         Width           =   1455
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Cancel"
         Height          =   495
         Left            =   13200
         TabIndex        =   23
         Top             =   1320
         Width           =   1455
      End
      Begin VB.TextBox txtCondition3 
         Height          =   315
         Left            =   9600
         TabIndex        =   22
         Top             =   1080
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.TextBox txtCondition2 
         Height          =   315
         Left            =   9600
         TabIndex        =   21
         Top             =   720
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.ComboBox cmbCondition3 
         Height          =   315
         ItemData        =   "frmValReportsConsRep.frx":00A6
         Left            =   7560
         List            =   "frmValReportsConsRep.frx":00B9
         Style           =   2  'Dropdown List
         TabIndex        =   20
         Top             =   1080
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.ComboBox cmbCondition2 
         Height          =   315
         ItemData        =   "frmValReportsConsRep.frx":00F9
         Left            =   7560
         List            =   "frmValReportsConsRep.frx":010C
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   720
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.TextBox txtCondition1 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0.00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   315
         Left            =   9600
         TabIndex        =   16
         Top             =   360
         Width           =   1575
      End
      Begin VB.ComboBox cmbCondition1 
         Height          =   315
         ItemData        =   "frmValReportsConsRep.frx":014C
         Left            =   7560
         List            =   "frmValReportsConsRep.frx":015F
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   360
         Width           =   1815
      End
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   315
         Left            =   3960
         TabIndex        =   13
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   16449537
         CurrentDate     =   40120
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   315
         Left            =   3960
         TabIndex        =   12
         Top             =   720
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   16449537
         CurrentDate     =   40120
      End
      Begin VB.ComboBox cmbMRU 
         Height          =   315
         Left            =   720
         TabIndex        =   9
         Text            =   "ALL"
         Top             =   1080
         Width           =   1575
      End
      Begin VB.CommandButton cmdGenerate 
         Caption         =   "&Generate"
         Height          =   495
         Left            =   13200
         TabIndex        =   4
         Top             =   480
         Width           =   1455
      End
      Begin VB.ComboBox cmbBillMonth 
         Height          =   315
         ItemData        =   "frmValReportsConsRep.frx":019F
         Left            =   720
         List            =   "frmValReportsConsRep.frx":01C7
         TabIndex        =   3
         Top             =   360
         Width           =   855
      End
      Begin VB.ComboBox cmbBusCen 
         Height          =   315
         Left            =   720
         TabIndex        =   2
         Text            =   "ALL"
         Top             =   720
         Width           =   2175
      End
      Begin VB.Label lblCondition5 
         Caption         =   "Condition 5: Consumption"
         Height          =   255
         Left            =   5520
         TabIndex        =   31
         Top             =   1800
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.Label lblCondition4 
         Caption         =   "Condition 4: Consumption"
         Height          =   255
         Left            =   5520
         TabIndex        =   30
         Top             =   1440
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.Label lblCondition3 
         Caption         =   "Condition 3: Consumption"
         Height          =   255
         Left            =   5520
         TabIndex        =   18
         Top             =   1080
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.Label lblCondition2 
         Caption         =   "Condition 2: Consumption"
         Height          =   255
         Left            =   5520
         TabIndex        =   17
         Top             =   720
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.Label lblCondition1 
         Caption         =   "Condition 1: Consumption"
         Height          =   255
         Left            =   5520
         TabIndex        =   14
         Top             =   360
         Width           =   1935
      End
      Begin VB.Label lblmru 
         Caption         =   "MRU"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   1080
         Width           =   615
      End
      Begin VB.Label Label5 
         Caption         =   "Date To"
         Height          =   255
         Left            =   3120
         TabIndex        =   8
         Top             =   720
         Width           =   975
      End
      Begin VB.Label Label3 
         Caption         =   "Date From"
         Height          =   255
         Left            =   3120
         TabIndex        =   7
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Cycle"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   360
         Width           =   495
      End
      Begin VB.Label Label4 
         Caption         =   "BC"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   720
         Width           =   615
      End
   End
   Begin VB.PictureBox CRViewer 
      Height          =   8055
      Left            =   120
      ScaleHeight     =   7995
      ScaleWidth      =   14955
      TabIndex        =   0
      Top             =   2400
      Width           =   15015
      Begin CrystalActiveXReportViewerLib10Ctl.CrystalActiveXReportViewer CRViewerConsRep 
         Height          =   8055
         Left            =   0
         TabIndex        =   11
         Top             =   0
         Width           =   15015
         lastProp        =   600
         _cx             =   26485
         _cy             =   14208
         DisplayGroupTree=   -1  'True
         DisplayToolbar  =   -1  'True
         EnableGroupTree =   -1  'True
         EnableNavigationControls=   -1  'True
         EnableStopButton=   -1  'True
         EnablePrintButton=   -1  'True
         EnableZoomControl=   -1  'True
         EnableCloseButton=   -1  'True
         EnableProgressControl=   -1  'True
         EnableSearchControl=   -1  'True
         EnableRefreshButton=   -1  'True
         EnableDrillDown =   -1  'True
         EnableAnimationControl=   -1  'True
         EnableSelectExpertButton=   0   'False
         EnableToolbar   =   -1  'True
         DisplayBorder   =   -1  'True
         DisplayTabs     =   -1  'True
         DisplayBackgroundEdge=   -1  'True
         SelectionFormula=   ""
         EnablePopupMenu =   -1  'True
         EnableExportButton=   0   'False
         EnableSearchExpertButton=   0   'False
         EnableHelpButton=   0   'False
         LaunchHTTPHyperlinksInNewBrowser=   -1  'True
         EnableLogonPrompts=   -1  'True
      End
   End
End
Attribute VB_Name = "frmConsRep"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ReportType As String
Dim BC_Code As String
Dim cycle As Integer
Dim date_to As Date
Dim date_from As Date
Dim Condition1 As String
Dim Condition2 As String
Dim Condition3 As String
Dim Condition4 As String
Dim Condition5 As String
Dim MRU As String

Private Sub cmbBillMonth_Click()
Dim lastday As Integer
Dim curdate As String
Dim curdate1 As String
DTPicker1.Refresh
DTPicker2.Refresh
curdate = cmbBillMonth.Text & "/1" & "/" & Year(Now)

'DTPicker1.Value = CDate(curdate)
DTPicker2.Value = CDate(curdate)


'DTPicker2.Value = DateAdd("d", -(Day(dtpicker1.value) - 1), DTPicker1.Value)
curdate1 = cmbBillMonth & "/" & getNumberOfDays(CDate(DTPicker2.Value)) & "/" & CStr(Year(Now))

DTPicker1.Value = CDate(curdate1)

load_MRU cmbBillMonth, Left$(cmbBusCen.Text, 4)

End Sub

'Get the number of days each month is having
Public Function getNumberOfDays(date1 As Date) As String
    Select Case DateTime.Month(date1)
        Case 1, 3, 5, 7, 8, 10, 12
            getNumberOfDays = 31
        Case 4, 6, 9, 11
            getNumberOfDays = 30
        Case 2
            'logic for checking leap years
            If (Year(date1) Mod 4) = 0 Then
                If (Year(date1) Mod 100) = 0 Then
                    If (Year(date1) Mod 400) = 0 Then
                        getNumberOfDays = 29
                    Else
                        getNumberOfDays = 28
                    End If
                Else
                    getNumberOfDays = 29
                End If
            Else
                getNumberOfDays = 28
            End If
    End Select
End Function

Private Sub cmbBusCen_Click()
If cmbBusCen.Text <> "ALL" Then
BC_Code = Mid(cmbBusCen.Text, 1, 4)
Else
BC_Code = "ALL"
End If
load_MRU cmbBillMonth, Left$(cmbBusCen.Text, 4)
End Sub

Private Sub cmdAddCondition_Click()

If Trim(cmbCondition1.Text) = "" Or Trim(txtCondition1.Text) = "" Then
    MsgBox "Please input values for condition 1 before adding another condition!", vbOKOnly, "Add Condition"
'Else
    ElseIf Me.lblCondition2.Visible = False Then
        Me.lblCondition2.Visible = True
        Me.cmbCondition2.Visible = True
        Me.txtCondition2.Visible = True
        Me.cmdDeleteCondition.Enabled = True
        cmbCondition1.Enabled = False
        txtCondition1.Enabled = False
'    ElseIf Me.lblCondition2.Visible = True And Trim(cmbCondition2.Text) = "" Or Trim(txtCondition2.Text) = "" Then
'        MsgBox "Please input values for condition 2 before adding another condition!"
    ElseIf Me.lblCondition3.Visible = False Then
        Me.lblCondition3.Visible = True
        Me.cmbCondition3.Visible = True
        Me.txtCondition3.Visible = True
        Me.cmdDeleteCondition.Enabled = True
    ElseIf Me.lblCondition4.Visible = False Then
        Me.lblCondition4.Visible = True
        Me.cmbCondition4.Visible = True
        Me.txtCondition4.Visible = True
        Me.cmdDeleteCondition.Enabled = True
    Else
        Me.lblCondition5.Visible = True
        Me.cmbCondition5.Visible = True
        Me.txtCondition5.Visible = True
        Me.cmdDeleteCondition.Enabled = True
'    End If
End If

'If Me.lblCondition3.Visible = True Then
'    Me.cmdAddCondition.Enabled = False
'End If
If Me.lblCondition5.Visible = True Then
    Me.cmdAddCondition.Enabled = False
End If

End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdDeleteCondition_Click()

If lblCondition5.Visible = True Then
    Me.lblCondition5.Visible = False
    Me.cmbCondition5.Visible = False
    Me.txtCondition5.Visible = False
    'Me.cmbCondition3.Text = ""
    cmbCondition5.ListIndex = 0
    Me.txtCondition5.Text = ""
    Me.cmdAddCondition.Enabled = True
Else
    If Me.lblCondition4.Visible = True Then
        Me.lblCondition4.Visible = False
        Me.cmbCondition4.Visible = False
        Me.txtCondition4.Visible = False
        'Me.cmbCondition4.Text = ""
        cmbCondition4.ListIndex = 0
        Me.txtCondition4.Text = ""
    ElseIf Me.lblCondition3.Visible = True Then
        Me.lblCondition3.Visible = False
        Me.cmbCondition3.Visible = False
        Me.txtCondition3.Visible = False
        'Me.cmbCondition3.Text = ""
        cmbCondition3.ListIndex = 0
        Me.txtCondition3.Text = ""
    ElseIf Me.lblCondition2.Visible = True Then
        Me.lblCondition2.Visible = False
        Me.cmbCondition2.Visible = False
        Me.txtCondition2.Visible = False
        'Me.cmbCondition2.Text = ""
        cmbCondition2.ListIndex = 0
        Me.txtCondition2.Text = ""
        cmbCondition1.Enabled = True
        txtCondition1.Enabled = True
    Else 'Me.lblCondition2.Visible = False
        'MsgBox "At least one condition should be displayed."
        Me.cmdDeleteCondition.Enabled = False
    End If
End If

'If lblCondition3.Visible = True Then
'    Me.lblCondition3.Visible = False
'    Me.cmbCondition3.Visible = False
'    Me.txtCondition3.Visible = False
'    'Me.cmbCondition3.Text = ""
'    cmbCondition3.ListIndex = 0
'    Me.txtCondition3.Text = ""
'    Me.cmdAddCondition.Enabled = True
'Else
'    If Me.lblCondition2.Visible = True Then
'        Me.lblCondition2.Visible = False
'        Me.cmbCondition2.Visible = False
'        Me.txtCondition2.Visible = False
'        'Me.cmbCondition2.Text = ""
'        cmbCondition2.ListIndex = 0
'        Me.txtCondition2.Text = ""
'    Else 'Me.lblCondition2.Visible = False
'        'MsgBox "At least one condition should be displayed."
'        Me.cmdDeleteCondition.Enabled = False
'    End If
'End If

If Me.lblCondition2.Visible = False Then
    Me.cmdDeleteCondition.Enabled = False
End If

End Sub

Private Sub cmdGenerate_Click()
    Dim i As Integer
        
    g_str_rep = "CONSUMPTION REPORT.rpt"
        
    If cmbBusCen.Text <> "ALL" Then
    BC_Code = Mid(cmbBusCen.Text, 1, 4)
    Else
    BC_Code = "ALL"
    End If
    
    cycle = CInt(cmbBillMonth.Text)
    date_to = DTPicker1.Value
    date_from = DTPicker2.Value
    
    If Me.cmbCondition1.Text <> "" And IsNumeric(txtCondition1.Text) = True Then
        Condition1 = Left(cmbCondition1.Text, 2) & Trim(txtCondition1.Text)
    Else
        Condition1 = "ALL"
    End If
    If Me.cmbCondition2.Text <> "" And IsNumeric(txtCondition2.Text) = True Then
        Condition2 = Left(cmbCondition2.Text, 2) & Trim(txtCondition2.Text)
    Else
        Condition2 = "ALL"
    End If
    If Me.cmbCondition3.Text <> "" And IsNumeric(txtCondition3.Text) = True Then
        Condition3 = Left(cmbCondition3.Text, 2) & Trim(txtCondition3.Text)
    Else
        Condition3 = "ALL"
    End If
    If Me.cmbCondition4.Text <> "" And IsNumeric(txtCondition4.Text) = True Then
        Condition4 = Left(cmbCondition4.Text, 2) & Trim(txtCondition4.Text)
    Else
        Condition4 = "ALL"
    End If
    If Me.cmbCondition5.Text <> "" And IsNumeric(txtCondition5.Text) = True Then
        Condition5 = Left(cmbCondition5.Text, 2) & Trim(txtCondition5.Text)
    Else
        Condition5 = "ALL"
    End If
    MRU = cmbMRU.Text

    Set Appn = CreateObject("CrystalRunTime.Application")
    
    Set cReport = Appn.OpenReport(App.Path & "\crystal reports\" & g_str_rep)
    
    cReport.Database.LogOnServer "p2ssql.dll", "SZ-MWSI-SUBIC", "MCFSDB", "sa", "sqladmin"
    For i = 1 To cReport.Database.Tables.Count
        cReport.Database.Tables(i).ConnectionProperties.Item("Password") = "sqladmin"
    Next i
    
    cReport.DiscardSavedData
    ' Parameters
    cReport.ParameterFields(1).AddCurrentValue BC_Code
    cReport.ParameterFields(2).AddCurrentValue cycle
    cReport.ParameterFields(3).AddCurrentValue date_from
    cReport.ParameterFields(4).AddCurrentValue date_to
    cReport.ParameterFields(5).AddCurrentValue MRU
    cReport.ParameterFields(6).AddCurrentValue Condition1
    cReport.ParameterFields(7).AddCurrentValue Condition2
    cReport.ParameterFields(8).AddCurrentValue Condition3
    cReport.ParameterFields(9).AddCurrentValue Condition4
    cReport.ParameterFields(10).AddCurrentValue Condition5
    CRViewerConsRep.ReportSource = cReport
    CRViewerConsRep.EnableExportButton = True
    CRViewerConsRep.ViewReport
    
End Sub

Private Sub Form_Activate()
If WindowState = vbNormal Then
    Me.Move (0)
    Me.Top = 0
    Me.Height = 11055
    Me.Width = 15435
End If

cmbBillMonth.Text = Month(Now)
load_BC
load_MRU cmbBillMonth, Left$(cmbBusCen.Text, 4)
End Sub

Public Sub load_MRU(intCycle As Integer, Optional ByVal BusCtr$ = "ALL")
Dim sql As String

' Default based on current cycle or on selected one from list
On Error GoTo errhandler
    With cmbMRU
        .Clear
        .AddItem "ALL"
        sql = "select bookno from T_BOOK where CYCLE = " & intCycle

        If BusCtr$ <> "ALL" Then
           sql = sql & " and BC_CODE = '" & BusCtr$ & "'"
        End If
        'Build sql statement
        sql = sql & " order by bookno"
        If OpenRecordset(g_rs_TBOOK, sql, "frmValReportsRdg", "") Then
            While Not g_rs_TBOOK.EOF
                .AddItem g_rs_TBOOK.Fields(0)
                g_rs_TBOOK.MoveNext
            Wend
'        Else
'            .ListIndex = 0
        End If
        .ListIndex = 0
    End With
    
errhandler:
If Err.Number <> 0 Then
    If Err.Number = 380 Then
    Else
        MsgBox Err.Description, , "System Message"
    End If
End If
End Sub

Public Sub load_BC()
    
On Error GoTo errhandler
    With cmbBusCen
        .Clear
        .AddItem "ALL"
        If OpenRecordset(g_rs_RBUSCTR, "select * from R_BUSCTR order by BC_CODE", "", "") Then
            While Not g_rs_RBUSCTR.EOF
                .AddItem g_rs_RBUSCTR.Fields(0) & " - " & g_rs_RBUSCTR.Fields(1)
                g_rs_RBUSCTR.MoveNext
            Wend
        Else
            .ListIndex = 0
        End If
    End With
cmbBusCen.Text = "ALL"
errhandler:
If Err.Number <> 0 Then
    If Err.Number = 380 Then
    Else
        MsgBox Err.Description, , "System Message"
    End If
End If
End Sub

