VERSION 5.00
Begin VB.Form frmMain 
   Caption         =   "MRMS Data Station"
   ClientHeight    =   5655
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   7935
   LinkTopic       =   "Form1"
   ScaleHeight     =   5655
   ScaleWidth      =   7935
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Menu mnuHost 
      Caption         =   "&Host"
      Begin VB.Menu mnuDnldBatch 
         Caption         =   "&Download Batch from Host"
      End
      Begin VB.Menu mnuCreate 
         Caption         =   "&Create Download Files from Batch"
      End
      Begin VB.Menu sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDnldTables 
         Caption         =   "Download &Tables"
      End
      Begin VB.Menu sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuUpload 
         Caption         =   "&Upload Books to Host"
      End
      Begin VB.Menu sep3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDnldList 
         Caption         =   "Download Table &List"
      End
      Begin VB.Menu mnuDnldUpldOpt 
         Caption         =   "Download/Upload &Option"
      End
   End
   Begin VB.Menu mnuRvrMgmt 
      Caption         =   "&Rover Management"
      Begin VB.Menu mnuScheduler 
         Caption         =   "&Scheduler"
      End
      Begin VB.Menu mnuDataTrans 
         Caption         =   "&Data Transfer"
      End
      Begin VB.Menu mnuInitRover 
         Caption         =   "&Initialize Rover"
      End
      Begin VB.Menu sep4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuOptions 
         Caption         =   "&Options"
      End
      Begin VB.Menu sep5 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSetuoComm 
         Caption         =   "Setup &Communications"
      End
   End
   Begin VB.Menu mnuReports 
      Caption         =   "Re&ports"
      Begin VB.Menu mnuSummMRRep 
         Caption         =   "&Summary Meter Reading"
      End
      Begin VB.Menu mnuFieldFindingRep 
         Caption         =   "&Field Finding"
         Begin VB.Menu mnuInactiveAcctsRep 
            Caption         =   "&Inactive Accounts"
         End
         Begin VB.Menu mnuOutofRangeRep 
            Caption         =   "&Out of Range"
         End
         Begin VB.Menu mnuMisscodeRep 
            Caption         =   "&Misscode"
         End
         Begin VB.Menu mnuFoundConnRep 
            Caption         =   "Found &Connected"
         End
         Begin VB.Menu mnuFieldFindingsRep 
            Caption         =   "&Field Findings"
         End
         Begin VB.Menu sep6 
            Caption         =   "-"
         End
         Begin VB.Menu mnuAllRep 
            Caption         =   "&All FF Reports"
         End
      End
      Begin VB.Menu mnuTimeReadRep 
         Caption         =   "&Time Read"
      End
      Begin VB.Menu mnuDetMRRep 
         Caption         =   "Detailed &Meter Reading"
      End
      Begin VB.Menu mnuDetDnldListRep 
         Caption         =   "&Detailed Download List"
      End
      Begin VB.Menu mnuDetUpldListRep 
         Caption         =   "Detailed &Upload List"
      End
      Begin VB.Menu mnuCustInfoSheetRep 
         Caption         =   "&Customer Information Sheet"
      End
      Begin VB.Menu mnuScheduleRep 
         Caption         =   "Schedu&le"
      End
      Begin VB.Menu sep7 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPrinterSetup 
         Caption         =   "&Printer Setup"
      End
   End
   Begin VB.Menu mnuFileMaintenance 
      Caption         =   "&File Maintenance"
      Begin VB.Menu mnuBookStat 
         Caption         =   "&Book Status"
      End
      Begin VB.Menu sep8 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditBooks 
         Caption         =   "&Edit Books"
         Begin VB.Menu mnuDnldBooks 
            Caption         =   "&Download Books"
         End
         Begin VB.Menu mnuUpldBooks 
            Caption         =   "&Upload Books"
         End
      End
      Begin VB.Menu mnuDeleteBooks 
         Caption         =   "&Delete Books"
      End
      Begin VB.Menu sep9 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFFCodes 
         Caption         =   "&Field Finding Codes"
      End
      Begin VB.Menu mnuMeterReader 
         Caption         =   "&Meter Reader"
      End
      Begin VB.Menu mnuRoverFile 
         Caption         =   "&Rover File"
      End
      Begin VB.Menu mnuRatesTable 
         Caption         =   "Ra&tes Table"
      End
      Begin VB.Menu sep10 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFoundConn 
         Caption         =   "F&ound Connected"
      End
      Begin VB.Menu mnuRepMeter 
         Caption         =   "Re&placed Meter"
      End
   End
   Begin VB.Menu mnuUtilities 
      Caption         =   "&Utilities"
      Begin VB.Menu mnuInitFiles 
         Caption         =   "&Initialize Files"
      End
      Begin VB.Menu mnuReindexDB 
         Caption         =   "&Reindex Databases"
      End
      Begin VB.Menu sep11 
         Caption         =   "-"
      End
      Begin VB.Menu mnuConfigFile 
         Caption         =   "&Configuration File"
      End
      Begin VB.Menu sep12 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDateCovered 
         Caption         =   "&Date Covered"
      End
      Begin VB.Menu sep13 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPWMgmt 
         Caption         =   "&Password Management"
      End
      Begin VB.Menu sep14 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDirectories 
         Caption         =   "&Directories"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
   Begin VB.Menu mnuExit 
      Caption         =   "&Exit"
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub mnuExit_Click()
    Dim lstrAns As String
    
    lstrAns = MsgBox("Do you want to exit MRMS?", vbOKCancel, "Exit")
    If lstrAns = vbYes Then
        Unload Me
        End
    End If
End Sub

Private Sub mnuScheduler_Click()
    frmScheduler.Show
End Sub
