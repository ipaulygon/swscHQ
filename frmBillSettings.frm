VERSION 5.00
Begin VB.Form frmBillSettings 
   Caption         =   "Sourcefile Settings"
   ClientHeight    =   3690
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4785
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   3690
   ScaleWidth      =   4785
   Begin VB.Frame Frame2 
      Caption         =   "Billed Error Directory"
      Height          =   975
      Left            =   120
      TabIndex        =   7
      Top             =   2040
      Width           =   4575
      Begin VB.CommandButton Command1 
         Caption         =   "..."
         Height          =   285
         Left            =   4080
         TabIndex        =   10
         Top             =   480
         Width           =   385
      End
      Begin VB.TextBox txtbilledError 
         Height          =   285
         Left            =   240
         TabIndex        =   9
         Top             =   480
         Width           =   3735
      End
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save"
      Height          =   495
      Left            =   1680
      TabIndex        =   6
      Top             =   3120
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Daily Billed File Directory"
      Height          =   1815
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4575
      Begin VB.TextBox txtUDest 
         Height          =   285
         Left            =   240
         TabIndex        =   8
         Top             =   1320
         Width           =   3735
      End
      Begin VB.CommandButton cmdUDest 
         Caption         =   "..."
         Height          =   285
         Left            =   4080
         TabIndex        =   3
         Top             =   1320
         Width           =   385
      End
      Begin VB.TextBox txtDSource 
         Height          =   285
         Left            =   240
         TabIndex        =   2
         Top             =   600
         Width           =   3735
      End
      Begin VB.CommandButton cmdDSource 
         Caption         =   "..."
         Height          =   285
         Left            =   4080
         TabIndex        =   1
         Top             =   585
         Width           =   385
      End
      Begin VB.Label Label4 
         Caption         =   "Destination : "
         Height          =   285
         Left            =   120
         TabIndex        =   5
         Top             =   1080
         Width           =   2865
      End
      Begin VB.Label Label5 
         Caption         =   "Download File Source : "
         Height          =   285
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   2475
      End
   End
End
Attribute VB_Name = "frmBillSettings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdDSource_Click()
frmFileSourcebilled.Show
frmFileSourcebilled.txtsource.Text = "1"
End Sub

Private Sub cmdSave_Click()
Dim SQLInsert As String

SQLInsert = "INSERT INTO dbo.DailyBilledPath(DbilledUploadPath,DbilledDestPath,DbilledErrorPath) VALUES ('" & txtDSource.Text & "','" & txtUDest.Text & "','" & txtbilledError.Text & "')"
g_Conn.Execute ("DELETE FROM dbo.DailyBilledPath")
g_Conn.Execute (SQLInsert)
MsgBox "Settings Saved!"
Unload Me
End Sub

Private Sub cmdUDest_Click()
frmFileSourcebilled.Show
frmFileSourcebilled.txtsource.Text = "2"
End Sub

Private Sub Command1_Click()
frmFileSourcebilled.Show
frmFileSourcebilled.txtsource.Text = "3"
End Sub

Private Sub Form_Activate()
If WindowState = vbNormal Then
    Me.Width = 4905
    Me.Height = 4200
End If
End Sub

Private Sub Form_Load()
Dim RSPath As ADODB.Recordset
Dim SQLstring As String
Dim DLPath As String
Dim TarPath As String
Dim ErrPath As String

SQLstring = "SELECT * FROM dbo.DailyBilledPath"

If OpenRecordset(RSPath, SQLstring, "", "") Then
    While Not RSPath.EOF
        DLPath = RSPath.Fields(0)
        TarPath = RSPath.Fields(1)
        ErrPath = RSPath.Fields(2)
        RSPath.MoveNext
    Wend
End If

txtDSource.Text = DLPath
txtUDest.Text = TarPath
txtbilledError.Text = ErrPath

End Sub

