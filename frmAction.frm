VERSION 5.00
Begin VB.Form frmAction 
   Caption         =   "Add Action"
   ClientHeight    =   2220
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   3375
   LinkTopic       =   "Form1"
   ScaleHeight     =   2220
   ScaleWidth      =   3375
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOK 
      Caption         =   "&Ok"
      Height          =   495
      Left            =   1080
      TabIndex        =   1
      Top             =   1680
      Width           =   1215
   End
   Begin VB.TextBox txtAction 
      Height          =   1455
      Left            =   120
      MaxLength       =   50
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   120
      Width           =   3135
   End
End
Attribute VB_Name = "frmAction"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdOk_Click()
Dim MRU As String
Dim ACCTNUM As String
Dim X As Integer
Dim ActionString As String
Dim SQLstring As String

ActionString = txtAction.Text
For X = 1 To frmValAnalysis.lsvMaster.ListItems.Count
    If frmValAnalysis.lsvMaster.ListItems(X).checked Then
      MRU = frmValAnalysis.lsvMaster.ListItems.Item(X).ListSubItems(1)
      ACCTNUM = frmValAnalysis.lsvMaster.ListItems.Item(X).ListSubItems(2)
    SQLstring = "UPDATE dbo.T_VER_EXTRACT SET ACTIONTAKEN = '" & ActionString & "' WHERE BOOKNO = '" & MRU & "' AND ACCTNUM = '" & ACCTNUM & "'"
    DBExecute SQLstring, "", ""
    End If
Next X

frmValAnalysis.genMasterList

Unload Me

End Sub

Private Sub Form_Load()
txtAction.Text = g_ActionItem

End Sub
