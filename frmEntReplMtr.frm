VERSION 5.00
Begin VB.Form frmEntReplMtr 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Enter Replace Meter"
   ClientHeight    =   5625
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3735
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5625
   ScaleWidth      =   3735
   Begin VB.TextBox txtAcctNo 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   2060
      Locked          =   -1  'True
      MaxLength       =   12
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   460
      Width           =   1400
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   390
      Left            =   1935
      TabIndex        =   12
      Top             =   5070
      Width           =   915
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   390
      Left            =   900
      TabIndex        =   11
      Top             =   5070
      Width           =   915
   End
   Begin VB.Frame fraNewMtr 
      Caption         =   " New Meter "
      Height          =   1410
      Left            =   60
      TabIndex        =   19
      Top             =   3500
      Width           =   3600
      Begin VB.TextBox txtInitRdng 
         Height          =   285
         Left            =   2000
         MaxLength       =   9
         TabIndex        =   10
         Top             =   920
         Width           =   1400
      End
      Begin VB.TextBox txtMult 
         Height          =   285
         Left            =   2000
         MaxLength       =   7
         TabIndex        =   9
         Top             =   580
         Width           =   1400
      End
      Begin VB.TextBox txtNewMtrNo 
         Height          =   285
         Left            =   2000
         MaxLength       =   10
         TabIndex        =   8
         Top             =   240
         Width           =   1400
      End
      Begin VB.Label lblInitRdng 
         Alignment       =   1  'Right Justify
         Caption         =   "Initial Reading : "
         Height          =   225
         Left            =   120
         TabIndex        =   22
         Top             =   930
         Width           =   1800
      End
      Begin VB.Label lblMult 
         Alignment       =   1  'Right Justify
         Caption         =   "Meter Multiplier : "
         Height          =   225
         Left            =   120
         TabIndex        =   21
         Top             =   590
         Width           =   1800
      End
      Begin VB.Label lblNewMtrNo 
         Alignment       =   1  'Right Justify
         Caption         =   "Meter No. : "
         Height          =   225
         Left            =   120
         TabIndex        =   20
         Top             =   250
         Width           =   1800
      End
   End
   Begin VB.Frame fraOldMtrInfo 
      Caption         =   " Old Meter Information "
      Height          =   2460
      Left            =   60
      TabIndex        =   14
      Top             =   900
      Width           =   3600
      Begin VB.TextBox txtDateRepl 
         Height          =   285
         Left            =   2000
         MaxLength       =   10
         TabIndex        =   7
         Top             =   1960
         Width           =   1400
      End
      Begin VB.TextBox txtMtrMult 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   2000
         MaxLength       =   7
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   580
         Width           =   1400
      End
      Begin VB.TextBox txtDisconRdng 
         Height          =   285
         Left            =   2000
         MaxLength       =   9
         TabIndex        =   6
         Top             =   1620
         Width           =   1400
      End
      Begin VB.TextBox txtPrevRdng 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   2000
         MaxLength       =   9
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   1260
         Width           =   1400
      End
      Begin VB.TextBox txtPrevRdngDate 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   2000
         MaxLength       =   10
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   920
         Width           =   1400
      End
      Begin VB.TextBox txtMtrNo 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   2000
         MaxLength       =   10
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   240
         Width           =   1400
      End
      Begin VB.Label lblDateRepl 
         Alignment       =   1  'Right Justify
         Caption         =   "Date Replaced : "
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   1970
         Width           =   1800
      End
      Begin VB.Label lblMtrMult 
         Alignment       =   1  'Right Justify
         Caption         =   "Meter Multiplier : "
         Height          =   195
         Left            =   120
         TabIndex        =   24
         Top             =   590
         Width           =   1800
      End
      Begin VB.Label lblDisconRdng 
         Alignment       =   1  'Right Justify
         Caption         =   "Disconnected Reading : "
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   1630
         Width           =   1800
      End
      Begin VB.Label lblPrevRdng 
         Alignment       =   1  'Right Justify
         Caption         =   "Previous Reading : "
         Height          =   195
         Left            =   120
         TabIndex        =   17
         Top             =   1270
         Width           =   1800
      End
      Begin VB.Label lblPrevRdngDate 
         Alignment       =   1  'Right Justify
         Caption         =   "Previous Reading Date : "
         Height          =   195
         Left            =   120
         TabIndex        =   16
         Top             =   930
         Width           =   1800
      End
      Begin VB.Label lblMtrNo 
         Alignment       =   1  'Right Justify
         Caption         =   "Meter No. : "
         Height          =   195
         Left            =   120
         TabIndex        =   15
         Top             =   250
         Width           =   1800
      End
   End
   Begin VB.TextBox txtBookNo 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   2060
      Locked          =   -1  'True
      MaxLength       =   5
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   120
      Width           =   1400
   End
   Begin VB.Label lblAcctNo 
      Alignment       =   1  'Right Justify
      Caption         =   "Account No. : "
      Height          =   255
      Left            =   180
      TabIndex        =   23
      Top             =   470
      Width           =   1800
   End
   Begin VB.Label lblBookNo 
      Alignment       =   1  'Right Justify
      Caption         =   "Book No. : "
      Height          =   255
      Left            =   180
      TabIndex        =   13
      Top             =   130
      Width           =   1800
   End
End
Attribute VB_Name = "frmEntReplMtr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdSave_Click()
    Dim l_int_Index As Integer

    If Not ValidEntries Then Exit Sub
    If DuplicateRecords Then Exit Sub
    If Not ReplaceMeter Then Exit Sub

    l_int_Index = frmEditBooks.lsvFileMain.SelectedItem.Index
    With frmEditBooks.lsvFileMain.ListItems(l_int_Index)
        .SubItems(5) = Trim(txtNewMtrNo)
        .SubItems(7) = CheckNull(txtMult, True)
        .SubItems(9) = CheckNull(txtInitRdng, True)
    End With

    frmEditBooks.lsvFileMain.Refresh
    Unload Me

End Sub

Function ValidEntries() As Boolean
    ValidEntries = False
    If Not NumericEntry(txtDisconRdng, lblDisconRdng) Then Exit Function
    
    If Len(Trim(txtDateRepl)) > 0 Then
        If Not IsDate(Trim(txtDateRepl)) Then
            MsgBox "Date Replaced entry is not a valid date.  Please re-enter a valid date.", vbExclamation, "Invalid Entry"
            txtDateRepl.SetFocus
            Exit Function
        End If
    End If
    
    If Not NumericEntry(txtMult, lblMult) Then Exit Function
    If Not NumericEntry(txtInitRdng, lblInitRdng) Then Exit Function
    ValidEntries = True

End Function

Function DuplicateRecords() As Boolean
    Dim l_str_Sql As String

    DuplicateRecords = True

    l_str_Sql = "SELECT BOOKNO FROM T_REPMTR WHERE ACCTNO = '" & txtAcctNo & _
        "' AND MTRNO = '" & Trim(txtNewMtrNo) & "'"

    If OpenRecordset(g_rs_TREPMTR, l_str_Sql, Me.Name, "DuplicateRecords") Then
        MsgBox "Record already exists!", vbExclamation, "Duplicate Record"
        g_rs_TREPMTR.Close
        Set g_rs_TREPMTR = Nothing
        txtNewMtrNo.SetFocus
        Exit Function
    End If

    l_str_Sql = "SELECT ACCTNO FROM T_METER WHERE ACCTNO = '" & txtAcctNo & _
        "' AND MTRNO = '" & Trim(txtNewMtrNo) & "'"

    If OpenRecordset(g_rs_TMETER, l_str_Sql, Me.Name, "DuplicateRecords") Then
        MsgBox "Record already exists!", vbExclamation, "Duplicate Record"
        g_rs_TMETER.Close
        Set g_rs_TMETER = Nothing
        txtNewMtrNo.SetFocus
        Exit Function
    End If
    
    DuplicateRecords = False

End Function

Function ReplaceMeter() As Boolean
    Dim l_str_Sql As String

    ReplaceMeter = False
    
    l_str_Sql = "INSERT INTO T_REPMTR(BOOKNO, ACCTNO, MTRNO, REPLDATE, MTRMULT, PRVRDGDT, PRVRDG, READING) VALUES('" & _
        txtBookNo & "', '" & txtAcctNo & "', '" & txtMtrNo & "', '" & Format(Trim(txtDateRepl), "mm/dd/yyyy") & "', " & _
        txtMtrMult & ", '" & txtPrevRdngDate & "', " & txtPrevRdng & ", " & CheckNull(txtDisconRdng, True) & ")"
    If Not DBExecute(l_str_Sql, Me.Name, "ReplaceMeter") Then Exit Function
    
    l_str_Sql = "UPDATE T_METER SET MTRNO = '" & Left(Trim(txtNewMtrNo), 10) & _
        "', MTRMULT = " & CheckNull(txtMult, True) & " PREVNO = '" & txtMtrNo & _
        "' WHERE ACCTNO = '" & txtAcctNo & "' AND MTRNO = '" & txtMtrNo & "'"
    If Not DBExecute(l_str_Sql, Me.Name, "SaveFConn") Then Exit Function

    l_str_Sql = "UPDATE T_READING SET MTRNO = '" & Left(Trim(txtNewMtrNo), 10) & _
        "', READING = " & CheckNull(txtInitRdng, True) & _
        " WHERE ACCTNO = '" & txtAcctNo & "' AND MTRNO = '" & txtMtrNo & "' AND RDGDATE = '" & txtPrevRdngDate & "'"
    ReplaceMeter = DBExecute(l_str_Sql, Me.Name, "SaveFConn")
    
End Function

Private Sub Form_Load()
    With frmEditBooks.lsvFileMain.SelectedItem
        txtBookNo = .Text
        txtAcctNo = .SubItems(2)
        txtMtrNo = .SubItems(5)
        txtMtrMult = .SubItems(7)
        txtPrevRdngDate = .SubItems(8)
        txtPrevRdng = .SubItems(9)
    End With
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmEditBooks.Enabled = True
End Sub

Private Sub TextChange()
    If Len(Trim(txtDateRepl)) > 0 And Len(Trim(txtDisconRdng)) > 0 And Len(Trim(txtNewMtrNo)) > 0 Then
        cmdSave.Enabled = True
    Else
        cmdSave.Enabled = False
    End If
End Sub

Private Sub txtDateRepl_Change()
    TextChange
End Sub

Private Sub txtDateRepl_GotFocus()
    TextHighlight txtDateRepl
End Sub

Private Sub txtDisconRdng_Change()
    TextChange
End Sub

Private Sub txtDisconRdng_GotFocus()
    TextHighlight txtDisconRdng
End Sub

Private Sub txtInitRdng_GotFocus()
    TextHighlight txtInitRdng
End Sub

Private Sub txtMult_GotFocus()
    TextHighlight txtMult
End Sub

Private Sub txtNewMtrNo_Change()
    TextChange
End Sub

Private Sub txtNewMtrNo_GotFocus()
    TextHighlight txtNewMtrNo
End Sub
