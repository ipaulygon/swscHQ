VERSION 5.00
Begin VB.Form frmLogin 
   Caption         =   "Login"
   ClientHeight    =   3540
   ClientLeft      =   5325
   ClientTop       =   3990
   ClientWidth     =   4800
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3540
   ScaleWidth      =   4800
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   390
      Left            =   2490
      TabIndex        =   3
      Top             =   2610
      Width           =   915
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&OK"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   390
      Left            =   1350
      TabIndex        =   2
      Top             =   2610
      Width           =   915
   End
   Begin VB.TextBox txtPassword 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1680
      MaxLength       =   8
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   2040
      Width           =   2265
   End
   Begin VB.TextBox txtUserId 
      BeginProperty DataFormat 
         Type            =   0
         Format          =   "AAAAAA"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1033
         SubFormatType   =   0
      EndProperty
      Height          =   285
      Left            =   1680
      MaxLength       =   8
      TabIndex        =   0
      Top             =   1710
      Width           =   2265
   End
   Begin VB.Label B 
      Alignment       =   2  'Center
      Caption         =   "<company name>"
      BeginProperty Font 
         Name            =   "Bookman Old Style"
         Size            =   9.75
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   563
      TabIndex        =   7
      Top             =   360
      Width           =   3525
   End
   Begin VB.Label lblMRMS 
      Alignment       =   2  'Center
      Caption         =   "MWSI MRU Management System"
      BeginProperty Font 
         Name            =   "Bookman Old Style"
         Size            =   9
         Charset         =   0
         Weight          =   300
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   1283
      TabIndex        =   6
      Top             =   960
      Width           =   1965
   End
   Begin VB.Label lblPassword 
      Alignment       =   1  'Right Justify
      Caption         =   "Password : "
      Height          =   285
      Left            =   690
      TabIndex        =   5
      Top             =   2040
      Width           =   855
   End
   Begin VB.Label lblUserId 
      Alignment       =   1  'Right Justify
      Caption         =   "User ID : "
      Height          =   285
      Left            =   690
      TabIndex        =   4
      Top             =   1710
      Width           =   855
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'NOTE: new form from cfshq 07-17-2013 jums


Private Sub cmdCancel_Click()
    Set g_rs_SUSER = Nothing
    g_Conn.Close
    Set g_Conn = Nothing
    End
    
End Sub

Private Sub cmdOk_Click()
Dim l_str_Sql As String

Dim sPassword As String
Dim dbPassword As String
    
g_rs_headername = txtUserID.Text
    
    l_str_Sql = "SELECT USERID,[LEVEL],[PASSWORD] FROM S_USER WHERE USERID = '" & UCase(g_rs_headername) & "'"
    If Not OpenRecordset(g_rs_SUSER, l_str_Sql, Me.Name, "cmdOk_Click") Then
            MsgBox "Invalid User Id!", vbExclamation, "Login"
            txtUserID.SetFocus
            Exit Sub
    Else
            dbPassword = CheckNull(g_rs_SUSER.Fields("PASSWORD"))
            sPassword = PWDEncrypt(dbPassword, 1)
            ' Check password
            'If sPassword <> "3A7PH" Then
            If sPassword <> UCase(txtPassword.Text) Then
                g_rs_SUSER.Close
                Set g_rs_SUSER = Nothing
                MsgBox "Invalid Password!", vbExclamation, "Login"
                txtPassword.SetFocus
                Exit Sub
            End If
    
    If Not IsNull(CheckNull(g_rs_SUSER.Fields("LEVEL"), True)) Then
    g_rs_userlevel = g_rs_SUSER.Fields("LEVEL")
        If g_rs_userlevel > 1 Then
            If g_rs_userlevel = 3 Then
                SetMenu2 True
            Else
                SetMenu True
            End If
        Else
            SetMenu False
        End If
    Else
        SetMenu False
    End If

    
    g_rs_SUSER.Close
    Set g_rs_SUSER = Nothing
    
LoginOk:
    g_User = txtUserID.Text
    MDIMain.Show
    Unload Me
End If

End Sub

Private Sub SetMenu(bolEnable As Boolean)
    MDIMain.mnuHost.Enabled = bolEnable
    MDIMain.mnuReports.Enabled = bolEnable
    MDIMain.mnuFileMaint.Enabled = bolEnable
    MDIMain.mnuUtilities.Enabled = bolEnable
    MDIMain.mnuScheduler.Enabled = bolEnable
    MDIMain.mnuScheduler.Enabled = bolEnable
    
    'new code added 07-17-2013 jums for special menu tariif and global
    MDIMain.mnuParameter.Enabled = False
    MDIMain.mnuTariff.Enabled = False
    MDIMain.mnuGlobalCharges.Enabled = False
'    MDIMain.mnuSPBillAccounts.Enabled = False
    MDIMain.mnuMRUMgmnt.Enabled = bolEnable
    
    
End Sub
Private Sub SetMenu2(bolEnable As Boolean)
'new code added 07-17-2013 jums for special menu tariif and global
    MDIMain.mnuHost.Enabled = bolEnable
    MDIMain.mnuReports.Enabled = bolEnable
    MDIMain.mnuFileMaint.Enabled = bolEnable
    MDIMain.mnuUtilities.Enabled = bolEnable
    MDIMain.mnuScheduler.Enabled = bolEnable
    MDIMain.mnuParameter.Enabled = bolEnable
    MDIMain.mnuTariff.Enabled = bolEnable
    MDIMain.mnuGlobalCharges.Enabled = bolEnable
'    MDIMain.mnuSPBillAccounts.Enabled = bolEnable
    MDIMain.mnuMRUMgmnt.Enabled = bolEnable
    
End Sub

Private Sub Form_Load()
    B.Caption = g_Company
End Sub

Private Sub txtPassword_GotFocus()
    TextHighlight txtPassword
End Sub

Private Sub txtUserID_Change()
    If Len(Trim(txtUserID)) > 0 Then
        cmdOK.Enabled = True
    Else
        cmdOK.Enabled = False
    End If
        
End Sub

Private Sub txtUserID_GotFocus()
    TextHighlight txtUserID
End Sub
