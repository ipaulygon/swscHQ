VERSION 5.00
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Begin VB.Form F_Main 
   Caption         =   "MWSI HHT LOADER"
   ClientHeight    =   3330
   ClientLeft      =   60
   ClientTop       =   315
   ClientWidth     =   3975
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   3330
   ScaleWidth      =   3975
   StartUpPosition =   2  'CenterScreen
   Begin MSCommLib.MSComm MSComm1 
      Left            =   330
      Top             =   390
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DTREnable       =   -1  'True
   End
   Begin VB.TextBox ENT_Rec 
      Alignment       =   2  'Center
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FF00&
      Height          =   420
      Left            =   180
      Locked          =   -1  'True
      TabIndex        =   2
      TabStop         =   0   'False
      Text            =   "Initialize..."
      Top             =   2790
      Width           =   2385
   End
   Begin VB.CommandButton btn_stop 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Enabled         =   0   'False
      Height          =   405
      Left            =   2670
      TabIndex        =   1
      Top             =   2760
      Width           =   1125
   End
   Begin VB.ListBox List1 
      Height          =   2010
      Left            =   120
      TabIndex        =   0
      Top             =   90
      Width           =   3735
   End
   Begin VB.Shape Shape2 
      BorderStyle     =   0  'Transparent
      FillColor       =   &H000040C0&
      FillStyle       =   0  'Solid
      Height          =   105
      Left            =   780
      Top             =   2550
      Width           =   2295
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Inactive"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   345
      Left            =   870
      TabIndex        =   3
      Top             =   2310
      Width           =   2205
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00808080&
      BackStyle       =   1  'Opaque
      Height          =   435
      Left            =   750
      Shape           =   4  'Rounded Rectangle
      Top             =   2250
      Width           =   2445
   End
End
Attribute VB_Name = "F_Main"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const gc_red = vbRed '&HFF&
Const gc_green = &HFF00&
Const gc_gray = &H808080
Const gc_yellow = &HFFFF&
Const gc_dyellow = 32896
Const gc_cyan = 16776960
Const gc_dcyan = 8421376
Const gc_violet = 16711935
Const gc_dviolet = 8388736

Const DOWNLOADED = "0"
Const MODIFIED = "1"
Const ALL_READ = "2"
Const UPLOADED = "3"

Const MODE_DOWNLOAD = 0
Const MODE_UPLOAD = 1

Dim mv_Fast%, mv_Filename$, mv_BookNo$
Dim mv_FCode1$, mv_FCode2$, mv_FCode3$
Dim mv_UCnt%, mv_RVRno$, mv_RVRid$, mv_RVRa$
Dim mv_Lines%
Dim mv_BkPath$, mv_LoadStatus$, mv_RvrID_MRMS$
Dim mv_HHTbkno$

Dim pMode As Integer
Dim HHTStat As String

Sub Read_Paths(stat As String)
Dim inpLine$

    ' path file contains one line
    ' rrrrrrbbbb<filepath>
    '   rrrrrr - rover id
    '   bbbb - book number
    '  <filepath> - path on where the file to process is located
    Open App.Path & "\rvrpath.dat" For Input As #1
    While Not EOF(1)
       Line Input #1, inpLine$
    Wend
    Close #1
    
    mv_RvrID_MRMS$ = Left(inpLine$, 6)
    mv_HHTbkno$ = Trim(Mid(inpLine$, 7, 8))
    mv_BkPath$ = Trim(Mid(inpLine$, 15))

    mv_BookNo$ = mv_HHTbkno$ 'Mid$(gv_Command$, 3, 4)
    mv_RVRno$ = Right(mv_HHTbkno$, 2) 'Mid$(gv_Command$, 5, 2)
    'mv_RVRno$ = Mid$(mv_BookNo$, 6, 2)
    mv_RVRa$ = "@"   'Mid$(gv_Command$ & " ", 7, 1)

    Select Case (stat) '(Left$(gv_Command$, 2))
    Case "/c", "/C": Proc_Assign
    Case "/p", "/P": Proc_Prog
    Case "/d", "/D": Proc_Dnld
    Case "/u", "/U": Proc_Upld
    Case Else:
        MsgBox "Unknown Command", vbCritical, "Invalid Command"
        Exit Sub
    End Select

End Sub

Private Sub RStatFile()
    Open App.Path & "\loadstat.dat" For Output As #1
    Print #1, mv_RVRid$ & mv_LoadStatus$
    Close #1
End Sub

Private Sub Form_Activate()

'    mv_BookNo$ = Mid$(gv_Command$, 3, 4)
'    mv_RVRno$ = Mid$(gv_Command$, 5, 2)
'    'mv_RVRno$ = Mid$(mv_BookNo$, 6, 2)
'    mv_RVRa$ = Mid$(gv_Command$ & " ", 7, 1)
    
'    Select Case (Left$(gv_Command$, 2))
'    Case "/c", "/C": Proc_Assign
'    Case "/p", "/P": Proc_Prog
'    Case "/d", "/D": Proc_Dnld
'    Case "/u", "/U": Proc_Upld
'    Case Else:
'        MsgBox "Unknown Command", vbCritical, "Invalid Command"
'        End
'    End Select
End Sub

Private Sub Form_Load()
    'Set rc = MSComm1
    'Port% = 1
    'Comm_Init
       
    Shape2.Visible = False
End Sub

Sub RVR_Insert()
    Flg_Cancel% = 0
    btn_stop.Enabled = True
    Shape2.Visible = False
    'Shape2.Width = 0
    Label1 = "Plug Device"

    a% = 1
    Do
        'DoEvents
        If a% = 0 Then
            Shape1.BackColor = &H808080
            a% = 1
        Else
            Shape1.BackColor = &HFF00&
            a% = 0
        End If
        TX (",")
        If Comm_Err% <> 0 Then comm_reinit_IR
    Loop Until (Comm_Err% = 0 Or Flg_Cancel% > 0)
    
    Label1 = "- - -"
    Shape1.BackColor = &H808080
    
    If Flg_Cancel% > 0 Then
        List1.AddItem "-Cancelled!"
        List1.ListIndex = List1.ListCount - 1
    End If
End Sub


Sub RVR_Remove()
    Flg_Cancel% = 0
    btn_stop.Enabled = True
    Shape2.Visible = False
    'Shape2.Width = 0
    Label1 = "Unplug Device"
    DoEvents
    a% = 0
    q = 0 'Now
    j = 20
    Do
        If q < Timer Then        'If q <> Now Then
            'DoEvents
            If a% = 0 Then
                Shape1.BackColor = &H1C0FF
                a% = 1
            Else
                Shape1.BackColor = &H808080
                a% = 0
            End If
            
            If j > 0 Then
                j = j - 1
                TX (",")
                q = Timer 'q = Now
            Else
                TX (".")
                q = Timer + 1 'q = Now
            End If
        End If
    Loop Until (Comm_Err% > 0 Or Flg_Cancel% > 0)
    Shape1.BackColor = &H808080
    Label1 = "- - -"
End Sub

Private Sub Insert_End()

    On Error GoTo err1
    If mv_RVRa$ = " " Then
        id$ = ""
        Open App.Path & "\rvrtable.dat" For Input As #2
        While Not EOF(2)
            Line Input #2, a$
            If Left$(a$, 2) = mv_RVRno$ Then id$ = Trim(Mid$(a$, 4))
        Wend
        Close #2
        
        If id$ = "" Then
            List1.AddItem " -File Error: HHT" & mv_RVRno$ & " not found in RVRTABLE."
            Error_End "File Error: HHT" & mv_RVRno$ & " not found in RVRTABLE.", "File Error"
        End If
    Else
        id$ = "ANY"
    End If
    
    'comm_reinit_IR

ret10:
    ENT_Rec = mv_RVRno$ & " - " & id$
    RVR_Insert
    If Flg_Cancel% > 0 Then GoTo ext1
    
    send3 ("?")
    If Comm_Err% > 0 Then
        List1.AddItem " -Error: Failed to send HHT ID request."
        Error_End "Failed to send HHT ID request", "Comm Error"
    End If
    
    recv3
    If Comm_Err% > 0 Then
        List1.AddItem " -Error: Failed to get HHT ID."
        Error_End "Failed to get HHT ID.", "Comm Error"
    End If
    
    mv_RVRid$ = Trim(Mid$(RX_Msg$, 3, 6))
    HHTStat = Mid$(RX_Msg$, 2, 1)
    
    List1.AddItem "HHT ID: " & mv_RVRid$
    List1.AddItem "BOOK NO: " & mv_BookNo$
    
'    If (mv_RVRa$ = " ") And (UCase$(id$) <> UCase$(mv_RVRid$)) Then
'        List1.AddItem " -Error: Wrong HHT inserted."
'        If MsgBox("Wrong HHT inserted!", vbRetryCancel, "Wrong HHT") = vbCancel Then GoTo ext1
'        RVR_Remove
'        GoTo ret10
'    End If
    
    ' Next 2 code blocks added by Jasper 20070123
    ' Check if files in HHT have not yet been uploaded before download
    If (pMode = MODE_DOWNLOAD) Then
        If ((HHTStat <> DOWNLOADED) And (HHTStat <> UPLOADED)) Then
           List1.AddItem " -Error: Book pending for upload."
           ans = MsgBox("Another book assigned to the same HHT is not yet uploaded." & vbCrLf & _
                "Continuing will overwrite the currently un-uploaded book in the HHT." & vbCrLf & _
                "Do you want to continue downloading new data to the HHT?", vbYesNo, "Download Book")
            If ans = vbNo Then GoTo ext1
        End If
    End If
    
    ' Check if uploaded file has not been all read!
    If (pMode = MODE_UPLOAD) Then
        If (UCase$(mv_RvrID_MRMS$) <> UCase$(mv_RVRid$)) Then
            List1.AddItem " -Error: Wrong HHT inserted."
            If MsgBox("Wrong HHT inserted!", vbRetryCancel, "Wrong HHT") = vbCancel Then GoTo ext1
            RVR_Remove
            GoTo ret10
        End If
        If (HHTStat = UPLOADED) Then
            List1.AddItem " -Error: File is already Uploaded."
            Error_End "File is already Uploaded!", "HHT Error"
        End If
        If (HHTStat <> ALL_READ) And (HHTStat <> UPLOADED) Then
            List1.AddItem " -Error: Unread Meters in Upload File."
            Error_End "Cannot upload when there are still unread meters in HHT!", "HHT Error"
        End If
    End If
    
    Exit Sub
    
ext1:
    'List1.AddItem " -Cancelled."
    'Log_Append
    mv_LoadStatus$ = "CANCELLED"
    RStatFile
    Exit Sub
'    End

err1:
    Close #2
    List1.AddItem " -File Error in RVRTABLE.DAT"
    Error_End "File Error in RVRTABLE.DAT" & vbCrLf & Err.Description, "File Error"
End Sub

Private Sub Remove_End()
    Log_Append
    btn_stop.Caption = "Close"
    RVR_Remove
    mv_LoadStatus$ = "SUCCESS"
    RStatFile
    'End
End Sub

Private Sub Error_End(msg$, title$)
    Log_Append
    MsgBox msg$, vbCritical, title$
    mv_LoadStatus$ = "ERROR"
    RStatFile
    Exit Sub
    'End
End Sub


Private Sub dtable()
    On Error GoTo errf
    Flg_Cancel% = 0
    
 '  Open App.Path & "\" & mv_Filename$ For Input As #1
    If (mv_FCode1$ = "B") Or (mv_FCode1$ = "D") Then
        Open mv_BkPath$ & "\" & mv_Filename$ For Input As #1
    Else
        Open App.Path & "\" & mv_Filename$ For Input As #1
    End If
    
    f1& = LOF(1) ' / Shape1.Width
    f2& = 0
    Shape2.Width = 0
    
    'RVR_Insert
    'If Flg_Cancel% Then
    '    Close #1
    '    Exit Sub
    'End If
    
    Label1 = "Downloading..."
    Shape1.BackColor = gc_yellow
    Shape2.Visible = True
    Shape2.Width = 0

    DoEvents

    send3 ("#6File: " & mv_Filename$)
    If Comm_Err% Then Exit Sub
    send3 ("x") 'beep
    
    'HEADER
    ENT_Rec.Text = "Table"
    send3 (mv_FCode1$ & mv_Filename$)
    'If comm_err% Then GoTo dpr80

    'RECORDS
    i% = 0
    While (Not EOF(1)) And (Flg_Cancel% = 0) And (Comm_Err% = 0)
        Line Input #1, a$
        f2& = f2& + Len(a$)
        Shape2.Width = f2& / f1& * Shape1.Width
        
        i% = i% + 1
        a1$ = Format(i%, "000#") 'Right$(Str$(i% + 10000), 4)
        ENT_Rec.Text = "Line: " & a1$
        
        If mv_Fast% = 0 Then
            send3 (mv_FCode2$ & a1$ & Chr$(4) & a$)
        Else
            send3 ("v" & a1$ & a$)
        End If
    Wend
    
    Close #1
    mv_UCnt% = i%
    List1_AddLines
    
    If Comm_Err% > 0 Then
        Label1 = "Comm Error! " & CStr(Comm_Err%)
        Shape1.BackColor = gc_red
        ENT_Rec = "Comm Error!"
        Exit Sub
    End If
    
    If Flg_Cancel% = 1 Then
        Label1 = "Cancelled"
        Shape1.BackColor = gc_violet
        ENT_Rec = "Cancelled"
        
        send3 ("X" & a1$ & Chr$(4))
        send3 ("#7 -Cancelled")
        send3 ("x") 'beep
        send3 ("x") 'beep
        send3 ("x") 'beep
        List1.AddItem " -Cancelled" '& CStr(Comm_Err%)
        Log_Append
        'End
        Exit Sub
    End If
    
    Label1 = "Download -OK!"
    Shape1.BackColor = gc_cyan
    Shape2.Visible = False
    ENT_Rec = "Download -OK!"
    
    send3 (mv_FCode3$ & a1$ & Chr$(4))
    send3 ("x") 'beep
    send3 ("x") 'beep
        
    Exit Sub
    
errf:
    'book_fe
    Close #1
    List1.AddItem " -File Error: " & Err.Description
    Label1 = "File Error!"
    Shape1.BackColor = vbRed
    ENT_Rec = "File Error!"
    Error_End "File Error in " & mv_Filename$ & vbCrLf & Err.Description, "File Error"
End Sub

Private Sub btn_stop_Click()
    Flg_Cancel% = 1
End Sub
Private Sub Proc_Prog()
End Sub

Private Sub Proc_Assign()
    
    On Error Resume Next
    FileCopy App.Path & "\rvrtable.dat", App.Path & "\rvrtable.bak"
    
    On Error GoTo errf
    s% = 0
    Open App.Path & "\rvrtable.dat" For Output As #2
    Open App.Path & "\rvrtable.bak" For Input As #1
    
    While Not EOF(1)
        Line Input #1, a$
        If Trim(a$) <> "" Then
            If Left$(a$, 2) <> mv_RVRno$ Then
                Print #2, a$           'Print #2, mv_RVRno$ & " " & Trim(Mid$(gv_Command$, 6))
            Else
                Print #2, mv_RVRno$ & " " & Trim(Mid$(gv_Command$, 6))
                s% = 1
            End If
        End If
    Wend
    Close #1
errf:
    If s% = 0 Then Print #2, mv_RVRno$ & " " & Trim(Mid$(gv_Command$, 6))
    Close #2
    
'    End
    Exit Sub
    
'errf:
'    MsgBox Err.Description
'    End
End Sub
    
Private Sub Proc_Dnld()

    List1.AddItem "HHT:" & mv_RVRno$ & mv_RVRa$ & " Download: " & Format(Now, "yyyy/MM/dd hh:mm")
    
    ' Set Program Mode to check for completed reading file
    pMode = MODE_DOWNLOAD
    
    mv_LoadStatus$ = ""
    
    Insert_End
    
    If mv_LoadStatus$ = "CANCELLED" Or mv_LoadStatus$ = "ERROR" Then
        Unload Me
        Exit Sub
    End If
    
    'Initialize HHT
    List1.AddItem "Initializing HHT... "
    ENT_Rec = "Initializing..."
    
    send3 ("A")
    If Comm_Err% > 0 Then GoTo errc
    List1.List(List1.ListCount - 1) = "Initializing HHT...  -OK"
    'List1.ListIndex = List1.ListCount - 1
    
    send3 ("X" & mv_RvrID_MRMS$)
    If Comm_Err% > 0 Then GoTo errc

    send3 ("@DOWNLOADING...")
    
    'Download Field Finding
    mv_Filename$ = "FFCODE.TXT"
    List1.AddItem "Download: FFCODE.TXT"
    mv_FCode1$ = "F"
    mv_FCode2$ = "V"
    mv_FCode3$ = "W"
    dtable
    If Comm_Err% > 0 Then GoTo errc
    
    'Download Rate
'    mv_Filename$ = "RATE.TXT"
'    List1.AddItem "Download: RATE.TXT"
'    mv_FCode1$ = "T"
'    mv_FCode2$ = "V"
'    mv_FCode3$ = "W"
'    dtable
'    If Comm_Err% > 0 Then GoTo errc

    'Download Range Code
    mv_Filename$ = "RANGECDE.TXT"
    List1.AddItem "Download: RANGECDE.TXT"
    mv_FCode1$ = "G"
    mv_FCode2$ = "V"
    mv_FCode3$ = "W"
    dtable
    If Comm_Err% > 0 Then GoTo errc
    
    'Download book info
    mv_Filename$ = "BNFO" & Mid(mv_BookNo$, 5, 4) & ".TXT"
    List1.AddItem "Download: " & mv_Filename$
    mv_FCode1$ = "B"
    mv_FCode2$ = "V"
    mv_FCode3$ = "W"
    dtable
    If Comm_Err% > 0 Then GoTo errc
    
    'Download book detail
    mv_Filename$ = "BK" & Mid(mv_BookNo$, 5, 4) & "D.TXT"
    List1.AddItem "Download: " & mv_Filename$
    mv_FCode1$ = "D"
    mv_FCode2$ = "V"
    mv_FCode3$ = "W"
    dtable
    If Comm_Err% > 0 Then GoTo errc
    
    List1.AddItem "=Download complete." '& Format(Now, " yyyy/MM/dd hh:mm")
    ENT_Rec = "DOWNLOADED"
    send3 ("@ * DOWNLOADED *")
    'Unplug device and exit
    Remove_End
    Unload Me
    Exit Sub

errc:
    List1.AddItem " -Comm Error" '& CStr(Comm_Err%)
    Error_End "Device Comm. Error", "Comm Error"
    Exit Sub
End Sub
    
Private Sub Proc_Upld()

    'List1.AddItem "Upload: " & Format(Now, "yyyy/MM/dd hh:mm")
    List1.AddItem "HHT:" & mv_RVRno$ & mv_RVRa$ & " Upload:" & Format(Now, "yyyy/MM/dd hh:mm")
    
    ' Set Program Mode to check for completed reading file
    pMode = MODE_UPLOAD
    
    mv_LoadStatus$ = ""
    
    Insert_End

    If mv_LoadStatus$ = "CANCELLED" Or mv_LoadStatus$ = "ERROR" Then
        Unload Me
        Exit Sub
    End If

    send3 ("@UPLOADING...")
    If Comm_Err% > 0 Then
        List1.AddItem " -Comm Error" '& CStr(Comm_Err%)
        Error_End "Device Communication Error (Init)", "Device Error"
    End If
    
    Label1 = "Uploading"
    Shape1.BackColor = gc_cyan
    Shape2.Visible = False
    
    send3 ("R")
    mv_UCnt% = 0
    
    While True
        recv3
        If Comm_Err% > 0 Then
            Close #10
            List1_AddLines
            List1.AddItem " -Comm Error" '& CStr(Comm_Err%)
            Error_End "Device Communication Error (Data)", "Device Error"
        End If
        If Flg_Cancel% > 0 Then
            Close #10
            send3 ("#7 -Cancelled")
            List1_AddLines
            List1.AddItem " -Cancelled."
            Log_Append
            Exit Sub
            'End
        End If
        If Left$(RX_Msg$, 1) = "U" Then
            Close #10
            List1.AddItem "=Upload complete." '& Format(Now, " yyyy/MM/dd hh:mm")
            ENT_Rec = "UPLOADED"
            send3 ("@ -- UPLOADED --")
            Remove_End
            Unload Me
            Exit Sub
        End If
        RX_Proc
    Wend

End Sub

Private Sub RX_Proc()

    On Error GoTo err1
    
    Select Case Left$(RX_Msg$, 1)
    Case "R":
        i% = InStr(RX_Msg$, "\")
        fl$ = Mid$(RX_Msg$, i% + 1)
        'fl$ = Replace(fl$, "xx", mv_RVRno$)
        fl$ = Replace(fl$, "xxxx", Mid(mv_BookNo$, 5, 4))
        'pth$ = App.Path & "\" ' & mid$(rx_msg$,2,i%-1)
        'Open pth$ & fl$ For Output As #10
        Open mv_BkPath$ & "\" & fl$ For Output As #10
        List1.AddItem "Upload: " & fl$
        mv_UCnt% = 0
        ENT_Rec = "Uploading..."
        
    Case "S":
        Print #10, Mid$(RX_Msg$, 2)
        mv_UCnt% = mv_UCnt% + 1
        ENT_Rec = "Line: " & CStr(mv_UCnt%)
        
    Case "T":
        Close #10
        List1_AddLines
        ENT_Rec = "Uploaded."
    
    Case "$":
        List1.AddItem RX_Msg$
        
    Case Default:
        List1.AddItem "?: " & RX_Msg$
        
    End Select
    'List1.ListIndex = List1.ListCount - 1
    Exit Sub
    
'Error Handler
err1:
    List1.AddItem " -Error: " & Err.Description
    'List1.ListIndex = List1.ListCount - 1
End Sub

Private Sub List1_AddLines()
    List1.List(List1.ListCount - 1) = List1.List(List1.ListCount - 1) & "  Lines: " & CStr(mv_UCnt%)
End Sub

Private Sub Log_Append()
    On Error GoTo err1
    Open App.Path & "\rl.log" For Append As #3
    Print #3, ""
    For i% = 0 To List1.ListCount - 1
        Print #3, List1.List(i%)
    Next i%
err1:
    Close #3
End Sub

