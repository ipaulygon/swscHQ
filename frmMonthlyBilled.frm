VERSION 5.00
Begin VB.Form frmMonthlyBilled 
   Caption         =   "Monthly Billed Accounts"
   ClientHeight    =   4800
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5280
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   4800
   ScaleWidth      =   5280
   Begin VB.Frame Frame1 
      Caption         =   "DBF FILE SOURCE"
      Height          =   4575
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5055
      Begin VB.CommandButton cmdDownload 
         Caption         =   "&Download"
         Height          =   375
         Left            =   240
         TabIndex        =   5
         Top             =   3000
         Width           =   4575
      End
      Begin VB.TextBox txtdbfsource 
         Height          =   735
         Left            =   240
         TabIndex        =   4
         Top             =   3720
         Width           =   4575
      End
      Begin VB.DirListBox dirSource 
         Height          =   2340
         Left            =   240
         TabIndex        =   3
         Top             =   600
         Width           =   1875
      End
      Begin VB.DriveListBox drvSource 
         Height          =   315
         Left            =   240
         TabIndex        =   2
         Top             =   240
         Width           =   1875
      End
      Begin VB.FileListBox File1 
         Height          =   2625
         Left            =   2160
         Pattern         =   "*.dbf"
         TabIndex        =   1
         Top             =   240
         Width           =   2655
      End
   End
End
Attribute VB_Name = "frmMonthlyBilled"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdDownload_Click()
Dim MyConn As ADODB.Connection
Dim dbfRS As ADODB.Recordset
Dim checkRS As ADODB.Recordset
Dim strSQL As String
Dim Ba As String
Dim ConAcct As String
Dim Inst_nb As String
Dim Blk_util As String
Dim dbfFileName As String
Dim sqlCheck As String
Dim sqlInst As String
Dim ConString As String



dbfFileName = File1.List(0)

ConString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & dirSource.Path & ";Extended Properties=DBASE IV;"
'cn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=CMRMS;Extended Properties='FoxPro 2.6'"

Set MyConn = New ADODB.Connection
MyConn.ConnectionString = ConString
'MyConn.Properties("Jet OLEDB:Max Buffer Size") = 256
MyConn.Open
MyConn.CursorLocation = adUseClient

Set dbfRS = New ADODB.Recordset
strSQL = "SELECT * FROM " & Mid(dbfFileName, 1, Len(dbfFileName) - 4)

dbfRS.Open strSQL, MyConn, adOpenDynamic, adLockPessimistic

Do While Not dbfRS.EOF
    Ba = dbfRS.Fields(0)
    ConAcct = dbfRS.Fields(1)
    Inst_nb = dbfRS.Fields(2)
    If dbfRS.Fields(3) <> "" Then
        Blk_util = dbfRS.Fields(3)
    Else
        Blk_util = ""
    End If
    
sqlCheck = "SELECT * FROM dbo.MCF_VALID WHERE CONACCT = '" & ConAcct & "' OR INST_NB = '" & Inst_nb & "'"
    
    Set checkRS = New ADODB.Recordset
    checkRS.Open sqlCheck, g_Conn, adOpenDynamic, adLockOptimistic

    If checkRS.EOF = False Then
        sqlInst = "INSERT INTO dbo.MCF_INVALID(BA,CONACCT,INST_NB,BLK_UTIL) VALUES ('" & Ba & "','" & ConAcct & "','" & Inst_nb & "','" & Blk_util & "')"
    Else
        sqlInst = "INSERT INTO dbo.MCF_VALID(BA,CONACCT,INST_NB,BLK_UTIL) VALUES ('" & Ba & "','" & ConAcct & "','" & Inst_nb & "','" & Blk_util & "')"
        g_Conn.Execute sqlInst
    Set checkRS = Nothing
    End If
    
    
dbfRS.MoveNext
Loop
Set dbfRS = Nothing


End Sub

Private Sub dirSource_Change()

File1.Path = dirSource.Path

End Sub

Private Sub drvSource_Change()
dirSource.Path = drvSource.Drive

End Sub

Private Sub File1_Click()
'txtdbfsource = dirSource.Path & "\" & File1.FileName
End Sub

Private Sub Form_Activate()
If WindowState = vbNormal Then
    Me.Move (1000)
    Me.Top = 500
    Me.Height = 5310
    Me.Width = 5400
End If
End Sub

