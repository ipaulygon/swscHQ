VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmRecMRUGrid 
   Caption         =   "MRU Scheduler"
   ClientHeight    =   9405
   ClientLeft      =   60
   ClientTop       =   750
   ClientWidth     =   15150
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   9405
   ScaleWidth      =   15150
   Begin VB.CommandButton cmbManage 
      Caption         =   "&Manage"
      Height          =   495
      Left            =   5280
      TabIndex        =   5
      Top             =   8640
      Width           =   1455
   End
   Begin VB.CommandButton cmbClose 
      Caption         =   "&Close"
      Height          =   495
      Left            =   8160
      TabIndex        =   4
      Top             =   8640
      Width           =   1455
   End
   Begin MSFlexGridLib.MSFlexGrid grdMRU 
      Height          =   7455
      Left            =   240
      TabIndex        =   0
      Top             =   840
      Width           =   14655
      _ExtentX        =   25850
      _ExtentY        =   13150
      _Version        =   393216
      Rows            =   3
      Cols            =   3
      AllowBigSelection=   0   'False
      FocusRect       =   2
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblDay 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   12840
      TabIndex        =   3
      Top             =   240
      Width           =   1815
   End
   Begin VB.Label lblBusCtr 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4920
      TabIndex        =   2
      Top             =   240
      Width           =   4095
   End
   Begin VB.Label lblCycle 
      Caption         =   "Cycle/Month :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   240
      Width           =   1695
   End
End
Attribute VB_Name = "frmRecMRUGrid"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub PopulateGridMRUCount(grd As MSFlexGrid, Query As String)
    On Error GoTo errFillGrid

    If OpenRecordset(g_rs_TSCHED, Query, "frmDiscMRUGrid", "MRU Management") Then
        Screen.MousePointer = vbHourglass
        LoadTSchedGrid g_rs_TSCHED, grd, "Discon"
        Screen.MousePointer = vbDefault
        Set g_rs_TSCHED = Nothing
    End If
    
    Exit Sub
    
errFillGrid:
    MsgBox Err.Description
End Sub
Private Sub BuildMRUSchedGrid(grd As MSFlexGrid, Query As String)
    On Error GoTo errFillGrid

    If OpenRecordset(g_rs_GRIDMRU, Query, "frmDiscMRUGrid", "MRU Management") Then
        Screen.MousePointer = vbHourglass
        ' Build the flexgrid
        LoadMRUSchedGrid g_rs_GRIDMRU, grd
        Screen.MousePointer = vbDefault
        Set g_rs_GRIDMRU = Nothing
        Exit Sub
    End If
    
errFillGrid:
    MsgBox Err.Description
End Sub

Private Sub cmbClose_Click()
    Unload Me
End Sub

Private Sub cmbManage_Click()

grdMRU_DblClick
'    With grdMRU
'        sBusCenter = .TextMatrix(.rowSel, 0)
'        intDayNo = .ColSel
'        lblMouseLoc.Caption = "Business Center = " & sBusCenter & "  Day = " & Val(intDayNo)
'    End With
'    Me.Enabled = False
'    frmMRUMngt.Show
End Sub

Private Sub Form_Activate()
    Dim strSQL As String
    Dim strCycle As String
    
    'frmMRUGrid.WindowState = 2
If WindowState = vbNormal Then
    Me.Move (0)
    Me.Top = 0
    Me.Height = 10230
    Me.Width = 15270
End If
    'strCycle = Right(g_BillMonth, 2)
    strCycle = str(intBillCycle)
    lblCycle.Caption = "Cycle/Month : " & strCycle
    'Build FlexGrid
    'strSQL = "SELECT BC_CODE &" & "' - '" & "& BC_DESC as BUSINESS_CENTER FROM R_BUSCTR ORDER BY BC_CODE"
    strSQL = "SELECT BC_CODE +" + "' - '" + "+ BC_DESC as BUSINESS_CENTER FROM R_BUSCTR ORDER BY BC_CODE"
    BuildMRUSchedGrid grdMRU, strSQL
    
    'Populate with the DCJOBID AND TOTAL RECORD counts FOR THIS CYCLE
'    strSQL = "SELECT T_DC_BOOK.BC_CODE, R_BUSCTR.BC_DESC, T_DC_SCHED.DAYNO, Max(R_BOOKSTAT.BKSTLEVEL) AS MAXMRUSTAT, Count(T_DC_BOOK.DCJOBID) AS NUM_MRU, sum(T_DC_BKINFO.ACCTS) AS TOTREC " & _
'             "FROM (R_BUSCTR INNER JOIN T_DC_BOOK ON R_BUSCTR.BC_CODE=T_DC_BOOK.BC_CODE) INNER JOIN (R_BOOKSTAT INNER JOIN T_DC_SCHED ON R_BOOKSTAT.BKSTCODE=T_DC_SCHED.BKSTCODE) ON (T_DC_BOOK.DCJOBID=T_DC_SCHED.DCJOBID) AND (T_DC_BOOK.CYCLE=T_DC_SCHED.CYCLE) INNER JOIN T_DC_BKINFO ON (T_DC_BOOK.DCJOBID=T_DC_BKINFO.DCJOBID) AND (T_DC_BOOK.CYCLE=T_DC_BKINFO.CYCLE)" & _
'             " WHERE T_DC_BOOK.CYCLE = " & strCycle & _
'             " GROUP BY T_DC_BOOK.BC_CODE, R_BUSCTR.BC_DESC, T_DC_SCHED.DAYNO " & _
'             " ORDER BY T_DC_BOOK.BC_CODE, R_BUSCTR.BC_DESC, T_DC_SCHED.DAYNO"
    strSQL = "SELECT T_RC_BOOK.BC_CODE, R_BUSCTR.BC_DESC, T_RC_BOOK.DAYNO, '1' AS MAXMRUSTAT, Count(T_RC_BOOK.RCJOBID) AS NUM_MRU, sum(T_RC_BOOK.ACCTS) AS TOTREC " & _
             "FROM (R_BUSCTR INNER JOIN T_RC_BOOK ON R_BUSCTR.BC_CODE=T_RC_BOOK.BC_CODE) " & _
             " WHERE T_RC_BOOK.CYCLE = " & strCycle & _
             " GROUP BY T_RC_BOOK.BC_CODE, R_BUSCTR.BC_DESC, T_RC_BOOK.DAYNO " & _
             " ORDER BY T_RC_BOOK.BC_CODE, R_BUSCTR.BC_DESC, T_RC_BOOK.DAYNO"
    PopulateGridMRUCount grdMRU, strSQL
End Sub

Private Sub Form_Unload(Cancel As Integer)
    MDIMain.mnuDisconSched.Enabled = True
End Sub


Private Sub grdMRU_Click()
    With grdMRU
        sBusCenter = .TextMatrix(.rowSel, 0)
        intDayNo = .ColSel
        lblBusCtr.Caption = "Business Center : " & sBusCenter
        lblDay.Caption = "Day : " & Val(intDayNo)
    End With
End Sub

Private Sub grdMRU_DblClick()

    With grdMRU
        sBusCenter = .TextMatrix(.rowSel, 0)
        intDayNo = .ColSel
        lblBusCtr.Caption = "Business Center : " & sBusCenter
        lblDay.Caption = "Day : " & Val(intDayNo)
    End With
    
    Me.Enabled = False
    frmRecMRUMngt.Show
    
End Sub

