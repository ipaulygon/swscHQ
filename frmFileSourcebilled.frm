VERSION 5.00
Begin VB.Form frmFileSourcebilled 
   Caption         =   "FileSource"
   ClientHeight    =   3675
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   3930
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   3675
   ScaleWidth      =   3930
   Begin VB.TextBox txtsource 
      Height          =   495
      Left            =   600
      TabIndex        =   4
      Top             =   3840
      Width           =   2295
   End
   Begin VB.DriveListBox drvSource 
      Height          =   315
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   3670
   End
   Begin VB.DirListBox dirSource 
      Height          =   2340
      Left            =   120
      TabIndex        =   2
      Top             =   585
      Width           =   3670
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&OK"
      Default         =   -1  'True
      Height          =   390
      Left            =   1020
      TabIndex        =   1
      Top             =   3090
      Width           =   915
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   390
      Left            =   2040
      TabIndex        =   0
      Top             =   3105
      Width           =   915
   End
End
Attribute VB_Name = "frmFileSourcebilled"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdOk_Click()
frmValSettings.txtDSource.Text = dirSource.Path
Unload Me
End Sub

Private Sub drvSource_Change()
dirSource.Path = drvSource
End Sub

Private Sub Form_Activate()
If WindowState = vbNormal Then
    Me.Width = 4035
    Me.Height = 4125
    Me.Move (2500)
    Me.Top = 900
End If
End Sub

