Attribute VB_Name = "modSched"
Public Sub load_cmb_HHID(comBox As ComboBox, Optional HHID As String)
Dim tListIndex As Integer
    tListIndex = 1
    With comBox
        '.Clear
        .AddItem ""
        
        If frmAddSched.Caption = "Add Schedule" Then
            sql = "select hhid from F_HH where HHTNO not in ( select HHTNO from T_SCHED) order by hhid"
        Else
            sql = "select hhid from F_HH order by hhid"
        End If
        
        If OpenRecordset(g_rs_RHH, sql, "frmAddSched", "MRU Management") Then
            While Not g_rs_RHH.EOF
                .AddItem g_rs_RHH.Fields(0)
                If g_rs_RHH.Fields(0) = HHID Then
                    .ListIndex = tListIndex
                Else
                    tListIndex = tListIndex + 1
                End If
                g_rs_RHH.MoveNext
            Wend
        'Else
        '    .ListIndex = 0
        End If
    End With
End Sub

Public Sub load_cmb_BookNo(comBox As ComboBox, Optional BookNo As String, Optional HHID As String)
Dim tListIndex As Integer
    tListIndex = 1
    With comBox
        .Clear
        .AddItem ""
        'sql = "select bookno from T_BOOK order by bookno"
        sql = "select bookno from T_BOOK where bookno not in (select bookno from T_SCHED where HHTNO=(select HHTNO from F_HH where hhid='" & HHID & "')) order by bookno"
        If OpenRecordset(g_rs_TBOOK, sql, "frmAddSched", "MRU Management") Then
            While Not g_rs_TBOOK.EOF
                .AddItem g_rs_TBOOK.Fields(0)
                If g_rs_TBOOK.Fields(0) = BookNo Then
                    .ListIndex = tListIndex
                Else
                    tListIndex = tListIndex + 1
                End If
                g_rs_TBOOK.MoveNext
            Wend
        Else
            .ListIndex = 0
        End If
    End With
End Sub

Public Sub load_txt_FromTo(BookNo As String, DayNo As Integer, HHID As String, txtFrom As TextBox, txtTo As TextBox)
Dim HHNo As String
    HHNo = get_HHNo(HHID)
    sql = "select bookfrom,bookto from T_SCHED where bookno='" & BookNo & "' and dayno=" & DayNo & " and HHTNO='" & HHNo & "'"
    If (OpenRecordset(g_rs_TSCHED, sql, "frmAddSched", "MRU Management")) = True Then
        If Not g_rs_TSCHED.EOF Then
            txtFrom.Text = CheckNull(g_rs_TSCHED.Fields(0))
            txtTo.Text = CheckNull(g_rs_TSCHED.Fields(1))
        Else
            txtFrom.Text = ""
            txtTo.Text = ""
        End If
    Else
        txtFrom.Text = ""
        txtTo.Text = ""
    End If
End Sub

Public Sub load_cmb_Reader(comBox As ComboBox, Optional ReaderName As String)
Dim tListIndex As Integer
    tListIndex = 1
    With comBox
        .Clear
        .AddItem ""
        sql = "select rdrname from R_READER order by rdrname"
        If OpenRecordset(g_rs_RREADER, sql, "frmAddSched", "MRU Management") Then
            While Not g_rs_RREADER.EOF
                .AddItem g_rs_RREADER.Fields(0)
                If g_rs_RREADER(0) = ReaderName Then
                    .ListIndex = tListIndex
                Else
                    tListIndex = tListIndex + 1
                End If
                g_rs_RREADER.MoveNext
            Wend
        Else
            .ListIndex = 0
        End If
    End With
End Sub

Public Sub load_cmb_HHNo(comBox As ComboBox, Optional HHNo As String)
Dim tHHNo As String
Dim tListIndex As Integer
    tListIndex = 1
    With comBox
        .Clear
        .AddItem ""
        sql = "select HHTNO from F_HH order by HHTNO"
        If OpenRecordset(g_rs_RHH, sql, "frmAddSched", "MRU Management") Then
            While Not g_rs_RHH.EOF
                .AddItem g_rs_RHH.Fields(0)
                If g_rs_RHH.Fields(0) = HHNo Then
                    .ListIndex = tListIndex
                Else
                    tListIndex = tListIndex + 1
                End If
                g_rs_RHH.MoveNext
            Wend
        Else
            .ListIndex = 0
        End If
    End With
End Sub

Public Sub load_cmb_DayNo(comBox As ComboBox, DayNo As Integer)
    With comBox
        .Clear
        .AddItem ""
        For i = 1 To 31
            .AddItem "Day " & i
        Next
        .ListIndex = DayNo
    End With
End Sub

Public Sub load_lst_Sched(Lst As MSHFlexGrid, HHID As String)
Dim tListIndex As Integer
Dim tBackColor As Long

    With Lst
'        .BackColor = &HCCCCCC
'        .Clear  'check purpose, clears headercaption
        .Row = 1
'        tBackColor = .BackColor
        sql = "select bookno,dayno,currflag from T_SCHED left join F_HH on F_HH.HHTNO=T_SCHED.HHTNO where hhid='" & HHID & "'"
        If OpenRecordset(g_rs_TSCHED, sql, "frmAddSched", "MRU Management") Then
            While Not g_rs_TSCHED.EOF
                .Col = g_rs_TSCHED.Fields(1) - 1
'                If g_rs_TSCHED.Fields(2) = vbYes Then
'                    .BackColor = &HCCCCCC
'                Else
'                    .BackColor = tBackColor
'                End If
                If Len(.Text) > 0 Then
                    .Text = .Text & vbCrLf & CheckNull(g_rs_TSCHED.Fields(0))  '"MULTI"
                Else
                    .Text = CheckNull(g_rs_TSCHED.Fields(0))
                End If
                g_rs_TSCHED.MoveNext
            Wend
        Else
        End If
    End With
End Sub

Public Sub save_Sched_Book(BookNo As String, DayNo As Integer, HHID As String, ReaderName As String, Optional BookFrom As String, Optional BookTo As String)
Dim HHNo As String
Dim ReaderNo As String
Dim SchedStat As String
    
    sql = "select HHTNO from F_HH where hhid='" & HHID & "'"
    If OpenRecordset(g_rs_RHH, sql, "frmMultiBook", "MRU Management") Then
        HHNo = g_rs_RHH.Fields(0)
    Else
        HHNo = ""
    End If
    
    sql = "select readerno from R_READER where rdrname='" & ReaderName & "'"
    If OpenRecordset(g_rs_RREADER, sql, "frmMultiBook", "MRU Management") Then
        ReaderNo = g_rs_RREADER.Fields(0)
    Else
        ReaderNo = ""
    End If
    
    'check bookfrom and book to
    BookFrom = Trim(BookFrom)
    BookTo = Trim(BookTo)
    If IsNumeric(BookFrom) And IsNumeric(BookTo) Then
    Else
        If IsNumeric(BookFrom) Then
        Else
            BookFrom = 0
        End If
        If IsNumeric(BookTo) Then
        Else
            BookTo = 0
        End If
    End If
    
    SchedStat = "D"
    If Len(BookNo) > 0 And Len(HHNo) > 0 And Len(ReaderNo) > 0 Then
        sql = "select count(*) from T_SCHED where bookno='" & BookNo & "' and HHTNO='" & HHNo & "' and readerno='" & ReaderNo & "'"
        OpenRecordset g_rs_TSCHED, sql, "frmMultiBook", "MRU Management"
        If g_rs_TSCHED.Fields(0) = 0 Then
            sql = "insert into T_SCHED(bookno,dayno,hhtno,readerno,bookfrom,bookto,schedstat) " & _
                "values('" & BookNo & "'," & DayNo & ",'" & HHNo & "','" & ReaderNo & "'," & BookFrom & "," & BookTo & ",'" & SchedStat & "')"
            If DBExecute(sql, "frmMultiBook", "") Then
                MsgBox "Schedule saved.", vbOKOnly, "MRMSHQ"
            Else
                MsgBox "Duplicate Schedule Found" & vbCrLf & vbCrLf & "Schedule not saved.", vbExclamation, "MRMSHQ"
                '---> update T_SCHED
            End If
        Else
            sql = "update T_SCHED set bookfrom='" & BookFrom & "', bookto='" & BookTo & "' " & _
                "where bookno='" & BookNo & "' and dayno=" & DayNo & " and hhtno='" & HHNo & "'"
            DBExecute sql, "frmMultiBook", ""
        End If
    End If
End Sub

Function get_HHNo(HHID As String) As String
    sql = "select hhtno from F_HH where hhid='" & HHID & "'"
    If OpenRecordset(g_rs_RHH, sql, "frmMultiBook", "MRU Management") Then
        get_HHNo = g_rs_RHH.Fields(0)
    Else
        get_HHNo = ""
    End If
End Function

Public Sub load_BookNo(comBox As ComboBox, Optional BookNo As String)
Dim tListIndex As Integer
    
On Error GoTo errhandler
    tListIndex = 1
    With comBox
        .Clear
        '.AddItem ""
        sql = "select bookno from T_BOOK order by bookno"
        'sql = "select bookno from T_BOOK where bookno not in (select bookno from T_SCHED where HHTNO=(select HHTNO from F_HH where hhid='" & frmAddSched.cmbRoverNo & "')) order by bookno"
        If OpenRecordset(g_rs_TBOOK, sql, "frmBookSched", "MRU Management") Then
            While Not g_rs_TBOOK.EOF
                .AddItem g_rs_TBOOK.Fields(0)
                If g_rs_TBOOK.Fields(0) = BookNo Then
                    .ListIndex = tListIndex
                Else
                    tListIndex = tListIndex + 1
                End If
                g_rs_TBOOK.MoveNext
            Wend
        Else
            .ListIndex = 0
        End If
    End With
errhandler:
If Err.Number <> 0 Then
    If Err.Number = 380 Then
    Else
        MsgBox Err.Description, , "System Message"
    End If
End If
End Sub

Public Sub load_HHID(comBox As ComboBox, Optional HHID As String)
Dim tListIndex As Integer
    tListIndex = 1
    With comBox
        .Clear
        .AddItem ""
        sql = "select hhid from F_HH order by hhid"
        If OpenRecordset(g_rs_RHH, sql, "frmAddSched", "MRU Management") Then
            While Not g_rs_RHH.EOF
                .AddItem g_rs_RHH.Fields(0)
                If g_rs_RHH.Fields(0) = HHID Then
                    .ListIndex = tListIndex
                Else
                    tListIndex = tListIndex + 1
                End If
                g_rs_RHH.MoveNext
            Wend
        Else
            .ListIndex = 0
        End If
    End With
End Sub

