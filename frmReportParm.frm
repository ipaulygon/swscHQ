VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmReportParm 
   Caption         =   "Report Parameters Screen"
   ClientHeight    =   3270
   ClientLeft      =   1905
   ClientTop       =   2550
   ClientWidth     =   11145
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   3270
   ScaleWidth      =   11145
   Begin VB.CommandButton cmdClose 
      Cancel          =   -1  'True
      Caption         =   "&Close"
      Height          =   495
      Left            =   5880
      TabIndex        =   15
      Top             =   2400
      Width           =   1095
   End
   Begin VB.CommandButton cmdGenerateReport 
      Caption         =   "&Generate"
      Height          =   495
      Left            =   4080
      TabIndex        =   14
      Top             =   2400
      Width           =   1095
   End
   Begin VB.Frame frameReportHeader 
      Caption         =   "Enter Report Parameters"
      Height          =   3135
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   10935
      Begin VB.ComboBox cmbConsecMo 
         Height          =   315
         ItemData        =   "frmReportParm.frx":0000
         Left            =   2640
         List            =   "frmReportParm.frx":000D
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   1440
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.ComboBox cmbBusCtr 
         Height          =   315
         ItemData        =   "frmReportParm.frx":001C
         Left            =   1800
         List            =   "frmReportParm.frx":001E
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   450
         Width           =   3135
      End
      Begin VB.TextBox txtDateFrom 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd-MM-yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   3
         EndProperty
         Height          =   300
         Left            =   4920
         TabIndex        =   1
         Top             =   1440
         Width           =   1695
      End
      Begin MSComCtl2.DTPicker DTPFrom 
         Height          =   300
         Left            =   5280
         TabIndex        =   7
         Top             =   1440
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   529
         _Version        =   393216
         Format          =   16515073
         CurrentDate     =   39448
      End
      Begin VB.TextBox txtDateTo 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd-MM-yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   3
         EndProperty
         Height          =   300
         Left            =   8040
         TabIndex        =   2
         Top             =   1440
         Width           =   1695
      End
      Begin MSComCtl2.DTPicker DTPTo 
         Height          =   300
         Left            =   8400
         TabIndex        =   8
         Top             =   1440
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   529
         _Version        =   393216
         Format          =   16515073
         CurrentDate     =   39813
      End
      Begin VB.ComboBox cmbCycle 
         Height          =   315
         ItemData        =   "frmReportParm.frx":0020
         Left            =   6120
         List            =   "frmReportParm.frx":004B
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   450
         Width           =   855
      End
      Begin VB.ComboBox cmbMRU 
         Height          =   315
         Left            =   8040
         TabIndex        =   4
         Top             =   450
         Width           =   1455
      End
      Begin VB.Frame frameDatePick 
         Caption         =   "Select Status to Match with Date Criteria"
         Height          =   1095
         Left            =   480
         TabIndex        =   11
         Top             =   960
         Visible         =   0   'False
         Width           =   9855
         Begin VB.ComboBox cmbCriteria 
            Height          =   315
            Left            =   360
            Style           =   2  'Dropdown List
            TabIndex        =   18
            Top             =   480
            Width           =   2775
         End
         Begin VB.Label Label4 
            Caption         =   "Date To:"
            Height          =   255
            Left            =   6720
            TabIndex        =   13
            Top             =   480
            Width           =   735
         End
         Begin VB.Label Label3 
            Caption         =   "Date From:"
            Height          =   255
            Left            =   3480
            TabIndex        =   12
            Top             =   480
            Width           =   855
         End
      End
      Begin VB.Label lblConsecMo 
         Caption         =   "No. of Consecutive Months:"
         Height          =   255
         Left            =   480
         TabIndex        =   20
         Top             =   1440
         Visible         =   0   'False
         Width           =   2415
      End
      Begin VB.Label Label7 
         Caption         =   "Date To:"
         Height          =   255
         Left            =   7200
         TabIndex        =   17
         Top             =   1440
         Width           =   735
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Date From:"
         Height          =   255
         Left            =   3720
         TabIndex        =   16
         Top             =   1440
         Width           =   975
      End
      Begin VB.Label Label2 
         Caption         =   "Business Center:"
         Height          =   255
         Left            =   480
         TabIndex        =   5
         Top             =   480
         Width           =   1335
      End
      Begin VB.Label Label5 
         Caption         =   "Cycle:"
         Height          =   255
         Left            =   5520
         TabIndex        =   10
         Top             =   480
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "MRU:"
         Height          =   255
         Left            =   7440
         TabIndex        =   6
         Top             =   480
         Width           =   615
      End
   End
End
Attribute VB_Name = "frmReportParm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub load_BUSCTR()
    
On Error GoTo errhandler
    With cmbBusCtr
        .Clear
        .AddItem "ALL"
        'sql = "select BC_CODE & ' - ' & BC_DESC as BUSCTR from R_BUSCTR order by BC_CODE"
        sql = "select BC_CODE + ' - ' + BC_DESC as BUSCTR from R_BUSCTR order by BC_CODE"
        If OpenRecordset(g_rs_BUSCTR, sql, "frmReportParm", "Report Parameters") Then
            While Not g_rs_BUSCTR.EOF
                .AddItem g_rs_BUSCTR.Fields(0)
                g_rs_BUSCTR.MoveNext
            Wend
'        Else
'            .ListIndex = 0
        End If
    .ListIndex = 0
    End With
    
errhandler:
If Err.Number <> 0 Then
    If Err.Number = 380 Then
    Else
        MsgBox Err.Description, , "System Message"
    End If
End If
End Sub

Private Sub load_MRU(intCycle As Integer, Optional ByVal BusCtr$ = "ALL")
    
' Default based on current cycle or on selected one from list
On Error GoTo errhandler
    With cmbMRU
        .Clear
        .AddItem "ALL"
        sql = "select bookno from T_BOOK where CYCLE = " & intCycle

        If BusCtr$ <> "ALL" Then
           sql = sql & " and BC_CODE = '" & BusCtr$ & "'"
        End If
        'Build sql statement
        sql = sql & " order by bookno"
        If OpenRecordset(g_rs_TBOOK, sql, "frmReportParm", "Report Parameters") Then
            While Not g_rs_TBOOK.EOF
                .AddItem g_rs_TBOOK.Fields(0)
                g_rs_TBOOK.MoveNext
            Wend
'        Else
'            .ListIndex = 0
        End If
        .ListIndex = 0
    End With
    
errhandler:
If Err.Number <> 0 Then
    If Err.Number = 380 Then
    Else
        MsgBox Err.Description, , "System Message"
    End If
End If
End Sub


Private Sub Load_MRU_Status()
    cmbCriteria.AddItem "Scheduled MR Date"
    cmbCriteria.AddItem "Downloaded From MWSI"
    cmbCriteria.AddItem "Sent To Satellite Office"
    cmbCriteria.AddItem "Received From Satellite Office"
    cmbCriteria.AddItem "Forwarded to MWSI"
    cmbCriteria.AddItem "ALL"
    cmbCriteria.ListIndex = 5
End Sub


Private Sub Load_Num_Tries()
    cmbCriteria.AddItem "One (1) Time"
    cmbCriteria.AddItem "Two (2) Times"
    cmbCriteria.AddItem "Three (3) Times"
    cmbCriteria.AddItem "Four or More (4+) Times"
    cmbCriteria.ListIndex = 0
End Sub


Private Sub cmbBusCtr_Click()
    Dim sBusCtr As String
    Dim sCycle As String
    
    sBusCtr = cmbBusCtr.Text
    sCycle = cmbCycle.Text
    If sBusCtr = "ALL" Then
        load_MRU CInt(sCycle)
    Else
        load_MRU CInt(sCycle), Left$(sBusCtr, 4)
    End If
End Sub


Private Sub cmbCycle_Click()
    Dim sBusCtr As String
    Dim sCycle As String
    Dim dte As Date
    
    sBusCtr = cmbBusCtr.Text
    sCycle = cmbCycle.Text
    load_MRU CInt(sCycle), Left$(sBusCtr, 4)
    If sCycle <> 0 Then
        dte = DateValue(sCycle & "/" & "01" & "/" & Year(Date))
        'Set Datepicker to first and last day of the selected month
        DTPFrom.Value = DateAdd("d", -(Day(dte) - 1), dte)
        DTPTo.Value = DateAdd("d", -(Day(DateAdd("m", 1, dte))), _
                              DateAdd("m", 1, dte))
        txtDateFrom.Text = sCycle & "/" & DTPFrom.Day & "/" & DTPFrom.Year
        txtDateTo.Text = sCycle & "/" & DTPTo.Day & "/" & DTPTo.Year
    End If
End Sub




Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub cmdGenerateReport_Click()
    Dim sParm As String
    Dim sMRU As String
    Dim sCycle As String
    
    sMRU = Trim(cmbMRU.Text)
    If sMRU = "" Then
        MsgBox "Please input value for MRU.", vbExclamation, "Report Viewer"
        Exit Sub
    End If
    
    sCycle = Trim(cmbCycle.Text)
    If sCycle = "0" And g_str_rep <> "MRU STATUS REPORT.rpt" Then
        MsgBox "This cycle value is not valid for this report.", vbExclamation, "Report Viewer"
        Exit Sub
    End If
    
    ' Collate all report parameters
    arrReportParm(0) = cmbBusCtr.Text
    arrReportParm(1) = cmbCycle.Text
    arrReportParm(2) = cmbMRU.Text
    
    If frameDatePick.Visible Then
        sParm = str$(cmbCriteria.ListIndex + 1)
        arrReportParm(3) = Trim(sParm)
    Else
        arrReportParm(3) = "*"
    End If
    
'    If g_str_rep = "FIELD FINDINGS SUMMARY REPORT.rpt" Then
'        arrReportParm(4) = txtDateFrom.Text
'        'cReport.ParameterFields(4).AddCurrentValue CStr(txtDateFrom.Text)
'    Else
        If txtDateFrom.Visible Then
            If IsDate(txtDateFrom.Text) = False Or IsDate(txtDateTo.Text) = False Then
                MsgBox "Date entered is not a valid date. Please re-enter a valid date.", vbExclamation, "Report Viewer"
                Exit Sub
            Else
                If CDate(txtDateFrom.Text) > CDate(txtDateTo.Text) Then
                    MsgBox "Date To value should be greater than Date From value.", vbExclamation, "Report Viewer"
                    Exit Sub
                Else
                    arrReportParm(4) = txtDateFrom.Text
                    arrReportParm(5) = txtDateTo.Text
                End If
            End If
        Else
            ' Make the parameters wild cards
            arrReportParm(4) = "*": arrReportParm(5) = "*":
        End If
'    End If
    
    arrReportParm(6) = cmbConsecMo.Text
    Unload Me
    frmCRViewer.Show
End Sub

Private Sub DTPFrom_Change()
    txtDateFrom.Text = DTPFrom.Month & "/" & DTPFrom.Day & "/" & DTPFrom.Year
End Sub

Private Sub DTPTo_Change()
    txtDateTo.Text = DTPTo.Month & "/" & DTPTo.Day & "/" & DTPTo.Year
End Sub

Private Sub Form_Load()
    Dim iCycle As Integer
If WindowState = vbNormal Then
        Me.Height = 3780
        Me.Width = 11265
End If
    Me.Caption = "Report Parameters for " & Left(g_str_rep, Len(g_str_rep) - 4)
    iCycle = CInt(Right$(g_BillMonth, 2))

    'Set Datepicker to first and last day of the current month
    DTPFrom.Value = DateAdd("d", -(Day(Now) - 1), Now)
    DTPTo.Value = DateAdd("d", -(Day(DateAdd("m", 1, Now))), _
                          DateAdd("m", 1, Now))
    txtDateFrom.Text = DTPFrom.Month & "/" & DTPFrom.Day & "/" & DTPFrom.Year
    txtDateTo.Text = DTPTo.Month & "/" & DTPTo.Day & "/" & DTPTo.Year
    
    If g_str_rep = "MRU STATUS REPORT.rpt" Then
        frameDatePick.Visible = True
        ' Load_Combo_Status
        Load_MRU_Status
        Label3.Visible = True
        Label4.Visible = True
        txtDateFrom.Visible = True
        txtDateTo.Visible = True
        DTPFrom.Visible = True
        DTPTo.Visible = True
    ElseIf g_str_rep = "REPLACED METER REPORT.rpt" Then
        Label6.Visible = True
        Label7.Visible = True
        txtDateFrom.Visible = True
        txtDateTo.Visible = True
        DTPFrom.Visible = True
        DTPTo.Visible = True
    ElseIf g_str_rep = "DETAILED METER READING REPORT.rpt" Then
        Label6.Visible = True
        Label7.Visible = True
        txtDateFrom.Visible = True
        txtDateTo.Visible = True
        DTPFrom.Visible = True
        DTPTo.Visible = True
    ElseIf g_str_rep = "SUMMARY METER READING REPORT.rpt" Then
        Label6.Visible = True
        Label7.Visible = True
        txtDateFrom.Visible = True
        txtDateTo.Visible = True
        DTPFrom.Visible = True
        DTPTo.Visible = True
    ElseIf g_str_rep = "EDITED READING REPORT.rpt" Then
        ' Change Frame Caption and Radio Button Labels
        frameDatePick.Visible = True
        frameDatePick.Caption = "Specify Minimum Number of Modifications"
        ' Load Number of Tries
        Load_Num_Tries
'        Label3.Visible = False
'        Label4.Visible = False
'        txtDateFrom.Visible = False
'        txtDateTo.Visible = False
'        DTPFrom.Visible = False
'        DTPTo.Visible = False
    ElseIf g_str_rep = "FIELD FINDINGS SUMMARY REPORT.rpt" Then
'        Label6.Caption = "Reading Date"
'        Label6.Visible = True
'        Label3.Visible = True
'        Label4.Visible = False
'        txtDateTo.Visible = False
'        DTPTo.Visible = False
   ' ElseIf g_str_rep = "OC VALIDATION REPORT.rpt" Then
    ElseIf g_str_rep = "CONSECUTIVE MONTHS SAME OC REPORT.rpt" Then
        lblConsecMo.Visible = True
        cmbConsecMo.Visible = True
        cmbConsecMo.Text = "ALL"
    Else
        frameDatePick.Visible = False
'        Label3.Visible = False
'        Label4.Visible = False
'        txtDateFrom.Visible = False
'        txtDateTo.Visible = False
'        DTPFrom.Visible = False
'        DTPTo.Visible = False
    End If
    'cmbCycle.ListIndex = iCycle
    cmbCycle.Text = iCycle
    load_MRU iCycle
    load_BUSCTR
End Sub
