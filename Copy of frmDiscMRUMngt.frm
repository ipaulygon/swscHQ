VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmDiscMRUMngt 
   Caption         =   "Manage MRUs"
   ClientHeight    =   9720
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15150
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9720
   ScaleWidth      =   15150
   Begin VB.CommandButton cmdSelectNone 
      Caption         =   "Select &None"
      Height          =   450
      Left            =   4080
      TabIndex        =   10
      Top             =   9000
      Width           =   1815
   End
   Begin VB.CommandButton cmdSelectAll 
      Caption         =   "Select &All"
      Height          =   450
      Left            =   1920
      TabIndex        =   9
      Top             =   9000
      Width           =   1815
   End
   Begin VB.CommandButton cmdRSO 
      Caption         =   "&Receive Upload"
      Height          =   450
      Left            =   8400
      TabIndex        =   8
      Top             =   9000
      Width           =   1815
   End
   Begin VB.CommandButton cmdSSO 
      Caption         =   "Create &Download"
      Height          =   450
      Left            =   6240
      TabIndex        =   7
      Top             =   9000
      Width           =   1815
   End
   Begin VB.Frame fraSelect 
      Height          =   7785
      Left            =   240
      TabIndex        =   1
      Top             =   960
      Width           =   14715
      Begin MSComctlLib.ListView lsvMRUMngt 
         Height          =   7395
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   14475
         _ExtentX        =   25532
         _ExtentY        =   13044
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   14
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "CAN"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Disconnection Reason"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "MRU"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Job ID"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Meter Number"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "PUA"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Current"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Total Amount"
            Object.Width           =   3705
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "SEQ"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Text            =   "NAME"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Text            =   "ADDRESS"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Text            =   "RECONFEE"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Text            =   "DISCTAG"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Text            =   "LASTRDG"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.CommandButton cmdClose 
      Cancel          =   -1  'True
      Caption         =   "&Close"
      Height          =   450
      Left            =   10560
      TabIndex        =   0
      Top             =   9000
      Width           =   1815
   End
   Begin VB.Label lblNumMRU 
      Caption         =   "Number of MRUs : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10920
      TabIndex        =   6
      Top             =   600
      Width           =   2295
   End
   Begin VB.Label lblDayNo 
      Caption         =   "Day Number : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10920
      TabIndex        =   5
      Top             =   240
      Width           =   1815
   End
   Begin VB.Label lblCycle 
      Caption         =   "Cycle/Month : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   480
      TabIndex        =   4
      Top             =   600
      Width           =   1575
   End
   Begin VB.Label lblBusCtr 
      Caption         =   "Business Center : "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   480
      TabIndex        =   3
      Top             =   240
      Width           =   6015
   End
End
Attribute VB_Name = "frmDiscMRUMngt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim iChecked As Integer
Dim MRUArray(1000) As String
Dim AcctArray(1000) As String
Dim MRUList As String
Dim AcctList As String
Dim SRSO As String

Sub UpdateRSOUpload(ByVal sMRU As String, iCycle As Integer)

    Dim strSQL As String

    ' First Update status to "Received From SO" in T_SCHED
    strSQL = "UPDATE T_SCHED SET BKSTCODE = 'RSO' WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
    If Not DBExecute(strSQL, Me.Name, "UpdateRSOUpload") Then
        Rollback_Upload sMRU, iCycle
        Exit Sub
    End If

    ' Next place the timestamp in T_BOOK
     strSQL = "UPDATE T_BOOK SET RECV_SO_DT = getdate() WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
     If Not DBExecute(strSQL, Me.Name, "UpdateRSOUpload") Then
        Rollback_Upload sMRU, iCycle
        Exit Sub
    End If

End Sub

Sub UpdateDCSSODownload(ByVal sAcct As String, iCycle As Integer)

    Dim strSQL As String

    ' First Update status to "Sent To SO" in T_SCHED
    strSQL = "UPDATE T_SCHED SET BKSTCODE = 'SSO' WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
    If Not DBExecute(strSQL, Me.Name, "UpdateSSODownload") Then Exit Sub

    ' Next place the timestamp in T_BOOK
     strSQL = "UPDATE T_BOOK SET UPLD_SO_DT = getdate() WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
     If Not DBExecute(strSQL, Me.Name, "UpdateSSODownload") Then Exit Sub

End Sub

Sub UpdateSSODownload(ByVal sMRU As String, iCycle As Integer)

    Dim strSQL As String

    ' First Update status to "Sent To SO" in T_SCHED
    strSQL = "UPDATE T_SCHED SET BKSTCODE = 'SSO' WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
    If Not DBExecute(strSQL, Me.Name, "UpdateSSODownload") Then Exit Sub

    ' Next place the timestamp in T_BOOK
     strSQL = "UPDATE T_BOOK SET UPLD_SO_DT = getdate() WHERE BOOKNO = '" & sMRU & "' AND CYCLE = " & str(iCycle)
     If Not DBExecute(strSQL, Me.Name, "UpdateSSODownload") Then Exit Sub

End Sub

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub cmdRSO_Click()
'Dim iCycle As Integer
'Dim sMRU As String, sBC As String
'Dim strSQL As String
'Dim sBCPath As String
'Dim n As Integer
'
'    SRSO = "RSO"
'    If Generate_MRU_List = False Then
'        Exit Sub
'    End If
'
'    'sMRU = lsvMRUMngt.SelectedItem.Text
'    'iCycle = Val(Right(g_BillMonth, 2))
'    iCycle = str(intBillCycle)
'
'    n = 1
'    For n = 1 To iChecked
'        sMRU = MRUArray(n)
'        sBC = Left$(sMRU, 4)
'
'        strSQL = "SELECT WORKING_DIR FROM R_BUSCTR WHERE BC_CODE = '" & sBC & "'"
'        OpenRecordset g_rs_BUSCTR, strSQL, "frmDiscMRUMngt", "cmdRSO_Click"
'        g_rs_BUSCTR.MoveFirst
'
'        sBCPath = g_rs_BUSCTR.Fields(0) & ""
'
'        If UploadToDB(sMRU, iCycle, sBCPath) Then
'            'UpdateRSOUpload sMRU, iCycle
''            MsgBox "Receive File from SO Successful!", vbInformation, "Receive File from Satellite Office"
''            ListView_Load
'        Else
'            MsgBox "Receive File from SO for MRU " & sMRU & " Failed or Aborted!", vbCritical, "Receive File from Satellite Office"
'            Exit Sub
'        End If
'    Next n
'
'    ListView_Load
'    MsgBox "Receive File from SO Successful!", vbInformation, "Receive File from Satellite Office"



    ' Function that reads the file of meter reading information from the Satellite Offices with an additional
    '   "header and footer line" that provides hash totals for the data.   This file will then
    '   be "unwrapped" at the MRMS HQ Side and processed before being uploaded to MWSI (Central)

'CurrentBookNo As String, cycle As Integer, dir As String
    ' Connection and recordset for the text and Access link
    Dim cnTxt As Connection, cnDB As Connection
    Dim rsTxt As Recordset, rsDB As Recordset, rsDBdl As Recordset, rsDBul As Recordset

    ' Use to copy the source file to "upload.txt"
    Dim f As Object, fso As Object

    ' Counts to update into T_BKINFO
    Dim totalrecs As Integer
    Dim unreadcount As Integer
    Dim readcount As Integer
    Dim oorcount As Integer

    Dim inv(8) As String
    Dim n As Integer
    Dim BCDESC As String

    ' local variables
    Dim bkfile As String
    Dim fcfile As String, fcfile2 As String
    Dim readtag As String, oortag As String
    Dim sql$
    Dim fs
    Dim i As Integer
    Dim CurrentDnldFilePath As String
    Dim progcnt As Double
    Dim bValidateOK As Boolean
    Dim sPath As String
    Dim fc As Integer
    Dim withfc As Boolean
    Dim dlcnt As Integer
    Dim ulcnt As Integer
    Dim frso As String
    Dim fcfrso As String
'    Dim bMatchOK As Boolean

    'On Error GoTo errhandler

' Text file cn and rs
Set cnTxt = New ADODB.Connection
Set rsTxt = New ADODB.Recordset

' Local DB cn and rs
Set cnDB = New ADODB.Connection
Set rsDB = New ADODB.Recordset
Set rsDBdl = New ADODB.Recordset
Set rsDBul = New ADODB.Recordset

' Open connections and recordsets
cnTxt.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & ";Extended Properties='text;HDR=No;FMT=Delimited'"
cnTxt.Open

cnDB.ConnectionString = "Provider=SQLOLEDB.1; Data Source=SZ-MWSI-SERVER;database=MCFSDB; Initial Catalog=;User Id=sa;Password=sqladmin;"
cnDB.Open

' start of transaction for rollback if error encountered
cnDB.BeginTrans

'rsDBdl.Open "select * from t_download where bookno='" & CurrentBookNo & "' and dlcycle= " & str(cycle) & "", cnDB, adOpenStatic, adLockReadOnly
'dlcnt = rsDBdl.RecordCount

' get bc and sched mr date
OpenRecordset g_rs_TDCBOOK, "SELECT BC_CODE, ACTUAL_DL_DT FROM T_DC_BOOK WHERE DCJOBID = '" & CurrentBookNo & "' and CYCLE = " & cycle & "", "", "UploadToDB"

If Not OpenRecordset(g_rs_RBUSCTR, "SELECT WORKING_DIR, BC_DESC FROM R_BUSCTR WHERE BC_CODE =(select bc_code from T_book where bookno='" & CurrentBookNo & "' and cycle=" & cycle & ")", "", "UploadToDB") Then
    UploadToDB = False
    Exit Function
End If

'==================== invalid folder name
    ' invalid characters for folder name
    inv(0) = "\"
    inv(1) = "/"
    inv(2) = ":"
    inv(3) = "*"
    inv(4) = "?"
    inv(5) = """"
    inv(6) = "<"
    inv(7) = ">"
    inv(8) = "|"
    ' check for invalid folder name
    For n = 0 To 8
        If InStr(1, g_rs_RBUSCTR.Fields(1).Value, inv(n), vbTextCompare) > 0 Then
            ' if first character is invalid, folder cannot be created and will exit sub
            If InStr(1, g_rs_RBUSCTR.Fields(1).Value, inv(n), vbTextCompare) = 1 Then
                MsgBox "A folder name cannot contain the character " & inv(n) & ". Please edit the business center name.", vbExclamation, "Invalid Folder Name"
                Exit Function
            ' folder name to be created will be chars before first occurrence of invalid character
            Else
                'MsgBox "A folder name cannot contain the character " & inv(n) & ". Reports will be saved in folder " & Mid(g_rs_RBUSCTR.Fields(1).Value, 1, InStr(1, g_rs_RBUSCTR.Fields(1).Value, inv(n), vbTextCompare) - 1) & ".", vbExclamation, "Invalid Folder Name"
                'BCDESC = Trim(Mid(g_rs_RBUSCTR.Fields(1).Value, 1, InStr(1, g_rs_RBUSCTR.Fields(1).Value, inv(n), vbTextCompare) - 1))
                BCDESC = Replace(g_rs_RBUSCTR.Fields(1).Value, inv(n), "")
                Exit For
            End If
        Else
            BCDESC = Trim(g_rs_RBUSCTR.Fields(1).Value)
        End If
    Next n
'=======================

sPath = g_rs_RBUSCTR.Fields(0) & ""
If sPath = "" Then
    MsgBox "Business Center Directory not specified!", vbCritical, "System Message"
    UploadToDB = False
    Exit Function
End If

Set f = CreateObject("Scripting.FileSystemObject")
'' create folder FRSO\BC\Cycle\sched MR date
'If Not f.FolderExists(sPath) Then f.CreateFolder (sPath) ' upload directory
'If Not f.FolderExists(sPath & "\FRSO\") Then f.CreateFolder (sPath & "\FRSO\") ' FRSO folder
'If Not f.FolderExists(sPath & "\FRSO\" & g_rs_TBOOK.Fields(0)) Then f.CreateFolder (sPath & "\FRSO\" & g_rs_TBOOK.Fields(0)) ' BC folder
'If Not f.FolderExists(sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & "\" & Padl(cycle, 2, "0")) Then f.CreateFolder (sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & "\" & Padl(cycle, 2, "0")) ' cycle folder
'If Not f.FolderExists(sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & "\" & Padl(cycle, 2, "0") & "\" & GetMonth(CInt(Month(g_rs_TBOOK.Fields(1))))) Then f.CreateFolder (sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & "\" & Padl(cycle, 2, "0") & "\" & GetMonth(CInt(Month(g_rs_TBOOK.Fields(1))))) ' sched mr date month folder
'If Not f.FolderExists(sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & "\" & Padl(cycle, 2, "0") & "\" & GetMonth(CInt(Month(g_rs_TBOOK.Fields(1)))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1)))) Then f.CreateFolder (sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & "\" & Padl(cycle, 2, "0") & "\" & GetMonth(CInt(Month(g_rs_TBOOK.Fields(1)))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1)))) ' sched mr date day folder
'
''CurrentDnldFilePath = sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & "\" & Padl(Cycle, 2, "0") & "\" & GetMonth(CInt(Month(g_rs_TBOOK.Fields(1)))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1))) & "\" & "HQ" & Format(Cycle, "0#") & CurrentBookNo & ".txt"
'CurrentDnldFilePath = sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & "\" & Padl(cycle, 2, "0") & "\" & GetMonth(CInt(Month(g_rs_TBOOK.Fields(1)))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1))) & "\"

' create folder frso\bccode bcdesc\year\cycle monthname\sched mr day, i.e. FRSO\0100 Novaliches\2008\09 September\06
If Not f.FolderExists(sPath) Then f.CreateFolder (sPath) ' upload directory
If Not f.FolderExists(sPath & "\FRSO\") Then f.CreateFolder (sPath & "\FRSO\") ' FRSO folder
If Not f.FolderExists(sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC) Then f.CreateFolder (sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC) ' BC folder
If Not f.FolderExists(sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1))) Then f.CreateFolder (sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1))) ' year folder
If Not f.FolderExists(sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0")))) Then f.CreateFolder (sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0")))) ' cycle folder
If Not f.FolderExists(sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1)))) Then f.CreateFolder (sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1)))) ' sched mr day folder

CurrentDnldFilePath = sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1))) & "\"

'Set fs = CreateObject("Scripting.FileSystemObject")
'Set f = fs.GetFolder(dir)
'
'If Not fs.FolderExists(f & "\FRSO") Then
'    i = MsgBox("Satellite upload file archive folder does not exist! Create it?", vbYesNo, "Receive SO File")
'    If i = vbYes Then
'       fs.CreateFolder (f & "\FRSO")
'    Else
'        Unload frmProg
'        Exit Function
'    End If
'End If
'CurrentDnldFilePath = f & "\FRSO\"

'==============================
'    'Initialize Form variables
'    iTotalAccts = 0
'    iRead = 0: iUnread = 0
'    iOutOfRange = 0
'    sReadingDate = ""
'    bValidateOK = True
'==============================

    ' Assign filenames to load
    Set fso = CreateObject("Scripting.FileSystemObject")
    'bkfile = sPath & "\" & "HQDC" & Format(cycle, "0#") & CurrentBookNo & "01.TXT"
    bkfile = sPath & "\" & "HQDC" & g_rs_RBUSCTR.Fields(0).Value & Format(Date, "yyyymmdd") & "01.TXT"
    If Not (fso.FileExists(bkfile)) Then
        MsgBox "Accomplished Reading File does not exist!", vbInformation, "System Message"
        'UploadToDB = False
        Exit Sub 'Function
    End If

'    ' Version 1.10 for MRMS HQ: unwrapper function; assign values to the Form Variables above, including iCycle
'    '   If ANY file cannot be unwrapped, exit the operation
'     If Not UnwrapHQfromSO(bkfile) Then
'       MsgBox "Could not unwrap source file " & bkfile, vbCritical, "System Message"
'       Exit Function
'     End If

    ' Check if batch was already received
    rsDB.Open "SELECT * FROM T_DC_BOOK WHERE BC_CODE = '" & CurrentBookNo & _
              "' AND CYCLE = " & str(cycle) & " AND RECV_SO_DT Is Not Null", cnDB, adOpenStatic, adLockReadOnly

    If rsDB.RecordCount > 0 Then
        Unload frmProg
        MsgBox "MRU already received from SO!", vbExclamation, "System Message"
        UploadToDB = False
        rsDB.Close
        Exit Function
    End If
    ' End Check Batch

    rsTxt.Open "SELECT * FROM upload.txt", cnTxt, adOpenStatic, adLockOptimistic, adCmdText

    rsDB.Close
    ' Open T_UPLOAD for initialization
    rsDB.Open "SELECT * FROM T_DC_UPLOAD", cnDB, adOpenDynamic, adLockOptimistic

    totalrecs = 0
    readcount = 0
    unreadcount = 0
    oorcount = 0

    ' Prevent Division by Zero if only one record in MRU
    If rsTxt.RecordCount = 1 Then
        progcnt = 100
    Else
        progcnt = 1 / (rsTxt.RecordCount - 1) * frmProg.Shape2.Width '100
    End If

    ' Load into T_UPLOAD - ACCOMPLISHED READINGS
    Do While Not rsTxt.EOF

    If OpenRecordset(g_rs_TUPLOAD, "SELECT * FROM T_DC_UPLOAD WHERE ACCTNUM = '" & rsTxt.Fields("AccountNum") & "' and bookno='" & CurrentBookNo & "' and ulcycle= " & str(cycle) & "", "", "") Then
        GoTo Continue_Process:
    Else
    End If

        totalrecs = totalrecs + 1
        frmProg.ProgressBar progcnt, CurrentBookNo

        With rsDB

            .AddNew
            '.Fields("BOOKNO") = rsTxt.Fields("MRUNumber")
           ' .Fields("ULCYCLE") = cycle
            '.Fields("ULDOC_NO") = rsTxt.Fields("DocumentNo") & ""
'            If rsTxt.Fields("AccountNum") = "" Or IsNull(rsTxt.Fields("AccountNum")) = True Then
'                'OpenRecordset g_rs_TDOWNLOAD, "SELECT ACCTNUM FROM T_DOWNLOAD WHERE BOOKNO = '" & rsTxt.Fields("MRUNumber") & "' and DLCYCLE = " & iCycle & " and DLDOC_NO = '" & rsTxt.Fields("DocumentNo") & "" & "'", "", "UploadToDB"
'                OpenRecordset g_rs_TDOWNLOAD, "SELECT ACCTNUM FROM T_DOWNLOAD WHERE BOOKNO = '" & rsTxt.Fields("MRUNumber") & "' and DLCYCLE = " & cycle & " and DLDOC_NO = '" & rsTxt.Fields("DocumentNo") & "" & "'", "", "UploadToDB"
'                .Fields("ACCTNUM") = g_rs_TDOWNLOAD.Fields(0)
'            Else
'                .Fields("ACCTNUM") = rsTxt.Fields("AccountNum") & ""
'            End If
'            If rsTxt.Fields("Installation") = "" Or IsNull(rsTxt.Fields("Installation")) = True Then
'                'OpenRecordset g_rs_TDOWNLOAD, "SELECT ACCTNUM FROM T_DOWNLOAD WHERE BOOKNO = '" & rsTxt.Fields("MRUNumber") & "' and DLCYCLE = " & iCycle & " and DLDOC_NO = '" & rsTxt.Fields("DocumentNo") & "" & "'", "", "UploadToDB"
'                OpenRecordset g_rs_TDOWNLOAD, "SELECT ACCTNUM FROM T_DOWNLOAD WHERE BOOKNO = '" & rsTxt.Fields("MRUNumber") & "' and DLCYCLE = " & cycle & " and DLDOC_NO = '" & rsTxt.Fields("DocumentNo") & "" & "'", "", "UploadToDB"
'                .Fields("ACCTNUM") = g_rs_TDOWNLOAD.Fields(0)
'            Else
'                .Fields("ACCTNUM") = rsTxt.Fields("Installation") & ""
'            End If
            .Fields("ACCTNUM") = rsTxt.Fields("AccountNum") & ""
            .Fields("DCDATE") = rsTxt.Fields("DT") & ""
            .Fields("READING") = rsTxt.Fields("Rdg") & ""
            .Fields("SEALNO") = rsTxt.Fields("SealNo") & ""
            .Fields("DISCREASON") = rsTxt.Fields("ReasonCd") & ""
            .Fields("RESP_PERSON") = rsTxt.Fields("RespPerson") & ""
            .Fields("REMARKS") = rsTxt.Fields("Rem") & ""
            .Fields("PYMT_DT") = rsTxt.Fields("PymtDt") & ""
            .Fields("PYMT_AMT") = rsTxt.Fields("PymtAmt") & ""
            .Fields("SERVICE_MAN") = rsTxt.Fields("ServiceMan") & ""
            .Fields("STAT_CD") = rsTxt.Fields("Status") & ""
''            readtag = rsTxt.Fields("MtrRdgType") & ""
''            If readtag = "01" Then
''               readcount = readcount + 1
''            Else
''               unreadcount = unreadcount + 1
''            End If
''            .Fields("MRTYPE") = readtag
'            .Fields("MRTYPE") = rsTxt.Fields("MtrRdgType") & ""
'            '.Fields("MTRRDRNAME") = rsTxt.Fields("MtrRdrName") & ""
'            .Fields("REMARKS") = rsTxt.Fields("Remarks") & ""
'            .Fields("BILLED_RDG") = rsTxt.Fields("BilledRdg") & ""
'            .Fields("TRIES") = Val(rsTxt.Fields("Tries") & "")
'            .Fields("BILLED_CONS") = rsTxt.Fields("BilledCons") & ""
''            oortag = rsTxt.Fields("RangeCode") & ""
''            If (oortag = "3") Or (oortag = "4") Then oorcount = oorcount + 1
''            .Fields("RANGECODE") = oortag
'            .Fields("RANGECODE") = rsTxt.Fields("RangeCode") & ""
'            .Fields("NEWSEQNO") = Val(rsTxt.Fields("NewSeqNo") & "")
'            .Fields("NEWMTRBRAND") = rsTxt.Fields("NewMtrBrand") & ""
'            .Fields("NEWMTRNUM") = rsTxt.Fields("NewMtrNumber") & ""
'            .Fields("DEL_CODE") = rsTxt.Fields("DelCode") & ""
'            .Fields("DEL_REMARKS") = rsTxt.Fields("DelRemarks") & ""
'            .Fields("PRINT_TAG") = rsTxt.Fields("PrintTag") & ""
'            .Fields("NUM_USERS") = rsTxt.Fields("NumUsers") & ""
'            .Fields("BASECHRG") = IIf(IsNull(rsTxt.Fields("BaseChrg").Value) = True, "0", rsTxt.Fields("BaseChrg").Value & "")
'            .Fields("DISCOUNT") = IIf(IsNull(rsTxt.Fields("Discount").Value) = True, "0", rsTxt.Fields("Discount").Value & "")
'            .Fields("CERA") = IIf(IsNull(rsTxt.Fields("CERA").Value) = True, "0", rsTxt.Fields("CERA").Value & "")
'            .Fields("FCDA") = IIf(IsNull(rsTxt.Fields("FCDA").Value) = True, "0", rsTxt.Fields("FCDA").Value & "")
'            .Fields("STM") = IIf(IsNull(rsTxt.Fields("STM").Value) = True, "0", rsTxt.Fields("STM").Value & "")
'            .Fields("ENVCHRG") = IIf(IsNull(rsTxt.Fields("EnvChrg").Value) = True, "0", rsTxt.Fields("EnvChrg").Value & "")
'            .Fields("SEWERCHRG") = IIf(IsNull(rsTxt.Fields("SewerChrg").Value) = True, "0", rsTxt.Fields("SewerChrg").Value & "")
'            .Fields("POWERCOST") = IIf(IsNull(rsTxt.Fields("Powercost").Value) = True, "0", rsTxt.Fields("Powercost").Value & "")
'            .Fields("PREPAYADJ") = IIf(IsNull(rsTxt.Fields("PrepayAdj").Value) = True, "0", rsTxt.Fields("PrepayAdj").Value & "")
'            .Fields("MSC") = IIf(IsNull(rsTxt.Fields("MSC").Value) = True, "0", rsTxt.Fields("MSC").Value & "")
'            .Fields("VAT") = IIf(IsNull(rsTxt.Fields("VAT").Value) = True, "0", rsTxt.Fields("VAT").Value & "")
'            .Fields("PIA") = IIf(IsNull(rsTxt.Fields("PIA").Value) = True, "0", rsTxt.Fields("PIA").Value & "")
'            .Fields("TOTCURRCHRG") = IIf(IsNull(rsTxt.Fields("TotCurrChrg").Value) = True, "0", rsTxt.Fields("TotCurrChrg").Value & "")
'            .Fields("TOTALAMTDUE") = IIf(IsNull(rsTxt.Fields("TotalAmtDue").Value) = True, "0", rsTxt.Fields("TotalAmtDue").Value & "")
'            .Fields("PRINT_CNT") = IIf(IsNull(rsTxt.Fields("PrintCnt").Value) = True, "0", rsTxt.Fields("PrintCnt").Value & "")
'            .Fields("SOA_NUMBER") = rsTxt.Fields("SOANumber") & ""
'            .Fields("CONS_TAG") = rsTxt.Fields("ConsTag") & ""
'            '.Fields("RDGHIST") = rsTxt.Fields("RdgHist") & ""
'            '.Fields("POST_DT") = rsTxt.Fields("PostDt") & ""
            .Update
        End With

Continue_Process:
        rsTxt.MoveNext

    Loop

' check total accounts loaded in t_upload
rsDBul.Open "select * from t_upload where bookno='" & CurrentBookNo & "' and ulcycle= " & str(cycle) & "", cnDB, adOpenStatic, adLockReadOnly
ulcnt = rsDBul.RecordCount

    ' Validate Totals
'     bValidateOK = ValidateTotals(totalrecs, readcount, unreadcount, oorcount)
'     If Not bValidateOK Then
'          Unload frmProg
'          UploadToDB = False
'          Rollback_Upload CurrentBookNo, iCycle
'          Exit Function
'     End If

     ' Match Uploaded Records with Downloaded Records for the MRU on this Cycle
'     bMatchOK = Match_UL_DL_Recs(CurrentBookNo, iCycle)
'     If Not bMatchOK Then
'          Unload frmProg
'          UploadToDB = False
'          Rollback_Upload CurrentBookNo, iCycle
'          Exit Function
'     End If

    ' Update T_BKINFO
'    sql$ = "UPDATE T_BKINFO SET UNREAD = " & str(iUnread) & ", READCNT = " & str(iRead) & _
'           ", OORCNT =  " & str(iOutOfRange) & " WHERE BOOKNO = '" & CurrentBookNo & "' AND CYCLE = " & iCycle
'    If Not DBExecute(sql$, "MDIMain", "UploadToDB") Then
'        Unload frmProg
'        UploadToDB = False
'        Rollback_Upload CurrentBookNo, iCycle
'        Exit Function
'    End If

    ' Close all recordsets for the next file
    rsTxt.Close
    rsDB.Close
    rsDBdl.Close
    rsDBul.Close

    frso = CurrentDnldFilePath & "HQ" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
    ' Delete file if exist
    If fso.FileExists(frso) Then fso.DeleteFile frso

    ' Move file to \FRSO folder in BC working folder
    fso.MoveFile bkfile, CurrentDnldFilePath & "HQ" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
    ' END ACCOMPLISHED READINGS FILE

    If withfc = True Then
        ' BEGIN FOUND CONNECTED FILE PROCESSING
        fso.CopyFile fcfile, App.Path & "\foundconn.txt"

        rsTxt.Open "SELECT * FROM foundconn.txt", cnTxt, adOpenStatic, adLockOptimistic, adCmdText
        ' Open T_FCONN for initialization
        rsDB.Open "SELECT * FROM T_FCONN", cnDB, adOpenDynamic, adLockOptimistic

        Do While Not rsTxt.EOF
            With rsDB
                .AddNew
                .Fields("BOOKNO") = rsTxt.Fields("MRUNumber")
                '.Fields("CYCLE") = iCycle
                .Fields("CYCLE") = cycle
                .Fields("METERNO") = rsTxt.Fields("MeterNum") & ""
                .Fields("SEQNO") = Val(rsTxt.Fields("SequenceNo") & "")
                .Fields("ACCTNUM") = rsTxt.Fields("Installation") & ""
                .Fields("ACCTNAME") = rsTxt.Fields("CustName") & ""
                .Fields("ADDRESS") = rsTxt.Fields("CustAddress") & ""
                .Fields("PRESRDG") = rsTxt.Fields("PresReading") & ""
                .Fields("RDGDATE") = rsTxt.Fields("ReadingDate") & "" 'CDate(rsTxt.Fields("ReadingDate") & "")
                .Fields("RDGTIME") = rsTxt.Fields("ReadingTime") & ""
                .Fields("MTRBRAND") = rsTxt.Fields("MtrBrand") & ""
                .Update
            End With
            rsTxt.MoveNext
        Loop

        ' Rename the FC file from SO to a specific cycle file name
        fcfile2 = sPath & "\FC" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
        'fso.CopyFile fcfile, fcfile2
        Name fcfile As fcfile2    ' Rename fc file.

        fcfrso = CurrentDnldFilePath & "FC" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
        ' Delete file if exist
        If fso.FileExists(fcfrso) Then fso.DeleteFile fcfrso

        ' Move file to \FRSO folder in BC working folder
        fso.MoveFile fcfile2, CurrentDnldFilePath & "FC" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
        ' END FOUND CONNECTED FILE PROCESSING

    rsTxt.Close
    rsDB.Close

    End If

'commit transaction
cnDB.CommitTrans
'Clean up
cnTxt.Close
cnDB.Close

Set rsTxt = Nothing
Set cnTxt = Nothing

Set rsDB = Nothing
Set rsDBdl = Nothing
Set rsDBul = Nothing
Set cnDB = Nothing

Unload frmProg
If ulcnt = dlcnt Then
    frmMRUMngt.UpdateRSOUpload CurrentBookNo, cycle
End If
UploadToDB = True

errhandler:
If Err.Number <> 0 Then
    MsgBox "Error getting " & CurrentBookNo & "." & vbCrLf & vbCrLf & Err.Description
    Unload frmProg
    If Err.Number <> -2147217887 Then
         ' rollback loaded mru with error
         cnDB.RollbackTrans
'         Rollback_Upload CurrentBookNo, cycle
    End If
End If

End Sub

Private Sub cmdSelectAll_Click()
SelectAll 1, 0
End Sub

Private Sub cmdSelectNone_Click()
SelectAll 0, 0
End Sub

Private Sub cmdSSO_Click()
Dim iCycle As Integer
Dim sMRU As String
Dim n As Integer
Dim iCount As Integer
Dim itm
Dim l_int_Index As Integer
Dim DFile As String
Dim sPath As String
Dim fs As Object
Dim a
Dim bccode As String
Dim BCDESC As String
Dim cdelim As String
Dim rsDL As Recordset
'On Error GoTo errhandler

bccode = Left(sBusCenter, 4)
BCDESC = Mid(sBusCenter, 7)
iCycle = str(intBillCycle)
cdelim = "|"

If Not OpenRecordset(g_rs_RBUSCTR, "SELECT WORKING_DIR, BC_DESC FROM R_BUSCTR WHERE BC_CODE ='" & bccode & "'", "", "") Then
    Exit Sub
End If

sPath = g_rs_RBUSCTR.Fields(0) & ""
If sPath = "" Then
    MsgBox "Business Center Directory not specified!", vbCritical, "System Message"
    Exit Sub
End If

Set fs = CreateObject("Scripting.FileSystemObject")

' create folder toso\bccode bcdesc\year\cycle monthname\sched mr day, i.e. TOSO\0100 Novaliches\2008\09 September\06
If Not fs.FolderExists(sPath) Then fs.CreateFolder (sPath) ' dl directory
If Not fs.FolderExists(sPath & "\TOSO\") Then fs.CreateFolder (sPath & "\TOSO\") ' TOSO folder
If Not fs.FolderExists(sPath & "\TOSO\" & bccode & " " & BCDESC) Then fs.CreateFolder (sPath & "\TOSO\" & bccode & " " & BCDESC) ' BC folder
If Not fs.FolderExists(sPath & "\TOSO\" & bccode & " " & BCDESC & "\" & Year(Date)) Then fs.CreateFolder (sPath & "\TOSO\" & bccode & " " & BCDESC & "\" & Year(Date)) ' year folder
If Not fs.FolderExists(sPath & "\TOSO\" & bccode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0")))) Then fs.CreateFolder (sPath & "\TOSO\" & bccode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0")))) ' cycle folder
If Not fs.FolderExists(sPath & "\TOSO\" & bccode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0"))) & "\" & Format(Date, "dd")) Then fs.CreateFolder (sPath & "\TOSO\" & bccode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0"))) & "\" & Format(Date, "dd")) ' sched mr day folder

DFile = sPath & "\TOSO\" & bccode & " " & BCDESC & "\" & Year(Date) & "\" & Padl(iCycle, 2, "0") & " " & GetMonth(CInt(Padl(iCycle, 2, "0"))) & "\" & Format(Date, "dd") & "\" & "DC" & Left(sBusCenter, 2) & Year(Date) & Format(Date, "mm") & Format(Date, "dd") & "01.txt"

Open DFile For Output As #1

Set rsDL = New ADODB.Recordset
strSQL = "SELECT A.*, B.BC_CODE, B.SCHED_RDG_DT FROM T_DOWNLOAD A INNER JOIN T_BOOK B ON A.BOOKNO=B.BOOKNO AND A.DLCYCLE=B.CYCLE WHERE A.BOOKNO = '" & booknum & "' AND DLCYCLE = " & str$(cycle) & " ORDER BY A.SEQNO"

rsDL.Open strSQL, g_Conn, adOpenStatic, adLockReadOnly
Do While Not rsDL.EOF

    '--assign values to variables
    MRU = rsDL.Fields("BOOKNO").Value & ""
    CustInstallNo = rsDL.Fields("ACCTNUM").Value & ""
    SEQNO = rsDL.Fields("SEQNO").Value & ""
    DocumentNo = rsDL.Fields("DLDOC_NO").Value & ""
    MeterNum = rsDL.Fields("SERIALNO").Value & ""
    meterbrand = rsDL.Fields("MTRBRAND").Value & ""
    CUSTNAME = Trim(rsDL.Fields("ACCTNAME").Value & "")
    CustAddress = Trim(rsDL.Fields("ADDRESS").Value & "")
    installation = Trim(rsDL.Fields("INSTALLATION").Value & "")
    prdate = Format(IIf(IsNull(rsDL.Fields("PRDGDATE").Value) = True, "        ", rsDL.Fields("PRDGDATE").Value), "yyyymmdd")
    Bill_Class = rsDL.Fields("BILL_CLASS").Value & ""
    Rate = IIf(IsNull(rsDL.Fields("RATE").Value) = True, "", rsDL.Fields("RATE").Value)
    New_Meter_Info = rsDL.Fields("NEW_METER_INFO").Value & ""
    Initial_Rdg = rsDL.Fields("INITIAL_RDG").Value & ""
    Removal_Dt = Format(IIf(IsNull(rsDL.Fields("REMOVAL_DT").Value) = True, "        ", rsDL.Fields("REMOVAL_DT").Value), "yyyymmdd")
    Final_Rdg = rsDL.Fields("FINAL_RDG").Value & ""
    Grp_Flag = rsDL.Fields("GRP_FLAG").Value & ""
    Stat_Flag = rsDL.Fields("STAT_FLAG").Value & ""
    Block_Cd = rsDL.Fields("BLOCK_CD").Value & ""
    Disc_Tag = rsDL.Fields("DISC_TAG").Value & ""
    PUA = rsDL.Fields("PUA").Value & ""
    Other_Charges = rsDL.Fields("OTHER_CHARGES").Value & ""
    Meter_Charges = rsDL.Fields("METER_CHARGES").Value & ""
    Re_Opening_Fee = rsDL.Fields("RE_OPENING_FEE").Value & ""
    GD = rsDL.Fields("GD").Value & ""
    Installment = rsDL.Fields("INSTALLMENT").Value & ""
    Install_Charge = rsDL.Fields("INSTALL_CHARGE").Value & ""
    Invoice = rsDL.Fields("INVOICE").Value & ""
    Water_Or = rsDL.Fields("WATER_OR").Value & ""
    Water_Pydt = Format(IIf(IsNull(rsDL.Fields("WATER_PYDT").Value) = True, "        ", rsDL.Fields("WATER_PYDT").Value), "yyyymmdd")
    Water_Net = rsDL.Fields("WATER_NET").Value & ""
    Water_VAT = rsDL.Fields("WATER_VAT").Value & ""
    Water_Total = rsDL.Fields("WATER_TOTAL").Value & ""
    Water_Or2 = rsDL.Fields("WATER_OR2").Value & ""
    'Water_Pydt = rsDL.Fields("WATER_PYDT") & ""
    Water_Pydt2 = Format(IIf(IsNull(rsDL.Fields("WATER_PYDT2").Value) = True, "        ", rsDL.Fields("WATER_PYDT2").Value), "yyyymmdd")
    Water_Net2 = rsDL.Fields("WATER_NET2").Value & ""
    Water_VAT2 = rsDL.Fields("WATER_VAT2").Value & ""
    Water_Total2 = rsDL.Fields("WATER_TOTAL2").Value & ""
    Misc_Or = rsDL.Fields("MISC_OR").Value & ""
    'Misc_Pydt = rsDL.Fields("MISC_PYDT") & ""
    Misc_Pydt = Format(IIf(IsNull(rsDL.Fields("MISC_PYDT").Value) = True, "        ", rsDL.Fields("MISC_PYDT").Value), "yyyymmdd")
    Misc_Net = rsDL.Fields("MISC_NET").Value & ""
    Misc_VAT = rsDL.Fields("MISC_VAT").Value & ""
    Misc_Total = rsDL.Fields("MISC_TOTAL").Value & ""
    GD_OR = rsDL.Fields("GD_OR").Value & ""
    'GD_Pydt = rsDL.Fields("GD_PYDT") & ""
    GD_Pydt = Format(IIf(IsNull(rsDL.Fields("GD_PYDT").Value) = True, "        ", rsDL.Fields("GD_PYDT").Value), "yyyymmdd")
    GD_Net = rsDL.Fields("GD_NET").Value & ""
    'bc = rsDL.Fields("BC_CODE") & ""
    Sanzper = rsDL.Fields("SANZPER").Value & ""
    TIN = rsDL.Fields("TIN").Value & ""
    '--end assign values to variables

'    '--insert data to text file
'    Print #1, MRU & CDelim & SEQNO & CDelim & DocumentNo & CDelim & MeterNum & CDelim & MeterNum & CDelim & meterbrand & CDelim & _
'    mtrrdrcode & CDelim & ffcd1 & CDelim & CUSTNAME & CDelim & CustAddress & CDelim & prevremark1 & CDelim & _
'    prevremark2 & CDelim & CustInstallNo & CDelim & "00" & CDelim & prdate & CDelim & _
'    Format$(pread1, "######0") & CDelim & Format$(prevcon1, "#####0") & _
'    CDelim & Format$(avecon1, "#####0") & CDelim & Rate & CDelim & "00" & CDelim & ExpectedRdg & _
'    CDelim & MeterType & CDelim & BillPrevRdg & CDelim & "000" & CDelim & SchedReadingDate

    '--insert data to text file
    Print #1, MRU & cdelim & CustInstallNo & cdelim & SEQNO & cdelim & PSEQNO & cdelim & DocumentNo & cdelim & MeterNum & cdelim & _
    meterbrand & cdelim & ffcd1 & cdelim & ffcd2 & cdelim & CUSTNAME & cdelim & CustAddress & cdelim & prevremark1 & cdelim & _
    installation & cdelim & prdate & cdelim & Format$(pread1, "######0") & cdelim & prevcon1 & cdelim & _
    avecon1 & cdelim & BillPrevRdg & cdelim & SchedReadingDate & cdelim & Bill_Class & cdelim & _
    Rate & cdelim & New_Meter_Info & cdelim & Initial_Rdg & cdelim & Removal_Dt & cdelim & Final_Rdg & cdelim & _
    Grp_Flag & cdelim & Stat_Flag & cdelim & Block_Cd & cdelim & Disc_Tag & cdelim & Constag & cdelim & _
    PUA & cdelim & Other_Charges & cdelim & Meter_Charges & cdelim & Re_Opening_Fee & cdelim & GD & cdelim & Installment & cdelim & _
    Install_Charge & cdelim & Invoice & cdelim & Water_Or & cdelim & Water_Pydt & cdelim & Water_Net & cdelim & _
    Water_VAT & cdelim & Water_Total & cdelim & Water_Or2 & cdelim & Water_Pydt2 & cdelim & Water_Net2 & cdelim & Water_VAT2 & cdelim & Water_Total2 & cdelim & _
    Misc_Or & cdelim & Misc_Pydt & cdelim & Misc_Net & cdelim & Misc_VAT & cdelim & Misc_Total & cdelim & GD_OR & cdelim & GD_Pydt & cdelim & GD_Net & cdelim & Sanzper & cdelim & TIN

    'g_rs_TDOWNLOAD.MoveNext
    rsDL.MoveNext
Loop


'
'    ' Reset MRUList
''    MRUList = ""
'    iChecked = 0
'
'    ' Count the number of Checked items in the ListView and build the MRUList from the text items
'    iCount = lsvMRUMngt.ListItems.Count
'
'    ' If no MRUs were selected, exit function with True, you will be using an "Empty" MRUList!
'    If iCount = 0 Then
'        Generate_Acct_List = False
'        Exit Function
'    End If
'
'    For Each itm In lsvMRUMngt.ListItems
'       With itm
''         If .checked Then
''            iChecked = iChecked + 1
'            l_int_Index = .Index
'            With lsvMRUMngt.ListItems(l_int_Index)
'
'
'                '--insert data to text file
'                'Print #1, .SubItems(2) & cdelim & .Text & cdelim & .SubItems(8) & cdelim & .SubItems(4) & cdelim & .SubItems(9) & cdelim & .SubItems(10) & cdelim & .SubItems(11) & cdelim & .SubItems(5) & cdelim & .SubItems(6) & cdelim & .SubItems(7)
'
'                Print #1, .SubItems(2) & cdelim & .Text & cdelim & .SubItems(8) & cdelim & .SubItems(4) & cdelim & .SubItems(9) & cdelim & .SubItems(10) & cdelim & .SubItems(11) & cdelim & .SubItems(5) & cdelim & .SubItems(6) & cdelim & .SubItems(7) & cdelim & IIf(.SubItems(12) = "?", "1", IIf(.SubItems(12) = "X", "2", "")) & cdelim & IIf(.SubItems(12) = "X", .SubItems(13), "")
'                End With
'
''            ' Add the MRU to the list; if first MRU, no comma in front
''            If MRUList <> "" Then
''                MRUList = MRUList & ",'" & .Text & "'"
''            Else
''                MRUList = MRUList & "'" & .Text & "'"
''            End If
''            MRUArray(iChecked) = itm
''         End If
'       End With
'    Next

Close #1

rsDL.Close
Set rsDL = Nothing

MsgBox "Download File Creation Successful!", vbInformation, ""

If Not DBExecute("UPDATE T_DC_BOOK SET UPLD_SO_DT=GETDATE() WHERE CYCLE=" & iCycle & " AND BC_CODE ='" & bccode & "'", "", "") Then
End If

errhandler:
If Err.Number <> 0 Then
    MsgBox Err.Description, , "Error"
End If
'    SRSO = "SSO"
'    If Generate_Acct_List = False Then
'        Exit Sub
'    End If

'    'sMRU = lsvMRUMngt.SelectedItem.Text
'    'iCycle = Val(Right(g_BillMonth, 2))
'    iCycle = str(intBillCycle)
'
'    n = 1
'    For n = 1 To iChecked
'         sMRU = MRUArray(n)
'         'If CreateDLFile(sMRU, iCycle, "|") Then
'         If CreateDLFile(sMRU, iCycle, intDayNo, "|") Then
'             UpdateSSODownload sMRU, iCycle
''             MsgBox "Download File Creation Successful!", vbInformation, "Send File to Satellite Office"
''             ListView_Load
'         Else
'             MsgBox "Download File Creation for MRU " & sMRU & " Failed or Aborted!", vbCritical, "Send File to Satellite Office"
'             Exit Sub
'         End If
'    Next n
'
'    ListView_Load
'    MsgBox "Download File Creation Successful!", vbInformation, "Send File to Satellite Office"

End Sub

Private Sub Form_Activate()
'frmMRUMngt.WindowState = 2
'    ListView_Load
End Sub

Private Sub Form_Load()

If WindowState = vbNormal Then
    Me.Move (0)
    Me.Top = 0
    Me.Height = 10230
    Me.Width = 15270
End If

    lblBusCtr.Caption = "Business Center : " & sBusCenter
    'lblCycle.Caption = "Cycle/Month : " & Right(g_BillMonth, 2)
    lblCycle.Caption = "Cycle/Month : " & str(intBillCycle)
    lblDayNo.Caption = "Day Number : " & str(intDayNo)

    ListView_Load

End Sub

Private Sub ListView_Load()

    Dim l_str_Sql As String
    Dim lBusCenter As String
    'Dim intCycle As Integer
    Dim intNumMRU As Integer

    ' Pass the variables from the MRU Scheduler Form
    lBusCenter = Left$(sBusCenter, 4)
    'intCycle = Right(g_BillMonth, 2)

'    l_str_Sql = "SELECT T_BOOK.BOOKNO, R_BOOKSTAT.BKSTDESC, T_BOOK.SCHED_RDG_DT, T_BOOK.ACTUAL_DL_DT, T_BOOK.UPLD_SO_DT, T_BOOK.RECV_SO_DT, T_BOOK.UPLD_MWSI_DT " & _
'                "FROM R_BOOKSTAT INNER JOIN (T_BOOK INNER JOIN T_SCHED ON (T_BOOK.CYCLE = T_SCHED.CYCLE) AND (T_BOOK.BOOKNO = T_SCHED.BOOKNO)) ON R_BOOKSTAT.BKSTCODE = T_SCHED.BKSTCODE " & _
'                "WHERE T_BOOK.BC_CODE = '" & lBusCenter & "' AND T_BOOK.CYCLE = " & intBillCycle & " AND T_SCHED.DAYNO = " & intDayNo

    l_str_Sql = "SELECT A.ACCTNUM, CASE WHEN PUA > 0 THEN 'DEL' ELSE 'REQ' END AS DISCREASON, " & _
                "A.BOOKNO , A.DCJOBID, A.SERIALNO, PUA, CURRENT_AMT, DUE_AMT, SEQ, CUSTNAME, ADDRESS, RECONFEE, C.DC_STATUS, RDG " & _
                "FROM T_DC_DOWNLOAD A INNER JOIN T_DC_BOOK B ON A.DCJOBID=B.DCJOBID AND A.CYCLE=B.CYCLE AND A.BC_CODE=B.BC_CODE AND A.DAYNO=B.DAYNO " & _
                "LEFT JOIN T_CUST_MASTER C ON A.ACCTNUM=C.ACCTNUM AND A.CYCLE=C.LBCYCLE " & _
                "WHERE B.BC_CODE = '" & lBusCenter & "' AND B.CYCLE = " & intBillCycle & " AND B.DAYNO = " & intDayNo

    lsvMRUMngt.ListItems.Clear
    If Not OpenRecordset(g_rs_RFF, l_str_Sql, Me.Name, "ListView_Load") Then Exit Sub

    g_rs_RFF.MoveFirst
    While Not g_rs_RFF.EOF
       ListViewSet 14
       g_rs_RFF.MoveNext
    Wend

    g_rs_RFF.Close
    Set g_rs_RFF = Nothing

    intNumMRU = lsvMRUMngt.ListItems.Count

    lblNumMRU.Caption = "Number of MRUs : " & str(intNumMRU)

End Sub

Private Sub ListViewSet(intCols As Integer, Optional intColDate As Integer, Optional intColTime As Integer)
    Dim lobjItem As Object
    Dim l_int_Index As Integer

    Set lobjItem = lsvMRUMngt.ListItems.Add(, , CheckNull(g_rs_RFF.Fields(0)))

    For l_int_Index = 1 To intCols - 1
        Select Case l_int_Index
            Case intColDate
                lobjItem.SubItems(l_int_Index) = Format(CheckNull(g_rs_RFF.Fields(l_int_Index)), "mm/dd/yyyy")
            Case intColTime
                lobjItem.SubItems(l_int_Index) = Format(CheckNull(g_rs_RFF.Fields(l_int_Index)), "hh:mm:ss")
            Case Else
                lobjItem.SubItems(l_int_Index) = CheckNull(g_rs_RFF.Fields(l_int_Index))
        End Select
    Next

    Set lobjItem = Nothing

End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmDiscMRUGrid.Enabled = True
End Sub

Private Sub lsvMRUMngt_Click()
If lsvMRUMngt.ListItems.Count > 0 Then
    If lsvMRUMngt.SelectedItem.SubItems(1) = "Downloaded from MWSI" Then
        cmdSSO.Enabled = True
        'cmdRSO.Enabled = False
    ElseIf lsvMRUMngt.SelectedItem.SubItems(1) = "Sent to Satellite" Then
        'cmdSSO.Enabled = False
        cmdRSO.Enabled = True
    Else
        'cmdSSO.Enabled = False
        'cmdRSO.Enabled = False
    End If

'    If lsvMRUMngt.SelectedItem.SubItems(2) = "Sent to Satellite" Then
'        cmdSSO.Enabled = False
'        cmdRSO.Enabled = True
'    ElseIf lsvMRUMngt.SelectedItem.SubItems(2) = "Received from Satellite" Then
'        cmdSSO.Enabled = True
'        cmdRSO.Enabled = False
'    Else
'        cmdSSO.Enabled = False
'        cmdRSO.Enabled = False
'    End If

End If
End Sub

Private Sub lsvMRUMngt_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ListviewSort lsvMRUMngt, ColumnHeader.Index
End Sub

Private Function Generate_Acct_List() As Boolean
Dim iCount As Integer
'Dim iChecked As Integer
Dim itm
'Dim MRUArray(1000) As String
Dim n As Integer
Dim iCycle As Integer
Dim l_int_Index As Integer

    ' Reset MRUList
    MRUList = ""
    iChecked = 0

    'iCycle = Val(Right(g_BillMonth, 2))
    iCycle = str(intBillCycle)
    ' Count the number of Checked items in the ListView and build the MRUList from the text items
    iCount = lsvMRUMngt.ListItems.Count

    ' If no MRUs were selected, exit function with True, you will be using an "Empty" MRUList!
    If iCount = 0 Then
        Generate_Acct_List = False
        Exit Function
    End If

    For Each itm In lsvMRUMngt.ListItems
       With itm
         If .checked Then
            iChecked = iChecked + 1
            l_int_Index = .Index
            With lsvMRUMngt.ListItems(l_int_Index)
                MsgBox .Text
                MsgBox .SubItems(1)
                MsgBox .SubItems(3)
                MsgBox .SubItems(5)
            End With

'            ' Add the MRU to the list; if first MRU, no comma in front
'            If MRUList <> "" Then
'                MRUList = MRUList & ",'" & .Text & "'"
'            Else
'                MRUList = MRUList & "'" & .Text & "'"
'            End If
'            MRUArray(iChecked) = itm
         End If
       End With
    Next

    If iChecked = 0 Then
        MsgBox "No MRU selected!", vbCritical, "Manage MRUs"
        Generate_Acct_List = False
        Exit Function
    End If

    If SRSO = "SSO" Then
        n = 1
        For n = 1 To iChecked
            ' check if mru stat is RSO
            If Not OpenRecordset(g_rs_TSCHED, "SELECT BOOKNO FROM T_SCHED WHERE BOOKNO = '" & MRUArray(n) & "' and BKSTCODE= 'DLM' and CYCLE= " & iCycle & "", Me.Name, "Generate_MRU_List") Then
                MsgBox "Please uncheck MRU " & MRUArray(n) & ". Can only Send to SO books with status 'Downloaded from MWSI'!", vbCritical, "Manage MRUs"
                'g_rs_TSCHED.Close
                'Set g_rs_TSCHED = Nothing
                Generate_Acct_List = False
                Exit Function
            End If
        Next n
    ElseIf SRSO = "RSO" Then
        n = 1
        For n = 1 To iChecked
            ' check if mru stat is RSO
            If Not OpenRecordset(g_rs_TSCHED, "SELECT BOOKNO FROM T_SCHED WHERE BOOKNO = '" & MRUArray(n) & "' and BKSTCODE= 'SSO' and CYCLE= " & iCycle & "", Me.Name, "Generate_MRU_List") Then
                MsgBox "Please uncheck MRU " & MRUArray(n) & ". Can only Receive from SO books with status 'Sent to Satellite Office'!", vbCritical, "Manage MRUs"
                'g_rs_TSCHED.Close
                'Set g_rs_TSCHED = Nothing
                Generate_Acct_List = False
                Exit Function
            End If
        Next n
    End If

    ' If ALL MRUs were selected, then do NOT use the list
'    If iCount = iChecked Then
'       Generate_MRU_List = False
'    Else
        Generate_Acct_List = True
'    End If

End Function

Private Function SelectAll(ByVal DeOrSelectAll As Integer, ByVal Listbx As Integer) As Boolean ' Added the action that is needed
  Dim Item As ListItem

If Listbx = 0 Then
  SelectAll = True ' Default succes
  Select Case DeOrSelectAll
    Case 0 ' Unchecked Check1
      For Each Item In lsvMRUMngt.ListItems
        Item.checked = False ' Deselect
      Next
    Case 1 ' Checked Check1
      For Each Item In lsvMRUMngt.ListItems
        Item.checked = True ' Select
      Next
    Case Else ' 2 = Grayed or else so no action for now....
      SelectAll = False ' No succes reported back...
  End Select
End If

End Function

