VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.ocx"
Begin VB.Form frmSettings 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Settings"
   ClientHeight    =   6240
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4815
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6240
   ScaleWidth      =   4815
   Begin VB.Frame Frame1 
      Caption         =   "Directories"
      Height          =   5535
      Left            =   120
      TabIndex        =   36
      Top             =   120
      Width           =   4575
      Begin VB.TextBox txtReopeningFee 
         Height          =   315
         Left            =   240
         TabIndex        =   52
         Top             =   5040
         Width           =   3735
      End
      Begin VB.TextBox txtrul 
         Height          =   315
         Left            =   240
         TabIndex        =   51
         Top             =   4200
         Width           =   3735
      End
      Begin VB.TextBox txtrdl 
         Height          =   315
         Left            =   240
         TabIndex        =   49
         Top             =   3480
         Width           =   3735
      End
      Begin VB.TextBox txtudl 
         Height          =   285
         Left            =   240
         TabIndex        =   47
         Top             =   2760
         Width           =   3735
      End
      Begin VB.TextBox txtddl 
         Height          =   285
         Left            =   240
         TabIndex        =   46
         Top             =   2040
         Width           =   3735
      End
      Begin VB.CommandButton cmdDSource 
         Caption         =   "..."
         Height          =   285
         Left            =   4080
         TabIndex        =   40
         Top             =   585
         Width           =   385
      End
      Begin VB.TextBox txtDSource 
         Height          =   285
         Left            =   240
         TabIndex        =   39
         Top             =   600
         Width           =   3735
      End
      Begin VB.TextBox txtUDest 
         Height          =   285
         Left            =   240
         TabIndex        =   38
         Top             =   1320
         Width           =   3735
      End
      Begin VB.CommandButton cmdUDest 
         Caption         =   "..."
         Height          =   285
         Left            =   4080
         TabIndex        =   37
         Top             =   1320
         Width           =   385
      End
      Begin VB.Label Label7 
         Caption         =   "Re-Opening Fee:"
         Height          =   255
         Left            =   120
         TabIndex        =   53
         Top             =   4680
         Width           =   1695
      End
      Begin VB.Label Label6 
         Caption         =   "Recon UL File Source:"
         Height          =   255
         Left            =   120
         TabIndex        =   50
         Top             =   3960
         Width           =   1695
      End
      Begin VB.Label Label3 
         Caption         =   "Recon DL File Source:"
         Height          =   255
         Left            =   120
         TabIndex        =   48
         Top             =   3240
         Width           =   1695
      End
      Begin VB.Label Label2 
         Caption         =   "Discon UL File Source:"
         Height          =   255
         Left            =   120
         TabIndex        =   45
         Top             =   2520
         Width           =   2415
      End
      Begin VB.Label Label1 
         Caption         =   "Discon DL File Source:"
         Height          =   255
         Left            =   120
         TabIndex        =   44
         Top             =   1800
         Width           =   1935
      End
      Begin VB.Label Label5 
         Caption         =   "Download File Source : "
         Height          =   285
         Index           =   0
         Left            =   120
         TabIndex        =   42
         Top             =   360
         Width           =   2475
      End
      Begin VB.Label Label4 
         Caption         =   "Upload File Destination : "
         Height          =   285
         Left            =   120
         TabIndex        =   41
         Top             =   1080
         Width           =   2865
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   390
      Left            =   2460
      TabIndex        =   20
      Top             =   5760
      Width           =   915
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save"
      Default         =   -1  'True
      Height          =   390
      Left            =   1440
      TabIndex        =   19
      Top             =   5760
      Width           =   915
   End
   Begin TabDlg.SSTab stabSettings 
      Height          =   2295
      Left            =   0
      TabIndex        =   21
      Top             =   6240
      Width           =   5430
      _ExtentX        =   9578
      _ExtentY        =   4048
      _Version        =   393216
      Style           =   1
      Tabs            =   5
      TabsPerRow      =   5
      TabHeight       =   529
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "System Mode"
      TabPicture(0)   =   "frmSettings.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblStand"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblDisplay"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "fraSettings(1)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "fraSettings(0)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "Download"
      TabPicture(1)   =   "frmSettings.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "lblDnldTables"
      Tab(1).Control(1)=   "lblDnldPrompt"
      Tab(1).Control(2)=   "lblDnldProg"
      Tab(1).Control(3)=   "fraDownload(3)"
      Tab(1).Control(4)=   "fraDownload(1)"
      Tab(1).Control(5)=   "fraDownload(0)"
      Tab(1).ControlCount=   6
      TabCaption(2)   =   "Print Options"
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "optPrint(0)"
      Tab(2).Control(1)=   "optPrint(1)"
      Tab(2).Control(2)=   "optPrint(2)"
      Tab(2).ControlCount=   3
      TabCaption(3)   =   "Date Covered"
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "lblDateTo"
      Tab(3).Control(1)=   "lblDateFrom"
      Tab(3).Control(2)=   "txtDateTo"
      Tab(3).Control(3)=   "txtDateFrom"
      Tab(3).ControlCount=   4
      TabCaption(4)   =   "Directories"
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "lblDest"
      Tab(4).Control(1)=   "lblSource"
      Tab(4).Control(2)=   "cmdDest"
      Tab(4).Control(3)=   "cmdSource"
      Tab(4).Control(4)=   "txtDest"
      Tab(4).Control(5)=   "txtSource"
      Tab(4).ControlCount=   6
      Begin VB.TextBox txtSource 
         Height          =   285
         Left            =   -74520
         MaxLength       =   255
         TabIndex        =   15
         Top             =   820
         Width           =   3500
      End
      Begin VB.TextBox txtDest 
         Height          =   285
         Left            =   -74520
         MaxLength       =   255
         TabIndex        =   17
         Top             =   1600
         Width           =   3500
      End
      Begin VB.CommandButton cmdSource 
         Caption         =   "..."
         Height          =   285
         Left            =   -70920
         TabIndex        =   16
         Top             =   840
         Width           =   385
      End
      Begin VB.CommandButton cmdDest 
         Caption         =   "..."
         Height          =   285
         Left            =   -70920
         TabIndex        =   18
         Top             =   1600
         Width           =   385
      End
      Begin VB.TextBox txtDateFrom 
         Height          =   285
         Left            =   -73880
         MaxLength       =   10
         TabIndex        =   13
         Top             =   600
         Width           =   1200
      End
      Begin VB.TextBox txtDateTo 
         Height          =   285
         Left            =   -73890
         MaxLength       =   10
         TabIndex        =   14
         Top             =   1020
         Width           =   1200
      End
      Begin VB.OptionButton optPrint 
         Caption         =   " Print to File"
         Height          =   285
         Index           =   2
         Left            =   -74760
         TabIndex        =   12
         Top             =   1400
         Width           =   2000
      End
      Begin VB.OptionButton optPrint 
         Caption         =   " Print to Screen"
         Height          =   285
         Index           =   1
         Left            =   -74760
         TabIndex        =   11
         Top             =   1000
         Width           =   2000
      End
      Begin VB.OptionButton optPrint 
         Caption         =   " Print to Printer"
         Height          =   285
         Index           =   0
         Left            =   -74760
         TabIndex        =   10
         Top             =   600
         Value           =   -1  'True
         Width           =   2000
      End
      Begin VB.Frame fraSettings 
         BorderStyle     =   0  'None
         Caption         =   "Frame2"
         Height          =   315
         Index           =   0
         Left            =   2400
         TabIndex        =   29
         Top             =   600
         Width           =   2000
         Begin VB.OptionButton optDisplay 
            Caption         =   "Yes"
            Height          =   285
            Index           =   0
            Left            =   30
            TabIndex        =   0
            Top             =   10
            Width           =   800
         End
         Begin VB.OptionButton optDisplay 
            Caption         =   "No"
            Height          =   285
            Index           =   1
            Left            =   1000
            TabIndex        =   1
            Top             =   10
            Value           =   -1  'True
            Width           =   800
         End
      End
      Begin VB.Frame fraSettings 
         BorderStyle     =   0  'None
         Height          =   315
         Index           =   1
         Left            =   2400
         TabIndex        =   28
         Top             =   1000
         Width           =   2000
         Begin VB.OptionButton optStand 
            Caption         =   "No"
            Height          =   285
            Index           =   1
            Left            =   1000
            TabIndex        =   3
            Top             =   10
            Value           =   -1  'True
            Width           =   800
         End
         Begin VB.OptionButton optStand 
            Caption         =   "Yes"
            Height          =   285
            Index           =   0
            Left            =   30
            TabIndex        =   2
            Top             =   10
            Width           =   800
         End
      End
      Begin VB.Frame fraDownload 
         BorderStyle     =   0  'None
         Height          =   315
         Index           =   0
         Left            =   -72600
         TabIndex        =   24
         Top             =   600
         Width           =   2000
         Begin VB.OptionButton optDnldProg 
            Caption         =   " Off"
            Height          =   285
            Index           =   1
            Left            =   1000
            TabIndex        =   5
            Top             =   10
            Width           =   800
         End
         Begin VB.OptionButton optDnldProg 
            Caption         =   " On"
            Height          =   285
            Index           =   0
            Left            =   30
            TabIndex        =   4
            Top             =   10
            Value           =   -1  'True
            Width           =   800
         End
      End
      Begin VB.Frame fraDownload 
         BorderStyle     =   0  'None
         Height          =   315
         Index           =   1
         Left            =   -72600
         TabIndex        =   23
         Top             =   1000
         Width           =   2000
         Begin VB.OptionButton optDnldTables 
            Caption         =   " Off"
            Height          =   285
            Index           =   1
            Left            =   1000
            TabIndex        =   7
            Top             =   10
            Width           =   800
         End
         Begin VB.OptionButton optDnldTables 
            Caption         =   " On"
            Height          =   285
            Index           =   0
            Left            =   30
            TabIndex        =   6
            Top             =   10
            Value           =   -1  'True
            Width           =   800
         End
      End
      Begin VB.Frame fraDownload 
         BorderStyle     =   0  'None
         Height          =   315
         Index           =   3
         Left            =   -72600
         TabIndex        =   22
         Top             =   1400
         Width           =   2000
         Begin VB.OptionButton optDnldPrompt 
            Caption         =   " Off"
            Height          =   285
            Index           =   1
            Left            =   1020
            TabIndex        =   9
            Top             =   10
            Width           =   800
         End
         Begin VB.OptionButton optDnldPrompt 
            Caption         =   " On"
            Height          =   285
            Index           =   0
            Left            =   30
            TabIndex        =   8
            Top             =   10
            Value           =   -1  'True
            Width           =   800
         End
      End
      Begin VB.Label lblSource 
         Caption         =   "Download File Source : "
         Height          =   285
         Left            =   -74760
         TabIndex        =   35
         Top             =   480
         Width           =   2475
      End
      Begin VB.Label lblDest 
         Caption         =   "Upload File Destination : "
         Height          =   285
         Left            =   -74760
         TabIndex        =   34
         Top             =   1260
         Width           =   2865
      End
      Begin VB.Label lblDateFrom 
         Caption         =   "Date From : "
         Height          =   285
         Left            =   -74760
         TabIndex        =   33
         Top             =   620
         Width           =   860
      End
      Begin VB.Label lblDateTo 
         Alignment       =   1  'Right Justify
         Caption         =   "To : "
         Height          =   285
         Left            =   -74760
         TabIndex        =   32
         Top             =   1020
         Width           =   860
      End
      Begin VB.Label lblDisplay 
         Caption         =   "Display Previous Reading?"
         Height          =   315
         Left            =   240
         TabIndex        =   31
         Top             =   600
         Width           =   2000
      End
      Begin VB.Label lblStand 
         Caption         =   "Stand-alone?"
         Height          =   315
         Left            =   240
         TabIndex        =   30
         Top             =   1000
         Width           =   2000
      End
      Begin VB.Label lblDnldProg 
         Caption         =   "Auto Download Program : "
         Height          =   315
         Left            =   -74760
         TabIndex        =   27
         Top             =   600
         Width           =   2000
      End
      Begin VB.Label lblDnldPrompt 
         Caption         =   "Prompt Before Download : "
         Height          =   315
         Left            =   -74760
         TabIndex        =   26
         Top             =   1400
         Width           =   2000
      End
      Begin VB.Label lblDnldTables 
         Caption         =   "Auto Download Tables : "
         Height          =   315
         Left            =   -74760
         TabIndex        =   25
         Top             =   1000
         Width           =   2000
      End
   End
   Begin VB.Label Label5 
      Caption         =   "Download File Source : "
      Height          =   285
      Index           =   1
      Left            =   240
      TabIndex        =   43
      Top             =   1920
      Width           =   2475
   End
End
Attribute VB_Name = "frmSettings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim m_bol_SParam As Boolean

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdDest_Click()
    frmFileSource.Caption = "Upload File Destination"
    Me.Enabled = False
    frmFileSource.Show

End Sub

Private Sub cmdDSource_Click()
    frmFileSource.Caption = "Download File Source"
    Me.Enabled = False
    frmFileSource.Show
End Sub

Private Sub cmdSave_Click()
    Dim l_str_Sql As String
    
    If m_bol_SParam = False Then
        'l_str_Sql = "INSERT INTO S_PARAM(UTILNAME,DIRDNLD, DIRUPLD) VALUES('SOLUZIONA','" & Trim(txtDSource) & "', '" & Trim(txtUDest) & "')"
        l_str_Sql = "INSERT INTO S_PARAM(COMPANY_NAME,DIRDNLD, DIRUPLD,DIRDLDISCON, DIRULDISCON,DIRDLRECON, DIRULRECON, REOPENINGFEE) VALUES('SUBIC WATER','" & Trim(txtDSource) & "', '" & Trim(txtUDest) & "','" & Trim(txtddl) & "', '" & Trim(txtudl) & "','" & Trim(txtrdl) & "', '" & Trim(txtrul) & "','" & Trim(txtReopeningFee) & "')"
    Else
        'l_str_Sql = "UPDATE S_PARAM SET DIRDNLD = '" & Trim(txtDSource) & "', DIRUPLD = '" & Trim(txtUDest) & "' where UTILNAME='SOLUZIONA'"
        l_str_Sql = "UPDATE S_PARAM SET DIRDNLD = '" & Trim(txtDSource) & "', DIRUPLD = '" & Trim(txtUDest) & "',DIRDLDISCON = '" & Trim(txtddl) & "', DIRULDISCON = '" & Trim(txtudl) & "',DIRDLRECON = '" & Trim(txtrdl) & "', DIRULRECON = '" & Trim(txtrul) & "', REOPENINGFEE ='" & Trim(txtReopeningFee) & "' where COMPANY_NAME='SUBIC WATER'"
    End If
        
    If DBExecute(l_str_Sql, Me.Name, "Save_Add") Then Unload Me
    MsgBox "Saved successfully.", , "System Message"
    
errhandler:
If Err.Number <> 0 Then
    MsgBox Err.Description, , "Error"
End If

End Sub

Function CheckDate() As Boolean
    CheckDate = False
    
    If Not IsDate(txtDateFrom) Then
        stabSettings.Tab = 3
        MsgBox "Date From entry is not a valid date.  Please re-enter a valid date.", vbExclamation, "Invalid Entry"
        txtDateFrom.SetFocus
        Exit Function
    End If

    If Not IsDate(txtDateTo) Then
        stabSettings.Tab = 3
        MsgBox "Date To entry is not a valid date.  Please re-enter a valid date.", vbExclamation, "Invalid Entry"
        txtDateTo.SetFocus
        Exit Function
    End If

    CheckDate = True

End Function

Function PrintOption() As String
    If optPrint(2).Value = True Then
        PrintOption = "F"
    ElseIf optPrint(1).Value = True Then
        PrintOption = "S"
    Else
        PrintOption = "P"
    End If
End Function

Private Sub cmdSource_Click()
    frmFileSource.Caption = "Download File Source"
    Me.Enabled = False
    frmFileSource.Show
End Sub

Private Sub cmdUDest_Click()
    frmFileSource.Caption = "Upload File Destination"
    Me.Enabled = False
    frmFileSource.Show

End Sub

Private Sub Form_Load()

If WindowState = vbNormal Then
    Me.Move (1000)
    Me.Top = 500
End If
    m_bol_SParam = OpenRecordset(g_rs_SPARAM, "SELECT * FROM S_PARAM", Me.Name, "cmdSave_Click")
    If m_bol_SParam = False Then Exit Sub
    g_rs_SPARAM.MoveFirst

    txtDSource = CheckNull(g_rs_SPARAM.Fields("DIRDNLD"))
    txtUDest = CheckNull(g_rs_SPARAM.Fields("DIRUPLD"))
    txtddl = CheckNull(g_rs_SPARAM.Fields("DIRDLDISCON"))
    txtudl = CheckNull(g_rs_SPARAM.Fields("DIRULDISCON"))
    txtrdl = CheckNull(g_rs_SPARAM.Fields("DIRDLRECON"))
    txtrul = CheckNull(g_rs_SPARAM.Fields("DIRULRECON"))
    txtReopeningFee = CheckNull(g_rs_SPARAM.Fields("REOPENINGFEE"))
    g_rs_SPARAM.Close
    Set g_rs_SPARAM = Nothing
    
End Sub

Private Sub SetOption(ByVal bolField As Boolean, ByRef opt1 As OptionButton, ByRef opt2 As OptionButton)
    If bolField = True Then
        opt1.Value = True
        opt2.Value = False
    Else
        opt1.Value = False
        opt2.Value = True
    End If

End Sub

Private Sub txtDateFrom_GotFocus()
    TextHighlight txtDateFrom
End Sub

Private Sub txtDateTo_GotFocus()
    TextHighlight txtDateTo
End Sub

Private Sub txtUDest_GotFocus()
    TextHighlight txtUDest
End Sub

Private Sub txtDSource_GotFocus()
    TextHighlight txtDSource
End Sub
