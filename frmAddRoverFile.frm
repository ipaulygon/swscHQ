VERSION 5.00
Begin VB.Form frmAddRoverFile 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Add Rover Record"
   ClientHeight    =   2130
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3810
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2130
   ScaleWidth      =   3810
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   390
      Left            =   2010
      TabIndex        =   4
      Top             =   1455
      Width           =   915
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Save"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   390
      Left            =   960
      TabIndex        =   3
      Top             =   1440
      Width           =   915
   End
   Begin VB.TextBox txtDateAcquired 
      Height          =   285
      Left            =   1500
      MaxLength       =   10
      TabIndex        =   2
      Top             =   880
      Width           =   2000
   End
   Begin VB.TextBox txtRoverId 
      Height          =   285
      Left            =   1500
      MaxLength       =   20
      TabIndex        =   1
      Top             =   500
      Width           =   2000
   End
   Begin VB.TextBox txtRoverNo 
      Height          =   285
      Left            =   1500
      MaxLength       =   6
      TabIndex        =   0
      Top             =   120
      Width           =   2000
   End
   Begin VB.Label lblDateAcquired 
      Alignment       =   1  'Right Justify
      Caption         =   "Date Acquired : "
      Height          =   285
      Left            =   120
      TabIndex        =   7
      Top             =   890
      Width           =   1300
   End
   Begin VB.Label lblRoverId 
      Alignment       =   1  'Right Justify
      Caption         =   "Rover ID : "
      Height          =   285
      Left            =   120
      TabIndex        =   6
      Top             =   510
      Width           =   1300
   End
   Begin VB.Label lblRoverNo 
      Alignment       =   1  'Right Justify
      Caption         =   "Rover No. : "
      Height          =   285
      Left            =   120
      TabIndex        =   5
      Top             =   130
      Width           =   1300
   End
End
Attribute VB_Name = "frmAddRoverFile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOk_Click()
    Dim l_obj_Item As Object
    Dim l_int_Index As Integer
    
    If Not ValidEntries Then Exit Sub
    
    If DuplicateRecords Then Exit Sub
    
    If g_str_AddEdit = "ADD" Then
        If Not Save_Add Then Exit Sub
        
        Set l_obj_Item = frmEditBooks.lsvFileMain.ListItems.Add(, , Trim(txtRoverNo))
            l_obj_Item.SubItems(1) = Trim(txtRoverId)
        If g_str_Table = "F_HH" Then _
            l_obj_Item.SubItems(2) = Format(Trim(txtDateAcquired), "mm/dd/yyyy")
        If g_str_Table = "R_BUSCTR" Then _
            l_obj_Item.SubItems(2) = Trim(txtDateAcquired)
        Set l_obj_Item = Nothing
        
        frmEditBooks.cmdEdit.Enabled = True
        frmEditBooks.cmdDelete.Enabled = True
    Else
        If Not Save_Edit Then Exit Sub
        
        l_int_Index = frmEditBooks.lsvFileMain.SelectedItem.Index
        frmEditBooks.lsvFileMain.ListItems(l_int_Index).Text = Trim(txtRoverNo)
        frmEditBooks.lsvFileMain.ListItems(l_int_Index).SubItems(1) = Trim(txtRoverId)
        If g_str_Table = "F_HH" Then _
            frmEditBooks.lsvFileMain.ListItems(l_int_Index).SubItems(2) = Format(Trim(txtDateAcquired), "mm/dd/yyyy")
        If g_str_Table = "R_BUSCTR" Then _
            frmEditBooks.lsvFileMain.ListItems(l_int_Index).SubItems(2) = Left(Trim(txtDateAcquired), 100)
    End If

    frmEditBooks.lsvFileMain.Refresh
    Unload Me

End Sub

Function ValidEntries() As Boolean
    ValidEntries = False
    
    If g_str_Table = "F_HH" And Not IsDate(Trim(txtDateAcquired)) Then
        MsgBox "Date Acquired entry is not a valid date.  Please re-enter a valid date.", vbExclamation, "Invalid Entry"
        txtDateAcquired.SetFocus
        Exit Function
    End If
    
    ValidEntries = True
    
End Function

Function DuplicateRecords() As Boolean
    Dim l_str_Sql As String
    
    DuplicateRecords = True
    
    If g_str_AddEdit = "EDIT" Then
        If g_str_Table = "F_HH" Then
            If Trim(txtRoverNo) = frmEditBooks.lsvFileMain.SelectedItem.Text And _
                    Trim(txtRoverId) = frmEditBooks.lsvFileMain.SelectedItem.SubItems(1) And _
                    Trim(txtDateAcquired) = frmEditBooks.lsvFileMain.SelectedItem.SubItems(2) Then
                Unload Me
                Exit Function
            End If
        ElseIf g_str_Table = "R_BUSCTR" Then
            If Trim(txtRoverNo) = frmEditBooks.lsvFileMain.SelectedItem.Text And _
                    Trim(txtRoverId) = frmEditBooks.lsvFileMain.SelectedItem.SubItems(1) And _
                    Trim(txtDateAcquired) = frmEditBooks.lsvFileMain.SelectedItem.SubItems(2) Then
                Unload Me
                Exit Function
            End If
        Else
            If Trim(txtRoverNo) = frmEditBooks.lsvFileMain.SelectedItem.Text And _
                    Trim(txtRoverId) = frmEditBooks.lsvFileMain.SelectedItem.SubItems(1) Then
                Unload Me
                Exit Function
            End If
        End If
         
        If Trim(txtRoverNo) = frmEditBooks.lsvFileMain.SelectedItem.Text Then
            DuplicateRecords = False
            Exit Function
        End If
    End If
    
    Select Case g_str_Table
        Case "R_FF"
            l_str_Sql = "SELECT FFCODE FROM R_FF WHERE FFCODE = '" & Trim(txtRoverNo) & "'"
        Case "R_RANGE"
            l_str_Sql = "SELECT RANGECODE FROM R_RANGE WHERE RANGECODE = '" & Trim(txtRoverNo) & "'"
        Case "R_READER"
            l_str_Sql = "SELECT READERNO FROM R_READER WHERE READERNO = '" & Trim(txtRoverNo) & "'"
        Case "F_HH"
            l_str_Sql = "SELECT HHTNO FROM F_HH WHERE HHTNO = '" & Trim(txtRoverNo) & "'"
        Case "R_BUSCTR"
            l_str_Sql = "SELECT BC_CODE FROM R_BUSCTR WHERE BC_CODE = '" & Trim(txtRoverNo) & "'"
    End Select

    If OpenRecordset(g_rs_RHH, l_str_Sql, Me.Name, "DuplicateRecords") Then
        MsgBox "Record already exists!", vbExclamation, "Duplicate Record"
        g_rs_RHH.Close
        Set g_rs_RHH = Nothing
        txtRoverNo.SetFocus
        Exit Function
    End If

    DuplicateRecords = False
    
End Function

Function Save_Add() As Boolean
    Dim l_str_Sql As String
    
    Select Case g_str_Table
        Case "L_FFCODE"
            l_str_Sql = "INSERT INTO L_FFCODE(FFCODE_ID, FF_DESC)VALUES( '" & _
                Left(Trim(txtRoverNo), 3) & "', '" & Left(Trim(txtRoverId), 20) & "')"
        
        Case "R_RANGE"
            l_str_Sql = "INSERT INTO R_RANGE(RANGECODE, RANGEDESC)VALUES( '" & _
                Left(Trim(txtRoverNo), 2) & "', '" & Left(Trim(txtRoverId), 15) & "')"
           
        Case "R_READER"
            l_str_Sql = "INSERT INTO R_READER(READERNO, RDRNAME)VALUES( '" & _
                Left(Trim(txtRoverNo), 6) & "', '" & Left(Trim(txtRoverId), 20) & "')"
            
        Case "F_HH"
            l_str_Sql = "INSERT INTO F_HH(HHTNO, HHID, HHDATE)VALUES( '" & _
                Left(Trim(txtRoverNo), 6) & "', '" & Left(Trim(txtRoverId), 6) & "', '" & Format(Trim(txtDateAcquired), "mm/dd/yyyy") & "')"
        
        Case "R_BUSCTR"
            l_str_Sql = "INSERT INTO R_BUSCTR(BC_CODE, BC_DESC, WORKING_DIR)VALUES( '" & _
                Left(Trim(txtRoverNo), 4) & "', '" & Left(Trim(txtRoverId), 30) & "', '" & Left(Trim(txtDateAcquired), 100) & "')"
            
    End Select
    
    Save_Add = DBExecute(l_str_Sql, Me.Name, "Save_Add")

End Function

Function Save_Edit() As Boolean
    Dim l_str_Sql As String
    
    Select Case g_str_Table
        Case "R_FF"
            l_str_Sql = "UPDATE R_FF SET FFCODE = '" & Left(Trim(txtRoverNo), 3) & _
                "', FFDESC = '" & Left(Trim(txtRoverId), 20) & _
                "' WHERE FFCODE = '" & frmEditBooks.lsvFileMain.SelectedItem.Text & "'"
        
        Case "R_RANGE"
            l_str_Sql = "UPDATE R_RANGE SET RANGECODE = '" & Left(Trim(txtRoverNo), 3) & _
                "', RANGEDESC = '" & Left(Trim(txtRoverId), 20) & _
                "' WHERE RANGECODE = '" & frmEditBooks.lsvFileMain.SelectedItem.Text & "'"
            
        Case "R_READER"
            l_str_Sql = "UPDATE R_READER SET READERNO = '" & Left(Trim(txtRoverNo), 6) & _
                "', RDRNAME = '" & Left(Trim(txtRoverId), 20) & _
                "' WHERE READERNO = '" & frmEditBooks.lsvFileMain.SelectedItem.Text & "'"
            
        Case "F_HH"
            l_str_Sql = "UPDATE F_HH SET HHTNO = '" & Left(Trim(txtRoverNo), 6) & _
                "', HHID = '" & Left(Trim(txtRoverId), 6) & _
                "', HHDATE = '" & Format(Trim(txtDateAcquired), "mm/dd/yyyy") & _
                "' WHERE HHTNO = '" & frmEditBooks.lsvFileMain.SelectedItem.Text & "'"
            
        Case "R_BUSCTR"
'            l_str_Sql = "UPDATE R_BUSCTR SET BC_CODE = '" & Left(Trim(txtRoverNo), 4) & _
'                "', BC_DESC = '" & Left(Trim(txtRoverId), 30) & _
'                "', WORKING_DIR = '" & Left(Trim(txtDateAcquired), 100) & _
'                "' WHERE BC_CODE = '" & frmEditBooks.lsvFileMain.SelectedItem.Text & "'"
            If OpenRecordset(g_rs_TBOOK, "SELECT BC_CODE FROM T_BOOK where BC_CODE='" & frmEditBooks.lsvFileMain.SelectedItem & "'", Me.Name, "cmdSave") Then
                l_str_Sql = "UPDATE R_BUSCTR SET BC_DESC = '" & Left(Trim(txtRoverId), 30) & _
                    "', WORKING_DIR = '" & Left(Trim(txtDateAcquired), 100) & _
                    "' WHERE BC_CODE = '" & frmEditBooks.lsvFileMain.SelectedItem.Text & "'"
            Else
                l_str_Sql = "UPDATE R_BUSCTR SET BC_CODE = '" & Left(Trim(txtRoverNo), 4) & _
                    "', BC_DESC = '" & Left(Trim(txtRoverId), 30) & _
                    "', WORKING_DIR = '" & Left(Trim(txtDateAcquired), 100) & _
                    "' WHERE BC_CODE = '" & frmEditBooks.lsvFileMain.SelectedItem.Text & "'"
            End If

    End Select
    
    Save_Edit = DBExecute(l_str_Sql, Me.Name, "Save_Edit")

End Function

Private Sub Form_Load()

If WindowState = vbNormal Then
    Me.Move (1000)
    Me.Top = 500
End If
    Select Case g_str_Table
        Case "L_FFCODE"
            Form_Setup "Code : ", "Description : ", False, 3, 20, "Field Finding"
            If g_str_AddEdit = "EDIT" Then
                If OpenRecordset(g_rs_TDOWNLOAD, "SELECT FF1 FROM T_UPLOAD where FF1='" & frmEditBooks.lsvFileMain.SelectedItem & "'", Me.Name, "cmdSave") Then
                    txtRoverNo.Enabled = False
                Else
                    txtRoverNo.Enabled = True
                End If
            End If
            
        Case "L_RANGECODE"
            Form_Setup "Code : ", "Description : ", False, 2, 20, "Range"
            If g_str_AddEdit = "EDIT" Then
                If OpenRecordset(g_rs_TUPLOAD, "SELECT RANGECODEID FROM T_UPLOAD where RANGECODEID='" & frmEditBooks.lsvFileMain.SelectedItem & "'", Me.Name, "cmdSave") Then
                    txtRoverNo.Enabled = False
                Else
                    txtRoverNo.Enabled = True
                End If
            End If
            
        Case "R_READER"
            Form_Setup "Reader No. : ", "Reader Name : ", False, 6, 20, "Meter Reader"
            
        Case "F_HH"
            Form_Setup "Rover No. : ", "Rover ID : ", True, 6, 6, "New Rover"
        
        Case "L_DMZCODE"
            Form_Setup "BC Code : ", "BC Description : ", True, 4, 30, "Business Center"
            lblDateAcquired.Caption = "Directory : "
            If g_str_AddEdit = "EDIT" Then
                If OpenRecordset(g_rs_TBOOK, "SELECT DMZCODE_ID FROM T_BOOK where DMZCODE_ID='" & frmEditBooks.lsvFileMain.SelectedItem & "'", Me.Name, "cmdSave") Then
                    txtRoverNo.Enabled = False
                Else
                    txtRoverNo.Enabled = True
                End If
            End If
    End Select
    
End Sub

Private Sub Form_Setup(strRovNoCap As String, strRovIdCap As String, bolDateVisible As Boolean, intRovNoLen As Integer, intRovIdLen As Integer, strFormCap As String)
    lblRoverNo.Caption = strRovNoCap
    lblRoverId.Caption = strRovIdCap
    lblDateAcquired.Visible = bolDateVisible
    txtDateAcquired.Visible = bolDateVisible
    txtRoverNo.MaxLength = intRovNoLen
    txtRoverId.MaxLength = intRovIdLen
        
    If g_str_Table = "R_BUSCTR" Then txtDateAcquired.MaxLength = 100
            
    If g_str_AddEdit = "ADD" Then
        Me.Caption = "Add " & strFormCap
        txtRoverNo.Text = ""
        txtRoverId.Text = ""
        If g_str_Table = "F_HH" Or g_str_Table = "R_BUSCTR" Then txtDateAcquired.Text = ""
    Else
        Me.Caption = "Edit " & strFormCap
        txtRoverNo.Text = frmEditBooks.lsvFileMain.SelectedItem.Text
        txtRoverId.Text = frmEditBooks.lsvFileMain.SelectedItem.SubItems(1)
        If g_str_Table = "F_HH" Or g_str_Table = "R_BUSCTR" Then txtDateAcquired.Text = frmEditBooks.lsvFileMain.SelectedItem.SubItems(2)
    End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmEditBooks.Enabled = True
End Sub

Private Sub txt_Change()
    If g_str_Table = "F_HH" Then
        If Len(Trim(txtRoverNo)) > 0 And Len(Trim(txtRoverId)) > 0 And Len(Trim(txtDateAcquired)) > 0 Then
            cmdOk.Enabled = True
        Else
            cmdOk.Enabled = False
        End If
    Else
        If Len(Trim(txtRoverNo)) > 0 And Len(Trim(txtRoverId)) > 0 Then
            cmdOk.Enabled = True
        Else
            cmdOk.Enabled = False
        End If
    End If
    
End Sub

Private Sub txtDateAcquired_Change()
    txt_Change
End Sub

Private Sub txtDateAcquired_GotFocus()
    TextHighlight txtDateAcquired
End Sub

Private Sub txtRoverId_Change()
    txt_Change
End Sub

Private Sub txtRoverId_GotFocus()
    TextHighlight txtRoverId
End Sub

Private Sub txtRoverNo_Change()
    txt_Change
End Sub

Private Sub txtRoverNo_GotFocus()
    TextHighlight txtRoverNo
End Sub
