Attribute VB_Name = "modDataTrans"
'Form variables for unwrapper function
Dim strMRU As String, iCycle As Integer
Dim iTotalAccts As Integer
Dim iUnread As Integer, iRead As Integer
Dim iOutOfRange As Integer
Dim sReadingDate As String

Dim temp$
Dim bcp As Boolean


Function Validate_Prev_Rdg(ByVal MRU As String, ByVal cycle As Integer) As Boolean

    Dim l_rs_DLERROR As Recordset
    Dim strSQL As String
    Dim intPrevCycle As Integer
    Dim dRdg As Long, uRdg As Long
    Dim dDoc As String
    Dim l_str_Sql As String
    Dim rsDL As Recordset
    Dim rec As Integer

    On Error GoTo errhandler

    Set l_rs_DLERROR = New ADODB.Recordset

    Validate_Prev_Rdg = True

    ' Initialize current and previous reading cycles
    intPrevCycle = cycle - 1
    If intPrevCycle = 0 Then intPrevCycle = 12
    Set rsDL = New ADODB.Recordset


    strSQL = "SELECT A.BOOKNO, " & cycle & " as CMONTH, A.SERIALNO, A.ACCTNAME, A.ADDRESS, " & _
             "A.PREVRDG, B.PRESRDG, A.DLDOC_NO " & _
             "FROM T_DOWNLOAD A, (SELECT c.* From t_upload c Where Not Exists (SELECT a.bookno, a.ulcycle, a.uldoc_no, a.ffcode, a.rangecode, a.deviceno, a.presrdg, a.rdgdate, " & _
             "a.rdgtime , a.METERNO, a.REMARKS, a.ACCTNUM, a.readtag, a.SEQNO, a.tries, a.newmtrbrand, a.newmtrnum " & _
             "From t_upload_his a inner join t_ver_extract b on a.bookno = b.bookno and a.ulcycle = b.ulcycle " & _
             "and a.acctnum = b.acctnum Where c.BookNo = a.BookNo And c.ulcycle = a.ulcycle And c.ACCTNUM = a.ACCTNUM " & _
             "AND a.origdata = 0 and b.condition = 1) Union " & _
             "SELECT a.bookno, a.ulcycle, a.uldoc_no, a.ffcode, a.rangecode, a.deviceno, a.presrdg, a.rdgdate, " & _
             "a.rdgtime , a.METERNO, a.REMARKS, a.ACCTNUM, a.readtag, a.SEQNO, a.tries, a.newmtrbrand, a.newmtrnum " & _
             "From t_upload_his a inner join t_ver_extract b on a.bookno = b.bookno and a.ulcycle = b.ulcycle " & _
             "and a.acctnum = b.acctnum WHERE " & _
             "a.origdata = 0 and b.condition = 1) as B  " & _
             "WHERE (A.ACCTNUM = B.ACCTNUM) AND A.PREVRDG <> B.PRESRDG AND A.DLCYCLE = " & cycle & " AND B.ULCYCLE = " & intPrevCycle & " and A.DLDOC_NO not in (SELECT DUP_DOCNO FROM T_DLERROR WHERE CMONTH = " & cycle & ")"

    rsDL.Open strSQL, g_Conn, adOpenStatic, adLockReadOnly

'    If Not OpenRecordset(g_rs_TDOWNLOAD, strSQL, "MDIMain", "Download from MWSI") Then
    If rsDL.RecordCount = 0 Then
        ' No records, no match with previous cycle to compare
        Exit Function
    Else
       ' Open DLERROR for tracking mismatched previous and current (past )reading
        ' Mismatch Reading
         Validate_Prev_Rdg = False


        l_str_Sql = "INSERT INTO T_DLERROR " & _
             "SELECT A.BOOKNO, " & cycle & " as CMONTH, A.SERIALNO, A.ACCTNAME, A.ADDRESS, " & _
             "A.PREVRDG, B.PRESRDG, A.DLDOC_NO " & _
             "FROM T_DOWNLOAD A, (SELECT c.* From t_upload c Where Not Exists (SELECT a.bookno, a.ulcycle, a.uldoc_no, a.ffcode, a.rangecode, a.deviceno, a.presrdg, a.rdgdate, " & _
             "a.rdgtime , a.METERNO, a.REMARKS, a.ACCTNUM, a.readtag, a.SEQNO, a.tries, a.newmtrbrand, a.newmtrnum " & _
             "From t_upload_his a inner join t_ver_extract b on a.bookno = b.bookno and a.ulcycle = b.ulcycle " & _
             "and a.acctnum = b.acctnum Where c.BookNo = a.BookNo And c.ulcycle = a.ulcycle And c.ACCTNUM = a.ACCTNUM " & _
             "AND a.origdata = 0 and b.condition = 1) Union " & _
             "SELECT a.bookno, a.ulcycle, a.uldoc_no, a.ffcode, a.rangecode, a.deviceno, a.presrdg, a.rdgdate, " & _
             "a.rdgtime , a.METERNO, a.REMARKS, a.ACCTNUM, a.readtag, a.SEQNO, a.tries, a.newmtrbrand, a.newmtrnum " & _
             "From t_upload_his a inner join t_ver_extract b on a.bookno = b.bookno and a.ulcycle = b.ulcycle " & _
             "and a.acctnum = b.acctnum WHERE " & _
             "a.origdata = 0 and b.condition = 1) as B  " & _
             "WHERE (A.ACCTNUM = B.ACCTNUM) AND A.PREVRDG <> B.PRESRDG AND A.DLCYCLE = " & cycle & " AND B.ULCYCLE = " & intPrevCycle & " and A.DLDOC_NO not in (SELECT DUP_DOCNO FROM T_DLERROR WHERE CMONTH = " & cycle & ")"
        If Not DBExecute(l_str_Sql, "MDIMain", "Download from MWSI") Then Exit Function


    End If

    rsDL.Close
    Set rsDL = Nothing


errhandler:
    If Err.Number <> 0 Then
        MsgBox "Error validating MRU " & MRU & "." & vbCrLf & vbCrLf & Err.Description
        Validate_Prev_Rdg = False

    End If
End Function

Sub Rollback_Download(ByVal OldBookNo As String, ByVal cycle As Integer)

    Dim l_str_Sql As String

    ' Delete entries in T_DOWNLOAD
    l_str_Sql = "DELETE FROM T_DOWNLOAD where SERIESNO='" & OldBookNo & "' AND BILL_MONTH = " & Padl(cycle, 2, "0")
    If Not DBExecute(l_str_Sql, "MDIMain", "LoadHostToDB") Then Exit Sub

    ' Delete entries in T_BKINFO
    l_str_Sql = "DELETE FROM T_BKINFO where BOOKNO='" & OldBookNo & "' AND CYCLE = " & cycle
    If Not DBExecute(l_str_Sql, "MDIMain", "LoadHostToDB") Then Exit Sub

    ' Reset ACTUAL_DL_DT from T_BOOK
    l_str_Sql = "UPDATE T_BOOK SET ACTUAL_DL_DT = Null WHERE BOOKNO = '" & OldBookNo & "' AND CYCLE = " & str(cycle)
    If Not DBExecute(l_str_Sql, "MDIMain", "LoadHostToDB") Then Exit Sub

    ' Reset BKSTCODE from T_SCHED
    l_str_Sql = "UPDATE T_SCHED SET BKSTCODE = 'SRD' WHERE BOOKNO = '" & OldBookNo & "' AND CYCLE = " & cycle
    If Not DBExecute(l_str_Sql, "MDIMain", "LoadHostToDB") Then Exit Sub

End Sub
Sub Rollback_Upload(ByVal OldBookNo As String, ByVal cycle As Integer)
    Dim l_str_Sql As String

    ' Clear T_UPLOAD
    l_str_Sql = "DELETE FROM T_UPLOAD WHERE SERIESNO = '" & OldBookNo & "' AND BILL_MONTH = " & Padl(str(cycle), 2, "0")
    If Not DBExecute(l_str_Sql, "MDIMain", "UploadToDB") Then Exit Sub

    ' Restore counts from T_BKINFO
    l_str_Sql = "UPDATE T_BKINFO SET UNREAD = ACCTS, READCNT = 0, OORCNT = 0 " & _
                 "WHERE BOOKNO = '" & OldBookNo & "' AND CYCLE = " & str(cycle)
    If Not DBExecute(l_str_Sql, "MDIMain", "UploadToDB") Then Exit Sub

    ' Reset RECV_SO_DT from T_BOOK
    l_str_Sql = "UPDATE T_BOOK SET RECV_SO_DT = Null WHERE BOOKNO = '" & OldBookNo & "' AND CYCLE = " & str(cycle)
    If Not DBExecute(l_str_Sql, "MDIMain", "UploadToDB") Then Exit Sub

    ' Clear T_FCONN
    l_str_Sql = "DELETE FROM T_FCONN WHERE BOOKNO = '" & OldBookNo & "' AND CYCLE = " & str(cycle)
    If Not DBExecute(l_str_Sql, "MDIMain", "UploadToDB") Then Exit Sub
End Sub

Function ValidateTotals(TR As Integer, RD As Integer, UNRD As Integer, OOR As Integer) As Boolean

    If iTotalAccts <> TR Then
        MsgBox "Mismatch on Total Accounts Count!" & vbCrLf & _
               "Expected" & str(iTotalAccts) & " but got" & str(TR), vbCritical, "Validate Totals"
        ValidateTotals = False
        Exit Function
    End If

    If iRead <> RD Then
        MsgBox "Mismatch on Actual Read Count!" & vbCrLf & _
               "Expected" & str(iRead) & " but got" & str(RD), vbCritical, "Validate Totals"
        ValidateTotals = False
        Exit Function
    End If

    If iUnread <> UNRD Then
        MsgBox "Mismatch on Unread Count!" & vbCrLf & _
               "Expected" & str(iIUnread) & " but got" & str(UNRD), vbCritical, "Validate Totals"
        ValidateTotals = False
        Exit Function
    End If

    If iOutOfRange <> OOR Then
        MsgBox "Mismatch on Out of Range Count!" & vbCrLf & _
               "Expected" & str(iOutOfRange) & " but got" & str(OOR), vbCritical, "Validate Totals"
        ValidateTotals = False
        Exit Function
    End If

    ValidateTotals = True
End Function

Function UnwrapHQfromSO(srcfile As String) As Boolean

Dim fso_in As Object, fso_out As Object
Dim f_in, f_out
Dim bHdrFound As Boolean, bFtrFound As Boolean

On Error GoTo errhandler

    ' Initialize
'    bHdrFound = False: bFtrFound = False
    Set fso_in = CreateObject("Scripting.FileSystemObject")
    Set fso_out = CreateObject("Scripting.FileSystemObject")

    ' Open input file for reading
    Set f_in = fso_in.OpenTextFile(srcfile, ForReading)

    ' Create upload.txt file for writing
    Set f_out = fso_out.CreateTextFile(App.Path & "\upload.txt", True)

    ' Read the input file and parse values
    Do While Not f_in.AtEndOfStream
       temp$ = f_in.ReadLine
       If Left(temp$, 3) = "HDR" Then
            bHdrFound = True
            ' Assign values and counts to Form Variables
            strMRU = Mid(temp$, 4, 8)   'MRU
            sStr = Mid(temp$, 12, 2)
            iCycle = Val(sStr)          'Cycle
            sStr = Mid(temp$, 14, 4)
            iTotalAccts = Val(sStr)     'Total Accounts
            sStr = Mid(temp$, 18, 4)
            iRead = Val(sStr)           'Total Read
            sStr = Mid(temp$, 22, 4)
            iUnread = Val(sStr)         'Total Unread
            sStr = Mid(temp$, 26, 4)
            iOutOfRange = Val(sStr)     'Total Out of Range
        Else
            ' Get Reading Date
            If Left(temp$, 3) = "FTR" Then
                bFtrFound = True
                sReadingDate = Mid$(temp$, 4, 8)
            Else
                ' Write detail lines to output file
                f_out.WriteLine (temp$)
            End If
        End If
    Loop

    Set fso_in = Nothing
    Set fso_out = Nothing
    'Check for existence of header and footer
'============================
'    If Not (bHdrFound And bFtrFound) Then
'        MsgBox "No header/footer record found!", vbCritical, "Unwrap File"
'        UnwrapHQfromSO = False
'    Else
        UnwrapHQfromSO = True
'    End If
'============================

errhandler:
    If Err.Number <> 0 Then
        MsgBox Err.Description, vbCritical, "Unwrap File"
        Set fso_in = Nothing
        Set fso_out = Nothing
        UnwrapHQfromSO = False
        Exit Function
    End If
End Function

Function UploadToHost(ByVal booknum As String, ByVal cycle As String, Optional ByVal CDelimiter = ",") As Boolean

    Const Filler4 = "    "
    Const Filler40 = "                                        "
    Const mtrrdrcode = "SZP"

    Dim cn As Connection, rs As Recordset, rsbasic As Recordset
    Dim UFile As String, fcfile As String
    Dim sPath As String
    Dim fs As Object
    Dim a, sval, mon$
    Dim dir As String
    Dim f As Object
    Dim prdgdt1 As Date
    Dim rsUpload As ADODB.Recordset
    Dim sqlUpload As String
 

    Set cn = CreateObject("ADODB.Connection")
    Set rs = CreateObject("ADODB.recordset")
    Set rsSQL = CreateObject("ADODB.recordset")
    Set rsbasic = CreateObject("ADODB.recordset")
    Set rsUpload = New ADODB.Recordset
    
    Dim SeriesNo As String
    Dim ACCTNUM As String
    Dim Bill_Month As Integer
    Dim SerialNo As String
    Dim SEQNO As String
    Dim Bill_date As String
    Dim UserDate As String
    Dim UserTime As String
    Dim FF1 As String
    Dim FF2 As String
    Dim REMARKS As String
    Dim Cons_Used As String
    Dim Ave_Tag As String
    Dim Bill_Amount As String
    Dim Discount As String
    Dim RangeCodeId As String
    Dim PrintTagId As String
    Dim DeliveryCodeId As String
    Dim DeliveryRemarks As String
    Dim PrintCount As String
    Dim MeterCode As String
    Dim upldCount As Integer
    Dim PRESRDG As Long 'updated from Integer to Long May 30,2011
    Dim dlCount As Integer
    Dim ulCount As Integer
    Dim dlulCount As ADODB.Recordset
    Set dlulCount = New ADODB.Recordset
    
    cn.ConnectionString = "Provider=SQLOLEDB.1; Data Source=" & g_DataSource & ";database=" & g_Database & "; Initial Catalog=;User Id=sa;Password=sqladmin;"
    cn.Open

    Set f = CreateObject("Scripting.FileSystemObject")
    lsql = "select DIRUPLD from S_PARAM where COMPANY_NAME='SUBIC WATER'"
    If OpenRecordset(g_rs_SPARAM, lsql, "frmBookStat", "MRU Management") Then
        If Not f.FolderExists(g_rs_SPARAM.Fields(0).Value & "\current\") Then f.CreateFolder (g_rs_SPARAM.Fields(0).Value & "\current\") ' upload directory
        dir = g_rs_SPARAM.Fields(0).Value & "\current\" & Format(Date, "dd") & "\"
        If Not f.FolderExists(dir) Then f.CreateFolder (dir) ' upload directory
    Else
        dir = ""
    End If

    mon$ = Padl(cycle, 2, "0") 'Format(Date, "MM")

    Dim RSCount As ADODB.Recordset
    Set RSCount = New ADODB.Recordset
    
    If OpenRecordset(RSCount, "SELECT UPLOADCNT, UPLOADDATE FROM T_BKINFO WHERE BOOKNO = '" & booknum & "' AND CYCLE = " & cycle & " AND UPLOADDATE = '" & Year(Now()) & Padl(Month(Now()), 2, "0") & Padl(Day(Now()), 2, "0") & "'", "", "") Then
    upldCount = RSCount.Fields("UPLOADCNT")
    Else
    upldCount = 0
    End If
    
    UFile = booknum & "U" & upldCount + 1
    sPath = dir & UFile & ".TXT"
    

    Set fs = CreateObject("Scripting.FileSystemObject")
    Set a = fs.CreateTextFile(sPath, True) '--create a textfile
    a.Close

    
    Bill_Month = cycle
    sqlUpload = "SELECT SERIESNO,ACCTNUM,BILL_MONTH,PRES_RDG,SERIALNO,SEQNO,BILL_DATE,USERDATE,USERTIME,FF1,FF2,REMARKS,CONS_USED,AVE_TAG,BILL_AMOUNT,DISCOUNT,RANGECODEID,PRINTTAGID,DELIVERYCODEID,DELIVERYREMARKS,PRINTCOUNT,METERCODE FROM T_UPLOAD WHERE SERIESNO = '" & booknum & "' AND BILL_MONTH = " & cycle & " AND UPLOADTAG = 0"
    If OpenRecordset(rsUpload, sqlUpload, "", "") Then
            Open sPath For Output As #1
            Do While Not rsUpload.EOF
                 
                SeriesNo = rsUpload.Fields("SERIESNO")
                ACCTNUM = rsUpload.Fields("ACCTNUM")
                If IsNull(rsUpload.Fields("SERIALNO")) = False Then
                    SerialNo = IIf(rsUpload.Fields("SERIALNO") = 0, "", rsUpload.Fields("SERIALNO"))
                Else
                    SerialNo = ""
                End If
                Bill_date = Mid(rsUpload.Fields("BILL_DATE"), 5, 2) & "/" & Mid(rsUpload.Fields("BILL_DATE"), 7, 2) & "/" & Mid(rsUpload.Fields("BILL_DATE"), 1, 4) 'updated May 30, 2011
                PRESRDG = CLng(rsUpload.Fields("PRES_RDG")) 'updated form CInt to CLng May 30, 2011
                UserDate = rsUpload.Fields("USERDATE")
                UserTime = rsUpload.Fields("USERTIME")
                
                If rsUpload.Fields("FF1") <> "0" Then
                    FF1 = Padl(rsUpload.Fields("FF1"), 2, "0")
                Else
                    FF1 = ""
                End If
                
                If rsUpload.Fields("FF2") <> "0" Then
                    FF2 = Padl(rsUpload.Fields("FF2"), 2, "0")
                Else
                    FF2 = ""
                End If
                
                If rsUpload.Fields("REMARKS") <> "0" Then
                    REMARKS = rsUpload.Fields("REMARKS")
                Else
                    REMARKS = ""
                End If
                
                If rsUpload.Fields("CONS_USED") <> "0" Then
                    Cons_Used = CInt(rsUpload.Fields("CONS_USED"))
                Else
                    Cons_Used = "0"
                End If
                
                If rsUpload.Fields("AVE_TAG") = True Then Ave_Tag = "1"
                If rsUpload.Fields("AVE_TAG") = False Then Ave_Tag = "0"
                
                Bill_Amount = rsUpload.Fields("BILL_AMOUNT")
                Discount = Format(CDbl(rsUpload.Fields("DISCOUNT")), "###0.00")
                RangeCodeId = rsUpload.Fields("RANGECODEID")
                PrintTagId = rsUpload.Fields("PRINTTAGID")
                If IsNull(rsUpload.Fields("DELIVERYCODEID")) = False Then
                DeliveryCodeId = Padl(rsUpload.Fields("DELIVERYCODEID"), 2, "0")
                Else
                DeliveryCodeId = ""
                End If
                
                If rsUpload.Fields("DELIVERYREMARKS") <> "0" Then
                    DeliveryRemarks = rsUpload.Fields("DELIVERYREMARKS")
                Else
                    DeliveryRemarks = ""
                End If
                
                
                PrintCount = rsUpload.Fields("PRINTCOUNT")
                MeterCode = rsUpload.Fields("METERCODE")
                
                Print #1, SeriesNo & CDelimiter & ACCTNUM & CDelimiter & SerialNo & CDelimiter & SEQNO & CDelimiter & Bill_date & CDelimiter & PRESRDG & CDelimiter & UserDate & CDelimiter & UserTime & CDelimiter & Trim(FF1) & CDelimiter & Trim(FF2) & CDelimiter & REMARKS & CDelimiter & Cons_Used & CDelimiter & Ave_Tag & CDelimiter & Bill_Amount & CDelimiter & Discount & CDelimiter & RangeCodeId & CDelimiter & PrintTagId & CDelimiter & DeliveryCodeId & CDelimiter & DeliveryRemarks & CDelimiter & PrintCount & CDelimiter & MeterCode
                
                rsUpload.MoveNext
            Loop
    
    End If
    
    Close #1 '--close txtfile for itinerary remarks
    Dim SQLUpdate As String
    SQLUpdate = "UPDATE T_BKINFO SET UPLOADCNT = " & upldCount + 1 & ", UPLOADDATE = '" & Year(Now()) & Padl(Month(Now()), 2, "0") & Padl(Day(Now()), 2, "0") & "' WHERE BOOKNO = '" & booknum & "' AND CYCLE = " & cycle
    DBExecute SQLUpdate, "", ""
    SQLUpdate = "UPDATE T_UPLOAD SET UPLOADTAG = 1 WHERE SERIESNO = '" & booknum & "' AND BILL_MONTH = " & cycle & " AND UPLOADTAG = 0"
    DBExecute SQLUpdate, "", ""
    

 
    
    
    UploadToHost = True

End Function

Public Function checkDecimal(deciNumber As String) As String
Dim len1 As Integer
Dim len2 As Integer

len1 = Len(deciNumber)
len2 = Len(Replace(deciNumber, ".", ""))

If len2 < len1 Then checkDecimal = deciNumber
If len2 = len1 Then checkDecimal = deciNumber & ".00"

End Function

Public Function UploadToDB2(CurrentBookNo As String, cycle As Integer, dir As String) As Boolean
'This function automatically inserts upload filed into database table T_UPLOAD_RAW

Dim SQLstring As String
Dim upldFileName As String
Dim sPath As String
Dim fso As Object
Dim withfc As Boolean
Dim fcfile As String, fcfile2 As String
Dim sqlStringFC As String
Dim inv(8) As String
Dim n As Integer
Dim cnDB As Connection
Dim rsDB As Recordset
Dim rstxt As Recordset
'On Error GoTo errhandler

Set rsDB = New ADODB.Recordset

Set cnTxt = New ADODB.Connection
Set rstxt = New ADODB.Recordset
Set fso = CreateObject("Scripting.FileSystemObject")
cnTxt.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & ";Extended Properties='text;HDR=No;FMT=Delimited'"
cnTxt.Open

OpenRecordset g_rs_TBOOK, "SELECT BC_CODE, SCHED_RDG_DT FROM T_BOOK WHERE BOOKNO = '" & CurrentBookNo & "' and CYCLE = " & cycle & "", "", "UploadToDB"

If Not OpenRecordset(g_rs_RBUSCTR, "SELECT WORKING_DIR, BC_DESC FROM R_BUSCTR WHERE BC_CODE =(select bc_code from T_book where bookno='" & CurrentBookNo & "' and cycle=" & cycle & ")", "", "UploadToDB") Then
    UploadToDB2 = False
    Exit Function
End If

    inv(0) = "\"
    inv(1) = "/"
    inv(2) = ":"
    inv(3) = "*"
    inv(4) = "?"
    inv(5) = """"
    inv(6) = "<"
    inv(7) = ">"
    inv(8) = "|"
    For n = 0 To 8
        If InStr(1, g_rs_RBUSCTR.Fields(1).Value, inv(n), vbTextCompare) > 0 Then
            If InStr(1, g_rs_RBUSCTR.Fields(1).Value, inv(n), vbTextCompare) = 1 Then
                MsgBox "A folder name cannot contain the character " & inv(n) & ". Please edit the business center name.", vbExclamation, "Invalid Folder Name"
                Exit Function
            Else
                BCDESC = Replace(g_rs_RBUSCTR.Fields(1).Value, inv(n), "")
                Exit For
            End If
        Else
            BCDESC = Trim(g_rs_RBUSCTR.Fields(1).Value)
        End If
    Next n





Set f = CreateObject("Scripting.FileSystemObject")
If Not f.FolderExists(dir) Then f.CreateFolder (dir) ' upload directory
If Not f.FolderExists(dir & "\FRSO\") Then f.CreateFolder (dir & "\FRSO\") ' FRSO folder
If Not f.FolderExists(dir & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC) Then f.CreateFolder (dir & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC) ' BC folder
If Not f.FolderExists(dir & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1))) Then f.CreateFolder (dir & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1))) ' year folder
If Not f.FolderExists(dir & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0")))) Then f.CreateFolder (dir & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0")))) ' cycle folder
If Not f.FolderExists(dir & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1)))) Then f.CreateFolder (dir & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1)))) ' sched mr day folder

CurrentDnldFilePath = dir & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1))) & "\"



    withfc = True
    fcfile = dir & "\FC" & Padl(cycle, 2, "0") & CurrentBookNo & ".TXT"
    If Not (fso.FileExists(fcfile)) Then
            withfc = False
    End If

' Check if batch was already received
rsDB.Open "SELECT * FROM T_BOOK WHERE BOOKNO = '" & CurrentBookNo & _
          "' AND CYCLE = " & str(cycle) & " AND RECV_SO_DT Is Not Null", g_Conn, adOpenStatic, adLockReadOnly
If rsDB.RecordCount > 0 Then
        Unload frmProg
        MsgBox "MRU already received from SO!", vbExclamation, "System Message"
        UploadToDB2 = False
        rsDB.Close
        Exit Function
End If
' End Check Batch


upldFileName = dir & "\" & "HQ" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
'UPLOAD TEXT FILE TO T_UPLOAD
If Not (fso.FileExists(upldFileName)) Then
    MsgBox "Accomplished Reading File does not exist!", vbInformation, "System Message"
    UploadToDB2 = False
    Exit Function
Else
    SQLstring = "EXEC sp_INSERT_UPLOADTOHOST " & cycle & ",'" & upldFileName & "'"
    DBExecute SQLstring, "", ""
    UploadToDB2 = True
End If

'MOVE UPLOAD FILE TO CURRENT
frso = CurrentDnldFilePath & "HQ" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
If fso.FileExists(frso) Then fso.DeleteFile frso
fso.MoveFile upldFileName, CurrentDnldFilePath & "HQ" & Format(cycle, "0#") & CurrentBookNo & ".TXT"

'UPLOAD FOUND CONNECTED TO T_FCONN
    If withfc = True Then
     fso.CopyFile fcfile, App.Path & "\foundconn.txt"
     rstxt.Open "SELECT * FROM foundconn.txt", cnTxt, adOpenStatic, adLockOptimistic, adCmdText
        Do While Not rstxt.EOF
            sqlStringFC = "INSERT INTO T_FCONN (BOOKNO,CYCLE,METERNO,SEQNO,ACCTNAME,PRESRDG,RDGDATE,RDGTIME,ADDRESS) VALUES ('" & rstxt.Fields("MRUNumber") & "'," & cycle & ",'" & rstxt.Fields("MeterNum") & "'," & Val(rstxt.Fields("SequenceNo")) & ",'" & rstxt.Fields("CustName") & "','" & rstxt.Fields("PresReading") & "','" & rstxt.Fields("ReadingDate") & "','" & rstxt.Fields("ReadingDate") & "','" & rstxt.Fields("ReadingTime") & "')"
            DBExecute sqlStringFC, "", ""
            rstxt.MoveNext
        Loop

        fcfile2 = dir & "\FC" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
        Name fcfile As fcfile2
        fcfrso = CurrentDnldFilePath & "FC" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
        If fso.FileExists(fcfrso) Then fso.DeleteFile fcfrso
        fso.MoveFile fcfile2, CurrentDnldFilePath & "FC" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
    rstxt.Close
    End If
'END FOUND CONNECTED FILE PROCESSING
cnTxt.Close

End Function

Public Function UploadToDB(CurrentBookNo As String, cycle As Integer, dir As String) As Boolean

    ' Function that reads the file of meter reading information from the Satellite Offices with an additional
    '   "header and footer line" that provides hash totals for the data.   This file will then
    '   be "unwrapped" at the MRMS HQ Side and processed before being uploaded to MWSI (Central)

    ' Connection and recordset for the text and Access link
    Dim cnTxt As Connection, cnDB As Connection
    Dim rstxt As Recordset, rsDB As Recordset, rsDBdl As Recordset, rsDBul As Recordset
    
    'for testing for reports
    Dim cnDBBackup As Connection, rsdbBackup As Recordset
    
    ' Use to copy the source file to "upload.txt"
    Dim f As Object, fso As Object

    ' Counts to update into T_BKINFO
    Dim totalrecs As Integer
    Dim unreadcount As Integer
    Dim readcount As Integer
    Dim oorcount As Integer

    Dim inv(8) As String
    Dim n As Integer
    Dim BCDESC As String

    ' local variables
    Dim bkfile As String
    Dim vlfile As String
    Dim fcfile As String, fcfile2 As String
    Dim readtag As String, oortag As String
    Dim sql$
    Dim fs
    Dim i As Integer
    Dim CurrentDnldFilePath As String
    Dim progcnt As Double
    Dim bValidateOK As Boolean
    Dim sPath As String
    Dim fc As Integer
    Dim withfc As Boolean
    Dim dlcnt As Integer
    Dim ulcnt As Integer
    Dim frso As String
    Dim fcfrso As String
    Dim rCount As Integer
    Dim Fileid As Long
    Dim rsFile As ADODB.Recordset
    
    

 On Error GoTo errhandler

' Text file cn and rs
Set cnTxt = New ADODB.Connection
Set rstxt = New ADODB.Recordset

' Local DB cn and rs
Set cnDB = New ADODB.Connection
Set rsDB = New ADODB.Recordset
Set rsFile = New ADODB.Recordset


Set rsDBdl = New ADODB.Recordset
Set rsDBul = New ADODB.Recordset

' Open connections and recordsets
cnTxt.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & ";Extended Properties='text;HDR=No;FMT=Delimited'"
cnTxt.Open

cnDB.ConnectionString = "Provider=SQLOLEDB.1; Data Source=" & g_DataSource & ";database=" & g_Database & "; Initial Catalog=;User Id=sa;Password=sqladmin;"
cnDB.Open


' start of transaction for rollback if error encountered
cnDB.BeginTrans


rsDBdl.Open "select * from t_download where seriesno='" & CurrentBookNo & "' and bill_month= " & str(cycle) & "", cnDB, adOpenStatic, adLockReadOnly
dlcnt = rsDBdl.RecordCount
rsDBdl.Close

' check total accounts loaded in t_upload
rsDBul.Open "select * from t_upload where seriesno='" & CurrentBookNo & "' and bill_month= " & str(cycle) & "", cnDB, adOpenStatic, adLockReadOnly
ulcnt = rsDBul.RecordCount
rsDBul.Close

' get bc and sched mr date
OpenRecordset g_rs_TBOOK, "SELECT DMZCODE_ID, SCHED_RDG_DT FROM T_BOOK WHERE BOOKNO = '" & CurrentBookNo & "' and CYCLE = " & cycle & "", "", "UploadToDB"

If Not OpenRecordset(g_rs_RBUSCTR, "SELECT WORKING_DIR, DMZ_DESC FROM L_DMZCODE WHERE  DMZCODE_ID =(select DMZCODE_ID from T_book where bookno='" & CurrentBookNo & "' and cycle=" & cycle & ")", "", "UploadToDB") Then
    UploadToDB = False
    Exit Function
End If

'==================== invalid folder name
    ' invalid characters for folder name
    inv(0) = "\"
    inv(1) = "/"
    inv(2) = ":"
    inv(3) = "*"
    inv(4) = "?"
    inv(5) = """"
    inv(6) = "<"
    inv(7) = ">"
    inv(8) = "|"
    ' check for invalid folder name
    For n = 0 To 8
        If InStr(1, g_rs_RBUSCTR.Fields(1).Value, inv(n), vbTextCompare) > 0 Then
            ' if first character is invalid, folder cannot be created and will exit sub
            If InStr(1, g_rs_RBUSCTR.Fields(1).Value, inv(n), vbTextCompare) = 1 Then
                MsgBox "A folder name cannot contain the character " & inv(n) & ". Please edit the business center name.", vbExclamation, "Invalid Folder Name"
                Exit Function
            ' folder name to be created will be chars before first occurrence of invalid character
            Else
                BCDESC = Replace(g_rs_RBUSCTR.Fields(1).Value, inv(n), "")
                Exit For
            End If
        Else
            BCDESC = Trim(g_rs_RBUSCTR.Fields(1).Value)
        End If
    Next n
'=======================

sPath = g_rs_RBUSCTR.Fields(0) & ""
If sPath = "" Then
    MsgBox "Business Center Directory not specified!", vbCritical, "System Message"
    UploadToDB = False
    Exit Function
End If

Set f = CreateObject("Scripting.FileSystemObject")
Set fso = CreateObject("Scripting.FileSystemObject")

' create folder frso\bccode bcdesc\year\cycle monthname\sched mr day, i.e. FRSO\0100 Novaliches\2008\09 September\06
If Not f.FolderExists(sPath) Then f.CreateFolder (sPath) ' upload directory
If Not f.FolderExists(sPath & "\FRSO\") Then f.CreateFolder (sPath & "\FRSO\") ' FRSO folder
If Not f.FolderExists(sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC) Then f.CreateFolder (sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC) ' BC folder
If Not f.FolderExists(sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1))) Then f.CreateFolder (sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1))) ' year folder
If Not f.FolderExists(sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0")))) Then f.CreateFolder (sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0")))) ' cycle folder
If Not f.FolderExists(sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1)))) Then f.CreateFolder (sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1)))) ' sched mr day folder

CurrentDnldFilePath = sPath & "\FRSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1))) & "\"



    ' Assign filenames to load
    'modified version 1.01 added validation to upload file - June 16,2011 JSE                                                                                                                                                                                                       Set FSO = CreateObject("Scripting.FileSystemObject")
    bkfile = sPath & "\" & "HQ" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
    vlfile = sPath & "\" & "VL" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
   
    If Not (fso.FileExists(vlfile)) Then
        fso.CopyFile bkfile, App.Path & "\upload.txt"
    Else
        ConcatenateFiles App.Path & "\uploadboth.txt", "", bkfile, vlfile
        fso.CopyFile App.Path & "\uploadboth.txt", App.Path & "\upload.txt"
    End If
    
    'Remove blank lines in upload.txt
    
    
    If Not (fso.FileExists(bkfile)) And Not (fso.FileExists(vlfile)) Then
        MsgBox "Accomplished Reading File does not exist!", vbInformation, "System Message"
        UploadToDB = False
        Exit Function
    End If



    withfc = True
    fcfile = sPath & "\FC" & CurrentBookNo & ".TXT"
    If Not (fso.FileExists(fcfile)) Then
            withfc = False
    End If

    
    If ulcnt = dlcnt Then
        Unload frmProg
        MsgBox "MRU received from SO already complete!", vbExclamation, "System Message"
        UploadToDB = False
        Exit Function
    End If
    ' End Check Batch


    
    If (fso.FileExists(bkfile)) Then
    s = "HQ" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
    ElseIf (fso.FileExists(vlfile)) Then
    s = "VL" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
    End If
    
        'INSERT INTO T_FILES AND GET FILEID
        rCount = 0
        DBExecute "INSERT INTO T_FILES(FILENAME,PROCESSDATE,LINECOUNT) VALUES ('" & s & "','" & Now() & "'," & rCount & ")", "", ""
            If OpenRecordset(rsFile, "SELECT TOP 1 FILENAMEID FROM T_FILES WHERE FILENAME = '" & s & "' ORDER BY PROCESSDATE DESC", "", "") = True Then
                Fileid = rsFile.Fields("FILENAMEID")
        End If
            
            
            

    rstxt.Open "SELECT * FROM upload.txt", cnTxt, adOpenStatic, adLockOptimistic, adCmdText


    ' Open T_UPLOAD for initialization
    rsDB.Open "SELECT * FROM T_UPLOAD", cnDB, adOpenDynamic, adLockOptimistic
    totalrecs = 0
    readcount = 0
    unreadcount = 0
    oorcount = 0

    ' Prevent Division by Zero if only one record in MRU
    If rstxt.RecordCount = 1 Then
        progcnt = 100
    Else
        progcnt = 1 / (rstxt.RecordCount - 1) * frmProg.Shape2.Width '100
    End If

    ' Load into T_UPLOAD - ACCOMPLISHED READINGS
    Do While Not rstxt.EOF
    
        If IsNull(rstxt.Fields("AccountNum")) = False Then

    If OpenRecordset(g_rs_TUPLOAD, "SELECT * FROM T_UPLOAD WHERE ACCTNUM = '" & rstxt.Fields("AccountNum") & "' and BILL_MONTH = '" & rstxt.Fields("BILLMONTH") & "'", "", "") Then
         GoTo Continue_Process:
    Else
    End If

        totalrecs = totalrecs + 1
        frmProg.ProgressBar progcnt, CurrentBookNo

        With rsDB

            .AddNew
            .Fields("SERIESNO") = rstxt.Fields("Series")
            .Fields("BILL_MONTH") = Padl(rstxt.Fields("BillMonth"), 2, "0")
            .Fields("ACCTNUM") = rstxt.Fields("AccountNum")
            If IsNull(rstxt.Fields("NewMeterSerial")) = False Then
                .Fields("SERIALNO") = rstxt.Fields("NewMeterSerial")
            End If
            .Fields("SEQNO") = rstxt.Fields("SequenceNo")
            .Fields("BILL_DATE") = Year(CDate(rstxt.Fields("BillDate"))) & Padl(Month(CDate(rstxt.Fields("BillDate"))), 2, "0") & Padl(Day(CDate(rstxt.Fields("BillDate"))), 2, "0")
            .Fields("PRES_RDG") = rstxt.Fields("Presrdg")
            .Fields("USERDATE") = rstxt.Fields("RdgDate")
            .Fields("USERTIME") = rstxt.Fields("RdgTime")
            .Fields("FF1") = IIf(IsNull(rstxt.Fields("FF1")) = True, "  ", rstxt.Fields("FF1"))
            .Fields("FF2") = IIf(IsNull(rstxt.Fields("FF2")) = True, "  ", rstxt.Fields("FF2"))
            .Fields("REMARKS") = rstxt.Fields("Remarks")
            .Fields("CONS_USED") = rstxt.Fields("Cons")
            .Fields("AVE_TAG") = rstxt.Fields("AveTag")
            .Fields("BILL_AMOUNT") = rstxt.Fields("BillAmount")
            .Fields("DISCOUNT") = rstxt.Fields("Discount")
            .Fields("BEFOREDUE") = rstxt.Fields("BeforeDueAmount")
            .Fields("PENALTY") = rstxt.Fields("PenaltyAmount")
            .Fields("AFTERDUE") = rstxt.Fields("AfterDueAmount")
            .Fields("RANGECODEID") = rstxt.Fields("RangeCode")
            .Fields("PRINTTAGID") = rstxt.Fields("PrintTag")
            .Fields("DELIVERYCODEID") = IIf(IsNull(rstxt.Fields("DelCode")) = True, "  ", rstxt.Fields("DelCode"))
            .Fields("DELIVERYDATE") = rstxt.Fields("DelDate")
            .Fields("DELIVERYTIME") = rstxt.Fields("DelTime")
            .Fields("DELIVERYREMARKS") = rstxt.Fields("DelRemarks")
            .Fields("PRINTCOUNT") = rstxt.Fields("PrintCount")
            .Fields("METERCODE") = rstxt.Fields("MeterCode")
            .Fields("FILEID") = Fileid
            .Fields("UPLOADTAG") = 0
            .Update
            
         
        End With

      
    
rCount = rCount + 1

    End If
    
Continue_Process:
        rstxt.MoveNext
    Loop



    ' Close all recordsets for the next file
    rstxt.Close
    rsDB.Close


DBExecute "UPDATE T_FILES SET LINECOUNT = " & rCount & " WHERE FILENAMEID = " & Fileid, "", ""

    frso = CurrentDnldFilePath & "HQ" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
    ' Delete file if exist
    If fso.FileExists(frso) Then fso.DeleteFile frso

    ' Move file to \FRSO folder in BC working folder
    If (fso.FileExists(bkfile)) Then
    fso.MoveFile bkfile, CurrentDnldFilePath & "HQ" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
    ' END ACCOMPLISHED READINGS FILE
    End If
    
    If (fso.FileExists(vlfile)) Then
    fso.MoveFile vlfile, CurrentDnldFilePath & "VL" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
    End If
    
    If withfc = True Then
        ' BEGIN FOUND CONNECTED FILE PROCESSING
        fso.CopyFile fcfile, App.Path & "\foundconn.txt"

        rstxt.Open "SELECT * FROM foundconn.txt", cnTxt, adOpenStatic, adLockOptimistic, adCmdText
        ' Open T_FCONN for initialization
        'rsDB.Open "SELECT * FROM T_FCONN", cnDB, adOpenDynamic, adLockOptimistic

        Dim sqlStringFC As String
       
        Do While Not rstxt.EOF

            sqlStringFC = "INSERT INTO T_FCONN (BOOKNO,CYCLE,METERNO,SEQNO,ACCTNAME,PRESRDG,RDGDATE,RDGTIME,ADDRESS)" & _
            " VALUES ('" & rstxt.Fields("Series") & "'," & cycle & ",'" & rstxt.Fields("MeterNum") & "'," & Val(rstxt.Fields("SequenceNo")) & ",'" & rstxt.Fields("CustName") & "','" & rstxt.Fields("PresReading") & "','" & rstxt.Fields("ReadingDate") & "','" & rstxt.Fields("ReadingTime") & "','" & rstxt.Fields("CustAddress") & "')"
            
            DBExecute sqlStringFC, "", ""
            rstxt.MoveNext
        Loop

        ' Rename the FC file from SO to a specific cycle file name
        fcfile2 = sPath & "\FC" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
        'fso.CopyFile fcfile, fcfile2
        Name fcfile As fcfile2    ' Rename fc file.

        fcfrso = CurrentDnldFilePath & "FC" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
        ' Delete file if exist
        If fso.FileExists(fcfrso) Then fso.DeleteFile fcfrso

        ' Move file to \FRSO folder in BC working folder
        fso.MoveFile fcfile2, CurrentDnldFilePath & "FC" & Format(cycle, "0#") & CurrentBookNo & ".TXT"
        ' END FOUND CONNECTED FILE PROCESSING

    rstxt.Close
   ' rsDB.Close

    End If

'commit transaction
cnDB.CommitTrans


'Clean up
cnTxt.Close
cnDB.Close


Set rstxt = Nothing
Set cnTxt = Nothing

Set rsDB = Nothing

Set rsDBdl = Nothing
Set rsDBul = Nothing
Set cnDB = Nothing


Unload frmProg
If ulcnt = dlcnt Then
    frmMRUMngt.UpdateRSOUpload CurrentBookNo, cycle
Else
    frmMRUMngt.UpdatePAUUpload CurrentBookNo, cycle
End If
UploadToDB = True

errhandler:
If Err.Number <> 0 Then
    MsgBox Err.Description & " for " & CurrentBookNo
    Unload frmProg
    If Err.Number <> -2147217887 Then
         ' rollback loaded mru with error
         cnDB.RollbackTrans
        
'         Rollback_Upload CurrentBookNo, cycle
    End If
End If
End Function


Function CreateDCDLFile(ByVal booknum As String, ByVal cycle As Integer, ByVal DayNo As Integer, Optional ByVal cdelim = ",") As Boolean
    ' Function that creates the file to send to the Satellite Offices with an additional
    '   "header and footer line" that provides hash totals for the data.   This file will then
    '   be "unwrapped" at the MRMS Satellite Office Side before being loaded to the local MRMS
    '   database and subsequently downloaded to the Rover.

    'Dim cnt As Integer
    Dim BusCenter As String
    Dim Dfile As String
    Dim sPath As String
    Dim fs As Object
    Dim a
    Dim strSQL As String
    Dim rsDL As Recordset

    ' Header and Footer Lines
    Dim hdrline As String, footrline As String

    ' Data Fields for Header Line
    Dim totaccts As String
    Dim activectr As String
    Dim inactivectr As String
    Dim readmtr As String
    Dim unreadmtr As String

    Dim inv(8) As String
    Dim n As Integer
    Dim BCDESC As String

    ' Data Fields for Footer Line
    Dim ReadingDate As String

    'Data Fields for download file
    Dim MRU As String
    Dim ACCTNUM As String
    Dim SEQNO As String
    Dim SerialNo As String
    Dim CUSTNAME As String
    Dim ADDRESS As String
    Dim RECONFEE As String
    Dim PUA As String
    Dim CURRENTAMT As String
    Dim DUEAMT As String

On Error GoTo errhandler
      'strSQL = "SELECT WORKING_DIR FROM R_BUSCTR WHERE BC_CODE = '" & BusCenter & "'"
' get bc and sched mr date
OpenRecordset g_rs_TBOOK, "SELECT BC_CODE, SCHED_RDG_DT FROM T_BOOK WHERE BOOKNO = '" & booknum & "' and CYCLE = " & cycle & "", "", "MRU Management"

If Not OpenRecordset(g_rs_RBUSCTR, "SELECT WORKING_DIR, BC_DESC FROM R_BUSCTR WHERE BC_CODE =(select bc_code from T_book where bookno='" & booknum & "' and cycle=" & cycle & ")", "", "MRU Management") Then
    CreateDCDLFile = False
    Exit Function
End If

sPath = g_rs_RBUSCTR.Fields(0) & ""
If sPath = "" Then
    MsgBox "Business Center Directory not specified!", vbCritical, "System Message"
    CreateDCDLFile = False
    Exit Function
End If

'==================== invalid folder name
    ' invalid characters for folder name
    inv(0) = "\"
    inv(1) = "/"
    inv(2) = ":"
    inv(3) = "*"
    inv(4) = "?"
    inv(5) = """"
    inv(6) = "<"
    inv(7) = ">"
    inv(8) = "|"
    ' check for invalid folder name
    For n = 0 To 8
        If InStr(1, g_rs_RBUSCTR.Fields(1).Value, inv(n), vbTextCompare) > 0 Then
            ' if first character is invalid, folder cannot be created and will exit sub
            If InStr(1, g_rs_RBUSCTR.Fields(1).Value, inv(n), vbTextCompare) = 1 Then
                MsgBox "A folder name cannot contain the character " & inv(n) & ". Please edit the business center name.", vbExclamation, "Invalid Folder Name"
                Exit Function
            ' folder name to be created will be chars before first occurrence of invalid character
            Else
                'MsgBox "A folder name cannot contain the character " & inv(n) & ". Reports will be saved in folder " & Mid(g_rs_RBUSCTR.Fields(1).Value, 1, InStr(1, g_rs_RBUSCTR.Fields(1).Value, inv(n), vbTextCompare) - 1) & ".", vbExclamation, "Invalid Folder Name"
                'BCDESC = Trim(Mid(g_rs_RBUSCTR.Fields(1).Value, 1, InStr(1, g_rs_RBUSCTR.Fields(1).Value, inv(n), vbTextCompare) - 1))
                BCDESC = Replace(g_rs_RBUSCTR.Fields(1).Value, inv(n), "")
                Exit For
            End If
        Else
            BCDESC = Trim(g_rs_RBUSCTR.Fields(1).Value)
        End If
    Next n
'=======================

Set fs = CreateObject("Scripting.FileSystemObject")
'' create folder TOSO\BC\Cycle\Sched MR date
'If Not fs.FolderExists(sPath) Then fs.CreateFolder (sPath) ' dl directory
'If Not fs.FolderExists(sPath & "\TOSO\") Then fs.CreateFolder (sPath & "\TOSO\") ' TOSO folder
'If Not fs.FolderExists(sPath & "\TOSO\" & g_rs_TBOOK.Fields(0)) Then fs.CreateFolder (sPath & "\TOSO\" & g_rs_TBOOK.Fields(0)) ' BC folder
'If Not fs.FolderExists(sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & "\" & Padl(cycle, 2, "0")) Then fs.CreateFolder (sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & "\" & Padl(cycle, 2, "0")) ' cycle folder
'If Not fs.FolderExists(sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & "\" & Padl(cycle, 2, "0") & "\" & GetMonth(CInt(Month(g_rs_TBOOK.Fields(1))))) Then fs.CreateFolder (sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & "\" & Padl(cycle, 2, "0") & "\" & GetMonth(CInt(Month(g_rs_TBOOK.Fields(1))))) ' sched mr date month folder
'If Not fs.FolderExists(sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & "\" & Padl(cycle, 2, "0") & "\" & GetMonth(CInt(Month(g_rs_TBOOK.Fields(1)))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1)))) Then fs.CreateFolder (sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & "\" & Padl(cycle, 2, "0") & "\" & GetMonth(CInt(Month(g_rs_TBOOK.Fields(1)))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1)))) ' sched mr date day folder
'DFile = sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & "\" & Padl(cycle, 2, "0") & "\" & GetMonth(CInt(Month(g_rs_TBOOK.Fields(1)))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1))) & "\" & BookNum & ".txt"

' create folder toso\bccode bcdesc\year\cycle monthname\sched mr day, i.e. TOSO\0100 Novaliches\2008\09 September\06
If Not fs.FolderExists(sPath) Then fs.CreateFolder (sPath) ' dl directory
If Not fs.FolderExists(sPath & "\TOSO\") Then fs.CreateFolder (sPath & "\TOSO\") ' TOSO folder
If Not fs.FolderExists(sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC) Then fs.CreateFolder (sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC) ' BC folder
If Not fs.FolderExists(sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1))) Then fs.CreateFolder (sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1))) ' year folder
If Not fs.FolderExists(sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0")))) Then fs.CreateFolder (sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0")))) ' cycle folder
If Not fs.FolderExists(sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1)))) Then fs.CreateFolder (sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1)))) ' sched mr day folder
If Not fs.FolderExists(sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1)))) Then fs.CreateFolder (sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1))) & "\" & "DISCON") ' DISCON folder

'DFile = sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1))) & "\" & g_rs_TBOOK.Fields(0) & "_" & booknum & ".txt"
Dfile = sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1))) & "\" & "DISCON" & "\" & g_rs_TBOOK.Fields(0) & "_" & booknum & ".txt"

'=============================
'' Get path to send download file to, based on business center code of MRU
'BusCenter = Left(BookNum, 4)
'strSQL = "SELECT WORKING_DIR FROM R_BUSCTR WHERE BC_CODE = '" & BusCenter & "'"
'
'If Not OpenRecordset(g_rs_BUSCTR, strSQL, "", "MRU Management") Then
'    CreateDLFile = False
'    Exit Function
'End If
'
'sPath = g_rs_BUSCTR.Fields(0) & ""
'If sPath = "" Then
'    MsgBox "Download Directory not specified!", vbCritical, "System Message"
'    CreateDLFile = False
'    Exit Function
'End If
'
'Set fs = CreateObject("Scripting.FileSystemObject")
'If Not fs.FolderExists(sPath) Then fs.CreateFolder (sPath)
''Create \TOSO folder if not existing
'
''If Not fs.FolderExists(sPath & "\TOSO\" & GetMonth(CInt(cycle)) & "\" & CStr(DayNo) & "\") Then fs.CreateFolder (sPath & "\TOSO\" & GetMonth(CInt(cycle)) & "\" & CStr(DayNo) & "\")
'If Not fs.FolderExists(sPath & "\TOSO\") Then fs.CreateFolder (sPath & "\TOSO\")
'If Not fs.FolderExists(sPath & "\TOSO\" & GetMonth(CInt(cycle))) Then fs.CreateFolder (sPath & "\TOSO\" & GetMonth(CInt(cycle)))
'If Not fs.FolderExists(sPath & "\TOSO\" & GetMonth(CInt(cycle)) & "\" & CStr(DayNo)) Then fs.CreateFolder (sPath & "\TOSO\" & GetMonth(CInt(cycle)) & "\" & CStr(DayNo))
'
'DFile = sPath & "\TOSO\" & GetMonth(CInt(cycle)) & "\" & CStr(DayNo) & "\" & BookNum & ".TXT"
'Set g_rs_BUSCTR = Nothing
'============================

'' Get info for header line from T_BKINFO and T_BOOK
'strSQL = "SELECT A.ACCTS, A.ACTIVE, A.INACTIVE, A.UNREAD, A.READCNT, B.SCHED_RDG_DT " & _
'         "FROM T_BOOK B, T_BKINFO A " & _
'         "WHERE A.BOOKNO = B.BOOKNO AND A.CYCLE = B.CYCLE AND B.BOOKNO = '" & booknum & "' " & _
'         "AND B.CYCLE = " & str$(cycle)
'
'If Not OpenRecordset(g_rs_TBKINFO, strSQL, "", "MRU Management") Then
'    CreateDCDLFile = False
'    Exit Function
'End If
'
'With g_rs_TBKINFO
'    .MoveFirst
'    totaccts = Padl(IIf(IsNull(.Fields("ACCTS").Value) = True, "", .Fields("ACCTS").Value), 4, "0")
'    activectr = Padl(IIf(IsNull(.Fields("ACTIVE").Value) = True, "", .Fields("ACTIVE").Value), 4, "0")
'    inactivectr = Padl(IIf(IsNull(.Fields("INACTIVE").Value) = True, "", .Fields("INACTIVE").Value), 4, "0")
'    unreadmtr = Padl(IIf(IsNull(.Fields("UNREAD").Value) = True, "", .Fields("UNREAD").Value), 4, "0")
'    readmtr = Padl(IIf(IsNull(.Fields("READCNT").Value) = True, "", .Fields("READCNT").Value), 4, "0")
'
'    ReadingDate = Format(.Fields("SCHED_RDG_DT"), "YYYYMMDD")
'    If IsNull(ReadingDate) Or ReadingDate = "" Then
'        MsgBox "No Scheduled Reading Date Found!", vbCritical, "System Message"
'        CreateDCDLFile = False
'        Set g_rs_TBKINFO = Nothing
'        Exit Function
'    End If
'End With
'
''Build Header and Footer Lines
'hdrline = "HDR" & booknum & Format(str(cycle), "0#") & totaccts & activectr & inactivectr & unreadmtr & readmtr
'footrline = "FTR" & ReadingDate
'
'Set g_rs_TBKINFO = Nothing

' Create download file with header and footer
Open Dfile For Output As #1
'Print #1, hdrline

Set rsDL = New ADODB.Recordset
'strSQL = "SELECT A.*, B.BC_CODE, B.SCHED_RDG_DT FROM T_DOWNLOAD A INNER JOIN T_BOOK B ON A.BOOKNO=B.BOOKNO AND A.DLCYCLE=B.CYCLE WHERE A.BOOKNO = '" & booknum & "' AND DLCYCLE = " & str$(cycle) & " ORDER BY A.SEQNO"
strSQL = "SELECT BOOKNO, ACCTNUM, SEQ, SERIALNO, CUSTNAME, ADDRESS, RECONFEE, PUA, CURRENT_AMT, DUE_AMT FROM T_DC_DOWNLOAD WHERE BOOKNO = '" & booknum & "' AND CYCLE = " & str$(cycle) & " ORDER BY SEQ"
rsDL.Open strSQL, g_Conn, adOpenStatic, adLockReadOnly

'strSQL = "SELECT * FROM T_DOWNLOAD WHERE BOOKNO = '" & BookNum & "' AND DLCYCLE = " & str$(cycle) & " ORDER BY SEQNO"
'If Not OpenRecordset(g_rs_TDOWNLOAD, strSQL, "", "MRU Management") Then
'    CreateDLFile = False
'    Exit Function
'End If

' Loop through download file records
'Do While Not g_rs_TDOWNLOAD.EOF
Do While Not rsDL.EOF

    '--assign values to variables
    MRU = rsDL.Fields("BOOKNO") & ""
    ACCTNUM = rsDL.Fields("ACCTNUM") & ""
    SEQNO = rsDL.Fields("SEQ") & ""
    SerialNo = rsDL.Fields("SERIALNO") & ""
    CUSTNAME = rsDL.Fields("CUSTNAME") & ""
    ADDRESS = rsDL.Fields("ADDRESS") & ""
    RECONFEE = rsDL.Fields("RECONFEE") & ""
    PUA = rsDL.Fields("PUA") & ""
    CURRENTAMT = rsDL.Fields("CURRENT_AMT") & ""
    DUEAMT = rsDL.Fields("DUE_AMT") & ""

    '--insert data to text file
    Print #1, MRU & cdelim & ACCTNUM & cdelim & SEQNO & cdelim & SerialNo & cdelim & CUSTNAME & cdelim & ADDRESS & cdelim & RECONFEE & cdelim & PUA & cdelim & CURRENTAMT & cdelim & DUEAMT

    'g_rs_TDOWNLOAD.MoveNext
    rsDL.MoveNext
Loop

'Footer
'Print #1, footrline
Close #1

CreateDCDLFile = True

rsDL.Close
Set rsDL = Nothing
'Set g_rs_TDOWNLOAD = Nothing

errhandler:
    If Err.Number <> 0 Then
        MsgBox Err.Description, , "Error"
    End If

End Function

Function CreateDLFile(ByVal Series As String, ByVal cycle As Integer, ByVal DayNo As Integer, Optional ByVal cdelim = "|") As Boolean
    ' Function that creates the file to send to the Satellite Offices with an additional
    '   "header and footer line" that provides hash totals for the data.   This file will then
    '   be "unwrapped" at the MRMS Satellite Office Side before being loaded to the local MRMS
    '   database and subsequently downloaded to the Rover.

    'Dim cnt As Integer
    Dim BusCenter As String
    Dim Dfile As String
    Dim sPath As String
    Dim fs As Object
    Dim a
    Dim strSQL As String
    Dim rsDL As Recordset
    Dim rsPRDG As Recordset
    Dim PREVCYCLE As Integer

    ' Header and Footer Lines
    Dim hdrline As String, footrline As String

    ' Data Fields for Header Line
    Dim totaccts As String
    Dim activectr As String
    Dim inactivectr As String
    Dim readmtr As String
    Dim unreadmtr As String

    Dim inv(8) As String
    Dim n As Integer


    ' Data Fields for Footer Line
    Dim ReadingDate As String

    'Data Fields for download file
      
Dim AccountNum As String
Dim SequenceNo As Integer
Dim MeterSerial As String
Dim CUSTNAME As String
Dim CustAddress As String
Dim ConsumerType As String
Dim Status As String
Dim PrevReading As String
Dim avecons As String
Dim PrevReadingDate As String
Dim arrears As String
Dim Others As String
Dim disconTag As String
Dim Multiplier As String
Dim CheckDigit As String
Dim discountTag As String
Dim Percent As String
Dim GovtFlag As String
Dim finalRdg As String
Dim ReplaceDate As String
Dim NewMtrInitRdg As String
Dim NewMtrCons As String
Dim PresConsTag As String
Dim FaxEmail As String
Dim MasterTag As String
Dim BillDate As String
Dim DueDate As String
Dim DiscDate As String
Dim PeriodStart As String
'Additional variable by William R Sunga prevcons
Dim prevcons As String
Dim prevcons2 As String
Dim prevcons3 As String
'Additional variable by Paul May 5 2018
Dim vat_code As String
On Error GoTo errhandler

OpenRecordset g_rs_TBOOK, "SELECT DMZCODE_ID, SCHED_RDG_DT FROM T_BOOK WHERE BOOKNO = '" & Series & "' and CYCLE = " & cycle & "", "", "MRU Management"

If Not OpenRecordset(g_rs_RBUSCTR, "SELECT WORKING_DIR, DMZ_DESC FROM dbo.L_DMZCODE WHERE DMZCODE_ID =(select DMZCODE_ID from T_book where BOOKNO='" & Series & "' and cycle=" & cycle & ")", "", "MRU Management") Then
    CreateDLFile = False
    Exit Function
End If

sPath = g_rs_RBUSCTR.Fields(0) & ""
If sPath = "" Then
    MsgBox "Business Center Directory not specified!", vbCritical, "System Message"
    CreateDLFile = False
    Exit Function
End If

' GET PREVCYCLE FOR T_UPLOAD DATA
If cycle = 1 Then
    PREVCYCLE = 12
Else
    PREVCYCLE = cycle - 1
End If

'==================== invalid folder name
    ' invalid characters for folder name
    inv(0) = "\"
    inv(1) = "/"
    inv(2) = ":"
    inv(3) = "*"
    inv(4) = "?"
    inv(5) = """"
    inv(6) = "<"
    inv(7) = ">"
    inv(8) = "|"
    ' check for invalid folder name
    For n = 0 To 8
        If InStr(1, g_rs_RBUSCTR.Fields(1).Value, inv(n), vbTextCompare) > 0 Then
            ' if first character is invalid, folder cannot be created and will exit sub
            If InStr(1, g_rs_RBUSCTR.Fields(1).Value, inv(n), vbTextCompare) = 1 Then
                MsgBox "A folder name cannot contain the character " & inv(n) & ". Please edit the business center name.", vbExclamation, "Invalid Folder Name"
                Exit Function
            ' folder name to be created will be chars before first occurrence of invalid character
            Else
                BCDESC = Replace(g_rs_RBUSCTR.Fields(1).Value, inv(n), "")
                Exit For
            End If
        Else
            BCDESC = Trim(g_rs_RBUSCTR.Fields(1).Value)
        End If
    Next n
'=======================

Set fs = CreateObject("Scripting.FileSystemObject")

' create folder toso\bccode bcdesc\year\cycle monthname\sched mr day, i.e. TOSO\0100 Novaliches\2008\09 September\06
If Not fs.FolderExists(sPath) Then fs.CreateFolder (sPath) ' dl directory
If Not fs.FolderExists(sPath & "\TOSO\") Then fs.CreateFolder (sPath & "\TOSO\") ' TOSO folder
If Not fs.FolderExists(sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC) Then fs.CreateFolder (sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC) ' BC folder
If Not fs.FolderExists(sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1))) Then fs.CreateFolder (sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1))) ' year folder
If Not fs.FolderExists(sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0")))) Then fs.CreateFolder (sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0")))) ' cycle folder
If Not fs.FolderExists(sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1)))) Then fs.CreateFolder (sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1)))) ' sched mr day folder

Dfile = sPath & "\TOSO\" & g_rs_TBOOK.Fields(0) & " " & BCDESC & "\" & Year(g_rs_TBOOK.Fields(1)) & "\" & Padl(cycle, 2, "0") & " " & GetMonth(CInt(Padl(cycle, 2, "0"))) & "\" & Day(CDate(g_rs_TBOOK.Fields(1))) & "\" & g_rs_TBOOK.Fields(0) & "_" & Mid(Series, 4, 3) & ".txt"



' Get info for header line from T_BKINFO and T_BOOK
strSQL = "SELECT A.ACCTS, A.ACTIVE, A.INACTIVE, A.UNREAD, A.READCNT, B.SCHED_RDG_DT FROM T_BOOK B, T_BKINFO A WHERE A.BOOKNO = B.BOOKNO AND A.CYCLE = B.CYCLE AND B.BOOKNO = '" & Series & "' AND B.CYCLE = " & str$(cycle)

If Not OpenRecordset(g_rs_TBKINFO, strSQL, "", "MRU Management") Then
    CreateDLFile = False
    Exit Function
End If

With g_rs_TBKINFO
    .MoveFirst
    totaccts = Padl(IIf(IsNull(.Fields("ACCTS").Value) = True, "", .Fields("ACCTS").Value), 4, "0")
    activectr = Padl(IIf(IsNull(.Fields("ACTIVE").Value) = True, "", .Fields("ACTIVE").Value), 4, "0")
    inactivectr = Padl(IIf(IsNull(.Fields("INACTIVE").Value) = True, "", .Fields("INACTIVE").Value), 4, "0")
    unreadmtr = Padl(IIf(IsNull(.Fields("UNREAD").Value) = True, "", .Fields("UNREAD").Value), 4, "0")
    readmtr = Padl(IIf(IsNull(.Fields("READCNT").Value) = True, "", .Fields("READCNT").Value), 4, "0")

    ReadingDate = Format(.Fields("SCHED_RDG_DT"), "YYYYMMDD")
    If IsNull(ReadingDate) Or ReadingDate = "" Then
        MsgBox "No Scheduled Reading Date Found!", vbCritical, "System Message"
        CreateDLFile = False
        Set g_rs_TBKINFO = Nothing
        Exit Function
    End If
End With

'Build Header and Footer Lines
hdrline = "HDR" & Series & Format(str(cycle), "0#") & totaccts & activectr & inactivectr & unreadmtr & readmtr


Set g_rs_TBKINFO = Nothing

' Create download file with header and footer
Open Dfile For Output As #1
Print #1, hdrline

Set rsDL = New ADODB.Recordset
strSQL = "SELECT A.*, B.DMZCODE_ID, B.SCHED_RDG_DT FROM T_DOWNLOAD A INNER JOIN T_BOOK B ON A.SERIESNO=B.BOOKNO AND A.BILL_MONTH=B.CYCLE WHERE A.SERIESNO = '" & Series & "' AND A.BILL_MONTH = " & str$(cycle) & " ORDER BY A.SEQNO"


rsDL.Open strSQL, g_Conn, adOpenStatic, adLockReadOnly

Do While Not rsDL.EOF
   
AccountNum = rsDL.Fields("ACCTNUM")
SequenceNo = rsDL.Fields("SEQNO")
MeterSerial = IIf(IsNull(rsDL.Fields("SERIALNO")) = True, "", rsDL.Fields("SERIALNO"))
CUSTNAME = rsDL.Fields("ACCTNAME")
CustAddress = rsDL.Fields("ADDRESS")
ConsumerType = rsDL.Fields("CONSUMERTYPEID")
Status = rsDL.Fields("STATUS")
If IsNull(rsDL.Fields("PREVRDG")) = False Then
PrevReading = rsDL.Fields("PREVRDG")
End If
avecons = rsDL.Fields("AVERAGE")
PrevReadingDate = ""
arrears = Format(rsDL.Fields("ARREARS"), "###0.00")
Others = rsDL.Fields("OTHERS")
disconTag = IIf(rsDL.Fields("DISCTAG") = True, "1", "0")
Multiplier = Trim(rsDL.Fields("MULTIPLIER"))
CheckDigit = rsDL.Fields("CHECK_DIGIT")
discountTag = IIf(rsDL.Fields("DISCOUNT_TAG") = True, "1", "0")
If IsNull(rsDL.Fields("DISC_PERCENT")) = False Then
    Percent = IIf(rsDL.Fields("DISC_PERCENT") = "0", "", rsDL.Fields("DISC_PERCENT"))
End If
GovtFlag = IIf(IsNull(rsDL.Fields("GOVT_TAG")) = True, "", rsDL.Fields("GOVT_TAG"))
finalRdg = IIf(IsNull(rsDL.Fields("FINAL_RDG")) = True, "", rsDL.Fields("FINAL_RDG"))
ReplaceDate = IIf(IsNull(rsDL.Fields("REPL_DATE")) = True, "", rsDL.Fields("REPL_DATE"))
NewMtrInitRdg = IIf(IsNull(rsDL.Fields("INIT_RDG")) = True, "", rsDL.Fields("INIT_RDG"))
NewMtrCons = IIf(IsNull(rsDL.Fields("NEWMTRCONS")) = True, "", rsDL.Fields("NEWMTRCONS"))
PresConsTag = IIf(IsNull(rsDL.Fields("PRES_CONS_TAG")) = True, "", rsDL.Fields("PRES_CONS_TAG"))
FaxEmail = IIf(IsNull(rsDL.Fields("FAX_EMAIL")) = True, "", rsDL.Fields("FAX_EMAIL"))
MasterTag = IIf(IsNull(rsDL.Fields("MASTERTAG")) = True, "", rsDL.Fields("MASTERTAG"))
PeriodStart = Year(rsDL.Fields("PERIOD_START")) & Padl(Month(rsDL.Fields("PERIOD_START")), 2, "0") & Padl(Day(rsDL.Fields("PERIOD_START")), 2, "0")

BillDate = IIf(IsNull(rsDL.Fields("BILL_DATE")) = True, "", rsDL.Fields("BILL_DATE"))
DueDate = IIf(IsNull(rsDL.Fields("DUE_DATE")) = True, "", rsDL.Fields("DUE_DATE"))
DiscDate = IIf(IsNull(rsDL.Fields("DISCDATE")) = True, "", rsDL.Fields("DISCDATE"))
'Added by William R Sunga Feb 18 2013
prevcons = rsDL.Fields("PREVCONS") & ""
prevcons2 = rsDL.Fields("PREVCONS2") & ""
prevcons3 = rsDL.Fields("PREVCONS3") & ""
'Additional vat_code pacruz 050918
vat_code = rsDL.Fields("VAT_CODE") & ""
    '--insert data to text file
    Print #1, Series & cdelim & AccountNum & cdelim & SequenceNo & cdelim & MeterSerial & cdelim & CUSTNAME & cdelim & CustAddress & _
        cdelim & ConsumerType & cdelim & Status & cdelim & PrevReading & cdelim & avecons & cdelim & PeriodStart & cdelim & _
        arrears & cdelim & Others & cdelim & disconTag & cdelim & Multiplier & cdelim & CheckDigit & cdelim & discountTag & _
        cdelim & Percent & cdelim & GovtFlag & cdelim & finalRdg & cdelim & ReplaceDate & cdelim & NewMtrInitRdg & cdelim & NewMtrCons & _
        cdelim; PresConsTag & cdelim & FaxEmail & cdelim & MasterTag & cdelim & prevcons & cdelim & prevcons2 & cdelim & prevcons3 & cdelim & vat_code
        
    rsDL.MoveNext
Loop
footrline = "FTR" & BillDate & DueDate & DiscDate
'Footer
Print #1, footrline
Close #1

CreateDLFile = True

rsDL.Close
Set rsDL = Nothing
'Set g_rs_TDOWNLOAD = Nothing

errhandler:
If Err.Number <> 0 Then
   MsgBox Err.Description, , "Error"
  'MsgBox Err.Description, "Error Processing MRU: " & MRU & ", Acct#: " & CustInstallNo
End If

End Function
Public Function ValidateDiscon(filenme As String, fileDir As String, Downloadtype As String, FolderDestin As String, processDate As Date) As Boolean
Dim filefullname As String
Dim sqlVal As String
Dim xlApp As Excel.Application
Dim fs As FileSystemObject
Dim ts As TextStream
Dim lineString As String
Set fs = New FileSystemObject
Dim X As Integer

    ' used to copy dowload file in excel
    Dim xlSht As Excel.Worksheet
    Dim xlRng As Excel.Range
    Dim MRU As String
    Dim BP As String
    Dim ACCTNUM As String
    Dim METERNO As String
    Dim seq As String
    Dim CUSTNAME As String
    Dim ADDRESS As String
    Dim PUA As Double
    Dim CURRENT_AMT As Double
    Dim INSTCHARGE As Double
    Dim METERCHARGE As Double
    Dim RECONFEE As Double
    Dim DUE_AMT As Double
    Dim PIA As Double
    Dim PREPAYADJ As Double
    Dim rowCount As Integer
    Dim sqlDISCON As String
    Dim sqlDISCONVER As String

filefullname = fileDir & "\" & filenme



If Downloadtype = "Disconnection" Then
    Set xlApp = New Excel.Application
    xlApp.DisplayAlerts = False
    xlApp.Workbooks.Open (filefullname)

    X = 2
    rowCount = 0
    While rowCount = 0
        If xlApp.Cells(X, 3) = "" Then rowCount = X
        X = X + 1
    Wend

sqlVal = "EXEC sp_INSERT_FILESOURCE '" & Downloadtype & "','" & filenme & "'," & rowCount - 2 & ",'" & processDate & "'"
DBExecute sqlVal, "", ""

For X = 2 To rowCount - 1
        MRU = xlApp.Cells(X, 1)
        BP = xlApp.Cells(X, 2)
        ACCTNUM = Padl(xlApp.Cells(X, 3), 12, 0)
        METERNO = xlApp.Cells(X, 4)
        seq = xlApp.Cells(X, 5)
        CUSTNAME = Replace(xlApp.Cells(X, 6), "'", "")
        ADDRESS = Replace(xlApp.Cells(X, 7), "'", "")
        PUA = xlApp.Cells(X, 8)
        CURRENT_AMT = xlApp.Cells(X, 9)
        INSTCHARGE = xlApp.Cells(X, 10)
        METERCHARGE = xlApp.Cells(X, 11)
        'RECONFEE = g_ReOpeningFee
        RECONFEE = xlApp.Cells(X, 12)
        DUE_AMT = Replace(xlApp.Cells(X, 13), "..", ".")
        PIA = xlApp.Cells(X, 14)
        PREPAYADJ = xlApp.Cells(X, 15)

 
        sqlDISCON = "EXEC sp_INSERT_DISCDOWNLOAD_RAW '" & MRU & _
        "','" & BP & "','" & ACCTNUM & "','" & METERNO & "'," & CInt(seq) & ",'" & CUSTNAME & "','" & ADDRESS & "'," & _
        CDbl(PUA) & "," & CDbl(CURRENT_AMT) & "," & CDbl(INSTCHARGE) & "," & CDbl(METERCHARGE) & "," & CDbl(RECONFEE) & "," & CDbl(DUE_AMT) & "," & _
        CDbl(PIA) & "," & CDbl(PREPAYADJ) & ",'" & filenme & "'"
        
        DBExecute sqlDISCON, "", ""

        
Next X

    xlApp.Workbooks.Close
    FileCopy filefullname, FolderDestin & "\" & filenme
    Kill filefullname
    ValidateDiscon = True
Else
    ValidateDiscon = False

End If

End Function

Public Function LoadDisconToHQ(filePath As String, cycle As Integer) As Boolean
    
Dim f As Object, fso As Object
Dim rsZone As ADODB.Recordset
Dim rsDiscon As ADODB.Recordset
' Text file cn and rs
Set cnTxt = New ADODB.Connection
Set rstxt = New ADODB.Recordset
Set rsdbBackup = New ADODB.Recordset
Set rsDLBackup = New ADODB.Recordset
Set rsZone = New ADODB.Recordset
' Local DB cn and rs
Set cnDB = New ADODB.Connection
Set rsDB = New ADODB.Recordset
Set rsBkInfo = New ADODB.Recordset
Set rsDiscon = New ADODB.Recordset
Dim rsFile As ADODB.Recordset

Dim Series As String
Dim AccountNum As String
Dim AccountName As String
Dim ADDRESS As String
Dim Serial As String
Dim arrears As String
Dim dcdate As String
Dim Fileid As Long
Dim DayNo As Integer
Dim Zone As String
Dim strZone As String
Dim rCount As Integer
' Additional columns by William May 17 2011
Dim dcstatus As String
' Open connections and recordsets
'cnTxt.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & ";Extended Properties='text;HDR=No;FMT=Delimited'"
cnTxt.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & ";Extended Properties='text;HDR=No;FMT=Delimited'"
cnTxt.Open

'cnDB.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\mrmshq.mdb"
cnDB.ConnectionString = "Provider=SQLOLEDB.1; Data Source=" & g_DataSource & ";database=" & g_Database & "; Initial Catalog=;User Id=sa;Password=sqladmin;"
cnDB.Open


Set fs = CreateObject("Scripting.FileSystemObject")
Set f = fs.GetFolder(filePath)
Set fc = f.Files
If fc.Count = 0 Then
    Unload frmProg
    MsgBox "No data to load, please check the directory.", vbExclamation, "Disconnection Download"
    Exit Function
End If
If Not fs.FolderExists(f & "\current") Then
    Unload frmProg
    MsgBox "Current archive folder does not exist!", vbExclamation, "Disconnection Download"
    Exit Function
End If

For Each f1 In fc
    ' start of transaction for rollback if error encountered
    cnDB.BeginTrans

    s = f1.Name
    dcdate = "20" & Mid(s, 11, 2) & Mid(s, 7, 2) & Mid(s, 9, 2)
    DayNo = Mid(s, 9, 2)
    bValidateOK = True
    'MsgBox s
    FileFullPath = filePath & "\" & s
    CurrentDnldFilePath = filePath & "\current\" & s
    Set fso = CreateObject("Scripting.FileSystemObject")

    If Not (fso.FileExists(FileFullPath)) Then
        MsgBox "File does not exist!", vbInformation, "System Message"
        LoadDisconToHQ = False
        Exit Function
    End If

    ' copy to local download.txt file
    fso.CopyFile FileFullPath, App.Path & "\dcdownload.txt"

    ' delete " (quotation mark) from data of download.txt
    Set a = fs.OpenTextFile(App.Path & "\dcdownload.txt", ForReading, TristateUseDefault)
    d = a.ReadAll
    Set a = fs.OpenTextFile(App.Path & "\dcdownload.txt", ForWriting, TristateUseDefault)
    a.Write Replace(d, """", "")
    a.Close
    
    DBExecute "INSERT INTO T_FILES(FILENAME,PROCESSDATE,LINECOUNT) VALUES ('" & s & "','" & Now() & "'," & rCount & ")", "", ""
    If OpenRecordset(rsFile, "SELECT TOP 1 FILENAMEID FROM T_FILES WHERE FILENAME = '" & s & "' ORDER BY PROCESSDATE DESC", "", "") = True Then
    Fileid = rsFile.Fields("FILENAMEID")
    End If
    
    rsDiscon.Open "SELECT * FROM T_DISCDOWNLOAD", cnDB, adOpenDynamic, adLockOptimistic
    ' Check if this batch was already loaded
    rstxt.Open "SELECT * from dcdownload.txt", cnTxt, adOpenStatic, adLockOptimistic, adCmdText
    rstxt.MoveFirst
    Do While Not rstxt.EOF
        

        AccountNum = rstxt.Fields("AccountNum")
        Zone = Mid(AccountNum, 1, 2)
        AccountName = rstxt.Fields("AcctName")
        ADDRESS = rstxt.Fields("Address")
        Serial = rstxt.Fields("SerialNo")
        arrears = rstxt.Fields("Arrears")
        dcstatus = "OPEN"
            strZone = "SELECT DMZCODE_ID FROM dbo.L_DMZZONE WHERE DMZZONE_ID = '" & Zone & "'"
            If OpenRecordset(rsZone, strZone, "", "") Then
            Series = rsZone.Fields("DMZCODE_ID") & Mid(AccountNum, 1, 3)
            End If
                With rsDiscon
                    .AddNew
                    .Fields("CYCLE") = cycle
                    .Fields("SERIES") = Series
                    .Fields("ACCTNUM") = AccountNum
                    .Fields("ACCTNAME") = AccountName
                    .Fields("ADDRESS") = ADDRESS
                    .Fields("SERIALNO") = Serial
                    .Fields("ARREARS") = arrears
                    .Fields("DCDATE") = dcdate
                    .Fields("DAYNO") = DayNo
                    .Fields("FILEID") = Fileid
                    .Fields("STATUS") = dcstatus
                    .Update
                End With
        
        
        rCount = rCount + 1
        rstxt.MoveNext
    Loop
    
DBExecute "UPDATE T_FILES SET LINECOUNT = " & rCount & " WHERE FILENAMEID = " & Fileid, "", ""
rstxt.Close
rsDiscon.Close
cnDB.CommitTrans
LoadDisconToHQ = True
fso.MoveFile FileFullPath, CurrentDnldFilePath

Next

    ' Move file to current in download source folder
    
errhandler:
If Err.Number <> 0 Then
    MsgBox "Error downloading file '" & s & "'." & vbCrLf & vbCrLf & Err.Description
    xlApp.Workbooks.Close
    Unload frmProg
    cnDB.RollbackTrans

End If
End Function
Public Function LoadReconToHQ(filePath As String, cycle As Integer) As Boolean
    
Dim f As Object, fso As Object
Dim rsZone As ADODB.Recordset
Dim rsDiscon As ADODB.Recordset
Dim rsFile As ADODB.Recordset

' Text file cn and rs
Set cnTxt = New ADODB.Connection
Set rstxt = New ADODB.Recordset
Set rsdbBackup = New ADODB.Recordset
Set rsDLBackup = New ADODB.Recordset
Set rsZone = New ADODB.Recordset
' Local DB cn and rs
Set cnDB = New ADODB.Connection
Set rsDB = New ADODB.Recordset
Set rsBkInfo = New ADODB.Recordset
Set rsDiscon = New ADODB.Recordset

Dim Series As String
Dim AccountNum As String
Dim AccountName As String
Dim ADDRESS As String
Dim Serial As String
Dim rcdate As String
Dim Fileid As Long
Dim DayNo As Integer
Dim Zone As String
Dim strZone As String
Dim rCount As Integer

'08-08-2013 Validation added variables jums
Dim TempVal As String
Dim Val_Check As ADODB.Recordset
Dim Val_Tag As String
Dim Counter As Integer

Dim TempAccountNum As String

Dim GenFileIDRec As ADODB.Recordset
Dim GenFileIDQuery As String
Dim GenFileID As String

' Open connections and recordsets
'cnTxt.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & ";Extended Properties='text;HDR=No;FMT=Delimited'"
cnTxt.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & ";Extended Properties='text;HDR=No;FMT=Delimited'"
cnTxt.Open

'cnDB.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\mrmshq.mdb"
cnDB.ConnectionString = "Provider=SQLOLEDB.1; Data Source=" & g_DataSource & ";database=" & g_Database & "; Initial Catalog=;User Id=sa;Password=sqladmin;"
cnDB.Open


Set fs = CreateObject("Scripting.FileSystemObject")
Set f = fs.GetFolder(filePath)
Set fc = f.Files

If fc.Count = 0 Then
    Unload frmProg
    MsgBox "No data to load, please check the directory.", vbExclamation, "Disconnection Download"
    Exit Function
End If

If Not fs.FolderExists(f & "\current") Then
    Unload frmProg
    MsgBox "Current archive folder does not exist!", vbExclamation, "Disconnection Download"
    Exit Function
End If

For Each f1 In fc
    ' start of transaction for rollback if error encountered
    cnDB.BeginTrans

    s = f1.Name
    rcdate = "20" & Mid(s, 11, 2) & Mid(s, 7, 2) & Mid(s, 9, 2)
    DayNo = Mid(s, 9, 2)
    bValidateOK = True
    'MsgBox s
    FileFullPath = filePath & "\" & s
    CurrentDnldFilePath = filePath & "\current\" & s
    Set fso = CreateObject("Scripting.FileSystemObject")

    If Not (fso.FileExists(FileFullPath)) Then
        MsgBox "File does not exist!", vbInformation, "System Message"
        LoadReconToHQ = False
        Exit Function
    End If

    ' copy to local download.txt file
    fso.CopyFile FileFullPath, App.Path & "\rcdownload.txt"

    ' delete " (quotation mark) from data of download.txt
    Set a = fs.OpenTextFile(App.Path & "\rcdownload.txt", ForReading, TristateUseDefault)
    d = a.ReadAll
    Set a = fs.OpenTextFile(App.Path & "\rcdownload.txt", ForWriting, TristateUseDefault)
    a.Write Replace(d, """", "")
    a.Close
    
    DBExecute "INSERT INTO T_FILES(FILENAME,PROCESSDATE,LINECOUNT) VALUES ('" & s & "','" & Now() & "'," & rCount & ")", "", ""
    If OpenRecordset(rsFile, "SELECT TOP 1 FILENAMEID FROM T_FILES WHERE FILENAME = '" & s & "' ORDER BY PROCESSDATE DESC", "", "") = True Then
        Fileid = rsFile.Fields("FILENAMEID")
    End If
    
    'old code 08-08-2013 jums
    'rsDiscon.Open "SELECT * FROM T_RECDOWNLOAD", cnDB, adOpenDynamic, adLockOptimistic
    
    'new code 08-08-2013 change the table to t_recdownload_raw for duplicate checking jums
    Counter = 0
    rsDiscon.Open "SELECT * FROM T_RECDOWNLOAD_RAW", cnDB, adOpenDynamic, adLockOptimistic
    
    ' Check if this batch was already loaded
    rstxt.Open "SELECT * from rcdownload.txt order by AccountNum", cnTxt, adOpenStatic, adLockOptimistic, adCmdText
    rstxt.MoveFirst
    Do While Not rstxt.EOF
        
        AccountNum = rstxt.Fields("AccountNum")
        Zone = Mid(AccountNum, 1, 2)
        AccountName = rstxt.Fields("AcctName")
        ADDRESS = rstxt.Fields("Address")
        Serial = rstxt.Fields("SerialNo")
        strZone = "SELECT DMZCODE_ID FROM dbo.L_DMZZONE WHERE DMZZONE_ID = '" & Zone & "'"
            If OpenRecordset(rsZone, strZone, "", "") Then
                Series = rsZone.Fields("DMZCODE_ID") & Mid(AccountNum, 1, 3)
            End If

'old code 08-15-2013 jums
'        TempVal = "SELECT acctnum FROM T_RECDOWNLOAD where acctnum = '" & AccountNum & "' and cycle = '" & cycle & "'"
'        If OpenRecordset(Val_Check, TempVal, "", "") = True Then
'            Val_Tag = 1
'            If Val_Tag = 1 Then
'                Counter = Counter + 1
'            End If
'        Else
'            Val_Tag = 0
'        End If

        TempVal = "SELECT acctnum FROM T_RECDOWNLOAD where acctnum = '" & AccountNum & "' and cycle = '" & cycle & "'"
        If OpenRecordset(Val_Check, TempVal, "", "") = True Then
            Val_Tag = 1
            Counter = Counter + 1
        Else
            If AccountNum = TempAccountNum Then
                Val_Tag = 1
                Counter = Counter + 1
            Else
                Val_Tag = 0
            End If
        End If
        
    TempAccountNum = AccountNum
        
        '08-08-2013 similar column with T_RECDOWNLOAD without the RCID and with Val Tag jums
                With rsDiscon
                    .AddNew
                    .Fields("CYCLE") = cycle
                    .Fields("SERIES") = Series
                    .Fields("ACCTNUM") = AccountNum
                    .Fields("ACCTNAME") = AccountName
                    .Fields("ADDRESS") = ADDRESS
                    .Fields("SERIALNO") = Serial
                    .Fields("RCDATE") = rcdate
                    .Fields("DAYNO") = DayNo
                    .Fields("FILEID") = Fileid
                    .Fields("VAL_TAG") = Val_Tag
                    .Update
                End With
                
        rCount = rCount + 1
        rstxt.MoveNext
    Loop
    
'DBExecute "UPDATE T_FILES SET LINECOUNT = " & rCount & " WHERE FILENAMEID = " & fileID, "", "" 'old code

If Counter > 0 Then
    DBExecute "UPDATE T_FILES SET LINECOUNT = " & rCount - Counter & " WHERE FILENAMEID = " & Fileid, "", ""
    'MsgBox "A total of " & Counter & " are duplicates"
        GenFileIDQuery = "Select [filename] from t_files where Filenameid = '" & Fileid & "'"
        If OpenRecordset(GenFileIDRec, GenFileIDQuery, "", "") = True Then
            GenFileID = GenFileIDRec.Fields("Filename")
            'MsgBox "A total of " & Counter & " are duplicates for " & GenFileID
            MsgBox GenFileID & " has a total of " & Counter & " duplicates " & vbCrLf & vbCrLf & "     Report Generated: See Error Log Folder     "
        End If
Else
    DBExecute "UPDATE T_FILES SET LINECOUNT = " & rCount & " WHERE FILENAMEID = " & Fileid, "", ""
End If

'ExecSql = "exec sp_INSERT_RECDOWNLOAD " & Fileid & ""
'DBExecute ExecSql, "", ""

rstxt.Close
rsDiscon.Close
cnDB.CommitTrans

If Counter > 0 Then
    If GenReport(Fileid) = True Then
        MsgBox "Duplicate File Report Generated"
    End If
End If
'
Set rsDiscon = Nothing
Set cnDB = Nothing

LoadReconToHQ = True
fso.MoveFile FileFullPath, CurrentDnldFilePath

Next

ExecSql = "exec sp_INSERT_RECDOWNLOAD " & cycle & "," & DayNo & ""
DBExecute ExecSql, "", ""

' Move file to current in download source folder
'fso.MoveFile FileFullPath, CurrentDnldFilePath 'old code 08-16-2013 jums
errhandler:
If Err.Number <> 0 Then
    MsgBox "Error downloading file '" & s & "'." & vbCrLf & vbCrLf & Err.Description
    xlApp.Workbooks.Close
    Unload frmProg
    cnDB.RollbackTrans
    
End If
End Function

Public Function LoadHostToDB(filePath As String, cycle As Integer) As Boolean
    ' Function that reads source file from MWSI (Central) and loads it into the MRMS HQ T_DOWNLOAD
    '   and other related tables

    ' Connection and recordset for the text and Access link
    Dim cnTxt As Connection, cnDB As Connection
    Dim rstxt As Recordset, rsDB As Recordset
    Dim rsBkInfo As Recordset

    ' use to copy the source file to "download.txt"
    Dim f As Object, fso As Object

    ' use to delete " (quotation mark) from data of download.txt
    Const ForReading = 1, ForWriting = 2, ForAppending = 3
    Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0
    Dim a, ds, d

    ' counts to insert into T_BKINFO
    Dim totalrecs As Integer
    Dim activecnt As Integer
    Dim inactivecnt As Integer

    ' local variables
    Dim convdate As String
    Dim PDate As String
    Dim l_str_Sql As String
    Dim OldBookNo As String, NewBookNo As String
    Dim fs, BookList$, ans%
    Dim bValidateOK As Boolean
    Dim FileFullPath As String, CurrentDnldFilePath As String
    Dim progcnt As Double
    Dim Fileid As Long
    Dim rCount As Integer
    
    Dim rsFile As ADODB.Recordset


'On Error GoTo errhandler

' Text file cn and rs
Set cnTxt = New ADODB.Connection
Set cnDBBackup = New ADODB.Connection
Set rstxt = New ADODB.Recordset
Set rsFile = New ADODB.Recordset

' Local DB cn and rs
Set cnDB = New ADODB.Connection
Set rsDB = New ADODB.Recordset
Set rsBkInfo = New ADODB.Recordset


cnTxt.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & ";Extended Properties='text;HDR=No;FMT=Delimited'"
cnTxt.Open


cnDB.ConnectionString = "Provider=SQLOLEDB.1; Data Source=" & g_DataSource & ";database=" & g_Database & "; Initial Catalog=;User Id=sa;Password=sqladmin;"
cnDB.Open



Set fs = CreateObject("Scripting.FileSystemObject")
Set f = fs.GetFolder(filePath)
Set fc = f.Files
If fc.Count = 0 Then
    Unload frmProg
    MsgBox "No data to load, please check the directory.", vbExclamation, "Download from MWSI"
    Exit Function
End If
If Not fs.FolderExists(f & "\current") Then
    Unload frmProg
    MsgBox "Current archive folder does not exist!", vbExclamation, "Download from MWSI"
    Exit Function
End If

For Each f1 In fc
    ' start of transaction for rollback if error encountered
    cnDB.BeginTrans

    s = f1.Name
    bValidateOK = True
    'MsgBox s
    FileFullPath = filePath & "\" & s
    CurrentDnldFilePath = filePath & "\current\" & s
    Set fso = CreateObject("Scripting.FileSystemObject")

    If Not (fso.FileExists(FileFullPath)) Then
        MsgBox "File does not exist!", vbInformation, "System Message"
        LoadHostToDB = False
        Exit Function
    End If

    ' copy to local download.txt file
    fso.CopyFile FileFullPath, App.Path & "\download.txt"

    ' delete " (quotation mark) from data of download.txt
    Set a = fs.OpenTextFile(App.Path & "\download.txt", ForReading, TristateUseDefault)
    d = a.ReadAll
    Set a = fs.OpenTextFile(App.Path & "\download.txt", ForWriting, TristateUseDefault)
    a.Write Replace(d, """", "")
    a.Close

    ' Check if this batch was already loaded
    rstxt.Open "SELECT DISTINCT(Series) from download.txt", cnTxt, adOpenStatic, adLockOptimistic, adCmdText

    BookList$ = ""
    rstxt.MoveFirst
    Do While Not rstxt.EOF
        
        BookList$ = "'" & Trim(rstxt.Fields(0)) & "'," & BookList$
        rstxt.MoveNext
    Loop

    'Remove last comma
    BookList$ = Mid$(BookList$, 1, Len(BookList$) - 1)

    rsDB.Open "SELECT * FROM T_BOOK WHERE BOOKNO IN (" & BookList$ & ") AND CYCLE =" & str$(cycle), _
          cnDB, adOpenStatic, adLockReadOnly

    ' Book must have been scheduled otherwise error!
    If rsDB.RecordCount = 0 Then
        Unload frmProg
        MsgBox "MRU " & BookList$ & " has not been scheduled!  Schedule MRU first.", vbExclamation, "MRU Unscheduled"
        LoadHostToDB = False
        rsDB.Close
        Exit Function
    End If
    ' End Check Schedule

    rstxt.Close
    rsDB.Close

    l_str_Sql = "SELECT * FROM T_BOOK WHERE BOOKNO IN (" & BookList$ & ") AND CYCLE =" & str$(cycle) & _
            " AND ACTUAL_DL_DT IS NOT Null"

    ' Check if this MRU has already been downloaded / uploaded before by looking at ACTUAL_DL_DT column
    '  in T_BOOK.  If yes, confirm delete of previous one before downloading the new one!
    OldBookNo = Strfilter(BookList$, "0123456789")
    If OpenRecordset(g_rs_TBOOK, l_str_Sql, "MDIMain", "LoadHostToDB") Then
        ans% = MsgBox("MRU " & OldBookNo & " has already been downloaded into the database. " & vbCrLf & _
                      "If this is a resend of the MRU, all existing downloaded and uploaded " & vbCrLf & _
                      "MRU data (if any) for the current cycle will be deleted." & vbCrLf & vbCrLf & _
                      "Do you really want to redownload this MRU?", vbYesNo, "Confirm Resend of MRU")
        If ans% = vbYes Then
            Rollback_Upload OldBookNo, cycle
            Rollback_Download OldBookNo, cycle
        Else
             GoTo Continue_Process
        End If
    End If
    
    
    ' check data of downloadfile
   ' If CheckDataIntegrity() Then
    If DownloadValidation = False Then
               LoadHostToDB = False
               Unload frmProg
               Exit Function
    End If
    

        rstxt.Open "SELECT * FROM download.txt ORDER BY SequenceNo", cnTxt, adOpenStatic, adLockOptimistic, adCmdText
    
        ' Open tables T_DOWNLOAD and T_BKINFO for initialization
 
        rsDB.Open "SELECT * FROM T_DOWNLOAD", cnDB, adOpenDynamic, adLockOptimistic
        rsBkInfo.Open "SELECT * FROM T_BKINFO", cnDB, adOpenDynamic, adLockOptimistic
        OldBookNo = ""
        NewBookNo = ""
    
        totalrecs = 0
        activecnt = 0
        inactivecnt = 0
    
        ' Prevent Division by Zero if only one record in MRU
        If rstxt.RecordCount = 1 Then
            progcnt = 100
        Else
            progcnt = 1 / (rstxt.RecordCount - 1) * frmProg.Shape2.Width '100
        End If
        
        
        'INSERT INTO T_FILES AND GET FILEID
        rCount = 0
        DBExecute "INSERT INTO T_FILES(FILENAME,PROCESSDATE,LINECOUNT) VALUES ('" & s & "','" & Now() & "'," & rCount & ")", "", ""
            If OpenRecordset(rsFile, "SELECT TOP 1 FILENAMEID FROM T_FILES WHERE FILENAME = '" & s & "' ORDER BY PROCESSDATE DESC", "", "") = True Then
                Fileid = rsFile.Fields("FILENAMEID")
            End If
            
            
    
        ' Load into T_DOWNLOAD
        Do While Not rstxt.EOF
    
            Do While IsNull(rstxt.Fields("Series")) = True
                rstxt.MoveNext
            Loop
    
            NewBookNo = rstxt.Fields("Series")
    
            ' Set a Control Break on change of Book Number and write to T_BOOK and T_BKINFO
            If OldBookNo <> NewBookNo Then
                ' Reset Counters only after First Control Break!
                If OldBookNo <> "" Then
                 ' Update ACTUAL_DL_DT on T_BOOK
                 'l_str_Sql = "UPDATE T_BOOK SET ACTUAL_DL_DT = Now() WHERE BOOKNO = '" & OldBookNo & "' AND CYCLE = " & cycle
                 l_str_Sql = "UPDATE T_BOOK SET ACTUAL_DL_DT = getdate() WHERE BOOKNO = '" & OldBookNo & "' AND CYCLE = " & cycle
                 If Not DBExecute(l_str_Sql, "LoadHosttoDB", "MRU Management") Then
                    LoadHostToDB = False
                    Unload frmProg
                    Exit Function
                 End If
                'Update Status on T_SCHED
                l_str_Sql = "UPDATE T_SCHED SET BKSTCODE = 'DLM' WHERE BOOKNO = '" & OldBookNo & "' AND CYCLE = " & cycle
                If Not DBExecute(l_str_Sql, "LoadHosttoDB", "MRU Management") Then
                   LoadHostToDB = False
                   Unload frmProg
                   Exit Function
                End If
                ' Load into T_BKINFO
                With rsBkInfo
                    .AddNew
                    .Fields("BOOKNO") = OldBookNo
                    .Fields("CYCLE") = cycle
                    .Fields("ACCTS") = totalrecs
                    .Fields("ACTIVE") = activecnt
                    .Fields("INACTIVE") = inactivecnt
                    .Fields("UNREAD") = totalrecs
                    .Fields("READCNT") = 0
                    .Fields("OORCNT") = 0
                    .Update
                End With
                

                  totalrecs = 0
                  activecnt = 0
                  inactivecnt = 0
                End If
    
                OldBookNo = NewBookNo
            End If
            ' End Control Break
    
            totalrecs = totalrecs + 1
            frmProg.ProgressBar progcnt, NewBookNo
    
            Dim PeriodStart As Date
            Dim PeriodEnd As Date
            
            PeriodStart = CDate(Mid(rstxt.Fields("PERIOD"), 1, 8))
            PeriodEnd = CDate(Mid(rstxt.Fields("PERIOD"), 10, 8))

            
            With rsDB
                .AddNew
                    .Fields("SERIESNO") = NewBookNo
                    .Fields("ACCTNUM") = rstxt.Fields("AccountNum")
                    .Fields("ACCTNAME") = rstxt.Fields("CUSTNAME")
                    .Fields("ADDRESS") = rstxt.Fields("CustAddress")
                    .Fields("SERIALNO") = IIf(IsNull(rstxt.Fields("MeterSerial")) = True, "0", rstxt.Fields("MeterSerial"))
                    .Fields("SEQNO") = rstxt.Fields("SequenceNo")
                    .Fields("CONSUMERTYPEID") = rstxt.Fields("ConsumerType")
                    .Fields("STATUS") = rstxt.Fields("Status")
                    .Fields("PREVRDG") = rstxt.Fields("PrevReading")
                    .Fields("AVERAGE") = rstxt.Fields("Avecons")
                    .Fields("BILL_DATE") = Year(CDate(rstxt.Fields("BillDate"))) & Padl(Month(CDate(rstxt.Fields("BillDate"))), 2, "0") & Padl(Day(CDate(rstxt.Fields("BillDate"))), 2, "0")
                    .Fields("DUE_DATE") = Year(CDate(rstxt.Fields("DueDate"))) & Padl(Month(CDate(rstxt.Fields("DueDate"))), 2, "0") & Padl(Day(CDate(rstxt.Fields("DueDate"))), 2, "0")
                    .Fields("BILL_MONTH") = Padl(cycle, 2, "0") 'updated by Jums 06-21-2011 to Pad 0
                    .Fields("PERIOD_START") = PeriodStart
                    .Fields("PERIOD_END") = PeriodEnd
                    .Fields("ARREARS") = rstxt.Fields("Arrears")
                    .Fields("OTHERS") = rstxt.Fields("Others")
                    .Fields("DISCDATE") = Year(CDate(rstxt.Fields("DisconDate"))) & Padl(Month(CDate(rstxt.Fields("DisconDate"))), 2, "0") & Padl(Day(CDate(rstxt.Fields("DisconDate"))), 2, "0")
                    .Fields("DISCTAG") = rstxt.Fields("DisconTag")
                    .Fields("MULTIPLIER") = rstxt.Fields("Multiplier")
                    .Fields("CHECK_DIGIT") = rstxt.Fields("CheckDigit")
                    .Fields("DISCOUNT_TAG") = rstxt.Fields("DiscountTag")
                    .Fields("DISC_PERCENT") = rstxt.Fields("Percent")
                    .Fields("GOVT_TAG") = rstxt.Fields("GovtFlag")
                    .Fields("FINAL_RDG") = rstxt.Fields("FinalRdg")
                    .Fields("REPL_DATE") = rstxt.Fields("ReplaceDate")
                    .Fields("INIT_RDG") = rstxt.Fields("NewMtrInitRdg")
                    .Fields("NEWMTRCONS") = rstxt.Fields("NewMtrCons")
                    .Fields("PRES_CONS_TAG") = rstxt.Fields("PresConsTag")
                    .Fields("FAX_EMAIL") = rstxt.Fields("FaxEmail")
                    .Fields("MASTERTAG") = rstxt.Fields("MasterTag")
                    .Fields("FILEID") = Fileid
                'Additional variable prevcons by Will Feb 18 2013
                    .Fields("PREVCONS") = IIf(IsNull(rstxt.Fields("PreviousCons")) = True, Null, rstxt.Fields("PreviousCons"))
                    .Fields("PREVCONS2") = IIf(IsNull(rstxt.Fields("PreviousCons2")) = True, Null, rstxt.Fields("PreviousCons2"))
                    .Fields("PREVCONS3") = IIf(IsNull(rstxt.Fields("PreviousCons3")) = True, Null, rstxt.Fields("PreviousCons3"))
                'Additional variable for VAT pacruz 050918
                    .Fields("VAT_CODE") = IIf(IsNull(rstxt.Fields("VatCode")) = True, Null, rstxt.Fields("VatCode"))
                .Update
            End With
    
            If rstxt.Fields("Status") = "AC" Then activecnt = activecnt + 1
            If rstxt.Fields("Status") = "DC" Then inactivecnt = inactivecnt + 1
            rCount = rCount + 1
            rstxt.MoveNext
            
           
    Loop
    
    DBExecute "UPDATE T_FILES SET LINECOUNT = " & rCount & " WHERE FILENAMEID = " & Fileid, "", ""

            'Last Control Break
            ' Update ACTUAL_DL_DT on T_BOOK
            l_str_Sql = "UPDATE T_BOOK SET ACTUAL_DL_DT = getdate() WHERE BOOKNO = '" & OldBookNo & "' AND CYCLE = " & cycle
            If Not DBExecute(l_str_Sql, "LoadHosttoDB", "MRU Management") Then
               LoadHostToDB = False
               Unload frmProg
               Exit Function
            End If

            'Update Status on T_SCHED
            l_str_Sql = "UPDATE T_SCHED SET BKSTCODE = 'DLM' WHERE BOOKNO = '" & OldBookNo & "' AND CYCLE = " & cycle
            If Not DBExecute(l_str_Sql, "LoadHosttoDB", "MRU Management") Then
               LoadHostToDB = False
               Unload frmProg
               Exit Function
            End If
        
            ' Load into T_BKINFO
            With rsBkInfo
                .AddNew
                .Fields("BOOKNO") = OldBookNo
                .Fields("CYCLE") = cycle
                .Fields("ACCTS") = totalrecs
                .Fields("ACTIVE") = activecnt
                .Fields("INACTIVE") = inactivecnt
                .Fields("UNREAD") = totalrecs
                .Fields("READCNT") = 0
                .Fields("OORCNT") = 0
                .Update
            End With
            

        ' End Last Control Break
    
        ' Close all recordsets for the next file
        rstxt.Close
    
  '  End If
    
    rsDB.Close
    rsBkInfo.Close


    ' Move file to current in download source folder
    fso.MoveFile FileFullPath, CurrentDnldFilePath

Continue_Process:

   ' commit transaction
    cnDB.CommitTrans
Next

'Clean up
cnTxt.Close
cnDB.Close



Set rstxt = Nothing
Set cnTxt = Nothing

Set rsDB = Nothing
Set rsBkInfo = Nothing
Set cnDB = Nothing

Unload frmProg
LoadHostToDB = True

errhandler:

If Err.Number <> 0 Then
    MsgBox "Error downloading MRU " & OldBookNo & "." & vbCrLf & vbCrLf & Err.Description
    Unload frmProg
    If Err.Number <> -2147217887 Then
        MsgBox "Error downloading MRU " & OldBookNo & "." & vbCrLf & vbCrLf & Err.Description, vbCritical, "Download Batch From Host"
        ' rollback loaded mru with error
        cnDB.RollbackTrans
    Else
        'MsgBox "Duplicated Document Number " & rstxt.Fields("DocumentNo") & " for MRU " & OldBookNo, vbCritical, "Download Batch From Host"
    End If


End If
End Function

Public Function GenReport(Fileid As Long) As Boolean
Dim cn As Connection, rs As Recordset
Dim cnt As Integer
Dim dir As String
Dim sPath As String
Dim uldir As String
Dim fs As Object
Dim a
Dim Delim As String

Dim GenRec As ADODB.Recordset
Dim GenCycle As String
Dim GenAcctnum As String
Dim GenSERIALNO As String
Dim GenRCDATE As String
Dim GenFileID As Long

Dim GenFilenameRec As ADODB.Recordset
Dim GenFilenameQuery As String
Dim GenFilename As String

Dim date1 As String

On Error GoTo errhandler

Set cn = CreateObject("ADODB.Connection")
Set rs = CreateObject("ADODB.recordset")
Set GenRec = New ADODB.Recordset
Set f = CreateObject("Scripting.FileSystemObject")


Delim = "|"

uldir = "select DIRDNLD from dbo.S_PARAM"
OpenRecordset g_dir, uldir, "", ""
g_dir.MoveFirst
sPath = Mid(g_dir.Fields(0), 1, 10) & ""

If Not f.FolderExists(sPath) Then f.CreateFolder (sPath) ' upload current directory
If Not f.FolderExists(sPath & "\" & "ErrorLog") Then f.CreateFolder (sPath & "\" & "ErrorLog") ' hq upload directory
dir = sPath & "\" & "ErrorLog" & "\" & Format(Date, "dd") & "\"
If Not f.FolderExists(dir) Then f.CreateFolder (dir)

cn.ConnectionString = "Provider=SQLOLEDB.1; Data Source=" & g_DataSource & ";database=" & g_Database & "; Initial Catalog=;User Id=sa;Password=sqladmin;"
cn.Open

rs.Open "Select CYCLE,ACCTNUM,SERIALNO,RCDATE,FILEID from T_RecDownload_Raw where val_tag = 1 and fileid = '" & Fileid & "'", cn, adOpenStatic, adLockReadOnly '08-16-2013 jums

'rs.Open "Select CYCLE,ACCTNUM,SERIALNO,RCDATE,FILEID from T_RecDownload_Raw where val_tag = 1 order by fileid ", cn, adOpenStatic, adLockReadOnly

If rs.RecordCount < 0 Then
    MsgBox "No Duplicates Found", vbInformation, "System Error"
    rs.Close
    cn.Close
    GenReport = False
    Exit Function
Else
'old code 08-16-2013 jums
    GenFilenameQuery = "Select [filename] from t_files where Filenameid = '" & Fileid & "'"
    If OpenRecordset(GenFilenameRec, GenFilenameQuery, "", "") Then
        GenFilename = GenFilenameRec.Fields("Filename")
    End If
End If

Set fs = CreateObject("Scripting.FileSystemObject")


sPath = dir & "\" & "Duplicates for " & GenFilename '& ".txt" 'old code 08-16-2013 jums

date1 = Format(Date, "mmddyyyy")

'sPath = dir & "\" & "Duplicates for " & Date1 & " Records " & TotalCount & ".txt"
Open sPath For Output As #1  '--open txtfile for data insert

Do While Not rs.EOF

GenCycle = rs.Fields("CYCLE").Value
GenAcctnum = rs.Fields("ACCTNUM").Value
GenSERIALNO = rs.Fields("SERIALNO").Value
GenRCDATE = rs.Fields("RCDATE").Value
GenFileID = rs.Fields("FILEID").Value
'
'GenFilenameQuery = "Select [filename] from t_files where Filenameid = '" & GenFileID & "'"
'If OpenRecordset(GenFilenameRec, GenFilenameQuery, "", "") Then
'    GenFilename = GenFilenameRec.Fields("Filename")
'End If

Print #1, GenCycle & Delim & GenAcctnum & Delim & GenSERIALNO & Delim & GenRCDATE '& Delim & GenFilename

rs.MoveNext
Loop

rs.Close
Close #1
cn.Close

Set rs = Nothing
Set cn = Nothing

GenReport = True

errhandler:
    If Err.Number <> 0 Then
        MsgBox Err.Description, , "Error"
    End If

End Function
Public Function DownloadValidation() As Boolean
    Dim disconTag As Integer
    Dim discountTag As Integer
    Dim repDate As String
    Dim arrears As String
    Dim NMC As String
    Dim initRdg As String
    Dim finalRdg As String
    Dim presconTag As String
    Dim consType As String
    Dim rstxt As Recordset, rsDB As Recordset
    Dim ErrorString As String
 
    ' Text file cn and rs
    Set cnTxt = New ADODB.Connection
    Set rstxt = New ADODB.Recordset
    
    ' Open connections and recordsets
    cnTxt.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & ";Extended Properties='text;HDR=No;FMT=Delimited'"
    cnTxt.Open

    numErrors = 0
    

    
      rstxt.Open "SELECT * FROM download.txt", cnTxt, adOpenStatic, adLockOptimistic, adCmdText
        Do While Not rstxt.EOF
            discountTag = rstxt.Fields("DiscountTag")
            If IsNull(rstxt.Fields("ReplaceDate")) = False Then
                repDate = rstxt.Fields("ReplaceDate")
            End If
            disconTag = rstxt.Fields("DisconTag")
            arrears = rstxt.Fields("Arrears")
            If IsNull(rstxt.Fields("NewMtrCons")) = False Then
                NMC = rstxt.Fields("NewMtrCons")
            End If
            If IsNull(rstxt.Fields("NewMtrInitRdg")) = False Then
                initRdg = rstxt.Fields("NewMtrInitRdg")
            End If
            If IsNull(rstxt.Fields("FinalRdg")) = False Then
                finalRdg = rstxt.Fields("FinalRdg")
            End If
            If IsNull(rstxt.Fields("PresConsTag")) = False Then
                presconTag = rstxt.Fields("PresConsTag")
            End If
            consType = rstxt.Fields("ConsumerType")
        rstxt.MoveNext
        Loop
      rstxt.Close
      
      If disconTag = 1 And Len(arrears) = 0 Then
        numErrors = numErrors + 1
        ErrorString = ErrorString & " Error: Account for Disconnection " & vbCrLf
      End If
      
      If Len(repDate) > 0 And Len(NMC) = 0 Then
        numErrors = numerros + 1
        ErrorString = ErrorString & " Error: New Meter " & vbCrLf
      End If
      
      If Len(repDate) > 0 And Len(initRdg) = 0 Then
        numErrors = numerros + 1
        ErrorString = ErrorString & " Error: New Meter " & vbCrLf
      End If
      
      'If Len(repDate) > 0 And Len(NMC) = 0 And Len(finalRdg) = 0 Then
      If Len(repDate) > 0 And Len(finalRdg) = 0 Then
        numErrors = numerros + 1
        ErrorString = ErrorString & " Error: New Meter " & vbCrLf
      End If
      
      If Len(repDate) > 0 And Len(initRdg) = 0 Then
        numErrors = numerros + 1
        ErrorString = ErrorString & " Error: New Meter " & vbCrLf
      End If
      
      If Len(repDate) > 0 And Len(presconTag) > 0 Then
        numErrors = numerros + 1
        ErrorString = ErrorString & " Error: New Meter " & vbCrLf
      End If
      
      If discountTag = 1 And consType <> 1 Then
        numErrors = numerros + 1
        ErrorString = ErrorString & " Error: Bill with Senior Citizen Discount " & vbCrLf
      End If
      
          If numErrors = 0 Then
            DownloadValidation = True
          Else
            MsgBox ErrorString
            DownloadValidation = False
          End If
  
End Function

Public Function UploadToRCDB(filePath As String, cycle As Integer) As Boolean
    ' Function that reads source file from MWSI (Central) and loads it into the MRMS HQ T_DOWNLOAD
    '   and other related tables

    ' Connection and recordset for the text and Access link
    Dim cnTxt As Connection, cnDB As Connection, cnDBBackup As Connection
    Dim rstxt As Recordset, rsDB As Recordset, rsVAL As Recordset, rsDBRC As Recordset
    Dim rsBkInfo As Recordset

    ' use to copy the source file to "download.txt"
    Dim f As Object, fso As Object

    ' use to delete " (quotation mark) from data of download.txt
    Const ForReading = 1, ForWriting = 2, ForAppending = 3
    Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0
    Dim a, ds, d

    ' counts to insert into T_BKINFO
    Dim totalrecs As Integer
    Dim activecnt As Integer
    Dim inactivecnt As Integer

    ' local variables
    Dim convdate As String
    Dim PDate As String
    Dim l_str_Sql As String
    Dim OldBookNo As String, NewBookNo As String
    Dim fs, BookList$, ans%
    Dim bValidateOK As Boolean
    Dim FileFullPath As String, CurrentDnldFilePath As String
    Dim progcnt As Double
    Dim dcdate As Date
    Dim PYDATE As Date
    Dim bc As String
    Dim ACCTNUM As String
'On Error GoTo errhandler

' Text file cn and rs
Set cnTxt = New ADODB.Connection
Set rstxt = New ADODB.Recordset

' Local DB cn and rs
Set cnDB = New ADODB.Connection
Set cnDBBackup = New ADODB.Connection
Set rsDB = New ADODB.Recordset
Set rsDBRC = New ADODB.Recordset
Set rsVAL = New ADODB.Recordset
Set rsBkInfo = New ADODB.Recordset

' Open connections and recordsets
'cnTxt.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & ";Extended Properties='text;HDR=No;FMT=Delimited'"
cnTxt.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & ";Extended Properties='text;HDR=No;FMT=Delimited'"
cnTxt.Open

'cnDB.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\mrmshq.mdb"
cnDB.ConnectionString = "Provider=SQLOLEDB.1; Data Source=" & g_DataSource & ";database=" & g_Database & "; Initial Catalog=;User Id=sa;Password=sqladmin;"
cnDB.Open


cnDBBackup.ConnectionString = "Provider=SQLOLEDB.1; Data Source=SZ-MWSI-SUBIC;database=" & g_DBBackup & "; Initial Catalog=;User Id=sa;Password=sqladmin;"
cnDBBackup.Open

Set fs = CreateObject("Scripting.FileSystemObject")
Set f = fs.GetFolder(filePath)
Set fc = f.Files
If fc.Count = 0 Then
    'Unload frmProg
    MsgBox "No data to load, please check the directory.", vbExclamation, ""
    Exit Function
End If
If Not fs.FolderExists(f & "\current") Then
    'Unload frmProg
    MsgBox "Current archive folder does not exist!", vbExclamation, ""
    Exit Function
End If

For Each f1 In fc
    ' start of transaction for rollback if error encountered
    cnDB.BeginTrans
    cnDBBackup.BeginTrans
    s = f1.Name
'    bValidateOK = True
    'MsgBox s
    FileFullPath = filePath & "\" & s
    CurrentDnldFilePath = filePath & "\current\" & s
    'bc = Mid(s, 5, 2) & "00"
    Set fso = CreateObject("Scripting.FileSystemObject")

    If Not (fso.FileExists(FileFullPath)) Then
        MsgBox "File does not exist!", vbInformation, "System Message"
        UploadToRCDB = False
        Exit Function
    End If

    ' copy to local download.txt file
    fso.CopyFile FileFullPath, App.Path & "\rcupload.txt"


    rstxt.Open "SELECT * FROM RCUPLOAD.txt", cnTxt, adOpenStatic, adLockOptimistic, adCmdText
    ' Open tables T_DOWNLOAD and T_BKINFO for initialization
    rsDB.Open "SELECT * FROM T_RC_UPLOAD", cnDB, adOpenDynamic, adLockOptimistic
    rsDBRC.Open "SELECT * FROM T_RC_UPLOAD", cnDBBackup, adOpenDynamic, adLockOptimistic
'    rsVAL.Open "SELECT * FROM T_DC_VAL_UPLOAD", cnDB, adOpenDynamic, adLockOptimistic
    
    totalrecs = 0
    activecnt = 0
    inactivecnt = 0



    ' Load into T_DOWNLOAD
    Do While Not rstxt.EOF


        totalrecs = totalrecs + 1
'        frmProg.ProgressBar progcnt, NewBookNo

        ACCTNUM = Padl(rstxt.Fields("AcctNum"), 12, "0") & ""
        '****************** EDITED BY JEFF ******************
        Dim rcjobid As Integer
        Dim rsjob As ADODB.Recordset
        Dim sqljob As String
        'sqljob = "SELECT RCJOBID,BC_CODE FROM T_RC_DOWNLOAD WHERE ACCTNUM = '" & ACCTNUM & "' AND CYCLE = " & cycle
        sqljob = "SELECT A.RCJOBID,A.BC_CODE FROM T_RC_DOWNLOAD A LEFT JOIN T_RC_UPLOAD B ON A.RCJOBID = B.RCJOBID AND A.ACCTNUM = B.ACCTNUM AND A.CYCLE = B.CYCLE WHERE A.ACCTNUM = '" & ACCTNUM & "' AND A.CYCLE = " & cycle & "AND B.RCJOBID IS NULL"
        
        If OpenRecordset(rsjob, sqljob, "", "") Then
        rcjobid = CInt(rsjob.Fields(0).Value)
        bc = rsjob.Fields("BC_CODE").Value
        End If
        
        With rsDB
            .AddNew
            .Fields("RCJOBID") = rcjobid
            
        '********************END OF EDIT*******************
            .Fields("CYCLE") = cycle
            .Fields("ACCTNUM") = ACCTNUM
            .Fields("BC_CODE") = bc
            rcdate = (Left(rstxt.Fields("RCDate"), 4) & "-" & Mid(rstxt.Fields("RCDate"), 5, 2) & "-" & Right(rstxt.Fields("RCDate"), 2))
            .Fields("RCDATE") = IIf(IsNull(rcdate) = True, Date, rcdate & "") '"04/23/2010" 'DCDATE '(Left(rsTxt.Fields("DT"), 4) & "-" & Mid(rsTxt.Fields("DT"), 5, 2) & "-" & Right(rsTxt.Fields("DT"), 2) & " 00:00:00.000") '2010-04-19 14:45:01.983
            .Fields("RCTIME") = rstxt.Fields("RCTime") & ""
            .Fields("STAT_CD") = rstxt.Fields("RCStatus") & ""
                If rstxt.Fields("Finalrdg") <> "" Then
                .Fields("READING") = Val(rstxt.Fields("FinalRdg") & "")
                Else
                .Fields("READING") = ""
                End If
            .Fields("SEALNO") = rstxt.Fields("SealNo") & ""
            .Fields("NDCR_REASON") = rstxt.Fields("NonRCCode") & ""
            .Fields("RESP_PERSON") = rstxt.Fields("RespPerson") & ""
            .Fields("REMARKS") = rstxt.Fields("Remarks") & ""
            .Fields("SERVICE_MAN") = rstxt.Fields("SMName") & ""
            .Update
        End With
        
        With rsDBRC
            .AddNew
            .Fields("RCJOBID") = rcjobid
            
        '********************END OF EDIT*******************
            .Fields("CYCLE") = cycle
            .Fields("ACCTNUM") = ACCTNUM
            .Fields("BC_CODE") = bc
            rcdate = (Left(rstxt.Fields("RCDate"), 4) & "-" & Mid(rstxt.Fields("RCDate"), 5, 2) & "-" & Right(rstxt.Fields("RCDate"), 2))
            .Fields("RCDATE") = IIf(IsNull(rcdate) = True, Date, rcdate & "") '"04/23/2010" 'DCDATE '(Left(rsTxt.Fields("DT"), 4) & "-" & Mid(rsTxt.Fields("DT"), 5, 2) & "-" & Right(rsTxt.Fields("DT"), 2) & " 00:00:00.000") '2010-04-19 14:45:01.983
            .Fields("RCTIME") = rstxt.Fields("RCTime") & ""
            .Fields("STAT_CD") = rstxt.Fields("RCStatus") & ""
                If rstxt.Fields("Finalrdg") <> "" Then
                .Fields("READING") = Val(rstxt.Fields("FinalRdg") & "")
                Else
                .Fields("READING") = ""
                End If
            .Fields("SEALNO") = rstxt.Fields("SealNo") & ""
            .Fields("NDCR_REASON") = rstxt.Fields("NonRCCode") & ""
            .Fields("RESP_PERSON") = rstxt.Fields("RespPerson") & ""
            .Fields("REMARKS") = rstxt.Fields("Remarks") & ""
            .Fields("SERVICE_MAN") = rstxt.Fields("SMName") & ""
            .Update
       End With
            
            If Not DBExecute("UPDATE T_CUST_MASTER SET DC_STATUS='" & rstxt.Fields("RCStatus") & "' WHERE ACCTNUM ='" & ACCTNUM & "'", "", "") Then
            End If
            
            If rstxt.Fields("RCStatus") = "S" Or rstxt.Fields("RCStatus") = "s" Then
            DBExecute "UPDATE T_CUST_MASTER SET RCDATE = '" & Now() & "' WHERE ACCTNUM = '" & ACCTNUM & "'", "", ""
                If Not DBExecute("UPDATE T_CUST_MASTER SET STATUS='CONN' WHERE  ACCTNUM ='" & ACCTNUM & "'", "", "") Then
                End If
                If Not DBExecute("UPDATE T_CUST_MASTER SET DC_STATUS='SS' WHERE  ACCTNUM ='" & ACCTNUM & "' AND ACCTNUM IN (SELECT ACCTNUM FROM T_RC_DOWNLOAD WHERE SOURCE='MAYNILAD' AND CYCLE=" & cycle & ")", "", "") Then
                End If
            End If
            
            If rstxt.Fields("NonRCCode") = "06" Then
                If Not DBExecute("UPDATE T_CUST_MASTER SET STATUS='CONN' WHERE  ACCTNUM ='" & ACCTNUM & "'", "", "") Then
                End If
            End If
            
            If rstxt.Fields("RCStatus") = "U" Or rstxt.Fields("RCStatus") = "u" Then
                If Not DBExecute("UPDATE T_CUST_MASTER SET DC_STATUS='SU' WHERE  ACCTNUM ='" & ACCTNUM & "' AND ACCTNUM IN (SELECT ACCTNUM FROM T_RC_DOWNLOAD WHERE SOURCE='MAYNILAD' AND CYCLE=" & cycle & ")", "", "") Then
                End If
            End If
            
         
        
        rstxt.MoveNext

    Loop



    ' Close all recordsets for the next file
    rstxt.Close
    rsDB.Close
    rsDBRC.Close
'    rsVAL.Close

    ' Move file to current in download source folder
    fso.MoveFile FileFullPath, CurrentDnldFilePath

Continue_Process:

   ' commit transaction
    cnDB.CommitTrans
    cnDBBackup.CommitTrans
Next

'Clean up
cnTxt.Close
cnDB.Close
cnDBBackup.Close
Set rstxt = Nothing
Set cnTxt = Nothing
Set rsDBRC = Nothing

'Set rsVAL = Nothing
Set rsDB = Nothing
Set cnDB = Nothing

'Unload frmProg
UploadToRCDB = True

errhandler:

If Err.Number <> 0 Then
    'MsgBox "Download Error." & vbCrLf & vbCrLf & Err.Description
    'Unload frmProg
    If Err.Number <> -2147217887 Then
        MsgBox "Download Error." & vbCrLf & vbCrLf & Err.Description, vbCritical, ""
        ' rollback loaded data with error
        cnDB.RollbackTrans
    End If

End If
End Function


Public Function UploadDiscon(filePath As String) As Boolean
    
    Dim cnTxt As Connection, cnDB As Connection
    Dim rstxt As Recordset, rsDB As Recordset, rsVAL As Recordset
    Dim rsUpload As ADODB.Recordset
    Dim rsBkInfo As Recordset
    Dim f As Object, fso As Object
    Dim rsDC As ADODB.Recordset
    
    Dim AccountNum As String
    Dim SerialNo As String
    Dim DisconDate As String
    Dim DisconTime As String
    Dim DCStat As String
    Dim finalRdg As String
    Dim RespPerson As String
    Dim NonDC_Code As String
    Dim REMARKS As String
    Dim ServiceMan As String
    'Dim DCJobID As Integer 'oldcode 01-30-2013 jums
    Dim DCJobID As Long
    'Dim fileID As Integer 'oldcode 01-30-2013 jums
    Dim Fileid As Long
    Dim cycle As Integer
    Dim Series As String
    'Additional variables for STATUS at T_DISCDOWNLOAD by William 2011-05-18
    Dim status1 As String
    Dim sqlstrclosedt As String
    Set rsDC = New ADODB.Recordset
    Set rsUpload = New ADODB.Recordset
    Set cnTxt = New ADODB.Connection
    Set cnDB = New ADODB.Connection
    Set rstxt = New ADODB.Recordset
    
    Set fs = CreateObject("Scripting.FileSystemObject")
    Set f = fs.GetFolder(filePath)
    Set fc = f.Files
    If fc.Count = 0 Then
        MsgBox "No data to load, please check the directory.", vbExclamation, ""
        Exit Function
    End If
    If Not fs.FolderExists(f & "\FRSO\current") Then
        MsgBox "Current archive folder does not exist!", vbExclamation, ""
        Exit Function
    End If
    
    Set rsDB = New ADODB.Recordset
    cnTxt.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & ";Extended Properties='text;HDR=No;FMT=Delimited'"
    cnTxt.Open

    cnDB.ConnectionString = "Provider=SQLOLEDB.1; Data Source=" & g_DataSource & ";database=" & g_Database & "; Initial Catalog=;User Id=sa;Password=sqladmin;"
    cnDB.Open
    
    For Each f1 In fc
    ' start of transaction for rollback if error encountered
    cnDB.BeginTrans

    s = f1.Name
    FileFullPath = filePath & "\" & s
    CurrentDnldFilePath = filePath & "\FRSO\current\" & s
    'bc = Mid(s, 5, 2) & "00"
    Set fso = CreateObject("Scripting.FileSystemObject")

    If Not (fso.FileExists(FileFullPath)) Then
        MsgBox "File does not exist!", vbInformation, "System Message"
        UploadDiscon = False
        Exit Function
    End If

    ' copy to local download.txt file
    fso.CopyFile FileFullPath, App.Path & "\dcupload.txt"
    rsUpload.Open "SELECT * FROM T_DISCUPLOAD", cnDB, adOpenDynamic, adLockOptimistic
    rstxt.Open "SELECT * FROM DCUPLOAD.txt", cnTxt, adOpenStatic, adLockOptimistic, adCmdText

 
   Do While Not rstxt.EOF
      AccountNum = rstxt.Fields("AccountNum")
      SerialNo = rstxt.Fields("SerialNo")
      DisconDate = rstxt.Fields("DisconDate")
      DisconTime = rstxt.Fields("DisconTime")
      DCStat = rstxt.Fields("DCStat")
        If IsNull(rstxt.Fields("FinalRdg")) = False Then
          finalRdg = rstxt.Fields("FinalRdg")
        End If
      RespPerson = IIf(IsNull(rstxt.Fields("RespPerson")) = True, "", rstxt.Fields("RespPerson"))
      NonDC_Code = IIf(IsNull(rstxt.Fields("NonDCCode")) = True, "", rstxt.Fields("NonDCCode"))
      REMARKS = IIf(IsNull(rstxt.Fields("Remarks")) = True, "", rstxt.Fields("Remarks"))
      ServiceMan = rstxt.Fields("ServiceMan")
      cycle = Mid(DisconDate, 5, 2)
      status1 = "CLOSED"
      sqlstrclosedt = "update T_DISCDOWNLOAD set STATUS = 'CLOSED',CLOSEDT = getdate() where ACCTNUM = '" & AccountNum & "' and STATUS = 'OPEN' and CLOSEDT is NULL"
      If OpenRecordset(rsDC, "SELECT TOP 1 DCID,SERIES FROM T_DISCDOWNLOAD WHERE ACCTNUM = '" & AccountNum & "' ORDER BY DCID DESC", "", "") = True Then
      DCJobID = rsDC.Fields("DCID")
      Series = rsDC.Fields("SERIES")
      End If
      
                With rsUpload
                    .AddNew
                    .Fields("CYCLE") = cycle
                    .Fields("SERIES") = Series
                    .Fields("ACCTNUM") = AccountNum
                    .Fields("SERIALNO") = SerialNo
                    .Fields("DISCONDATE") = DisconDate
                    .Fields("DISCONTIME") = DisconTime
                    .Fields("DCSTAT") = DCStat
                    
                    If IsNull(rstxt.Fields("FinalRdg")) = False Then
                        .Fields("FINALRDG") = finalRdg
                    End If
                    
                    .Fields("RESP_PERSON") = RespPerson
                     .Fields("NONDCCODE") = NonDC_Code
                    .Fields("REMARKS") = REMARKS
                    .Fields("SERVICEMAN") = ServiceMan
                    .Fields("DCID") = DCJobID
                    .Fields("FILEID") = 0
                    .Update

                
                End With
    
                DBExecute sqlstrclosedt, "", ""

       rstxt.MoveNext
   Loop
    
        rstxt.Close
        rsUpload.Close
    
        ' Move file to current in download source folder
        fso.MoveFile FileFullPath, CurrentDnldFilePath
    
Continue_Process:
    
       ' commit transaction
        cnDB.CommitTrans
        
    Next
    
    'Clean up
    cnTxt.Close
    cnDB.Close
    
    Set rstxt = Nothing
    Set cnTxt = Nothing
    
    Set rsVAL = Nothing
    Set rsDB = Nothing
    Set cnDB = Nothing
    
    'Unload frmProg
     UploadDiscon = True
    
errhandler:
    
    If Err.Number <> 0 Then
        'MsgBox "Download Error." & vbCrLf & vbCrLf & Err.Description
        'Unload frmProg
        If Err.Number <> -2147217887 Then
            MsgBox "Download Error." & vbCrLf & vbCrLf & Err.Description, vbCritical, ""
            ' rollback loaded data with error
                cnDB.RollbackTrans
        End If
    
    End If
End Function

Public Function UploadRecon(filePath As String) As Boolean
    
    Dim cnTxt As Connection, cnDB As Connection
    Dim rstxt As Recordset, rsDB As Recordset, rsVAL As Recordset
    Dim rsUpload As ADODB.Recordset
    Dim rsBkInfo As Recordset
    Dim f As Object, fso As Object
    Dim rsDC As ADODB.Recordset
    'Revised by William Lawrence R Sunga changed variable name (ReconDate,ReconTime,RCStat,NonRC_Code) from Recon to Discon June 17 2011
    Dim AccountNum As String
    Dim SerialNo As String
    Dim ReconDate As String
    Dim ReconTime As String
    Dim RCStat As String
    Dim finalRdg As String
    Dim RespPerson As String
    Dim NonRC_Code As String
    Dim REMARKS As String
    Dim ServiceMan As String
    Dim DCJobID As Integer
    Dim Fileid As Long
    Dim cycle As Integer
    Dim Series As String
    
    Set rsDC = New ADODB.Recordset
    Set rsUpload = New ADODB.Recordset
    Set cnTxt = New ADODB.Connection
    Set cnDB = New ADODB.Connection
    Set rstxt = New ADODB.Recordset
    
    Set fs = CreateObject("Scripting.FileSystemObject")
    Set f = fs.GetFolder(filePath)
    Set fc = f.Files
    If fc.Count = 0 Then
        MsgBox "No data to load, please check the directory.", vbExclamation, ""
        Exit Function
    End If
    If Not fs.FolderExists(f & "\current") Then
        MsgBox "Current archive folder does not exist!", vbExclamation, ""
        Exit Function
    End If
    
    Set rsDB = New ADODB.Recordset
    cnTxt.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & ";Extended Properties='text;HDR=No;FMT=Delimited'"
    cnTxt.Open

    cnDB.ConnectionString = "Provider=SQLOLEDB.1; Data Source=" & g_DataSource & ";database=" & g_Database & "; Initial Catalog=;User Id=sa;Password=sqladmin;"
    cnDB.Open
    
    For Each f1 In fc
    ' start of transaction for rollback if error encountered
    cnDB.BeginTrans

    s = f1.Name
    FileFullPath = filePath & "\" & s
    CurrentDnldFilePath = filePath & "\current\" & s
    'bc = Mid(s, 5, 2) & "00"
    Set fso = CreateObject("Scripting.FileSystemObject")

    If Not (fso.FileExists(FileFullPath)) Then
        MsgBox "File does not exist!", vbInformation, "System Message"
        UploadRecon = False
        Exit Function
    End If

    ' copy to local download.txt file
    fso.CopyFile FileFullPath, App.Path & "\rcupload.txt"
    rsUpload.Open "SELECT * FROM T_RECUPLOAD", cnDB, adOpenDynamic, adLockOptimistic
    rstxt.Open "SELECT * FROM RCUPLOAD.txt", cnTxt, adOpenStatic, adLockOptimistic, adCmdText

 
   Do While Not rstxt.EOF
      AccountNum = rstxt.Fields("AccountNum")
      SerialNo = rstxt.Fields("SerialNo")
      ReconDate = rstxt.Fields("ReconDate")
      ReconTime = rstxt.Fields("ReconTime")
      RCStat = rstxt.Fields("RCStat")
        If IsNull(rstxt.Fields("InitialRdg")) = False Then
          finalRdg = rstxt.Fields("InitialRdg")
        Else
          finalRdg = ""
        End If
      RespPerson = IIf(IsNull(rstxt.Fields("Resp_Person")) = True, "", rstxt.Fields("Resp_Person"))
      NonRC_Code = IIf(IsNull(rstxt.Fields("NonRC_Code")) = True, "", rstxt.Fields("NonRC_Code"))
      REMARKS = IIf(IsNull(rstxt.Fields("Remarks")) = True, "", rstxt.Fields("Remarks"))
      ServiceMan = rstxt.Fields("ServiceMan")
      cycle = Mid(ReconDate, 5, 2)
      
      If OpenRecordset(rsDC, "SELECT TOP 1 RCID,SERIES FROM T_RECDOWNLOAD WHERE ACCTNUM = '" & AccountNum & "' ORDER BY RCID DESC", "", "") = True Then
      DCJobID = rsDC.Fields("RCID")
      Series = rsDC.Fields("SERIES")
      End If
      
                With rsUpload
                    .AddNew
                    .Fields("CYCLE") = cycle
                    .Fields("SERIES") = Series
                    .Fields("ACCTNUM") = AccountNum
                    .Fields("SERIALNO") = SerialNo
                    .Fields("RECONDATE") = ReconDate
                    .Fields("RECONTIME") = ReconTime
                    .Fields("RCSTAT") = RCStat
                    If IsNull(rstxt.Fields("InitialRdg")) = False Then
                    .Fields("INITIALRDG") = finalRdg
                    End If
                    .Fields("RESP_PERSON") = RespPerson
                    .Fields("NONRC_CODE") = NonRC_Code
                    .Fields("REMARKS") = REMARKS
                    .Fields("SERVICEMAN") = ServiceMan
                    .Fields("RCID") = DCJobID
                    .Fields("FILEID") = 0
                    .Update
                End With


       rstxt.MoveNext
   Loop
    
        rstxt.Close
        rsUpload.Close
    
        ' Move file to current in download source folder
        fso.MoveFile FileFullPath, CurrentDnldFilePath
    
Continue_Process:
    
       ' commit transaction
        cnDB.CommitTrans
    Next
    
    'Clean up
    cnTxt.Close
    cnDB.Close
    
    Set rstxt = Nothing
    Set cnTxt = Nothing
    
    Set rsVAL = Nothing
    Set rsDB = Nothing
    Set cnDB = Nothing
    
    'Unload frmProg
     UploadRecon = True
    
errhandler:
    
    If Err.Number <> 0 Then
        'MsgBox "Download Error." & vbCrLf & vbCrLf & Err.Description
        'Unload frmProg
        If Err.Number <> -2147217887 Then
            MsgBox "Download Error." & vbCrLf & vbCrLf & Err.Description, vbCritical, ""
            ' rollback loaded data with error
                cnDB.RollbackTrans
        End If
    
    End If
End Function
Public Function UploadToDCDB(filePath As String, cycle As Integer) As Boolean
    ' Function that reads source file from MWSI (Central) and loads it into the MRMS HQ T_DOWNLOAD
    '   and other related tables

    ' Connection and recordset for the text and Access link
    Dim cnTxt As Connection, cnDB As Connection
    Dim rstxt As Recordset, rsDB As Recordset, rsVAL As Recordset
    Dim rsBkInfo As Recordset

    ' use to copy the source file to "download.txt"
    Dim f As Object, fso As Object

    ' use to delete " (quotation mark) from data of download.txt
    Const ForReading = 1, ForWriting = 2, ForAppending = 3
    Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0
    Dim a, ds, d

    ' counts to insert into T_BKINFO
    Dim totalrecs As Integer
    Dim activecnt As Integer
    Dim inactivecnt As Integer

    ' local variables
    Dim convdate As String
    Dim PDate As String
    Dim l_str_Sql As String
    Dim OldBookNo As String, NewBookNo As String
    Dim fs, BookList$, ans%
    Dim bValidateOK As Boolean
    Dim FileFullPath As String, CurrentDnldFilePath As String
    Dim progcnt As Double
    Dim dcdate As Date
    Dim PYDATE As Date
    Dim bc As String
    Dim dcreason As String
'On Error GoTo errhandler

' Text file cn and rs
Set cnTxt = New ADODB.Connection
Set rstxt = New ADODB.Recordset

' Local DB cn and rs
Set cnDB = New ADODB.Connection
Set rsDB = New ADODB.Recordset
Set rsVAL = New ADODB.Recordset
Set rsBkInfo = New ADODB.Recordset
Dim SQLstring As String
Dim payAmt

' Open connections and recordsets
'cnTxt.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & ";Extended Properties='text;HDR=No;FMT=Delimited'"
cnTxt.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & ";Extended Properties='text;HDR=No;FMT=Delimited'"
cnTxt.Open

'cnDB.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\mrmshq.mdb"
cnDB.ConnectionString = "Provider=SQLOLEDB.1; Data Source=" & g_DataSource & ";database=" & g_Database & "; Initial Catalog=;User Id=sa;Password=sqladmin;"
cnDB.Open

Set fs = CreateObject("Scripting.FileSystemObject")
Set f = fs.GetFolder(filePath)
Set fc = f.Files
If fc.Count = 0 Then
    'Unload frmProg
    MsgBox "No data to load, please check the directory.", vbExclamation, ""
    Exit Function
End If
If Not fs.FolderExists(f & "\current") Then
    'Unload frmProg
    MsgBox "Current archive folder does not exist!", vbExclamation, ""
    Exit Function
End If

For Each f1 In fc
    ' start of transaction for rollback if error encountered
    cnDB.BeginTrans

    s = f1.Name
    FileFullPath = filePath & "\" & s
    CurrentDnldFilePath = filePath & "\current\" & s
    'bc = Mid(s, 5, 2) & "00"
    Set fso = CreateObject("Scripting.FileSystemObject")

    If Not (fso.FileExists(FileFullPath)) Then
        MsgBox "File does not exist!", vbInformation, "System Message"
        UploadToDCDB = False
        Exit Function
    End If

    ' copy to local download.txt file
    fso.CopyFile FileFullPath, App.Path & "\dcupload.txt"
    rstxt.Open "SELECT * FROM DCUPLOAD.txt", cnTxt, adOpenStatic, adLockOptimistic, adCmdText

   Do While Not rstxt.EOF

   Dim rsBC As ADODB.Recordset
   Set rsBC = New ADODB.Recordset
 
   If OpenRecordset(rsBC, "SELECT BC_CODE FROM T_DC_DOWNLOAD WHERE ACCTNUM = '" & Padl(rstxt.Fields("AccountNum"), 12, "0") & "'", "", "") Then
   bc = rsBC.Fields("BC_CODE").Value
   End If
   
  payAmt = IIf(IsNull(rstxt.Fields("PymtAmt")) = True, "0", rstxt.Fields("PymtAmt") & "")
  
        SQLstring = "EXEC sp_UPLOAD_TO_DCDB " & cycle & ",'" & Padl(rstxt.Fields("AccountNum"), 12, "0") & "','" & bc & "','" & rstxt.Fields("DT") & "','" & rstxt.Fields("Rdg") & "','" & rstxt.Fields("SealNo") & "','" & rstxt.Fields("ReasonCd") & "','" & rstxt.Fields("RespPerson") & "','" & rstxt.Fields("Rem") & "','" & rstxt.Fields("PymtDt") & "'," & payAmt & ",'" & rstxt.Fields("ServiceMan") & "','" & rstxt.Fields("Status") & "'"
        DBExecute SQLstring, "", ""
   rstxt.MoveNext

    Loop

    rstxt.Close


    ' Move file to current in download source folder
    fso.MoveFile FileFullPath, CurrentDnldFilePath

Continue_Process:

   ' commit transaction
    cnDB.CommitTrans
Next

'Clean up
cnTxt.Close
cnDB.Close

Set rstxt = Nothing
Set cnTxt = Nothing

Set rsVAL = Nothing
Set rsDB = Nothing
Set cnDB = Nothing

'Unload frmProg
UploadToDCDB = True

errhandler:

If Err.Number <> 0 Then
    'MsgBox "Download Error." & vbCrLf & vbCrLf & Err.Description
    'Unload frmProg
    If Err.Number <> -2147217887 Then
        MsgBox "Download Error." & vbCrLf & vbCrLf & Err.Description, vbCritical, ""
        ' rollback loaded data with error
            cnDB.RollbackTrans
    End If

End If
End Function

Function GenerateFF() As Boolean

    Dim cn As Connection, rs As Recordset
    Dim cnt As Integer
    Dim sPath As String
    Dim fs As Object
    Dim a

    Dim ffcode As String
    Dim ffdesc As String

On Error GoTo errhandler

    Set cn = CreateObject("ADODB.Connection")
    Set rs = CreateObject("ADODB.recordset")

    cn.ConnectionString = "Provider=SQLOLEDB.1; Data Source=" & g_DataSource & ";database=" & g_Database & "; Initial Catalog=;User Id=sa;Password=sqladmin;"
    cn.Open

    rs.Open "SELECT * FROM R_FF", cn, adOpenStatic, adLockReadOnly

    If rs.RecordCount < 1 Then
        MsgBox "Field Finding not found!", vbInformation, "System Error"
        rs.Close
        cn.Close
        GenerateFF = False
        Exit Function
    End If

    Set fs = CreateObject("Scripting.FileSystemObject")
    Set a = fs.CreateTextFile(App.Path & "\" & "ffcode.txt", True)  '--create a textfile
    a.Close
    sPath = App.Path & "\" & "ffcode.txt"
    Open sPath For Output As #1  '--open txtfile for data insert
    Do While Not rs.EOF

    ffcode = Padr(rs.Fields("FFCODE").Value, 3)
    ffdesc = Padr(rs.Fields("FFDESC").Value, 20)

    Print #1, ffcode & ffdesc & "#"

    rs.MoveNext
    Loop

    rs.Close
    Close #1
cn.Close

Set rs = Nothing
Set cn = Nothing

GenerateFF = True

errhandler:
    If Err.Number <> 0 Then
        MsgBox Err.Description, , "Error"
    End If

End Function

Function GenerateRange() As Boolean

    Dim cn As Connection, rs As Recordset
    Dim cnt As Integer
    Dim sPath As String
    Dim fs As Object
    Dim a

    Dim rangecode As String
    Dim rangedesc As String

On Error GoTo errhandler

    Set cn = CreateObject("ADODB.Connection")
    Set rs = CreateObject("ADODB.recordset")

    cn.ConnectionString = "Provider=SQLOLEDB.1; Data Source=" & g_DataSource & ";database=" & g_Database & "; Initial Catalog=;User Id=sa;Password=sqladmin;"
    cn.Open

    rs.Open "SELECT * FROM R_RANGE", cn, adOpenStatic, adLockReadOnly

    If rs.RecordCount < 1 Then
        MsgBox "Range Code not found!", vbInformation, "System Error"
        rs.Close
        cn.Close
        GenerateRange = False
        Exit Function
    End If

    Set fs = CreateObject("Scripting.FileSystemObject")
    Set a = fs.CreateTextFile(App.Path & "\" & "rangecde.txt", True)  '--create a textfile
    a.Close
    sPath = App.Path & "\" & "rangecde.txt"
    Open sPath For Output As #1  '--open txtfile for data insert
    Do While Not rs.EOF

    rangecode = Padr(rs.Fields("RANGECODE").Value, 2)
    rangedesc = Padr(rs.Fields("RANGEDESC").Value, 15)

    Print #1, rangecode & rangedesc & "#"

    rs.MoveNext
    Loop

    rs.Close
    Close #1
cn.Close

Set rs = Nothing
Set cn = Nothing

GenerateRange = True

errhandler:
    If Err.Number <> 0 Then
        MsgBox Err.Description, , "Error"
    End If

End Function

Function GenerateRate() As Boolean

    Dim cn As Connection, rs As Recordset
    Dim cnt As Integer
    Dim sPath As String
    Dim fs As Object
    Dim a

    Dim ratecode As String
    Dim ratedesc As String

On Error GoTo errhandler

    Set cn = CreateObject("ADODB.Connection")
    Set rs = CreateObject("ADODB.recordset")

    cn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\mrmshq.mdb"
    cn.Open

    rs.Open "SELECT * FROM R_RATE", cn, adOpenStatic, adLockReadOnly

    If rs.RecordCount < 1 Then
        MsgBox "Rate Code not found!", vbInformation, "System Error"
        rs.Close
        cn.Close
        GenerateRate = False
        Exit Function
    End If

    Set fs = CreateObject("Scripting.FileSystemObject")
    Set a = fs.CreateTextFile(App.Path & "\" & "rate.txt", True)  '--create a textfile
    a.Close
    sPath = App.Path & "\" & "rate.txt"
    Open sPath For Output As #1  '--open txtfile for data insert
    Do While Not rs.EOF

    ratecode = Padr(rs.Fields("RATECODE").Value, 3)
    ratedesc = Padr(rs.Fields("RATEDESC").Value, 15)

    Print #1, ratecode & ratedesc & "#"

    rs.MoveNext
    Loop

    rs.Close
    Close #1
cn.Close

Set rs = Nothing
Set cn = Nothing

GenerateRate = True

errhandler:
    If Err.Number <> 0 Then
        MsgBox Err.Description, , "Error"
    End If

End Function

Function GetMonth(billcycle As Integer) As String
If billcycle = 1 Then GetMonth = "January"
If billcycle = 2 Then GetMonth = "February"
If billcycle = 3 Then GetMonth = "March"
If billcycle = 4 Then GetMonth = "April"
If billcycle = 5 Then GetMonth = "May"
If billcycle = 6 Then GetMonth = "June"
If billcycle = 7 Then GetMonth = "July"
If billcycle = 8 Then GetMonth = "August"
If billcycle = 9 Then GetMonth = "September"
If billcycle = 10 Then GetMonth = "October"
If billcycle = 11 Then GetMonth = "November"
If billcycle = 12 Then GetMonth = "December"

End Function

Public Function LoadBCPayments(filePath As String, cycle As Integer) As Boolean
     
    ' Connection and recordset for the text and Access link
    Dim cnTxt As Connection, cnDB As Connection
    Dim rstxt As Recordset
    Dim rsDB As Recordset
    Dim rsDBBC As Recordset

    ' use to copy the source file to "download.txt"
    Dim f As Object, fso As Object

    ' use to delete " (quotation mark) from data of download.txt
    Const ForReading = 1, ForWriting = 2, ForAppending = 3
    Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0
    Dim a, ds, d

    ' counts to insert into T_BKINFO
    Dim totalrecs As Integer
    Dim activecnt As Integer
    Dim inactivecnt As Integer

    ' local variables
    Dim convdate As String
    Dim PDate As String
    Dim l_str_Sql As String
    Dim OldBookNo As String, NewBookNo As String
    Dim fs, BookList$, ans%
    Dim bValidateOK As Boolean
    Dim FileFullPath As String, CurrentDnldFilePath As String
    Dim progcnt As Double
    Dim dcdate As Date
    Dim PYDATE As Date
    Dim bc As String
    Dim acctNo As String
    Dim bkno As String
    Dim lastbc As String
    Dim amtpd As Double
On Error GoTo errhandler

' Text file cn and rs
Set cnTxt = New ADODB.Connection
Set rstxt = New ADODB.Recordset

' Local DB cn and rs
Set cnDB = New ADODB.Connection
Set rsDB = New ADODB.Recordset
Set rsDBBC = New ADODB.Recordset

bcp = True
lastbc = 0
Unload frmProg

' Open connections and recordsets
cnTxt.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & ";Extended Properties='text;HDR=No;FMT=Delimited'"
cnTxt.Open

cnDB.ConnectionString = "Provider=SQLOLEDB.1; Data Source=" & g_DataSource & ";database=" & g_Database & "; Initial Catalog=;User Id=sa;Password=sqladmin;"
cnDB.Open

Set fs = CreateObject("Scripting.FileSystemObject")
Set f = fs.GetFolder(filePath)
Set fc = f.Files
If fc.Count = 0 Then
    'Unload frmProg
    MsgBox "No data to load, please check the directory.", vbExclamation, ""
    Exit Function
End If
If Not fs.FolderExists(f & "\current") Then
    'Unload frmProg
    MsgBox "Current archive folder does not exist!", vbExclamation, ""
    Exit Function
End If

For Each f1 In fc
    ' start of transaction for rollback if error encountered
    cnDB.BeginTrans

    s = f1.Name
'    bValidateOK = True
    'MsgBox s
    FileFullPath = filePath & "\" & s
    CurrentDnldFilePath = filePath & "\current\" & s
    'bc = Mid(s, 5, 2) & "00"
    Set fso = CreateObject("Scripting.FileSystemObject")

    If Not (fso.FileExists(FileFullPath)) Then
        MsgBox "File does not exist!", vbInformation, "System Message"
        LoadBCPayments = False
        Exit Function
    End If

    ' copy to local download.txt file
    fso.CopyFile FileFullPath, App.Path & "\bcrcdnld.txt"

    rstxt.Open "SELECT * FROM bcrcdnld.txt", cnTxt, adOpenStatic, adLockOptimistic, adCmdText
    
    totalrecs = 0
    activecnt = 0
    inactivecnt = 0

    Do While Not rstxt.EOF

        acctNo = Padl(rstxt.Fields("AccountNum"), 12, "0") & ""
        bkno = GetBook(acctNo, cycle)
        bc = GetBCCode(acctNo, cycle)
        
        
        totalrecs = totalrecs + 1

        If OpenRecordset(g_rs_BUSCTR, "select BC_CODE from R_BUSCTR where BC_CODE='" & bc & "'", "", "") Then
            If OpenRecordset(g_rs_TRCBOOK, "select max(cast(rcjobid as integer)) from t_rc_book where bc_code='" & bc & "' and cycle=" & cycle & "", "", "") Then
                If g_rs_TRCBOOK.Fields(0).Value & "" = "" Then
                    rcjobid = 1
                    l_str_Sql = "INSERT INTO T_RC_BOOK(RCJOBID, CYCLE, BC_CODE, ACTUAL_DL_DT, ACCTS, NOTRCED, TRCOUNT, DAYNO)VALUES( '" & _
                        rcjobid & "', " & cycle & ",'" & bc & "' , getdate()," & 0 & " , " & 0 & ", " & 0 & ", Day(getdate()) )"
                        If Not DBExecute(l_str_Sql, "", "") Then Exit Function
                Else
                        If bc = lastbc Then
                            rcjobid = g_rs_TRCBOOK.Fields(0).Value
                        Else
                            rcjobid = g_rs_TRCBOOK.Fields(0).Value + 1
                            l_str_Sql = "INSERT INTO T_RC_BOOK(RCJOBID, CYCLE, BC_CODE, ACTUAL_DL_DT, ACCTS, NOTRCED, TRCOUNT, DAYNO)VALUES( '" & _
                                rcjobid & "', " & cycle & ",'" & bc & "' , getdate()," & 0 & " , " & 0 & ", " & 0 & ", Day(getdate()))"
                                If Not DBExecute(l_str_Sql, "", "") Then Exit Function
                        End If
                End If
            End If
            lastbc = bc
        Else
            MsgBox "Business Center does not exist", vbExclamation, "System Message"
            Exit Function
        End If
        
        Dim sql21 As String
        sql21 = "SELECT SERIALNO,SEQNO,ACCTNAME,ADDRESS FROM T_DOWNLOAD WHERE DLCYCLE IN ('" & LTrim(cycle) & "','" & LTrim(cycle - 1) & "') AND ACCTNUM = '" & acctNo & "'"
        If OpenRecordset(g_rs_TDOWNLOAD, sql21, "", "") Then
                    Dim SQLstring1 As String
                    SQLstring1 = "EXEC sp_INSERT_RECONTOHOST '" & rcjobid & "','" & Padl(rstxt.Fields("AccountNum"), 12, "0") & "','" & bkno & "'," & cycle & ",'" & bc & "','','" & g_rs_TDOWNLOAD.Fields("SERIALNO") & "'," & g_rs_TDOWNLOAD.Fields("SEQNO") & ",'" & g_rs_TDOWNLOAD.Fields("ACCTNAME") & "','" & Replace(g_rs_TDOWNLOAD.Fields("ADDRESS"), "'", "") & "',NULL,NULL,NULL,NULL,NULL," & Val(rstxt.Fields("BillAmt")) & ",0,0,'" & Day(Date) & "','BAYADCTR'," & Val(rstxt.Fields("AmtPaid")) & ",'" & rstxt.Fields("PayDate") & " ','" & rstxt.Fields("RAPTag") & "','" & rstxt.Fields("TPAID") & "','" & rstxt.Fields("UserId") & "','" & FileFullPath & "'"
        
                    DBExecute SQLstring1, "", ""
        End If
        

        
        If Not DBExecute("UPDATE T_RC_BOOK SET ACCTS=" & totalrecs & ", NOTRCED=" & totalrecs & " WHERE BC_CODE = '" & bc & "' AND CYCLE = " & cycle & " AND DAYNO = " & Day(Date) & "", "", "") Then
        End If
        
        rstxt.MoveNext
    Loop
    ' Close all recordsets for the next file
    rstxt.Close

    ' Move file to current in download source folder
    fso.MoveFile FileFullPath, CurrentDnldFilePath

Continue_Process:

    cnDB.CommitTrans
Next

'Clean up
cnTxt.Close
cnDB.Close

Set rstxt = Nothing
Set cnTxt = Nothing


Set rsDB = Nothing
Set rsDBBC = Nothing
Set cnDB = Nothing

'Unload frmProg
LoadBCPayments = True

errhandler:

If Err.Number <> 0 Then
    If Err.Number <> -2147217887 Then
        MsgBox "Download Error." & vbCrLf & vbCrLf & Err.Description, vbCritical, ""
        ' rollback loaded data with error
        cnDB.RollbackTrans
    End If

End If
 
End Function

Private Function GetBook(sAcctno As String, cycle As Integer) As String
    Dim strSQL As String

       strSQL = "SELECT TOP 1 BOOKNO FROM T_DOWNLOAD WHERE " & _
             " ACCTNUM = '" & sAcctno & "'"

    If Not OpenRecordset(g_rs_TBOOK, strSQL, "MDIMain", "GetBCCode") Then
        strSQL = "SELECT BOOKNO FROM T_RC_DOWNLOAD WHERE ACCTNUM = '" & sAcctno & "'"
        If OpenRecordset(g_rs_TBOOK, strSQL, "", "") Then
        GetBook = g_rs_TBOOK.Fields(0)
        End If
        
    Else
        GetBook = g_rs_TBOOK.Fields(0)
    End If
End Function

Private Function GetBCCode(sAcctno As String, cycle As Integer) As String
    Dim strSQL As String
    Dim rsBOOK As ADODB.Recordset
    Set rsBOOK = New ADODB.Recordset
'    strSQL = "SELECT TOP 1 BOOKNO FROM T_DOWNLOAD WHERE " & _
'             " ACCTNUM = '" & sAcctNo & "'"

    
strSQL = "SELECT DISTINCT BC_CODE  FROM T_DOWNLOAD " & _
"LEFT JOIN T_BOOK " & _
"ON T_DOWNLOAD.BOOKNO = T_BOOK.BOOKNO " & _
"WHERE T_DOWNLOAD.ACCTNUM = '" & sAcctno & "'"

                
    If Not OpenRecordset(rsBOOK, strSQL, "", "") Then
    
        strSQL = "SELECT BC_CODE FROM T_RC_DOWNLOAD WHERE ACCTNUM = '" & sAcctno & "'"
        If OpenRecordset(g_rs_TDOWNLOAD, strSQL, "MDIMain", "GetBook") Then
            GetBCCode = g_rs_TDOWNLOAD.Fields(0)
        End If
        
    Else
        GetBCCode = rsBOOK.Fields(0)
       
    End If
    
    
End Function

Private Sub ConcatenateFiles(ByVal ResultFile As String, _
    ByVal Separator As String, ParamArray SourceFiles() As Variant)
    Dim fso As New FileSystemObject
    Dim fsSourceStream As TextStream
    Dim fsResStream As TextStream
    Dim sSeparator As String
    Dim i As Integer
    
    On Error Resume Next
    
    ' create a new file
    Set fsResStream = fso.OpenTextFile(ResultFile, ForWriting, True)
    
    ' for each source file in the input array
    For i = 0 To UBound(SourceFiles)
        ' add the separator first (replacing the special tag for the file path)
        sSeparator = Replace(Separator, "#FilePath#", SourceFiles(i))
        fsResStream.Write sSeparator & vbCrLf
        ' open the file in read mode
        Set fsSourceStream = fso.OpenTextFile(SourceFiles(i), ForReading)
        ' add its content + a blank line to the result file
        fsResStream.Write fsSourceStream.ReadAll & vbCrLf
        ' close this source file
        fsSourceStream.Close
    Next i
    
    fsResStream.Close
End Sub



