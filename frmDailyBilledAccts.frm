VERSION 5.00
Begin VB.Form frmDailyBilledAccts 
   Caption         =   "Billed Accounts"
   ClientHeight    =   4635
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4995
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   4635
   ScaleWidth      =   4995
   Begin VB.Frame frmMain 
      Caption         =   "Billed Accounts"
      Height          =   4575
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   4815
      Begin VB.TextBox txtcurFile 
         Height          =   855
         Left            =   1200
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   10
         Top             =   3600
         Width           =   3495
      End
      Begin VB.FileListBox File1 
         Height          =   2430
         Left            =   2040
         Pattern         =   "*.txt"
         TabIndex        =   5
         Top             =   600
         Width           =   2655
      End
      Begin VB.DriveListBox drvSource 
         Height          =   315
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   1875
      End
      Begin VB.DirListBox dirSource 
         Height          =   2340
         Left            =   120
         TabIndex        =   3
         Top             =   720
         Width           =   1875
      End
      Begin VB.CommandButton cmdDownload 
         Caption         =   "&Download"
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Top             =   3120
         Width           =   4575
      End
      Begin VB.Label lblTotCount 
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   840
         TabIndex        =   9
         Top             =   4080
         Width           =   495
      End
      Begin VB.Label Label4 
         Caption         =   "/"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   480
         TabIndex        =   8
         Top             =   4080
         Width           =   255
      End
      Begin VB.Label lblcurCount 
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   4080
         Width           =   255
      End
      Begin VB.Label Label3 
         Caption         =   "Processing:"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   3720
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Source File"
         Height          =   255
         Left            =   2160
         TabIndex        =   1
         Top             =   360
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmDailyBilledAccts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim RSPath As ADODB.Recordset
Dim SQLstring As String
Dim DLPath As String
Dim TarPath As String
Dim ErrPath As String
Dim ErrFileName As String






Private Sub cmdBrowse_Click()
'frmFileSource.Show
'FileDetails
End Sub

Private Sub cmdDownload_Click()
Dim X As Integer

txtcurFile.Text = "Downloading..."
txtcurFile.SetFocus
For X = 1 To File1.ListCount
lblcurCount.Caption = X
If File1.List(X - 1) <> "" Then FileDetails (File1.List(X - 1))
'MsgBox File1.List
Next X
txtcurFile.Text = txtcurFile.Text & vbCrLf & "Done.."
txtcurFile.SetFocus
End Sub

Private Sub dirSource_Change()

File1.Path = dirSource.Path
lblcurCount.Caption = 0
lblTotCount.Caption = File1.ListCount
txtcurFile.Text = ""
End Sub

Private Sub drvSource_Change()
dirSource.Path = drvSource.Drive

End Sub

Private Sub Form_Activate()
GetPATH
If WindowState = vbNormal Then
    Me.Move (1000)
    Me.Top = 500
    Me.Height = 5145
    Me.Width = 5115
End If
dirSource.Path = DLPath
End Sub


Private Sub FileDetails(fileName As String)
Dim filePath As String
Dim SQLInsert As String
Dim filePart As String
Dim dldate As Date
Dim rCount As Integer
Dim ConnAccountNo As String
Dim InvoiceNo As String
Dim BilledAmount As Double
Dim BilledMonth As String
Dim BilledYear As String
Dim BrCode As String
Dim FileFullPath As String, CurrentDnldFilePath As String
Dim FileError As Boolean
Dim billMonthCycle As Integer
Dim billYearCycle As Integer
Dim sUPath
Dim temp$
Dim oComm As Command
Dim oRs As ADODB.Recordset


Set oRs = New ADODB.Recordset
Set oComm = New ADODB.Command

FileError = False

Dim f As Object, fso As Object

Set fso = CreateObject("Scripting.FileSystemObject")

'
On Error GoTo errorhandler
filePath = dirSource.Path & "\" & fileName
CurrentDnldFilePath = Trim(TarPath) & "\" & fileName
sUPath = filePath
rCount = 0
Open sUPath For Input As #1



filePart = Mid(fileName, 6, 8)
dldate = CDate(Mid(filePart, 1, 2) & "/" & Mid(filePart, 3, 2) & "/" & Year(Now))
BrCode = Mid(filePart, 6, 2)

billMonthCycle = Month(dldate)
billYearCycle = Year(dldate)


Do While Not EOF(1)
    Input #1, temp$
    
 If Mid$(temp$, 1, 1) <> "|" Then
    ConnAccountNo = Trim(Mid$(temp$, 6, 12))
    InvoiceNo = Mid$(temp$, 19, 10)
    BilledAmount = Abs(CDbl(Mid$(temp$, 30, 12)))
    BilledMonth = Mid$(temp$, 1, 2)
    BilledYear = "20" & Mid$(temp$, 3, 2)
    
Dim SQLstring As String
'Edited by J.Espiritu Aug 19,2010
SQLstring = "INSERT INTO DAILY_BILLED (CONACCTNO,INVOICENO,BILLEDAMOUNT,BILLEDMONTH,BILLEDYEAR,BRANCHCODE,DLDATE) VALUES ('" & ConnAccountNo & "','" & InvoiceNo & "'," & BilledAmount & "," & BilledMonth & "," & BilledYear & ",'" & BrCode & "','" & dldate & "')"
'oRs.Open "SELECT * FROM DAILY_BILLED", g_Conn, adOpenDynamic, adLockOptimistic
'    With oRs
'        .AddNew
'        .Fields("CONACCTNO") = ConnAccountNo
'        .Fields("INVOICENO") = InvoiceNo
'        .Fields("BILLEDAMOUNT") = BilledAmount
'        .Fields("BILLEDMONTH") = BilledMonth
'        .Fields("BILLEDYEAR") = BilledYear
'        .Fields("BRANCHCODE") = BrCode
'        .Fields("DLDATE") = dldate
'        .Update
'    End With
' oRs.Close
DBExecute SQLstring, "", ""
 rCount = rCount + 1
 End If
 
'----FOR INCOMPLETE BILL MONTH INSERT TO DAILY_BILLED ERROR TABLE
 
 If Mid$(temp$, 1, 1) = "|" Then
    FileError = True
    ConnAccountNo = Trim(Mid$(temp$, 2, 12))
    InvoiceNo = Mid$(temp$, 15, 10)
    BilledAmount = Abs(CDbl(Trim(Mid$(temp$, 26, 12))))
    BilledMonth = 0
    BilledYear = 0
    
    Dim SQLstring1 As String
'Edited by J.Espiritu Aug 19,2010
SQLstring1 = "INSERT INTO DAILY_BILLED_ERROR (CONACCTNO,INVOICENO,BILLEDAMOUNT,BILLEDMONTH,BILLEDYEAR,BRANCHCODE,DLDATE) VALUES ('" & ConnAccountNo & "','" & InvoiceNo & "'," & BilledAmount & "," & BilledMonth & "," & BilledYear & ",'" & BrCode & "','" & dldate & "')"
' oRs.Open "SELECT * FROM DAILY_BILLED_ERROR", g_Conn, adOpenDynamic, adLockOptimistic
'    With oRs
'        .AddNew
'        .Fields("CONACCTNO") = ConnAccountNo
'        .Fields("INVOICENO") = InvoiceNo
'        .Fields("BILLEDAMOUNT") = BilledAmount
'        .Fields("BILLEDMONTH") = BilledMonth
'        .Fields("BILLEDYEAR") = BilledYear
'        .Fields("BRANCHCODE") = BrCode
'        .Fields("DLDATE") = dldate
'        .Update
'    End With
' oRs.Close
DBExecute SQLstring1, "", ""
 rCount = rCount + 1
 
 End If
 
 
Loop
Close #1
' Move file to current in download source folder
fso.MoveFile filePath, CurrentDnldFilePath
txtcurFile.Text = txtcurFile.Text & vbCrLf & fileName & " ---- " & rCount

If FileError = True Then GenerateErrorFile CInt(billMonthCycle), CInt(billYearCycle), dldate


errorhandler:
If Err.Number <> 0 Then
    If Err.Number = 13 Then
        MsgBox "Erroneous Data. Please check Daily Billed File", vbExclamation, "File Error"
    Else
        MsgBox Err.Description, , "System Message"
    End If
End If


End Sub

Private Sub GenerateErrorFile(billMonth As Integer, billYear As Integer, DownloadDate As Date)
Dim SQLsp As String
Dim rsError As ADODB.Recordset
Dim textbody As String
Dim textheader As String
Dim txtRS As String

textheader = "Download Date" & vbTab & "Branch" & vbTab & "CAN" & vbTab & "Installation No." & vbTab & "Observation Code" & vbTab & "CUR_SOA#" & vbTab & "CUR_Amount" & vbTab & "PREV_SOA#" & vbTab & "PREV_Amount"
Dim fs As FileSystemObject
Dim ts As TextStream
Set fs = New FileSystemObject

ErrFileName = Trim(ErrPath) & "\BILLED_ERROR_" & CStr(billMonth) & "_" & CStr(Year(Now)) & ".txt"
'To Read
If fs.FileExists(ErrFileName) Then
   Set ts = fs.OpenTextFile(ErrFileName)
        Do While Not ts.AtEndOfStream
            textbody = textbody & vbCrLf & ts.ReadLine
        Loop
        ts.Close
Else
'To write
Set ts = fs.OpenTextFile(ErrFileName, ForWriting, True)
ts.WriteLine textheader
ts.Close
End If



SQLsp = "EXEC sp_BILLED_ERROR " & billMonth & "," & billYear & ",'" & DownloadDate & "'"
    
If OpenRecordset(rsError, SQLsp, "", "") Then
textbody = textbody & vbCrLf & textheader & vbCrLf
            While Not rsError.EOF
            'Append Recordset to text body
            txtRS = rsError.Fields(0) & vbTab & rsError.Fields(1) & vbTab & rsError.Fields(2) & vbTab & rsError.Fields(3) & vbTab & rsError.Fields(4) & vbTab & rsError.Fields(5) & vbTab & rsError.Fields(6) & vbTab & rsError.Fields(7) & vbTab & rsError.Fields(8)
             textbody = textbody & txtRS & vbCrLf
               rsError.MoveNext
            Wend
      
'save text file

   Set ts = fs.OpenTextFile(ErrFileName, ForWriting, True)
    ts.WriteLine textbody
    ts.Close
End If
End Sub

Private Sub GetPATH()
SQLstring = "SELECT * FROM dbo.DailyBilledPath"

If OpenRecordset(RSPath, SQLstring, "", "") Then
    While Not RSPath.EOF
        DLPath = RSPath.Fields(0)
        TarPath = RSPath.Fields(1)
        ErrPath = RSPath.Fields(2)
        RSPath.MoveNext
    Wend
End If

End Sub
