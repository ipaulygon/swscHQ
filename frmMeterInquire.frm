VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmMeterInquire 
   Caption         =   "Meter Inquire"
   ClientHeight    =   6000
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13800
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6000
   ScaleWidth      =   13800
   Begin VB.CommandButton cmdReports 
      Caption         =   "&Generate File"
      Enabled         =   0   'False
      Height          =   495
      Left            =   9240
      TabIndex        =   17
      Top             =   5280
      Width           =   1215
   End
   Begin VB.CommandButton Command2 
      Caption         =   "&Close"
      Height          =   495
      Left            =   11880
      TabIndex        =   16
      Top             =   5280
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&View Details"
      Height          =   495
      Left            =   10560
      TabIndex        =   15
      Top             =   5280
      Width           =   1215
   End
   Begin VB.Frame frmSearch 
      Caption         =   "Search Criteria"
      Height          =   1455
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   13575
      Begin VB.OptionButton optInactive 
         Caption         =   "Inactive"
         Height          =   375
         Left            =   8280
         TabIndex        =   13
         Top             =   840
         Width           =   1215
      End
      Begin VB.OptionButton optActive 
         Caption         =   "Active"
         Height          =   375
         Left            =   6600
         TabIndex        =   12
         Top             =   840
         Width           =   1215
      End
      Begin VB.TextBox txtInstallNo 
         Height          =   285
         Left            =   2160
         TabIndex        =   11
         Top             =   840
         Width           =   1935
      End
      Begin VB.TextBox txtMeterNo 
         Height          =   285
         Left            =   2160
         TabIndex        =   10
         Top             =   480
         Width           =   1935
      End
      Begin VB.CommandButton cmdRetrieve 
         Caption         =   "&Retrieve"
         Height          =   375
         Left            =   12000
         TabIndex        =   2
         Top             =   360
         Width           =   1215
      End
      Begin VB.CommandButton cmdReset 
         Caption         =   "&Reset"
         Height          =   375
         Left            =   12000
         TabIndex        =   1
         Top             =   840
         Width           =   1215
      End
      Begin MSComCtl2.DTPicker dtpFrom 
         Height          =   285
         Left            =   6600
         TabIndex        =   3
         Top             =   480
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   503
         _Version        =   393216
         Format          =   59310081
         CurrentDate     =   39589
      End
      Begin MSComCtl2.DTPicker dtpTo 
         Height          =   285
         Left            =   10200
         TabIndex        =   4
         Top             =   480
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   503
         _Version        =   393216
         Format          =   59310081
         CurrentDate     =   39589
      End
      Begin VB.Label Label1 
         Caption         =   "Meter/Device Number:"
         Height          =   255
         Left            =   360
         TabIndex        =   9
         Top             =   480
         Width           =   1815
      End
      Begin VB.Label Label2 
         Caption         =   "Installation Number:"
         Height          =   255
         Left            =   360
         TabIndex        =   8
         Top             =   840
         Width           =   1695
      End
      Begin VB.Label Label4 
         Caption         =   "Status:"
         Height          =   255
         Left            =   4680
         TabIndex        =   7
         Top             =   840
         Width           =   1215
      End
      Begin VB.Label Label5 
         Caption         =   "Reading Date From:"
         Height          =   255
         Left            =   4680
         TabIndex        =   6
         Top             =   480
         Width           =   1575
      End
      Begin VB.Label Label6 
         Caption         =   "Reading Date To:"
         Height          =   255
         Left            =   8400
         TabIndex        =   5
         Top             =   480
         Width           =   1695
      End
   End
   Begin MSFlexGridLib.MSFlexGrid msgridView 
      Height          =   3615
      Left            =   120
      TabIndex        =   14
      Top             =   1560
      Width           =   13605
      _ExtentX        =   23998
      _ExtentY        =   6376
      _Version        =   393216
   End
End
Attribute VB_Name = "frmMeterInquire"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim MRUstat As String
Dim AndStat As String
Dim AndMeterNo As String
Dim AndInstallNo As String
Dim AndDates As String
Dim rowSel As Integer
Dim bc As String
Dim acctnme As String
Dim ulcycle As Integer
Dim installation As String

Private Sub cmdReports_Click()
    Dim sDir As String
    Dim sFName As String
    'Dim rdgdate1 As Long
    'Dim rdgdate2 As Long
    Dim rdgdate1 As Date
    Dim rdgdate2 As Date
    Dim acctNo As String
    Dim METERNO As String
    Dim actvinactv As String
    Dim f As Object
    Dim d, drivename
    
    Set f = CreateObject("Scripting.FileSystemObject")

    Dim Appl As New CRAXDRT.Application
    Dim Report As New CRAXDRT.Report
    Dim i As Integer

    'Set fs = CreateObject("Scripting.FileSystemObject")
    Set d = f.GetDrive(f.GetDriveName(App.Path))
    drivename = d.DriveLetter & ":"

    If Not f.FolderExists(drivename & "\MWSIReports\") Then f.CreateFolder (drivename & "\MWSIReports\")
     sDir = drivename & "\MWSIReports\Meter Inquire\"
    If Not f.FolderExists(sDir) Then f.CreateFolder (sDir)
    
    'GET PARAMETERS
    'rdgdate1 = dateconvert(DTPFrom.Value)
    'rdgdate2 = dateconvert(DTPTo.Value)
    rdgdate1 = DTPTo.Value
    rdgdate2 = DTPFrom.Value
    If txtInstallNo <> "" Then
        acctNo = txtInstallNo.Text
    Else
        'acctNo = "*"
        acctNo = "%"
    End If
    
    If txtMeterNo.Text <> "" Then
        METERNO = txtMeterNo.Text
    Else
        'meterNo = "*"
        METERNO = "%"
    End If
    
    If optActive = True Then
        actvinactv = "01"
    ElseIf optInactive = True Then
        actvinactv = ""
    Else
        actvinactv = "*"
    End If

    Set Appn = CreateObject("CrystalRunTime.Application")

    Set cReport = Appn.OpenReport(App.Path & "\crystal reports\METER INQUIRE.rpt")
    cReport.Database.LogOnServer "p2ssql.dll", "SZ-MWSI-SUBIC", "MCFSDB", "sa", "sqladmin"
    For i = 1 To cReport.Database.Tables.Count
        cReport.Database.Tables(i).ConnectionProperties.Item("Password") = "sqladmin"
    Next i

    cReport.DiscardSavedData

        ' Common Parameters
        cReport.ParameterFields(1).AddCurrentValue rdgdate1
        cReport.ParameterFields(2).AddCurrentValue rdgdate2
        cReport.ParameterFields(3).AddCurrentValue acctNo
        cReport.ParameterFields(4).AddCurrentValue METERNO
        cReport.ParameterFields(5).AddCurrentValue actvinactv
               
        If acctNo <> "*" Or METERNO <> "*" Then
            'sFName = acctNo & "_" & meterNo & ".xls"
            sFName = bc & "_" & acctnme & "_" & installation & ".xls"
        Else
            sFName = "METER_INQUIRY.xls"
        End If
                
        cReport.ExportOptions.DiskFileName = sDir & sFName
        cReport.ExportOptions.DestinationType = crEDTDiskFile
        cReport.ExportOptions.FormatType = crEFTExcelDataOnly
        cReport.ExportOptions.ExcelAreaType = crDetail
        cReport.ExportOptions.ExcelUseFormatInDataOnly = True
        cReport.ExportOptions.ExcelMaintainRelativeObjectPosition = True
        cReport.ExportOptions.ExcelMaintainColumnAlignment = True
        cReport.ExportOptions.ExcelChopPageHeader = False
        cReport.Export False
    MsgBox ("File Generated!")
        
End Sub

Private Sub cmdReset_Click()
txtMeterNo.Text = ""
txtInstallNo.Text = ""
AndMeterNo = ""
AndInstallNo = ""
AndStat = ""
optActive.Value = False
optInactive.Value = False
cmdReports.Enabled = False
FormatDataGrid
End Sub

Private Sub cmdRetrieve_Click()
Dim sqlMRU As String
Dim rsMRUList As ADODB.Recordset

If txtMeterNo.Text <> "" Then
    AndMeterNo = " AND UPLD.METERNO like '%" & txtMeterNo.Text & "%'"
Else
    AndMeterNo = ""
End If

If txtInstallNo.Text <> "" Then
'    AndInstallNo = " AND T_UPLOAD.ACCTNUM like '%" & txtInstallNo.Text & "%'"
    AndInstallNo = " AND T_DOWNLOAD.ACCTNUM like '%" & txtInstallNo.Text & "%'"
Else
    AndInstallNo = ""
End If

If DTPFrom.Value > DTPTo.Value Then
    MsgBox ("Please enter valid dates for criteria!"), vbCritical
    Exit Sub
Else
    AndDates = " AND UPLD.RDGDATE >= '" & dateconvert(DTPFrom.Value) & "' AND UPLD.RDGDATE <= '" & dateconvert(DTPTo.Value) & "' order by UPLD.RDGDATE DESC"

    'sqlMRU = "SELECT T_UPLOAD.ACCTNUM, T_UPLOAD.METERNO, T_UPLOAD.RDGDATE, T_DOWNLOAD.PREVCONS, T_DOWNLOAD.PREVRDG, T_DOWNLOAD.BILL_PREVRDG, " & _
    '"T_UPLOAD.PRESRDG, T_UPLOAD.FFCODE, T_UPLOAD.REMARKS, T_DOWNLOAD.ACCTNAME FROM (T_UPLOAD INNER JOIN T_DOWNLOAD ON T_UPLOAD.METERNO = " & _
    '"T_DOWNLOAD.SERIALNO AND T_UPLOAD.ULCYCLE = T_DOWNLOAD.DLCYCLE)  WHERE T_UPLOAD.ACCTNUM <> '' " & AndStat & AndMeterNo & AndInstallNo & AndDates
    
    'SELECT T_UPLOAD.ACCTNUM to SELECT T_DOWNLOAD.ACCTNUM
    ' orig
    'sqlMRU = "SELECT T_DOWNLOAD.ACCTNUM, T_UPLOAD.METERNO, T_UPLOAD.RDGDATE, T_DOWNLOAD.PREVCONS, T_DOWNLOAD.PREVRDG, T_DOWNLOAD.BILL_PREVRDG, " & _
    '"T_UPLOAD.PRESRDG, T_UPLOAD.FFCODE, T_UPLOAD.REMARKS, T_DOWNLOAD.ACCTNAME, R_BUSCTR.BC_DESC, T_UPLOAD.ULCYCLE FROM " & _
    '"((T_UPLOAD INNER JOIN T_DOWNLOAD ON T_UPLOAD.METERNO = T_DOWNLOAD.SERIALNO AND T_UPLOAD.ULCYCLE = T_DOWNLOAD.DLCYCLE) INNER JOIN T_BOOK ON T_BOOK.BOOKNO = T_UPLOAD.BOOKNO AND T_BOOK.CYCLE = T_UPLOAD.ULCYCLE)  INNER JOIN R_BUSCTR ON R_BUSCTR.BC_CODE = T_BOOK.BC_CODE " & _
    '"WHERE T_UPLOAD.ACCTNUM <> '' " & AndStat & AndMeterNo & AndInstallNo & AndDates
    
    ' based from report
'    sqlMRU = "SELECT T_DOWNLOAD.ACCTNUM, T_UPLOAD.METERNO, T_UPLOAD.RDGDATE, T_DOWNLOAD.PREVCONS, T_DOWNLOAD.PREVRDG, T_DOWNLOAD.BILL_PREVRDG, " & _
'    "T_UPLOAD.PRESRDG, T_UPLOAD.FFCODE, T_UPLOAD.REMARKS, T_DOWNLOAD.ACCTNAME, R_BUSCTR.BC_DESC, T_UPLOAD.ULCYCLE FROM " & _
'    "R_BUSCTR INNER JOIN ((T_UPLOAD INNER JOIN T_DOWNLOAD ON (T_UPLOAD.ULDOC_NO = T_DOWNLOAD.DLDOC_NO) AND (T_UPLOAD.METERNO = T_DOWNLOAD.SERIALNO)) INNER JOIN T_BOOK ON (T_BOOK.CYCLE = T_DOWNLOAD.DLCYCLE) AND (T_BOOK.BOOKNO = T_DOWNLOAD.BOOKNO) AND (T_UPLOAD.BOOKNO = T_BOOK.BOOKNO) AND (T_UPLOAD.ULCYCLE = T_BOOK.CYCLE)) ON R_BUSCTR.BC_CODE = T_BOOK.BC_CODE " & _
'    "WHERE T_UPLOAD.ACCTNUM <> '' and T_UPLOAD.RDGDATE<>'' " & AndStat & AndMeterNo & AndInstallNo & AndDates
    sqlMRU = "SELECT T_DOWNLOAD.ACCTNUM, UPLD.METERNO, UPLD.RDGDATE, T_DOWNLOAD.PREVCONS, T_DOWNLOAD.PREVRDG, T_DOWNLOAD.BILL_PREVRDG, " & _
    "UPLD.PRESRDG, UPLD.FFCODE, UPLD.REMARKS, T_DOWNLOAD.ACCTNAME, R_BUSCTR.BC_DESC, UPLD.ULCYCLE FROM " & _
    "R_BUSCTR INNER JOIN (((SELECT c.* From t_upload c Where Not Exists (SELECT a.bookno,a.ulcycle,a.uldoc_no,a.ffcode,a.rangecode,a.deviceno,a.presrdg,a.rdgdate, " & _
    "a.rdgtime , a.METERNO, a.REMARKS, a.ACCTNUM, a.readtag, a.SEQNO, a.tries, a.newmtrbrand, a.newmtrnum " & _
    "From t_upload_his a inner join t_ver_extract b on a.bookno=b.bookno and a.ulcycle=b.ulcycle " & _
    "and a.acctnum=b.acctnum Where " & _
    "c.BookNo = a.BookNo And c.ulcycle = a.ulcycle And c.ACCTNUM = a.ACCTNUM " & _
    "AND a.origdata=0 and b.condition=1) " & _
    "Union " & _
    "SELECT a.bookno,a.ulcycle,a.uldoc_no,a.ffcode,a.rangecode,a.deviceno,a.presrdg,a.rdgdate, " & _
    "a.rdgtime , a.METERNO, a.REMARKS, a.ACCTNUM, a.readtag, a.SEQNO, a.tries, a.newmtrbrand, a.newmtrnum " & _
    "From t_upload_his a inner join t_ver_extract b on a.bookno=b.bookno and a.ulcycle=b.ulcycle " & _
    "and a.acctnum=b.acctnum WHERE " & _
    "a.origdata=0 and b.condition=1) AS UPLD " & _
    "INNER JOIN T_DOWNLOAD ON (UPLD.ULDOC_NO = T_DOWNLOAD.DLDOC_NO) AND (UPLD.METERNO = T_DOWNLOAD.SERIALNO)) INNER JOIN T_BOOK ON (T_BOOK.CYCLE = T_DOWNLOAD.DLCYCLE) AND (T_BOOK.BOOKNO = T_DOWNLOAD.BOOKNO) AND (UPLD.BOOKNO = T_BOOK.BOOKNO) AND (UPLD.ULCYCLE = T_BOOK.CYCLE)) ON R_BUSCTR.BC_CODE = T_BOOK.BC_CODE " & _
    "WHERE UPLD.ACCTNUM <> '' and UPLD.RDGDATE<>'' " & AndStat & AndMeterNo & AndInstallNo & AndDates
        
    ' some meterno values are padded with 0 (same with serialno) which arent padded in deviceno
    '"T_DOWNLOAD.DEVICENO AND T_UPLOAD.ULCYCLE = T_DOWNLOAD.DLCYCLE)  WHERE T_UPLOAD.ACCTNUM <> '' " & AndStat & AndMeterNo & AndInstallNo & AndDates
    
    'sqlMRU = "SELECT T_UPLOAD.ACCTNUM, T_UPLOAD.METERNO, T_UPLOAD.RDGDATE, T_DOWNLOAD.BILL_PREVRDG, " & _
    '"T_UPLOAD.PRESRDG, T_UPLOAD.PRESRDG - T_DOWNLOAD.BILL_PREVRDG, T_UPLOAD.FFCODE FROM (T_UPLOAD INNER JOIN T_DOWNLOAD ON T_UPLOAD.METERNO = " & _
    '"T_DOWNLOAD.SERIALNO AND T_UPLOAD.ULCYCLE = T_DOWNLOAD.DLCYCLE)  WHERE T_UPLOAD.ACCTNUM <> '' " & AndStat & AndMeterNo & AndInstallNo & AndDates
    
    FormatDataGrid
    GenerateView (sqlMRU)

End If

End Sub

Private Sub Command1_Click()

If msgridView.TextMatrix(msgridView.Row, 0) <> "" Then
   frmMeterDetails.Show
Else
   MsgBox "Please select a Meter First", vbCritical
   
End If

End Sub

Private Sub Command2_Click()
Unload Me
End Sub

Private Sub Form_Load()
frmMeterInquire.Width = 13920
frmMeterInquire.Height = 6510
FormatDataGrid

End Sub

Private Sub FormatDataGrid()
Dim ColNames As String
Dim X As Integer

    msgridView.Clear
    'msgridView.Rows = 20
    msgridView.FixedRows = 1
    msgridView.FixedCols = 0
    ColNames = "<Installation No.|<Meter No.|<Reading Date|<Prev Consumption|<Previous Rdg|<Billed Previous Rdg |<Current Rdg|<Observation Code|<Remarks|<Account Name|<Business Center|<Cycle"
    msgridView.FormatString = ColNames
    For X = 0 To 11
    If X = 9 Or X = 10 Or X = 11 Then
        msgridView.ColWidth(X) = 0
    Else
        msgridView.ColWidth(X) = 1500
    End If
    Next X
    
End Sub

Private Sub GenerateView(SQLQuery As String)
Dim X As Integer
Dim Y As Integer
Dim Count As Integer

    If OpenRecordset(g_rs_BOOKINQ, SQLQuery, "frmMRUGrid", "MRU Management") Then
     ulcycle = g_rs_BOOKINQ.Fields(11)
     bc = g_rs_BOOKINQ.Fields(10)
     installation = g_rs_BOOKINQ.Fields(0)
     Y = 1
        Do While Not g_rs_BOOKINQ.EOF
        
            Count = Count + 1
            If ulcycle < g_rs_BOOKINQ.Fields(11) Then
                ulcycle = g_rs_BOOKINQ.Fields(11)
                bc = g_rs_BOOKINQ.Fields(10)
            End If
            If Count >= msgridView.Rows Then
                'increase the amount of rows by 100 at a time - makes everything faster
                msgridView.Rows = msgridView.Rows + 100
            End If
            
                For X = 0 To g_rs_BOOKINQ.Fields.Count - 1
                    If Not IsNull(g_rs_BOOKINQ.Fields(X).Value) Then
                        msgridView.TextMatrix(Y, X) = g_rs_BOOKINQ.Fields(X).Value
                    Else
                        msgridView.TextMatrix(Y, X) = ""
                    End If
                Next X
        Y = Y + 1
        cmdReports.Enabled = True
        acctnme = g_rs_BOOKINQ.Fields(9)
        g_rs_BOOKINQ.MoveNext
        Loop
    End If

End Sub

Private Sub msgridView_Click()
Dim gridCol As Integer
Dim GridRow As Integer
Dim i As Integer

GridRow = CInt(msgridView.Row)
gridCol = CInt(msgridView.Col)

gInstallNo = msgridView.TextMatrix(GridRow, 0)
gMeterNo = msgridView.TextMatrix(GridRow, 1)
gRdgDate = msgridView.TextMatrix(GridRow, 2)

'set previous selected forecolor to black
If rowSel <> 0 Then
    With msgridView
        .Row = rowSel
        For i = 0 To .Cols - 1
         .Col = i
         .CellForeColor = RGB(30, 0, 0) 'black
         .CellBackColor = RGB(255, 255, 255)
        Next
    End With
End If

'set current selected forecolor to red
      With msgridView
        .Row = GridRow
        For i = 0 To .Cols - 1
         .Col = i
         .CellForeColor = RGB(255, 255, 255) 'white
         .CellBackColor = RGB(30, 0, 0)
        Next
        .Col = gridCol
    End With

'assign current selected to previous
rowSel = GridRow

End Sub

Private Sub optActive_Click()

AndStat = " AND T_DOWNLOAD.MTRTYPE = '01' "

End Sub

Private Sub optInactive_Click()
'mruStat = "Inactive"
AndStat = " AND T_DOWNLOAD.MTRTYPE <> '01' "
End Sub

Function dateconvert(date1 As Date) As Long
Dim StrYearPart As String
Dim StrMonthPart As String
Dim StrDayPart As String
Dim StrFulldate As String

StrYearPart = Year(date1)
StrMonthPart = Month(date1)
StrDayPart = Day(date1)
If Len(StrMonthPart) = 1 Then StrMonthPart = "0" & StrMonthPart
If Len(StrDayPart) = 1 Then StrDayPart = "0" & StrDayPart
StrFulldate = StrYearPart & StrMonthPart & StrDayPart
dateconvert = CLng(StrFulldate)

End Function
