VERSION 5.00
Begin VB.Form frmFoundConn 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Edit Found Connected"
   ClientHeight    =   3975
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4650
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3975
   ScaleWidth      =   4650
   Begin VB.TextBox txtAcctNo 
      Height          =   285
      Left            =   1815
      MaxLength       =   12
      TabIndex        =   20
      Top             =   6465
      Width           =   2500
   End
   Begin VB.TextBox txtSeqNo 
      Height          =   285
      Left            =   1815
      MaxLength       =   4
      TabIndex        =   19
      Top             =   6120
      Width           =   2500
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save"
      Default         =   -1  'True
      Height          =   390
      Left            =   1335
      TabIndex        =   8
      Top             =   3420
      Width           =   915
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   390
      Left            =   2355
      TabIndex        =   9
      Top             =   3420
      Width           =   915
   End
   Begin VB.Frame Frame1 
      Height          =   3165
      Left            =   120
      TabIndex        =   10
      Top             =   60
      Width           =   4400
      Begin VB.TextBox txtBookNo 
         Height          =   285
         Left            =   1700
         MaxLength       =   6
         TabIndex        =   0
         Top             =   300
         Width           =   2500
      End
      Begin VB.TextBox txtMtrNo 
         Height          =   285
         Left            =   1700
         MaxLength       =   10
         TabIndex        =   1
         Top             =   660
         Width           =   2500
      End
      Begin VB.TextBox txtDateRdng 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd-MM-yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   3
         EndProperty
         Height          =   285
         Left            =   1700
         MaxLength       =   10
         TabIndex        =   2
         Top             =   1005
         Width           =   2500
      End
      Begin VB.TextBox txtTimeRdng 
         Height          =   285
         Left            =   1700
         MaxLength       =   8
         TabIndex        =   3
         Top             =   1335
         Width           =   2500
      End
      Begin VB.TextBox txtCustName 
         Height          =   285
         Left            =   1700
         MaxLength       =   25
         TabIndex        =   4
         Top             =   1680
         Width           =   2500
      End
      Begin VB.TextBox txtCustAdd 
         Height          =   285
         Left            =   1700
         MaxLength       =   25
         TabIndex        =   5
         Top             =   2025
         Width           =   2500
      End
      Begin VB.TextBox txtKWHRdng 
         Height          =   285
         Left            =   1700
         MaxLength       =   9
         TabIndex        =   6
         Top             =   2355
         Width           =   2500
      End
      Begin VB.TextBox txtDemRdng 
         Height          =   285
         Left            =   1700
         MaxLength       =   6
         TabIndex        =   7
         Top             =   2700
         Width           =   2500
      End
      Begin VB.Label lblBookNo 
         Alignment       =   1  'Right Justify
         Caption         =   "Book No. : "
         Height          =   300
         Left            =   120
         TabIndex        =   18
         Top             =   300
         Width           =   1500
      End
      Begin VB.Label lblMtrNo 
         Alignment       =   1  'Right Justify
         Caption         =   "Meter No. : "
         Height          =   300
         Left            =   120
         TabIndex        =   17
         Top             =   675
         Width           =   1500
      End
      Begin VB.Label lblDateRdng 
         Alignment       =   1  'Right Justify
         Caption         =   "Reading Date : "
         Height          =   300
         Left            =   120
         TabIndex        =   16
         Top             =   1005
         Width           =   1500
      End
      Begin VB.Label lblTimeRdng 
         Alignment       =   1  'Right Justify
         Caption         =   "Reading Time : "
         Height          =   300
         Left            =   120
         TabIndex        =   15
         Top             =   1350
         Width           =   1500
      End
      Begin VB.Label lblCustName 
         Alignment       =   1  'Right Justify
         Caption         =   "Customer Name : "
         Height          =   300
         Left            =   120
         TabIndex        =   14
         Top             =   1695
         Width           =   1500
      End
      Begin VB.Label lblCustAdd 
         Alignment       =   1  'Right Justify
         Caption         =   "Cust. Address : "
         Height          =   300
         Left            =   120
         TabIndex        =   13
         Top             =   2025
         Width           =   1500
      End
      Begin VB.Label lblKWHRdng 
         Alignment       =   1  'Right Justify
         Caption         =   "KWH Reading : "
         Height          =   300
         Left            =   120
         TabIndex        =   12
         Top             =   2370
         Width           =   1500
      End
      Begin VB.Label lblDemRdng 
         Alignment       =   1  'Right Justify
         Caption         =   "Meter Brand : "
         Height          =   300
         Left            =   120
         TabIndex        =   11
         Top             =   2715
         Width           =   1500
      End
   End
   Begin VB.Label lblAcctNo 
      Alignment       =   1  'Right Justify
      Caption         =   "Account No. : "
      Height          =   300
      Left            =   240
      TabIndex        =   22
      Top             =   6465
      Width           =   1500
   End
   Begin VB.Label lblSeqNo 
      Alignment       =   1  'Right Justify
      Caption         =   "Sequence No. : "
      Height          =   300
      Left            =   240
      TabIndex        =   21
      Top             =   6135
      Width           =   1500
   End
End
Attribute VB_Name = "frmFoundConn"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdSave_Click()
    Dim l_int_Index As Integer
    
    If Not ValidEntries Then Exit Sub
    
    If DuplicateRecords Then Exit Sub
    
    If Not SaveFConn Then Exit Sub
    
    l_int_Index = frmEditBooks.lsvFileMain.SelectedItem.Index
    With frmEditBooks.lsvFileMain.ListItems(l_int_Index)
        .Text = Trim(txtBookNo)
        '.SubItems(1) = CheckNull(txtSeqNo, True)
        '.SubItems(2) = Trim(txtAcctNo)
        .SubItems(1) = Trim(txtMtrNo)
        .SubItems(2) = Format(Trim(txtDateRdng), "mm/dd/yyyy")
        .SubItems(3) = Format(Trim(txtTimeRdng), "hh:mm:ss")
        .SubItems(4) = Trim(txtCustName)
        .SubItems(5) = Trim(txtCustAdd)
        .SubItems(6) = CheckNull(txtKWHRdng, True)
        .SubItems(7) = Trim(txtDemRdng)
    End With
    
    frmEditBooks.lsvFileMain.Refresh
    Unload Me

End Sub

Function ValidEntries() As Boolean
    ValidEntries = False
    'If Not NumericEntry(txtSeqNo, lblSeqNo) Then Exit Function
    
    If Len(Trim(txtDateRdng)) > 0 Then
        If Not IsDate(Trim(txtDateRdng)) Then
            MsgBox "Reading Date entry is not a valid date.  Please re-enter a valid date.", vbExclamation, "Invalid Entry"
            txtDateRdng.SetFocus
            Exit Function
        End If
    End If
    
    If Len(Trim(txtTimeRdng)) > 0 Then
        If Not IsDate(Trim(txtTimeRdng)) Then
            MsgBox "Reading Time entry is not a valid time.  Please re-enter a valid time.", vbExclamation, "Invalid Entry"
            txtTimeRdng.SetFocus
            Exit Function
        End If
    End If
    
    If Not NumericEntry(txtKWHRdng, lblKWHRdng) Then Exit Function
    'If Not NumericEntry(txtDemRdng, lblKWHRdng) Then Exit Function
    ValidEntries = True
    
End Function

Function DuplicateRecords() As Boolean
    Dim l_str_Sql As String
    
    DuplicateRecords = True
    
    With frmEditBooks.lsvFileMain.SelectedItem
        If .Text = Trim(txtBookNo) And _
                .SubItems(1) = Trim(txtMtrNo) And _
                .SubItems(2) = Trim(txtDateRdng) And .SubItems(3) = Trim(txtTimeRdng) And _
                .SubItems(4) = Trim(txtCustName) And .SubItems(5) = Trim(txtCustAdd) And _
                .SubItems(6) = Trim(txtKWHRdng) And .SubItems(7) = Trim(txtDemRdng) Then
            Unload Me
            Exit Function
        End If
    
'        If .Text = Trim(txtBookNo) And .SubItems(1) = Trim(txtSeqNo) And _
'                .SubItems(2) = Trim(txtAcctNo) And .SubItems(3) = Trim(txtMtrNo) And _
'                .SubItems(4) = Trim(txtDateRdng) And .SubItems(5) = Trim(txtTimeRdng) And _
'                .SubItems(6) = Trim(txtCustName) And .SubItems(7) = Trim(txtCustAdd) And _
'                .SubItems(8) = Trim(txtKWHRdng) And .SubItems(9) = Trim(txtDemRdng) Then
'            Unload Me
'            Exit Function
'        End If
    End With
    
    If Trim(txtBookNo) = frmEditBooks.lsvFileMain.SelectedItem.Text And _
            Trim(txtMtrNo) = frmEditBooks.lsvFileMain.SelectedItem.SubItems(1) Then
        DuplicateRecords = False
        Exit Function
    End If
    
    l_str_Sql = "SELECT BOOKNO FROM T_FCONN WHERE BOOKNO = '" & Trim(txtBookNo) & _
        "' AND MTRNO = '" & Trim(txtMtrNo) & "'"
            
'    If Trim(txtBookNo) = frmEditBooks.lsvFileMain.SelectedItem.Text And _
'            Trim(txtAcctNo) = frmEditBooks.lsvFileMain.SelectedItem.SubItems(2) Then
'        DuplicateRecords = False
'        Exit Function
'    End If
'
'    l_str_Sql = "SELECT BOOKNO FROM T_FCONN WHERE BOOKNO = '" & Trim(txtBookNo) & _
'        "' AND ACCTNUM = '" & Trim(txtAcctNo) & "'"
            
    If OpenRecordset(g_rs_TFCONN, l_str_Sql, Me.Name, "DuplicateRecords") Then
        MsgBox "Record already exists!", vbExclamation, "Duplicate Record"
        g_rs_TFCONN.Close
        Set g_rs_TFCONN = Nothing
        txtBookNo.SetFocus
        Exit Function
    End If

    DuplicateRecords = False
    
End Function

Function SaveFConn() As Boolean
    Dim l_str_Sql As String
    Dim bookn As String
    Dim acct As String
    Dim mtrnum As String
    
    bookn = frmEditBooks.lsvFileMain.SelectedItem.Text
    'acct = frmEditBooks.lsvFileMain.SelectedItem.SubItems(2)
    mtrnum = frmEditBooks.lsvFileMain.SelectedItem.SubItems(1)
    
    l_str_Sql = "UPDATE T_FCONN SET BOOKNO = '" & Left(Trim(txtBookNo), 6) & _
        "', MTRNO = '" & Left(Trim(txtMtrNo), 10) & _
        "', RDGDATE = '" & IIf(Format(Trim(txtDateRdng), "mm/dd/yyyy") = "", "01-01-1901", Format(Trim(txtDateRdng), "mm/dd/yyyy")) & _
        "', RDGTIME = '" & Format(Trim(txtTimeRdng), "hh:mm:ss") & _
        "', CUSTNAME = '" & Left(Trim(txtCustName), 25) & _
        "' WHERE BOOKNO = '" & bookn & _
        "' AND MTRNO = '" & mtrnum & "'"
        
'    l_str_Sql = "UPDATE T_FCONN SET BOOKNO = '" & Left(Trim(txtBookNo), 6) & _
'        "', SEQNO = '" & CheckNull(txtSeqNo, True) & _
'        "', ACCTNUM = '" & Left(Trim(txtAcctNo), 12) & _
'        "', MTRNO = '" & Left(Trim(txtMtrNo), 10) & _
'        "', RDGDATE = '" & Format(Trim(txtDateRdng), "mm/dd/yyyy") & _
'        "', RDGTIME = '" & Format(Trim(txtTimeRdng), "hh:mm:ss") & _
'        "', CUSTNAME = '" & Left(Trim(txtCustName), 25) & _
'        "' WHERE BOOKNO = '" & bookn & _
'        "' AND ACCTNUM = '" & acct & "'"
        
    SaveFConn = DBExecute(l_str_Sql, Me.Name, "SaveFConn")

    l_str_Sql = "UPDATE T_FCONN SET  ADDRESS = '" & Left(Trim(txtCustAdd), 25) & _
        "', READING = " & CheckNull(txtKWHRdng, True) & _
        ", MTRBRAND = '" & txtDemRdng & _
        "' WHERE BOOKNO = '" & bookn & _
        "' AND MTRNO = '" & mtrnum & "'"
        
'    l_str_Sql = "UPDATE T_FCONN SET  ADDRESS = '" & Left(Trim(txtCustAdd), 25) & _
'        "', READING = " & CheckNull(txtKWHRdng, True) & _
'        ", DEMDRDG = '" & CheckNull(txtDemRdng, True) & _
'        "' WHERE BOOKNO = '" & bookn & _
'        "' AND MTRNO = '" & mtrnum & "'"
        
    SaveFConn = DBExecute(l_str_Sql, Me.Name, "SaveFConn")

End Function

Private Sub Form_Load()
If WindowState = vbNormal Then
    Me.Move (1000)
    Me.Top = 500
End If
    With frmEditBooks.lsvFileMain.SelectedItem
        txtBookNo = .Text
        'txtSeqNo = .SubItems(1)
        'txtAcctNo = .SubItems(2)
        txtMtrNo = .SubItems(1)
        txtDateRdng = .SubItems(2)
        txtTimeRdng = .SubItems(3)
        txtCustName = .SubItems(4)
        txtCustAdd = .SubItems(5)
        txtKWHRdng = .SubItems(6)
        txtDemRdng = .SubItems(7)
    End With

End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmEditBooks.Enabled = True
End Sub

Private Sub txtAcctNo_Change()
    txt_Change
End Sub

Private Sub txtAcctNo_GotFocus()
    TextHighlight txtAcctNo
End Sub

Private Sub txtBookNo_Change()
    txt_Change
End Sub

Private Sub txtBookNo_GotFocus()
    TextHighlight txtBookNo
End Sub

Private Sub txtCustAdd_GotFocus()
    TextHighlight txtCustAdd
End Sub

Private Sub txtCustName_GotFocus()
    TextHighlight txtCustName
End Sub

Private Sub txtDateRdng_GotFocus()
    TextHighlight txtDateRdng
End Sub

Private Sub txtDemRdng_GotFocus()
    TextHighlight txtDemRdng
End Sub

Private Sub txtKWHRdng_GotFocus()
    TextHighlight txtKWHRdng
End Sub

Private Sub txtMtrNo_GotFocus()
    TextHighlight txtMtrNo
End Sub

Private Sub txtSeqNo_GotFocus()
    TextHighlight txtSeqNo
End Sub

Private Sub txtTimeRdng_GotFocus()
    TextHighlight txtTimeRdng
End Sub

Private Sub txt_Change()
    If Len(Trim(txtBookNo)) > 0 Then 'And Len(Trim(txtMtrNo)) > 0 Then
        cmdSave.Enabled = True
    Else
        cmdSave.Enabled = False
    End If
    
End Sub

